Ext.define('Pce.prestamos.PrestamoInfo', {
    extend: 'Ext.data.Model',

    fields: [
        'titular',
        'dependencia',
        'folio',
        'importe',
        'plazo',
        'tasaInteres',
        'descuentoQuincenal',
        {name: 'fechaPrimerDescuento', type: 'date'},
        'tipoPrestamo',
        'cuentaTitular',
        'bancoTitular',
        'etapa',
        'clabeInterbancariaTitular',
        'claveBancoSat',
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false}
    ]
});