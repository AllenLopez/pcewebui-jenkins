/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.TipoPrestamoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.prestamos-tipo-prestamo',
    model: 'Pce.prestamos.TipoPrestamo'
});