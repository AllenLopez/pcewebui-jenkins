// Ext.define('Pce.prestamos.banco.Banco', {
//     extend: 'Ext.data.Model',
    
//     fields: [
//         'bancoNombre',
//         'descripcion',
//         'claveBancoSat',
//         'estatus',
//         {name: 'fechaCreacion', type: 'date', persist: false},
//         {name: 'usuarioCreacion', persist: false},
//         {name: 'fechaModificacion', type: 'date', persist: false},
//         {name: 'usuarioModificacion', persist: false}
//     ],
    
//     proxy: {
//         type: 'rest',
//         url: '/api/prestamos/bancos',
//         writer: {
//             type: 'json',
//             writeAllFields: true
//         }
//     }
// });