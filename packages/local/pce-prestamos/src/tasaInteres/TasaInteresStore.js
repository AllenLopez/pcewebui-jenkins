Ext.define('Pce.prestamos.tasaInteres.TasaInteresStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tasa-interes',
    model: 'Pce.prestamos.tasaInteres.TasaInteres'
});