Ext.define('Pce.prestamos.tasaInteres.TasaInteres', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.proxy.Rest'
    ],
    fields: [
        'tasa',
        {name: 'fechaAplicacion', type: 'date', persist: false},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'UsuarioCreacion', persist: false},
        {name: 'fechaModif', type: 'date', persist: false},
        {name: 'usuarioModif', persist: false},
    ],
    proxy: {
        type: 'rest',
        url: '/api/prestamos/tasa-interes',
        writer: {
            type: 'json',
            writeAllFields: true
        },
        reader: {
            type: 'json'
        }
    }
});