// Ext.define('Pce.prestamos.cuentaBanco.CuentaBanco', {
//     extend: 'Ext.data.Model',
    
//     fields: [
//         'bancoId',
//         'numeroCuenta',
//         'descripcion',
//         'clabeInterbancaria',
//         'claveBancoSat',
//         'estatus',
//         {name: 'fechaCreacion', type: 'date', persist: false},
//         {name: 'fechaModificacion', type: 'date', persist: false},
//         {name: 'usuarioCreacion', persist: false},
//         {name: 'usuarioModificacion', persist: false}
//     ],

//     proxy: {
//         type: 'rest',
//         url: '/api/prestamos/cuenta-banco',
//         writer: {
//             type: 'json',
//             writeAllFields: true,
//         }
//     }
// });