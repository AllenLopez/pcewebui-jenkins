Ext.define('Pce.prestamos.solicitud.Solicitud', {
    extend: 'Pce.prestamos.PrestamoInfo',
    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [{
        name: 'folio',
        persist: false,
        convert(v) {
            return String(v).padStart(8, '0');
        }
    }],

    proxy: {
        type: 'rest',
        url: '/api/prestamos/solicitud',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});