/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.solicitud.SolicitudStore', {
    extend: 'Ext.data.Store',
    alias: 'store.prestamos-solicitud',
    model: 'Pce.prestamos.solicitud.Solicitud'
});