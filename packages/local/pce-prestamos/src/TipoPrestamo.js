Ext.define('Pce.prestamos.TipoPrestamo', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: ['clave', 'descripcion'],

    proxy: {
        type: 'rest',
        url: '/api/prestamos/tipo'
    }
});