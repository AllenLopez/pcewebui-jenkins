Ext.define('Pce.Prestamos.nomina.NominaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.nomina',
    model: 'Pce.Prestamos.nomina.Nomina',
});