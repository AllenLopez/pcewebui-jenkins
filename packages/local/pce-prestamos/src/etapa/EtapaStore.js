/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.EtapaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.prestamos-etapa',

    data: [
        {id: 'CAPTURADO'},
        {id: 'SELECCIONADO'},
        {id: 'TRASPASO'},
        {id: 'FINALIZADO'},
        {id: 'AUTORIZADO'},
        {id: 'CANCELADO'},
        {id: 'RECHAZADO'}
    ]
});