Ext.define('Pce.prestamos.descuento-quincenal.CargoInteresStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cargo-interes',
    model: 'Pce.prestamos.descuento-quincenal.CargoInteres'
});