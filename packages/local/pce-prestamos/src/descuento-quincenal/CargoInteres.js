Ext.define('Pce.prestamos.descuento-quincenal.CargoInteres', {
    extend: 'Ext.data.Model',
    
    fields: [
        'tasaInteres',
        'fechaAplicacion',
        'idTransaccion',
        'estatus',
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/prestamos/cargo-interes',
        writer: {
            type: 'json',
            writeAllFields: true,
        }
    }
});