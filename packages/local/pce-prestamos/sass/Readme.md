# pce-prestamos/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    pce-prestamos/sass/etc
    pce-prestamos/sass/src
    pce-prestamos/sass/var
