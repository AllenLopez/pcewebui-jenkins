Ext.define('Pce.prestamos.tipo.TipoPrestamoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'prestamos-tipo-prestamo-selector',

    requires: [
        'Pce.prestamos.TipoPrestamo'
    ],

    queryMode: 'local',
    valueField: 'clave',
    displayField: 'clave',
    tpl:
    '<tpl for=".">' +
        '<li class="x-boundlist-item">' +
            '{clave}: {descripcion}' +
        '</li>' +
    '</tpl>',
    displayTpl:
    '<tpl for=".">' +
        '{clave}: {descripcion}' +
    '</tpl>',

    store: {
        model: 'Pce.prestamos.TipoPrestamo',
        autoLoad: true
    }
});