/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.EtapaSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'prestamos-etapa-selector',

    requires: [
        'Pce.prestamos.EtapaStore'
    ],

    valueField: 'id',
    displayField: 'id',
    store: {
        type: 'prestamos-etapa'
    }
});