Ext.define('Pce.prestamos.tasaInteres.TasaInteresTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'tasa-interes-tabla',
    requires: [
        'Pce.prestamos.tasaInteres.TasaInteresController',
        'Pce.prestamos.tasaInteres.TasaInteresStore'
    ],

    controller: 'tasa-de-interes',
    store: {
        model: 'Pce.prestamos.tasaInteres.TasaInteres',
        type: 'tasa-interes',
        autoLoad: false,
    },

    title: 'Catalogo de tasas de interes',
    tbar: [{
        text: 'Agregar tasa de interes',
        handler: 'onAgregarTasaInteres'
    }],
    
    columns: [{
        text: 'Tasa de Interes',
        dataIndex: 'tasa',
        flex: 1
    }, {
        text: 'Fecha de uso',
        dataIndex: 'fechaCreacion',
        flex: 3,
        renderer: Ext.util.Format.dateRenderer('d/m/Y'),
    }, {
        xtype: 'actioncolumn',
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-trash',
            handler: 'onEliminarTasaInteres'
        }]
    }],
});