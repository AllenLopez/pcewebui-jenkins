Ext.define('Pce.prestamos.tasaInteres.DialogoEdicionTasaInteres', {
    extend: 'Ext.window.Window',
    xtype: 'dialogo-edicion-tasa-interes',
    requires: [
        'Pce.prestamos.tasaInteres.TasaInteresForma'
    ],
    items: {
        xtype: 'tasa-interes-forma'
    },
    bodyPadding: 10,

    buttons: [{
        text: 'Aceptar',
        handler() {
            this.up('window').guardar();
        }
    }, {
        text: 'Cancelar',
        handler() {
            this.up('window').close();
        }
    }],

    guardar() {
        let form = this.down('form');
        let record = form.getRecord();
        if (!form.isValid()) {
            return;
        }
        form.updateRecord();
        this.fireEvent('save', this, record);
        this.close();
    },

    afterRender() {
        this.callParent();
        if (this.tasaInteres) {
            this.down('form').loadRecord(this.tasaInteres);
        }
    }
});