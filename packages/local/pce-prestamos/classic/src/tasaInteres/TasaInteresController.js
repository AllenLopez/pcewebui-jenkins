Ext.define('Pce.prestamos.tasaInteres.TasaInteresController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.tasa-de-interes',

    onAgregarTasaInteres() {
        let tasaInteres = new Pce.prestamos.tasaInteres.TasaInteres();
        this.mostrarEdicionTasaInteres(tasaInteres, 'Nueva tasa de interes');
    },

    afterRender() {
        this.cargaTasaInteres();
    },

    cargaTasaInteres() {
        let tabla = this.getView();
        let store = tabla.getStore();

        store.load();
    },

    mostrarEdicionTasaInteres(tasaInteres, titulo) {
        Ext.create({
            xtype: 'dialogo-edicion-tasa-interes',
            title: titulo,
            width: 300,
            autoShow: true,
            modal: true,
            tasaInteres: tasaInteres,
            listeners: {
                save: {
                    scope: this,
                    fn: this.onGuardar
                }
            }
        });
    },

    onEliminarTasaInteres(grid, row) {
        let record = grid.getStore().getAt(row);
        let store = this.getView().getStore();

        Ext.Msg.confirm(
            'Confirmar operación',
            `¿Estás seguro que deseas eliminar esta
            tasa de interés <b>${record.get('tasa')}`,
            ans => {
                if(ans === 'yes'){
                    store.remove(record);
                    store.sync({
                        failure: function(batch, operations){
                            store.rejectChanges();
                            batch.exceptions.forEach(element => {
                                Ext.toast(element.error.response.responseText);
                            });
                            
                            return false;
                        }
                    });
                }
            });
    },

    onGuardar(win, tasaInteres) {
        let store = this.view.getStore();
        
        if (tasaInteres.phantom) {
            store.add(tasaInteres);
        }

        store.save();
    }
});