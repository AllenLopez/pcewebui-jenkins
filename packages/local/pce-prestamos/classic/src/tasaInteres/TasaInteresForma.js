Ext.define('Pce.prestamos.tasaInteres.TasaInteresForma', {
    extend: 'Ext.form.Panel',
    xtype: 'tasa-interes-forma',
    requires: [
        'Ext.form.field.Text'
    ],
    layout: {
        type: 'vbox',
        align: 'stretch',
    },
    defaults: {
        xtype: 'container',
        layout: 'hbox'
    },

    items: [{
        items: [{
            xtype: 'textfield',
            name: 'tasa',
            fieldLabel: 'Agregar tasa de interes',
            flex: 1,
            allowBlank: false
        }, {
            xtype: 'hiddenfield',
            name: 'usuarioCreacion',
            value: 'A',
        }],
    }],
    loadRecord(record) {
        this.callParent(arguments);
    }
});