Ext.define('Pce.prestamos.solicitud.SolicitudCapturaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.prestamos-solicitud-captura',

    requires: [
        'Pce.prestamos.solicitud.SolicitudForma',
        'Pce.prestamos.solicitud.SolicitudEtapaHandler',
        'Pce.view.dialogo.FormaContenedor'
    ],

    /**
     * Dependencia de utilidad para manejar los cambios de etapa
     * de una solicitud
     *
     * @private {Pce.prestamos.solicitud.SolicitudEtapaHandler}
     */
    etapaHandler: null,

    initViewModel(vm) {
        this.etapaHandler = new Pce.prestamos.solicitud.SolicitudEtapaHandler(
            vm.get('solicitudes')
        );
    },

    onNuevaSolicitudTap() {
        this.editarSolicitud(new Pce.prestamos.solicitud.Solicitud({
            titular: this.getViewModel().get('asegurado').getId()
        }));
    },
    onEditarSolicitudTap() {
        this.editarSolicitud(this.getSolicitud());
    },

    /**
     * Inicia la edición de la solicitud proporcionada
     *
     * @param solicitud
     */
    editarSolicitud(solicitud) {
        let data = solicitud.data;
        // noinspection JSUnresolvedVariable
        let title = solicitud.phantom ? 'Nueva solicitud'
            : `Solicitud ${data.folio} - ${data.titularNombreCompleto}`;
        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title,
            record: solicitud,
            items: {xtype: 'prestamos-solicitud-forma'},
            width: 800,
            listeners: {
                guardar: this.onGuardarSolicitud.bind(this)
            }
        });
    },

    /**
     * @param {Ext.window.Window} dialogo
     * @param {Pce.prestamos.solicitud.Solicitud} solicitud
     */
    onGuardarSolicitud(dialogo, solicitud) {
        dialogo.close();
        solicitud.save({
            success: (solicitud) => {
                let solicitudes = this.getViewModel().get('solicitudes');
                if (!solicitudes.contains(solicitud)) {
                    solicitudes.add(solicitud);
                } else {
                    solicitudes.reload();
                }
            }
        });
    },

    /**
     * Reacciona al evento de recargar los datos contenidos en la tabla
     * de solicitudes
     */
    onSolicitudesRefreshTap() {
        this.getViewModel().get('solicitudes').reload();
    },

    /**
     * Reacciona al evento de marcar una solicitud como seleccionada
     */
    onSeleccionarSolicitudTap() {
        this.etapaHandler.seleccionarSolicitud(this.getSolicitud());
    },

    /**
     * Reacciona al evento de marcar una solicitud como cancelada
     */
    onCancelarSolicitud() {
        this.etapaHandler.cancelarSolicitud(this.getSolicitud());
    },

    /**
     * Reacciona al evento de marcar una solicitud como autorizada
     */
    onAutorizarSolicitudTap() {
        this.etapaHandler.autorizarSolicitud(this.getSolicitud());
    },

    /**
     * Reacciona a la selección de un asegurado
     *
     * @param selector
     * @param numeroAfiliacion
     */
    onAseguradoChange(selector, numeroAfiliacion) {
        this.getViewModel().get('solicitudes').load({
            params: {
                numeroAfiliacion
            }
        });
    },

    /**
     * Regresa la solicitud almacenada en el ViewModel
     *
     * @returns {Pce.prestamos.solicitud.Solicitud|null}
     */
    getSolicitud() {
        return this.getViewModel().get('solicitud');
    }
});