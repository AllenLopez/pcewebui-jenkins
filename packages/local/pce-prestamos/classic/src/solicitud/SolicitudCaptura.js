Ext.define('Pce.prestamos.solicitud.SolicitudCaptura', {
    extend: 'Ext.panel.Panel',
    xtype: 'prestamos-solicitud-captura',

    requires: [
        'Pce.prestamos.solicitud.SolicitudCapturaController',

        'Pce.prestamos.solicitud.SolicitudTabla',
        'Pce.prestamos.solicitud.SolicitudToolbar',
        'Pce.prestamos.solicitud.SolicitudStore',
        'Pce.afiliacion.asegurado.AseguradoSelector'
    ],

    controller: 'prestamos-solicitud-captura',
    viewModel: {
        data: {
            asegurado: null,
            solicitud: null,
            deshabilitarCaptura: false
        },
        stores: {
            solicitudes: {
                type: 'prestamos-solicitud',
                proxy: {
                    type: 'ajax',
                    url: '/api/prestamos/solicitud/por-titular'
                }
            }
        }
    },

    title: 'Captura de solicitud de préstamo personal',
    bodyPadding: 10,
    layout: {
        type: 'vbox'
    },

    items: [{
        xtype: 'asegurado-selector',
        bind: {
            selection: '{asegurado}'
        },
        listeners: {
            change: 'onAseguradoChange'
        }
    }, {
        xtype: 'prestamos-solicitud-tabla',
        flex: 1,
        width: '100%',
        tbar: {xtype: 'prestamos-solicitud-toolbar'},
        bind: {
            title: 'Solicitudes {asegurado.nombreCompleto}',
            disabled: '{!asegurado}',
            store: '{solicitudes}',
            selection: '{solicitud}'
        }
    }]
});