/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.solicitud.SolicitudViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.prestamos-solicitud',

    data: {
        titular: null,
        plazo: 24,
        importe: 15000
    },
    formulas: {
        descuentoQuincenal(get) {
            let plazo = get('plazo');
            let importe = get('importe');

            return importe / plazo;
        },
        fechaPrimerDescuento() {
            let fecha = new Date();
            let dia = fecha.getDate();

            if (dia >= 6 && dia <= 20) {
                fecha.setMonth(fecha.getMonth() + 1);
                fecha.setDate(0);
            } else {
                if (dia > 20) {
                    fecha.setDate(1);
                    fecha.setMonth(fecha.getMonth() + 1);
                }
                fecha.setDate(15);
            }

            return fecha;
        }
    }
});