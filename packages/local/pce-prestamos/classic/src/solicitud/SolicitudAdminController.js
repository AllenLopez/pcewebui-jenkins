/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.solicitud.SolicitudAdminController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.prestamos-solicitud-admin',


    onEtapaFiltroChange(selector, etapa) {
        let vm = this.getViewModel();
        vm.get('solicitudes').load({
            params: {
                etapa
            }
        });
    }
});