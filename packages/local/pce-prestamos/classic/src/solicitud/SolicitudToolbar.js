/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.solicitud.SolicitudToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'prestamos-solicitud-toolbar',

    items: [{
        text: 'Nueva solicitud',
        handler: 'onNuevaSolicitudTap',
        hidden: true,
        bind: {
            hidden: '{deshabilitarCaptura}'
        }
    }, {
        text: 'Editar solicitud',
        handler: 'onEditarSolicitudTap',
        disabled: true,
        bind: {
            disabled: '{!solicitud || solicitud.etapa != "CAPTURADO"}'
        }
    }, {
        text: 'Marcar como seleccionada',
        handler: 'onSeleccionarSolicitudTap',
        hidden: true,
        bind: {
            hidden: '{!solicitud || solicitud.etapa != "CAPTURADO"}'
        }
    }, {
        text: 'Autorizar solicitud',
        handler: 'onAutorizarSolicitudTap',
        hidden: true,
        bind: {
            hidden: '{!solicitud || solicitud.etapa != "SELECCIONADO"}'
        }
    }, {
        text: 'Cancelar solicitud',
        handler: 'onCancelarSolicitud',
        hidden: true,
        bind: {
            hidden: '{!solicitud || solicitud.etapa == "CANCELADO" || solicitud.etapa == "AUTORIZADO"}'
        }
    }, {
        xtype: 'component',
        hidden: true,
        bind: {
            html: '<span class="color-invalid bold">Solicitud cancelada</span>',
            hidden: '{!solicitud || solicitud.etapa != "CANCELADO"}'
        }
    }, '->', {
        iconCls: 'x-fa fa-refresh',
        handler: 'onSolicitudesRefreshTap'
    }]
});