Ext.define('Pce.prestamos.solicitud.SolicitudFormaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.prestamos-solicitud-forma',

    onSolicitudGuardar() {
        if (this.view.isValid()) {
            this.guardarSolicitud();
        }
    },

    guardarSolicitud() {
        let solicitud = this.view.getRecord();
        if (!solicitud) {
            let values = this.view.getForm().getFieldValues();
            solicitud = new Pce.prestamos.solicitud.Solicitud(values);
        }

        solicitud.save({
            success: this.onSolicitudSaveSuccess.bind(this)
        });
    },

    onSolicitudSaveSuccess(solicitud) {
        console.log(solicitud);
        Ext.Msg.alert('Solicitud guardada', 'La solicitud ha sido registrada');
    }
});