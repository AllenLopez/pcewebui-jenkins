Ext.define('Pce.prestamos.solicitud.SolicitudTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'prestamos-solicitud-tabla',

    emptyText: 'No hay solicitudes que mostrar',
    columns: [{
        text: 'Folio de solicitud',
        dataIndex: 'folio',
        flex: 1,
        renderer(folio) {
            return `<b>${folio}</b>`;
        }
    }, {
        text: 'Número afil.',
        dataIndex: 'titular',
        flex: 1
    }, {
        text: 'Titular',
        dataIndex: 'titularNombreCompleto',
        flex: 2
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        flex: 2
    }, {
        xtype: 'datecolumn',
        text: 'Fecha captura',
        dataIndex: 'fechaCaptura',
        format: 'd/m/Y H:i:s',
        flex: 1
    }, {
        text: 'Etapa',
        dataIndex: 'etapa',
        flex: 1,
        renderer(etapa) {
            if (etapa === 'CANCELADO') {
                etapa = `<span class="color-invalid bold">${etapa}</span>`;
            } else if (etapa === 'AUTORIZADO') {
                etapa = `<span class="color-approved bold">${etapa}</span>`;
            }
            return etapa;
        }
    }],
});