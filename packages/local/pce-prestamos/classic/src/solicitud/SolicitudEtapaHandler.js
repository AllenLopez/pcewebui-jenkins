/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.solicitud.SolicitudEtapaHandler', {

    /**
     * @private {Ext.data.Store}
     */
    store: undefined,

    constructor(store) {
        this.store = store;
    },

    /**
     * Lógica para marcar una solicitud como seleccionada
     *
     * @param {Pce.prestamos.solicitud.Solicitud} solicitud
     */
    seleccionarSolicitud(solicitud) {
        solicitud.set('etapa', 'SELECCIONADO');
        solicitud.save({
            success: () => {
                this.store.reload();
                Ext.Msg.alert(
                    'Solicitud seleccionada',
                    `La solicitud ${this.formatoFolio(solicitud)} ha 
                    sido marcada como seleccionada`
                );
            }
        });
    },

    /**
     * Lógica para autorizar una solicitud
     *
     * @param {Pce.prestamos.solicitud.Solicitud} solicitud
     */
    autorizarSolicitud(solicitud) {
        Ext.Msg.confirm(
            'Autorizar solicitud',
            `Autorizar la solicitud ${this.formatoFolio(solicitud)} generará un 
            pagaré con los datos de la solicitud. ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    solicitud.set('etapa', 'AUTORIZADO');
                    solicitud.save({
                        success: () => this.store.reload()
                    });
                }
            }
        );
    },

    /**
     * Lógica para cancelar una solicitud
     *
     * @param {Pce.prestamos.solicitud.Solicitud} solicitud
     */
    cancelarSolicitud(solicitud) {
        Ext.Msg.confirm(
            'Cancelar solicitud',
            `La solicitud ${this.formatoFolio(solicitud)} será cancelada. ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    solicitud.set('etapa', 'CANCELADO');
                    solicitud.save({
                        success: () => this.store.reload
                    });
                }
            }
        );
    },

    formatoFolio(solicitud) {
        return `<b>${solicitud.data.folio}</b>`;
    }
});