Ext.define('Pce.prestamos.solicitud.SolicitudForma', {
    extend: 'Ext.form.Panel',
    xtype: 'prestamos-solicitud-forma',

    requires: [
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.prestamos.tipo.TipoPrestamoSelector',

        'Pce.prestamos.solicitud.SolicitudViewModel',
        'Pce.prestamos.solicitud.SolicitudFormaController'
    ],

    controller: 'prestamos-solicitud-forma',
    viewModel: 'prestamos-solicitud',
    layout: {
        type: 'table',
        columns: 6
    },

    defaults: {
        width: '100%',
        labelAlign: 'top',
        labelSeparator: ''
    },
    items: [{
        xtype: 'component',
        bind: {
            html: '{aseguradoNombreCompleto}'
        },
        colspan: 6
    }, {
        xtype: 'dependencia-selector',
        name: 'dependencia',
        colspan: 6,
        allowBlank: false
    }, {
        xtype: 'prestamos-tipo-prestamo-selector',
        name: 'tipoPrestamo',
        fieldLabel: 'Tipo de préstamo',
        colspan: 5,
        allowBlank: false
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Importe',
        name: 'importe',
        allowBlank: false,
        bind: '{importe}',
        colspan: 1,
        validator: importe => {
            if (importe <= 0) {
                return 'Importe inválido';
            }
            return true;
        }
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Plazo',
        name: 'plazo',
        allowBlank: false,
        allowDecimals: false,
        bind: '{plazo}',
        colspan: 1
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Tasa de interés',
        name: 'tasaInteres',
        colspan: 1
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Desc. quincenal',
        name: 'descuentoQuincenal',
        readOnly: true,
        bind: '{descuentoQuincenal}',
        cls: 'read-only',
        colspan: 1
    }, {
        xtype: 'datefield',
        fieldLabel: 'Fecha primer desc.',
        name: 'fechaPrimerDescuento',
        allowBlank: false,
        bind: '{fechaPrimerDescuento}',
        colspan: 3
    }, {
        xtype: 'textfield',
        name: 'bancoTitular',
        fieldLabel: 'Banco del titular',
        allowBlank: false,
        colspan: 3
    }, {
        xtype: 'textfield',
        name: 'cuentaTitular',
        fieldLabel: 'Cuenta del titular',
        allowBlank: false,
        colspan: 3
    }]
});