/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.prestamos.solicitud.SolicitudAdminPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'prestamos-solicitud-admin',

    requires: [
        'Pce.prestamos.solicitud.SolicitudAdminController',
        'Pce.prestamos.solicitud.SolicitudCapturaController',

        'Pce.prestamos.EtapaSelector',
        'Pce.prestamos.solicitud.SolicitudTabla',
        'Pce.prestamos.solicitud.SolicitudToolbar',
        'Pce.prestamos.solicitud.SolicitudStore'
    ],

    layout: 'fit',
    controller: 'prestamos-solicitud-admin',
    viewModel: {
        data: {
            deshabilitarCaptura: true,
            etapa: 'CAPTURADO',
            solicitud: null
        },
        stores: {
            solicitudes: {
                type: 'prestamos-solicitud'
            }
        }
    },

    tbar: [{
        xtype: 'prestamos-etapa-selector',
        fieldLabel: 'Mostrar solicitudes en la etapa',
        labelWidth: 200,
        width: 500,
        bind: '{etapa}',
        listeners: {
            change: 'onEtapaFiltroChange'
        }
    }],
    items: [{
        xtype: 'prestamos-solicitud-tabla',
        controller: 'prestamos-solicitud-captura',
        viewModel: {},
        tbar: {xtype: 'prestamos-solicitud-toolbar'},
        bind: {
            store: '{solicitudes}',
            selection: '{solicitud}'
        }
    }]
});