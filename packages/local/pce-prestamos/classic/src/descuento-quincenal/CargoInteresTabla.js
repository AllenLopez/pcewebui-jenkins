/**
 * @class Pce.prestamos.descuento-quincenal.CargoInteresTabla
 * @extends Ext.grid.Panel
 * @xtype cargo-interes-tabla
 * Tabla que enlista pólizas a nivel maestro
 */
Ext.define('Pce.prestamos.descuento-quincenal.CargoInteresTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'cargo-interes-tabla',

    requires: [
        'Pce.prestamos.descuento-quincenal.CargoInteresStore'
    ],

    store: {
        type: 'cargo-interes',
        autoLoad: true
    },

    emptyText: 'No existen registros',

    columns: [{
        text: 'Cargo Interés',
        dataIndex: 'id',
        flex: 1
    }, {
        text: 'Tasa',
        dataIndex: 'tasaInteres',
        flex: 1
    }, {
        xtype: 'datecolumn',
        text: 'Fecha',
        dataIndex: 'fechaAplicacion',
        format: 'd/m/Y',
        flex: 1
    }, {
        text: 'Estado',
        dataIndex: 'estatus',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        flex: 3,
        width: 50,
        align: 'center',
        items: [ {
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar cargo interés',
            handler: 'onEliminarCargoInteres'
        }]
    }],

  
});