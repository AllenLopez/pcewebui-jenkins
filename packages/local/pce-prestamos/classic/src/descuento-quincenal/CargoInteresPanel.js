Ext.define('Pce.prestamos.descuento-quincenal.CargoInteresPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'prestamos-cargo-interes',

    requires: [
        'Pce.ingresos.quincena.QuincenaSelector',
        'Pce.prestamos.descuento-quincenal.CargoInteresTabla',
        'Pce.prestamos.descuento-quincenal.CargoInteresController'
    ],

    controller: 'cargo-interes-controller',
    viewModel: {
        data: {
            asegurado: null,
            solicitud: null,
            deshabilitarCaptura: false
        },
    },

    title: 'Proceso cargo interés',
    // bodyPadding: 10,
    layout: {
        type: 'fit'
    },

    items: [
        {
            xtype: 'cargo-interes-tabla',
            reference: 'cargoInteresTabla',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'numberfield',
                    fieldLabel: 'Tasa Interés',
                    allowBlank: true,
                    reference: 'tasaInteres',
                    // blankText: 'Este campo es obligatorio',
                    listeners:{
                        change: 'onChangeTasa'
                    }
                },{
                    xtype: 'quincena-selector',
                    // flex: 3,
                    valueField: 'fecha',
                    allowBlank: true,
                    reference: 'quincenaSelector',
                    store: {
                        type: 'quincena',
                        autoLoad: true,
                        proxy: {
                            type: 'rest',
                            url: '/api/ingresos/quincena/ejercicio-actual',
                        },
                    },
                    listeners: {
                        change: 'onSeleccionQuincena'
                    },
                    width: 500,
                    blankText: 'Este campo es obligatorio'
                },{
                    text: 'Calcular',
                    disabled: true,
                    reference: 'btnCalcular',
                    handler: 'onCalcularCargoInteres'
                }]
            },
        
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    text: 'Revisión por dependencia',
                    handler: 'onRevisionPorDependencia',
                    bind: {
                        disabled: '{!cargoInteresTabla.selection }'
                    }
                },
                {
                    text: 'Revisión detallada',
                    handler: 'onRevisionDetallada',
                    bind: {
                        disabled: '{!cargoInteresTabla.selection }'
                    }
                },
                {
                    text: 'Afecta cartera',
                    handler: 'onAfectarCartera',
                    disabled: true,
                    bind: {
                        disabled: '{!(cargoInteresTabla.selection && cargoInteresTabla.selection.estatus == "CARGADO") }'
                    }
                },
                {
                    text: 'Enviar',
                    handler: 'onEnviar',
                    disabled: true,
                    bind: {
                        disabled: '{!(cargoInteresTabla.selection && cargoInteresTabla.selection.estatus == "PROCESADO") }'
                    }
                },
                {
                    text: 'Mostrar errores de la transacción',
                    handler: 'onMostrarResultados',
                    tooltip:'Mostrar errores generados en la transacción',
                    disabled: true,
                    bind: {
                        disabled: '{!cargoInteresTabla.selection }'
                    }
                }]
            }]
        }


        
    ]
});