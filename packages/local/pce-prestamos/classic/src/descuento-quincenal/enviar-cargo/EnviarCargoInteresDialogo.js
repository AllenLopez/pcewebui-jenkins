Ext.define('Pce.prestamos.descuento-quincenal.enviar-cargo.EnviarCargoInteresDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'enviar-cargo-interes-dialogo',

    requires: [
        'Pce.prestamos.descuento-quincenal.enviar-cargo.EnviarCargoInteresTabla',
        'Pce.prestamos.descuento-quincenal.enviar-cargo.EnviarCargoInteresController',
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    controller: 'enviar-cargo-interes-controller',

    config: {
        cargo: undefined
    },

    title: 'Enviar cargo interés dependencias',
    layout: 'fit',

    maximizable: true,
    // maximized: true,

    width: 800,
    height: 600,
    items: [
        {
            xtype: 'enviar-cargo-interes-tabla',
            reference: 'enviarCargoInteresTabla',
            store: {
                type: 'dependencias',
                proxy: {
                    type: 'rest',
                    url: '/api/prestamos/cargo-interes/correo-dependecias',

                },
            },
            dockedItems: [

                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [{
                        xtype: 'label',
                        reference: 'lblCargoId'
                    }, {
                        xtype: 'label',
                        reference: 'lblCargoTasa'
                    }, {
                        xtype: 'label',
                        reference: 'lblCargoFecha'
                    }]
                }],

            buttons: [{
                // iconCls: 'x-fa fa-flag-o',
                text: 'Enviar',
                handler: 'onEnviar',
                // bind: {
                //     disabled: '{!transaccion}'
                // }s
            }]
        }



    ],

    afterRender() {
        this.callParent();
        let storeTabla = this.lookup('enviarCargoInteresTabla').getStore();
        storeTabla.load({
            params: {
                cargoId: this.cargo.id
            }
        })
        this.lookup('lblCargoId').setHtml(`<b>Cargo Intéres:</b> ${this.cargo.id}`)
        this.lookup('lblCargoTasa').setHtml(`<b>Tasa Intéres:</b> ${this.cargo.get('tasaInteres')}`)
        this.lookup('lblCargoFecha').setHtml(`<b>Fecha Nómina:</b> ${Ext.util.Format.date(this.cargo.get('fechaAplicacion'), 'd/m/y')}`)
    },
});