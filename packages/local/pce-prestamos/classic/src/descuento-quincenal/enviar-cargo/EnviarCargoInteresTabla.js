/**
 * @class Pce.prestamos.descuento-quincenal.enviar-cargo.EnviarCargoInteresTabla
 * @extends Ext.grid.Panel
 * @xtype enviar-cargo-interes-tabla
 * Tabla que enlista los correos de las depencias a enviar el cargo de interes prestamo
 */
Ext.define('Pce.prestamos.descuento-quincenal.enviar-cargo.EnviarCargoInteresTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'enviar-cargo-interes-tabla',

    requires: [
        
    ],
    selModel: {
        selType: 'checkboxmodel',
        showHeaderCheckbox : true,
        checkOnly: true
    },

    emptyText: 'No existen registros',

    columns: [{
        text: 'Dependecia',
        dataIndex: 'descripcion',
        flex: 1
    }, {
        text: 'Correo',
        dataIndex: 'correo',
        flex: 1
    }],

  
});