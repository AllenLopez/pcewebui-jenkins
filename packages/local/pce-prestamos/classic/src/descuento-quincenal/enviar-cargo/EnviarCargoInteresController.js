
Ext.define('Pce.prestamos.descuento-quincenal.enviar-cargo.EnviarCargoInteresController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.enviar-cargo-interes-controller',

    requires: [
        'Pce.view.dialogo.FormaContenedor'
    ],

    onEnviar() {
        let tabla = this.lookup('enviarCargoInteresTabla');
        let selected = tabla.selModel.selected;
        if (selected.length == 0) {
            Ext.Msg.alert(
                'Advertencia!',
                `Debes seleccionar al menos una dependencia.`
            );
            return;
        }
        let cargoId = this.getView().getConfig().cargo.id;
        let dependencias = [];
        selected.items.forEach(dep => {
            dependencias.push(dep.id);
            
        });
        let dependenciasIds = dependencias.join(",");

        this.getView().close();
        
        Ext.getBody().mask('Enviando correos');
        Ext.Ajax.request({
            url: '/api/prestamos/cargo-interes/enviar-correos',
            method: 'GET',
            params: {cargoId, dependenciasIds},
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                if (res.exito) {
                    let mensaje = `Se enviaron los correos correctamente`;
                    Ext.Msg.alert(
                        'Operación completada',
                        mensaje
                    );
                } else {
                    Ext.Msg.alert(
                        'Ocurrió un error',
                        `${res.mensaje}`
                    );
                }
            },
            failure(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                Ext.Msg.alert(
                    'Ocurrió un error',
                    `${res.mensaje}`
                );
            }
        })
    }
});