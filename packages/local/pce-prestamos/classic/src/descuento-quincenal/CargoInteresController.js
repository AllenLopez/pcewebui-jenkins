
Ext.define('Pce.prestamos.descuento-quincenal.CargoInteresController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.cargo-interes-controller',

    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.prestamos.descuento-quincenal.CargoInteresTabla',
        'Pce.ingresos.transaccion.ResultadoDialogo',
        'Pce.prestamos.descuento-quincenal.enviar-cargo.EnviarCargoInteresDialogo'    ],

    parametros: {
        fechaAplicacion: undefined,
        tasa: undefined
    },


    onSeleccionQuincena(selector, valor) {
        this.parametros.fechaAplicacion = valor;
        this.validarParametros();
    },

    onChangeTasa(comp, valor) {
        this.parametros.tasa = valor;
        this.validarParametros();
    },

    onEliminarCargoInteres(tabla, i, j, item, e, cargo) {
        
        if (cargo.get('estatus') === 'PROCESADO') {
            Ext.Msg.alert('Alerta!', 'No es posible eliminar este cargo interés');
            return;
        }
        Ext.getBody().mask('Eliminado cargo de interés');
        let fechaCargo = Ext.util.Format.date(cargo.get("fechaAplicacion"), 'd/m/y');
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar este cargo de interés? 
            <i>${fechaCargo}</i>`,
            ans => {
                if(ans === 'yes'){
                    this.eliminarCargoInteres(cargo);
                }
            }
        );
    },

    onCalcularCargoInteres(component, event) {
        if (!this.validarParametros()) {
            return;
        }
        let fechaAplicacion = Ext.util.Format.date(this.parametros.fechaAplicacion, 'd/m/Y');
        Ext.Msg.confirm(
            'Calcular cargo de interés',
            `Se hara el calculo del cargo de interés del ${fechaAplicacion}. ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    this.calcularCargoInteres();
                }
            }
        )
    },

    validarParametros() {
        if (this.parametros.tasa == null || this.parametros.fechaAplicacion == null) {
            this.lookup('btnCalcular').setDisabled(true);
            return false;
        }
        
        this.lookup('btnCalcular').setDisabled(false);
        return true;
    },

    eliminarCargoInteres(cargo){
        cargo.erase({
            success(data, response){
                let res = JSON.parse(response.getResponse().responseText);
                Ext.Msg.alert(
                    'Operación completada',
                    `${res.mensaje}`
                );
                Ext.getBody().unmask();
            },
            failure(data,response) {
                let res = JSON.parse(response.getResponse().responseText);
                Ext.getBody().unmask();
                this.reloadTabla();
                Ext.Msg.alert(
                    'Ocurrió un error',
                    `${res.mensaje}`
                );
            }
        });
    },

    calcularCargoInteres() {
        let fechaAplicacion = Ext.util.Format.date(this.parametros.fechaAplicacion, 'm/d/Y');
        let jsonData = { fechaAplicacion, tasaInteres: this.parametros.tasa };
        Ext.getBody().mask('Calculando cargo de interés');
        Ext.Ajax.request({
            url: '/api/prestamos/cargo-interes',
            method: 'POST',
            jsonData,
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                if (res.exito) {
                    this.reloadTabla();
                    this.limpiarCampos();
                    this.parametros.tasa = "";
                    this.parametros.fechaAplicacion = "";
                    Ext.Msg.alert(
                        'Operación completada',
                        `${res.mensaje}`
                    );
                } else {
                    Ext.Msg.alert(
                        'Ocurrió un error',
                        `${res.mensaje}`
                    );
                }
            },
            failure(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                Ext.Msg.alert(
                    'Ocurrió un error',
                    `${res.mensaje}`
                );
            }
        })
    },

    reloadTabla(){
        let store = this.lookup('cargoInteresTabla').getStore();
        store.load();
        
    },

    limpiarCampos(){
        this.lookup('tasaInteres').reset();
        this.lookup('quincenaSelector').reset();
        this.lookup('btnCalcular').setDisabled(true);
    },

    onRevisionPorDependencia() {
        let seleccion = this.lookup('cargoInteresTabla').getSelection()[0];
        let fecha = Ext.util.Format.date(seleccion.get("fechaAplicacion"), 'd/m/y');
        let tasa = seleccion.get('tasaInteres');
        //let id = this.getTransaccion().getId();
        window.location = `/api/prestamos/cargo-interes/revision-dependencia?cargoId=${seleccion.id}&tasa=${tasa}&fecha=${fecha}`;
    },

    onRevisionDetallada() {
        let id = this.lookup('cargoInteresTabla').getSelection()[0].id;
        window.location = `/api/prestamos/cargo-interes/revision-detallada?cargoId=${id}`;
    },

    onAfectarCartera() {
        let seleccion = this.lookup('cargoInteresTabla').getSelection()[0];
        this.afectarCartera(seleccion);
    },

    afectarCartera(cargo){
        let jsonData = {idCargoInteres: cargo.id};
        Ext.getBody().mask('Calculando cargo de interés');
        Ext.Ajax.request({
            url: '/api/prestamos/cargo-interes/afectar-cartera',
            method: 'POST',
            jsonData,
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                this.reloadTabla();
                if (res.exito) {
                    let mensaje = `Se realizó la afectación de cartera correctamente y se generó la transacción: <b>${res.informacion.transaccion}</b>`;
                    Ext.Msg.alert(
                        'Operación completada',
                        mensaje
                    );
                } else {
                    Ext.Msg.alert(
                        'Ocurrió un error',
                        `${res.mensaje}`
                    );
                }
            },
            failure(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                this.reloadTabla();
                Ext.Msg.alert(
                    'Ocurrió un error',
                    `${res.mensaje}`
                );
            }
        })
    },

    onEnviar() {
        let cargo = this.lookup('cargoInteresTabla').getSelection()[0];
        Ext.create({
            xtype: 'enviar-cargo-interes-dialogo',
            autoShow: true,
            modal: true,
            cargo
        });
    },

    onMostrarResultados() {
        let seleccion = this.lookup('cargoInteresTabla').getSelection()[0];
        this.mostrarResultados(seleccion.get("transaccion"));
    },

    mostrarResultados(transaccionId) {
        Pce.ingresos.transaccion.Transaccion.load(transaccionId, {
            success: (transaccion) => {
                Ext.create({
                    xtype: 'transaccion-resultado-dialogo',
                    transaccion: transaccion,
                    autoShow: true,
                    modal: true
                });
            }
        });
    },
});