Ext.define('Pce.prestamos.catalogo.CatalogoContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'catalogo-contenedor',

    layout: 'fit',

    requires: [
        'Pce.prestamos.tasaInteres.TasaInteresTabla'
    ],

    mapaCatalogos: {
        'tasa-interes': 'tasa-interes-tabla'
    },

    catalogo: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.catalogo) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaCatalogos[this.catalogo]
            });
        }
    }
});