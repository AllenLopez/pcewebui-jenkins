Ext.define('Pce.prestamos.view.main.Navbar', {
    extend: 'Ext.container.Container',
    xtype: 'navbar',
    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Split',
        'Ext.button.Segmented',
        'Ext.menu.Menu'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'component',
        cls: 'site-bar',
        flex: 1
    }, {
        xtype: 'toolbar',
        items: {
            xtype: 'segmentedbutton',
            items: [{
                text: 'Inicio'
            }, {
                text: 'Solicitudes',
                menu: {
                    items: [{
                        text: 'Captura de solicitudes',
                        href: '#solicitud-captura'
                    }, {
                        text: 'Administración de solicitudes',
                        href: '#solicitudes'
                    }]
                }
            },  {
                text: 'Nomina',
                menu: {
                    items: [{
                        text: 'Carga de nómina',
                        href: '#carga-nomina'
                    }, 
                  ]
                }
            },{
                text: 'Emisión',
                menu: {
                    items: [{
                        text: 'Pólizas'
                    }, {
                        text: 'Conciliaciones'
                    }, {
                        text: 'Factura'
                    }, {
                        text: 'Cheques'
                    }]
                }
            }, {
                text: 'Mantenimiento créditos',
                menu: {
                    items: [{
                        text: 'layou blanco'
                    }]
                }
            }, {
                text: 'Consulta',
                menu: {
                    items: [{
                        text: 'Estado de cuenta afiliado'
                    }, {
                        text: 'Estado de cuenta dependencia'
                    }, {
                        text: 'solicitudes pendientes'
                    }, {
                        text: 'Pólizas canceladas'
                    }]
                }
            }, {
                text: 'Reportes',
                menu: {
                    items: [{
                        text: 'Pagaré'
                    }, {
                        text: 'Antigüedad de saldos'
                    }, {
                        text: 'Saldos incobrables al dpto Jurídico'
                    }]
                }
            }, {
                text: 'Catálogos',
                menu: {
                    items: [{
                        text: 'Tasa de Interés',
                        href: '#catalogo/tasa-interes'
                    }, {
                        text: 'Bancos',
                        href: '#catalogo/banco'
                    }, {
                        text: 'Cuentas Bancarias',
                        href: '#catalogo/cuenta-banco'
                    }]
                }
            },{
                text: 'Descuento Quincenal',
                menu:{
                    items: [{
                        text: 'Proceso cargo interés',
                        href: '#cargo-interes'
                    }]
                }
            }]
        }
    }]
});