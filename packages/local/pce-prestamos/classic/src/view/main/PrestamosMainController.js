Ext.define('Pce.prestamos.view.main.PrestamosMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.prestamos-main',

    requires: [
        'Pce.prestamos.tasaInteres.TasaInteresTabla',
        'Pce.prestamos.catalogo.CatalogoContenedor',

        'Pce.prestamos.solicitud.SolicitudAdminPanel',
        'Pce.prestamos.solicitud.SolicitudCaptura',
        'Pce.prestamos.descuento-quincenal.CargoInteresPanel',
        'Pce.Prestamos.nomina.CargaNominaPrestamos'
    ],

    routes: {
        'main': 'main',
        'catalogo/:catalogo': 'catalogo',
        'solicitud-captura' : 'solicitudCaptura',
        'solicitudes' : 'solicitudes',
        'cargo-interes' : 'procesoCargoInteres',
        'carga-nomina' : 'cargaNomina'

    },

    main() {

    },

    catalogo(catalogo) {
        this.view.renderPage({
            xtype: 'catalogo-contenedor',
            catalogo: catalogo
        });
    },

    solicitudes() {
        this.view.renderPage({
            xtype: 'prestamos-solicitud-admin'
        });
    },


    solicitudCaptura() {
        this.view.renderPage({
            xtype: 'prestamos-solicitud-captura'
        });
    },


    procesoCargoInteres() {
        this.view.renderPage({
            xtype: 'prestamos-cargo-interes'
        });
    },

    cargaNomina() {
     
        this.view.renderPage({
            xtype: 'carga-nomina-prestamos',
          
        });
    },

});