Ext.define('Pce.prestamos.view.main.Main', {
    extend: 'Pce.view.main.Main',
    xtype: 'prestamos-main',

    requires: [
        'Pce.prestamos.view.main.PrestamosMainController',

        // Barra de navegación para el módulo de prestamos
        'Pce.prestamos.view.main.Navbar'
    ],

    controller: 'prestamos-main',

    dockedItems: [{
        // Barra de navegación
        xtype: 'navbar',
        dock: 'top'
    }]
});