Ext.define('Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionFiltrosToolbarCaja', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'cancelacion-transaccion-filtros-toolbar-caja',

    viewModel: {
        data: {
            idTransaccion: null,
            fechaInicio: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            fechaFinal: new Date(new Date().getFullYear(), new Date().getMonth()+1, 0)
        }
    },

    items: [{
        xtype: 'datefield',
        name: 'desde',
        fieldLabel: 'A partir de',
        labelWidth: 80,
        flex: 1,
        bind:{
            value: '{fechaInicio}'
        },
        listeners: {
            change: 'onSeleccionDesde'
        }
    }, {
        xtype: 'datefield',
        name: 'hasta',
        fieldLabel: 'Hasta',
        labelWidth: 80,
        flex: 1,
        bind:{
            value: '{fechaFinal}'
        },
        listeners: {
            change: 'onSeleccionHasta'
        }
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Transacción',
        valueField: 'transaccion',
        name: 'transaccion',
        labelWidth: 100,
        bind: {
            value: '{idTransaccion}'
        },
        flex: 1
    }, {
        text: 'Buscar',
        flex: 0.5,
        bind: {
            disabled: '{!idTransaccion}'
        },
        handler: 'onBuscar'
    }]
});