Ext.define('Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionPanelCaja', {
    extend: 'Ext.panel.Panel',
    xtype: 'cancelacion-transaccion-panel-caja',

    requires: [
        'Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionTablaCaja',
        'Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionPanelControllerCaja',
        'Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionToolbarCaja',
        'Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionFiltrosToolbarCaja'
    ],

    title: 'Cancelación transacción',
    layout: 'fit',

    controller: 'cancelacion-transaccion-panel-caja',
    
    viewModel: {},

    dockedItems: [{
        xtype: 'cancelacion-transaccion-filtros-toolbar-caja',
        dock: 'top',
        reference: 'toolbarFiltros',
    }, {
        xtype: 'cancelacion-transaccion-toolbar-caja',
        dock: 'top',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    },],

    items: [{
        xtype: 'cancelacion-transaccion-tabla-caja',
        reference: 'transaccionTabla',
        store: {
            type: 'transacciones'
        }
    }]
});