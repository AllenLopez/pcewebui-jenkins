Ext.define('Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionToolbarCaja', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'cancelacion-transaccion-toolbar-caja',

    items: [{
        text: 'Cancelar transacción 882',
        handler: 'onCancelarTransaccion',
        tooltip:'Realizar la cancelación de la transacción',
        bind: {
            disabled: '{transaccionTabla.selection.cancelado == "SI" || transaccionTabla.selection.origenClave == "CANCELAR"}',
            text: 'Cancelar transaccion {transaccionTabla.selection.id}'
        }
    }, {
        text: 'Reporte transacción',
        handler: 'onMostrarReporteTransaccion',
        iconCls: 'x-fa fa-file-pdf-o',
        tooltip: 'Genera reporte de la transacción seleccionada'
,        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    }, {
        text: 'Mostrar errores de la transacción',
        handler: 'onMostrarResultados',
        tooltip:'Mostrar errores generados en la transacción',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    }]
});