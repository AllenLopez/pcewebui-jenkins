Ext.define('Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionPanelControllerCaja', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.cancelacion-transaccion-panel-caja',

    requires: [
        // 'Pce.ingresos.transaccion.TransaccionServicio',
        // 'Pce.ingresos.transaccion.ResultadoDialogo'
    ],

    // /**
    //  * @property {Pce.ingresos.transaccion.TransaccionServicio}
    //  */
    // servicio: undefined,
    params: {
        desde: null,
        hasta: null,
        idTransaccion: null,
        sistema: "CAJA"
    },

    init() {

    },

    onMostrarResultados() {
        this.mostrarResultados();
    },

    mostrarResultados() {
        Ext.create({
            xtype: 'transaccion-resultado-dialogo',
            transaccion: this.getTransaccion(),
            autoShow: true,
            modal: true
        });
    },

    onSeleccionDesde(comp, valor){
        if( !comp.validate() || !valor ){
            this.params.desde = null;
            return;
        }
        this.params.desde = valor;
        this.validarFechas();
    },

    onSeleccionHasta(comp, valor){
        if( !comp.validate() || !valor ){
            this.params.hasta = null;
            return;
        }
        this.params.hasta = valor;
        this.validarFechas();
    },

    onBuscar(){
        let filtros = this.lookup('toolbarFiltros').getViewModel().getData();

        this.lookup('transaccionTabla').getStore().porId(filtros.idTransaccion);

    },

    onCancelarTransaccion(){

        let transaccion = this.lookup('transaccionTabla').getSelection()[0];
        let origen = transaccion.getData().origenClave;
        // if(origen != 'NOMINA' && origen != 'NOMINA_PR' && origen != 'NOM_DIN_JPV' && origen != 'CAPTURA'){
        //     Ext.Msg.alert('Advertencia!', `No se puede cancelar la transacción: <b>${transaccion.id}</b>`);
        //     return;
        // }

        Ext.Msg.confirm(
            'Cancelar transacción',
            `Se cancelara la siguiente transacción <b>${transaccion.id}</b>.
            ¿Desea proceder?`,
            ans => {
                if (ans === 'yes') {
                    this.cancelarTransaccion(transaccion);
                }
            }
        );
    },

    cancelarTransaccion(transaccion) {
        Ext.getBody().mask('Cancelando transacción...');
        Ext.Ajax.request({
            url: '/api/ingresos/transaccion/cancelar-transaccion-caja/' + transaccion.id,
            method: 'GET',
            scope: this,
            callback(op, success, response) {
                Ext.getBody().unmask();
                let respuesta = JSON.parse(response.responseText);
                if (respuesta.excepcion) {
                    Ext.Msg.alert(
                        'Advertencia!', 
                        respuesta.mensaje, 
                        () => this.onMostrarResultados());

                    return;
                }

                this.lookup('transaccionTabla').getStore().reload();
                let mensaje = `La cancelación de la transacción <b>${transaccion.id}</b>, se completo exitosamente y genero la transacción <b>${respuesta.informacion}</b>`;

                Ext.MessageBox.show({
                    title:'Operación completada',
                    msg: mensaje,
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.INFO,
                    fn:(btn) => {
                        
                    }
                });



            }
        });
    },

    onMostrarResultados() {
        this.mostrarResultados();
    },

    mostrarResultados() {
        Ext.create({
            xtype: 'transaccion-resultado-dialogo',
            transaccion: this.getTransaccion(),
            autoShow: true,
            modal: true
        });
    },

    onMostrarReporteRetenciones() {
        let id = this.getTransaccion().getId();
        window.location = `/api/ingresos/transaccion/reporte-retenciones?transaccionId=${id}`;
    },

    onMostrarReporteTransaccion(id, cancela) {
        let transaccion = this.getTransaccion();
        this.mostrarReporteTransaccion(transaccion.id, transaccion.get('idTransaccionOrigen'));
    },

    mostrarReporteTransaccion(transaccionId) {
        window.location = `/api/ingresos/transaccion/reporte-detalle-transaccion?transaccionId=${transaccionId}`;
    },

    getTransaccion() {
        return this.lookup('transaccionTabla').getSelection()[0];
    },

    formatDate(date, format){
        return  Ext.util.Format.date(date, format);
    },

    validarFechas(){
        if( !this.params.hasta || !this.params.desde){
            return;
        }

        if(this.params.desde > this.params.hasta){
            Ext.Msg.alert('Advertencia!', 'No es un rango de fechas valido');
            return;
        }
        this.params.desde = this.formatDate(this.params.desde, 'm/d/Y');
        this.params.hasta = this.formatDate(this.params.hasta, 'm/d/Y');
        this.lookup('transaccionTabla').getStore().porFechas(this.params);
    }
});