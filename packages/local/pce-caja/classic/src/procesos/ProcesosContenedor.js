Ext.define('Pce.caja.procesos.ProcesosContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'procesos-contenedor',

    layout: 'fit',

    requires: [
        'Pce.caja.procesos.cancelacionTransaccion.CancelacionTransaccionPanelCaja',
        'Pce.caja.procesos.certificados.ConsultaCertificadosPanel'
    ],

    mapaProcesos: {
        
        'cancelar-transaccion': 'cancelacion-transaccion-panel-caja',
        'consulta-certificados': 'consulta-certificados-panel'
    },

    proceso: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.proceso) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaProcesos[this.proceso]
            });
        }
    }
});