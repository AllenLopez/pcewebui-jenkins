Ext.define('Pce.caja.procesos.certificados.ConsultaCertificadosPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'consulta-certificados-panel',

    requires: [
        'Pce.caja.procesos.certificados.ConsultaCertificadosTabla',
        'Pce.caja.procesos.certificados.ConsultaCertificadosPanelController',
        'Pce.caja.procesos.certificados.ConsultaCertificadosToolbar',
        'Pce.caja.procesos.certificados.ConsultaCertificadosFiltrosToolbar',
        'Pce.caja.procesos.certificados.ConsultaCertificadosForma'
    ],

    title: 'Consulta / Reimpresión de certificados',
    layout: 'fit',

    controller: 'consulta-certificados-panel',
    
    viewModel: {},

    dockedItems: [{
        xtype: 'consulta-certificados-filtros-toolbar',
        dock: 'top',
        reference: 'toolbarFiltros',
    }, 
    {
        xtype: 'consulta-certificados-toolbar',
        dock: 'top'
    },
],

    items: [
        {
        xtype: 'consulta-certificados-tabla',
        reference: 'certificadostabla',
        store: {
            type: 'certificado-ingresos'
        }
    }
],
afterRender() {
    this.callParent();
    this.inicializarFecha();
},

inicializarFecha() {
    let fecha = new Date();
    var firstDay = new Date(fecha.getFullYear(), fecha.getMonth(), 1);
    var lastDay = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
    let campo = Ext.ComponentQuery.query('[name=desde]')[0];
    let hasta = Ext.ComponentQuery.query('[name=hasta]')[0];
    campo.setValue(firstDay);
    
    hasta.setValue(lastDay);
},
});