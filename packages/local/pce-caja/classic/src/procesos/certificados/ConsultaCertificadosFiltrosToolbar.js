Ext.define('Pce.caja.procesos.certificados.ConsultaCertificadosFiltrosToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'consulta-certificados-filtros-toolbar',

    viewModel: {
        data: {
            idCertificado: null
        }
    },

    
    items: [{
        xtype: 'datefield',
        name: 'desde',
        fieldLabel: 'A partir de',
        labelWidth: 80,
        flex: 1.5,
        listeners: {
            change: 'onSeleccionDesde'
        }
    }, {
        xtype: 'datefield',
        name: 'hasta',
        fieldLabel: 'Hasta',
        labelWidth: 80,
        flex: 1.5,
        listeners: {
            change: 'onSeleccionHasta'
        }
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Certificado',
        valueField: 'certificado',
        name: 'certificado',
        reference: 'certificadoFolio',
        labelWidth: 100,
        bind: {
            value: '{idCertificado}'
        },
        flex: 1
    }, {
        text: 'Buscar',
        flex: 0.5,
        // bind: {
        //     disabled: '{!idCertificado}'
        // },
        handler: 'onBuscar'
    }, ]
});