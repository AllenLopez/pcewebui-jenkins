Ext.define('Pce.caja.procesos.certificados.ConsultaCertificadosToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'consulta-certificados-toolbar',

    viewModel: {
        data: {
            idCertificado: null
        }
    },

    items: [{
        xtype: 'asegurado-selector',
        name: 'afiliado',
        labelWidth: 80,
        flex: 1,
        listeners: {
            change: 'onSeleccionAfiliado'
        }
    }, {
        xtype: 'dependencia-selector',
        name: 'dependencia',
        labelWidth: 80,
        flex: 1,
        listeners: {
            change: 'onSeleccionDependencia'
        }
    }]
});