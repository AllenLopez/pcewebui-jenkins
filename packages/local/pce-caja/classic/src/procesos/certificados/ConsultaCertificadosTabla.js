Ext.define('Pce.caja.procesos.certificados.ConsultaCertificadosTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'consulta-certificados-tabla',

    columns: [{
        text: 'Certificado',
        dataIndex: 'certificado',
        tooltip:'Folio de certificado',
        flex: 1
    }, {
        text: 'Afiliado',
        dataIndex: 'afiliado',
        tooltip:'Número de afiliado',
        flex: 1
    }, {
        text: 'Dependencia',
        dataIndex: 'dependencia',
        tooltip:'Número de dependencia',
        flex: 1
    }, {
        text: 'Nombre',
        dataIndex: 'nombre',
        tooltip:'Nombre de afiliado/dependecia',
        flex: 2
    }, {
        text: 'Ban. recepción',
        dataIndex: 'bancoReceptor',
        tooltip:'Banco receptor',
        flex: 2
    }, {
        text: 'Aviso',
        dataIndex: 'avisoCargo',
        tooltip:'Número de aviso de cargo',
        flex: 1
    }, {
        text: 'Transacción',
        dataIndex: 'transaccion',
        tooltip:'Transacción del certificado',
        flex: 1
    }, {
        text: 'Importe',
        dataIndex: 'importe',
        tooltip:'Importe del certificado',
        flex: 1,
        renderer(v) {
            return `${Ext.util.Format.usMoney(v)}`;
        },
    },{
        text: 'Fecha',
        dataIndex: 'fechaPago',
        tooltip:'Fecha en la que se realizo el movimiento',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
        flex: 1
    }, {
        text: 'Creado por',
        dataIndex: 'usuario',
        tooltip:'Usuario que creo el certificado',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        flex: 1,
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'Ver detalles',
            handler: 'onVerDetalles'
        },{
            iconCls: 'x-fa fa-print',
            tooltip: 'Imprimir certificado',
            handler: 'onImprimir'
        }]
    }, ]
});