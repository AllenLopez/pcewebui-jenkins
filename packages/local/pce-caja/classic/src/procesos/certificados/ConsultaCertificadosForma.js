Ext.define('Pce.caja.procesos.certificados.ConsultaCertificadosForma',{
    extend: 'Ext.form.Panel',
    xtype: 'consulta-certificados-forma',

    requieres: [
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox',
        'Ext.layout.container.Form',
    ],

    layout: {
        type: 'table',
        columns: 10
    },
    padding: 10,

    defaults: {
        width: '100%'
    },

    viewModel:{
    },

    items: [{
        xtype: 'numberfield',
        fieldLabel: 'Certificado',
        name: 'certificado',
        colspan: 2,
        readOnly: true,
        cls: 'read-only',
    },{
        xtype: 'numberfield',
        fieldLabel: 'Afiliado',
        name: 'afiliado',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'numberfield',
        fieldLabel: 'Dependencia',
        name: 'dependencia',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Nombre',
        name: 'nombre',
        colspan: 4,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'datefield',
        fieldLabel: 'Fecha pago',
        name: 'fechaPago',
        colspan: 2,
        format: 'd/m/Y',
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Forma de pago',
        name: 'formaPago',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Banco emisor',
        name: 'bancoEmisor',
        colspan: 6,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Cheque / Transferencia',
        name: 'referencia',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Banco receptor',
        name: 'bancoReceptor',
        colspan: 6,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Cuenta bancaria',
        name: 'cuentaBancaria',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Importe',
        name: 'importeString',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Tipo pago',
        name: 'tipoPagoDescr',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Estatus',
        name: 'estatusDescripcion',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Usuario',
        name: 'usuario',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Transacción',
        name: 'transaccion',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Aviso cargo',
        name: 'avisoCargo',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Tipo movimiento',
        name: 'tipoMovimiento',
        colspan: 8,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'textfield',
        fieldLabel: 'Núm. licencia',
        name: 'licencia',
        colspan: 2,
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'datefield',
        fieldLabel: 'Fecha inicio',
        name: 'fechaInicio',
        colspan: 2,
        format: 'd/m/Y',
        readOnly: true,
        cls: 'read-only'
    },{
        xtype: 'datefield',
        fieldLabel: 'Fecha termino',
        name: 'fechaTermino',
        colspan: 2,
        format: 'd/m/Y',
        readOnly: true,
        cls: 'read-only'
    },]
});