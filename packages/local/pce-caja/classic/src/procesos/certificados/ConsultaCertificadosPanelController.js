Ext.define('Pce.caja.procesos.certificados.ConsultaCertificadosPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-certificados-panel',

    requires: [
    ],

    parametros: {
        fechaInicio: undefined,
        fechaFin: undefined,
        afiliado: undefined,
        dependencia: undefined,
        certificadoFolio: undefined
    },


    onSeleccionDesde(comp, valor) {
        this.parametros.fechaInicio = Ext.util.Format.date(valor, 'd/m/Y');
    },
    
    onSeleccionHasta(comp, valor) {
        this.parametros.fechaFin = Ext.util.Format.date(valor, 'd/m/Y');
    },
    
    onSeleccionAfiliado(comp, valor) {
        this.parametros.afiliado = valor;
    },
    
    onSeleccionDependencia(comp, valor) {
        this.parametros.dependencia = valor;

    },
    
    onBuscar() {
        let certificado =  this.lookup('certificadoFolio');
        this.parametros.certificadoFolio = certificado.getValue();
        this.buscarCertificados(this.parametros);
    },

    onImprimir(tabla, i, j, item, e, certificado) {
        this.imprimir(certificado.get('certificado'), certificado.get('reporte'));
    },

    imprimir(certificadoId, reporte){
        window.location = `/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=${reporte}`;
    },

    onVerDetalles(tabla, i, j, item, e, certificado){
        this.mostrarDialogoTabla(certificado);
    },

    mostrarDialogoTabla(certificado) {
        Ext.create({
            xtype: 'window',
            title: `Detalle del certificado - ${certificado.get('certificado')}`,
            autoShow: true,
            modal: true,
            height: '90vh',
            width: '100vw',
            layout: 'fit',
            items: {
                xtype: 'consulta-certificados-forma',
                data: certificado,
                listeners: {
                    afterrender: (comp)=>{
                        comp.getForm().setValues(certificado.data);
                    }
                }
            },
        });
    },

    buscarCertificados(params){
        const tabla =  this.lookup('certificadostabla').getStore();

        Ext.getBody().mask('Buscando certificados...');
        // Ext.Ajax.request({
        //     url: '/api/ingresos/certificado/buscar',
        //     method: 'GET',
        //     params,
        //     callback(opt, success, response) {
        //         Ext.getBody().unmask();
        //         const resultado = Ext.decode(response.responseText);
        //         tabla.getStore().setData(resultado);
        //     }
        // });
        tabla.setProxy({
            type: 'rest',
            url: '/api/ingresos/certificado/buscar',
            writer: {
                type: 'json',
                writeAllFields: true
            }
        }),
        tabla.load({
            params: params
        });
        Ext.getBody().unmask();

    }
});