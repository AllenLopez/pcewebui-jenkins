Ext.define('Pce.caja.prestamos.ConceptoPrestamosTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'concepto-prestamos-tabla',

    requires: [
        'Ext.plugin.Abstract',
        'Ext.grid.plugin.CellEditing'
    ],

    emptyText: 'Sin datos',

    features: [{
        ftype: 'summary'
    }],

    viewConfig: {
        markDirty: false
    },

    columns: [{
        text: 'Concepto',
        dataIndex: 'descripcionConcepto',
        flex: 3
    }, {
        text: 'Pagaré',
        dataIndex: 'id',
        flex: 2
    },{
        text: 'Descuento Quincenal',
        dataIndex: 'descuento',
        flex: 2,
        renderer(v) {
            return `${Ext.util.Format.usMoney(v)}`;
        },
    },{
        text: 'Saldo',
        dataIndex: 'saldo',
        renderer: Ext.util.Format.usMoney,
        flex: 2
    },{
        text: 'Importe a pagar',
        dataIndex: 'porPagar',
        flex: 2,
        renderer(v) {
            return `<b style="color: #2862a0">
            <i class="fa fa-caret-right"></i>
            ${Ext.util.Format.usMoney(v)}
            </b>`;
        },
        summaryType: 'sum',
        summaryRenderer: 'onSummary',
        editor: {
            xtype: 'numberfield',
            allowBlank: false,
            hideTrigger: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false,
            minValue: 0,
            validator: 'validaImporte'
        },
    }],

    plugins: [{
        ptype: 'cellediting',
        clicksToEdit: 1
    }],
    
    // listeners: [{
    //     beforeedit: 'onBeforeEdit',
    //     validateedit: 'onValidateEdit'
    // }]
});