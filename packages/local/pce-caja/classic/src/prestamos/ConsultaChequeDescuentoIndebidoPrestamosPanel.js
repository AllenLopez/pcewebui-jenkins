Ext.define('Pce.caja.prestamos.ConsultaChequeDescuentoIndebidoPrestamosPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'consulta-cheque-descindeb-prestamos-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.caja.cheques.emisionCheque.EmisionChequeTabla',
        'Pce.caja.cheques.emisionCheque.EmisionChequeController',
        'Ext.grid.plugin.CellEditing'
    ],

    controller: 'consulta-cheque-descindeb-prestamos',

    viewModel: {
        data: {
            cheque: null,
            banco: null,
            cuentaBanco: null,
            botonInvalido: false
        }
    },

    title: 'Consulta cheques descuento indebido préstamos',
    layout: 'fit',
    emtpyText: 'No hay datos para mostar',
    items: [{
        xtype: 'emision-cheque-descindeb-tabla',
        reference: 'consultaDescindebTabla',
        // store: {
        //     type: 'EmisionPagare',
        //     autoLoad: true
        // },
        columns: [{
            text: 'No.Registro',
            dataIndex: 'id',
            tooltip:'Número de registro',
            flex: 1
        },{
            text: 'Afiliación',
            dataIndex: 'numAfil',
            tooltip:'Numero de Afiliado',
            flex: 1
        }, {
            text: 'Beneficiario',
            dataIndex: 'beneficiario',
            tooltip:'Nombre que corresponde la emisión del cheque',
            flex: 2
        },{
            text: 'Importe',
            dataIndex: 'importe',
            tooltip:'Muestra el importe que se devolverá al asegurado.',
            renderer(v){
                return v === 0 ? '' : Ext.util.Format.usMoney(v);
            },
            flex: 1
        },{
            text: 'Banco',
            dataIndex: 'banco',
            tooltip:'Banco del cheque',
            flex: 1
        },{
            text: 'Número Cheque',
            dataIndex: 'numCheque',
            tooltip:'Numero del cheque',
            flex: 1
        },{
            text: 'Fecha Movimiento',
            dataIndex: 'fechaMovimiento',
            tooltip:'Fecha en la que se expidió el cheque',
            flex: 1,
            renderer(v){
                return Ext.util.Format.date(v, 'd/m/Y');
            },
        },{
            text: 'Fecha Impresión',
            dataIndex: 'fechaImpresion',
            tooltip:'Fecha en la que se imprimió el cheque',
            flex: 1,
            renderer(v){
                return Ext.util.Format.date(v, 'd/m/Y');
            },
        },{
            text: 'Usuario',
            dataIndex: 'usuario',
            tooltip:'usuario quien imprimió el cheque',
            flex: 1
        },{
            xtype: 'actioncolumn',
            flex: 1,
            width: 50,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-print',
                tooltip: 'Imprimir cheque',
                handler: 'onImprimirCheque'
            }]
        }],
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Fecha desde',
                name: 'fechaInicio',
                bind:{
                    maxValue: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionFechaInicio'
                }
            },{
                xtype: 'datefield',
                fieldLabel: 'Fecha Hasta',
                reference: 'fechaFinal',
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicio}',
                },
                listeners: {
                    change: 'onSeleccionFechaFinal'
                }
            }, {
                xtype: 'textfield',
                fieldLabel: 'Beneficiario',
                reference: 'beneficiario',
                name: 'beneficiario',
                width: '400px',
                listeners: {
                    change: 'onChangeBeneficiario'
                }
            },{
                xtype: 'textfield',
                fieldLabel: 'Num. Cheque',
                reference: 'NumCheque',
                name: 'Num. Cheque',
                listeners: {
                    change: 'onChangeNumCheque'
                }
            }, {
                text: 'Buscar',
                reference: 'botonBuscar',
                handler: 'onBuscarCheques',
            }]
        }],
        bind: {
            selection: '{cheque}'
        },
    }]
});
