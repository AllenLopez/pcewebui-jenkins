/**
 * @author Alan López
 */
Ext.define('Pce.caja.prestamos.PagoPrestamoAfiliadoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'pago-prestamo-afiliado-panel',

    requires: [
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.caja.banco.BancoSelector'
    ],

    controller: 'pago-prestamos-afiliado',
    viewModel: {
        data: {
            asegurado: null,
            botonInvalido: false,
            tablavalida: false
        },
    },

    title: 'Pago prestamos por afiliado',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    tbar: [{
        xtype: 'asegurado-selector',
        reference: 'aseguradoSelector',
        labelAlign: 'left',
        labelSeparator: ':',
        width: '50%',
        bind: {
            selection: '{asegurado}'
        }
    },{
        xtype: 'dependencia-selector',
        reference: 'dependenciaSelector',
        name: 'dependencia',
        allowBlank: false,
        margin: 5,
        flex: 3,
        labelWidth: 90,
        listeners: {
            change: 'onSeleccionDependencia'
        },
        bind: {
            selection: '{dependencia}',
            disabled: '{!asegurado}'
        }
    }],
    items: [{
        xtype: 'form',
        padding: 10,
        layout: {
            type: 'table',
            columns: 3
        },
        defaults: {
            width: '100%'
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Dirección',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.direccion}'
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'RFC',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.rfc}'
            }
        },{
            fieldlabel: 'Forma de pago',
            xtype: 'forma-pago-selector',
            name: 'formaPago',
            reference: 'formaPago',
            allowBlank: false,
        },{
            xtype: 'banco-selector',
            allowBlank: false,
            name: 'bancoEmisor',
            reference: 'bancoEmisor',
            fieldLabel: 'Banco emisor',
            valueField: 'bancoNombre',
            displayField: 'bancoNombre',
        },{
            xtype: 'numberfield',
            fieldLabel: 'Num. referencia',
            name: 'referencia',
            reference: 'referencia',
            allowBlank: false,
        },{
            xtype: 'banco-selector',
            allowBlank: false,
            name: 'bancoReceptor',
            reference: 'bancoReceptor',
            fieldLabel: 'Banco receptor',
            listeners: {
                change: 'onChangeBanco'
            }
        },{
            fieldlabel: 'Cuenta banco',
            xtype: 'cuenta-banco-selector',
            allowBlank: false,
            reference: 'cuenta',
            name: 'cuentaBanco',
            width: '440px',
            listeners: {
                change: 'onValidaBoton'
            }
        }]
    }, {
        items: [{
            xtype: 'concepto-prestamos-tabla',
            reference: 'tablaConceptoPrestamos',
            store: {
                type: 'dependencia-estados-de-cuenta',
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/api/prestamos/pagare/concepto-numafil-dep'
                }
            },
            bind: {
                disabled: '{!tablavalida}'
            }
        }]
    }],
    buttons: [{
        text: 'Procesar Movimiento',
        reference: 'botonProcesar',
        handler: 'onGenerarPago',
        bind: {
            disabled: '{!botonInvalido}'
        }
    }]
});