Ext.define('Pce.caja.prestamos.EmisionChequeDescIndebPrestamosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emision-cheque-descindeb-prestamos',
    
    fechaInicio: undefined,
    fechaFinal: undefined,
    banco: undefined,


    init: function() {
        this.resetStore();
    },

    resetStore(){
        let store = this.lookup('EmisionChequeDescIndebTabla').getStore()
        store.removeAll();
        
    },
    onSeleccionaFechaInicial(control, seleccion) {
        this.fechaInicio = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    onSeleccionaFechaFinal(control, seleccion) {
        if(!control.isValid()){
            return;
        }
        this.fechaFinal = Ext.util.Format.date(seleccion, 'd/m/Y');
        
    },

    onBuscarCheques(){
        this.cargaTablaCheques(this.fechaInicio, this.fechaFinal);
    },

    cargaTablaCheques(fechaInicio, fechaFinal) {
        let store = this.lookup('EmisionChequeDescIndebTabla').getStore();
        this.lookup('EmisionChequeDescIndebTabla').getStore().load({
            params: {
                fechaInicio,
                fechaFinal
            } 
        });
    },

    onImprimirCheque() {
        let tabla = this.lookup('EmisionChequeDescIndebTabla');
        let cheque = tabla.selection

        this.confirmarProceso().then(ans => {
            if (ans === 'yes') {
                this.ImprimirCheque(cheque);
            }
        }); 
        
    },

    confirmarProceso() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Impresión de Cheque',
                `¿Desea Imprimir el cheque seleccionado?`,
                ans => resolve(ans)
            );
        });
    },

    ImprimirCheque(cheque) {
        let me = this;
        Ext.getBody().mask('Imprimiendo cheque...');
        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/ingresos/asegurado/descuento-indebido/actualiza-cheque-prestamo',
            params: {chequeId: cheque.data.id},
                success(resp) {
                    let respuesta = JSON.parse(resp.responseText);
                    if(respuesta.exito) {
                        me.imprimirCheque(cheque);
                        mensaje = `Se imprimió el cheque correctamente`;
                        Ext.Msg.alert(
                            'Operación exitosa',
                            mensaje,
                            () => {
                                me.lookup('fechaInicio').reset();
                                me.lookup('fechaFinal').reset();
                                me.resetStore();
                            }
                        );
                    } else {
                        Ext.Msg.alert('Error',
                            `Ocurrió un error al imprimir los cheques  ${respuestaSolicitud.mensaje}`);
                    }
                    Ext.getBody().unmask();
                },
                failure(resp) {
                    let error = JSON.parse(resp.responseText);
                    Ext.Msg.alert('Error',
                        `Ocurrió un error al imprimir los cheques ${error.errorMessage}`);

                    Ext.getBody().unmask();
                }
            });
        },
        imprimirCheque(cheque){
            let chequeId = cheque.data.id
            window.open(`/api/ingresos/asegurado/descuento-indebido/imprimir-cheque?pidemision=${chequeId}`);
        }
});
//PIDSOLCHEQUE   -- cambiar status a IM