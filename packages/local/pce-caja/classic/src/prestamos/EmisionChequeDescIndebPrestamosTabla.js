Ext.define('Pce.caja.prestamos.EmisionChequeDescIndebPrestamosTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'emision-cheque-descindeb-tabla',

    columns: [{
        text: 'No.Registro',
        dataIndex: 'id',
        tooltip:'Descuento indebido',
    },{
        text: 'Afiliación',
        dataIndex: 'numAfil',
        tooltip:'Numero de Afiliado',
        flex: 1
    }, {
        text: 'Beneficiario',
        dataIndex: 'beneficiario',
        tooltip:'Nombre que corresponde la emisión del cheque',
        flex: 2
    },{
        text: 'Importe',
        dataIndex: 'importe',
        tooltip:'Muestra el importe que se devolverá al asegurado.',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        flex: 1
    },{
        text: 'Banco',
        dataIndex: 'banco',
        tooltip:'Banco',
        flex: 1
    },{
        text: 'Número Cuenta',
        dataIndex: 'numeroCuenta',
        tooltip:'Numero de cuenta banco',
        flex: 1
    },{
        text: 'Numero Cheque',
        dataIndex: 'numCheque',
        tooltip:'Numero del cheque',
        flex: 1
    },{
        text: 'Fecha Movimiento',
        dataIndex: 'fechaMovimiento',
        tooltip:'Fecha en la que se realizó el movimiento',
        flex: 1,
        renderer(v){
            return Ext.util.Format.date(v, 'd/m/Y');
        },
    },{
        text: 'Etapa',
        dataIndex: 'etapa',
        tooltip:'Etapa en la que se encuentra el cheque',
        flex: 1,
        renderer(v) {
            switch (v) {
                case 'TR': return 'EN TRÁMITE';
                case 'AU': return 'AUTORIZADA';
                case 'CA': return 'CANCELADA';
                case 'CR': return 'CERRADA';
                case 'CT': return 'EN CONTROL';
                case null: return '';
                default: return 'OTRA';
            }
        },
    },]
});