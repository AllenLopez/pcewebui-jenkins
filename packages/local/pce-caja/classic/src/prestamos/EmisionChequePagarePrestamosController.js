Ext.define('Pce.caja.prestamos.EmisionChequePagarePrestamosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emision-cheque-pagare-prestamos',
    
    fechaExpedicion: undefined,
    banco: undefined,
    cuenta: undefined,


    init: function() {
    },

    resetStore(){
        let store = this.lookup('chequesPagareTabla').getStore()
        store.removeAll();
    },
    onSeleccionaFechaExpedicion(control, seleccion) {
        this.fechaExpedicion = Ext.util.Format.date(seleccion, 'd/m/Y');
        let bancoValue = this.lookup('banco');
        let numCuenta = this.lookup('cuenta');
        let boton = this.lookup('boton');
        this.cargaTablaCheques(this.fechaExpedicion);
        this.cargaBanco(this.fechaExpedicion, bancoValue,numCuenta, boton);
        boton.enable();
        
    },
    cargaTablaCheques(fechaExpedicion, cuentaBanco) {
        this.lookup('chequesPagareTabla').getStore().load({
            params: {
                fechaExpedicion
            } 
        });
    },

    cargaBanco(fechaExpedicion, bancoValue,numCuenta,boton){
        let store = this.lookup('chequesPagareTabla').getStore().load({
            params: {
                fechaExpedicion: fechaExpedicion
            },
            
                callback: function () {
                    success: {
                        store.data.items.forEach(el => {
                        if(el){
                            bancoValue.setValue(el.data.banco);
                            numCuenta.setValue(el.data.numeroCuenta);  
                        }
                    });
                    console.log(store.data.items.length);
                    if(store.data.items.length == 0){
                        bancoValue.setValue(' ');
                        numCuenta.setValue(' '); 
                        boton.disable();
                    }
                }

            } 
        });
        
        
    },
    confirmarProceso() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Impresión de Cheques',
                `¿Desea Imprimir la lista de cheques que se muestran en pantalla?`,
                ans => resolve(ans)
            );
        });
    },
    onImprimirCheques() {
        let storePagare = this.lookup('chequesPagareTabla').getStore().getData().items;
        let fecha = this.lookup('fechaExpedicion').value;
        let fechaExpedicion = Ext.util.Format.date(fecha, "d/m/Y");

        storePagare.forEach(el => {
        cuentaBanco = el.data.cuentaBancariaId;
        });

        let datos = {
            cuentaBancoId: cuentaBanco,
            banco: this.lookup('banco').value,
        }

        this.confirmarProceso().then(ans => {
            if (ans === 'yes') {
                this.ImprimirCheques(fechaExpedicion, datos);
            }
        }); 
        
    },

    ImprimirCheques(fechaExpedicion, datos) {
        let me = this;
        Ext.getBody().mask('Imprimiendo lista de cheques...');
        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/prestamos/pagare/actualiza-pagare',
            params: {fechaExpedicion: fechaExpedicion},
                success(resp) {
                    let respuesta = JSON.parse(resp.responseText);
                    if(respuesta.exito) {
                        me.imprimirCheque(fechaExpedicion, datos.cuentaBancoId);
                        mensaje = `Se imprimeron los cheques correctamente`;
                        Ext.Msg.alert(
                            'Operación exitosa',
                            mensaje,
                            () => {
                                
                                me.lookup('fechaExpedicion').reset();
                                me.lookup('cuenta').reset();
                                me.lookup('banco').reset();
                                me.lookup('chequesPagareTabla').getStore().removeAll();
                            }
                        );
                    } else {
                        Ext.Msg.alert('Error',
                            `Ocurrió un error al imprimir los cheques  ${respuestaSolicitud.mensaje}`);
                    }
                    Ext.getBody().unmask();
                },
                failure(resp) {
                    let error = JSON.parse(resp.responseText);
                    Ext.Msg.alert('Error',
                        `Ocurrió un error al imprimir los cheques ${error.errorMessage}`);

                    Ext.getBody().unmask();
                }
            });
        },
        imprimirCheque(fechaExpedicion, cuentaBancoId){
            window.open(`/api/prestamos/pagare/imprimir-cheque?pfecha=${fechaExpedicion}&pctaban=${cuentaBancoId}`);
        }
});

//PIDSOLCHEQUE   -- cambiar status a IM