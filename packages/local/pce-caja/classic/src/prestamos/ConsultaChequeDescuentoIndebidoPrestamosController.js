Ext.define('Pce.caja.prestamos.ConsultaChequeDescuentoIndebidoPrestamosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-cheque-descindeb-prestamos',
    requires: [

    ],
    fechaInicio: undefined,
    fechaFinal: undefined,
    numCheque: undefined,
    beneficiario: undefined,

    init: function() {
        this.resetStore();
    },

    resetStore(){
        let store = this.lookup('consultaDescindebTabla').getStore()
        store.removeAll();
    },

    onSeleccionFechaInicio(control, seleccion) {
        if(seleccion == ""){
            this.fechaInicio = null;
            return;
        }
        this.fechaInicio = Ext.util.Format.date(seleccion, 'd/m/Y');
    },
    onSeleccionFechaFinal(control, seleccion) {
        if(seleccion == ""){
            this.fechaFinal = null;
            return;
        }
        this.fechaFinal = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    onBuscarCheques(){
        if(
            this.numCheque == null 
            && (this.beneficiario == null || this.beneficiario == "") 
            && (this.fechaInicio == null && this.fechaFinal == null)
        ){
            return;
        }
        this.cargaTablaCheques(this.fechaInicio, this.fechaFinal, this.beneficiario, this.numCheque);
    },
    
    onChangeNumCheque(comp, valor) {
        this.numCheque = valor;
    },
    
    onChangeBeneficiario(comp, valor) {
        this.beneficiario = valor.toUpperCase();
    },

    cargaTablaCheques(fechaInicio, fechaFinal, beneficiario, numCheque) {
        let store = this.lookup('consultaDescindebTabla').getStore();
        store.setProxy({
            type: 'rest',
            url: '/api/ingresos/asegurado/descuento-indebido/consulta-cheque',
            writer: {
                writeAllFields: true
            }
        });
        this.lookup('consultaDescindebTabla').getStore().load({
            params: {
                fechaInicio,
                fechaFinal,
                beneficiario, 
                numCheque,
            } 
        });
    },

    onImprimirCheque(tabla, i, j, item, e, cheque){
        let chequeId = cheque.data.id
        window.open(`/api/ingresos/asegurado/descuento-indebido/imprimir-cheque?pidemision=${chequeId}`);
    }
});
