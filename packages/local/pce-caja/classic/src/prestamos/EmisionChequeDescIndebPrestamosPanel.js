Ext.define('Pce.caja.prestamos.EmisionChequeDescIndebPrestamosPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'emision-cheque-descindeb-prestamos-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.caja.cheques.emisionCheque.EmisionChequeTabla',
        'Pce.caja.cheques.emisionCheque.EmisionChequeController',
        'Ext.grid.plugin.CellEditing'
    ],

    controller: 'emision-cheque-descindeb-prestamos',

    viewModel: {
        data: {
            cheque: null,
            fechaInicio: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            fechaFinal: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
            banco: null,
            cuentaBanco: null,
        }
    },

    title: 'Emisión de cheques descuento indebido préstamos',
    layout: 'fit',
    emtpyText: 'No hay datos para mostar',
    items: [{
        xtype: 'emision-cheque-descindeb-tabla',
        reference: 'EmisionChequeDescIndebTabla',
        store: {
            type: 'EmisionChequeDescIndeb',
            autoLoad: true
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Fecha desde',
                name: 'fechaInicio',
                allowBlank: false,
                bind:{
                    value: '{fechaInicio}',
                    maxValue: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaInicial'
                }
            }, {
                xtype: 'datefield',
                fieldLabel: 'Fecha Hasta',
                reference: 'fechaFinal',
                allowBlank: false,
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicio}',
                    value: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaFinal'
                }
            }, {
                text: 'Buscar',
                handler: 'onBuscarCheques',
                bind:{
                    disabled: '{!fechaInicio && ! fechaFinal}'
                }
            }]
        }],
        bind: {
            selection: '{cheque}'
        },
    }],
    buttons: [{
        // iconCls: 'x-fa fa-flag-o',
        text: 'Imprimir cheque',
        handler: 'onImprimirCheque',
        bind: {
            disabled: '{!cheque}'
        }
    }]
});
