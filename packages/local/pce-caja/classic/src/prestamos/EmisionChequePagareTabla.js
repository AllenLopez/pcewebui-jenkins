Ext.define('Pce.caja.prestamos.EmisionChequePagareTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'emision-cheque-pagare-tabla',

    columns: [{
        text: 'Pagaré',
        dataIndex: 'pagareId',
        tooltip:'Pagaré',
        flex: 1
    },{
        text: 'Afiliación',
        dataIndex: 'numAfil',
        tooltip:'Numero de Afiliado',
        flex: 1
    }, {
        text: 'Beneficiario',
        dataIndex: 'beneficiario',
        tooltip:'Nombre que corresponde la emisión del cheque',
        flex: 2
    },{
        text: 'Importe',
        dataIndex: 'importe',
        tooltip:'Muestra el importe que se devolverá al asegurado.',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        flex: 1
    },{
        text: 'Etapa',
        dataIndex: 'estatus',
        tooltip:'Etapa en la que se encuentra el cheque',
        flex: 1
    },{
        text: 'Numero Cheque',
        dataIndex: 'numCheque',
        tooltip:'Numero del cheque',
        flex: 1
    }]
});