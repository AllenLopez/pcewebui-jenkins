Ext.define('Pce.caja.prestamos.ConsultaChequePagarePrestamosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-cheque-pagare-prestamos',
    requires: [

    ],
    fechaExpedicion: undefined,
    numCheque: undefined,
    beneficiario: undefined,

    init: function() {
        this.resetStore();
    },

    resetStore(){
        let store = this.lookup('consultaPagareTabla').getStore()
        store.removeAll();
    },

    onSeleccionFechaExpedicion(control, seleccion) {
        if(seleccion == ""){
            this.fechaExpedicion = null;
            return;
        }
        this.fechaExpedicion = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    onBuscarCheques(){
        if(
            this.numCheque == null 
            && (this.beneficiario == null || this.beneficiario == "") 
            && (this.fechaExpedicion == null)
        ){
            return;
        }
        this.cargaTablaCheques(this.fechaExpedicion, this.numCheque, this.beneficiario);
    },
    
    onChangeNumCheque(comp, valor) {
        this.numCheque = valor;
    },
    
    onChangeBeneficiario(comp, valor) {
        this.beneficiario = valor.toUpperCase();
    },

    cargaTablaCheques(fechaExpedicion, numCheque, beneficiario) {
        let store = this.lookup('consultaPagareTabla').getStore();
        store.setProxy({
            type: 'rest',
            url: '/api/prestamos/pagare/consulta-cheque-pagare',
            writer: {
                writeAllFields: true
            }
        });
        this.lookup('consultaPagareTabla').getStore().load({
            params: {
                fechaExpedicion,
                numCheque,
                beneficiario
            } 
        });
    },

    onImprimirCheque(tabla, i, j, item, e, cheque){
        let fechaExpedicion = Ext.util.Format.date(cheque.data.fechaExpedicion, "d/m/Y");
        let chequeId = cheque.data.id;
        window.open(`/api/prestamos/pagare/imprimir-cheque-consulta?pidemprestamo=${chequeId}`);
    }
});
