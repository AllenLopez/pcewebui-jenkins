/**
 * @author Alan López
 */
Ext.define('Pce.caja.prestamos.PagoPrestamoAfiliadoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.pago-prestamos-afiliado',

    requires: [
        'Pce.caja.antiguedad.PagoServicio',
        'Ext.data.writer.Writer',
        'Pce.reporte.ReporteGenerador'
    ],
    
    totalPagar: 0,
    totalRetiro: 0,
    total: 0,
    contador: 0,

   
    onGenerarPago() {
        let viewModel = this.getViewModel();
        let dependenciaFiltro = this.lookup('dependenciaSelector');
        let aseguradoFiltro = this.lookup('aseguradoSelector');
        let storeConceptos = this.lookup('tablaConceptoPrestamos').getStore();
        let bancoEmisor =  this.lookup('bancoEmisor').rawValue;
        let bancoReceptor = this.lookup('bancoReceptor').rawValue;
        let numeroReferencia = this.lookup('referencia').rawValue;
        let cuenta = viewModel.get('cuenta').selection.data.id;
        let formaPago = this.lookup('formaPago').selection.data.id;
        const fechaMovimiento = viewModel.getData().fechaMovimiento;
        if (this.sumaTotal > 0) {
        Ext.Msg.confirm(
            'Confirmar operación',
            `¿Seguro que desea generar el pago?`,
            ans => {
                if (ans === 'yes') {
                    Ext.getBody().mask('Generando pago al prestamo...');
                    Ext.Ajax.request({
                        method: 'POST',
                        url: '/api/prestamos/pagare',
                        jsonData: { 
                            dependenciaId: viewModel.get('dependencia').data.id,
                            numeroAfil: viewModel.get('asegurado').data.id, 
                            importe: this.sumaTotal,
                            bancoEmisor,
                            bancoReceptor,
                            numeroReferencia,
                            cuenta,
                            formaPago,
                            Conceptos: this.lookup('tablaConceptoPrestamos')
                            .getStore()
                            .getRange()
                            .map(model => model.data)
                        },
                        success(response) {
                            const resp = JSON.parse(response.responseText);
                            if (resp.exito) {
                                Ext.Msg.alert('Operación exitosa',
                                `La operación se completó correctamente.
                                Se generó el certificado: <b>${resp.certificado}</b>, con transacción: <b>${resp.transaccionId}</b>`);
                                window.open(`/api/prestamos/pagare/descargar?Pfolio=${resp.certificado}`);
                                aseguradoFiltro.reset();
                                dependenciaFiltro.reset();
                                storeConceptos.removeAll();
                                this.lookup("bancoEmisor").reset();
                                this.lookup("bancoReceptor").reset();
                                this.lookup("referencia").reset();
                                this.lookup("cuenta").reset();
                                this.lookup("formaPago").reset();
                            }
                        },
                        failure(response) {
                            Ext.Msg.alert('error', `Ocurrió un error al intentar procesar los datos: ${resp.mensaje}`);
                        }
                        
                    });
                    Ext.getBody().unmask();
                }
            }
        );
      
        }else{
            Ext.Msg.alert(
                'Total Incorrecto',
                `El total a liquidar debe ser mayor que 0 y/o la fecha es invalida`
            );
        }
    },


    imprimirCertificado(certificadoId) {
        window.open(`/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=IN_CERTIFICADO_INGRESOS_AFIL_RA`);
    },

    onChangeBanco(comp, value){
        let form = this.getView().items.items[0];
         let cuenta =  form.getForm().findField('cuentaBanco');
         cuenta.reset();
         cuenta.getStore().porBanco(value);
    },

    onValidaBoton(){
        this.lookup('tablaConceptoPrestamos').disable();
        cuentaBancaria = this.lookup('cuenta');
        if(cuentaBancaria)
        this.lookup('tablaConceptoPrestamos').enable();
    },

    onSummaryTotalPagar(value){
        this.total += value;
        this.contador++;
        this.totalPagar += value;
        return `<b>${Ext.util.Format.usMoney(value)}</b>`;
    },

    onSummaryTotalRetiro(value){
        this.contador++;
        if(this.contador <= 2){
            
            this.total += value;
            
        }
        let btnPagar = this.lookup('btnPagar');
            if(this.total === 0){
                btnPagar.setDisabled(true);
                btnPagar.setText('Pagar');
            }else{
                btnPagar.setDisabled(false);
                btnPagar.setText(`Pagar ${Ext.util.Format.usMoney(this.total)}`);

            }
        this.totalRetiro += value;
        return `<b>${Ext.util.Format.usMoney(value)}</b>`;
    },

    onSeleccionDependencia(component) {    
            let dependencia = this.getViewModel().get('dependencia')
            let asegurado = this.getViewModel().get('asegurado');
            if (asegurado && dependencia) {
            this.lookup('tablaConceptoPrestamos').getStore().load({
                params: {
                    dependenciaId: dependencia.getId(),
                    aseguradoId: asegurado.getId(),
               },
            });
        }
    },
    onSummary(value, summaryData, dataIndex, context) {
        let saldo = this.lookup('tablaConceptoPrestamos').selection.getData().saldo
        if (value !== undefined && value > 0 && saldo >= value) {
            this.sumaTotal = Math.round(value * 100) / 100;
            this.lookup('botonProcesar').enable();
        }
        else{
            this.lookup('botonProcesar').disable();
        }
        return `<b>Total a pagar: ${Ext.util.Format.usMoney(value)}</b>`;
    },  
   
    /**
     * @returns {Pce.reporte.ReporteGenerador}
     */
    getGeneradorReporte() {
        if (!this.generadorReporte) {
            this.generadorReporte = new Pce.reporte.ReporteGenerador();
        }
        return this.generadorReporte;
    }
});