Ext.define('Pce.caja.prestamos.EmisionChequePagarePrestamosPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'emision-cheque-pagare-prestamos-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.caja.cheques.emisionCheque.EmisionChequeTabla',
        'Pce.caja.cheques.emisionCheque.EmisionChequeController',
        'Ext.grid.plugin.CellEditing'
    ],

    controller: 'emision-cheque-pagare-prestamos',

    viewModel: {
        data: {
            cheque: null,
            fechaExpedicion: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            banco: null,
            cuentaBanco: null,
            botonInvalido: false
        }
    },

    title: 'Emisión de cheques pagarés préstamos',
    layout: 'fit',
    emtpyText: 'No hay datos para mostar',
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            xtype: 'datefield',
            reference: 'fechaExpedicion',
            fieldLabel: 'Fecha Expedición',
            allowBlank: false,
            name: 'fechaExpendicion',
            listeners: {
                change: 'onSeleccionaFechaExpedicion'
            },
        }]
    },{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            xtype: 'textfield',
            reference: 'banco',
            fieldLabel: 'Banco',
            readOnly: true,
            cls: 'read-only',
        },{
            xtype: 'textfield',
            reference: 'cuenta',
            fieldLabel: 'Cuenta',
            readOnly: true,
            cls: 'read-only',
            store: {
                type: 'EmisionPagare',
                autoLoad: true
            },
        }]
    }],
    items: [{
        xtype: 'emision-cheque-pagare-tabla',
        reference: 'chequesPagareTabla',
        store: {
            type: 'EmisionPagare',
            autoLoad: true
        },
    }],
    buttons: [{
        text: 'Imprimir cheques',
        reference: 'boton',
        handler: 'onImprimirCheques',
        bind: {
            disabled: '{!botonInvalido}'
        }
    }]
});