Ext.define('Pce.caja.pagos.PagoAvisoCargoMultipleController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.pago-aviso-cargo-multiple',

    onBancoChange(comp, valor) {
        let cuenta = this.lookup('CuentaSelector');
        cuenta.reset();
        cuenta.getStore().porBanco(valor);
    },

    onDependenciaSeleccionChange(comp, valor) {
        this.cargaEstadosDeCuenta(valor);
    },

    onProcesarPago() {
        const records = this.getEstadoDeCuentaStore().getModifiedRecords();
        const formulario = this.getFormulario();
        let datos = formulario.getValues();

        if (!(records.length > 0 && formulario.isDirty())) {
            return;
        }

        datos.importeTotal = this.getImporteTotal();
        datos.estadoDeCuentaVm = records.map(rec => {
            if (rec.get('importe')) { return rec.data; }
        });

        this.procesarPagosMultiples(datos);
        // this.onConfirmarPago().then(ans => {
        //     if (ans === 'yes') {
        //         this.procesarPagosMultiples(datos);
        //     }
        // });
    },

    onSummary(value) {
        if (value !== undefined && value > 0) {
            this.setImporteTotal(Math.round(value * 100) / 100);
        }
        return `<b>Total a Liquidar: ${Ext.util.Format.usMoney(value)}</b>`;
    },

    onBeforeEdit() {
    },

    onValidateEdit(editor, context) {
        const saldo = Number(context.record.get('saldo'));
        if (context.value < 0 || context.value > saldo) {
            context.cancel = true;
            return false;
        }
        return true;
    },

    onConfirmarPago() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Procesar pago múltiple',
                `¿Seguro que deseas procesar los pagos de los avisos/conceptos seleccionados?`,
                ans => resolve(ans)
            );
        });
    },

    onEditImporte() {

    },

    procesarPagosMultiples(datos) {
        this.onConfirmarPago().then(ans => {
            if (ans === 'yes') {
                Ext.Ajax.request({
                    method: 'POST',
                    url: '/api/caja/dependencia/pagos/pago-aviso-cargo',
                    jsonData: datos,
                    success: response => this.onProcesarPagoMultipleSuccess(JSON.parse(response.responseText)),
                    failure: response => this.onProcesarPagoMultipleFailure(JSON.parse(response.responseText))
                });
            }
        });
    },

    onProcesarPagoMultipleSuccess(resp) {
        if (resp.exito){
            Ext.Msg.alert({
                title:'Operación completada',
                msg: `La operación se completo correctamente.Se generó el certificado: <b>${resp.folioCertificado}</b> y la transacción: <b>${resp.transaccionId}</b>`
            });
            this.resetPantalla();
            this.mostrarReporteAfectacion(resp.folioCertificado);
        } else {
            Ext.Msg.alert({
                title:'Operación completada',
                msg: `La operación no pudo ser procesada: <b>${resp.mensaje}</b>`
            });
        }
        Ext.getBody().unmask();
    },

    onProcesarPagoMultipleFailure(resp) {
        Ext.Msg.alert({
            title:'Operación completada',
            msg: `Ocurrió un error al procesar los datos: ${resp.mensaje}`
        });
        Ext.getBody().unmask();
    },

    cargaEstadosDeCuenta(dependenciaId) {
        const store = this.getEstadoDeCuentaStore();
        if (store) {
            store.load({
                params: { dependenciaId: dependenciaId }
            });
        }
    },

    mostrarReporteAfectacion(certificadoId) {
        window.open(`/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=IN_CERTIFICADO_INGRESOS_MAC`);
    },

    resetPantalla() {
        this.getEstadoDeCuentaStore().load({});
        this.getFormulario().reset();
    },

    getFormulario() {
        const formulario = this.getView().down('form').getForm();
        if (formulario) { return formulario; }
    },

    getEstadoDeCuentaStore() {
        const view = this.getView();
        let store;

        if (view.getXType() === 'pago-aviso-cargo-multiple-panel') {
            store = this.lookup('EstadoCuentaAgrupadoTabla').getStore();
        } else {
            store = view.getStore();
        }
        if (store) { return store; }
    },

    getImporteTotal() {
        return this.getViewModel().get('importeTotal') || 0;
    },

    setImporteTotal(valor) {
        const vm = this.getViewModel();
        if (vm) {
            vm.set('importeTotal', valor);
        }
    },
});