/**
 * @class Pce.caja.pagos.avisoCargoMultiple.AvisosCargoAgrupaConceptoTabla
 * @extends Ext.grid.Panel
 * @xtype aplicacion-saldo-tabla
 * Grid que enlista estado de cuenta de dependencia agrupado por emisión de aviso de cargo.
 * Perimite editar el monto a pagar por cada concepto y sumarizar el total
 */
Ext.define('Pce.caja.pagos.avisoCargoMultiple.AvisosCargoAgrupaConceptoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'avisos-cargo-agrupados-tabla',

    requires: [
        'Ext.grid.feature.Grouping',
        'Pce.caja.pagos.PagoAvisoCargoMultipleController',
        'Pce.caja.estadoDeCuenta.EstadoDeCuentaPorEmisionStore',

        'Ext.plugin.Abstract',
        'Ext.grid.plugin.CellEditing'
    ],

    store: {
        type: 'estado-cuenta-por-emision',
        groupField: 'folio',
        groupDir: 'DESC',
        remoteSort: false
    },

    features: [{
        ftype:'grouping',
        groupHeaderTpl: ['Aviso cargo: {name} ({[values.children.length]})']
    },{
        ftype: 'summary',
        dock: 'top'
    }],


    plugins: [{
        ptype: 'cellediting',
        reference: 'captura-editor',
        clicksToEdit: 1
    }],


    listeners: [{
        beforeedit: 'onBeforeEdit',
        validateedit: 'onValidateEdit',
        edit: 'onEditImporte'
    }],

    // queryMode: 'local',

    // selModel: {
    //     selType: 'checkboxmodel',
    //     showHeaderCheckbox : false,
    //     checkOnly: true,
    //     listeners: {
    //         deselect: 'onDeselect'
    //     }
    // },

    controller: 'pago-aviso-cargo-multiple',

    emptyText: 'No existen registros',

    columns: [{
        text: 'Concepto',
        dataIndex: 'conceptoDescripcion',
        flex: 2
    }, {
        xtype: 'datecolumn',
        text: 'Fecha Emisión',
        dataIndex: 'fechaEmision',
        // sortable: true,
        // format: 'd/m/Y',
        flex: 1
    },{
        text: 'Saldo',
        dataIndex: 'saldo',
        renderer: v => Ext.util.Format.currency(v,'$'),
        flex: 1
    },  {
        text: 'Importe a pagar',
        dataIndex: 'importe',
        readOnly: true,
        reference: 'editorImporte',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        summaryType: 'sum',
        summaryRenderer: 'onSummary',
        editor: {
            xtype: 'numberfield',
            hideTrigger: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false
        },
        flex: 1,
    }],


});