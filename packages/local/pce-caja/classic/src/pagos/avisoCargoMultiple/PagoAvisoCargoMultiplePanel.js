Ext.define('Pce.caja.pagos.PagoAvisoCargoMultiplePanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'pago-aviso-cargo-multiple-panel',

    requieres: [
        'Pce.caja.pagos.PagoAvisoCargoMultipleController',
        'Pce.caja.pagos.avisoCargoMultiple.AvisosCargoAgrupaConceptoTabla'
    ],

    controller: 'pago-aviso-cargo-multiple',

    viewModel: {
        data: {
            referenciaBancaria: null,
            importeTotal: 0
        }
    },

    layout: 'fit',
    title: 'Pago de aviso de cargo de múltiple',

    items: [{
        xtype: 'avisos-cargo-agrupados-tabla',
        reference: 'EstadoCuentaAgrupadoTabla',
        sortableColumns: false,
        dockedItems: [{
            xtype: 'form',
            doc: 'top',
            reference: 'formularioFiltros',
            defaults: {
                width: '95%',
                padding: 5,
            },
            layout: {
                type: 'table',
                columns: 4
            },
            items: [{
                xtype: 'dependencia-selector',
                name: 'dependenciaId',
                reference: 'DependenciaSelector',
                allowBlank: false,
                blankText: 'Este campo es obligatorio',
                listeners: {
                    change: 'onDependenciaSeleccionChange'
                },
                colspan: 3
            }, {
                xtype: 'combobox',
                name: 'formaPagoId',
                reference: 'FormaPagoSelector',
                fieldLabel: 'Forma de pago',
                valueField: 'id',
                displayField: 'descripcion',
                forceSelection: true,
                queryMode: 'local',
                store: {
                    autoLoad: true,
                    fields: [
                        'descripcion'
                    ],
                    proxy: {
                        type: 'rest',
                        url: '/api/ingresos/forma-pago',
                        writer: {
                            type: 'json',
                            writeAllFields: true
                        }
                    }
                },
                bind: {
                    disabled: '{!DependenciaSelector.selection}'
                },
                allowBlank: false,
                colspan: 1
            }, {
                xtype: 'combobox',
                name: 'bancoEmisorId',
                reference: 'BancoEmisorSelector',
                fieldLabel: 'Banco emisor',
                valueField: 'id',
                displayField: 'bancoNombre',
                queryMode: 'local',
                forceSelection: true,
                allowBlank: false,
                store: {
                    autoLoad: true,
                    fields: [
                        'bancoNombre',
                        'descripcion',
                        'claveBancoSat',
                        'activo',
                        'reporte',
                        {name: 'fechaCreacion', type: 'date', persist: false},
                        {name: 'usuarioCreacion', persist: false},
                        {name: 'fechaModificacion', type: 'date', persist: false},
                        {name: 'usuarioModificacion', persist: false}
                    ],
                    proxy: {
                        type: 'rest',
                        url: '/api/caja/banco',
                        writer: {
                            type: 'json',
                            writeAllFields: true
                        }
                    }
                },
                bind: {
                    disabled: '{!FormaPagoSelector.selection}'
                },
                colspan: 1
            }, {
                xtype: 'textfield',
                name: 'referencia',
                reference: 'ReferenciaTexto',
                fieldLabel: 'Referencia',
                fieldName: 'referencia',
                allowBlank: false,
                bind: {
                    value: '{referenciaBancaria}',
                    disabled: '{!BancoEmisorSelector.selection}'
                },
                flex: 1
            }, {
                xtype: 'combobox',
                name: 'bancoReceptorId',
                reference: 'BancoReceptorSelector',
                fieldLabel: 'Banco receptor',
                valueField: 'id',
                displayField: 'bancoNombre',
                queryMode: 'local',
                allowBlank: false,
                forceSelection: true,
                listeners: {
                    change: 'onBancoChange'
                },
                store: {
                    autoLoad: true,
                    fields: [
                        'bancoNombre',
                        'descripcion',
                        'claveBancoSat',
                        'activo',
                        'reporte',
                        {name: 'fechaCreacion', type: 'date', persist: false},
                        {name: 'usuarioCreacion', persist: false},
                        {name: 'fechaModificacion', type: 'date', persist: false},
                        {name: 'usuarioModificacion', persist: false}
                    ],
                    proxy: {
                        type: 'rest',
                        url: '/api/caja/banco',
                        writer: {
                            type: 'json',
                            writeAllFields: true
                        }
                    },
                    filters: [{
                        property: 'reporte',
                        operator: '!=',
                        value: null
                    }],
                },
                bind: {
                    disabled: '{!referenciaBancaria}'
                },
                colspan: 1
            }, {
                xtype: 'cuenta-banco-selector',
                name: 'cuentaBancariaId',
                reference: 'CuentaSelector',
                allowBlank: false,
                bind: {
                    disabled: '{!BancoReceptorSelector.selection}',
                },
                flex: 1
            }]
        }]
    }, {
    }],

    buttons: [{
        text: 'Procesar',
        reference: 'botonProcesar',
        handler: 'onProcesarPago',
        bind: {
            disabled: '{importeTotal < 0.001 || !CuentaSelector.selection}'
        }
    }]

});