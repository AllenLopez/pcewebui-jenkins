Ext.define('Pce.caja.pagoLicencias.PagoLicenciasPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'pago-licencias-panel',

    requires: [
        'Pce.caja.pagoLicencias.PagoLicenciasForma',
        'Pce.caja.cuentaBanco.CuentaBancoSelector',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.asegurado.AseguradoSelector',
    'Pce.caja.pagoLicencias.PagoLicenciasController',
    'Pce.caja.pagoLicencias.LicenciaSelector',
        'Pce.afiliacion.asegurado.AseguradoDisplayer'
    ],

    viewModel: {
        data: {
            afiliado: null,
            licencia: null
        }
    },

    controller: 'pago-licencias-panel',
    title: 'Pago de licencias',
    layout: 'fit',
    items: [{
        xtype: 'pago-licencias-forma',
        reference: 'forma',
        padding: 10
    },],
    buttons: [{
        text: 'Procesar movimiento',
        handler: 'onProcesarMovimiento',
    }]
});