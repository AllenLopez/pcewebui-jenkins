/**
 * @author Felipe fierro
 *
 * @class
 * @name Pce.caja.pagoLicencias.LicenciaSelector
 */
Ext.define('Pce.caja.pagoLicencias.LicenciaSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'licencia-selector',
    requires: [
         'Pce.caja.licenciaSolicitud.LicenciaSolicitudStore',
    ],
   
    queryMode: 'local',
    valueField: 'id',
    displayField: 'tipoLicencia',
    fieldLabel: 'Licencias',
    // labelWidth: 70,
    forceSelection: true,
    // emptyText: 'Nombre completo, número de afiliación o RFC',

    store: {
        type: 'licencia-solicitud',
       
        
    },
    displayTpl: [
        '<tpl for=".">',
            '{id} [{tipoLicencia}]',
        '</tpl>'
    ],
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{id} [{tipoLicencia}]</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ]
});