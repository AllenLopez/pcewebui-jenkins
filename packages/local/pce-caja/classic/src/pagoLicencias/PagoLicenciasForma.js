Ext.define('Pce.caja.pagoLicencias.PagoLicenciasForma',{
    extend: 'Ext.form.Panel',
    xtype: 'pago-licencias-forma',

    requieres: [
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox',
        'Ext.layout.container.Form',
        'Pce.ingresos.FormaPagoSelector',
        'Pce.ingresos.concepto.ConceptoSelector',
    ],

    layout: {
        type: 'table',
        columns: 12
    },
    padding: 10,

    defaults: {
        width: '100%'
    },
    viewModel: {
        data: {
            afiliado: null, 
            fecha: new Date()
        }
    },

    items: [{
        xtype: 'asegurado-selector',
        name: 'asegurado',
        colspan: 6,
        allowBlank: false,
        blankText: "Este campo es obligatorio",
        listeners: {
            change: 'onChangeAsegurado'
        },
        bind: {
            selection: '{afiliado}'
        }
    },{
        xtype: 'licencia-selector',
        name: 'licencia',
        colspan: 6,
        allowBlank: false,
        blankText: "Este campo es obligatorio",
        bind: {
            selection: '{licencia}'
        }
    },{
        xtype: 'textfield',
        fieldLabel: 'Dirección',
        name: 'direccion',
        readOnly: true,
        colspan: 4,
        bind:{
            value: '{afiliado.direccion}'
        },
        cls:"read-only"
    },{
        xtype: 'textfield',
        fieldLabel: 'Colonia',
        name: 'colonia',
        readOnly: true,
        colspan: 4,
        bind:{
            value: '{afiliado.colonia}'
        },
        cls:"read-only"
    },{
        xtype: 'textfield',
        fieldLabel: 'Código postal',
        name: 'codigoPostal',
        readOnly: true,
        colspan: 2,
        bind:{
            value: '{afiliado.codigoPostal}'
        },
        cls:"read-only"
    },{
        xtype: 'textfield',
        fieldLabel: 'Teléfono',
        name: 'telefono',
        readOnly: true,
        colspan: 2,
        bind:{
            value: '{afiliado.telefono}'
        },
        cls:"read-only"
    },{
        xtype: 'textfield',
        fieldLabel: 'Dependencia',
        name: 'dependencia',
        readOnly: true,
        colspan: 5,
        bind:{
            value: '{licencia.dependencia}'
        },
        cls:"read-only"
    },{
        xtype: 'textfield',
        fieldLabel: 'Tipo licencia',
        name: 'tipoLicencia',
        readOnly: true,
        colspan: 3,
        bind:{
            value: '{licencia.tipoLicencia}'
        },
        cls:"read-only"
    },{
        xtype: 'datefield',
        fieldLabel: 'Fecha inicio:',
        name: 'fechaInicio',
        readOnly: true,
        colspan: 2,
        format: 'd/m/Y',
        bind:{
            value: '{licencia.fechaInicio}'
        },
        cls:"read-only"
    },{
        xtype: 'datefield',
        fieldLabel: 'Fecha fin',
        name: 'fechaFin',
        readOnly: true,
        colspan: 2,
        format: 'd/m/Y',
        bind:{
            value: '{licencia.fechaFin}'
        },
        cls:"read-only"
    },{
        xtype: 'textfield',
        fieldLabel: 'Días',
        name: 'dias',
        readOnly: true,
        colspan: 2,
        bind:{
            value: '{licencia.dias}'
        },
        cls:"read-only"
    },{
        xtype: 'numberfield',
        fieldLabel: 'Importe licencia',
        name: 'importe',
        readOnly: true,
        colspan: 3,
        labelWidth: 120,
        decimalPrecision: 2,
        decimalSeparator: '.',
        allowDecimals: true,
        bind:{
            value: '{licencia.importe}'
        },
        cls:"read-only"
    },{
        xtype: 'datefield',
        reference: 'fecha',
        fieldLabel: 'Fecha',
        name: 'fecha',
        bind:{
            value: '{fecha}'
        },
        colspan: 4,
        readOnly: true,
        cls:"read-only",
    }, {
        xtype: 'forma-pago-selector',
        name: 'formaPago',
        allowBlank: false,
        colspan: 3
    },{
        xtype: 'banco-selector',
        allowBlank: false,
        name: 'bancoEmisor',
        fieldLabel: 'Banco emisor',
        colspan: 4
    },{
        xtype: 'numberfield',
        fieldLabel: 'Num. referencia',
        name: 'chequeCuenta',
        allowBlank: false,
        colspan: 4,
    },{
        xtype: 'banco-selector',
        allowBlank: false,
        name: 'bancoReceptor',
        fieldLabel: 'Banco receptor',
        colspan: 4,
        listeners: {
            change: 'onChangeBanco'
        }
    },{
        xtype: 'cuenta-banco-selector',
        allowBlank: false,
        name: 'cuenta',
        colspan: 4
    },{
        xtype: 'numberfield',
        readOnly: true,
        fieldLabel: 'Certificado',
        name: 'certificado',
        colspan: 2,
        labelStyle: 'font-weight: bold',
        fieldStyle: 'font-weight: bold',
        store:{
            proxy: {
                type: 'rest',
                url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
            },
            autoLoad: true
        },
        cls:"read-only",
    },]
});