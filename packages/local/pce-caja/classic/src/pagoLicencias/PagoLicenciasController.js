Ext.define('Pce.caja.pagoLicencias.PagoLicenciasController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.pago-licencias-panel',
    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.caja.pagoLicencias.PagoLicenciasForma'
    ],

    init: function () {
        this.obtenerFolioCertificado();
    },

    obtenerFolioCertificado() {
        Ext.Ajax.request({
            url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
            method: 'GET',
            scope: this,
            success(response) {
                let res = JSON.parse(response.responseText);
                let certificado = this.lookup('forma').getForm().findField('certificado');
                certificado.setValue(res);
            }
        });
    },

    onChangeAsegurado(comp, aseguradoId) {
        let forma = this.lookup('forma');
        let licenciaSelector = forma.getForm().findField('licencia');
        licenciaSelector.store.load({
            params: {
                aseguradoId
            }
        });
    },

    onProcesarMovimiento() {
        let forma = this.lookup('forma');
        if (!forma.isValid()) {
            return;
        }

        this.confirmarProceso(forma.getForm().getFieldValues().certificado).then(ans => {
            if (ans === 'yes') {
                this.procesar(forma.getForm().getFieldValues(), forma);
            }
        });
    },

    procesar(data, forma) {
        Ext.getBody().mask('Realizando pago...');
        delete data.codigoPostal;
        delete data.colonia;
        delete data.dependencia;
        delete data.dias;
        delete data.direccion;
        delete data.telefono;
        delete data.tipoLicencia;
        delete data.fechaFin;
        delete data.fechaInicio;
        delete data.importe;
        Ext.Ajax.request({
            url: '/api/caja/pago-licencia',
            method: 'POST',
            jsonData:data,
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
        Ext.getBody().unmask();
                if(res.success){
                    Ext.MessageBox.show({
                        title:'Operación completada',
                        msg: `La operación se completo correctamente y se genero la transacción <b>${res.transaccion.id}</b>`,
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.INFO,
                        fn:(btn) => {
                                forma.reset();
                        this.getView().getController().obtenerFolioCertificado();
                        this.getView().getController().generarReporte(data.certificado);
                        }
                    });


                }else{

                }
            },
            failure(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                Ext.Msg.alert(
                    'Ocurrió un error',
                    `La operación termino con errores`
                );
            }
        })
    },

    confirmarProceso(certificado) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Procesar Movimiento',
                `Se va a generar el certificado con el folio <b>${certificado}</b>. ¿Desea generar el pago?`,
                ans => resolve(ans)
            );
        });
    },

    onChangeBanco(comp, value, old) {
        let cuenta = this.lookup('forma').getForm().findField('cuenta');
        cuenta.reset();
        cuenta.getStore().porBanco(value);
    },

    generarReporte(certificadoId) {
        
        window.open(`/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=IN_CERTIFICADO_INGRE_AFIL_LICENCIAS`);
        // window.open(`/api/caja/pago-licencia/reporte?transaccionId=${transaccionId}`);
    }
});