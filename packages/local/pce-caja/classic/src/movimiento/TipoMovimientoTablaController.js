Ext.define('Pce.caja.movimiento.TipoMovimientoTablaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.tipo-movimiento-tabla',

    requires: [
        'Pce.view.dialogo.FormaContenedor',

        'Pce.caja.movimiento.TipoMovimientoForma'
    ],

    onNuevoTipoMovimiento() {
        this.mostrarDialogoEdicion(new Pce.ingresos.movimiento.TipoMovimiento({
            activo: true
        }));
    },

    onEditarTipoMovimiento(tabla, i, j, item, e, tipoMovimiento) {
        this.mostrarDialogoEdicion(tipoMovimiento);
    },

    onEliminarTipoMovimiento(tabla, i, j, item, e, tipoMovimiento) {
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar el tipo de movimiento 
            "${tipoMovimiento.get('descripcion')}"`,
            ans => {
                if (ans === 'yes') {
                    tipoMovimiento.erase({
                        success() {
                            Ext.toast('El tipo de movimiento se ha eliminado');
                        },
                        failure() {
                            Ext.Msg.alert(
                                'Operación inválida',
                                'El tipo de movimiento no puede ser ' +
                                'eliminado por que existen datos asociados ' +
                                'a él'
                            );
                        }
                    });
                }
            }
        );
    },

    onGuardarTipoMovimiento(dialogo, tipoMovimiento) {
        let me = this;
        let nuevo = tipoMovimiento.phantom;
        let form = dialogo.down('tipo-movimiento-forma').getForm();
        let estatus = form.findField('activo').getValue();
        
        tipoMovimiento.data.activo = estatus;
        Ext.Msg.confirm(
            'Guardar tipo de movimiento',
            `¿Seguro que deseas guardar el movimiento <i>${tipoMovimiento.get('descripcion')}</i>?`,
            ans => {
                if(ans === 'yes') {
                    tipoMovimiento.save({
                        success() {
                            let msg = 'Los cambios se han guardado correctamente';
                            if (nuevo) {
                                me.view.getStore().reload();
                                msg = 'Se ha creado un nuevo tipo de movimiento';
                            }
                            Ext.toast(msg);
                            dialogo.close();
                        }
                    });
                }
            }
        );
    },

    mostrarDialogoEdicion(tipoMovimiento) {
        let title = tipoMovimiento.phantom ? 'Nuevo tipo de movimiento'
            : 'Editar tipo movimiento';
        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title,
            record: tipoMovimiento,
            width: 400,

            items: {
                xtype: 'tipo-movimiento-forma'
            },
            listeners: {
                guardar: this.onGuardarTipoMovimiento.bind(this)
            }
        });
    }
});