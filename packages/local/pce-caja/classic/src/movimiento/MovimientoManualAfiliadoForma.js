Ext.define('Pce.caja.movimiento.MovimientoManualAfiliadoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'movimiento-manual-afiliado-forma',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.Text',
        'Ext.form.field.ComboBox',
        'Ext.form.RadioGroup',
        'Pce.caja.movimiento.TipoMovimientoSelector'
    ],

    items: [{
        xtype: 'container',
        items: [{
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'hiddenfield',
                name: 'tipo',
                value: 'A'
            }, {
                xtype: 'movimiento-selector',
                name: 'tipoMovimiento',
                allowBlank: false,
                flex: 4,
                margin: 20
            }, {
                xtype: 'textfield',
                name: 'numeroAfiliacion',
                fieldLabel: 'No. Afiliacion',
                enabled: false,
                allowBlank: false,
                flex: 2,
                margin: 20
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'concepto-selector',
                name: 'concepto',
                allowBlank: false,
                flex: 3,
                margin: 20
            }, {
                xtype: 'radiogroup',
                name: 'asiento',
                margin: 20,
                flex: 1,
                items: [
                    { boxLabel: 'Cargo', name: 'rb', inputValue: 'C', checked: true },
                    { boxLabel: 'Abono', name: 'rb', inputValue: 'A' }
                ]
            }, {
                xtype: 'textfield',
                name: 'importe',
                fieldLabel: 'Importe',
                labelWidth: 60,
                allowBlank: false,
                flex: 2,
                margin: 20
            }]
        }]
    }],

    // hidden: 'A'
    
    setValor (valor) {
        this.valor = valor;
    },
});