Ext.define('Pce.caja.movimiento.MovimientoManualDependenciaForma', {
    extend: 'Ext.form.Panel',
    xtype: 'movimiento-manual-dependencia-forma',
    reference: 'movimientoManualDependenciaForma',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.Text',
        'Ext.form.field.ComboBox',
        'Ext.form.RadioGroup',

        'Pce.caja.movimiento.PagoManualConceptosTabla',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        // 'Pce.ingresos.avisocargo.EmisionAvisoCargoSelector',

        // 'Pce.ingresos.formaPago.FormaPagoStore',
        // 'Pce.caja.banco.BancoStore',
        // 'Pce.caja.banco.CuentaBancoStore'
    ],

    viewModel: {
        data: {
            cuentaBancaria: null,
            montoCheque: null
        }
    },

    items: [{
        xtype: 'container',
        items: [{
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'hiddenfield',
                name: 'tipo',
                value: 'D'
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'dependencia-selector',
                reference: 'dependenciaSelector',
                name: 'dependencia',
                flex: 4,
                margin: 5,
                labelWidth: 120,
                listeners: {
                    change: 'onSeleccionDependencia'
                }
            }, {
                xtype: 'emision-avisocargo-selector',
                reference: 'emisionAvisoCargoSelector',
                name: 'avisocargo',
                fieldLabel: 'Folio aviso cargo',
                emptyText: 'Seleccione aviso de cargo',
                labelWidth: 120,
                flex: 2,
                margin: 5,
                listeners: {
                    change: 'onSeleccionAvisoCargo'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{ 
                xtype: 'textfield',
                reference: 'fechaEmisionAvisoCargo',
                fieldLabel: 'Fecha de emisión',
                name: 'fecha',
                flex: 2,
                readOnly: true,
                cls:"read-only",
                labelWidth: 120,
                margin: 5
            }, {
                xtype: 'textfield',
                name: 'saldo',
                reference: 'saldoAvisoCargo',
                fieldLabel: 'Saldo',
                readOnly: true,
                flex: 2,
                labelWidth: 115,
                cls:"read-only",
                margin: 5
            }, {
                xtype: 'combobox',
                name: 'formaPagoId',
                fieldName: 'formaPagoId',
                fieldLabel: 'Forma de pago',
                valueField: 'id',
                flex: 2,
                displayField: 'descripcion',
                forceSelection: true,
                emptyText: 'Seleccione forma de pago',
                queryMode: 'local',
                labelWidth: 120,
                allowBlank: false,
                margin: 5,
                store: {type: 'forma-pago', autoLoad: true},
                listeners:{
                    change: 'onSeleccionFormaPago'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'combobox',
                name: 'banco',
                fieldName: 'BancoId',
                fieldLabel: 'Banco emisor',
                valueField: 'bancoNombre',
                displayField: 'bancoNombre',
                emptyText: 'Seleccione banco',
                queryMode: 'local',
                labelWidth: 120,
                margin: 5,
                flex: 1,
                allowBlank: false,
                forceSelection: true,
                store: {type: 'banco', autoLoad: true},
                listeners: {
                    change: 'onDefineBancoEmisor'
                }
            }, {
                xtype: 'textfield',
                name: 'cheque',
                reference: 'cheque',
                fieldLabel: 'Num. referencia',
                labelWidth: 115,
                margin: 5,
                flex: 1,
                allowBlank: false,
                listeners: {
                    change: 'onDefineCheque'
                }
            }, {
                xtype: 'combobox',
                name: 'banco',
                fieldName: 'BancoId',
                fieldLabel: 'Banco receptor',
                valueField: 'id',
                displayField: 'bancoNombre',
                emptyText: 'Seleccione banco',
                queryMode: 'local',
                labelWidth: 120,
                margin: 5,
                flex: 1,
                allowBlank: false,
                forceSelection: true,
                store: {type: 'banco', autoLoad: true},
                listeners: {
                    change: 'onSeleccionBanco'
                }
            }, {
                xtype: 'cuenta-banco-selector',
                allowBlank: false,
                name: 'numeroCuenta',
                colspan: 4,
                margin: 5,
                flex: 1,
                reference: 'cuentaBancoSelector',
                listeners: {
                    change: 'onSeleccionCuentaBanco'
                },
                bind: {
                    selection: '{cuentaBancaria}'
                }
            },
        ]
        }, ,{
            xtype: 'numberfield',
            readOnly: true,
            fieldLabel: 'Certificado',
            name: 'certificado',
            labelStyle: 'font-weight: bold',
            fieldStyle: 'font-weight: bold',
            margin: 5,
            labelWidth: 120,
            store:{
                proxy: {
                    type: 'rest',
                    url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
                },
                autoLoad: true
            },
            cls:"read-only",
        },{
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'numberfield',
                name: 'montoCheque',
                reference: 'montoCheque',
                fieldLabel: 'Importe a pagar',
                allowBlank: false,
                bind: {
                    value: '{montoCheque}'
                },
                labelWidth: 120,
                maxValue: 999999999,
                width: 200,
                flex: 1,
                margin: 5
            }]
        }]
    }, {
        items: [{
            xtype: 'pago-manual-concepto-tabla',
            reference: 'tablaEstadosDeCuenta',
            bind: {
                disabled: '{!cuentaBancaria}'
            }
        }]
    }],

    buttons: [ {
        text: 'Procesar Movimiento',
        reference: 'botonProcesar',
        handler: 'onProcesarPago',
        bind: {
            disabled: '{!montoCheque}'
        }
    }]
});