Ext.define('Pce.caja.movimiento.MovimientoManualPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'movimiento-manual-panel',
    reference: 'movimientoManualPanel',

    requires: [
        'Pce.caja.movimiento.forma.MovimientoFormaContenedor',
        'Pce.caja.movimiento.MovimientoManualDependenciaForma',
        // 'Pce.ingresos.avisocargo.EmisionAvisoCargoStore',
        'Pce.caja.movimiento.MovimientoManualController'
    ],

    controller: 'movimiento-manual-controller',

    title: 'Pagos aviso de cargo',

    viewModel: {
        data: {
            tipoForma: 'D'
        }
    },
    
    items: [{
        xtype: 'container',
        items: [
        //     {
        //     xtype: 'combobox',
        //     name: 'movimiento',
        //     reference: 'movimientoSelector',
        //     fieldLabel: 'Movimiento por',
        //     labelWidth: 110,
        //     allowBlank: false,
        //     flex: 3,
        //     margin: 5,
        //     store: {
        //         fields: ['valor', 'display'],
        //         data : [
        //             {'valor': 'D', 'display': 'DEPENDENCIA'},
        //             {'valor': 'A', 'display': 'AFILIADO'}
        //         ]
        //     },
        //     queryMode: 'local',
        //     displayField: 'display',
        //     valueField: 'valor',
        //     bind: {
        //         value: '{tipoForma}'
        //     }
        // }, {
        //     xtype: 'movimiento-forma-contenedor',
        //     hidden: true,
        //     bind: {
        //         hidden: '{tipoForma != "A"}'
        //     }
        // },
         {
            xtype: 'movimiento-manual-dependencia-forma',
            hidden: true,
            bind: {
                hidden: '{tipoForma != "D"}'
            }
        }],
    }]
});