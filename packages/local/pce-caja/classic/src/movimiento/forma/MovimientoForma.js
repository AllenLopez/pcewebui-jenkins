Ext.define('Pce.caja.movimiento.forma.MovimientoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'movimiento-forma',

    requires: [
        // 'Pce.ingresos.concepto.ConceptoSelector',
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.afiliacion.dependencia.DependenciaSelector'
    ],

    defaultType: 'container',
    defaults: {
        width: '100%',
        layout: {
            type: 'hbox',
            align: 'stretch'
        }
    },
    style: 'padding: 10px',
    items: [{
        style: 'margin-bottom: 10px',
        items: [{
            xtype: 'asegurado-selector',
            name: 'numeroAfiliacion',
            flex: 1,
        }, {
            xtype: 'dependencia-selector',
            name: 'dependencia',
            flex: 1
        }]
    }, {
        defaults: {style: 'margin-right: 10px;'},
        style: 'margin-bottom: 10px',
        items: [{
            xtype: 'concepto-selector',
            name: 'concepto',
            flex: 2
        }, {
            xtype: 'combobox',
            name: 'tipo',
            fieldLabel: 'Tipo',
            valueField: 'tipo',
            displayField: 'tipo',
            store: {data: [{tipo: 'CARGO'}, {tipo: 'ABONO'}]},
            value: 'CARGO',
            forceSelection: true,
            allowBlank: false,
            flex: 1
        }, {
            xtype: 'textfield',
            name: 'importe',
            fieldLabel: 'Importe',
            allowBlank: false,
            flex: 1,
            style: 'margin-right: 0px;'
        }]
    }, {
        defaults: {style: 'margin-right: 10px;'},
        items: [{
            xtype: 'textfield',
            name: 'banco',
            fieldLabel: 'Banco',
            allowBlank: false,
            width: '25%'
        }, {
            xtype: 'textfield',
            name: 'numeroCheque',
            fieldLabel: 'Núm. Cheque',
            allowBlank: false,
            width: '25%'
        }]
    }]
});