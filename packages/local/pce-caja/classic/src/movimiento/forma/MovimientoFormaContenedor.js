Ext.define('Pce.caja.movimiento.forma.MovimientoFormaContenedor', {
    extend: 'Ext.panel.Panel',
    xtype: 'movimiento-forma-contenedor',

    requires: [
        'Pce.caja.movimiento.forma.MovimientoForma',
        'Pce.caja.movimiento.forma.TraspasoForma',
        'Pce.caja.movimiento.TipoMovimientoSelector',
        // 'Pce.ingresos.transaccion.Transaccion',
        // 'Pce.ingresos.transaccion.ResultadoDialogo'
    ],

    defaultListenerScope: true,

    layout: 'card',
    tipoMovimientoMap: {
        TRA: 1,
        COR: 2
    },

    tbar: [{
        xtype: 'movimiento-selector',
        width: '50%',
        store: {
            type: 'tipos-movimiento',
            autoLoad: true,
            filters: [{
                property: 'activo',
                value: true
            }, {
                property: 'esMovimientoAfiliado',
                value: true
            }, {
                property: 'esManual',
                value: true
            }]
        },
        listeners: {
            select: 'onTipoMovimientoSelect'
        }
    }],
    items: [{
        xtype: 'movimiento-forma'
    }, {
        xtype: 'movimiento-traspaso-forma'
    }, {
        xtype: 'movimiento-correccion-forma'
    }],
    buttons: [{
        text: 'Procesar movimiento',
        handler: 'onProcesarMovimiento'
    }],

    onTipoMovimientoSelect(selector, tipoMovimiento) {
        let clave = tipoMovimiento.get('clave');
        let pos = this.tipoMovimientoMap[clave] || 0;

        let layout = this.getLayout();
        layout.setActiveItem(pos);
        layout.getActiveItem().getForm().reset();

        this.tipoMovimiento = clave;
    },

    onProcesarMovimiento() {
        var me = this;
        let form = this.getLayout().getActiveItem().getForm();
        let values = form.getFieldValues();

        values.tipoMovimiento = this.tipoMovimiento;

        Ext.Ajax.request({
            url: '/api/ingresos/asegurado/movimiento/procesar',
            method: 'POST',
            params: values,
            callback(options, success, request) {
                let resultado = JSON.parse(request.responseText);
                if (resultado.exito) {
                    Ext.toast('Movimiento procesado exitósamente');
                    form.reset();
                } else {
                    me.mostrarErrores(resultado.transaccionId);
                }
            }
        });
    },

    mostrarErrores(transaccionId) {
        Pce.ingresos.transaccion.Transaccion.load(transaccionId, {
            success: (transaccion) => {
                Ext.create({
                    xtype: 'transaccion-resultado-dialogo',
                    transaccion: transaccion,
                    autoShow: true,
                    modal: true
                });
            }
        });
    }
});