Ext.define('Pce.caja.movimiento.MovimientoManualController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.movimiento-manual-controller',

    requires: [
        'Pce.caja.movimiento.PagoManualConceptosTabla'
    ],

    sumaTotal: undefined,
    folio: undefined,
    formaPagoId: undefined,
    bancoId: undefined,
    cuentaBancoId: undefined,
    bancoEmisor: undefined,
    numeroCheque: undefined,
    transaccionId: undefined,
    certificado: undefined,

    init: function () {
        this.obtenerFolioCertificado();
    },

    obtenerFolioCertificado() {
        Ext.Ajax.request({
            url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
            method: 'GET',
            scope: this,
            success(response) {
                let res = JSON.parse(response.responseText);
                let certificado = this.lookup('movimientoManualDependenciaForma').getForm().findField('certificado');
                certificado.setValue(res);
                this.certificado = res;
            }
        });
    },

    onProcesarPago() {
        let forma = this.lookup('movimientoManualDependenciaForma');
        if (!forma.isValid()) {
         return;   
        }
        let montoCheque = this.lookup('montoCheque').value;
        if (montoCheque == this.sumaTotal && montoCheque > 0) {    
                        this.confirmarProceso().then(ans => {
                if (ans === 'yes') {
                    this.procesarPago();
                }
            }); 
        }else{
            Ext.Msg.alert(
                'Importe Incorrecto',
                `El importe a pagar debe coincidir con el total a pagar`
            );
            return;
        }
    },
    procesarPago() {
        let me = this;
        let montoCheque = me.lookup('montoCheque').value;
        let storeConceptos = me.lookup('tablaEstadosDeCuenta').getStore();
        let dependenciaForma = this.lookup('movimientoManualDependenciaForma');
        let panel = this.lookup('movimientoManualPanel');
        Ext.getBody().mask('Realizando pago...');
        for (let i = 0; i < storeConceptos.data.items.length; i++) {
            let item = storeConceptos.data.items[i].data;
            item.bancoEmisor = me.bancoEmisor;
            item.cheque = me.numeroCheque;
            item.cuentaBancoId = me.cuentaBancoId;
            item.bancoId = me.bancoId;
            item.formaPagoId = me.formaPagoId;
            item.certificado = me.certificado;
        }

            storeConceptos.sync({
                success: function (batch, operations) {
                    Ext.getBody().unmask();
                    operations.operations.update.forEach(el => {
                        if(me.transaccionId != null){
                            return;
                        }
                        me.transaccionId = el.data.resultado.transaccionId;
                    
                    });

                    me.imprimirCertificado(me.certificado);

                    dependenciaForma.reset();
                    me.obtenerFolioCertificado();
                    storeConceptos.removeAll();
                    Ext.Msg.alert(
                        'Operación Exitosa',
                        `La operación se completo correctamente y se genero la transacción <b>${me.transaccionId}</b>`
                    );
                    return true;
                },
                failure: function (batch, operations) {
                    Ext.getBody().unmask();
                    storeConceptos.rejectChanges();
                    batch.exceptions.forEach(el => {
                        Ext.toast(el.error.response.statusText);
                    });

                    return false;
                }
            });

        
    },


    confirmarProceso() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Procesar Movimiento',
                `Se va a generar el certificado con el folio <b>${this.certificado}</b>. ¿Desea generar el pago?`,
                ans => resolve(ans)
            );
        });
    },

    onSeleccionAvisoCargo(selector, avisoCargoId) {
        let avisoCargo = selector.getSelection();
        if (avisoCargo) {
            let tablaConceptos = this.lookup('tablaEstadosDeCuenta');
            this.folio = avisoCargo.data.folio;

            tablaConceptos.getStore().load({
                params: {
                    avisoCargoId: avisoCargoId
                },
                callback: function () {
                    success: {
                        for (let i = 0; i < this.data.items.length; i++) {
                            const el = this.data.items[i];
                            el.set('porPagar', el.data.saldo);
                        }
                    }
                }
            });

            let saldo = Ext.util.Format.usMoney(avisoCargo.data.saldo);
            let fecha = Ext.util.Format.date(avisoCargo.data.fechaEmision, "d/m/Y");

            this.lookup('fechaEmisionAvisoCargo').setValue(fecha);
            this.lookup('saldoAvisoCargo').setValue(saldo);
        }
    },

    onSeleccionBanco(selector, bancoId) {
        this.bancoId = bancoId;
        this.lookup('cuentaBancoSelector').getStore().load({
            params: {
                bancoId
            }
        });
    },

    onSeleccionCuentaBanco(selector, cuentaBancoId) {
        this.cuentaBancoId = cuentaBancoId;
    },

    onDefineBancoEmisor(componente, bancoEmisor, oldValue, eOpts) {
        this.bancoEmisor = bancoEmisor;
    },

    onDefineCheque(componente, numeroCheque, oldValue, eOpts) {
        this.numeroCheque = numeroCheque;
    },

    onSeleccionDependencia(component, dependenciaId) {
        let selector = this.lookup('emisionAvisoCargoSelector');
        selector.getStore().getProxy().setExtraParams({
            dependenciaId: dependenciaId
        });
        selector.getStore().load();
    },

    onSeleccionFormaPago(selector, formaPagoId) {
        this.formaPagoId = formaPagoId;
    },

    validaImporte(val) {
        let edoCta = this.ownerCt.editingPlugin.estadoDeCuenta;
        let saldo = edoCta.get('saldo');
        if (val > saldo) {
            return `El importe a pagar no puede ser mayor a 
            ${Ext.util.Format.usMoney(saldo)}`;
        }
        return true;
    },

    onBeforeEdit(editor, context) {
        editor.estadoDeCuenta = context.record;
    },

    onValidateEdit(editor, context) {
        let saldo = Math.round(context.record.get('saldo')* 100) / 100;
        if (context.value > saldo) {
            Ext.Msg.alert(
                'Importe incorrecto',
                `El importe a pagar no puede ser mayor a ${Ext.util.Format.usMoney(saldo)}`
            );
            editor.cancelEdit();
            
        }
    },

    onSummary(value, summaryData, dataIndex, context) {
        let val = Math.round(value * 100) / 100;
        if (val !== undefined && val > 0) {
            this.sumaTotal = val
        }
        return `<b>Total a pagar: ${Ext.util.Format.usMoney(value)}</b>`;
    },

    imprimirCertificado(certificadoId) {
        window.open(`/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=IN_CERTIFICADO_INGRESOS_DEP`);
    }

});