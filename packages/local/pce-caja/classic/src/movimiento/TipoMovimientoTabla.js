Ext.define('Pce.caja.movimiento.TipoMovimientoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'tipo-movimiento-tabla',

    requires: [
        'Pce.caja.movimiento.TipoMovimientoTablaController',

        // 'Pce.ingresos.movimiento.TipoMovimientoStore'
    ],

    controller: 'tipo-movimiento-tabla',
    store: {
        type: 'tipos-movimiento',
        autoLoad: true
    },

    title: 'Catálogo de movimientos',
    tbar: [{
        text: 'Agregar',
        handler: 'onNuevoTipoMovimiento',
        tooltip: 'Ingrese un nuevo tipo de movimiento.'
    }],
    columns: [{
        text: 'Clave',
        dataIndex: 'clave',
        tooltip: 'Valor alfanúmerico único para identificar al movimiento.',
        flex: 1
    }, {
        text: 'Descripción',
        tooltip: 'Título corto del movimiento, sin abreviaturas.',
        dataIndex: 'descripcion',
        flex: 5
    }, {
        text: 'Estatus',
        tooltip: 'Estatus del movimiento',
        dataIndex: 'activo',
        flex: 1,
        renderer(v) {
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        }
    }, {
        text: 'Creado en',
        tooltip:'Fecha en que fue creado el concepto.',
        dataIndex: 'fechaCreacion',
        xtype: 'datecolumn',
        format: 'd/m/Y',
        flex: 1
    }, {
        text: 'Creado por',
        dataIndex: 'usuarioCreacion',
        flex: 1
    },{
        xtype: 'actioncolumn',
        width: 50,
        items: [{
            iconCls: 'x-fa fa-edit',
            handler: 'onEditarTipoMovimiento',
            tooltip: 'Editar el tipo de movimiento.',
        }, {
            iconCls: 'x-fa fa-trash',
            handler: 'onEliminarTipoMovimiento',
            tooltip: 'Eliminar tipo de movimiento.',
        }]
    }]
});