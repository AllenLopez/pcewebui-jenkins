Ext.define('Pce.caja.pagoAnticipadoDependencia.PagoAnticipadoDependenciaController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.pago-anticipado-dependencia-panel',
    requires: [
        'Pce.view.dialogo.FormaContenedor',
        // 'Pce.caja.pagoAnticipadoDependencia.PagoAnticipadoDependenciaForma'
    ],

    init: function(){
        this.obtenerFolioCertificado();
    },

    obtenerFolioCertificado(){
        Ext.Ajax.request({
            url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
            method: 'GET',
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
                let certificado =  this.lookup('forma').getForm().findField('certificado');
                certificado.setValue(res);
            }
        });
    },

    onProcesarMovimiento(){
        let forma = this.lookup('forma');
        if (!forma.isValid()) {
            return;
        }
        this.confirmarProceso(forma.getForm().findField('certificado').getValue()).then(ans => {
            if (ans === 'yes') {
                this.procesar(forma.getForm().getFieldValues(), forma);
            }
        });
    },

    procesar(data, forma) {       
        Ext.getBody().mask('Realizando pago...');
        Ext.Ajax.request({
            url: '/api/caja/pago-anticipado-dependencia',
            method: 'POST',
            jsonData:data,
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                if(res.success){
                    Ext.MessageBox.show({
                        title:'Operación completada',
                        msg: `La operación se completo correctamente y se genero la transacción <b>${res.transaccion.id}</b>`,
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.INFO,
                        fn:(btn) => {
                                forma.reset();
                        this.getView().getController().obtenerFolioCertificado();
                        this.getView().getController().generarReporte(data.certificado);
                        }
                    });
                    

                }else{

                }
            },
            failure(response) {
                let res = JSON.parse(response.responseText);
                Ext.getBody().unmask();
                Ext.Msg.alert(
                    'Ocurrió un error',
                    `La operación termino con errores`
                );
            }
        })
    },
    
    confirmarProceso(certificado) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Procesar Movimiento',
                `Se va a generar el certificado con el folio <b>${certificado}</b>. ¿Desea generar el pago?`,
                ans => resolve(ans)
            );
        });
    },

    onChangeBanco(comp, value, old){
         let cuenta =  this.lookup('forma').getForm().findField('cuenta');
         cuenta.reset();
         cuenta.getStore().porBanco(value);
    },

    onChangeDependencia(comp, value, old){
         let concepto =  this.lookup('forma').getForm().findField('concepto');
         concepto.reset();
         Ext.Ajax.request({
            url: '/api/ingresos/concepto/por-dependencia',
            method: 'GET',
            params: {
                dependenciaId: value
            },
            callback(op, success, response){
                if(success){
                    let resultado = Ext.decode(response.responseText);
                    concepto.getStore().setData(resultado);
                }
            }
        });
    },

    generarReporte(certificadoId){
        window.open(`/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=IN_CERTIFICADO_INGRESOS_DEP_PA`);
    }
});