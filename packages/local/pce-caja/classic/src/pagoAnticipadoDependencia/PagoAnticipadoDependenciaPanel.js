Ext.define('Pce.caja.PagoAnticipadoDependencia.PagoAnticipadoDependenciaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'pago-anticipado-dependencia-panel',

    requires: [
        'Pce.caja.cuentaBanco.PagoAnticipadoDependenciaForma',
        'Pce.caja.cuentaBanco.CuentaBancoSelector',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        //'Pce.ingresos.FormaPagoSelector',
       // 'Pce.ingresos.concepto.ConceptoSelector',
        'Pce.caja.pagoAnticipadoDependencia.PagoAnticipadoDependenciaController',
    ],

    viewModel: {
        data: {
            bancoId: null
        }
    },

    controller: 'pago-anticipado-dependencia-panel',
    title: 'Pago anticipado de dependencia sin aviso de cargo',
    layout: 'fit',
    items: [{
        xtype: 'pago-anticipado-dependencia-forma',
        reference: 'forma',
        padding: 10
    },],
    buttons: [{
        // iconCls: 'x-fa fa-flag-o',
        text: 'Procesar Movimiento',
        handler: 'onProcesarMovimiento',
    }]
});