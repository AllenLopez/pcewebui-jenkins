Ext.define('Pce.caja.cuentaBanco.PagoAnticipadoDependenciaForma',{
    extend: 'Ext.form.Panel',
    xtype: 'pago-anticipado-dependencia-forma',

    requieres: [
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox',
        'Ext.layout.container.Form',
    ],

    layout: {
        type: 'table',
        columns: 9
    },
    padding: 10,

    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'dependencia-selector',
        name: 'dependencia',
        colspan: 3,
        allowBlank: false,
        blankText: "Este campo es obligatorio",

        store: {
            type: 'dependencias',
            autoLoad: true,
            proxy: {
                type: 'rest',
                url: '/api/afiliacion/dependencia/por-aviso-cargo',
                writer: {
                    writeAllFields: true
                }
            },
            filters: [{
                property: 'activo',
                value: true
            }]
        },
        listeners: {
            change: 'onChangeDependencia'
        }
    },{
        xtype: 'concepto-selector',
        name: 'concepto',
        colspan: 3,
        allowBlank: false,
        blankText: "Este campo es obligatorio",
    
        store: {
            type: 'conceptos',
        }
    },{
        xtype: 'quincena-selector',
        reference: 'fecha',
        valueField: 'fecha',
        fieldLabel: 'Fecha nomina',
        allowBlank: false,
        name: 'fecha',
        colspan: 3,
        blankText: "Este campo es obligatorio",
        store: {
            type: 'quincena',
            autoLoad: true,
            proxy: {
                type: 'rest',
                url: '/api/ingresos/quincena/ejercicio-actual',
            },
        },
    }, {
        xtype: 'forma-pago-selector',
        name: 'formaPago',
        allowBlank: false,
        colspan: 3
    },{
        xtype: 'banco-selector',
        allowBlank: false,
        name: 'bancoEmisor',
        fieldLabel: 'Banco emisor',
        colspan: 3
    },{
        xtype: 'numberfield',
        fieldLabel: 'Num. referencia',
        name: 'chequeCuenta',
        allowBlank: false,
        colspan: 3,
    },{
        xtype: 'banco-selector',
        allowBlank: false,
        name: 'bancoReceptor',
        fieldLabel: 'Banco receptor',
        colspan: 3,
        listeners: {
            change: 'onChangeBanco'
        }
    },{
        xtype: 'cuenta-banco-selector',
        allowBlank: false,
        name: 'cuenta',
        colspan: 3
    },{
        xtype: 'numberfield',
        allowBlank: false,
        fieldLabel: 'Importe',
        name: 'importe',
        colspan: 3,
    },{
        xtype: 'numberfield',
        readOnly: true,
        fieldLabel: 'Certificado',
        name: 'certificado',
        colspan: 2,
        store:{
            proxy: {
                type: 'rest',
                url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
            },
            autoLoad: true
        },
        cls: 'read-only',
        fieldStyle: 'font-weight: bold',
        labelStyle: 'font-weight: bold'
    },]
});