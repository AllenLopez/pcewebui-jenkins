/**
 * @author Felipe Fierro
 * @desc Tabla especializada para mostrar periodos de antiguedad a pagar de una dependencia
 */
Ext.define('Pce.caja.antiguedad.dependencia.PeriodoDependenciaGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'reconocimiento-antiguedad-periodo-dependencia-grid',

    requires: [
        'Ext.grid.feature.Summary'
    ],

    title: 'Pagos pendientes',

    features: [{
        ftype: 'summary'
    }],
    tbar: [{
        iconCls: 'x-fa fa-money',
        text: 'Pagar',
        handler: 'onPagarPeriodosTap',
        tooltip:'Realizar pago de reconocimiento de antiguedad'
    }],
    columns: [{
        text: 'Periodo',
        dataIndex: 'periodo',
        tooltip:'Año en el que se autorizó el pago o realizo el pago el trabajador',
        flex: 2
    }, {
        text: 'Mes',
        dataIndex: 'mes',
        tooltip:'Mes en el que se autorizó el pago o realizo el pago el trabajador',
        flex: 2
    }, {
        text: 'No. afiliación',
        dataIndex: 'numAfiliacion',
        tooltip:'Numero de afiliación del que va realizar el pago la dependencia',
        flex: 2,
        summaryType: 'count',
        summaryRenderer() {
            return `<b  style="text-align: right" >Totales:</b>`;
        }
    }, {
        text: 'Total a pagar',
        dataIndex: 'cantPagar',
        flex: 2,
        tooltip:'Importe de total a pagar',
        renderer: Ext.util.Format.usMoney,
        summaryType: 'sum',
        summaryRenderer(value) {
            return `<b>${Ext.util.Format.usMoney(value)}</b>`;
        }
    },]
});