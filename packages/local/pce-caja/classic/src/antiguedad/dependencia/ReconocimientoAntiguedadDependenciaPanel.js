/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.antiguedad.depenencia.ReconocimientoAntiguedadDependenciaPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reconocimiento-antiguedad-dependencia-panel',

    requires: [
        'Pce.caja.antiguedad.dependencia.ReconocimientoAntiguedadDependenciaController',
        'Pce.afiliacion.antiguedad.PeriodoDependenciaStore'
    ],

    controller: 'reconocimiento-antiguedad-dependencia',
    viewModel: {
        data: {
            dependencia: null
        },
        stores: {
            periodos: {
                type: 'antiguedad-dependencia-periodos'
            }
        }
    },

    title: 'Reconocimiento de antiguedad - Pago por dependencia',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    tbar: [
        {
        xtype: 'dependencia-selector',
        labelAlign: 'left',
        labelSeparator: ':',
        width: '50%',
        bind: {
            selection: '{dependencia}'
        }
    }, ' ', {
        iconCls: 'x-fa fa-calendar',
        text: 'Mostrar periodos',
        handler: 'onMostrarPeriodosTap',
        bind: {
            disabled: '{!dependencia}'
        },
    }
],
    items: [{
        xtype: 'form',
        references: 'forma',
        padding: 10,
        layout: {
            type: 'table',
            columns: 9
        },
        defaults: {
            width: '100%'
        },
        items: [{
            xtype: 'forma-pago-selector',
            name: 'formaPago',
            allowBlank: false,
            colspan: 3
        },{
            xtype: 'banco-selector',
            allowBlank: false,
            name: 'bancoEmisor',
            valueField: 'bancoNombre',
            fieldLabel: 'Banco emisor',
            colspan: 3
        },{
            xtype: 'numberfield',
            fieldLabel: 'Num. referencia',
            name: 'referencia',
            allowBlank: false,
            colspan: 3,
        },{
            xtype: 'banco-selector',
            allowBlank: false,
            name: 'bancoReceptor',
            fieldLabel: 'Banco receptor',
            colspan: 3,
            listeners: {
                change: 'onChangeBanco'
            }
        },{
            xtype: 'cuenta-banco-selector',
            allowBlank: false,
            name: 'cuentaBanco',
            colspan: 3
        },{
            xtype: 'numberfield',
            readOnly: true,
            fieldLabel: 'Certificado',
            name: 'certificado',
            colspan: 2,
            store:{
                proxy: {
                    type: 'rest',
                    url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
                },
                autoLoad: true
            },
            cls: 'read-only',
            fieldStyle: 'font-weight: bold',
            labelStyle: 'font-weight: bold'
        }, ]
    },{
        xtype: 'reconocimiento-antiguedad-periodo-dependencia-grid',
            emptyText: 'No se encontraron periodos de reconocimiento de ' +
                'antiguedad para la dependencia seleccionada',
            flex: 1,
            scrollable: true,
            bind: {
                title: '{dependencia.descripcion}: Pagos pendientes',
                store: '{periodos}',
                disabled: '{!dependencia}'
            },
            viewConfig: {
                deferEmptyText: true
            }
    }]
});