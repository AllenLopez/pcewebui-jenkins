/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.antiguedad.dependencia.ReconocimientoAntiguedadDependenciaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reconocimiento-antiguedad-dependencia',

    requires: [
        'Pce.caja.antiguedad.PagoServicio',
        'Ext.data.writer.Writer',
        'Pce.reporte.ReporteGenerador'
    ],

    init: function(){
        this.obtenerFolioCertificado();
    },

    obtenerFolioCertificado(){
        Ext.Ajax.request({
            url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
            method: 'GET',
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
                let certificado =  this.getView().items.items[0].getForm().findField('certificado');
                certificado.setValue(res);
            }
        });
    },

    onMostrarPeriodosTap() {
        let viewModel = this.getViewModel();
        let store = viewModel.get('periodos');
        let dependencia = viewModel.get('dependencia');
        store.load({
            params: {
                dependenciaId: dependencia.getId()
            }
        });
    },

    onPagarPeriodosTap() {
        let form = this.getView().down('form');
        let store = this.getViewModel().get('periodos');
        let dependencia = this.getViewModel().getData().dependencia.id;
        let excedeFechaLimite = false;
        if(store.data.items.length == 0){
            Ext.Msg.alert('Error', 'No hay periodos para realizar el pago');
            return;
        }
        if(!form.isValid()){
            
            return;
        }
        let datos = form.getValues();
        Ext.Msg.confirm(
            'Pago de reconocimiento de antigüedad',
            `Se va a generar el certificado con el folio <b>${datos.certificado}</b>. ¿Desea generar el pago?`,
            ans => {
                if (ans === 'yes') {
                    Ext.getBody().mask('Procesando.....');
                    this.efectuarPago(store.data.items, dependencia);
                }
            }
        );
    },

    efectuarPago(periodos, dependencia) {
        let form = this.getView().down('form');
        let datos = form.getValues();
        datos.dependencia = dependencia;
        this.getServicioPago().pagarDependencia(periodos, datos).then(result => {
            if (result.exito) {
                Ext.Msg.alert(
                    'Operación exitosa',
                    `La operación se completo correctamente y se genero la transacción  <b>${result.informacion.transaccion}</b>`,
                    () => {
                        Ext.getBody().unmask();
                        form.reset();
                        this.getViewModel().get('periodos').reload();
                        this.obtenerFolioCertificado();
                        this.generarReporteCertAntiguedad(result.informacion.certificado);
                        // this.getGeneradorReporte().generar('RANT_1004', {
                        //     P_NUM_ANTIGUEDAD: result.antiguedad,
                        //     P_CERTIFICADO: result.certificado
                        // });
                    }
                );
            } else {
                Ext.getBody().unmask();
                form.reset();
                Ext.Msg.alert('Error', result.mensaje);
            }
        }).catch(() => {
            Ext.Msg.alert('Error', 'La operación no pudo ser efectuada');
        });
    },

    onChangeBanco(comp, value, old){
        let form = this.getView().items.items[0];
         let cuenta =  form.getForm().findField('cuentaBanco');
         cuenta.reset();
         cuenta.getStore().porBanco(value);
    },

    /**
     * @returns {Pce.ingresos.antiguedad.PagoServicio}
     */
    getServicioPago() {
        if (!this.servicioPago) {
            this.servicioPago = new Pce.ingresos.antiguedad.PagoServicio();
        }
        return this.servicioPago;
    },

    /**
     * @returns {Pce.reporte.ReporteGenerador}
     */
    getGeneradorReporte() {
        if (!this.generadorReporte) {
            this.generadorReporte = new Pce.reporte.ReporteGenerador();
        }
        return this.generadorReporte;
    },

    generarReporteCertAntiguedad(certificadoId){
        window.open(`/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=IN_CERTIFICADO_INGRESOS_DEP_RA`);
    }
});