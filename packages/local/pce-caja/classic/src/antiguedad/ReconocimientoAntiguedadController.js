/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.antiguedad.ReconocimientoAntiguedadController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reconocimiento-antiguedad',

    requires: [
        'Pce.caja.antiguedad.PagoServicio',
        'Ext.data.writer.Writer',
        'Pce.reporte.ReporteGenerador'
    ],
    
    totalPagar: 0,
    totalRetiro: 0,
    total: 0,
    contador: 0,

    init: function(){
        this.obtenerFolioCertificado();
    },

    obtenerFolioCertificado(){
        Ext.Ajax.request({
            url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
            method: 'GET',
            scope: this, 
            success(response) {
                let res = JSON.parse(response.responseText);
                let form = this.getView().items.items[0];
                let certificado =  form.getForm().findField('certificado');
                certificado.setValue(res);
            }
        });
    },

    onMostrarPeriodosTap() {
        this.contador = 0;
        this.total = 0;
        let viewModel = this.getViewModel();
        let store = viewModel.get('periodos');
        let asegurado = viewModel.get('asegurado');
        store.load({
            params: {
                numAfil: asegurado.getId()
            }
        });
    },
    onPagarPeriodosTap() {
        let form = this.getView().down('form');
        if(!form.isValid()){
            return;
        }
        let store = this.getViewModel().get('periodos');
        let excedeFechaLimite = false;
        store.each(periodo => {
            let fechaPeriodo = periodo.get('fechaLimitePago');
            let fecha = new Date();
            let fechaActual = new Date(fecha.setHours(0,0,0,0));

            if (fechaPeriodo < fechaActual) {
                excedeFechaLimite = true;
                return false;
            }
        });

        if (excedeFechaLimite) {
            Ext.Msg.alert(
                'Pago no permitido',
                'No es posible realizar el pago debido a que la fecha ' +
                'límite de pago de uno o más periodos ha vencido'
            );
            return;
        }
        let datos = this.getView().down('form').getValues();
        Ext.Msg.confirm(
            'Pago de reconocimiento de antigüedad',
            `Se va a generar el certificado con el folio <b>${datos.certificado}</b>. ¿Desea generar el pago?`,
            ans => {
                if (ans === 'yes') {
                    this.efectuarPago(store.data.items);
                }
            }
        );
    },

    efectuarPago(periodos) {
        let form = this.getView().down('form');
        let datos = form.getValues();
        this.getServicioPago().pagar(periodos, datos).then(result => {
            if (result.exito) {
                Ext.Msg.alert(
                    'Operación exitosa',
                    `La operación se completo correctamente y se genero la transacción <b>${result.transaccion.id}</b>`,
                    () => {
                        this.getViewModel().get('periodos').reload();
                        this.imprimirCertificado(result.certificado);
                        this.obtenerFolioCertificado();
                        form.reset();
                        this.lookup('btnPagar').setText('Pagar');
                        this.lookup('btnPagar').setDisabled(true);
                        this.getViewModel().set('asegurado', null);
                    }
                );
            } else {
                Ext.Msg.alert('Error', result.message);
            }
        }).catch(() => {
            Ext.Msg.alert('Error', 'La operación no pudo ser efectuada');
        });
    },


    imprimirCertificado(certificadoId) {
        window.open(`/api/ingresos/certificado/reporte?certificadoId=${certificadoId}&reporte=IN_CERTIFICADO_INGRESOS_AFIL_RA`);
    },

    onChangeBanco(comp, value){
        let form = this.getView().items.items[0];
         let cuenta =  form.getForm().findField('cuentaBanco');
         cuenta.reset();
         cuenta.getStore().porBanco(value);
    },

    onSummaryTotalPagar(value){
        this.total += value;
        this.contador++;
        this.totalPagar += value;
        return `<b>${Ext.util.Format.usMoney(value)}</b>`;
    },

    onSummaryTotalRetiro(value){
        this.contador++;
        if(this.contador <= 2){
            
            this.total += value;
            
        }
        let btnPagar = this.lookup('btnPagar');
            if(this.total === 0){
                btnPagar.setDisabled(true);
                btnPagar.setText('Pagar');
            }else{
                btnPagar.setDisabled(false);
                btnPagar.setText(`Pagar ${Ext.util.Format.usMoney(this.total)}`);

            }
        this.totalRetiro += value;
        return `<b>${Ext.util.Format.usMoney(value)}</b>`;
    },

    onChangeAsegurado(){
        if(this.getViewModel().get('periodos')){

            this.getViewModel().get('periodos').removeAll();
        }
        this.total = 0;
        this.lookup('btnPagar').setText('Pagar');
        this.lookup('btnPagar').setDisabled(true);
    },

    /**
     * @returns {Pce.ingresos.antiguedad.PagoServicio}
     */
    getServicioPago() {
        if (!this.servicioPago) {
            this.servicioPago = new Pce.ingresos.antiguedad.PagoServicio();
        }
        return this.servicioPago;
    },

    /**
     * @returns {Pce.reporte.ReporteGenerador}
     */
    getGeneradorReporte() {
        if (!this.generadorReporte) {
            this.generadorReporte = new Pce.reporte.ReporteGenerador();
        }
        return this.generadorReporte;
    }
});