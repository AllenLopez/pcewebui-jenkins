/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.antiguedad.ReconocimientoAntiguedadPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reconocimiento-antiguedad-panel',

    requires: [
        'Pce.caja.antiguedad.ReconocimientoAntiguedadController',
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.afiliacion.antiguedad.PeriodoStore',
        'Pce.caja.antiguedad.PeriodoGrid',
        'Pce.caja.banco.BancoSelector'
    ],

    controller: 'reconocimiento-antiguedad',
    viewModel: {
        data: {
            asegurado: null
        },
        stores: {
            periodos: {
                type: 'antiguedad-periodos'
            }
        }
    },

    title: 'Reconocimiento de antiguedad - Pago por afiliado',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    tbar: [{
        xtype: 'asegurado-selector',
        labelAlign: 'left',
        labelSeparator: ':',
        width: '50%',
        bind: {
            selection: '{asegurado}'
        },
        listeners: {
            change: 'onChangeAsegurado'
        }
    }, ' ', {
        iconCls: 'x-fa fa-calendar',
        text: 'Mostrar periodos',
        handler: 'onMostrarPeriodosTap'
    }],
    items: [{
        xtype: 'form',
        padding: 10,
        layout: {
            type: 'table',
            columns: 3
        },
        defaults: {
            width: '100%'
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Dirección',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.direccion}'
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'Colonia',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.colonia}'
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'Código postal',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.codigoPostal}'
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'Teléfono',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.telefono2}'
            }
        },{
            xtype: 'forma-pago-selector',
            name: 'formaPago',
            allowBlank: false,
        },{
            xtype: 'banco-selector',
            allowBlank: false,
            name: 'bancoEmisor',
            fieldLabel: 'Banco emisor',
            valueField: 'bancoNombre',
            displayField: 'bancoNombre',
        },{
            xtype: 'numberfield',
            fieldLabel: 'Num. referencia',
            name: 'referencia',
            allowBlank: false,
        },{
            xtype: 'banco-selector',
            allowBlank: false,
            name: 'bancoReceptor',
            fieldLabel: 'Banco receptor',
            listeners: {
                change: 'onChangeBanco'
            }
        },{
            xtype: 'cuenta-banco-selector',
            allowBlank: false,
            name: 'cuentaBanco',
        },{
            xtype: 'numberfield',
            readOnly: true,
            fieldLabel: 'Certificado',
            name: 'certificado',
            store:{
                proxy: {
                    type: 'rest',
                    url: '/api/caja/pago-anticipado-dependencia/folio-certificado',
                },
                autoLoad: true
            },
            cls: 'read-only',
            fieldStyle: 'font-weight: bold',
            labelStyle: 'font-weight: bold'
        },]
    }, {
        xtype: 'reconocimiento-antiguedad-periodo-grid',
        emptyText: 'No se encontraron periodos de reconocimiento de ' +
            'antiguedad para el asegurado seleccionado',
        flex: 1,
        bind: {
            title: '{asegurado.nombreCompleto}: Periodos de reconocimiento',
            store: '{periodos}',
            disabled: '{!asegurado}'
        },
        viewConfig: {
            deferEmptyText: true
        }
    }]
});