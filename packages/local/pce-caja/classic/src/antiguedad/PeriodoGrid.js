/**
 * @author Rubén Maguregui
 * @desc Tabla especializada para mostrar periodos de antiguedad a pagar
 */
Ext.define('Pce.caja.antiguedad.PeriodoGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'reconocimiento-antiguedad-periodo-grid',

    requires: [
        'Ext.grid.feature.Summary'
    ],

    title: 'Periodos',

    features: [{
        ftype: 'summary'
    }],
    buttons: [{
        text:'Pagar',
        iconCls: 'x-fa fa-money',
        reference: 'btnPagar',
        handler: 'onPagarPeriodosTap',
        tooltip:'Realizar pago de reconocimiento de antiguedad'
    }], 
    columns: [{
        text: 'Dependencia',
        dataIndex: 'dependenciaNombre',
        tooltip:'Dependencia del asegurado',
        flex: 2
    }, {
        xtype: 'datecolumn',
        text: 'Periodo desde',
        dataIndex: 'desde',
        tooltip:'Periodo desde',
        format: 'd/m/Y',
        flex: 1
    }, {
        xtype: 'datecolumn',
        text: 'Periodo hasta',
        dataIndex: 'hasta',
        tooltip:'Periodo hasta',
        format: 'd/m/Y',
        flex: 1,
        summaryType: 'count',
        summaryRenderer() {
            return `<b>Totales:</b>`;
        }
    }, {
        text: 'Aportación',
        dataIndex: 'aportacion',
        flex: 1,
        tooltip:'Importe de aportación',
        renderer: Ext.util.Format.usMoney,
        summaryType: 'sum',
        summaryRenderer(value) {
            return `<b>${Ext.util.Format.usMoney(value)}</b>`;
        }
    }, {
        text: 'Aportación patrón',
        dataIndex: 'aportacionPatron',
        flex: 1,
        tooltip:'Importe de aportación del patrón',
        renderer: Ext.util.Format.usMoney,
        summaryType: 'sum',
        summaryRenderer(value) {
            return `<b>${Ext.util.Format.usMoney(value)}</b>`;
        }
    }, {
        text: 'Subsidio',
        dataIndex: 'subsidio',
        flex: 1,
        tooltip:'Importe de subsidio',
        renderer: Ext.util.Format.usMoney,
        summaryType: 'sum',
        summaryRenderer(value) {
            return `<b>${Ext.util.Format.usMoney(value)}</b>`;
        }
    },{
        text: 'Retiro actualizado',
        dataIndex: 'retiroActualizado',
        flex: 1,
        tooltip:'Importe de retiro actualizado',
        renderer: Ext.util.Format.usMoney,
        summaryType: 'sum',
        summaryRenderer: 'onSummaryTotalRetiro'
    }, {
        text: 'Total a pagar',
        dataIndex: 'total',
        flex: 1,
        tooltip:'Importe de total a pagar',
        renderer: Ext.util.Format.usMoney,
        summaryType: 'sum',
        summaryRenderer: 'onSummaryTotalPagar'
    }, {
        xtype: 'datecolumn',
        text: 'Fecha límite de pago',
        dataIndex: 'fechaLimitePago',
        tooltip:'Fecha limite de pago',
        format: 'd/m/Y',
        flex: 1
    }]
});