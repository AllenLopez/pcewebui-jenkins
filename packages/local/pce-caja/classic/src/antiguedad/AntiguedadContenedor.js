Ext.define('Pce.caja.antiguedad.AntiguedadContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'antiguedad-contenedor-caja',

    layout: 'fit',

    requires: [
        'Pce.caja.antiguedad.ReconocimientoAntiguedadPanel',
        'Pce.caja.pagoLicencias.PagoLicenciasPanel',
        'Pce.caja.antiguedad.depenencia.ReconocimientoAntiguedadDependenciaPanel',
        'Pce.caja.PagoAnticipadoDependencia.PagoAnticipadoDependenciaPanel',
        'Pce.caja.movimiento.MovimientoManualPanel'
    ],

    mapaPagos: {
        'pagos': 'movimiento-manual-panel',
        'reconocimiento-antiguedad-afiliado': 'reconocimiento-antiguedad-panel',
        'reconocimiento-antiguedad-dependencia': 'reconocimiento-antiguedad-dependencia-panel',
        'pago-licencias': 'pago-licencias-panel',
        'pago-anticipado-dependencia': 'pago-anticipado-dependencia-panel'
    },

    pago: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.pago) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaPagos[this.pago]
            });
        }
    }
});