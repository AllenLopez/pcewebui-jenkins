
Ext.define('Pce.caja.banco.BancoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'banco-tabla',
    requires: [
        'Pce.caja.banco.BancoStore'
    ],

    store: {
        type: 'banco',
        autoLoad: true
    }, 
    columns: [{
        text: 'Banco', 
        dataIndex: 'bancoNombre',
        flex: 1
    },{
        text: 'Descripción', 
        dataIndex: 'descripcion',
        flex: 3
    },{
        text: 'Clave SAT', 
        dataIndex: 'claveBancoSat',
        flex: 1
    },{
        text: 'Activo', 
        dataIndex: 'activo',
        flex: 1,
        renderer(v) {
            let cls = v ? 'x-fa fa-check': 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        }
    }, {
        text: 'Creado en',
        dataIndex: 'fechaCreacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
        flex: 1
    },{
        text: 'Creado por', 
        dataIndex: 'usuarioCreacion',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        flex: 3,
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar Banco',
            handler: 'onEditarBanco'
        }, {
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar Banco',
            handler: 'onEliminarBanco'
        }]
    }]
});