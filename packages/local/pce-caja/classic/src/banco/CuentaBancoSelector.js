/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.banco.CuentaBancoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'banco-cuenta-selector',

    requires: [
        'Pce.caja.banco.CuentaBancoStore'
    ],

    fieldLabel: 'Cuenta bancaria',
    valueField: 'id',
    displayField: 'numeroCuenta',
    queryMode: 'local',
    forceSelection: true,

    store: {
        type: 'cuenta-banco'
    }
});