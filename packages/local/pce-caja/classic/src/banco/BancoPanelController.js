Ext.define('Pce.caja.banco.BancoPanelController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.banco-panel',
    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.caja.banco.BancoForma'
    ],

    onAgregarBanco(){
        this.mostrarDialogoEdicion(new Pce.caja.banco.Banco());
    },

    onGuardarBanco(dialogo, banco){
        let me = this;
        let grid = me.lookup('bancoTabla');
        let form = dialogo.down('banco-forma').getForm();
        let activo = form.findField('activo').getValue();

        banco.data.activo = activo;
        
        Ext.Msg.confirm(
            'Guardar banco',
            `¿Seguro que deseas guardar este banco 
            <i>${banco.get('bancoNombre')}</i>`,
            ans => {
                if(ans === 'yes') {
                    banco.save({
                        success() {
                            grid.getStore().reload();
                            Ext.toast('Los datos se han guardado correctamente');
                            dialogo.close();
                        }
                    });          
                }
            }
        );
    },

    onEditarBanco(tabla, i, j, item, e, banco){
        this.mostrarDialogoEdicion(banco);
    },

    onEliminarBanco(tabla, i, j, item, e, banco){
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar este banco? 
            <i>${banco.get('bancoNombre')}</i>`,
            ans => {
                if(ans === 'yes'){
                    banco.erase({
                        success(){
                            Ext.toast('El banco seleccionado se ha eliminado ');
                        }
                    });
                }
            }
        );
    },

    mostrarDialogoEdicion(banco){
        let title = banco.phantom ? 'Nuevo banco'
        : 'Editar banco';

        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title,
            record: banco,
            width: 400,
            items: {
                xtype: 'banco-forma'
            },
            listeners: {
                guardar: this.onGuardarBanco.bind(this)
            }
        });
    }
});