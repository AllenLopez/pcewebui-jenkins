/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.banco.BancoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'banco-selector',

    requires: [
        'Pce.caja.banco.BancoStore'
    ],

    fieldLabel: 'Banco',
    valueField: 'id',
    displayField: 'bancoNombre',
    queryMode: 'local',
    forceSelection: true,

    store: {
        type: 'banco',
        autoLoad: true
    }
});