Ext.define('Pce.caja.banco.BancoForma',{
    extend: 'Ext.form.Panel',
    xtype: 'banco-forma',

    requieres: [
        'Ext.layout.container.Form',
      'Ext.form.field.Text',
      'Ext.form.field.Checkbox'
    ],

    layout: 'form',

    items: [{
        xtype: 'textfield',
        name: 'bancoNombre',
        fieldLabel: 'Banco',
        allowBlank: false
    }, {
        xtype: 'checkbox',
        name: 'activo',
        checked: true,
        fieldLabel: 'Activo'
    },{
        xtype: 'textfield',
        name: 'descripcion',
        fieldLabel: 'Descripción',
        allowBlank: false
    },{
        xtype: 'textfield',
        name: 'claveBancoSat',
        fieldLabel: 'Clave SAT',
        allowBlank: false
    }]
});