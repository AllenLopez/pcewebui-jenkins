Ext.define('Pce.caja.banco.BancoPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'banco-panel',

    requires: [
        'Pce.caja.banco.BancoPanelController',
        'Pce.caja.banco.BancoTabla'
    ],
    controller: 'banco-panel',
    title: 'Catálogo de bancos',
    layout: 'fit',
    items: [{
        xtype: 'banco-tabla', 
        reference: 'bancoTabla',
        emptyText: 'no existen registros',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Agregar',
                handler: 'onAgregarBanco'
            }]
        }]
    }]
});