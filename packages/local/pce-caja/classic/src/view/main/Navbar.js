/**
 * Barra de navegación para el módulo de caja
 */
Ext.define('Pce.caja.view.main.Navbar', {
    extend: 'Ext.container.Container',
    xtype: 'navbar-caja',
    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Split',
        'Ext.button.Segmented',
        'Ext.menu.Menu'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'component',
        cls: 'site-bar',
        flex: 1
    }, {
        xtype: 'toolbar',
        items: {
            xtype: 'segmentedbutton',
            reference: 'menu',
            items: [  ]
        }
    }]
});