/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Pce.caja.view.main.Main', {
    extend: 'Pce.view.main.Main',
    xtype: 'caja-main',

    requires: [
        'Pce.caja.view.main.MainController',

        // Barra de navegación para el módulo de caja
        'Pce.caja.view.main.Navbar'
    ],

    controller: 'caja-main',

    dockedItems: [{
        //Barra de navegación 
        xtype: 'navbar-caja',
        dock: 'top'
    }]
});
