Ext.define('Pce.caja.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.caja-main',

    requires: [
        'Pce.caja.Inicio',
        'Pce.caja.facturacion.ComplementoFactura',
        'Pce.caja.antiguedad.AntiguedadContenedor',
        'Pce.caja.poliza.PolizaContenedor',
        'Pce.caja.reportes.ReportesContenedor',
        'Pce.caja.procesos.ProcesosContenedor',
        'Pce.caja.cheques.ChequesContenedor',
        'Pce.caja.pagos.PagoAvisoCargoMultiplePanel',
        'Pce.caja.prestamos.PagoPrestamoAfiliadoPanel',
        'Pce.caja.prestamos.EmisionChequePagarePrestamosPanel',
        'Pce.caja.prestamos.EmisionChequeDescIndebPrestamosPanel',
        'Pce.caja.prestamos.ConsultaChequePagarePrestamosPanel',
        'Pce.caja.prestamos.ConsultaChequeDescuentoIndebidoPrestamosPanel'
    ],

    routes: {
        // 'main': 'main',
        'catalogo/:catalogo': 'catalogo',
        'facturacion/complemento': 'complementoFactura',
        'proceso/:proceso': 'proceso',
        'pagos/:pago': 'pagos',
        'pagos/pago-aviso-cargo-multiple': 'pagosAvisoCargoMultiple',
        'pagos/pago-prestamos-afiliados': 'pagosPrestamoAfiliado',
        'poliza/:poliza': 'poliza',
        'reporte/:reporte': 'reporte',
        'cheques/:cheque':'cheque',
        'cheques/emision-cheque-pagare-prestamos' : 'emisionChequePagarePrestamos',
        'cheques/consulta-cheque-pagare-prestamos' : 'consultaChequePagarePrestamos',
        'cheques/emision-cheque-descuentoindebido-prestamos' : 'emisionChequeDescuentoIndebidoPrestamos',
        'cheques/consulta-cheque-descuentoindebido-prestamos' : 'consultaChequeDescuentoIndebidoPrestamos',
        'cerrar-sesion': 'cerrarSesion'
    },
     beforeRender: function(){
         let me = this;
        Ext.Ajax.request({
            url: '/api/seguridad/autenticacion/estatus?host=/apps/Caja/index.html',
            method: 'GET',
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                if(resultado.estatus === ''){
                    window.location = '/apps/Seguridad/index.html#login';
                    return;
                }
                if(window.location.pathname !== resultado.redireccion){
                    window.location = resultado.redireccion;
                    return;
                }
                let menu = me.lookup('menu');
               
               menu.add(resultado.menu);
            }
        });
            
            
         },
    main() {
        this.view.renderPage({
            xtype: 'inicio'
        });
    },

    complementoFactura() {
        this.view.renderPage({
            xtype: 'carga-complemento-factura'
        });
    },

    cerrarSesion() {
        Ext.Ajax.request({
            url: '/api/seguridad/autenticacion/cerrar-sesion',
            method: 'GET',
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                if(resultado.estatus === 'anonimo'){
                    window.location = '/apps/Seguridad/index.html#login';
                    return;
                }
            }
        });
    },

    catalogo(catalogo) {
        this.view.renderPage({
            xtype: 'catalogo-contenedor-caja',
            catalogo: catalogo
        });
    },
    

    poliza(poliza) {
        this.view.renderPage({
            xtype: 'poliza-contenedor-caja',
            poliza
        });
    },
    

    reporte(reporte) {
        this.view.renderPage({
            xtype: 'reportes-contenedor',
            reporte
        });
    },

    proceso(proceso) {
        this.view.renderPage({
            xtype: 'procesos-contenedor',
            proceso
        });
    },

    cheque(cheque) {
        this.view.renderPage({
            xtype: 'cheques-contenedor',
            cheque
        });
    },

    pagos(pago) {
        this.view.renderPage({
            xtype: 'antiguedad-contenedor-caja',
            pago
        });
    },

    pagosAvisoCargoMultiple() {
        this.view.renderPage({
            xtype: 'pago-aviso-cargo-multiple-panel'
        });
    },

    pagosPrestamoAfiliado(){
        this.view.renderPage({
            xtype: 'pago-prestamo-afiliado-panel'
        });
    },

    emisionChequePagarePrestamos(){
        this.view.renderPage({
            xtype: 'emision-cheque-pagare-prestamos-panel'
        });
    },
    consultaChequePagarePrestamos(){
        this.view.renderPage({
            xtype: 'consulta-cheque-pagare-prestamos-panel'
        });
    },

    emisionChequeDescuentoIndebidoPrestamos(){
        this.view.renderPage({
            xtype: 'emision-cheque-descindeb-prestamos-panel'
        });
    },

    consultaChequeDescuentoIndebidoPrestamos(){
        this.view.renderPage({
            xtype: 'consulta-cheque-descindeb-prestamos-panel'
        }); 
    },

});