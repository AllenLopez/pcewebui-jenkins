Ext.define('Pce.caja.cheques.ChequesContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'cheques-contenedor',

    layout: 'fit',

    requires: [
        'Pce.caja.cheques.emisionCheque.EmisionChequePanel',
        'Pce.caja.cheques.consultaCheque.ConsultaChequePanel',
    ],

    mapaCheques: {
        'emision': 'emision-cheque-panel',
        'consulta': 'consulta-cheque-panel'
    },

    cheque: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.cheque) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaCheques[this.cheque]
            });
        }
    }
});