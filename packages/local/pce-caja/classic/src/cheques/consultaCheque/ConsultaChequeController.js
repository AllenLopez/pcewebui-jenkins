Ext.define('Pce.caja.cheques.consultaCheque.ConsultaChequeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-cheque-panel',
    requires: [

    ],

    fechaInicio: undefined,
    fechaFin: undefined,
    numCheque: undefined,
    beneficiario: undefined,

    init: function() {
        
        this.resetStore();
    },

    resetStore(){
        let store = this.lookup('chequesTabla').getStore()
        store.removeAll();
    },

    onSeleccionaFechaInicial(control, seleccion) {
        if(seleccion == ""){
            this.fechaInicio = null;
            return;
        }
        this.fechaInicio = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    onSeleccionaFechaFinal(control, seleccion) {
        if(seleccion == ""){
            this.fechaFin = null;
            return;
        }
        this.fechaFin = Ext.util.Format.date(seleccion, 'd/m/Y');
        
    },

    onBuscarCheques(){
        if(
            this.numCheque == null 
            && (this.beneficiario == null || this.beneficiario == "") 
            && (this.fechaInicio == "" || this.fechaFin == "")
        ){
            return;
        }
        this.cargaTablaCheques(this.fechaInicio, this.fechaFin, this.numCheque, this.beneficiario);
    },
    
    onChangeNumCheque(comp, valor) {
        this.numCheque = valor;
    },
    
    onChangeBeneficiario(comp, valor) {
        this.beneficiario = valor.toUpperCase();
    },



    cargaTablaCheques(fechaInicial, fechaFinal, numCheque, beneficiario) {
        let store = this.lookup('chequesTabla').getStore();
        store.setProxy({
            type: 'rest',
            url: '/api/cajas/chequera/cheque-solicitud/impresos',
            writer: {
                writeAllFields: true
            }
        });
        this.lookup('chequesTabla').getStore().load({
            params: {
                fechaInicial,
                fechaFinal,
                numCheque,
                beneficiario
            } 
        });
    },

    onImprimirCheque(tabla, i, j, item, e, cheque){
        console.log("chequeImprimir", cheque);
        if(cheque.get("estatus") != "IM"){
            Ext.Msg.alert(
                'Advertencia',
                'No es posible imprimir el cheque ya que no se encuentra en estado "IMPRESO"'
            );
            return;
        }
        console.log("Imprimir", cheque.id, cheque.get('reporteCheque'));
        window.open(`/api/cajas/chequera/cheque-solicitud/descargar?idSolCheque=${cheque.id}&reporte=${cheque.get('reporteCheque')}`);
    }
});

//PIDSOLCHEQUE   -- cambiar status a IM