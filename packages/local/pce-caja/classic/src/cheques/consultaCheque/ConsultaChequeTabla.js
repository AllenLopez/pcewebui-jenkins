Ext.define('Pce.caja.cheques.consultaCheque.ConsultaChequeTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'consulta-cheque-tabla',

    columns: [{
        text: 'Núm. cheque',
        dataIndex: 'numCheque',
        tooltip:'Numero de cheque',
        flex: 1
    },{
        text: 'Importe',
        dataIndex: 'importe',
        tooltip:'Muestra el importe que se devolverá al asegurado.',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        flex: 1
    },{
        text: 'Beneficiario',
        dataIndex: 'beneficiario',
        tooltip:'Nombre que corresponde la emisión del cheque',
        flex: 2
    }, {
        text: 'Dependencia',
        dataIndex: 'dependencia',
        tooltip:'Nombre de la dependencia que corresponde la emisión del cheque',
        flex: 2
    },{
        text: 'Fecha cheque',
        dataIndex: 'fechaAfectacion',
        tooltip:'Fecha en la que se hizo la afectación de la cartera',
        flex: 1,
        renderer(v){
            return Ext.util.Format.date(v, 'd/m/Y');
        },
    }, {
        text: 'Etapa',
        dataIndex: 'estatus',
        tooltip:'Etapa en la que se encuentra el cheque',
        flex: 1,
        renderer(v){
                switch (v) {
                    case 'TR': return 'EN TRÁMITE';
                    case 'AU': return 'AUTORIZADO';
                    case 'CP': return 'COMPLETADO';
                    case 'IM': return 'IMPRESO';
                    case 'CA': return 'CANCELADO';
                    default: return 'OTRA';
                }
        }
    },{
        text: 'Tipo',
        dataIndex: 'tipoMovimiento',
        tooltip:'Descripción del movimiento',
        flex: 2
    }, {
        text: 'Banco',
        dataIndex: 'banco',
        tooltip:'Banco de emisión del cheque',
        flex: 2
    },{
        xtype: 'actioncolumn',
        flex: 1,
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-print',
            tooltip: 'Imprimir cheque',
            handler: 'onImprimirCheque'
        }]
    }]
});