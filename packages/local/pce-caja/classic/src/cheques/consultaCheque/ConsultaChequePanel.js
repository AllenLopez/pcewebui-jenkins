/**
 * @class Pce.caja.cheques.consultaCheque.ConsultaChequePanel
 * @extends Ext.grid.Panel
 * @xtype consulta-cheque-panel
 * Pantalla para la consulta de cheques
 */
Ext.define('Pce.caja.cheques.consultaCheque.ConsultaChequePanel', {
    extend: 'Ext.form.Panel',
    xtype: 'consulta-cheque-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.caja.cheques.consultaCheque.ConsultaChequeTabla',
        'Pce.caja.cheques.consultaCheque.ConsultaChequeController',
        // 'Pce.ingresos.movimientoContable.MovimientoContableStore',
        // 'Pce.ingresos.transaccion.TransaccionTabla',
        // 'Pce.ingresos.transaccion.TransaccionPolizaStore',
        'Ext.grid.plugin.CellEditing'
    ],

    controller: 'consulta-cheque-panel',

    viewModel: {
        data: {
            movimientoContable: null,
            cheque: null,
            beneficiario: null,
            fechaInicial: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            fechaFinal: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
        }
    },

    title: 'Consulta de cheques',
    layout: 'fit',
    emtpyText: 'No hay datos para mostar',

    items: [{
        xtype: 'consulta-cheque-tabla',
        reference: 'chequesTabla',
        // store: {
        //     type: 'transacciones-polizas',
        //     autoLoad: true
        // },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Fecha emisión desde',
                name: 'fechaInicio',
                bind: {
                    value: '{fechaInicial}',
                    maxValue: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaInicial'
                }
            }, {
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                reference: 'fechaFinal',
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicial}',
                    // disabled: '{!fechaInicial}',
                    value: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaFinal'
                }
            },{
                xtype: 'numberfield',
                fieldLabel: 'Núm. cheque',
                valueField: 'cheque',
                // reference: 'polizaSelector',
                name: 'numCheque',
                labelWidth: 100,
                // allowBlank: false,
                // emptyText: 'No existen folios',
                flex: 1,
                listeners: {
                    change: 'onChangeNumCheque'
                }
            },{
                xtype: 'textfield',
                fieldLabel: 'Beneficiario',
                valueField: 'beneficiario',
                // reference: 'polizaSelector',
                name: 'beneficiario',
                labelWidth: 100,
                // allowBlank: false,
                // emptyText: 'No existen folios',
                flex: 1,
                fieldStyle : 'text-transform: uppercase',
                listeners: {
                    change: 'onChangeBeneficiario'
                }
            }, {
                text: 'Buscar',
                handler: 'onBuscarCheques',
                bind:{
                    // disabled: '{!cheque && !beneficiario && (!fechaInicial || !fechaFinal)}'
                }
            }]
        }],
        bind: {
            selection: '{cheque}'
        },
    }],
});