Ext.define('Pce.caja.cheques.emisionCheque.EmisionChequeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emision-cheque-panel',
    requires: [

    ],

    fechaInicio: undefined,
    fechaFin: undefined,
    banco: undefined,


    init: function() {
        
        this.resetStore();
    },

    resetStore(){
        let store = this.lookup('chequesTabla').getStore()
        store.removeAll();
        
    },
    onSeleccionaFechaInicial(control, seleccion) {
        this.fechaInicio = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    onSeleccionaFechaFinal(control, seleccion) {
        if(!control.isValid()){
            return;
        }
        this.fechaFin = Ext.util.Format.date(seleccion, 'd/m/Y');
        
    },

    onBuscarCheques(){
        this.cargaTablaCheques(this.fechaInicio, this.fechaFin);
    },

    onChangeBanco(comp, value, old) {
        this.banco = comp.selection;
        let cuenta = this.lookup('cuenta');
        cuenta.reset();
        cuenta.getStore().porBanco(value);
    },

    onEmitirCheque() {
        let tabla = this.lookup('chequesTabla');
        let datos = {
            numCheque: this.lookup('numCheque').value,
            cuentaBanco: this.lookup('cuenta').value,
            banco: this.lookup('banco').value,
            chequeId: tabla.selection.id
        }
        if (datos.cuentaBanco == null || datos.numCheque == null) {

            Ext.Msg.alert(
                'Advertencia',
                `Faltan datos por llenar`
            );
            return;
        }
        this.emitirCheque(datos);
    },

    emitirCheque(datos) {

        let me = this;
        Ext.getBody().mask('Emitiendo cheque...');
        Ext.Ajax.request({
            url: '/api/cajas/chequera/cheque-solicitud/actualizar-cheque',
            method: 'POST',
            jsonData: datos,
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                let mensaje = ``;

                Ext.getBody().unmask();
                if (success) {
                    mensaje = `Se emitió el cheque correctamente`;
                    Ext.Msg.alert(
                        'Operación exitosa',
                        mensaje,
                        () => {
                            me.imprimirCheque(datos.chequeId, me.banco.get('reporte'));
                            me.lookup('numCheque').reset();
                            me.lookup('cuenta').reset();
                            me.lookup('banco').reset();
                            me.lookup('fechaInicio').reset();
                            me.lookup('fechaFinal').reset();
                            me.lookup('chequesTabla').getStore().removeAll();
                        }
                    );
                }else{
                    mensaje = resultado.errorMessage;
                    icono = Ext.Msg.INFO;
                    Ext.Msg.alert(
                        'Proceso terminado con errores',
                        mensaje
                    );
                }
            }
        });
    },

    cargaTablaCheques(fechaInicial, fechaFinal) {
        let store = this.lookup('chequesTabla').getStore();
        store.setProxy({
            type: 'rest',
            url: '/api/cajas/chequera/cheque-solicitud/emision-cheques',
            writer: {
                writeAllFields: true
            }
        });
        this.lookup('chequesTabla').getStore().load({
            params: {
                fechaInicial,
                fechaFinal
            } 
        });
    },

    imprimirCheque(idSolCheque, reporte){
        window.open(`/api/cajas/chequera/cheque-solicitud/descargar?idSolCheque=${idSolCheque}&reporte=${reporte}`);
    }
});

//PIDSOLCHEQUE   -- cambiar status a IM