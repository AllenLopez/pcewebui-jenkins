/**
 * @class Pce.caja.cheques.emisionCheque.EmisionChequePanel
 * @extends Ext.grid.Panel
 * @xtype genera-poliza-caja-panel
 * Pantalla para la emision de cheques
 */
Ext.define('Pce.caja.cheques.emisionCheque.EmisionChequePanel', {
    extend: 'Ext.form.Panel',
    xtype: 'emision-cheque-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.caja.cheques.emisionCheque.EmisionChequeTabla',
        'Pce.caja.cheques.emisionCheque.EmisionChequeController',
        // 'Pce.ingresos.movimientoContable.MovimientoContableStore',
        // 'Pce.ingresos.transaccion.TransaccionTabla',
        // 'Pce.ingresos.transaccion.TransaccionPolizaStore',
        'Ext.grid.plugin.CellEditing'
    ],

    controller: 'emision-cheque-panel',

    viewModel: {
        data: {
            movimientoContable: null,
            cheque: null,
            fechaInicial: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            fechaFinal: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
        }
    },

    title: 'Emisión de cheques',
    layout: 'fit',
    emtpyText: 'No hay datos para mostar',

    items: [{
        xtype: 'emision-cheque-tabla',
        reference: 'chequesTabla',
        // store: {
        //     type: 'transacciones-polizas',
        //     autoLoad: true
        // },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Desde',
                allowBlank: false,
                name: 'fechaInicio',
                bind: {
                    value: '{fechaInicial}',
                    maxValue: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaInicial'
                }
            }, {
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                reference: 'fechaFinal',
                allowBlank: false,
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicial}',
                    disabled: '{!fechaInicial}',
                    value: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaFinal'
                }
            }, {
                text: 'Buscar',
                handler: 'onBuscarCheques',
                bind:{
                    disabled: '{!fechaInicial && ! fechaFinal}'
                }
            }]
        },{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'banco-selector',
                allowBlank: false,
                reference: 'banco',
                fieldLabel: 'Banco receptor',
                flex: 1,
                listeners: {
                    change: 'onChangeBanco'
                },
                store:{
                    type: 'banco',
                    autoLoad: true,
                    filters: [{
                        property: 'reporte',
                        operator: '!=',
                        value: null
                    }],
                }
            },{
                xtype: 'cuenta-banco-selector',
                allowBlank: false,
                reference: 'cuenta',
                flex: 1
            },{
                xtype: 'numberfield',
                fieldLabel: 'Num. cheque',
                reference: 'numCheque',
                allowBlank: false,
                flex: 1,
            },]
        }],
        bind: {
            selection: '{cheque}'
        },
    }],
    buttons: [{
        // iconCls: 'x-fa fa-flag-o',
        text: 'Emitir cheque',
        handler: 'onEmitirCheque',
        bind: {
            disabled: '{!cheque}'
        }
    }]
});