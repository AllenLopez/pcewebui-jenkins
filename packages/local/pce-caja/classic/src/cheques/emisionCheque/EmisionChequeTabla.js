Ext.define('Pce.caja.cheques.emisionCheque.EmisionChequeTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'emision-cheque-tabla',

    columns: [{
        text: 'Id',
        dataIndex: 'id',
        tooltip:'Numero de solicitud',
        width: 50
    },{
        text: 'Tipo',
        dataIndex: 'tipoMovimiento',
        tooltip:'Descripción del movimiento',
        flex: 1
    }, {
        text: 'Beneficiario',
        dataIndex: 'beneficiario',
        tooltip:'Nombre que corresponde la emisión del cheque',
        flex: 1
    }, {
        text: 'Etapa',
        dataIndex: 'estatus',
        tooltip:'Etapa en la que se encuentra el cheque',
        flex: 1
    }, {
        text: 'Fecha',
        dataIndex: 'fechaAfectacion',
        tooltip:'Fecha en la que se hizo la afectación de la cartera',
        flex: 1,
        renderer(v){
            return Ext.util.Format.date(v, 'd/m/Y');
        },
    },{
        text: 'Importe',
        dataIndex: 'importe',
        tooltip:'Muestra el importe que se devolverá al asegurado.',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        flex: 1
    },]
});