Ext.define('Pce.caja.poliza.PolizaCajaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-poliza-caja-controller',

    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.caja.poliza.PolizaDetalleCajaTabla',
        //'Pce.ingresos.poliza.PolizaDetalleStore'
    ],

    parametros: {
        tipo: undefined,
        serie: undefined,
        transaccion: undefined,
        folio: undefined,
        fechaInicio: undefined,
        fechaFinal: undefined,
        polizaId: undefined
    },

    onSeleccionTipoPoliza(selector, valor) {
        this.parametros.tipo = valor;
    },

    validarFiltros() {
        let boton = Ext.get('btnImprimirPolizas').component;
        if(this.parametros.folio || this.parametros.transaccion){
            boton.setDisabled(true);
        }else{
            boton.setDisabled(false);
        }
        // if (this.parametros.folio != undefined &&
        //     ((this.parametros.serie == undefined || this.parametros.serie == "")
        //         || (this.parametros.tipo == undefined || this.parametros.tipo == ""))) {

        //     Ext.Msg.alert('Advertencia!', 'Es necesario seleccionar tipo y serie de póliza para filtrar por folio');
        //     return false;
        // }


        return true;

    },
    onSeleccionSeriePoliza(selector, valor) {
        this.parametros.serie = valor;
    },

    onSeleccionEjercicio(selector, valor) {
        this.parametros.ejercicio = valor;
    },

    onSeleccionMes(selector, valor) {
        let me = this;
        let polizaSelector = me.lookup('polizaSelector');

        this.parametros.mes = valor;

        polizaSelector.getStore().load({
            params: me.parametros
        });
    },
    onSeleccionDesde(comp, valor) {
        this.parametros.fechaInicio = Ext.util.Format.date(valor, 'm/d/Y');

    },
    onSeleccionHasta(comp, valor) {
        this.parametros.fechaFinal = Ext.util.Format.date(valor, 'm/d/Y');

    },
    onConsultaPoliza(selector, valor) {
        let polizasTabla = this.lookup('polizaDetalleTabla');

        polizasTabla.getStore().load({
            params: {
                polizaId: valor
            }
        });
    }, onChangeTransaccion(comp, valor) {
        this.parametros.transaccion = valor;
    }, onChangeFolio(comp, valor) {
        this.parametros.folio = valor;
    },

    onImprimirPoliza(tabla, i, j, item, e, poliza) {
        let params = `?Pidpoliza=${poliza.getId()}&serie=${poliza.get('seriePoliza')}&tipo=${poliza.get('tipoPoliza')}&folio=${poliza.get('folio')}`;

        window.open(`/api/ingresos/poliza/descargar${params}`);
    },

    onImprimirPolizas(tabla, i, j, item, e, poliza) {
        let store = this.lookup('poliza-tabla-caja').getStore();
        if (store.data.length == 0) {
            Ext.Msg.alert('Advertencia!', 'No hay {Pólizas para imprimir');
            return;
        }
        let fecha1 = Ext.util.Format.date(this.parametros.fechaInicio, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(this.parametros.fechaFinal, 'd/m/Y');
        let params = `?fecha1=${fecha1}&fecha2=${fecha2}&serie=${this.parametros.serie}&tipo=${this.parametros.tipo}`;

        window.open(`/api/ingresos/poliza/descargar-polizas${params}`);
    },

    onBuscarPoliza() {
        
        if (!this.validarFiltros()) {
            return;
        }
        let fecha1 = new Date(this.parametros.fechaInicio);
        let fecha2 = new Date(this.parametros.fechaFinal);
        this.parametros.serie = this.lookup('serie').value;
        this.parametros.tipo = this.lookup('tipo').value;
        if (fecha2 < fecha1) {
            Ext.Msg.alert('Advertencia!', 'La fecha final tiene que ser mayor a la fecha inicial');
            return;
        }
        let store = this.lookup('poliza-tabla-caja').getStore();
        store.load({
            params: this.parametros
        });

    },

    onVerDetalles(tabla, i, j, item, e, poliza) {
        this.mostrarDialogoTabla(poliza);
    },

    onSelectItem(comp, record, item, index, e, eOpts) {
        this.view.viewModel.data.poliza = record;
    },

    mostrarDialogoTabla(poliza) {
        Ext.create({
            xtype: 'window',
            title: `Detalle de póliza - ${poliza.get('descripcion')}`,
            autoShow: true,
            modal: true,
            height: '90vh',
            width: '100vw',
            layout: 'fit',
            items: {
                xtype: 'poliza-detalle-caja-tabla',
                store: {
                    type: 'poliza-detalle',
                    autoLoad: {
                        params: {
                            polizaId: poliza.getId()
                        }
                    }
                }
            }
        })
    }
});