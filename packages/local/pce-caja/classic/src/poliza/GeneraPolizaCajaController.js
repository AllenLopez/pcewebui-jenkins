/**
 * @class Pce.caja.poliza.GeneraPolizaCajaController
 * @extends Ext.app.ViewController
 * @alias controller.genera-poliza-caja-panel
 * ejecuta procedimiento para generar póliza según el registro de transacción seleccionado
 */
Ext.define('Pce.caja.poliza.GeneraPolizaCajaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.genera-poliza-caja-panel',

    requires: [
        'Pce.caja.poliza.PolizaCajaTabla',
    ],

    polizas: undefined,
    transaccion: undefined,
    tipoMovimiento: undefined,
    fechaInicio: undefined,
    fechaFin: undefined,
    tabla: undefined,

    onCrearPoliza(){
        let me = this;
        const tabla = this.getTablaTransacciones();
        const seleccion = tabla.getSelection();
        if (seleccion.length === 0) {
            Ext.Msg.alert('Advertencia', 'No se ha seleccionado ningun registro');
            return;
        }
        const vm = this.getViewModel().getData();
        const tipoMov = vm.movimientoContable;
        const instrucciones = this.creaInstruccion(seleccion, tipoMov);

        Ext.Msg.confirm(
            'Generación de póliza',
            `Se efectuará la generación de póliza para esta transacción, ¿Desea proceder?`,
            ans => {
                if (ans === 'yes') {
                    console.log(instrucciones);
                    me.onGenerarPoliza(instrucciones, tipoMov);
                }
            }
        );
    },
  
    cargaTablaTransacciones(tipoId, fechaInicio, fechaFin) {
        this.lookup('transaccionTabla').getStore().load({
            params: {
                tipoId: tipoId,
                fechaInicial: fechaInicio,
                fechaFinal: fechaFin
            },
            callback: () => {    
                this.onSeleccionarTodo();               
            }
        });
    },

    onGenerarPoliza(instruccion, tipoMov) {
        let me = this;
        const tabla = me.getTablaTransacciones();
        let tipoMovimientoFiltro = me.lookup('tipoMovimiento');
        Ext.getBody().mask('Generando pólizas...');
        
        Ext.Ajax.request({
            url: '/api/cajas/poliza/crear',
            method: 'POST',
            jsonData: Ext.JSON.encode(instruccion),
            success(response) {
                Ext.getBody().unmask();
                const resultado = JSON.parse(response.responseText);
                let mensaje = `${resultado.mensaje}`;
                let icono = Ext.Msg.ERROR;

                if(resultado.exito) {
                    mensaje = `Se generaron exitosamente ${resultado.totalPolizas} pólizas. Por favor verifique el detalle de cada póliza`;
                    icono = Ext.Msg.INFO;
                    Ext.Msg.alert('Proceso terminado',mensaje);
                } else {
                    let transaccion = new Pce.transaccion.Transaccion(resultado.transaccionId);
                    me.mostrarRespuesta(transaccion);
                }
                tabla.getSelectionModel().deselectAll();
                tipoMovimientoFiltro.reset();
                me.onSeleccionaTipoMovimiento(null, tipoMov.tipoMovimientoId);
            }
        });
    },

    onMostrarDetallesPolizas(view, row, col, item, e, trans){
        let me = this;
        Ext.Ajax.request({
            url: '/api/ingresos/poliza/por-transaccion',
            method: 'GET',
            params: {
                transaccionId: trans.id
            },
            callback(op, success, response){
                if(success){
                    let resultado = Ext.decode(response.responseText);
                    Ext.create('Ext.window.Window', {
                        autoShow: true,
                        modal: true,
                        controller: me,
                        width: 1200,
                        height: 600,
                        layout: 'fit',
                        title: 'Detalle póliza',
                        items: {
                            xtype: 'poliza-tabla',
                            polizas: resultado
                        }
                    });
                }
            }
        });
    },

    mostrarRespuesta(transaccion){
        Ext.Msg.alert(
            'Proceso terminado',
            `Ocurrio un error al intentar crear la póliza, en la transacción:<b>${transaccion.data}</b>`,
            () => this.mostrarResultadoPoliza(transaccion)
        );
    },

    mostrarResultadoPoliza(transaccion) {
        Ext.create({
            xtype: 'poliza-resultado-dialogo',
            transaccion: transaccion,
            autoShow: true,
            modal: true
        });
    },

    onSeleccionarTodo(){
        const tabla = this.getTablaTransacciones();
        tabla.getSelectionModel().selectAll();
    },

    onQuitarTodo(){
        const tabla = this.getTablaTransacciones();
        tabla.getSelectionModel().deselectAll();
    },
    
    onBuscarTransacciones() {
        this.cargaTablaTransacciones(this.tipoId, this.fechaInicio, this.fechaFin);
    },

    onSeleccionaTipoMovimiento(selector, tipoId) {
        this.tipoMovimiento = tipoId;
        this.cargaTablaTransacciones(tipoId, this.fechaInicio, this.fechaFin);
    },

    onSeleccionaFechaInicial(control, seleccion) {
        this.fechaInicio = Ext.util.Format.date(seleccion, 'd/m/Y');
    },
    
    onSeleccionaFechaFinal(control, seleccion) {
        this.fechaFin = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    onVerDetalles(tabla, i, j, item, e, poliza) {
        this.mostrarDialogoTabla(poliza);
    },

    onImprimirPoliza(tabla, i, j, item, e, poliza) {
        let params = `?Pidpoliza=${poliza.getId()}&serie=${poliza.get('seriePoliza')}&tipo=${poliza.get('tipoPoliza')}&folio=${poliza.get('folio')}`;

        window.open(`/api/ingresos/poliza/descargar${params}`);
    },

    mostrarDialogoTabla(poliza) {
        Ext.create({
            xtype: 'window',
            title: `Detalle de póliza - ${poliza.get('descripcion')}`,
            autoShow: true,
            modal: true,
            height: '90vh',
            width: '100vw',
            layout: 'fit',
            items: {
                xtype: 'poliza-detalle-caja-tabla',
                store: {
                    type: 'poliza-detalle',
                    autoLoad: {
                        params: {
                            polizaId: poliza.getId()
                        }
                    }
                }
            }
        });
    },

    creaInstruccion(seleccion, tipoMov) {
        let datos = [];
        seleccion.forEach(el => {
            let instruccion = {};
            instruccion.transaccionId = el.getId();
            instruccion.usuario = el.get('usuarioCreacion');
            instruccion.origen = tipoMov.get('origenMovimiento');
            instruccion.tipoAfectacion = tipoMov.get('tipoAfectacion');
            instruccion.fechaPoliza = el.get('fechaPoliza');
            instruccion.tipoMovimientoId = tipoMov.get('tipoMovimientoId');
            datos.push(instruccion);
        });

        return datos;
    },

    getTablaTransacciones() {
        const tabla = this.lookup('transaccionTabla');
        if (tabla) { return tabla; }
    }
});