Ext.define('Pce.caja.poliza.ExportacionPolizaCajaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.exportacion-poliza-caja-controller',

    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.caja.poliza.PolizaDetalleCajaTabla',
        // 'Pce.ingresos.poliza.PolizaDetalleStore'
    ],

    parametros: {
        tipo: undefined,
        serie: undefined,
        transaccion: undefined,
        folio: undefined,
        fechaInicio: undefined,
        fechaFinal: undefined,
        polizaId: undefined
    },

    validarFiltros() {
        let boton = Ext.get('btnImprimirPolizas').component;
        if (this.parametros.fechaInicio != undefined
            && this.parametros.fechaFinal != undefined ) {
            boton.setDisabled(false);
            return;
        }

        boton.setDisabled(true);


        return true;

    },

    onSeleccionDesde(comp, valor) {
        this.parametros.fechaInicio = Ext.util.Format.date(valor, 'm/d/Y');

    },

    onSeleccionHasta(comp, valor) {
        this.parametros.fechaFinal = Ext.util.Format.date(valor, 'm/d/Y');

    },
    
    onGenerarArchivo(comp, valor) {
        // this.parametros.fechaFinal = Ext.util.Format.date(valor, 'm/d/Y');
        let store = this.lookup('poliza-tabla').getStore();
        if (store.data.length == 0) {
            Ext.Msg.alert('Advertencia!', 'No hay pólizas para generar el archivo de exportación');
            return;
        }

        Ext.Msg.confirm('Generar archivo de exportación', '¿Seguro que desea generar el archivo de las pólizas en el rango de fechas capturado?',
             (choice) => {
                if (choice === 'yes') {
                    let params = `?fecha1=${this.parametros.fechaInicio}&fecha2=${this.parametros.fechaFinal}`;
                    window.open(`/api/ingresos/poliza/archivo-exportacion${params}`);
                }
            }
        );
    },
    
    onImprimirPoliza(tabla, i, j, item, e, poliza) {
        let params = `?Pidpoliza=${poliza.getId()}&serie=${poliza.get('seriePoliza')}&tipo=${poliza.get('tipoPoliza')}&folio=${poliza.get('folio')}`;

        window.open(`/api/ingresos/poliza/descargar${params}`);
    },

    onImprimirPolizas(tabla, i, j, item, e, poliza) {
        let store = this.lookup('poliza-tabla').getStore();
        if (store.data.length == 0) {
            Ext.Msg.alert('Advertencia!', 'No hay pólizas para imprimir');
            return;
        }
        let fecha1 = Ext.util.Format.date(this.parametros.fechaInicio, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(this.parametros.fechaFinal, 'd/m/Y');
        let params = `?fecha1=${fecha1}&fecha2=${fecha2}`;
        window.open(`/api/ingresos/poliza/descargar-polizas-exportacion${params}`);
    },

    onBuscarPoliza() {
        let fecha1 = new Date(this.parametros.fechaInicio);
        let fecha2 = new Date(this.parametros.fechaFinal);
        if (fecha2 < fecha1) {
            Ext.Msg.alert('Advertencia!', 'La fecha final tiene que ser mayor a la fecha inicial');
            return;
        }
        let store = this.lookup('poliza-tabla').getStore();
        store.load({
            params: this.parametros
        });
        // this.validarFiltros();
    },

    onVerDetalles(tabla, i, j, item, e, poliza) {
        this.mostrarDialogoTabla(poliza);
    },

    onSelectItem(comp, record, item, index, e, eOpts) {
        this.view.viewModel.data.poliza = record;
    },

    mostrarDialogoTabla(poliza) {
        Ext.create({
            xtype: 'window',
            title: `Detalle de póliza - ${poliza.get('descripcion')}`,
            autoShow: true,
            modal: true,
            height: '90vh',
            width: '100vw',
            layout: 'fit',
            items: {
                xtype: 'poliza-detalle-tabla',
                store: {
                    type: 'poliza-detalle',
                    autoLoad: {
                        params: {
                            polizaId: poliza.getId()
                        }
                    }
                }
            }
        })
    }
});