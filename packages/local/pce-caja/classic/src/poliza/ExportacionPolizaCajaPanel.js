Ext.define('Pce.caja.poliza.ExportacionPolizaCajaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'exportacion-poliza-caja-panel',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Pce.caja.poliza.ExportacionPolizaCajaController',
        // 'Pce.ingresos.poliza.PolizaSelector',
        // 'Pce.ingresos.poliza.PolizaStore',
        'Pce.caja.poliza.PolizaDetalleCajaTabla',
        // 'Pce.ingresos.poliza.TipoPolizaStore',
        // 'Pce.ingresos.poliza.SeriePolizaStore',
        // 'Pce.ingresos.quincena.QuincenaStore'
    ],

    controller: 'exportacion-poliza-caja-controller',
    viewModel: {
        data: {
            folio: null,
            poliza: null
        }
    },
    title: 'Exportación de póliza caja',
    layout: 'fit',

    items: [{
        xtype: 'poliza-tabla-caja',
        reference: 'poliza-tabla',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                name: 'desde',
                fieldLabel: 'A partir de',
                labelWidth: 80,
                flex: 1,
                listeners: {
                    change: 'onSeleccionDesde'
                }
            }, {
                xtype: 'datefield',
                name: 'hasta',
                fieldLabel: 'Hasta',
                labelWidth: 80,
                flex: 1,
                listeners: {
                    change: 'onSeleccionHasta'
                }
            }, {
                text: 'Buscar',
                flex: 0.5,
                bind: {
                    // disabled: '{!folio}'
                },
                handler: 'onBuscarPoliza'
            }, {
                text: 'Generar Archivo Exportar',
                flex: 0.5,
                bind: {
                    // disabled: '{!folio}'
                },
                handler: 'onGenerarArchivo'
            }, {
                text: 'Imprimir',
                flex: 0.5,
                bind: {
                    // disabled: '{!folio}'
                },
                handler: 'onImprimirPolizas'
            }]
        }]
    }],
    afterRender() {
        this.callParent();
        this.inicializarFecha();
    },

    inicializarFecha() {
        let fecha = new Date();
        var firstDay = new Date(fecha.getFullYear(), fecha.getMonth(), 1);
        var lastDay = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
        let campo = Ext.ComponentQuery.query('[name=desde]')[0];
        let hasta = Ext.ComponentQuery.query('[name=hasta]')[0];

        campo.setValue(firstDay);
        
        hasta.setValue(lastDay);
    },
    // buttons: [{
    //     text: 'Imprimir',
    //     bind: {
    //         disabled: '{!folio}'
    //     },
    //     handler: 'onImprimirPoliza'
    // }]
});

/*

*/