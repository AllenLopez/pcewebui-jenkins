Ext.define('Pce.caja.poliza.PolizaDetalleCajaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'poliza-detalle-caja-tabla',

    requires: [
        // 'Pce.ingresos.poliza.PolizaDetalleStore'
    ],

    store: {
        type: 'poliza-detalle',
        autoLoad: true
    },

    layout: 'fit',

    features: [{
        ftype: 'summary'
    }],

    viewConfig: {
        markDirty: false
    },

    columns: [{
        text: 'Asiento',
        dataIndex: 'consecutivo',
        flex: 0.5
    }, {
        text: 'Cuenta',
        dataIndex: 'cuenta',
        flex: 0.5
    }, {
        text: 'Subcuenta 1',
        dataIndex: 'subCuenta01',
        flex: 0.7,
    }, {
        text: 'Subcuenta 2',
        dataIndex: 'subCuenta02',
        flex: 0.7,
    }, {
        text: 'Subcuenta 3',
        dataIndex: 'subCuenta03',
        flex: 0.7,
    }, {
        text: 'Subcuenta 4',
        dataIndex: 'subCuenta04',
        flex: 0.7,
    }, {
        text: 'Descripción',
        dataIndex: 'descripcion',
        flex: 2,
    }, {
        text: 'Debe',
        dataIndex: 'debe',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        summaryType: 'sum',
        summaryRenderer: function(valor, summaryData, dataIndex) {
            return `<b>Debe Total: ${Ext.util.Format.usMoney(valor)}</b>`;
        },
        flex: 1.3,
    }, {
        text: 'Haber',
        dataIndex: 'haber',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        summaryType: 'sum',
        summaryRenderer: function(valor, summaryData, dataIndex) {
            return `<b>Haber Total: ${Ext.util.Format.usMoney(valor)}</b>`;
        },
        flex: 1.3,
    }],
    // buttons: [{
    //     text: 'Imprimir',
    // }]
});