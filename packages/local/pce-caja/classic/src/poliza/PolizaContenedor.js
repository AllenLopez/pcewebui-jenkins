Ext.define('Pce.caja.poliza.PolizaContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'poliza-contenedor-caja',

    layout: 'fit',

    requires: [
        'Pce.caja.banco.BancoPanel',
        'Pce.caja.cuentaBanco.CuentaBancoPanel',
        'Pce.caja.poliza.GeneraPolizaCajaPanel',
        'Pce.caja.poliza.ConsultaPolizasCajaPanel',
        'Pce.caja.poliza.ExportacionPolizaCajaPanel',
        'Pce.caja.poliza.captura.ConsultaCapturaPolizaCajaPanel'
    ],

    mapaPolizas: {
        'generar': 'genera-poliza-caja-panel',
        'consulta': 'consulta-poliza-caja-panel',
    },

    poliza: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.poliza) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaPolizas[this.poliza]
            });
        }
    }
});
