/**
 * @class Pce.caja.poliza.GeneraPolizaCajaPanel
 * @extends Ext.grid.Panel
 * @xtype genera-poliza-caja-panel
 * Pantalla para generar polizas según el tipo de movimiento y transacción
 */

 Ext.create('Ext.data.Store', {
    alias: 'store.tipoEfecto',
    storeId: 'tipoEfecto',
    fields:[ 'nombre', 'valor'],
    data: [
        { nombre: 'Afectación', valor: 'AFECTACION' },
        { nombre: 'Cancelación', valor: 'CANCELACION' },
    ]
});

Ext.define('Pce.caja.poliza.GeneraPolizaCajaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'genera-poliza-caja-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.caja.poliza.GeneraPolizaCajaController',
        'Ext.grid.plugin.CellEditing',
        'Pce.caja.poliza.GeneraPolizaCajaTabla'
        // 'Pce.ingresos.poliza.GeneraPolizaCajaTabla'
    ],

    controller: 'genera-poliza-caja-panel',

    viewModel: {
        data: {
            movimientoContable: null,
            transaccion: null,
            fechaInicial: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            fechaFinal: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
        }
    },

    title: 'Generar pólizas caja',
    layout: 'fit',
    emtpyText: 'no hay datos para mostar',

    items: [{
        xtype: 'genera-poliza-caja-tabla',
        reference: 'transaccionTabla',
        store: {
            type: 'transacciones-polizas',
            autoLoad: true
        },
        bind: {
            selection: '{transaccion}'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Desde',
                allowBlank: false,
                name: 'fechaInicio',
                bind: {
                    value: '{fechaInicial}',
                    maxValue: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaInicial'
                }
            }, {
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                reference: 'fechaFinal',
                allowBlank: false,
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicial}',
                    disabled: '{!fechaInicial}',
                    value: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaFinal'
                }
            }, {
                xtype: 'combobox',
                reference: 'tipoMovimiento',
                name: 'movimientoContable',
                fieldLabel: 'Tipo de movimiento',
                displayField: 'movimientoDescripcion',
                valueField: 'tipoMovimientoId',
                emptyText: 'Selecciona el tipo de movimiento',
                width: 600,
                labelWidth: 150,
                store: {
                    type: 'movimiento-contable',
                    autoLoad: true,
                    proxy: {
                        type: 'rest',
                        url: '/api/ingresos/movimiento-contable/cajas',
                
                        writer: {
                            type: 'json',
                            writeAllFields: true
                        },
                
                        reader: {
                            type: 'json'
                        }
                    }
                },
                listeners: {
                    change: 'onSeleccionaTipoMovimiento'
                },
                disabled: true,
                bind: {
                    disabled: '{!fechaFinal}',
                    selection: '{movimientoContable}'
                }
            }, {
                text: 'Buscar',
                handler: 'onBuscarTransacciones'
            }]
        },{
            xtype: 'toolbar',
            dock: 'top',
            items: [   {
                text: 'Seleccionar todo',
                handler: 'onSeleccionarTodo'
            }, {
                text: 'Quitar todo',
                handler: 'onQuitarTodo'
            }]
        }],
       
    }],
    buttons: [{
        // iconCls: 'x-fa fa-flag-o',
        text: 'Generar póliza',
        handler: 'onCrearPoliza',
        bind: {
            disabled: '{!transaccion}'
        }
    }]
});