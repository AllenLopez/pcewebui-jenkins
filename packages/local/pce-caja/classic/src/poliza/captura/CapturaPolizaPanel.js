/**
 * @class Pce.caja.poliza.CapturaPolizaForma
 * @extends Ext.grid.Panel
 * @xtype captura-poliza-forma
 * Pantalla para capturar polizas de forma manual
 */
Ext.define('Pce.caja.poliza.captura.CapturaPolizaPanel', {
    extend: 'Ext.form.Panel',
    reference: 'capturaPolizaPanel',
    xtype: 'captura-poliza-panel-caja',
    
    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.grid.plugin.CellEditing',

        'Pce.caja.poliza.captura.PolizaCuentaCajaTabla',
        // 'Pce.ingresos.poliza.CuentaPoliza',

        'Pce.caja.poliza.captura.CapturaPolizaPanelCajaController'
    ],

    viewModel: {
        data: { validForm: true }
    },

    controller: 'captura-poliza-panel-caja',

    emptyText: 'No existen registros',
    title: 'Captura manual de póliza',
    layout: 'fit',

    items: [{
        xtype: 'poliza-cuenta-caja-tabla',
        reference: 'polizaCuentaTabla',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'combobox',
                name: 'tipo',
                fieldName: 'tipo',
                fieldLabel: 'Tipo poliza',
                valueField: 'tipo',
                displayField: 'nombre',
                forceSelection: true,
                allowBlank: false,
                queryMode: 'local',
                store: {
                    type: 'tipos-poliza',
                    autoLoad: true,
                    filters: [{
                        property: 'estatus',
                        value: true
                    }]
                },
                flex: 1,
            }, {
                xtype: 'combobox',
                name: 'serie',
                fieldName: 'serie',
                fieldLabel: 'Serie poliza',
                valueField: 'serie',
                displayField: 'nombre',
                allowBlank: false,
                forceSelection: true,
                queryMode: 'local',
                store: {
                    type: 'series-poliza',
                    autoLoad: true,
                    filters: [{
                        property: 'estatus',
                        value: true
                    }]
                },
                flex: 1,
            }, {
                xtype: 'datefield',
                name: 'fecha',
                reference: 'fecha',
                fieldLabel: 'Fecha',
                allowBlank: false,
                flex: 1
            }]
        }, {
            xtype: 'toolbar',
            doc: 'top',
            items: [{
                xtype: 'textfield',
                name: 'descripcion',
                fieldLabel: 'Descripcion',
                fieldName: 'descripcion',
                allowBlank: false,
                flex: 3
            }, {
                xtype: 'textfield',
                name: 'referencia',
                fieldLabel: 'Referencia',
                fieldName: 'referencia',
                allowBlank: false,
                flex: 1
            }]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Agregar Partida',
                handler: 'onAgregarPoliza',
            }]
        }]
    }],

    buttons: [{
        text: 'Procesar captura',
        reference: 'btAceptar',
        handler: 'onAceptar'
    }, {
        text: 'Cancelar',
        handler: 'onCancelar'
    }]
});