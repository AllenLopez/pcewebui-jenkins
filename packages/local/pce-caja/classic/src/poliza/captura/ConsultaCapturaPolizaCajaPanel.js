/**
 * @class Pce.caja.poliza.ConsultaCapturaPolizaPanel
 * @extends Ext.grid.Panel
 * @xtype consulta-captura-poliza-panel
 * Pantalla que filtra y enlista pólizas capturadas manualmente por usuario
 */
Ext.define('Pce.caja.poliza.captura.ConsultaCapturaPolizaCajaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'consulta-captura-poliza-caja-panel',
    
    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Pce.caja.poliza.captura.ConsultaCapturaPolizaPanelCajaController',
        'Pce.caja.poliza.PolizaCajaTabla',

    ],

    controller: 'consulta-captura-caja-controller',

    viewModel: {
        data: {
            fechaInicial: null,
            fechaFinal: null
        }
    },

    emptyText: 'No existen registros',
    title: 'Captura de pólizas caja',
    layout: 'fit',

    items: [{
        xtype: 'poliza-tabla-caja',
        reference: 'poliza-tabla',
        store: {
            autoLoad: true
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Desde',
                allowBlank: false,
                name: 'fechaInicio',
                bind: {
                    value: '{fechaInicial}',
                    maxValue: '{fechaFinal}'
                },
                flex: 2
            }, {
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                reference: 'fechaFinal',
                allowBlank: false,
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicial}',
                    disabled: '{!fechaInicial}',
                    value: '{fechaFinal}'
                },
                flex: 2
            }, {
                text: 'Imprimir',
                handler: 'onImprimirPoliza',
            }, {
                xtype: 'tbseparator'
            }]
        }],
        // columns: [{
        //     text: 'Tipo',
        //     dataIndex: 'tipo',
        //     flex: 1
        // }, {
        //     text: 'serie',
        //     dataIndex: 'serie',
        //     flex: 1
        // }, {
        //     text: 'folio',
        //     dataIndex: 'folio',
        //     flex: 1
        // }, {
        //     text: 'descripcion',
        //     dataIndex: 'descripcion',
        //     flex: 4
        // }, {
        //     text: 'referencia',
        //     dataIndex: 'referencia',
        //     flex: 1
        // }, {
        //     text: 'estatus',
        //     dataIndex: 'estatus',
        //     renderer(v){
        //         let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
        //         return `<span class="${cls}"></span>`;
        //     } ,
        //     flex: 1
        // }, {
        //     text: 'fecha',
        //     dataIndex: 'fecha',
        //     flex: 1
        // }, {
        //     text: 'Creado por',
        //     dataIndex: 'usuarioCreacion',
        //     flex: 1
        // }]
    }],
});