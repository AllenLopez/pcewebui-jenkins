/**
 * @class Pce.caja.poliza.ConsultaCapturaPolizaPanelController
 * @extends Ext.app.ViewController
 * @alias controller.consulta-captura-controller
 * Controlador de pantalla CapturaPolizaPanel que determina funcionalidad para filtrar y mostrar resultados de pólizas capturadas manualmente
 */
Ext.define('Pce.caja.poliza.captura.ConsultaCapturaPolizaPanelCajaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-captura-caja-controller',

    require: [
        'Pce.caja.poliza.captura.CapturaPolizaCajaDialogo',
    ],

    onAgregarPoliza() {
        this.mostrarDialogoEdicion(new Pce.ingresos.poliza.Poliza());
    },

    onImprimirPoliza() {
        Ext.toast('imprimir poliza...');
    },

    mostrarDialogoEdicion(poliza){
        let title = poliza.phantom ? 'Nueva póliza manual' : 'Editar póliza';

        Ext.create({
            xtype: 'dialogo-captura-poliza-caja',
            title,
            record: poliza,
            maximizable: true,
            maximized: true
        });
    },

    // onGuardar(dialogo, registro, forma) {
    //     let controller = forma.getController();
    //     let refItems = forma.getRefItems();
    //     let grid = refItems[0];
    //     let detalles = [];
    //     let valores = forma.getForm().getValues();
    //     valores.debe = controller.totalHaber;
    //     valores.haber = controller.totalDebe;
    //
    //     if(grid.xtype === 'poliza-cuenta-tabla') {
    //         detalles = grid.getStore().getData();
    //     }
    //     console.log(valores);
    //
    //     let polizaDetalles = detalles.items;
    //     let poliza = new Pce.ingresos.poliza.Poliza(valores);
    //
    //     console.log(polizaDetalles);
    //     console.log(poliza);
    // },
});