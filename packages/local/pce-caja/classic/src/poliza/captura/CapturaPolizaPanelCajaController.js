/**
 * @class Pce.caja.poliza.CapturaPolizaPanelCajaController
 * @extends Ext.app.ViewController
 * @alias controller.captura-poliza-panel-caja
 * Controlador para formulario de captura de póliza manual
 * 
 */
Ext.define('Pce.caja.poliza.captura.CapturaPolizaPanelCajaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.captura-poliza-panel-caja',

    requires: [
        'Pce.caja.poliza.captura.PolizaCuentaCajaTabla',
        'Pce.caja.poliza.captura.CuentaPolizaCajaDialogo'
    ],

    totalHaber: undefined,
    totalDebe: undefined,
    registro: undefined,
    detalles: undefined,
    detallesGuardados: 0,

    onAgregarPoliza() {
        let polizaDetalle = new Pce.ingresos.poliza.PolizaDetalle();

        Ext.create('Pce.ingresos.poliza.captura.CuentaPolizaDialogo', {
            xtype: 'dialogo-cuenta-poliza',
            polizaDetalle,
            listeners: {
                close: () => {
                    if(polizaDetalle.get('cuentaMaestra')) {
                        let tablaStore = this.lookup('polizaCuentaTabla').getStore();
                        tablaStore.insert(0, polizaDetalle);
                    }
                }
            }
        });

    },

    onBeforeEdit(editor, contexto) {
        editor.polizaDetalle = contexto.record;
    },

    onValidateEdit(editor, contexto) {
        // this.totalDebe = contexto.record.get('debe');
        // this.totalHaber = contexto.record.get('haber');

        // if (this.validaTotalesCeros() && this.validaTotalesIguales()) {
        //     this.mensajeFormulario('Importes incorrectos',
        //         'Los importes totales de "DEBE" y "HABER" deben coincidir');
        // }
    },

    onEditImporte(editor, contexto) {
        if (contexto) {
            let registro = contexto.record;
            let debe = registro.get('debe');
            let haber = registro.get('haber');

            if(debe > 0) {
                registro.set('haber', 0);
            } else if(haber > 0) {
                registro.set('debe', 0);
            }
        }
    },

    onSummary(value, summaryData, dataIndex) {
        if(dataIndex === 'debe') {
            this.totalDebe = value;
        } else {
            this.totalHaber = value;
        }

        // if (this.obtieneTotalRegistrosTabla() > 1 && this.validaTotalesCeros()) {
        //     if(!this.validaTotalesIguales() && dataIndex === 'haber') {
        //         this.mensajeFormulario('Importes incorrectos',
        //             'Los importes totales de "DEBE" y "HABER" deben coincidir');
        //     }
        // }
        this.deshabilitaBotonAceptar(false);

        return `<b>Total ${dataIndex}: ${Ext.util.Format.usMoney(value)}</b>`;
    },

    validaTotalesCeros() {
        return (this.totalDebe > 0 && this.totalHaber > 0);
    },

    validaTotalesIguales() {
        return this.totalDebe === this.totalHaber;
    },

    mensajeFormulario(cabecero, mensaje) {
        Ext.Msg.alert(cabecero, mensaje );
    },

    onAceptar() {
        if (!this.obtieneDatosFormulario()) {
            return;
        }

        this.procesarPoliza();
    },

    obtieneDatosFormulario() {
        let form = this.getView();
        let datos = form.getValues();

        let valido = this.validaFormulario(form);
        let totales = this.validaRegistrosDetalle();

        if(!valido || !totales) {
            // this.deshabilitaBotonAceptar(true);
            return false;
        }

        this.registro = new Pce.ingresos.poliza.Poliza(datos);
        this.detalles = this.obtieneDatosTablaDetalles();

        return true;
    },

    obtieneTablaDetalles() {
        return this.lookup('polizaCuentaTabla');
    },

    obtieneDatosTablaDetalles() {
        let tabla = this.obtieneTablaDetalles();
        return tabla.getStore().getData();
    },

    obtieneTotalRegistrosTabla() {
        let storeTabla = this.obtieneDatosTablaDetalles();
        if (storeTabla) {
            return storeTabla.count();
        }
        return 0;
    },

    deshabilitaBotonAceptar(valor) {
        let btnAceptar = this.lookup('btAceptar');
        // si el valor es true se deshabilita el componente
        btnAceptar.setDisabled(valor);
    },

    validaFormulario(form) {
        if(!form.isValid()) {
            this.mensajeFormulario('Formulario Inválido',
                'Revise cuidadosamente su captura, existen datos inválidos en el formulario');
            return false;
        }
        return true;
    },

    validaRegistrosDetalle() {
        if(this.obtieneTotalRegistrosTabla() < 2) {
            this.mensajeFormulario('Detalles incompletos',
                'Por favor asegurese que el número de registros de detalle sea mayor a 1');
            return false;
        }
        if(this.totalDebe !== this.totalHaber) {
            this.mensajeFormulario('Importes incorrectos',
                'Por favor asegurese que el total de importes sea igual para Debe y Haber');
            return false;
        } else {
            if (this.totalDebe === 0 || this.totalHaber === 0 ){
                this.mensajeFormulario('Importes incorrectos',
                    'Por favor asegurese que el total de importes sea mayor a 0');
                return false;
            }
        }

        return true;
    },

    procesarPoliza() {
        this.confirmaGuardarPoliza().then( ans => {
            if(ans === 'yes') {
                Ext.getBody().mask('Procesando datos capturados...');
                this.registro.set('debe', this.totalDebe);
                this.registro.set('haber', this.totalHaber);
                this.guardarPoliza(this.registro)
                    .then(
                        exito => {
                           // console.log(`poliza guardada : ${exito}`);
                            //console.log(this.detalles.items);

                            this.procesaDetalles();
                        },
                        error => this.mensajeFormulario('Error al guardar poliza', error)
                    );
                Ext.getBody().unmask();
            }
        });
    },

    confirmaGuardarPoliza() {
        this.registro.set('debe', this.totalDebe);
        this.registro.set('haber', this.totalHaber);
        return new Promise(resolve => {
            Ext.Msg.confirm('Guardar poliza',
                `Se guardará la póliza capturada. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    guardarPoliza(record){
        if(record.phantom) {
            //console.log('poliza perparada para guardar...');
            return new Promise((resolve, reject) => {
                record.save({
                    success(record, oper) {
                       // console.log(`Póliza id: ${record.getId()}`);
                        resolve(oper.success);
                    },
                    failure(record, operation) {
                        console.log(operation.error);
                        let response = operation.error.response;
                        let msjError = response.statusText;
                        if (operation.error.response.responseText) {
                            msjError = response.responseText;
                        }
                        reject(msjError);
                    }
                });
            });
        } else {
            this.mensajeFormulario('Póliza existente',
                `Esta póliza ya ha sido guardada. Folio: ${this.registro.get('folio')} `);
        }
        //console.log('saliendo de guardar póliza');
    },

    procesaDetalles() {
        let me = this;
        let detalles = me.detalles.items;
        let polizaId = me.registro.getId();
        let consecutivo = 1;
        //console.log(`preparando ${detalles.length} detalles para guardar...`);

        detalles.forEach( el => {
            el.set('consecutivo', consecutivo++);
            el.set('polizaId', polizaId);
            //console.log(`id poliza detalle: ${el.get('polizaId')}`);

            me.guardarDetalles(el)
                .then(
                    valor => {
                        //console.log(typeof valor);
                        if (typeof valor === 'number') {
                            me.detallesGuardados += valor;
                            //console.log(`registros guardados: ${me.detallesGuardados}`);
                        }

                        if (this.detallesGuardados === this.detalles.length) {
                            this.mensajeFormulario('Proceso exitoso',
                                `Póliza guardada con éxito. Folio generado: ${this.registro.get('folio')} `);
                            this.formReset();
                        }
                    },
                    error => console.log(`error con registro: ${error}`));
        });
    },

    guardarDetalles(record) {
        return new Promise((resolve, reject) => {
            if(record.phantom) {
                let cuenta = record.get('cuenta');
                let id = record.getId();

                record.save({
                    success() {
                        //console.log(`detalle ${id} - ${cuenta}. guardado`);
                        resolve(1);
                    },
                    failure(rec, oper) {
                        //console.log(`detalle ${id} - ${cuenta}. ${oper.error}`);
                        let response = oper.error.response;
                        let msjError = response.statusText;
                        if (oper.error.response.responseText) {
                            msjError = response.responseText;
                        }
                        reject(msjError);
                    }
                });
            }
        });
    },

    onCancelar() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar cancelación',
                'Se procederá a cancelar la captura y todos sus datos ¿Deseas continuar?',
                ans => {
                    resolve(ans);
                    this.formReset();
                }
            );
        });
    },

    formReset() {
        this.getView().getForm().reset();
        this.lookup('polizaCuentaTabla').getStore().reload();
    },
});