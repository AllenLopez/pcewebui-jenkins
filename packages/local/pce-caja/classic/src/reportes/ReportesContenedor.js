Ext.define('Pce.caja.reportes.ReportesContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'reportes-contenedor',

    layout: 'fit',

    requires: [
        'Pce.caja.reportes.cajaGeneral.ReporteCajaGeneralPanel',
    ],

    mapaReportes: {
        'caja-general': 'reporte-caja-general-panel'
    },

    reporte: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.reporte) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaReportes[this.reporte]
            });
        }
    }
});