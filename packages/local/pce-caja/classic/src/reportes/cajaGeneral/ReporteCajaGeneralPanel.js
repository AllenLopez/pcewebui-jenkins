Ext.define('Pce.caja.reportes.cajaGeneral.ReporteCajaGeneralPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'reporte-caja-general-panel',

    requires: [
        'Pce.caja.cuentaBanco.PagoAnticipadoDependenciaForma',
        'Pce.caja.cuentaBanco.CuentaBancoSelector',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.caja.reportes.cajaGeneral.ReporteCajaGeneralController',
        'Pce.caja.pagoAnticipadoDependencia.PagoAnticipadoDependenciaController',
    ],

    viewModel: {
        data: {
            fechaInicial: new Date(),
            fechaFinal: new Date()
        }
    },

    controller: 'reporte-caja-general-panel',
    title: 'Reporte caja general',
    bodyPadding: 10,
    items: [{
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                name: 'fechaInicio',
                colspan: 2,
                fieldLabel: 'Fecha inicial',
                bind: {
                    value: '{fechaInicial}'
                }
            }, {
                xtype: 'datefield',
                name: 'fechaFinal',
                colspan: 2,
                fieldLabel: 'Fecha final',
                bind: {
                    minValue: '{fechaInicial}',
                    value: '{fechaFinal}'
                }
            }]
        }]
    }, {
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Detallado por concepto',
                handler: 'onReporteConcepto'
            },{
                text: 'Detallado general',
                handler: 'onReporteGeneral'
            },{
                text: 'Generar archivo',
                handler: 'onGenerarArchivo'
            }]
        }]
    }]
});