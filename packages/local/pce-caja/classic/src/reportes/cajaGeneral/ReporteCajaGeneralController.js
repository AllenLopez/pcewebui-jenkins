Ext.define('Pce.caja.reportes.cajaGeneral.ReporteCajaGeneralController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.reporte-caja-general-panel',
    requires: [
        'Pce.view.dialogo.FormaContenedor',
        // 'Pce.caja.pagoAnticipadoDependencia.PagoAnticipadoDependenciaForma'
    ],

    onReporteConcepto() {
        let vm = this.getViewModel();
        if(!this.validarFechas(vm)){
            return;
        }
        let fecha1 = Ext.util.Format.date(vm.data.fechaInicial, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(vm.data.fechaFinal, 'd/m/Y');
        
        this.generarArchivo(fecha1, fecha2, 'IN_MOVTOS_CAJA_CONCEP');
    },

    onReporteGeneral() {
        let vm = this.getViewModel();
        if(!this.validarFechas(vm)){
            return;
        }
        
        let fecha1 = Ext.util.Format.date(vm.data.fechaInicial, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(vm.data.fechaFinal, 'd/m/Y');
        
        this.generarArchivo(fecha1, fecha2, 'IN_MOVTOS_CAJA_LISTADO');
    },

    onGenerarArchivo() {
        let vm = this.getViewModel();
        if(!this.validarFechas(vm)){
            return;
        }
        
        let fecha1 = Ext.util.Format.date(vm.data.fechaInicial, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(vm.data.fechaFinal, 'd/m/Y');
        
        this.generarArchivo(fecha1, fecha2, 'IN_ARCH_MOVTOS_CAJA');
    },

    generarArchivo(fecha1, fecha2, reporte){
        
        window.open(`/api/cajas/reportes/caja-general?fecha1=${fecha1}&fecha2=${fecha2}&reporte=${reporte}`);
    },

    validarFechas(vm){
        if(vm.data.fechaInicial > vm.data.fechaFinal){
            
            Ext.Msg.alert(
                'Advertencia',
                `La fecha inicial no puede ser mayor a la fecha final`
            );
            return false;
        }

        return true;
    }
});