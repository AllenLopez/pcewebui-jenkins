/**
 * @class Pce.caja.facturacion.complementoFactura.ComplementoFacturaController
 * @extends Ext.app.ViewController
 *@alias controller.complemento-factura-panel
 * Controllador de pantalla para descargar archivo de facturación electrónica basado en
 * tipo, fecha inicial y fecha final
 */
Ext.define('Pce.caja.facturacion.complementoFactura.ComplementoFacturaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.complemento-factura-panel',

    onGenerarArchivo() {
        let vm = this.getViewModel();
        
        let fecha1 = Ext.util.Format.date(vm.data.fechaInicial, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(vm.data.fechaFinal, 'd/m/Y');
        
        this.generarArchivo(fecha1, fecha2);
    },

    generarArchivo(fecha1, fecha2) {
        window.open(`/api/cajas/facturacion/complemento-factura/reporte?fecha1=${fecha1}&fecha2=${fecha2}`);
    }

});