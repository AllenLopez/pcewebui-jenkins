/**
 * @class Pce.caja.facturacion.ComplementoFactura
 * @extends Ext.panel.Panel
 * @xtype carga-complemento-factura
 * Pantalla para generar  archivo con listado de documentos para emitir factura electrónica
 *
 * @author Jorge Escamilla
 */
Ext.define('Pce.caja.facturacion.ComplementoFactura', {
    extend: 'Ext.panel.Panel',
    xtype: 'carga-complemento-factura',

    requires: [
        'Pce.caja.facturacion.complementoFactura.ComplementoFacturaController'
    ],


    title: 'Archivo complemento factura',
    bodyPadding: 10,
    
    controller: 'complemento-factura-panel',

    viewModel: {
        data: {
            fechaInicial:  new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            fechaFinal: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)
        }
    },

    layout: {
        type: 'table',
        columns: 10
    },

    // padding: 10,
    
    defaults: {
        width: '100%'
    },

    items: [ {
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                name: 'fechaInicio',
                colspan: 2,
                fieldLabel: 'Fecha inicial',
                bind: {
                    value: '{fechaInicial}'
                }
            }, {
                xtype: 'datefield',
                name: 'fechaFinal',
                colspan: 2,
                fieldLabel: 'Fecha final',
                bind: {
                    minValue: '{fechaInicial}',
                    value: '{fechaFinal}'
                }
            }]
        }]
    }],
    buttons: [{
        text: 'Generar archivo',
        handler: 'onGenerarArchivo',
    }],
    
});