Ext.define('Pce.caja.catalogo.CatalogoContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'catalogo-contenedor-caja',

    layout: 'fit',

    requires: [
        'Pce.caja.banco.BancoPanel',
        'Pce.caja.cuentaBanco.CuentaBancoPanel'
    ],

    mapaCatalogos: {
        'banco': 'banco-panel',
        'cuenta-banco': 'cuenta-banco-panel'
    },

    catalogo: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.catalogo) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaCatalogos[this.catalogo]
            });
        }
    }
});