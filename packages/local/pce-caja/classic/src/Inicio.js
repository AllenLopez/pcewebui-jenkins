/**
 * @class Pce.caja.Inicio
 * @extends Ext.container.Container
 * @xtype inicio
 * Página de inicio de módulo Caja
 */
Ext.define('Pce.caja.Inicio', {
    extend: 'Ext.Component',
    xtype: 'inicio-caja',
    
    margin: 10,
    html: `<b>Bienvenido al Sistema de Caja de Pensiones Civiles del Estado de Chihuahua</b>
        <p>El objetivo del Sistema de Caja es administrar la cartera de Dependencias afiliadas y asegurados en base a las retenciones de seguridad social y préstamos de los empleados así como las aportaciones patronales.</p>`,

  
});