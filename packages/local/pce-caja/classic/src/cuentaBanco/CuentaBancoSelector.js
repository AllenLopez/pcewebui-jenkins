/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.cuentaBanco.CuentaBancoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'cuenta-banco-selector',

    requires: [
        'Pce.caja.banco.CuentaBancoStore'
    ],

    fieldLabel: 'Cuenta banco',
    valueField: 'id',
    displayField: 'numeroCuenta',
    queryMode: 'local',
    forceSelection: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{numeroCuenta} [{descripcion}]</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{numeroCuenta} [{descripcion}]',
        '</tpl>'
    ],

    store: {
        type: 'cuenta-banco'
    }
});