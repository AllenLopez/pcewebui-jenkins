Ext.define('Pce.caja.cuentaBanco.CuentaBancoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'cuenta-banco-tabla',
    requires: [
        'Pce.caja.banco.CuentaBancoStore'
    ],

    store: {
        type:'cuenta-banco',
        autoLoad: true
    },

    columns:[{
        text: 'Número de cuenta',
        dataIndex: 'numeroCuenta',
        flex: 1
    },{
        text: 'Descripción',
        dataIndex: 'descripcion',
        flex: 1
    },{
        text: 'Clabe interbancaria',
        dataIndex: 'clabeInterbancaria',
        flex: 1
    },{
        text: 'Activo',
        dataIndex: 'activo',
        flex: 1,
        renderer(v) {
            let cls = v ? 'x-fa fa-check': 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        }
    }, {
        text: 'Creado en',
        dataIndex: 'fechaCreacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
        flex: 1
    },{
        text: 'Creado por', 
        dataIndex: 'usuarioCreacion',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        flex: 3,
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar Cuenta Bancaria',
            handler: 'onEditarCuentaBanco'
        }, {
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar Cuenta Bancaria',
            handler: 'onEliminarCuentaBanco'
        }]
    }]
});