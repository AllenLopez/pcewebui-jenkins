Ext.define('Pce.caja.cuentaBanco.CuentaBancoForma',{
    extend: 'Ext.form.Panel',
    xtype: 'cuenta-banco-forma',

    requieres: [
        'Ext.layout.container.Form',
      'Ext.form.field.Text',
      'Ext.form.field.Checkbox'
    ],

    layout: 'form',

    items: [{
        xtype: 'textfield',
        name: 'numeroCuenta',
        fieldLabel: 'Número de cuenta',
        allowBlank: false
    },{
        xtype: 'textfield',
        name: 'descripcion',
        fieldLabel: 'Descripción',
        allowBlank: false
    },{
        xtype: 'textfield',
        name: 'clabeInterbancaria',
        fieldLabel: 'Clabe interbancaria',
        allowBlank: false
    }, {
        xtype: 'checkbox',
        name: 'activo',
        checked: true,
        fieldLabel: 'Activo'
    }]
});