Ext.define('Pce.caja.cuentaBanco.CuentaBancoPanelController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.cuenta-banco-panel',
    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.caja.cuentaBanco.CuentaBancoForma'
    ],

    onSeleccionBanco(comp, bancoId){
        if(typeof bancoId !== 'number'){
            return;
        }
        this.lookup('cuentaBancoTabla').getStore().porBanco(bancoId);
    },

    onAgregarCuentaBanco(){
        this.mostrarDialogoEdicion(new Pce.caja.banco.CuentaBanco());
    },

    onEditarCuentaBanco(tabla, i, j, item, e, cuenta){
        this.mostrarDialogoEdicion(cuenta);
    },

    onEliminarCuentaBanco(tabla, i, j, item, e, cuenta){
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar esta cuenta bancaria? 
            <i>${cuenta.get('descripcion')} - ${cuenta.get('numeroCuenta')}</i>`,
            ans => {
                if(ans === 'yes'){
                    cuenta.erase({
                        success(){
                            Ext.toast('La cuenta seleccionado se ha eliminado ');
                        }
                    });
                }
            }
        );
    },

    onGuardarCuentaBanco(dialogo, cuenta){
        let me = this;
        let grid = me.lookup('cuentaBancoTabla');
        let form = dialogo.down('cuenta-banco-forma').getForm();
        let activo = form.findField('activo').getValue();

        cuenta.data.activo = activo;
        cuenta.data.bancoId = this.view.viewModel.data.bancoId.getId();
        console.log(cuenta);
        Ext.Msg.confirm(
            'Guardar cuenta bancaria',
            `¿Seguro que deseas guardar esta cuenta bancaria 
            <i>${cuenta.get('descripcion')} - ${cuenta.get('numeroCuenta')}</i>`,
            ans => {
                if(ans === 'yes') {
                    cuenta.save({
                        success() {
                            grid.getStore().reload();
                            Ext.toast('Los datos se han guardado correctamente');
                            dialogo.close();
                        }
                    });          
                }
            }
        );
    },

    mostrarDialogoEdicion(cuenta){
        let title = cuenta.phantom ? 'Nueva cuenta bancaria'
        : 'Editar cuenta bancaria';

        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title,
            record: cuenta,
            width: 400,
            items: {
                xtype: 'cuenta-banco-forma'
            },
            listeners: {
                guardar: this.onGuardarCuentaBanco.bind(this)
            }
        });
    }
});