Ext.define('Pce.caja.cuentaBanco.CuentaBancoPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'cuenta-banco-panel',

    requires: [
        'Pce.caja.cuentaBanco.CuentaBancoPanelController',
        'Pce.caja.cuentaBanco.CuentaBancoTabla',
        'Pce.caja.banco.BancoSelector'
    ],

    viewModel: {
        data: {
            bancoId: null
        }
    },

    controller: 'cuenta-banco-panel',
    title: 'Catálogo de cuentas bancarias',
    layout: 'fit',
    items: [{
        xtype: 'cuenta-banco-tabla', 
        reference: 'cuentaBancoTabla',
        emptyText: 'no existen registros',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'banco-selector',
                width: '500px',
                bind:{
                    selection: '{bancoId}'
                },
                listeners: {
                    change: 'onSeleccionBanco'
                }
            },{
                text: 'Agregar',
                handler: 'onAgregarCuentaBanco',
                bind:{
                    disabled: '{!bancoId}'
                }
            }]
        }]
    }]
});