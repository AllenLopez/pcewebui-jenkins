Ext.define('Pce.caja.estadoDeCuenta.EstadoDeCuentaPorEmisionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.estado-cuenta-por-emision',
    model: 'Pce.caja.estadoDeCuenta.EstadoDeCuentaPorEmision'
});