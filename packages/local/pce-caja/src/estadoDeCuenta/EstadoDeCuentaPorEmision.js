Ext.define('Pce.caja.estadoDeCuenta.EstadoDeCuentaPorEmision', {
    extend: 'Ext.data.Model',

    fields: [
        'folio',
        'emisionAvisoCargoId',
        'conceptoId',
        'dependenciaId',
        'conceptoDescripcion',
        'conceptoClave',
        'fechaEmision',
        'saldo',
        'importe'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/dependencia/estado-de-cuenta/por-dependencia'
    },
});