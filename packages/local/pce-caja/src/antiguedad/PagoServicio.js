/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.caja.antiguedad.PagoServicio', {
    url: '/api/caja/antiguedad/pagar',
    pagar(periodos, datos) {
        let pagos = periodos.map(periodo => {
            return {periodoId: periodo.getId()};
        });
        datos.periodos = pagos;
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: this.url,
                method: 'POST',
                jsonData: datos,
                success: (response) => {
                    resolve(JSON.parse(response.responseText));
                },
                failure(response) {
                    reject(JSON.parse(response.responseText));
                }
            });
        });
    }
});