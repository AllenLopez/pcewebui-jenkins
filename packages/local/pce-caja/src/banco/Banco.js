Ext.define('Pce.caja.banco.Banco', {
    extend: 'Ext.data.Model',
    
    fields: [
        'bancoNombre',
        'descripcion',
        'claveBancoSat',
        'activo',
        'reporte',
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],
    
    proxy: {
        type: 'rest',
        url: '/api/caja/banco',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});