Ext.define('Pce.caja.banco.DatosBancoPagare', {
    extend: 'Ext.data.Model',
    
    fields: [
        'id',
        'banco',
        'numeroCuenta',
    ],
    
    proxy: {
        type: 'rest',
        url: '/api/prestamos/pagare/datos-banco',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});