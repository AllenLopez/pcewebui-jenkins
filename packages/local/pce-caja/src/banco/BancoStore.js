Ext.define('Pce.caja.banco.BancoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.banco',
    model: 'Pce.caja.banco.Banco'
});