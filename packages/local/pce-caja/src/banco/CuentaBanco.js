Ext.define('Pce.caja.banco.CuentaBanco', {
    extend: 'Ext.data.Model',

    fields: [
        'bancoId',
        'numeroCuenta',
        'descripcion',
        'clabeInterbancaria',
        'estatus',
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/caja/cuenta-banco',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});