Ext.define('Pce.caja.banco.DatosBancoPagareStore', {
    extend: 'Ext.data.Store',
    alias: 'store.DatosbancoPagare',
    model: 'Pce.caja.banco.DatosBancoPagare'
});