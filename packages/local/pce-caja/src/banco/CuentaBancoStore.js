Ext.define('Pce.caja.banco.CuentaBancoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cuenta-banco',
    model: 'Pce.caja.banco.CuentaBanco',

    porBanco(bancoId) {
        this.load({
            params: {
                bancoId: bancoId
            }
        });
    }
});