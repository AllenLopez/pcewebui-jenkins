Ext.define('Pce.caja.prestamos.Pagare', {
    extend: 'Ext.data.Model',
    
    fields: [
        'id',
        'numeroAfil',
        'conceptoId',
        'descripcionConcepto',
        'saldo',
        'descuento'
    ],
    
    proxy: {
        type: 'rest',
        url: '/api/prestamos/pagare',
        writer: {
            writeAllFields: true
        }
    }
});