Ext.define('Pce.caja.prestamos.PagareStore', {
    extend: 'Ext.data.Store',
    alias: 'store.Pagare',
    model: 'Pce.caja.prestamos.Pagare'
});