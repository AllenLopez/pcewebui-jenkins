Ext.define('Pce.caja.prestamos.EmisionPagare', {
    extend: 'Ext.data.Model',
    
    fields: [
        'id',
        'pagareId',
        'numAfil',
        'beneficiario',
        'importe',
        'estatus',
        'numCheque',
        'numeroCuenta',
        'banco',
        'cuentaBancariaId',
        'fechaExpedicion',
        'fechaImpresion',
        'usuario'
    ],
    
    proxy: {
        type: 'ajax',
        url: '/api/prestamos/pagare/consulta-emision-cheque-pagare',
        writer: {
            writeAllFields: true
        }
    }
});