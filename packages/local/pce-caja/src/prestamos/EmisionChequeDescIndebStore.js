Ext.define('Pce.caja.prestamos.EmisionDescIndebStore', {
    extend: 'Ext.data.Store',
    alias: 'store.EmisionChequeDescIndeb',
    model: 'Pce.caja.prestamos.EmisionChequeDescIndeb'
});