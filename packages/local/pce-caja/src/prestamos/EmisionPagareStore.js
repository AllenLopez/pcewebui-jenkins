Ext.define('Pce.caja.prestamos.EmisionPagareStore', {
    extend: 'Ext.data.Store',
    alias: 'store.EmisionPagare',
    model: 'Pce.caja.prestamos.EmisionPagare'
});