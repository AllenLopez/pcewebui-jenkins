Ext.define('Pce.caja.prestamos.EmisionChequeDescIndeb', {
    extend: 'Ext.data.Model',
    
    fields: [
        'id',
        'numAfil',
        'beneficiario',
        'importe',
        'etapa',
        'numCheque',
        'numeroCuenta',
        'banco',
        'cuentaBancariaId',
        'fechaMovimiento',
        'fechaImpresion',
        'usuarioImpresion'

    ],
    
    proxy: {
        type: 'ajax',
        url: '/api/ingresos/asegurado/descuento-indebido/consulta-emision-cheque',
        writer: {
            writeAllFields: true
        }
    }
});