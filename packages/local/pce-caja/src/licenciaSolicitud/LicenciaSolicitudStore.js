Ext.define('Pce.caja.licenciaSolicitud.LicenciaSolicitudStore', {
    extend: 'Ext.data.Store',
    alias: 'store.licencia-solicitud',

    model: 'Pce.caja.licenciaSolicitud.LicenciaSolicitud'
});