Ext.define('Pce.caja.view.main.MainViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.caja-main',

    data: {
        aplicacionNombre: 'Caja'
    }
});