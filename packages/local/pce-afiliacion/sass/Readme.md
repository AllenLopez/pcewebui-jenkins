# pce-afiliacion/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    pce-afiliacion/sass/etc
    pce-afiliacion/sass/src
    pce-afiliacion/sass/var
