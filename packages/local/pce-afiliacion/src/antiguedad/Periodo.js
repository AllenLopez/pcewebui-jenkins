/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.antiguedad.Periodo', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.proxy.Rest'],

    fields: [
        'estatusPago',
        'formaPago',
        'aportacion',
        'subsidio',
        'retiroActualizado',
        {name: 'desde', type: 'date'},
        {name: 'hasta', type: 'date'},
        {name: 'fechaLimitePago', type: 'date'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/antiguedad/periodo'
    }
});