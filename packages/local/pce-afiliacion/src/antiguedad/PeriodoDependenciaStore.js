/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.antiguedad.PeriodoDependenciaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.antiguedad-dependencia-periodos',
    model: 'Pce.afiliacion.antiguedad.PeriodoDependencia'
});