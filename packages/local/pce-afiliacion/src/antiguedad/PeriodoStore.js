/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.antiguedad.PeriodoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.antiguedad-periodos',
    model: 'Pce.afiliacion.antiguedad.Periodo'
});