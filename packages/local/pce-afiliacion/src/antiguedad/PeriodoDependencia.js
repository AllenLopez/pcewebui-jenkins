/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.antiguedad.PeriodoDependencia', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.proxy.Rest'],

    fields: [
        'periodo',
        'mes',
        'numAfiliacion',
        'cantPagar',
    ],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/antiguedad/periodo/periodo-dependencia'
    }
});