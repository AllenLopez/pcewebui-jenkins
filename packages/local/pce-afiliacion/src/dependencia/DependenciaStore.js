Ext.define('Pce.afiliacion.dependencia.DependenciaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dependencias',

    model: 'Pce.afiliacion.dependencia.Dependencia'
});