Ext.define('Pce.afiliacion.dependencia.SubmotivoNomina', {
    extend: 'Ext.data.Model',

    fields: ['Descripcion'],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/submotivoNomina'
    }
});