/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.dependencia.PoderStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dependencia-poder',
    model: 'Pce.afiliacion.dependencia.Poder'
});
