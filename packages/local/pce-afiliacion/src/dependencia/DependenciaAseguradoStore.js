Ext.define('Pce.afiliacion.dependencia.DependenciaAseguradoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dependencia-asegurado',

    model: 'Pce.afiliacion.dependencia.DependenciaAsegurado'
});