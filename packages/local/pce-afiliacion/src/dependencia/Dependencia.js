Ext.define('Pce.afiliacion.dependencia.Dependencia', {
    extend: 'Ext.data.Model',

    requires: [
        'Pce.afiliacion.dependencia.SubmotivoNomina',
        'Pce.afiliacion.dependencia.Poder'
    ],

    fields: [
        'numeroDependencia',
        'clave',
        'descripcion',
        'correo',
        'numeroDependenciaAs400',
        'tipoServicioMedico',
        'rfc',
        'razonSocial',
        {name: 'activo', type: 'boolean'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioModificacion', persist: false},
        {name: 'submotivoId', reference: 'Pce.afiliacion.dependencia.SubmotivoNomina'},
        {name: 'poderId', reference: 'Pce.afiliacion.dependencia.Poder'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/dependencia',
        writer: {
            writeAllFields: true
        }
    }
});