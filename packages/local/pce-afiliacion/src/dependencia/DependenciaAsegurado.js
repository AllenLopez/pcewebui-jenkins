Ext.define('Pce.afiliacion.dependencia.DependenciaAsegurado', {
    extend: 'Ext.data.Model',

    fields: [
        'dependenciaDescripcion',
        'numeroDependencia',
        'fechaIngreso',
        'fechaBaja',
        'fechaJubilacion',
        'tipoServicioMedico'
    ],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/dependencia-asegurado/por-asegurado'
    }
});