/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.dependencia.SubmotivoNominaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dependencia-submotivo',
    model: 'Pce.afiliacion.dependencia.SubmotivoNomina'
});