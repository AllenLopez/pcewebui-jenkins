Ext.define('Pce.afiliacion.dependencia.Poder', {
    extend: 'Ext.data.Model',

    fields: ['descripcion', 'clave'],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/poder'
    }
});