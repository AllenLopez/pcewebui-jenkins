Ext.define('Pce.afiliacion.delegacion.Delegacion', {
    extend: 'Ext.data.Model',

    fields: ['descripcion'],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/delegacion/'
    }
});