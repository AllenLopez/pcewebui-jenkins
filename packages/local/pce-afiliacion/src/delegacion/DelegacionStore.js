Ext.define('Pce.afiliacion.delegacion.DelegacionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.delegacion',

    model: 'Pce.afiliacion.delegacion.Delegacion'
});