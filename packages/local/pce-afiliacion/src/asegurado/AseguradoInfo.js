Ext.define('Pce.afiliacion.asegurado.AseguradoInfo', {
    extend: 'Ext.data.Model',

    fields: [
        'jubilado',
        'delegacionId',
        'delegacion',
        'regimen',
        'claveRegimen',
        'estatus',
        'conceptoId',
        'factor80'
    ],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/asegurado/info'
    }
});