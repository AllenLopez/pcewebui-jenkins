Ext.define('Pce.afiliacion.asegurado.Asegurado', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        {name: 'numeroAfiliacion', type: 'int'},
        'parentesco',
        'apellidoPaterno',
        'apellidoMaterno',
        'nombre',
        'rfc',
        'curp',
        'direccion',
        'colonia',
        'codigoPostal',
        'telefono',
        'telefono2',
        'numDerechohabiente',
        'fechaNacimiento',
        'edad',
        {
            name: 'nombreCompleto',
            convert(v, model){
                let data = model.data;
                return `${data.nombre} ${data.apellidoPaterno} ${data.apellidoMaterno}`;
            }
        }
    ],

    getInfo() {
        return new Promise((resolve, reject) => {
            Pce.afiliacion.asegurado.AseguradoInfo.load(this.getId(), {
                success: info => {resolve(info);},
                failure: reject
            });
        });
    },

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/asegurado'
    }
});