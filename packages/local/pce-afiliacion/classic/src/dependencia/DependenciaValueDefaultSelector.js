Ext.define('Pce.afiliacion.dependencia.DependenciaValueDefaultSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'dependencia-default-selector',
    requires: [
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    fieldLabel: 'Dependencia',
    displayField: 'descripcion',
    queryMode: 'local',
    forceSelection: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{descripcion} [{submotivoDescripcion}]</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{descripcion} [{submotivoDescripcion}]',
        '</tpl>'
    ],

    store: {
        type: 'dependencias',
        autoLoad: true,
        filters: [{
            property: 'activo',
            value: true
        }]
    }
});