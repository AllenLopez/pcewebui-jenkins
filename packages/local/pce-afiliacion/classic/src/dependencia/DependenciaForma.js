/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.dependencia.DependenciaForma', {
    extend: 'Ext.form.Panel',
    xtype: 'dependencia-forma',

    requires: [
        'Pce.afiliacion.dependencia.PoderSelector',
        'Pce.afiliacion.dependencia.SubmotivoNominaSelector'
    ],

    layout: {
        type: 'table',
        columns: 3
    },
    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'textfield',
        fieldLabel: 'Descripción',
        name: 'descripcion',
        allowBlank: false,
        colspan: 2
    }, {
        xtype: 'checkbox',
        fieldLabel: 'Activa',
        name: 'activo',
        uncheckedValue: false
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Clave',
        name: 'clave',
        allowBlank: false
    }, {
        xtype: 'textfield',
        fieldLabel: 'Clave AS400',
        name: 'numeroDependenciaAs400'
    }, {
        xtype: 'combobox',
        fieldLabel: 'Tipo serv. méd.',
        name: 'tipoServicioMedico',
        allowBlank: false,
        forceSelection: true,
        valueField: 'tipo',
        displayField: 'tipo',
        value: 'P',
        store: {
            fields: ['tipo'],
            data: [{tipo: 'P'}, {tipo: 'I'}, {tipo: 'A'}]
        }
    }, {
        xtype: 'dependencia-submotivo-selector',
        allowBlank: false,
        name: 'submotivoId',
        colspan: 3
    }, {
        xtype: 'dependencia-poder-selector',
        allowBlank: false,
        name: 'poderId',
        colspan: 3
    }],

    loadRecord(record) {
        this.callParent([record]);

        this.down('[name="clave"]').setDisabled(!record.phantom);
    }
    
});