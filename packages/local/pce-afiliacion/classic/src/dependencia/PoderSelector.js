/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.dependencia.PoderSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'dependencia-poder-selector',

    requires: [
        'Pce.afiliacion.dependencia.PoderStore'
    ],

    fieldLabel: 'Poder',
    valueField: 'id',
    displayField: 'descripcion',
    queryMode: 'local',
    forceSelection: true,

    store: {
        type: 'dependencia-poder',
        autoLoad: true
    }
});