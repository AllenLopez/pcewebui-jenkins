Ext.define('Pce.afiliacion.dependencia.DependenciaSelectorMultiple', {
    extend: 'Ext.form.field.Tag',
    xtype: 'dependencia-selector-multiple',

    valueField: 'id',
    displayField: 'descripcion',
    queryMode: 'local',
    filterPickList: true,
    forceSelection: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{descripcion} [{submotivoDescripcion}]</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{descripcion} [{submotivoDescripcion}]',
        '</tpl>'
    ],
});