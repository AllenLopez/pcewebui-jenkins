/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.dependencia.DependenciaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'dependencia-tabla',

    requires: [
        'Pce.afiliacion.dependencia.DependenciaForma',
        'Pce.view.dialogo.FormaContenedor',
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    referenceHolder: true,
    defaultListenerScope: true,
    store: {
        type: 'dependencias',
        autoLoad: {
            params: {inactivos: 'true'}
        }
    },

    title: 'Catálogo de dependencias',
    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarDependencia',
        tooltip:'Crear una nueva dependencia.'
    }],

    columns: [{
        text: 'Núm. dependencia',
        dataIndex: 'id',
        tooltip:'Numero identificador único de la dependencia.',
        flex: 1
    }, {
        text: 'Núm. dep. AS400',
        dataIndex: 'numeroDependenciaAs400',
        tooltip:'Numero identificador de la dependencia en sistema AS400.',
        flex: 1
    }, {
        text: 'Clave',
        dataIndex: 'clave',
        tooltip:'Clave armonizada de la dependencia.',
        flex: 1
    }, {
        text: 'Descripción',
        dataIndex: 'descripcion',
        tooltip:'Descripción de la dependencia.',
        flex: 4
    }, {
        text: 'Submotivo',
        dataIndex: 'submotivoDescripcion',
        tooltip:'Indica el tipo de nomina de la dependencia.',
        flex: 2
    }, {
        text: 'Tipo serv. méd.',
        dataIndex: 'tipoServicioMedico',
        tooltip:'Indica el tipo de servicio médico de la dependencia.',
        flex: 1
    }, {
        text: 'Estatus',
        dataIndex: 'activo',
        tooltip:'Estatus en el que se encuentra la dependencia.',
        renderer(v) {
            let cls = v ? 'check' : 'times';
            return `<span class="x-fa fa-${cls}"></span>`;
        }
    },{
        text: 'Creado en',
        dataIndex: 'fechaModificacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
        flex: 1
    },{
        text: 'Creado por', 
        dataIndex: 'usuarioModificacion',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar dependencia',
            handler: 'onEditarDependencia'
        },
        // {
        //     iconCls: 'x-fa fa-trash',
        //     tooltip: 'Eliminar dependencia',
        //     handler: 'onEliminarDependencia'
        // }
    ]
    }],

    onAgregarDependencia() {
        this.mostrarDialogoEdicion(new Pce.afiliacion.dependencia.Dependencia({}));
    },
    
    onEditarDependencia(view, row, col, item, e, record) {
        this.mostrarDialogoEdicion(record);
    },
    onEliminarDependencia(view, row, col, item, e, record) {
        let store = this.getStore();
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar esta dependencia? 
            <i>${record.get('descripcion')}</i>`,
            ans => {
                if(ans === 'yes'){
                    record.erase({
                        success(){
                            Ext.toast('La dependencia seleccionada se ha eliminado ');
                        },
                        failure(batch, operations){
                            store.rejectChanges();
                            Ext.toast(operations.error.response.responseText);
                            if(!store.contains(record)){
                                store.add(record);
                            }
                        }
                    });
                }
            }
        );
    },

    mostrarDialogoEdicion(dependencia) {
        let store = this.getStore();
        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title: dependencia.phantom ? 'Nueva dependencia' : 'Editar dependencia',
            items: {xtype: 'dependencia-forma'},
            width: 600,
            record: dependencia,
            listeners: {
                guardar(dialogo) {
                    Ext.Msg.confirm(
                        'Guardar dependencia',
                        `¿Seguro que deseas guardar la dependencia <i>${dependencia.get('descripcion')}</i>?`,
                        ans => {
                            if(ans === 'yes') {
                                dependencia.save({
                                    success() {
                                        if(!store.contains(dependencia)){
                                            store.add(dependencia);
                                        }
                                        Ext.toast('Los cambios se han guardado correctamente');
                                        dialogo.close();
                                    },
                                });
                            }
                        }
                    );
                }
            }
        });
    }
});