/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.afiliacion.dependencia.SubmotivoNominaSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'dependencia-submotivo-selector',

    requires: [
        'Pce.afiliacion.dependencia.SubmotivoNominaStore'
    ],

    fieldLabel: 'Submotivo nómina',
    valueField: 'id',
    displayField: 'descripcion',
    queryMode: 'local',
    forceSelection: true,

    store: {
        type: 'dependencia-submotivo',
        autoLoad: true
    }
});