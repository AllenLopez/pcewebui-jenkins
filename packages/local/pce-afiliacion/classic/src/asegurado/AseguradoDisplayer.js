Ext.define('Pce.afiliacion.asegurado.AseguradoDisplayer', {
    extend: 'Ext.Component',
    xtype: 'asegurado-displayer',

    bind: {
        html:
        '<table class="asegurado-displayer">' +
        '<tbody>' +
            '<tr>' +
                '<td><b>Número de afiliación:</b> {afiliado.numeroAfiliacion}</td>' +
                '<td>|</td>' +
                // '<td><b>Nombre completo:</b> {afiliado.nombre} {afiliado.apellidoPaterno} {afiliado.apellidoMaterno}</td>' +
                '<td><b>Nombre completo:</b> {afiliado.nombreCompleto}</td>' +
                '<td>|</td>' +
                '<td><b>RFC:</b> {afiliado.rfc}</td>' +
                '<td>|</td>' +
                '<td><b>Dirección:</b> {afiliado.direccion}</td>' +
            '</tr>' +
        '</tbody>' +
        '</table>'
    }
});