/**
 * @author Rubén Maguregui
 *
 * @class
 * @name Pce.afiliacion.asegurado.AseguradoSelector
 */
Ext.define('Pce.afiliacion.asegurado.AseguradoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'asegurado-selector',
    requires: [
        'Pce.afiliacion.asegurado.Asegurado'
    ],
    fieldLabel: 'Asegurado',
    valueField: 'id',
    forceSelection: true,
    minChars: 4,
    queryParam: 'term',
    hideTrigger: true,
    width: 600,
    emptyText: 'Nombre completo, número de afiliación o RFC',

    store: {
        type: 'store',
        model: 'Pce.afiliacion.asegurado.Asegurado',
        proxy: {
            type: 'ajax',
            url: '/api/afiliacion/asegurado/buscar'
        }
    },
    displayTpl: [
        '<tpl for=".">',
            // '{nombre} {apellidoPaterno} {apellidoMaterno}',
            '{nombreCompleto}',
        '</tpl>'
    ],
    tpl: [
            '<tpl for=".">',
                '<li role="option" class="x-boundlist-item" ' +
                    'style="display: flex; align-items: center; line-height: 0; padding: 0">',
                    '<div style="width: 2.5cm; margin-right: 10px">' +
                        '<div>' +
                            '<img style="width: 100%;" src="/api/afiliacion/asegurado/fotografia/{id}" />' +
                        '</div>' +
                    '</div>',
                    '<div style="text-align: left; flex-grow: 1; line-height: 20px;">' +
                        // '<b>Nombre: </b>{nombre} {apellidoPaterno} {apellidoMaterno} <br />' +
                        'Nombre: </b>{nombreCompleto} <br />' +
                        '<b>Núm. Afil: </b>{id} <br />' +
                        '<b>RFC: </b>{rfc}' +
                    '</div>',
                '</li>',
            '</tpl>',
    ]
});