/**
 * @author Felipe Fierro
 *
 * @class
 * @name Pce.afiliacion.asegurado.AseguradoSelectorMultiple
 */
Ext.define('Pce.afiliacion.asegurado.AseguradoSelectorMultiple', {
    extend: 'Ext.form.field.Tag',
    xtype: 'asegurado-selector-multiple',
    requires: [
        'Pce.afiliacion.asegurado.Asegurado'
    ],
    valueField: 'id',
    forceSelection: true,
    // minChars: 4,
    queryParam: 'term',
    hideTrigger: true,
    filterPickList: true,
    emptyText: 'Nombre completo, número de afiliación o RFC',
    beforeQuery: (queryPlan, opts) => {
        console.log(queryPlan);
        console.log(opts);
    },

    store: {
        type: 'store',
        model: 'Pce.afiliacion.asegurado.Asegurado',
        proxy: {
            type: 'ajax',
            url: '/api/afiliacion/asegurado/buscar'
        }
    },
    displayTpl: [
        '<tpl for=".">',
            '{nombre} {apellidoPaterno} {apellidoMaterno}',
        '</tpl>'
    ],
    tpl: [
            '<tpl for=".">',
                '<li role="option" class="x-boundlist-item" ' +
                    'style="display: flex; align-items: center; line-height: 0; padding: 0">',
                    '<div style="width: 2.5cm; margin-right: 10px">' +
                        '<div>' +
                            '<img style="width: 100%;" src="/api/afiliacion/asegurado/fotografia/{id}" />' +
                        '</div>' +
                    '</div>',
                    '<div style="text-align: left; flex-grow: 1; line-height: 20px;">' +
                        // '<b>Nombre: </b>{nombre} {apellidoPaterno} {apellidoMaterno} <br />' +
                        '<b>Nombre: </b>{nombreCompleto} <br />' +
                        '<b>Núm. Afil: </b>{id} <br />' +
                        '<b>RFC: </b>{rfc}' +
                    '</div>',
                '</li>',
            '</tpl>',
    ]
});