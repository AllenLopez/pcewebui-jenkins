# pce-seguridad/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    pce-seguridad/sass/etc
    pce-seguridad/sass/src
    pce-seguridad/sass/var
