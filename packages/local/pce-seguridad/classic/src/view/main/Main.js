Ext.define('Pce.seguridad.view.main.Main', {
    extend: 'Pce.view.main.Main',
    xtype: 'seguridad-main',

    requires: [
        'Pce.seguridad.view.main.MainController'
    ],

    controller: 'seguridad-main',
});