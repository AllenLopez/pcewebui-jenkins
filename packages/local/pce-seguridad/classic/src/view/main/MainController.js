Ext.define('Pce.seguridad.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.seguridad-main',

    requires: [
        'Pce.seguridad.login.LoginPagina'
    ],

    routes: {
        'main': 'main',
        'login': 'login'
    },

    main() {

    },

    login() {
        this.view.renderPage({
            xtype: 'seguridad-login-pagina'
        });
    }
});