Ext.define('Pce.seguridad.login.LoginPagina', {
    extend: 'Ext.panel.Panel',
    xtype: 'seguridad-login-pagina',

    requires: [
        'Pce.seguridad.login.LoginPaginaController',
        'Pce.seguridad.login.LoginForma'
    ],

    layout: {
        type: 'center',
        align: 'stretch'
    },

    controller: 'seguridad-login-pagina',
    
    items: [{
        xtype: 'login-forma',
        reference: 'loginForma',
        flex: 1,
        bodyPadding: 30
    }]
});