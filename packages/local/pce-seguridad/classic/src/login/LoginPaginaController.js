Ext.define('Pce.seguridad.login.LoginPaginaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.seguridad-login-pagina',
    requires: [
        'Pce.seguridad.login.LoginForma'
    ],

    init: function(){
        Ext.Ajax.request({
            url: '/api/seguridad/autenticacion/estatus',
            method: 'GET',
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                if(resultado.estatus == "authenticado"){
                    window.location = resultado.redireccion;
                }
            }
        });
    },
    // afterRender(){
    //     this.cargaLogin();
    // },

    // cargaLogin(){
    //     let vista = this.getView();
    //     // let store = tabla.getStore();

    //     // store.Load();
    // }
});