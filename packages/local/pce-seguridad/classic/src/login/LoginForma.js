Ext.define('Pce.seguridad.login.LoginForma', {
    extend: 'Ext.form.Panel',
    xtype: 'login-forma',
    requires: [
        'Ext.form.field.Text',
    ],
    layout: {
        type: 'vbox',
        align: 'stretch'
        //center
        //standar submit
        //method: post
        //url
    },

    title: 'Inicio de sesión',
    // standardSubmit: true,
    method: 'POST',
    url: '/api/seguridad/autenticacion/login',

    defaultListenerScope: true,

    items: [{
        xtype: 'textfield',
        name: 'usuario',
        fieldLabel: 'Usuario',
        allowBlank: false
    }, {
        xtype: 'textfield',
        name: 'password',
        fieldLabel: 'Password',
        allowBlank: false,
        inputType: 'password'
    }],

    buttons: [{
        text: 'Entrar',
        handler: 'onEntrarClick'
    }],

    onEntrarClick() {
        if (this.isValid()) {
            this.submit({
                success(request,response) {
                    let data = response.result;
                    if(data.success){
                        window.location = data.redireccion;
                    }
                },
                failure(request, response){
                    let data = response.result;
                    Ext.Msg.alert(
                        'Error al iniciar sesión',
                        data.message
                    );
                }
            });
        }
    }
});