/**
 * @class Pce.ingresos.poliza.ResultadoPoliza
 * @extends Ext.data.Model
 * @xtype model
 * Modelo de datos para entidad in_poliza_resultado que muestra detalles del resultado de error para generar póliza
 */
Ext.define('Pce.poliza.ResultadoPoliza', {
    extend: 'Ext.data.Model',
    
    fields: [
        'polizaFolio',
        'fechaPoliza',
        'cuenta',
        'descripcion',
        'debe',
        'haber',
        'dependencia',
        'conceptoDescripcion',
        'fechaNomina',
        'nombreAsegurado',
        'numeroAfiliacion',
        'resultadoDescripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/poliza/resultado'
    }
});