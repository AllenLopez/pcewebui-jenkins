Ext.define('Pce.poliza.PolizaDetalleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.poliza-detalle',

    model: 'Pce.poliza.PolizaDetalle',

    porPoliza(polizaId) {
        this.load({
            params: {
                polizaId: polizaId
            }
        });
    }
});