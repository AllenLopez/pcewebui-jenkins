Ext.define('Pce.poliza.PolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.poliza',

    model: 'Pce.poliza.Poliza',

    porFiltro(filtro) {
        this.load({
            params: {
                filtro: filtro
            }
        });
    }
});