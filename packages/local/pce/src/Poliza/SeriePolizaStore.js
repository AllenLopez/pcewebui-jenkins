Ext.define('Pce.poliza.SeriePolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.series-poliza',
    
    model: 'Pce.poliza.SeriePoliza'
});