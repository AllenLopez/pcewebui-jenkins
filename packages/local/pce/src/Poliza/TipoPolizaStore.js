Ext.define('Pce.poliza.TipoPolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tipos-poliza',
    
    model: 'Pce.poliza.TipoPoliza'
});