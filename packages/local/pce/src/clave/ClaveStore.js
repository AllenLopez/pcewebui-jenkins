Ext.define('Pce.clave.ClaveStore', {
    extend: 'Ext.data.Store',
    alias: 'store.claves',
    model: 'Pce.clave.Clave'
});