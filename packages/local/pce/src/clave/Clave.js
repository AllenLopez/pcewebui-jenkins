Ext.define('Pce.clave.Clave', {
    extend: 'Ext.data.Model',

    fields: [
        'tabla',
        'columna',
        'valor',
        'descripcion',
        {name: 'activo', type: 'boolean'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/base/clave',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});