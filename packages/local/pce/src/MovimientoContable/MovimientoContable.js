/**
 * @class Pce.ingresos.movimientoContable.MovimientoContable
 * @extends Ext.data.Model
 * Modelo de datos para entidad MovimientoContable
 */
Ext.define('Pce.movimientoContable.MovimientoContable', {
    extend: 'Ext.data.Model',
    
    fields: [
        'tipoPoliza',
        'seriePoliza',
        'tipoAfectacion',
        'estatus',
        'origenMovimiento',
        'procedimientoAlmacenado',
        'sistema',
        'movimientoDescripcion',
        'tipoMovimientoId',
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/movimiento-contable',

        writer: {
            type: 'json',
            writeAllFields: true
        },

        reader: {
            type: 'json'
        }
    }
});