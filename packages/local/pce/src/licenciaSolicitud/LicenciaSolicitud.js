Ext.define('Pce.licenciaSolicitud.LicenciaSolicitud', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'tipoLicencia',
        'afiliado',
        'numDep',
        'dependencia',
        {name: 'fechaInicio', type: 'date', persist: false},
        'dias',
        {name: 'fechaFin', type: 'date', persist: false},
        'importe',
    ],

    proxy: {
        type: 'rest',
        url: '/api/caja/pago-licencia/licencias',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});