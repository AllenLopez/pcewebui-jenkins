Ext.define('Pce.licenciaSolicitud.LicenciaSolicitudStore', {
    extend: 'Ext.data.Store',
    alias: 'store.licencia-solicitud',

    model: 'Pce.licenciaSolicitud.LicenciaSolicitud'
});