Ext.define('Pce.asegurado.procesos.devolucionFondo.ChequeAdeudoSMHelper', {
    singleton: true,

    onCrearChequeServicioMedico(cheque) {
        // Ext.getBody().mask();
        if (cheque.phantom) {
            this.guardaChequeServicioMedico(cheque)
                .then(resp => resp)
                .otherwise(err => this.alertaError(err.errorMessage));
        }
    },

    guardaChequeServicioMedico(cheque) {
        const deferred = new Ext.Deferred();
        cheque.save({
            success() { deferred.resolve(`cheque creado correctamente`); },
            failure(record, batch) {
                const error = batch.getExceptions()[0].getError();
                const msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                deferred.reject(msg);
            }
        });
        return deferred.promise;
    },

    alertaError(msgError){
        Ext.Msg.alert('Error en operación',
            `Ocurrió un error al intentar actualizar: ${msgError}`);
    }
});