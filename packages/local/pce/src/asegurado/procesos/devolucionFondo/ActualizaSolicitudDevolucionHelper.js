Ext.define('Pce.asegurado.procesos.devolucionFondo.ActualizaSolicitudDevolucionHelper', {
    singleton: true,

    conceptosMatriz: {'H': 'TR', 'A': 'LA', 'I': 'CI', 'J': 'CI'},

    onActualizarSolicitud(solicitud, chequesStore) {
        Ext.getBody().mask('actualizando...');
        if (solicitud.dirty) {
            this.actualizarSolicitud(solicitud)
                .then(resp => Ext.Msg.alert('Operacion finalizada', `${resp}`))
                .otherwise(err => this.alertaError(err.errorMessage) );
        }
        if (chequesStore.needsSync) {
            this.actualizaCheque(chequesStore)
                .then(resp => Ext.Msg.alert('Operacion finalizada', `${resp}`))
                .otherwise(err => this.alertaError(err.errorMessage));
        }
        Ext.getBody().unmask();
    },

    confirmarActualizacion(solicitudId) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `La solicitud con folio <b>${solicitudId}</b> ha sido modificada, ¿Desea guardar los cambios?`,
                ans => resolve(ans));
        });
    },

    actualizarSolicitud(solicitud) {
        const deferred = new Ext.Deferred();
        solicitud.save({
            success() {
                deferred.resolve(`solicitud ${solicitud.getId()} actualizada correctamente`);
            },
            failure(record, batch) {
                const error = batch.getExceptions()[0].getError();
                const msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                deferred.reject(msg);
            }
        });
        return deferred.promise;
    },

    actualizaCheque(chequesStore) {
        const deferred = new Ext.Deferred();
        chequesStore.sync({
            success(rec) {
                console.log(rec);
                deferred.resolve(`cheques actualizados correctamente`);
            },
            failure(batch) {
                const error = batch.getExceptions()[0].getError();
                const msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                deferred.reject(msg);
            }
        });
        return deferred.promise;
    },

    /**
     * Consulta movimientos de asegurado para determinar si es jubilado recontratado
     * @param aseguradoId
     * @param info
     * @returns promesa con objeto info reescribiendo valor de determinación si es jubilado recontratado
     */
    determinaJubilacionAsegurado(aseguradoId, info) {
        const deferred = new Ext.Deferred();
        if (info && info.get('jubilado') === 'SI') {
            Ext.Ajax.request({
                method: 'GET',
                url: '/api/ingresos/asegurado/movimiento/movimientos-postbaja',
                params: { aseguradoId: aseguradoId },
                success(resp) {
                    if (resp.responseText !== 0) {
                        info.set('jubilado', 'RECONTRATADO');
                        deferred.resolve(info);
                    }
                },
                failure(batch) {
                    const error = batch.getExceptions()[0].getError();
                    const msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                    deferred.reject(msg);
                }
            });
            return deferred.promise;
        }
        deferred.resolve(info);
        return deferred.promise
    },

    findHomologoClaveConcepto(clave) {
        // busca concepto en matriz H(TR), I(CI), A(LA)
        return Object.keys(this.conceptosMatriz)
            .find(key =>
                this.conceptosMatriz[key] === clave
            );
    },

    alertaError(msgError){
            Ext.Msg.alert('Error en operación',
                `Ocurrió un error al intentar actualizar: ${msgError}`);
    }
});