Ext.define('Pce.asegurado.procesos.descuentoIndebido.ActualizaSolicitudDescIndebidoHelper', {
    singleton: true,

    onActualizarSolicitud(solicitud) {
        Ext.getBody().mask('actualizando...');
        if (solicitud.dirty) {
            this.actualizarSolicitud(solicitud)
                .then(resp => Ext.Msg.alert('Operacion finalizada', `${resp}`))
                .otherwise(err => this.alertaError(err.errorMessage) );
        }
        Ext.getBody().unmask();
    },

    confirmarActualizacion(solicitudId) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `La solicitud con folio <b>${solicitudId}</b> ha sido modificada, ¿Desea guardar los cambios?`,
                ans => resolve(ans));
        });
    },

    actualizarSolicitud(solicitud) {
        const deferred = new Ext.Deferred();
        solicitud.save({
            success() {
                deferred.resolve(`Solicitud <b>${solicitud.getId()}</b> actualizada correctamente`);
            },
            failure(record, batch) {
                const error = batch.getExceptions()[0].getError();
                const msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                deferred.reject(msg);
            }
        });
        return deferred.promise;

    },
    determinaJubilacionAsegurado(aseguradoId, info) {
        if (info && info.get('jubilado') === 'SI') {
            const deferred = new Ext.Deferred();

            Ext.Ajax.request({
                method: 'GET',
                url: '/api/ingresos/asegurado/movimiento/movimientos-postbaja',
                params: { aseguradoId: aseguradoId },
                success(resp) {
                    if (resp !== 0) {
                        info.set('jubilado', 'RECONTRATADO');
                        deferred.resolve(info);
                    }
                },
                failure(batch) {
                    const error = batch.getExceptions()[0].getError();
                    const msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                    deferred.reject(msg);
                }
            });

            return deferred.promise;
        }
    },

    alertaError(msgError){
            Ext.Msg.alert('Error en operación',
                `Ocurrió un error al intentar actualizar: ${msgError}`);
    }
});