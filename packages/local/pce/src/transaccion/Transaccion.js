Ext.define('Pce.transaccion.Transaccion', {
    extend: 'Ext.data.Model',

    fields: [
        'interfazId',
        {name: 'fechaMovimiento', type: 'date'},
        'etapa',
        {name: 'fechaCreacion', type: 'date'},
        {name: 'fechaModificacion', type: 'date'},
        'usuarioCreacion',
        'usuarioModificacion',
        'origenClave',
        'OrigenDescripcion',
        'cancelado',
        'idTransaccionOrigen',
        'origenClaveTransaccionOrigen',
        'descripcionTransaccionOrigen',
        {
         name: 'cancelado',
         convert: (v, transaccion) =>{
             return transaccion.get('cancelado') ? 'SI' : 'NO';
         }   
        },
        {
            name: 'etapaDescripcion',
            convert: (v, transaccion) => {
                let etapa = transaccion.get('etapa');
                switch (etapa) {
                    case 'TR': return 'En trámite';
                    case 'CT': return 'En control';
                    case 'RE': return 'Rechazada';
                    case 'AU': return 'Autorizada';
                    case 'RC': return 'Rechazada en control';
                    default: return 'Otra';
                }
            }
        }
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/transaccion'
    }
});