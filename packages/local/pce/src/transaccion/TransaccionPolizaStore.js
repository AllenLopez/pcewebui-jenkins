Ext.define('Pce.transaccion.TransaccionPolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.transacciones-polizas',
    model: 'Pce.transaccion.TransaccionPoliza'
});