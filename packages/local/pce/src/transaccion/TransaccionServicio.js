Ext.define('Pce.transaccion.TransaccionServicio', {
    calculoAportacionesUrl: '/api/ingresos/transaccion/calcular-aportaciones',

    calcularAportaciones(transaccion) {
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: this.calculoAportacionesUrl,
                method: 'GET',
                params: {transaccionId: transaccion.getId()},
                success(resp) {
                    resolve(JSON.parse(resp.responseText));
                },
                failure(resp) {
                    reject(resp);
                }
            });
        });
    }
});