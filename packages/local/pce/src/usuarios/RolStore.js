Ext.define('Pce.configuracion.usuarios.RolStore', {
    extend: 'Ext.data.Store',
    alias: 'store.rol',

    model: 'Pce.configuracion.usuarios.Rol'
});