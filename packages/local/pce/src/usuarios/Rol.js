Ext.define('Pce.configuracion.usuarios.Rol', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'id',
        'descripcion',
        {name: 'estatus', type: 'boolean'},
        {name: 'usuarioCreacion', persist: false},
        {name: 'fechaCreacion', type: 'date', persist: false},
        'rolId',
    ],

    proxy: {
        type: 'rest',
        url: '/api/seguridad/lca/rol',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});