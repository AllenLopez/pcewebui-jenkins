/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.reporte.ReporteGenerador', {
    url: '/api/ingresos/reporte/generar',

    /**
     * @param {string} reporte
     * @param {object} parametros
     */
    generar(reporte, parametros) {
        let _parametros = JSON.stringify(Object.getOwnPropertyNames(parametros).map(p => {
            return {nombre: p, valor: parametros[p]};
        }));
        window.location = `${this.url}?reporte=${reporte}&parametros=${_parametros}`;
    }
});