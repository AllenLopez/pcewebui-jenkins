Ext.define('Pce.movimiento.TipoMovimientoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'movimiento-selector',

    require: [
        'Pce.movimiento.TipoMovimientoStore'
    ],

    queryMode: 'local',
    valueField: 'id',
    displayField: 'descripcion',
    fieldLabel: 'Tipo movimiento',
    labelWidth: 120,
    forceSelection: true,
    anyMatch: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{descripcion} [{clave}]</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{descripcion} [{clave}]',
        '</tpl>'
    ],

    store: {
        type: 'tipos-movimiento',
        autoLoad: true,
        filters: [{
            property: 'activo',
            value: true
        }, {
            property: 'esManual',
            value: true
        }]
    }
});