Ext.define('Pce.movimiento.TipoMovimientoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tipos-movimiento',

    model: 'Pce.movimiento.TipoMovimiento'
});