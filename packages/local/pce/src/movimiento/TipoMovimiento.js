Ext.define('Pce.movimiento.TipoMovimiento', {
    extend: 'Ext.data.Model',

    fields: [
        'clave',
        'descripcion',
        {name: 'fechaCreacion', type: 'date'},
        'usuarioCreacion',
        {name: 'fechaModificacion', type: 'date'},
        'usuarioModificacion',
        {name: 'activo', type: 'boolean'},
        {name: 'esMovimientoAfiliado', type: 'boolean'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/tipo-movimiento',

        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});