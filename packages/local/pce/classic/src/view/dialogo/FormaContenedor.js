Ext.define('Pce.view.dialogo.FormaContenedor', {
    extend: 'Ext.window.Window',
    xtype: 'dialogo-forma-contenedor',

    defaultListenerScope: true,

    modal: true,
    autoShow: true,

    layout: 'fit',
    bodyPadding: 10,

    buttons: [{
        text: 'Aceptar',
        handler: 'onAceptar'
    }, {
        text: 'Cancelar',
        handler: 'onCancelar'
    }],

    record: undefined,

    afterRender() {
        this.callParent();
        if (this.record) {
            this.down('form').loadRecord(this.record);
        }
    },

    onAceptar() {
        let form = this.down('form');
        if (form.isValid()) {
            form.updateRecord();
            this.fireEvent('guardar', this, this.record, form);
        }
    },

    onCancelar() {
        this.close();
    }
});