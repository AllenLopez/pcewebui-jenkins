Ext.define('Pce.view.main.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'main',

    layout: 'fit',

    afterRender() {
        this.callParent();

        this.setTitle(
            `<img 
                src="/packages/local/pce-theme/resources/img/pce-logo-horizontal-blanco.svg" 
                style="width: 150px; margin-right: 1ex; vertical-align: middle"/>
            <div style="text-align: center; position: relative; top: -25px">${document.title}</div>`
        );
    },

    renderPage(page) {
        this.removeAll(true);
        this.add(page);
    }
});