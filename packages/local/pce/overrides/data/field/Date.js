Ext.define('Ext.override.data.field.Date', {
    override: 'Ext.data.field.Date',
    dateFormat: 'Y-m-d\\TH:i:s'
});