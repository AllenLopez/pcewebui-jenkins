Ext.define('Ext.override.util.Format', {
    override: 'Ext.util.Format',

    decimalSeparator: '.',
    thousandSeparator: ','
});