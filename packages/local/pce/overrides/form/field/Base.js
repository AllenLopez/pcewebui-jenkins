/**
 * @author Rubén Maguregui
 */
Ext.define('Ext.override.form.field.Base', {
    override: 'Ext.form.field.Base',

    msgTarget: 'under'
});