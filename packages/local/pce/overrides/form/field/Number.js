/**
 * @author Rubén Maguregui
 */
Ext.define('Ext.override.form.field.Number', {
    override: 'Ext.form.field.Number',

    hideTrigger: true,
    keyNavEnabled: false,
    mouseWheelEnabled: false
});