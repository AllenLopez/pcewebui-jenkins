Ext.define('Pce.configuracion.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.configuracion-main',

    requires: [
        'Pce.configuracion.Inicio',
        /*'Pce.configuracion.usuarios.alta.AltaUsuarioPanel',
        'Pce.configuracion.usuarios.consulta.ConsultaUsuarioPanel',*/
        'Pce.configuracion.usuarios.roles.UsuarioRolesTabla'
    ],

    routes: {
        // 'main': 'main',
        'usuarios/roles-usuarios': 'usuariosRolesUsuarios',
        /*'usuarios/alta-usuarios': 'usuariosAltaUsuarios',
        'usuarios/consulta-usuarios': 'usuariosConsultaUsuarios',*/
        'cerrar-sesion': 'cerrarSesion'
    },
     beforeRender: function(){
         let me = this;
        Ext.Ajax.request({
            url: '/api/seguridad/autenticacion/estatus?host=/apps/Configuracion/index.html', 
            method: 'GET',
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                if(resultado.estatus === ''){
                    window.location = '/apps/Seguridad/index.html#login';
                    return;
                }
                if(window.location.pathname !== resultado.redireccion){
                    window.location = resultado.redireccion;
                    return;
                }
                let menu = me.lookup('menu');
               
               menu.add(resultado.menu);
            }
        });
            
            
         },
    main() {
        this.view.renderPage({
            xtype: 'inicio'
        });
    },

    cerrarSesion() {
        Ext.Ajax.request({
            url: '/api/seguridad/autenticacion/cerrar-sesion',
            method: 'GET',
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                if(resultado.estatus === 'anonimo'){
                    window.location = '/apps/Seguridad/index.html#login';
                    return;
                }
            }
        });
    },

    /*usuariosAltaUsuarios() {
        this.view.renderPage({
            xtype: 'alta-usuario-panel'
        });
    },

    usuariosConsultaUsuarios() {
        this.view.renderPage({
            xtype: 'usuarios-consulta-panel'
        });
    },*/

    usuariosRolesUsuarios() {
        this.view.renderPage({
            xtype: 'usuarios-roles-tabla'
        });
    },
})