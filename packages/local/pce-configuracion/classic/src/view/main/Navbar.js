/**
 * Barra de navegación para el módulo de configuracion
 */
Ext.define('Pce.configuracion.view.main.Navbar', {
    extend: 'Ext.container.Container',
    xtype: 'navbar-configuracion',
    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Split',
        'Ext.button.Segmented',
        'Ext.menu.Menu'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'component',
        cls: 'site-bar',
        flex: 1
    }, {
        xtype: 'toolbar',
        items: {
            xtype: 'segmentedbutton',
            reference: 'menu',
            items: [  ]
        }
    }]
});