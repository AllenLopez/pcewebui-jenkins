/**
 * @class Pce.configuracion.Inicio
 * @extends Ext.container.Container
 * @xtype inicio
 * Página de inicio de módulo Configuracion
 */
Ext.define('Pce.configuracion.Inicio', {
    extend: 'Ext.Component',
    xtype: 'inicio-configuracion',
    
    margin: 10,
    html: `<b>Bienvenido al Sistema de Configuracion de Pensiones Civiles del Estado de Chihuahua</b>
        <p>El objetivo del Sistema de Configuracion es administrar la cartera de Dependencias afiliadas y asegurados en base a las retenciones de seguridad social y préstamos de los empleados así como las aportaciones patronales.</p>`,

  
});