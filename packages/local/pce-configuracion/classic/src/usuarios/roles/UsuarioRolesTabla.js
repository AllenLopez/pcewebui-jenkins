Ext.define('Pce.configuracion.usuarios.roles.UsuarioRolesTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'usuarios-roles-tabla',

    requires: [

        'Pce.configuracion.usuarios.RolStore',

        'Pce.configuracion.usuarios.roles.UsuarioRolesController',

    ],

    title: 'Catálogo de Roles de Usuarios',

    controller: 'usuario-roles',
    store: {
        type: 'rol',
        autoLoad: true
    },


    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarRolUsuario'
    }],

    columns: [{
        text: 'Clave',
        dataIndex: 'rolId',
        tooltip: `Valor alfanúmerico único para identificar al concepto`,
        flex: 1
    }, {
        text: 'Descripción',
        dataIndex: 'descripcion',
        tooltip:`Titulo corto del concepto.`,
        flex: 4
    }, {
        text: 'Estatus',
        dataIndex: 'estatus',
        tooltip:`Estado del concepto, es utilizado actualmente o dejo de usarse.`,
        renderer(v) {
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        },
        flex: 1
    }, {
        text: 'Creado en',
        tooltip:`Fecha en que fue creado el concepto.`,
        dataIndex: 'fechaCreacion',
        xtype: 'datecolumn',
        format: 'd/m/Y',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar rol',
            handler: 'onEditarRol'
        }, /*{
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar Rol',
            handler: 'onEliminarRol'
        }*/]
    }],

});