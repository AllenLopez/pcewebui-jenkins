Ext.define('Pce.configuracion.usuarios.roles.UsuarioRolesAgregar', {
    extend: 'Ext.form.Panel',
    xtype: 'roles-agregar',


    requires: [

        'Ext.form.field.Text',
        'Ext.form.field.TextArea'

    ],

    layout: 'form',
    defaultListenerScope: true,
    viewModel: {},



    items: [{
        xtype: 'textfield',
        name: 'rolId',
        fieldLabel: 'Clave',
        tooltip: `Valor alfanúmerico único para identificar el concepto.`,
        allowBlank: false
    }, {
        xtype: 'textarea',
        name: 'descripcion',
        fieldLabel: 'Descripción',
        tooltip: `Titulo corto del concepto.`,
        allowBlank: false
    }, {
        xtype: 'checkbox',
        name: 'estatus',
        fieldLabel: 'Activo',
        uncheckedValue: false
    }],


    config: {
        rol: undefined,
    },

    afterRender() {
        this.callParent();
        if (this.rol) {
            this.loadRecord(this.rol);
        }
    },

    loadRecord(record) {
        this.callParent([record]);

        /*['rolId', 'descripcion', 'estatus'].forEach(name => {
            this.down(`[name="${name}"]`).setDisabled(!record.phantom);
        });*/
    },

    /*onAceptar() {
        let form = this.down('form');
        if (form.isValid()) {
            //form.updaterol();
            this.fireEvent('guardar', this, this.rol, form);
        }
    },

    onCancelar() {
        this.close();
    }*/

});