Ext.define('Pce.configuracion.usuarios.roles.UsuarioRolesController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.usuario-roles', 
    requires: [
        'Pce.configuracion.usuarios.Rol',
        'Pce.configuracion.usuarios.RolStore',
        'Pce.view.dialogo.FormaContenedor'
    ],

    onEditarRol(grid, row, col, cmp, e, rol) {
        this.mostrarDialogoEdicion(rol, 'Editar');
    },
    onAgregarRolUsuario() {
        this.mostrarDialogoEdicion(new Pce.configuracion.usuarios.Rol({
            estatus: true
        }), 'Nuevo');
    },
    onEliminarRol(grid, row, col, cmp, e, rol) {
        let store = grid.getStore();
        // let store = this.getView().getStore();
        let msg = Ext.String.format(
            '¿Seguro que deseas eliminar el rol <i>{0}</i>?',
            rol.get('descripcion')
        );
        Ext.Msg.confirm(
            'Eliminar',
            msg,
            function(ans) {
                if (ans === 'yes') {
                    store.remove(rol);
                    store.sync({
                        succes() {
                            Ext.toast('El rol se ha eliminado correctamente');
                        }
                    });
                }
            }
        );
    },

    mostrarDialogoEdicion(rol, titulo) {
        let grid = this.view;
        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title: `${titulo} rol de usuarios`,
            items: {xtype: 'roles-agregar'},
            width: 700,
            
            record: rol,
            listeners: {
                guardar(dialogo) {
                    Ext.Msg.confirm(
                        'Guardar rol',
                        `¿Seguro que deseas guardar el rol <i>${rol.get('descripcion')}</i>?`,
                        ans => {
                            if(ans === 'yes') {
                                rol.save({
                                    success() {
                                        let store = grid.getStore();
                                        if(!store.contains(rol)){
                                            grid.getStore().add(rol);
                                        }

                                        Ext.toast('Los datos se han guardado correctamente');
                                        dialogo.close();
                                    },
                                });
                            }
                        });
                }
            }
        });
    }

});