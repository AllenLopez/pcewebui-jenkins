Ext.define('Pce.configuracion.view.main.MainViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.configuracion-main',

    data: {
        aplicacionNombre: 'Configuracion'
    }
});