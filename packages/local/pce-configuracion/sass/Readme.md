# pce-configuracion/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    pce-configuracion/sass/etc
    pce-configuracion/sass/src
    pce-configuracion/sass/var
