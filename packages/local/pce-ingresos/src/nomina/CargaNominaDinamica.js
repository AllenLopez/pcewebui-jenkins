/**
 * @class Pce.ingresos.nomina.CargaNominaDinamica
 * @extends Ext.data.Model
 * @xtype carga-nomina-dinamica
 * ViewModel para entidad de nómina dinámica
 */
Ext.define('Pce.ingresos.nomina.CargaNominaDinamica', {
    extend: 'Ext.data.Model',
    xtype: 'carga-nomina-dinamica',
    
    fields: [
        {name: 'fechaCarga', type: 'date'},
        {name: 'usuarioCarga', type: 'string'},
        {name: 'estatus', type: 'string'},
        {name: 'fechaNomina', type: 'date'},
        {name: 'estatusCargado', type: 'string'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/nomina-dinamica/carga'
    }
});