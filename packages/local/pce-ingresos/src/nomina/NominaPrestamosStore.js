Ext.define('Pce.ingresos.nomina.NominaPrestamosStore', {
    extend: 'Ext.data.Store',
    alias: 'store.nominaPrestamos',
    model: 'Pce.ingresos.nomina.NominaPrestamos',
});