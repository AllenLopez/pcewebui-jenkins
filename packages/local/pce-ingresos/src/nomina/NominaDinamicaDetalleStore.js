/**
 * @class Pce.ingresos.nomina.NominaDinamicaDetalleStore
 * @extends Ext.data.Store
 * @xtype store.nomina-dinamica-detalle-store
 * Store para detalles de nómina dinámica
 */
Ext.define('Pce.ingresos.nomina.NominaDinamicaDetalleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.nomina-dinamica-detalle-store',
    
    model: 'Pce.ingresos.nomina.NominaDinamicaDetalle'    
});