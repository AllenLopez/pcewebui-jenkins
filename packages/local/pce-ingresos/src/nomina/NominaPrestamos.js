Ext.define('Pce.ingresos.nomina.NominaPrestamos', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'fechaCarga', type: 'date'},
        {name: 'fechaNomina', type: 'date'},
        {name: 'usuarioCarga', type: 'string'},
        {name: 'totalRegistros', type: 'integer'},
        {name: 'dependenciaId', type: 'integer'},
        {name: 'dependenciaDescripcion', type: 'string'},
        {name: 'estatus', type: 'string'},
        {name: 'estatusCargado', type: 'string'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/prestamos/nomina/carga'
    }
});