/**
 * @class Pce.ingresos.nomina.NominaDinamicaDetalle
 * @extends Ext.data.Model
 * @xtype xtype
 * description
 */
Ext.define('Pce.ingresos.nomina.NominaDinamicaDetalle', {
    extend: 'Ext.data.Model',
    
    fields: [
        {name: 'nominaDinamicaMstId', type: 'integer'},
        {name: 'fechaNomina', type: 'date'},
        {name: 'dependenciaId', type: 'integer'},
        {name: 'dependenciaDescripcion', type: 'string'},
        {name: 'estatusCargado', type: 'string'},
        {name: '', type: ''},

        /*
        "nominaDinamicaMstId": 1,
        "fechaNomina": "2018-09-30T00:00:00",
        "dependenciaId": 4,
        "dependenciaDescripcion": "TRIBUNAL SUPERIOR DE JUSTICIA",
        "estatusCargado": null
         */
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/nomina-dinamica/carga',

        reader: {
            type: 'json'
        }
    }
});