Ext.define('Pce.ingresos.nomina.CargaNomina', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'fechaCarga', type: 'date'},
        {name: 'fechaNomina', type: 'date'},
        {name: 'usuarioCarga', type: 'string'},
        {name: 'registrosTotal', type: 'integer'},
        {name: 'dependenciaClave', type: 'integer'},
        {name: 'dependenciaDescripcion', type: 'string'},
        {name: 'estatus', type: 'string'},
        {name: 'estatusCargado', type: 'string'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/nomina/carga'
    }
});