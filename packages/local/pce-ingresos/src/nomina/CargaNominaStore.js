Ext.define('Pce.ingresos.nomina.CargaNominaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.carga-nomina',
    model: 'Pce.ingresos.nomina.CargaNomina',

    proxy: {
        type: 'rest',
        url: '/api/ingresos/nomina/carga/pendientes'
    }
});