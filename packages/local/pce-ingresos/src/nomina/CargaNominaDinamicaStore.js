/**
 * @class Pce.ingresos.nomina.CargaNominaDinamicaStore
 * @extends Ext.data.Store
 * @alias store.carga-nomina-dinamica
 * Store para modelo de nómina dinámica
 */
Ext.define('Pce.ingresos.nomina.CargaNominaDinamicaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.carga-nomina-dinamica',
    model: 'Pce.ingresos.nomina.CargaNominaDinamica',

    proxy: {
        type: 'rest',
        url: '/api/ingresos/nomina-dinamica/carga/pendientes'
    }
    
});