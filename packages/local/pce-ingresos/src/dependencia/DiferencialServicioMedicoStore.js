Ext.define('Pce.ingresos.dependencia.DiferencialServicioMedicoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.diferencial-sm-store',

    model: 'Pce.ingresos.dependencia.DiferencialServicioMedico'
});