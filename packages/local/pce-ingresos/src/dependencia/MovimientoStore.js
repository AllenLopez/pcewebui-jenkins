Ext.define('Pce.ingresos.dependencia.MovimientoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.movimiento-dependencia',
    model: 'Pce.ingresos.dependencia.Movimiento'
});