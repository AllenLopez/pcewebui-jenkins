Ext.define('Pce.ingresos.dependencia.DiferencialServicioMedicoDetalle', {
    extend: 'Ext.data.Model',

    fields: [
        'numeroDependencia',
        'dependenciaDescripcion',
        'totalAsegurado',
        'totalBeneficiario',
        'totalAseguradoBeneficiario',
        'importeProrrateo',
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/diferencial-sm/prorrateos',
    }
});