Ext.define('Pce.ingresos.dependencia.AportacionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dependencias-aportaciones',
    model: 'Pce.ingresos.dependencia.Aportacion'
});