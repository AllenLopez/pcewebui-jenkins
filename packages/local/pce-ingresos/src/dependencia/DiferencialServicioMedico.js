Ext.define('Pce.ingresos.dependencia.DiferencialServicioMedico', {
    extend: 'Ext.data.Model',

    fields: [
        'fechaQuincena',
        'costo',
        'transaccionId'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/diferencial-sm'
    }
});