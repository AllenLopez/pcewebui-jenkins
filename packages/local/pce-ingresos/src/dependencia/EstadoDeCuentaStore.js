Ext.define('Pce.ingresos.dependencia.EstadoDeCuentaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.dependencia-estados-de-cuenta',

    model: 'Pce.ingresos.dependencia.EstadoDeCuenta',

    porAvisoCargo(avisoCargoId) {
        return new Promise((resolve) => {
            this.load({
                params: {
                    avisoCargoId: avisoCargoId
                },
                callback(data) {
                    resolve(data);
                }
            });
        });
    },

    porCertificadoIngresos(certificadoId) {
        return new Promise((resolve) => {
            this.load({
                params: {
                    certificadoId
                },
                proxy: {
                    type: 'rest',
                    url: '/api/ingresos/certificado/avisos-cargo'
                },
                callback(data) {
                    resolve(data);
                }
            });
        });
    }
});