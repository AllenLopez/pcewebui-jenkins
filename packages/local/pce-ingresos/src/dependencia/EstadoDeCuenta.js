Ext.define('Pce.ingresos.dependencia.EstadoDeCuenta', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'conceptoId',
        'conceptoDescripcion',
        'clave',
        'dependenciaId',
        'formaPagoId',
        'bancoId',
        'cuentaBancoId',
        'bancoEmisor',
        'cheque',
        'saldo',
        'importe',
        'porPagar',
        'importeInicial',
        'fechaNomina',
        'fechaVencimiento',
        'emisionAvisoCargoId',
        'emisionAvisoCargoFolio',
        'transaccionId',

        {
            name: 'saldoRestante',
            calculate(data) {
                return data.saldo - data.porPagar;
            }
        }

    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/dependencia/estado-de-cuenta'
    },

    onMetaChange: function (meta) {
        this.fireEvent('metachange', this, meta);
    },
});