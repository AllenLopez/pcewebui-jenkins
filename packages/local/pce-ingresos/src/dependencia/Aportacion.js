Ext.define('Pce.ingresos.dependencia.Aportacion', {
    extend: 'Ext.data.Model',

    fields: [
        'dependenciaId',
        'dependenciaDescripcion',
        'conceptoId',
        'conceptoClave',
        'conceptoDescripcion',
        'porcentaje',
        {name: 'activo', type: 'boolean'},
        {name: 'fechaEfectiva', type: 'date'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/dependencia/aportacion',
        writer: {
            writeAllFields: true
        }
    }
});