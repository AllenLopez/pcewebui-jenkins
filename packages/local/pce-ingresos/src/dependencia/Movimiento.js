Ext.define('Pce.ingresos.dependencia.Movimiento', {
    extend: 'Ext.data.Model',

    fields: [
        'importe',
        'numeroDependencia',
        'fechaNomina',
        'dependenciaDescripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/dependencia/movimiento'
    }
});