Ext.define('Pce.ingresos.dependencia.DiferencialServicioMedicoDetalleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.diferencial-sm-detalle',

    model: 'Pce.ingresos.dependencia.DiferencialServicioMedicoDetalle'
});