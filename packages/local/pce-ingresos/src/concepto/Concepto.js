Ext.define('Pce.ingresos.concepto.Concepto', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'clave',
        'descripcion',
        {name: 'estatus', type: 'boolean'},
        'clasificacion',
        'destino',
        {name: 'derivaAportacionPatronal', type: 'boolean'},
        {name: 'usuarioCreacion', persist: false},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'usuarioModificacion', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        'conceptoOrigenId'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/concepto',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});