Ext.define('Pce.ingresos.concepto.ConceptoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.conceptos',

    model: 'Pce.ingresos.concepto.Concepto'
});