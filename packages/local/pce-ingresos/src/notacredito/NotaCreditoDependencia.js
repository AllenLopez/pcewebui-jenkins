/**
 * @class
 * @author: Alan López
 */
Ext.define('Pce.ingresos.notacredito.NotaCreditoDependencia', {
    extend: 'Ext.data.Model',

    fields: [
        'dependenciaId',
        'emisionAvisoCargoId',
        'importe',
        'fechaMovimiento',
        { name: 'fechaCreacion', type: 'date', persist: false },
        { name: 'fechaModificacion', type: 'date', persist: false },
        { name: 'usuarioCreacion', persist: false },
        { name: 'usuarioModificacion', persist: false }
        
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/nota-credito',
        writer: {
            type: 'json',
            writeAllFields: true,
            // allowSingle: true
        }
    }
    
});