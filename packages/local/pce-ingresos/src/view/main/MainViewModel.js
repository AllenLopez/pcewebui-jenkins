Ext.define('Pce.ingresos.view.main.MainViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.ingresos-main',

    data: {
        aplicacionNombre: 'Ingresos'
    }
});