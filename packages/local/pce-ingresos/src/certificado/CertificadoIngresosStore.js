Ext.define('Pce.ingresos.certificado.CertificadoIngresosStore', {
    extend: 'Ext.data.Store',
    alias: 'store.certificado-ingresos',

    model: 'Pce.ingresos.certificado.CertificadoIngresos'
});