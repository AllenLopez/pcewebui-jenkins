Ext.define('Pce.ingresos.certificado.CertificadoIngresos', {
    extend: 'Ext.data.Model',

    fields: [
        'certificado',
        'descripcionConcepto',
        'concepto',
        'importe',
        'importeString',
        'saldo',
        'saldoString',
        'dependencia',
        'dependenciaDescripcion',
        {name: 'fecha', type: 'date'},
        {name: 'fechaPago', type: 'date'},
        'avisoCargo',
        'nombre',
        'formaPago',
        'bancoEmisor',
        'referencia',
        'bancoReceptor',
        'cuentaBancaria',
        'tipoPago',
        'estatus',
        'usuario',
        'transaccion',
        'tipoMovimiento',
        'licencia',
        {name: 'fechaInicio', type: 'date'},
        {name: 'fechaTermino', type: 'date'},
        'reporte',
        {
            name: 'estatusDescripcion',
            convert: (v, record) => {
                let estatus = record.get('estatus');
                switch (estatus) {
                    case 'TR': return 'EN TRÁMITE';
                    case 'AU': return 'AUTORIZADA';
                    case 'CA': return 'CANCELADO';
                    case 'CR': return 'CERRADO';
                    default: return 'OTRA';
                }
            }
        },
        {
            name: 'tipoPagoDescr',
            convert: (v, record) => {
                // Tipo de certificado de pago NR- NORMAL AN- ANTICIPO MA- MULTIPLES AVISOS CARGO
                let tipoPago = record.get('tipoPago');
                switch (tipoPago) {
                    case 'NR': return 'NORMAL';
                    case 'AN': return 'ANTICIPO';
                    case 'MA': return 'MULTIPLES AVISOS CARGO';
                    default: return 'OTRA';
                }
            }
        }
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/certificado',
    }
});