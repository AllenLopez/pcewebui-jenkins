Ext.define('Pce.ingresos.transaccion.Origen', {
    extend: 'Ext.data.Model',

    fields: [
        'clave',
        'descripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/transaccion/origen',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});