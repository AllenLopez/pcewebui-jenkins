Ext.define('Pce.ingresos.transaccion.TipoResultadoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.transaccion-tipo-resultado',
    model: 'Pce.ingresos.transaccion.TipoResultado'
});