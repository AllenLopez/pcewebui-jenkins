Ext.define('Pce.ingresos.transaccion.TransaccionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.transacciones',
    model: 'Pce.ingresos.transaccion.Transaccion',

    porFechas(params) {
        this.load({
            params: {
                desde: params.desde,
                hasta: params.hasta,
                sistema: params.sistema
            },
            url: '/api/ingresos/transaccion/por-fechas'
        });
    },

    porId(idTransaccion) {
        this.load({
            url: '/api/ingresos/transaccion/'+idTransaccion
        });
    },

    cancelarTransaccion(idTransaccion) {
        this.load({
            url: '/api/ingresos/transaccion/cancelar-transaccion/'+idTransaccion
        });
    },

    porFechaMovimiento(params) {
        this.load({
            params: {
                desde: params.desde,
                hasta: params.hasta,
                sistema: params.sistema
            },
            url: '/api/ingresos/transaccion'
        });
    },
    porFechaMovimientoPrestamos(params) {
        this.load({
            params: {
                desde: params.desde,
                hasta: params.hasta,
                sistema: params.sistema
            },
            url: '/api/prestamos/nomina/carga/transaccion-carga-nomina'
        });
    },
});