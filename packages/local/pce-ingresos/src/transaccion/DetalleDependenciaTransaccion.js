/**
 * @class Pce.ingresos.transaccion.DetalleDependenciaTransaccion
 * @extends Ext.data.Model
 * Modelo de DTO para representar las dependencias de la transacción seleccionada
 * @author: Jorge Escamilla
 */

Ext.define('Pce.ingresos.transaccion.DetalleDependenciaTransaccion', {
    extend: 'Ext.data.Model',

    fields: [
        'transaccion',
        'etapa',
        'claveDependencia',
        'dependencia',
        'submotivo',
        'origen',
        'usuario',
        'fechaMovimiento',
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/transaccion-detalle/detalle-dep',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});