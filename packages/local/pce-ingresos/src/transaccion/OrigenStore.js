Ext.define('Pce.ingresos.transaccion.OrigenStore', {
    extend: 'Ext.data.Store',
    alias: 'store.transaccion-origenes',
    model: 'Pce.ingresos.transaccion.Origen'
});