Ext.define('Pce.ingresos.transaccion.Transaccion', {
    extend: 'Ext.data.Model',

    fields: [
        'interfazId',
        {name: 'fechaMovimiento', type: 'date'},
        'dependencia',
        'registros',
        'etapa',
        {name: 'fechaCreacion', type: 'date'},
        {name: 'fechaModificacion', type: 'date'},
        'usuarioCreacion',
        'usuarioModificacion',
        'origenClave',
        'OrigenDescripcion',
        'cancelado',
        'idTransaccionOrigen',
        'origenClaveTransaccionOrigen',
        'descripcionTransaccionOrigen',
        {
         name: 'cancelado',
         convert: (v, transaccion) =>{
             return transaccion.get('cancelado') ? 'SI' : 'NO';
         }   
        },{
         name: 'dependencia',
         convert: (v, transaccion) => {
            let dependencia = transaccion.get('dependencia');
            let registros = transaccion.get('registros');
            
            if(registros === 1 || dependencia == null){
                return 'AlGUNAS DEPENDENCIAS'                 
            }
            switch (dependencia ) {
                case null: return 'ALGUNAS DEPENDENCIAS'
                default: return dependencia;
            }
            }
        },{
            name: 'etapaDescripcion',
            convert: (v, transaccion) => {
                let etapa = transaccion.get('etapa');
                switch (etapa) {
                    case 'TR': return 'En trámite';
                    case 'CT': return 'En control';
                    case 'RE': return 'Rechazada';
                    case 'AU': return 'Autorizada';
                    case 'RC': return 'Rechazada en control';
                    default: return 'Otra';
                }
            }
        }
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/transaccion'
    }
});