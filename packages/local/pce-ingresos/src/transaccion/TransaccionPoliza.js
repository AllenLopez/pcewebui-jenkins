/**
 * @class Pce.ingresos.transaccion.TransaccionPoliza
 * @extends Ext.data.Model
 * ViewModel de transacciones/polizas
 */
Ext.define('Pce.ingresos.transaccion.TransaccionPoliza', {
  extend: 'Ext.data.Model',
  
  fields: [
    {name: 'fechaMovimiento', type: 'date'},
    {name: 'fechaPoliza', type: 'date'},
    'etapa',
    {
        name: 'etapaDescripcion',
        convert: (v, transaccion) => {
            let etapa = transaccion.get('etapa');
            switch (etapa) {
                case 'TR': return 'EN TRÁMITE';
                case 'CT': return 'EN CONTROL';
                case 'RE': return 'RECHAZADA';
                case 'AU': return 'AUTORIZADA';
                case 'RC': return 'RECHAZADA EN CONTROL';
                default: return 'OTRA';
            }
        }
    },
    'usuarioCreacion',
    {name: 'fechaCreacion', type: 'date'},
    'polizasIds',
    'polizasFolios',
    'polizasDescripcion'
  ],

  proxy: {
      type: 'rest',
      url: '/api/ingresos/transaccion/por-tipo-movimiento',
      timeout: 120000
  }
});