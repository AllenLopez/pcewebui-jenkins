Ext.define('Pce.ingresos.transaccion.TipoResultado', {
    extend: 'Ext.data.Model',

    fields: [
        'descripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/transaccion/tipo',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});