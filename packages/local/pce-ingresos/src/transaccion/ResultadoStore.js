Ext.define('Pce.ingresos.transaccion.ResultadoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.transaccion-resultados',
    model: 'Pce.ingresos.transaccion.Resultado'
});