/**
 * @class Pce.ingresos.transaccion.DetalleDependenciaTransaccionStore
 * @extends Ext.data.Model
 * Modelo representativo de entidad TransaccionDetalle
 * @author: Jorge Escamilla
 */

Ext.define('Pce.ingresos.transaccion.DetalleDependenciaTransaccionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.detalles-dep-transaccion',

    model: 'Pce.ingresos.transaccion.DetalleDependenciaTransaccion'
});