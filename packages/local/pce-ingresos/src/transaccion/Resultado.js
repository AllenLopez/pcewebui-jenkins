Ext.define('Pce.ingresos.transaccion.Resultado', {
    extend: 'Ext.data.Model',

    fields: [
        'numeroDependencia',
        'numeroAfiliacion',
        'conceptoClave',
        'conceptoDescripcion',
        'importe',
        'descripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/transaccion/resultado'
    }
});