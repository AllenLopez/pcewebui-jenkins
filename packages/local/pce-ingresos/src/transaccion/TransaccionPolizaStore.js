Ext.define('Pce.ingresos.transaccion.TransaccionPolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.transacciones-polizas',
    model: 'Pce.ingresos.transaccion.TransaccionPoliza'
});