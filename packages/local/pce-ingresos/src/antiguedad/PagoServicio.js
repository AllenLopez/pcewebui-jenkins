/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.antiguedad.PagoServicio', {
    url: '/api/caja/antiguedad/pagar',
    pagar(periodos, datos) {
        let pagos = periodos.map(periodo => {
            return {periodoId: periodo.getId()};
        });
        datos.periodos = pagos;
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: this.url,
                method: 'POST',
                jsonData: datos,
                success: (response) => {
                    resolve(JSON.parse(response.responseText));
                },
                failure(response) {
                    reject(JSON.parse(response.responseText));
                }
            });
        });
    },
    pagarDependencia(periodos, datos) {
        let pagos = periodos.map(periodo => {
            return {periodoId: periodo.getId()};
        });
        datos.periodos = pagos;
        console.log(datos);
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: '/api/caja/antiguedad/pagar-dependencia',
                method: 'POST',
                jsonData: datos,
                success: (response) => {
                    resolve(JSON.parse(response.responseText));
                },
                failure(response) {
                    reject(JSON.parse(response.responseText));
                }
            });
        });
    }
});