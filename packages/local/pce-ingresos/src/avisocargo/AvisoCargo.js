Ext.define('Pce.ingresos.avisocargo.AvisoCargo', {
    extend: 'Ext.data.Model',

    fields: [
        'descripcion',
        'dependenciaId',
        'dependenciasIds',
        {name: 'dependenciasDescripciones', persist: false},
        'conceptosIds',
        {name: 'conceptosDescripciones', persist: false},
        {name: 'activo', type: 'boolean'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/aviso-cargo',

        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});