/**
 * @class
 * @author: Jorge Escamilla
 */

Ext.define('Pce.ingresos.avisocargo.AvisoCargoManualStore', {
    extend: 'Ext.data.Store',
    alias: 'store.avisos-cargo-manual',

    model: 'Pce.ingresos.avisocargo.AvisoCargoManual'
});