Ext.define('Pce.ingresos.avisocargo.EmisionAvisoCargoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.emision-aviso-cargo',

    model: 'Pce.ingresos.avisocargo.EmisionAvisoCargo',

    porDependencia(dependenciaId) {
        this.load({
            params: {
                dependenciaId: dependenciaId
            }
        });
    }
});