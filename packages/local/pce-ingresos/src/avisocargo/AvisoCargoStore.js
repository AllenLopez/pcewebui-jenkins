Ext.define('Pce.ingresos.avisocargo.AvisoCargoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.avisos-cargo',

    model: 'Pce.ingresos.avisocargo.AvisoCargo'
});