Ext.define('Pce.ingresos.avisocargo.EmisionAvisoCargo', {
    extend: 'Ext.data.Model',

    fields: [
        'folio',
        'avisoCargoDescripcion',
        'avisoCargoId',
        'dependenciaId',
        'fechaEmision',
        {name: 'saldo', persist: false},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/emision-aviso-cargo'
    }

});