/**
 * @class
 * @author: Jorge Escamilla
 */
Ext.define('Pce.ingresos.avisocargo.AvisoCargoManual', {
    extend: 'Ext.data.Model',

    fields: [
        'dependenciaId',
        // 'conceptoClave',
        'conceptoId',
        'importe',
        'fechaNomina',
        { name: 'fechaCreacion', type: 'date', persist: false },
        { name: 'fechaModificacion', type: 'date', persist: false },
        { name: 'usuarioCreacion', persist: false },
        { name: 'usuarioModificacion', persist: false }
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/aviso-cargo-manual',
        writer: {
            type: 'json',
            writeAllFields: true,
            allowSingle: true
        }
    }


    // proxy: {
    //     type: 'ajax',
    //     api: {
    //         create: '/api/ingresos/aviso-cargo-manual',
    //         read: undefined,
    //         update: undefined,
    //         destroy: undefined
    //     },

    //     writer: {
    //         type: 'json',
    //         writeAllFields: true,
    //         allowSingle: false
    //     }
    // }
});