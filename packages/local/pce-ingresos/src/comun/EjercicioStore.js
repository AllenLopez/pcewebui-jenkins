Ext.define('Pce.ingresos.comun.EjercicioStore', {
    extend: 'Ext.data.Store',
    alias: 'store.ejercicio',

    fields: ['ejercicio'],
    data: (function() {
        let val = [];
        let year = (new Date()).getFullYear();
        for (let i = 1900; i <= year; i++) {
            val.push({'ejercicio': i});
        }
        return val;
    })(),

    sorters: [{
        property: 'ejercicio',
        direction: 'DESC'
    }]

});