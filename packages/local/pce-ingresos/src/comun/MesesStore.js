Ext.define('Pce.ingresos.comun.MesesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.meses-ejercicio',

    fields: ['nombre', 'mes'],

    data: [
        { 'nombre': 'Enero', 'mes': 1 },
        { 'nombre': 'Febrero', 'mes': 2 },
        { 'nombre': 'Marzo', 'mes': 3 },
        { 'nombre': 'Abril', 'mes': 4 },
        { 'nombre': 'Mayo', 'mes': 5 },
        { 'nombre': 'Junio', 'mes': 6 },
        { 'nombre': 'Julio', 'mes': 7 },
        { 'nombre': 'Agosto', 'mes': 8 },
        { 'nombre': 'Septiembre', 'mes': 9 },
        { 'nombre': 'Octubre', 'mes': 10 },
        { 'nombre': 'Noviembre', 'mes': 11 },
        { 'nombre': 'Diciembre', 'mes': 12 },
    ]
});