/*
* @class Pce.ingresos.jubilado.JubiladoAgrupadoMensualStore
* @extends Ext.data.Store
* @alias jubilado-nuevo
* */

Ext.define('Pce.ingresos.jubilado.JubiladoAgrupadoMensualStore', {
    extend: 'Ext.data.Store',
    alias: 'store.jubilado-agrupado-mes',

    model: 'Pce.ingresos.jubilado.JubiladoAgrupadoMensual'
});