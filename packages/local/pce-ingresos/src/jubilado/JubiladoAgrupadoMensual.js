/**
 * @class Pce.ingresos.jubilado.JubiladoAgrupadoMensual
 * @extends Ext.data.Model
 */

Ext.define('Pce.ingresos.jubilado.JubiladoAgrupadoMensual', {
    extend: 'Ext.data.Model',

    fields: [
        'fechaCreacion',
        'fechaJubilacion',
        'fechaCarga',
        {
            name: 'mes',
            convert(m) {
                const meses = ['ENERO','FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
                    'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];
                return meses[m-1];
            }
        },
        'ejercicio',
        'totalJubilados',
        'folioTransaccion',
        'regimen',
        'tipo'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/jubilado/carga-saldo/jubilados-mes-ejercicio',

        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});