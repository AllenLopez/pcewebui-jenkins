/**
 * @class Pce.ingresos.jubilado.JubiladoNuevo
 * @extends Ext.data.Model
 */

Ext.define('Pce.ingresos.jubilado.JubiladoNuevo', {
    extend: 'Ext.data.Model',

    fields: [
        'aseguradoNumeroAfiliacion',
        'conceptoId',
        'saldo',
        'dependenciaNumeroDependencia',
        'aseguradoNombreCompleto',
        'coRegimenClaveConceptoAfil',
        'fechaJubilacion',
        'dependenciaDescripcion',
        'concepto',
        'usuarioCreacion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/jubilado/carga-saldo/jubilados-cargados'
    }
});