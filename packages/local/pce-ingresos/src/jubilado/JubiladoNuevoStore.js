/*
* @class Pce.ingresos.jubilado.JubiladoNuevoStore
* @extends Ext.data.Store
* @alias jubilado-nuevo
* */

Ext.define('Pce.ingresos.jubilado.JubiladoNuevoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.jubilado-nuevo',

    model: 'Pce.ingresos.jubilado.JubiladoNuevo'
});