/**
 * @class Pce.ingresos.facturacionelectronica.FacturacionElectronica
 * @extends Ext.data.Model
 * Viewmodel de entidad de facturación electrónica
 */
Ext.define('Pce.ingresos.facturacionElectronica.FacturacionElectronica', {
    extend: 'Ext.data.Model',
    
    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'nombre',
        'descripcion',
        'reporteId'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/facturacion-electronica',
        writter: {
            type: 'json',
            writeAllFields: true
        }
    }
});