/**
 * @class Pce.ingresos.facturacionelectronica.FacturacionElectronicaStore
 * @extends Ext.data.Store
 * @alias store.facturacion-electronica
 * Store de entidad Facturacion Electronica
 */
Ext.define('Pce.ingresos.facturacionElectronica.FacturacionElectronicaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.facturacion-electronica',
    
    model: 'Pce.ingresos.facturacionElectronica.FacturacionElectronica'
});