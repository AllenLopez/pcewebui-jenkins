Ext.define('Pce.ingresos.formaPago.FormaPago', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'descripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/forma-pago',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});