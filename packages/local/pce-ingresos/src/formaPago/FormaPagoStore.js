Ext.define('Pce.ingresos.formaPago.FormaPagoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.forma-pago',

    model: 'Pce.ingresos.formaPago.FormaPago'
});