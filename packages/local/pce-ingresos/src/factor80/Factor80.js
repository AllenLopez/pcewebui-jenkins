Ext.define('Pce.ingresos.factor80.Factor80', {
    extend: 'Ext.data.Model',

    

    fields: [
        'id',
        'edad',
        {name: 'fechaEfectiva', type: 'date'},
        {name: 'estatus', type: 'boolean'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/afiliacion/factor80',
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});