/**
 * @class Pce.ingresos.movimientoContable.MovimientoContableStore
 * @extends Ext.data.Store
 * @alias store.movimiento-contable
 * Store para modelo MovimientoContable
 */
Ext.define('Pce.ingresos.movimientoContable.MovimientoContableStore', {
    extend: 'Ext.data.Store',
    alias: 'store.movimiento-contable',
    
    model: 'Pce.ingresos.movimientoContable.MovimientoContable'
});