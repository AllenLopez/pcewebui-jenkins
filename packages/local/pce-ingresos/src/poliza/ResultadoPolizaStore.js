/**
 * @class Pce.ingresos.poliza.ResultadoPolizaStore
 * @extends Ext.data.Store
 * @alias store.resultado-poliza
 * Store para modelo ResultadoPoliza
 */
Ext.define('Pce.ingresos.poliza.ResultadoPolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.resultado-poliza',

    model: 'Pce.ingresos.poliza.ResultadoPoliza'
});