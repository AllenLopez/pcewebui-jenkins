Ext.define('Pce.ingresos.poliza.TipoPoliza', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'tipo', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'estatus', type: 'boolean'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/tipo-poliza',

        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});