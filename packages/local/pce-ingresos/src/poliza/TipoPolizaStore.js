Ext.define('Pce.ingresos.poliza.TipoPolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tipos-poliza',
    
    model: 'Pce.ingresos.poliza.TipoPoliza'
});