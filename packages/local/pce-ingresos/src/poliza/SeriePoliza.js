/**
 * @class Pce.ingresos.poliza.SeriePoliza
 * @extends Ext.data.Model
 * @description View Model de entidad Serie Poliza
 */

Ext.define('Pce.ingresos.poliza.SeriePoliza', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'integer'},
        {name: 'serie', type: 'string'},
        {name: 'nombre', type: 'string'},
        {name: 'estatus', type: 'boolean'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/serie-poliza',

        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});