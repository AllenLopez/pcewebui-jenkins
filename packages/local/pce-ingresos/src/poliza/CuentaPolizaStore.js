/**
 * @class Pce.ingresos.poliza.CuentaPolizaStore
 * @extends Ext.data.Store
 * @alias store.cuenta-poliza
 * Store para entidad CuentaPoliza
 */
Ext.define('Pce.ingresos.poliza.CuentaPolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cuenta-poliza',
    model: 'Pce.ingresos.poliza.CuentaPoliza'
});