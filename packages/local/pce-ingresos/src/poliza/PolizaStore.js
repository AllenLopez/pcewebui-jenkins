Ext.define('Pce.ingresos.poliza.PolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.poliza',

    model: 'Pce.ingresos.poliza.Poliza',

    porFiltro(filtro) {
        this.load({
            params: {
                filtro: filtro
            }
        });
    }
});