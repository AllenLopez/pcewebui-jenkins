Ext.define('Pce.ingresos.poliza.PolizaDetalle', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'consecutivo', type: 'integer'},
        {name: 'fecha', type: 'date'},
        {name: 'estatus', type: 'string'},
        {name: 'debe', type: 'float'},
        {name: 'haber', type: 'float'},
        {name: 'descripcion', type: 'string'},
        {name: 'referencia', type: 'string'},
        {name: 'cuenta', type: 'string'},
        {name: 'subCuenta01', type: 'string'},
        {name: 'subCuenta02', type: 'string'},
        {name: 'subCuenta03', type: 'string'},
        {name: 'subCuenta04', type: 'string'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false},
        {name: 'polizaId', reference: { parent: 'Pce.ingresos.poliza.Poliza', }}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/poliza-detalle',

        reader: {
            type: 'json'
        }
    }
});