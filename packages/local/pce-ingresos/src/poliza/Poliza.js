Ext.define('Pce.ingresos.poliza.Poliza', {
    extend: 'Ext.data.Model',

    fileds: [
        {name: 'tipo', type: 'string'},
        {name: 'tipoPoliza', type: 'string'},
        {name: 'serie', type: 'string'},
        {name: 'seriePoliza', type: 'string'},
        {name: 'folio', type: 'integer'},
        {name: 'transaccion', type: 'integer'},
        {name: 'expContabilidad', type: 'integer'},
        {name: 'fecha', type: 'date'},
        {name: 'descripcion', type: 'string'},
        {name: 'referencia', type: 'string'},
        {name: 'estatus', type: 'string'},
        {name: 'debe', type: 'float'},
        {name: 'haber', type: 'float'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/poliza',

        // writer: {
        //     type: 'json',
        //     writeAllFields: true
        // },

        reader: {
            type: 'json'
        }
    }
});