Ext.define('Pce.ingresos.poliza.PolizaDetalleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.poliza-detalle',

    model: 'Pce.ingresos.poliza.PolizaDetalle',

    porPoliza(polizaId) {
        this.load({
            params: {
                polizaId: polizaId
            }
        });
    }
});