/**
 * @class Pce.ingresos.poliza.CuentaPoliza
 * @extends Ext.data.Model
 * Modelo de entidad cuenta contable para pólizas
 */
Ext.define('Pce.ingresos.poliza.CuentaPoliza', {
    extend: 'Ext.data.Model',

    fields: [
        {idProperty: 'cuentaMaestra'},
        'cuenta',
        'descripcion',
        'subCuenta01',
        'subCuenta02',
        'subCuenta03',
        'subCuenta04',
        'combinacionContableId',
    ],

    proxy: {
        type: 'ajax',
        url: '/api/ingresos/poliza/poliza-captura/cuenta-contable',
        reader: {
            type: 'json'
        }
    }
});