Ext.define('Pce.ingresos.poliza.SeriePolizaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.series-poliza',
    
    model: 'Pce.ingresos.poliza.SeriePoliza'
});