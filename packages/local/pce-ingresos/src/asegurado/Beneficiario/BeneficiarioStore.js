Ext.define('Pce.ingresos.asegurado.beneficiario.BeneficiarioStore', {
    extend: 'Ext.data.Store',
    alias: 'store.beneficiario',

    model: 'Pce.ingresos.asegurado.beneficiario.Beneficiario'
});