Ext.define('Pce.ingresos.asegurado.beneficiario.TipoBeneficiario', {
    extend: 'Ext.data.Model',

    fields: [
        'descripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/beneficiario-testament/tipo'
    }
});