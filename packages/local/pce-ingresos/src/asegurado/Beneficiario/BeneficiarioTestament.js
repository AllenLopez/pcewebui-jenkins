Ext.define('Pce.ingresos.asegurado.BeneficiarioTestament', {
    extend: 'Ext.data.Model',
    requires:['Ext.data.proxy.Rest'],

    fields: [
        'nombrePersona',
        'direccion',
        'telefono',
        'rfc',
        'ciudad',
        'porcentaje',
        'parentescoBeneficiarioId',
        'parentesco',
        'tipoBeneficiarioId',
        'tipo',
        'origen',
        'numeroBeneficiario',
        {
            name: 'solicitudDevolucionId',
            type: 'int',
            reference: 'Pce.ingresos.asegurado.fondo.SolicitudDevolucion'
        },
        {
            name: 'descuentoIndebidoDevId',
            type: 'int',
            reference: 'Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebido'
        },
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/beneficiario-testament',
        // api: {
        //     create  : null,
        //     read    : null,
        //     update  : '/api/ingresos/asegurado/beneficiario-testament/beneficiario/',
        //     destroy : null
        // },
        writer: {
            writeAllFields: true
        }
    }
});