Ext.define('Pce.ingresos.asegurado.beneficiario.ParentescoBeneficiarioStore', {
    extend: 'Ext.data.Store',
    alias: 'store.parentesco-beneficiario',

    model: 'Pce.ingresos.asegurado.beneficiario.ParentescoBeneficiario',
});