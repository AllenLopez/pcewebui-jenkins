Ext.define('Pce.ingresos.asegurado.beneficiario.Beneficiario', {
    extend: 'Ext.data.Model',

    fields:[
        'nombrePersona',
        'porcentaje',
        'cartaTestamentariaId',
        'parentescoBeneficiarioId',
        'tipoBeneficiarioId',
        'origen'
    ],

    proxy: {
        type: 'rest',
        url: 'api/ingresos/asegurado/beneficiario-testament',
    }
});