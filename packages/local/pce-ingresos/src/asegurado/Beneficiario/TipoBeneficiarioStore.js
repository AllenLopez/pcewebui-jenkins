Ext.define('Pce.ingresos.asegurado.beneficiario.TipoBeneficiarioStore', {
    extend: 'Ext.data.Store',
    alias: 'store.beneficiario-tipo',

    model: 'Pce.ingresos.asegurado.beneficiario.TipoBeneficiario'
});