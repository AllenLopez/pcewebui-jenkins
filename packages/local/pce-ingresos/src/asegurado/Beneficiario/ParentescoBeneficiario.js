Ext.define('Pce.ingresos.asegurado.beneficiario.ParentescoBeneficiario', {
    extend: 'Ext.data.Model',

    fields: [
        'descripcion'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/parentesco-beneficiario'
    }
});