Ext.define('Pce.ingresos.asegurado.BeneficiarioTestamentStore', {
    extend: 'Ext.data.Store',
    alias: 'store.beneficiario-testament',

    model: 'Pce.ingresos.asegurado.BeneficiarioTestament',
});