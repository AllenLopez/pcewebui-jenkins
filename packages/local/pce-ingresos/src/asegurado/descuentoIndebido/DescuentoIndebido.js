Ext.define('Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebido', {
    extend: 'Ext.data.Model',

    fields: [
        'asegurado',
        'aseguradoNumeroAfiliacion',
        'aseguradoNombrecompleto',
        'tipoMovimiento',
        'tipoMovimientoDescripcion',
        'concepto',
        'conceptoDescripcion',
        'importeReconocimientoAntiguedad',
        'importePrestamo',
        'importeRetencion',
        'importeAportacion',
        'etapa',
        'observaciones',
        'quincenaInicio',
        'quincenaFin',
        'dependencia',
        'departamento',
        'sueldoGravable',
        'telefono',
        'tipoMovimientoDescripcion',
        'tipoMovimientoClave',
        'movimientoDescripcion',
        'usuarioAutorizacion',
        'fechaAutorizacion',
        'usuarioCancelacion',
        'fechaCancelacion',
        'usuarioCreacion',
        'fechaCreacion',
        'usuarioAfectacion',
        'fechaAfectacion',
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/descuento-indebido',
        writer: {
            writeAllFields: true
        },
        api: {
            create: '/api/ingresos/asegurado/descuento-indebido/reconocimiento-antiguedad',
            read: '/api/ingresos/asegurado/descuento-indebido/por-filtro',
            update: undefined,
            destroy: undefined
        },
    },
});