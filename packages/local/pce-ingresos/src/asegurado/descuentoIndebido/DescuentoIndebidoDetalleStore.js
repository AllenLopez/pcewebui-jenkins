Ext.define('Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoDetalleStore', {
    extend: 'Ext.data.Store',
    alias: 'store.descuento-indebido-detalle',

    model: 'Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoDetalle'
});