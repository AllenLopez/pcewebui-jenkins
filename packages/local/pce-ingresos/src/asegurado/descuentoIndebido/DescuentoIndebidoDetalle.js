Ext.define('Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoDetalle', {
    extend: 'Ext.data.Model',

    fields: [
        'importeRetencion',
        'importeAportacion',
        'usuarioCreacion',
        'descripcionMovimiento',
        'regimenAsegurado',
        'regimenIncorrecto',
        {name: 'fechaMovimiento', type: 'date'},
        {name: 'fechaCreacion', type: 'date', persist: false},
        {name: 'fechaModificacion', type: 'date', persist: false},
        {name: 'usuarioCreacion', persist: false},
        {name: 'usuarioModificacion', persist: false},
        'descuentoIndebido'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/descuento-indebido-detalle',
        writer: {
            writeAllFields: true
        }
    }
});