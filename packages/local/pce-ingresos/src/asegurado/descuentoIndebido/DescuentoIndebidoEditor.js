Ext.define('Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoEditor', {
    extend: 'Ext.data.Model',
    requires:['Ext.data.proxy.Rest'],

    fields: [
        'asegurado',
        'aseguradoNumeroAfiliacion',
        'aseguradoNombrecompleto',
        'tipoMovimiento',
        'tipoMovimientoDescripcion',
        'concepto',
        'conceptoDescripcion',
        'importeReconocimientoAntiguedad',
        'importePrestamo',
        'importeRetencion',
        'importeAportacion',
        'etapa',
        'observaciones',
        {name: 'quincenaInicio', type: 'date'},
        {name: 'quincenaFin', type: 'date'},
        'dependencia',
        'dependenciaDescripcion',
        'departamento',
        'sueldoGravable',
        'telefono',
        'tipoMovimientoDescripcion',
        'tipoMovimientoClave',
        'movimientoDescripcion',
        'usuarioAutorizacion',
        'fechaAutorizacion',
        'usuarioCancelacion',
        'fechaCancelacion',
        'usuarioCreacion',
        'fechaCreacion',
        'usuarioAfectacion',
        'fechaAfectacion',
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/descuento-indebido/',
        writer: {
            writeAllFields: true
        },
        api: {
            create: null,
            read:  '/api/ingresos/asegurado/descuento-indebido/solicitud',
            update: null,
            destroy: null
        },
    },
});