Ext.define('Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.descuento-indebido',

    model: 'Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebido'
});