Ext.define('Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacionConsultaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.asegurado-sin-aportacion-consulta',

    model: 'Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacionConsulta'
});
 