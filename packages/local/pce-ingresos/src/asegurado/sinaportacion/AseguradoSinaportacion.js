
Ext.define('Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacion', {
    extend: 'Ext.data.Model',

    fields: [
        'aseguradoNumeroAfiliacion',
        'aseguradoNombreCompleto',
        'coRegimenClaveAfiliac',
        'fechaUltimaAportacion',
        'dependenciaNumeroDependencia',
        'dependenciaDescripcion',
        'saldo'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/asegurados-cargados'
    }
});