/*
* @class Pce.ingresos.asegurado.sinaportacion.AseguradoCanceladoStore
* @extends Ext.data.Store
* @alias asegurado-sin-aportacion
* */

Ext.define('Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.asegurado-sin-aportacion',

    model: 'Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacion' 
});