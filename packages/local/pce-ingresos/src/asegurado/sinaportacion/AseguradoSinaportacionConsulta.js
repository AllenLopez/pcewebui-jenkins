
Ext.define('Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacionConsulta', {
    extend: 'Ext.data.Model',

    fields: [
        'fechaCreacion',
        'fechaUltimaAportacion',
        {
            name: 'mes',
            convert(m) {
                const meses = ['ENERO','FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO',
                    'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];
                return meses[m-1];
            }
        },
        'ejercicio',
        'totalAsegurados',
        'folioTransaccion',
        'regimen',
        'tipo'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/asegurados-consulta',

        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});