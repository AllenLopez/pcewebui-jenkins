Ext.define('Pce.ingresos.asegurado.EstadoDeCuenta', {
    extend: 'Ext.data.Model',

    fields: [
        'conceptoId',
        'conceptoClave',
        'conceptoDescripcion',
        'subcuentaId',
        'subcuentaDescripcion',
        'saldo'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/estado-de-cuenta'
    },

    statics: {
        porAseguradoYConcepto(numeroAfiliacion, conceptoId) {
            let url = this.getProxy().url;
            return new Promise(resolve => {
                Ext.Ajax.request({
                    url: `${url}/por-asegurado-y-concepto`,
                    method: 'GET',
                    params: {numeroAfiliacion, conceptoId},
                    success(response) {
                        let edoCta = null;
                        try {
                            edoCta = new Pce.ingresos.asegurado.EstadoDeCuenta(
                                JSON.parse(response.responseText)
                            );
                        } catch(e) {
                        }
                        resolve(edoCta);
                    }
                });
            });
        }
    }
});