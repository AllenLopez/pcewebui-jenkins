Ext.define('Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitudStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cheque-fondo-solicitud',

    model: 'Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud'
});