Ext.define('Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud', {
    extend: 'Ext.data.Model',

    fields: [
        'beneficiario',
        'rfc',
        'estatus',
        'tipoMovimiento',
        'fechaAfectacion',
        'dependencia',
        'numCheque',
        'importe',
        'transaccionId',
        'tipoEmision',
        'numeroBeneficiario',
        {
            name: 'solicitudDevolucionId',
            type: 'int',
            reference: 'Pce.ingresos.asegurado.fondo.SolicitudDevolucion'
        },
        {
            name: 'descuentoIndebidoDevId',
            type: 'int',
            reference: 'Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebido'
        },
        { name: 'fechaCreacion', type: 'date', persist: false },
        { name: 'usuarioCreacion', type: 'date', persist: false },
        { name: 'fechaModificacion', type: 'date', persist: false },
        { name: 'usuarioModificacion', type: 'date', persist: false },
    ],

    proxy: {
        type: 'rest',
        url: '/api/cajas/chequera/cheque-solicitud',
        writer: {
            writeAllFields: true
        }
    }
});