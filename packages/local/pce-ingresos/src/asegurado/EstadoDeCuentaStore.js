Ext.define('Pce.ingresos.asegurado.EstadoDeCuentaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.asegurado-estado-de-cuenta',
    model: 'Pce.ingresos.asegurado.EstadoDeCuenta',

    porNumeroAfiliacion(numeroAfiliacion) {
        return new Promise((resolve) => {
            this.load({
                params: {
                    numeroAfiliacion
                },
                callback(data) {
                    resolve(data);
                }
            });
        });
    }
});