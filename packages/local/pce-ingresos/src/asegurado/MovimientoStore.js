Ext.define('Pce.ingresos.asegurado.MovimientoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.movimientos',

    model: 'Pce.ingresos.asegurado.Movimiento',

    porEstadoDeCuenta(estadoDeCuentaId) {
        return new Promise(resolve => {
            this.load({
                params: {
                    estadoDeCuentaId
                },
                callback(data) {
                    resolve(data);
                }
            });
        });
    }
});