/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.cartera.CarteraServicio', {

    /**
     * @private
     */
    url: {
        PDPP: '/api/ingresos/cartera-asegurado/pdpp'
    },

    /**
     * Procesa el movimiento especificado por el objeto proporcionado
     * @param {MovimientoInstruccion} movimiento
     * @returns {Promise<any>}
     */
    procesar(movimiento) {
        let url = this.url[movimiento.tipoMovimiento];
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url,
                method: 'POST',
                jsonData: movimiento,
                success(response) {
                    resolve(JSON.parse(response.responseText));
                },
                failure(response) {
                    reject(JSON.parse(response.responseText));
                }
            });
        });
    }
});

/**
 * Los datos necesario para efectuar el registro de un movimiento
 * @typedef {Object} MovimientoInstruccion
 * @property {string} tipoMovimiento El tipo de movimiento a procesar
 * @property {int} estadoDeCuentaId - El estado de cuenta afectado por el movimiento
 * @property {number} numeroAfiliacion - El número de afiliación de quien pertenece la
 * cuenta afectada por el movimiento
 * @property {number} importe - El importe del movimiento
 * @property {'ABONO'|'CARGO'} tipo - El tipo de importe del movimiento
 */
