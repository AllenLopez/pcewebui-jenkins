Ext.define('Pce.ingresos.asegurado.Movimiento', {
    extend: 'Ext.data.Model',
    fields: [
        'transaccionId',
        'conceptoId',
        'depenencia',
        'subcuenta',
        'asegurado',
        'detalleTransaccion',
        'tipoMovimiento',
        'signo',
        'tipo',
        'ejercicio',
        'documento',
        'importe',
        'importeNeto',
        'tipoMovimientoDescripcion',
        'dependenciaDescripcion',
        'dependenciaSubmotivoDescripcion',
        'dependenciaMovimientoDescripcion',
        'usuarioCreacion',
        {name: 'fechaCreacion', type: 'date'},
        {name: 'fechaMovimiento', type: 'date'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/movimiento'
    }
});