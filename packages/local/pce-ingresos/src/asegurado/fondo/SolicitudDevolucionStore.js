/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.fondo.SolicitudDevolucionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.fondo-solicitud-devolucion',

    model: 'Pce.ingresos.asegurado.fondo.SolicitudDevolucion'
});