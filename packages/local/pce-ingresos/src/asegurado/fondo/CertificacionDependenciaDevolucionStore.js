Ext.define('Pce.ingresos.asegurado.fondo.CertificacionDependenciaDevolucionStore', {
    extend: 'Ext.data.Store',
    alias: 'store.certificacion-dependencia-store',

    Model: 'Pce.ingresos.asegurado.fondo.CertificacionDependenciaDevolucion'
});