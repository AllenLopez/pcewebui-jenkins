/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.fondo.SolicitudDevolucion', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.proxy.Rest'],

    fields: [
        'folio',
        'asegurado',
        'concepto',
        {name: 'conceptoDesc', persist: false},
        'aseguradoNombreCompleto',
        'depBeneficiario',
        'dependenciaBeneficiarioDesc',
        'dependenciaMovimiento',
        'dependenciaMovimientoDesc',
        'etapa',
        'observaciones',
        'telefono',
        'defuncion',
        'delegacion',
        'delegacionDescripcion',
        'importePagar',
        'importeDependencia',
        'importeFondo',
        'importeServicioMedico',
        'importeRetCI',
        'importeAportCI',
        'afectacion',
        'usuarioCertificaServicioMedico',
        'usuarioCertificaDependencia',
        'totalChequesDevolucion',
        {name: 'fechaRegistrosDep', type: 'date'},
        {name: 'fechaRegistrosSm', type: 'date'},
        {name: 'fechaCaptura', type: 'date'},
        {name: 'fechaAprobacion', type: 'date'},
        {name: 'usuarioCreacion', type: 'date'},
        {name: 'fechaCreacion', type: 'date'},
        {name: 'fechaModificacion', type: 'date'},
        {name: 'fechacreacion', type: 'date'},
    ],

    hasMany: {
        model: 'Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud',
        name: 'cheques'
    },

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/fondo/solicitud-devolucion',
        api: {
            create  : null,
            read    : '/api/ingresos/asegurado/fondo/solicitud-devolucion/solicitud/',
            update  : null,
            destroy : null
        },
        writer: {
            writeAllFields: true
        }
    }
});