Ext.define('Pce.ingresos.asegurado.fondo.CertificacionDependenciaDevolucion', {
    extend: 'Ext.data.Model',

    fields: [
        'numeroDependencia',
        'dependenciaDescripcion',
        'importe',
        'usuarioCertificacionDependencia',
        {
            name: 'solicitudDevolucionId',
            type: 'int',
            reference: 'Pce.ingresos.asegurado.fondo.SolicitudDevolucion'
        }
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/asegurado/certificacion-dependencia-devolucion',
        writer: {
            writeAllFields: true
        }
    }
});