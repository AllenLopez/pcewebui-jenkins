Ext.define('Pce.ingresos.reporte.ReporteStore', {
    extend: 'Ext.data.Store',
    alias: 'store.reportes',
    model: 'Pce.ingresos.reporte.Reporte'
});