Ext.define('Pce.ingresos.reporte.Reporte', {
    extend: 'Ext.data.Model',

    fields: [
        'nombre'
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/reporte'
    }
});