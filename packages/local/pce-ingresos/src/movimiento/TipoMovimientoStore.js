Ext.define('Pce.ingresos.movimiento.TipoMovimientoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tipos-movimiento',

    model: 'Pce.ingresos.movimiento.TipoMovimiento'
});