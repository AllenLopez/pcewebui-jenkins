Ext.define('Pce.ingresos.quincena.Quincena', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'numQuincena', type: 'integer'},
        {name: 'fecha', type: 'date'},
        {name: 'ejercicio', type: 'integer'},
        {name: 'descripcion', type: 'string'}
    ],

    proxy: {
        type: 'rest',
        url: '/api/ingresos/quincena/por-ejercicio',

        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});