Ext.define('Pce.ingresos.quincena.QuincenaStore', {
    extend: 'Ext.data.Store',
    alias: 'store.quincena',
    
    model: 'Pce.ingresos.quincena.Quincena'
});