/**
 * @class Pce.ingresos.jubilado.procesos.ConsultaSaldoJubiladoPanel
 * @extends Ext.panel.Panel
 * @xtype consulta-saldo-jubilado-panel
 * Pantalla de consulta de listado de asegurados nuevos jubilados para reajustar saldo pendiente
 * @author Jorge Escamilla
 * */

Ext.define('Pce.ingresos.jubilado.procesos.ConsultaSaldoJubiladoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'consulta-saldo-jubilado-panel',

    requires: [
        'Pce.ingresos.jubilado.procesos.ConsultaSaldoJubiladoController',
        'Pce.ingresos.jubilado.procesos.MesEjercicioJubiladoTabla',
        'Pce.ingresos.jubilado.procesos.ConsultaSinAportacionTabla',
        // 'Pce.caja.movimiento.TipoMovimientoSelector',
        'Pce.movimiento.TipoMovimientoSelector',
        'Pce.componentes.MonthField',

        'Pce.ingresos.movimiento.TipoMovimientoStore',
        'Pce.ingresos.comun.MesesStore',
        'Pce.ingresos.comun.EjercicioStore'
    ],

    controller: 'consulta-jubilados-panel',

    layout: { type: 'fit', },

    viewModel: {
        data: {
            mesInicial: null,
            mesFinal: null,
            etapa: null,
            tipo: null,
            regimen: null,
            transaccionId: null,
            jubiladosCount: 0,
        }
    },

    title: 'Consulta de actualización fondo',
        items: [{
        xtype: 'mes-ejercicio-jubilado-tabla',
        reference: 'jubiladoAgrupadoTabla',
        layout: 'fit',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            reference: 'formulario',
            items: [{
                xtype: 'monthfield',
                name: 'mesInicio',
                fieldLabel: 'Desde',
                queryMode: 'local',
                bind: {
                    value: '{mesInicial}'
                },
                flex: 1
            }, {
                xtype: 'monthfield',
                name: 'mesFinal',
                fieldLabel: 'Hasta',
                queryMode: 'local',
                bind: {
                    value: '{mesFinal}'
                },
                flex: 1
            }, {
                xtype: 'movimiento-selector',
                reference: 'movimientoSelector',
                name: 'tipo',
                fieldLabel: 'Tipo',
                allowblank: false,
                store: {
                    type: 'tipos-movimiento',
                    autoLoad: true,
                    filters: [{
                        property: 'clave',
                        operator: 'in',
                        value: ['JUBILACION', 'SAM5']
                    }]
                },
                bind: {
                    value: '{tipo}'
                },
                flex: 2
            }]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'combobox',
                name: 'regimen',
                displayField: 'desc',
                valueField: 'value',
                fieldLabel: 'Regimen',
                queryMode: 'local',
                store: {
                    fields: ['desc'],
                    data: [
                        {desc: 'LEY ANTERIOR', value: 'LA'},
                        {desc: 'TRANSICIÓN', value: 'TR'},
                    ]
                },
                bind: {
                    value: '{regimen}'
                },
                flex: 1
            }, {
                xtype: 'combobox',
                name: 'estatus',
                fieldLabel: 'Estatus',
                displayField: 'etapa',
                valueField: 'value',
                store: {
                    data: [
                        { etapa: 'AUTORIZADA', value: 'AU' },
                        { etapa: 'CANCELADA', value: 'CA' },
                        { etapa: 'TRAMITE', value: 'TR' },
                        { etapa: 'EN CONTROL', value: 'CT' }
                    ]
                },
                queryMode: 'local',
                bind: {
                    value: '{etapa}'
                },
                flex: 1
            }, {
                xtype: 'numberfield',
                name: 'Folio transaccion',
                fieldLabel: 'Transacción',
                bind: {
                    value: '{transaccionId}'
                },
                flex: 1
            }, {
                text: 'Buscar',
                handler: 'consultarJubiladosTap',
                flex: 1
            }]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Realiza autorización',
                handler: 'onAutorizarTap',
                bind: {
                    disabled: '{!jubiladoAgrupadoTabla.selection || jubiladoAgrupadoTabla.selection.folioTransaccion > 0}'
                }
            }, {
                text: 'Realiza afectación',
                reference: 'afectarCarteraTap',
                disabled: true,
                handler: 'onAfectarCarteraTap',
                bind: {
                    disabled: '{!jubiladoAgrupadoTabla.selection || jubiladoAgrupadoTabla.selection.folioTransaccion <= 0 || jubiladoAgrupadoTabla.selection.etapa !== "CT"}'
                }
            }
            //{
            //     text: 'Reporte de carga saldo',
            //     handler: 'onMostrarReporteTap',
            //     disabled: true,
            //     bind: {
            //         disabled: '{!jubiladoAgrupadoTabla.selection || jubiladoAgrupadoTabla.selection.folioTransaccion > 0}'
            //     }
            // }, {
            //     text: 'Reporte afectación saldo',
            //     handler: 'onMostrarReporteAfectacionTap',
            //     disabled: true,
            //     bind: {
            //         disabled: '{!jubiladoAgrupadoTabla.selection || jubiladoAgrupadoTabla.selection.etapa !== "AU"}'
            //     }
            //}
        ]
        }],
    }],
    
});

