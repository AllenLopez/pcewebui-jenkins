/**
 * @class Pce.ingresos.jubilado.procesos.MesEjercicioJubiladoTabla
 * @extends Ext.grid.Panel
 * @xtype mes-ejercicio-jubilado-tabla
 * Grid que enlista resultados de jubilados con saldo por mes y regimen determinados
 * @author Jorge Escamilla
 * */

Ext.define('Pce.ingresos.jubilado.procesos.MesEjercicioJubiladoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'mes-ejercicio-jubilado-tabla',

    requires: [
        'Pce.ingresos.jubilado.JubiladoAgrupadoMensualStore'
    ],

    store: {
        type: 'jubilado-agrupado-mes'
    },
    // bind:{
    //     hidden: '!movimientoSelector.seleccion === JUBILACION'
    // },
    emptyText: 'No hay datos para mostrar',

    columns: [{
        text: 'Fecha creacion',
        dataIndex: 'fechaCreacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Fecha Carga',
        dataIndex: 'fechaCarga',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1,
        // bind: {
        //     hidden: '{movimientoSelector.selection}'
        // }
    }, {
        text: 'Mes',
        dataIndex: 'mes',
        flex: 1,
        // bind:{
        //     hidden: 'movimientoSelector.selection.id === 244'
        // }
    }, {
        text: 'Año',
        dataIndex: 'ejercicio',
        flex: 1,
        // bind:{
        //     hidden: 'movimientoSelector.selection.id === 244'
        // }
    }, {
        text: 'Total Asegurados/Jubilados',
        dataIndex: 'totalJubilados',
        flex: 1.5
    }, {
        text: 'Tipo',
        dataIndex: 'tipo',
        flex: 3
    }, {
        text: 'Régimen',
        dataIndex: 'regimen',
        renderer(v) {
            switch (v) {
                case 'LA': return 'LEY ANTERIOR';
                case 'TR': return 'TRANSICIÓN';
                default: return 'OTRO';
            }
        },
        flex: 1
    }, {
        text: 'Transacción',
        dataIndex: 'folioTransaccion',
        flex: 1
    }, {
        text: 'Etapa',
        dataIndex: 'etapa',
        renderer(v) {
            switch (v) {
                case 'TR': return 'EN TRÁMITE';
                case 'AU': return 'AUTORIZADA';
                case 'CA': return 'CANCELADA';
                case 'CR': return 'CERRADA';
                case 'CT': return 'EN CONTROL';
                case null: return '';
                default: return 'OTRA';
            }
        },
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: 50,
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'Ver detalles',
            handler: 'onMostrarDetalles'
        },{
            iconCls: 'x-fa fa-print',
            tooltip: 'Imprimir carga saldo',
            handler: 'onMostrarReporteTap' 
        }]
    }]
});