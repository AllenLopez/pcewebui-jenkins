Ext.define('Pce.ingresos.jubilado.procesos.ConsultaSaldoJubiladoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-jubilados-panel',

    requires: [
        'Pce.ingresos.transaccion.ResultadoDialogo',
    ],
    
    consultarJubiladosTap() {
        const { mesInicial, mesFinal, tipo, etapa, regimen, transaccionId } = this.getViewModel().getData();
        const tipoMovimiento = this.lookup('movimientoSelector').getSelection().getId();
        const store = this.getAjusteSaldoStore();
        if (tipoMovimiento == 242){
            store.setProxy({
                type: 'rest',
                url: '/api/ingresos/jubilado/carga-saldo/jubilados-mes-ejercicio',
                writer: {
                    type: 'json',
                    writeAllFields: true
                }
            }),
            store.load({
                params: {
                    fechaInicial: mesInicial,
                    fechaFinal: mesFinal,
                    tipo: tipo,
                    etapa: etapa,
                    regimen: regimen,
                    transaccionId: transaccionId
                },
             });
        }
        else {
            store.setProxy({
                type: 'rest',
                url: '/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/asegurados-consulta',
                writer: {
                    type: 'json',
                    writeAllFields: true
                }
            }),
            store.load({
                params: {
                    fechaInicial: mesInicial,
                    fechaFinal: mesFinal,
                    tipo: tipo,
                    etapa: etapa,
                    regimen: regimen,
                    transaccionId: transaccionId
                },
           });
        }
    },

    onAutorizarTap(){
        const me = this;
        const registro = this.getAjusteSaldoRecord();
        const regimen = registro.get('regimen');
        const tipoMovimiento = this.lookup('movimientoSelector').getSelection().getId();
        let fechaJubilacion = `01/${registro.get('mesJubilacion')}`;
        const fechaNatural = me.getFechaJubilacionNatural(`01/${registro.get('mesJubilacion')}`);


        if (registro && tipoMovimiento == 242) {
            this.confirmarAutorizacion(fechaNatural, regimen).then(ans => {
                if (ans === 'yes') {
                    Ext.getBody().mask('autorizando...');
                    this.realizarAutorizacion(fechaJubilacion, regimen)
                        .then(transaccionId => {
                            Ext.Msg.alert('Proceso terminado',
                                `Se ha completado exitosamente la autorización de los saldos de jubilados con la transacción: <b>${transaccionId}</b>`);
                            me.consultarJubiladosTap();
                            Ext.getBody().unmask();
                        },
                        fallo => {
                            Ext.Msg.alert('Proceso terminado',
                                `Ocurrió un problema al intentar autorizar los saldos de jubilados seleccionados: ${fallo}`);
                            Ext.getBody().unmask();
                        });
                }
            });
        }
        else{
            let fechaAportacion = Ext.util.Format.date(registro.get('fechaCarga'),'d/m/y');            
            this.confirmarAutorizacion(fechaAportacion, regimen).then(ans => {
                if (ans === 'yes') {
                    Ext.getBody().mask('autorizando...');
                    this.RealizarAutorizacionSinAportacion(fechaAportacion, regimen)
                        .then(transaccionId => {
                            Ext.Msg.alert('Proceso terminado',
                                `Se ha completado exitosamente la autorización de los saldos de asegurados sin aportacion en mas de 5 años con la transacción: <b>${transaccionId}</b>`);
                            me.consultarJubiladosTap();
                            Ext.getBody().unmask();
                        },
                        fallo => {
                            Ext.Msg.alert('Proceso terminado',
                                `Ocurrió un problema al intentar autorizar los saldos de asegurados sin aportacion en mas de 5 años seleccionados: ${fallo}`);
                            Ext.getBody().unmask();
                        });
                }
            });

        }
    },
    confirmarAutorizacion(fecha, regimen) {
        const regimenTotal = regimen === 'LA' ? 'LEY ANTERIOR' : 'TRANSICION';
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se realizará la autorización del saldo de los asegurados/jubilados seleccionados para ${fecha} con ${regimenTotal}. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    onAfectarCarteraTap() {
        const me = this;
        const tipoMovimiento = me.lookup('movimientoSelector').getSelection().getId();
        const registro = me.getAjusteSaldoRecord();
        const fechaNatural = me.getFechaJubilacionNatural(`01/${registro.get('mesJubilacion')}`);
        const fecha = registro.get('mesJubilacion');
        const transaccionId = registro.get('folioTransaccion');
        const regimen = registro.get('regimen');

        if (registro && tipoMovimiento == 242) {
            me.confirmarAfectacion(fechaNatural, regimen).then(ans => {
            if (ans === 'yes') {
                Ext.getBody().mask('afectando cartera...');
                me.realizarAfectacion(fecha, regimen, transaccionId)
                .then(resp => {
                    if (resp.success) {
                        Ext.Msg.alert('Proceso terminado',
                            `Se ha completado exitosamente la afectación de los saldos de asegurados/jubilados`);
                        this.mostrarReporteAfectacionJubilados(transaccionId, regimen);
                        this.getAjusteSaldoStore().reload();
                    } else {
                        Ext.Msg.alert('Proceso terminado',
                            `Ocurrió un problema al intentar afectar cartera con los saldos de asegurados/jubilados seleccionados: ${resp.mensaje}`);
                    }
                    Ext.getBody().unmask();
                    },
                    fallo => {
                        Ext.Msg.alert('Proceso terminado',
                            `Ocurrió un problema al intentar afectar cartera con los saldos de asegurados/jubilados seleccionados: ${fallo}`);
                        Ext.getBody().unmask();
                    });
                }
            });
        }
        else{
            let fechaAportacion = Ext.util.Format.date(registro.get('fechaCarga'),'d/m/y');
            me.confirmarAfectacion(fechaAportacion, regimen).then(ans => {
                if (ans === 'yes') {
                    Ext.getBody().mask('afectando cartera...');
                    me.realizarAfectacionSinAportacion(fechaAportacion, regimen, transaccionId)
                    .then(resp => {
                        if (resp.success) {
                            Ext.Msg.alert('Proceso terminado',
                                `Se ha completado exitosamente la afectación
                                 de los saldos de asegurados sin aportacion en mas de 5 años`);
                            this.mostrarReporteAfectacionJubilados(transaccionId, regimen);
                            this.getAjusteSaldoStore().reload();
                        } else {
                            Ext.Msg.alert('Proceso terminado',
                                `Ocurrió un problema al intentar afectar cartera 
                                 con los saldos de asegurados sin aportacion en mas de 5 años seleccionados: ${resp.mensaje}`);
                        }
                        Ext.getBody().unmask();
                        },
                        fallo => {
                            Ext.Msg.alert('Proceso terminado',
                                `Ocurrió un problema al intentar afectar cartera 
                                con los saldos de asegurados sin aportacion en mas de 5 años seleccionados: ${fallo}`);
                            Ext.getBody().unmask();
                        });
                    }
                });
        }
    },

    realizarAutorizacion(fecha, regimen) {
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: '/api/ingresos/jubilado/carga-saldo/autorizar-saldo',
                method: 'POST',
                jsonData: {
                    fechaMovimiento: fecha,
                    regimen: regimen
                },
                success(response) {
                    if(response.responseText) {
                        let resp = JSON.parse(response.responseText);
                        if (resp.success) {
                            resolve(resp.transaccionId);
                        } else {
                            reject(resp.mensaje);
                        }
                    } else {
                        reject(response);
                    }
                },
                failure(error) {
                    console.log(error);
                    reject(error);
                }
            });
        });
     },

    RealizarAutorizacionSinAportacion(fecha, regimen){
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: '/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/autorizar-saldo',
                method: 'POST',
                jsonData: {
                    fechaMovimiento: fecha,
                    regimen: regimen
                },
                success(response) {
                    if(response.responseText) {
                        let resp = JSON.parse(response.responseText);
                        if (resp.success) {
                            resolve(resp.transaccionId);
                        } else {
                            reject(resp.mensaje);
                        }
                    } else {
                        reject(response);
                    }
                },
                failure(error) {
                    console.log(error);
                    reject(error);
                }
            });
        });
    },

    realizarAfectacion(fecha, regimen, transaccionId) {
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: '/api/ingresos/jubilado/carga-saldo/afectar-saldo',
                method: 'POST',
                jsonData: {
                    fechaMovimiento: `01/${fecha}`,
                    regimen: regimen,
                    transaccionId: transaccionId
                },
                success(response) {
                    resolve(JSON.parse(response.responseText));
                },
                failure(error) {
                    console.log(error);
                    reject(error);
                }
            });
        });
    },

    realizarAfectacionSinAportacion(fecha, regimen, transaccionId){
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: '/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/afectar-saldo',
                method: 'POST',
                jsonData: {
                    fechaMovimiento: fecha,
                    regimen: regimen,
                    transaccionId: transaccionId
                },
                success(response) {
                    resolve(JSON.parse(response.responseText));
                },
                failure(error) {
                    console.log(error);
                    reject(error);
                }
            });
        });
    },

    onMostrarReporteTap() {
        const tipoMovimiento = this.lookup('movimientoSelector').getSelection().getId();
        const record = this.lookup('jubiladoAgrupadoTabla').getSelection()[0];
        const regimen = record.get('regimen');
        const mesJubilacion = record.get('mesJubilacion');
        let fechaAportacion = Ext.util.Format.date(record.get('fechaCarga'),'d/m/y');
        if(tipoMovimiento == 242){
            this.mostrarReporteJubilados(`01/${mesJubilacion}`, regimen, record);
        }
        else{
            this.mostrarReporteSinAportacion(fechaAportacion, regimen, record);
        }
    },

    mostrarReporteJubilados(fechaJubilacion, regimen, record) {
        if(record.data.etapa == null || record.data.etapa == 'CT')
        {
            window.open(`/api/ingresos/jubilado/carga-saldo/descargar-jubilados?fecha=${fechaJubilacion}&regimen=${regimen}`);
        }else{
            window.open(`/api/ingresos/jubilado/carga-saldo/descargar-afectacion-jubilados?transaccionid=${record.data.folioTransaccion}&regimen=${regimen}`);
        }
    },
    mostrarReporteSinAportacion(fecha, regimen,record) {
        if(record.data.etapa == null || record.data.etapa == 'CT')
        {
            window.open(`/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/descargar-asegurados?fecha=${fecha}&regimen=${regimen}`);
        }else{
            window.open(`/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/descargar-afectacion-asegurados?transaccionid=${record.data.folioTransaccion}&regimen=${regimen}`);
        }
       
    },

    onMostrarReporteAfectacionTap() {
        const tipoMovimiento = this.lookup('movimientoSelector').getSelection().getId();
        const record = this.lookup('jubiladoAgrupadoTabla').getSelection()[0];
        const regimen = record.get('regimen');
        const transaccionId = record.get('folioTransaccion');
        if(tipoMovimiento == 242){
            this.mostrarReporteAfectacionJubilados(transaccionId, regimen);
        } 
        else{
            this.mostrarReporteAfectacionSinAportacion(transaccionId, regimen);
        }
        

    },

    mostrarReporteAfectacionJubilados(transaccionId, regimen) {
        window.open(`/api/ingresos/jubilado/carga-saldo/descargar-afectacion-jubilados?transaccionId=${transaccionId}&regimen=${regimen}`);
    },
    mostrarReporteAfectacionSinAportacion(transaccionId, regimen) {
        window.open(`/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/descargar-afectacion-asegurados?transaccionId=${transaccionId}&regimen=${regimen}`);
    },

    onMostrarDetalles(tabla, i, j, item, e, record) {
        this.mostrarDialogoDetallesTabla(record);
    },

    mostrarDialogoDetallesTabla(record){
        console.log(record.get('folioTransaccion'));
        const tipoMovimiento = this.lookup('movimientoSelector').getSelection().getId();
        if(tipoMovimiento ==  242){
        Ext.create('Ext.window.Window',{
            title: 'Jubilados con saldo',
            autoShow: true,
            modal: true,
            width: 1300,
            height: 600,
            layout: 'fit',
            items: {
                xtype: 'saldo-jubilado-tabla',
                mesJubilacion: record.get('mesJubilacion'),
                regimen: record.get('regimen'),
                transaccionId: record.get('folioTransaccion')
            }
        });
        }
        else{
            Ext.create('Ext.window.Window',{
                title: 'Asegurados sin aportacion en mas de 5 años',
                autoShow: true,
                modal: true,
                width: 1300,
                height: 600,
                layout: 'fit',
                items: {
                    xtype: 'saldo-sin-aportacion-tabla',
                    mesAportacion: Ext.util.Format.date(record.get('fechaCarga'), 'd/m/Y'),
                    regimen: record.get('regimen'),
                    transaccionId: record.get('folioTransaccion')
                }
            });
        }
    }, 

    confirmarAfectacion(fecha, regimen) {
        const regimenTotal = regimen === 'LA' ? 'LEY ANTERIOR' : 'TRANSICION';
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar efactación',
                `Se realizará a afectación de cartera del saldo 
                de los asegurados/jubilados seleccionados para ${fecha} con ${regimenTotal}. 
                ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    getFechaJubilacionNatural(fechaJubilacionString) {
        const fechaJubilacion = new Date(
            parseInt(fechaJubilacionString.substr(6,4)),
            parseInt(fechaJubilacionString.substr(3,2)),
            parseInt(fechaJubilacionString.substr(0,2)));
        const meses = ['','ENERO','FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO',
                        'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];
        return `${meses[fechaJubilacion.getMonth()]} de ${fechaJubilacion.getFullYear()}`;
    },

    getAjusteSaldoStore() {
        return this.lookup('jubiladoAgrupadoTabla').getStore(); 
    },

    getAjusteSaldoRecord() {
        return this.lookup('jubiladoAgrupadoTabla').getSelection()[0];
    },
});