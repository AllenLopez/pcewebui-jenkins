/**
 * @class Pce.ingresos.jubilado.procesos.CargaSaldoJubiladoController
 * @extend Ext.app.ViewController
 * @alias cancelacion-saldo-jubilado
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.jubilado.procesos.CargaSaldoJubiladoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.carga-saldo-jubilado',

    onConsultarSaldoJubiladoTap() {
        const { regimen } = this.getViewModel().getData();
        const fecha = this.obtieneFechaJubilacion();
        const store = this.getJubiladosStore();

        store.load({
            params: {
                fechaMovimiento: fecha,
                regimen: regimen
            },
        });
    },

    onSeleccionChange() {
        const store = this.getJubiladosStore();
        if (store.count() > 0) {
            store.load({});
            this.getViewModel().set('jubiladosCount', 0);
        }
    },

    onCargarSaldoTap() {
        const me = this;
        const { jubiladosCount, regimen } = me.getViewModel().getData();
        let fecha = this.obtieneFechaJubilacion();

        if (jubiladosCount < 1) {
            Ext.Msg.alert('No hay registros para procesar',
                `Debe haber al menos un elemento en el listado de jubilados para poder continuar.
                Realice una nueva búsqueda por fecha y régimen`);
            return;
        }

        me.confirmarCargaSaldo(jubiladosCount, regimen, fecha)
            .then(resp => {
                if (resp === 'yes') {
                    Ext.getBody().mask('Procesando...');
                    me.cargaListadoSaldoJubilados(fecha, regimen);
                }
            });
    },

    onMostrarReporteTap() {
        const vm = this.getViewModel().getData();
        this.mostrarReporteJubilados(`01/${vm.mesJubilacion}/${vm.ejercicioJubilacion}`, vm.regimen);
    },

    cargaListadoSaldoJubilados(fechaJubilacion, regimen) {
        const me = this;
        Ext.Ajax.request({
            method: 'POST',
            url: '/api/ingresos/jubilado/carga-saldo/cargar-jubilados',
            jsonData: {
                FechaMovimiento: fechaJubilacion,
                Regimen: regimen
            },
            success(response) {
                me.cargaListadoSaldoJubiladoSuccess(
                    JSON.parse(response.responseText),
                    fechaJubilacion, regimen);
            },
            failure(response) {
                me.cargaListadoSaldoJubiladoFail(JSON.parse(response.responseText));
            }
        });
    },

    cargaListadoSaldoJubiladoSuccess(response, fechaJubilacion, regimen) {
        console.log(response);
        if (response.success){
            Ext.Msg.alert(
                'Proceso terminado',
                `Carga de saldos completada con éxito`
            );

            this.mostrarReporteJubilados(fechaJubilacion, regimen);
        } else {
            this.mostrarMensajeAlerta(response.mensaje);
        }

        Ext.getBody().unmask();
    },

    cargaListadoSaldoJubiladoFail(response) {
        this.mostrarMensajeAlerta(response.mensaje);
        Ext.getBody().unmask();
    },

    afectacionSaldoJubiladoSuccess(response, transaccionId) {
        if (response.success){
            Ext.Msg.alert(
                'Proceso terminado',
                `La transacción ${transaccionId}, se completó exitosamente`
            );
        } else {
            Ext.Msg.alert(
                'Ocurrió un error',
                `Se generó un error al procesar: ${response.mensaje}`
            );
        }

        Ext.getBody().unmask();
    },

    confirmarCargaSaldo(jubilados, regimen, fecha) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar carga',
                `Se cargarán ${jubilados} jubilados al sistema con saldo pendiente correspondiente a la fecha ${fecha} seleccionado. ¿Desea proceder?`,
                ans => resolve(ans));
        });
    },

    mostrarReporteJubilados(fecha, regimen) {
        window.open(`/api/ingresos/jubilado/carga-saldo/descargar-jubilados?fecha=${fecha}&regimen=${regimen}`);
    },

    mostrarMensajeAlerta(mensaje) {
        Ext.Msg.alert(
            'Alerta',
            `Ocurrió un error al procesar: ${mensaje}`
        );
    },

    obtieneFechaJubilacion() {
        const { mesJubilacion, ejercicioJubilacion } = this.getViewModel().getData();
        return `01/${mesJubilacion}/${ejercicioJubilacion}`;
    },

    getTransaccion(id) {
        const me = this;

        Ext.Ajax.request({
            method: 'GET',
            url: `/api/ingresos/transaccion/${id}`,
            success(response) {
                const transaccion = Ext.decode(response.responseText);
                me.setViewModelDataProp('transaccionEtapa', me.decodeEtapa(transaccion.etapa));
            }
        });
    },

    getJubiladosStore() {
        return this.lookup('saldoJubiladoTabla').getStore();
    },

    decodeEtapa(etapa) {
        switch (etapa) {
            case 'TR': return 'EN TRAMITE';
            case 'CT': return 'EN CONTROL';
            case 'CA': return 'CANCELADA';
            case 'AU': return 'AUTORIZADA';
            default: return 'Se define al cargar saldos';
        }
    },

    onStoreUpdate(store) {
        let records = store.getCount();
        if (records > 0) {
            this.setViewModelDataProp('jubiladosCount', records);
        }
    },

    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },
});