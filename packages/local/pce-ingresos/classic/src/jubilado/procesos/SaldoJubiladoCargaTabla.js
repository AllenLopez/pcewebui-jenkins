/**
 * @class Pce.ingresos.jubilado.procesos.SaldoJubiladoCargaTabla
 * @extends Ext.grid.Panel
 * @xtype saldo-jubilado-carga-tabla
 * Grid que enlista resultados de jubilados con saldo por mes y regimen determinados
 * @author Jorge Escamilla
 * */

Ext.define('Pce.ingresos.jubilado.procesos.SaldoJubiladoCargaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'saldo-jubilado-carga-tabla',

    columns: [{
        text: 'Id',
        dataIndex: '',
        flex: 1
    }, {
        text: 'Transaccion',
        dataIndex: 'transaccion',
        flex: 1
    }, {
        text: 'Regimen',
        dataIndex: 'regimen',
        flex: 2
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        flex: 4
    }, {
        text: 'Fecha jubilacion',
        dataIndex: 'fechaJubilacion',
        flex: 2
    }, {
        text: 'Fecha creacion',
        dataIndex: 'fechaCreacion',
        flex: 2
    }, {
        text: 'Saldo',
        dataIndex: 'saldo',
        flex: 1
    }]
});