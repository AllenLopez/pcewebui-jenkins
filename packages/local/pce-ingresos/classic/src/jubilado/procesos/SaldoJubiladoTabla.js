/**
 * @class Pce.ingresos.jubilado.procesos.SaldoJubiladoTabla
 * @extends Ext.grid.Panel
 * @xtype saldo-jubilado-tabla
 * Grid que enlista resultados de busqueda de jubilados con saldo por mes y regimen determinados
 * @author Jorge Escamilla
 * */

Ext.define('Pce.ingresos.jubilado.procesos.SaldoJubiladoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'saldo-jubilado-tabla',

    requires: [
        'Pce.ingresos.jubilado.JubiladoNuevoStore'
    ],

    store: {
        type: 'jubilado-nuevo'
    },

    emptyText: 'No hay datos para mostrar',

    columns: [{
        text: 'Núm. afil.',
        dataIndex: 'aseguradoNumeroAfiliacion',
        flex: 1
    }, {
        text: 'Nombre',
        dataIndex: 'aseguradoNombreCompleto',
        flex: 3
    }, {
        text: 'Fecha jubilación',
        dataIndex: 'fechaJubilacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Clave dep.',
        dataIndex: 'dependenciaNumeroDependencia',
        flex: 1
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        flex: 4
    }, {
        text: 'Folio transacción',
        dataIndex: 'transaccionId',
        flex: 1
    }, {
        text: 'Saldo actual',
        dataIndex: 'saldo',
        renderer: v => Ext.util.Format.usMoney(v),
        flex: 1
    }, {
        text: 'Usuario creación',
        dataIndex: 'usuarioCreacion',
        flex: 1
    }],

    setRecord(fecha, regimen, transaccion) {
        this.mesJubilacion = fecha;
        this.regimen = regimen;
        this.transaccionId = transaccion;

        this.store.load({
            params: {
                mesJubilacion: fecha,
                regimen: regimen,
                transaccionId: transaccion
            }
        });
    },

    afterRender() {
        this.callParent();
        if(this.mesJubilacion && this.regimen && this.transaccionId) {
            this.setRecord(this.mesJubilacion, this.regimen, this.transaccionId);
        }
        if (this.mesJubilacion && this.regimen) {
            this.setRecord(this.mesJubilacion, this.regimen, 0);
        }
    }
});