/**
 * @class Pce.ingresos.jubilado.procesos.CargaSaldoJubiladoPanel
 * @extends Ext.panel.Panel
 * @xtype carga-saldo-jubilado-panel
 * Pantalla de búsqueda de asegurados nuevos jubilados para reajustar saldo pendiente
 * @author Jorge Escamilla
 * */

Ext.define('Pce.ingresos.jubilado.procesos.CargaSaldoJubiladoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'carga-saldo-jubilado-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.ingresos.comun.MesesStore',
        'Pce.ingresos.comun.EjercicioStore',
        'Pce.ingresos.jubilado.procesos.CargaSaldoJubiladoController',
        'Pce.ingresos.jubilado.procesos.SaldoJubiladoTabla'
    ],

    controller: 'carga-saldo-jubilado',

    layout: {
        type: 'fit',
    },

    viewModel: {
        data: {
            mesJubilacion: null,
            ejercicioJubilacion: null,
            regimen: null,
            jubiladosCount: 0,
        },
        stores: {
            jubilados: {
                type: 'jubilado-nuevo',
                proxy: {
                    type: 'rest',
                    url: '/api/ingresos/jubilado/carga-saldo',
                    reader: {
                        type: 'json',
                    }
                },
                listeners: {
                    load: 'onStoreUpdate',
                    update: 'onStoreUpdate',
                    datachanged: 'onStoreUpdate'
                }
            }
        }
    },

    title: 'Carga saldos de jubilados nuevos',

    items: [{
        xtype: 'saldo-jubilado-tabla',
        reference: 'saldoJubiladoTabla',
        layout: 'fit',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            reference: 'formulario',
            items: [{
                xtype: 'combobox',
                name: 'mesJubilacion',
                fieldLabel: 'Mes',
                displayField: 'nombre',
                valueField: 'mes',
                store: {
                    type: 'meses-ejercicio'
                },
                forceSelection: true,
                listeners: {
                    change: 'onSeleccionChange'
                },
                queryMode: 'local',
                bind: {
                    value: '{mesJubilacion}'
                }
            } ,{
                xtype: 'combobox',
                reference: 'ejerciciosSelector',
                name: 'mesJubilacion',
                fieldLabel: 'Ejercicio',
                displayField: 'ejercicio',
                valueField: 'ejercicio',
                store: {
                    type: 'ejercicio',
                },
                forceSelection: true,
                listeners: {
                    change: 'onSeleccionChange'
                },
                queryMode: 'local',
                bind: {
                    value: '{ejercicioJubilacion}'
                }
            }, {
                xtype: 'combobox',
                name: 'regimen',
                displayField: 'desc',
                valueField: 'value',
                fieldLabel: 'Regimen',
                queryMode: 'local',
                listeners: {
                    change: 'onSeleccionChange'
                },
                store: {
                    fields: ['desc'],
                    data: [
                        {desc: 'LEY ANTERIOR', value: 'LA'},
                        {desc: 'TRANSICIÓN', value: 'TR'},
                    ]
                },
                bind: {
                    value: '{regimen}'
                }
            }]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Consultar jubilados del mes',
                handler: 'onConsultarSaldoJubiladoTap',
                bind: {
                    disabled: '{!regimen}',
                }
            }, {
                text: 'Cargar saldo',
                reference: 'cargaSaldoTap',
                handler: 'onCargarSaldoTap',
                bind: {
                    disabled: '{jubiladosCount < 1}'
                },
            }, {
                text: 'Generar reporte de carga',
                handler: 'onMostrarReporteTap',
                bind: {
                    disabled: '{jubiladosCount < 1}'
                }
            }, {
                xtype: 'displayfield',
                fieldLabel: 'Total jubilados',
                bind: {
                    value: '{jubiladosCount}'
                }
            }]
        }],
        bind: {
            store: '{jubilados}'
        }
    }]

});