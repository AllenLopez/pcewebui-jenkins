/**
 * @class Pce.ingresos.jubilado.procesos.MesEjercicioSinAportacionTabla
 * @extends Ext.grid.Panel
 * @xtype mes-ejercicio-jubilado-tabla
 * Grid que enlista resultados de asegurados sin aportacion con saldo regimen determinados
 * @author Jorge Escamilla
 * */

Ext.define('Pce.ingresos.jubilado.procesos.ConsultaSinAportacionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'consulta-sin-aportacion-tabla',
    
    

    requires: [
        'Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacionConsultaStore'
    ],

    store: {
        type: 'asegurado-sin-aportacion-consulta'
    },
    bind:{
        hidden: '!movimientoSelector.seleccion === SAM5'
    },
    emptyText: 'No hay datos para mostrar',
    columns: [{
        text: 'Fecha creacion',
        dataIndex: 'fechaCreacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    },{
        text: 'Fecha Carga',
        dataIndex: 'fechaCarga',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    },{
        text: 'Total Asegurados',
        dataIndex: 'totalAsegurados',
        flex: 1
    }, {
        text: 'Tipo',
        dataIndex: 'tipo',
        flex: 1
    }, {
        text: 'Régimen',
        dataIndex: 'regimen',
        renderer(v) {
            switch (v) {
                case 'LA': return 'LEY ANTERIOR';
                case 'TR': return 'TRANSICIÓN';
                default: return 'OTRO';
            }
        },
        flex: 1
    }, {
        text: 'Transacción',
        dataIndex: 'folioTransaccion',
        flex: 1
    }, {
        text: 'Etapa',
        dataIndex: 'etapa',
        renderer(v) {
            switch (v) {
                case 'TR': return 'EN TRÁMITE';
                case 'AU': return 'AUTORIZADA';
                case 'CA': return 'CANCELADA';
                case 'CR': return 'CERRADA';
                case 'CT': return 'EN CONTROL';
                case null: return '';
                default: return 'OTRA';
            }
        },
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: 50,
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'Ver detalles',
            handler: 'onMostrarDetalles'
        },{
            iconCls: 'x-fa fa-print',
            tooltip: 'Imprimir carga saldo',
            handler: 'onMostrarReporteTap' 
        }]
    }]
});