/**
 * @class Pce.ingresos.aplicacionSaldo.AplicacionSaldoTabla
 * @extends Ext.grid.Panel
 * @xtype aplicacion-saldo-tabla
 * Tabla que enlista los avisos de cargo 
 */
Ext.define('Pce.ingresos.aplicacionSaldo.AplicacionSaldoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'aplicacion-saldo-tabla',

    requires: [
       'Ext.grid.feature.Grouping',
       'Pce.ingresos.aplicacionSaldo.AplicacionSaldoController',
       'Pce.ingresos.dependencia.EstadoDeCuentaStore',

       'Ext.plugin.Abstract',
       'Ext.grid.plugin.CellEditing'
    ],


    store: {
        type: 'dependencia-estados-de-cuenta',
        groupField: 'emisionAvisoCargoFolio',
        proxy: {
            type: 'rest',
            url: '/api/ingresos/certificado/avisos-cargo'
        },
        autoLoad:true
    },
    features: [{
        ftype:'grouping',
        //startCollapsed: true,
        groupHeaderTpl: ['Aviso cargo: {name} ({[values.children.length]})']
    },{
        ftype: 'summary',
        dock: 'bottom'
    }],


    plugins: [{
        ptype: 'cellediting',
        reference: 'captura-editor',
        clicksToEdit: 1
    }],
    

    listeners: [{
        beforeedit: 'onBeforeEdit',
        validateedit: 'onValidateEdit',
        edit: 'onEditImporte'
    }],

    queryMode: 'local',

    selModel: {
        selType: 'checkboxmodel',
        showHeaderCheckbox : false,
        checkOnly: true,
        listeners: {
            deselect: 'onDeselect'
        }
    },

    controller: 'aplicacion-saldo-controller',

    emptyText: 'No existen registros',

    columns: [{
        text: 'Concepto',
        dataIndex: 'conceptoDescripcion',
        flex: 2
    }, {
        xtype: 'datecolumn',
        text: 'Fecha',
        dataIndex: 'fechaNomina',
        format: 'd/m/Y',
        flex: 1
    },{
        text: 'Saldo',
        dataIndex: 'saldo',
        renderer: v => Ext.util.Format.currency(v,'$'),
        flex: 1
    },  {
        text: 'Importe aplicar',
        dataIndex: 'importe',
        flex: 1,
        readOnly: true,
        reference: 'editorImporte',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        summaryType: 'sum',
        summaryRenderer: 'onSummary',
        editor: {
            xtype: 'numberfield',
            hideTrigger: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false
        },
    }],

  
});