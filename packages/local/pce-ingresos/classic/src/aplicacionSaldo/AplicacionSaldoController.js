
Ext.define('Pce.ingresos.aplicacionSaldo.AplicacionSaldoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.aplicacion-saldo-controller',

    requires: [
        
       ],

    importeTotal: 0,

    certificado: null,

    onDependenciaSelection(comp, dependencia){
        let certificadoStore = this.lookup('certificadoSelector').getStore();
        certificadoStore.load({
            params: {
                dependenciaId: dependencia.id
            }
        });
        this.getView().getStore().removeAll()
    },

    onCertificadoSelection(comp, certificado){
        this.loadTable(certificado);

    },

    onBeforeEdit(editor, contexto) {
        editor.aviso = contexto.record;
    },

    onValidateEdit(editor, contexto) {
        
    },

    onEditImporte(editor, contexto) {
        let registro = contexto.record;
        let seleccion = this.lookup('aplicacionSaldoTabla').getSelectionModel().getSelection();
        let filtro = seleccion.filter(x => x.id == registro.id);
        if(filtro.length == 0){
            if(registro.get('importe') == 0){
                return;
            }
            registro.set('importe', 0);
            Ext.Msg.alert('Advertencia!', 'Se debe seleccionar primero para editar el importe a aplicar');
            return;
        }
        if(registro.get('importe') === 0){
            Ext.Msg.alert('Advertencia!', 'El importe a aplicar no puede ser igual 0');
            return;
        }
        if(registro.get('importe') > registro.get('saldo')){
            registro.set('importe', 0);
            Ext.Msg.alert('Advertencia!', 'El importe a aplicar no puede ser mayor al saldo');
            return;
        }
    },

    onSummary(value, summaryData, dataIndex) {
        this.importeTotal = value;

        return `<b>Total: ${Ext.util.Format.usMoney(value)}</b>`;
    },

    getSelectedSumFn(columna){
            var records = this.view.getSelectionModel().getSelection(),
            result  = 0;
            Ext.each(records, function(record){
                result += record.get(columna) * 1;
            });
            return result;
    
    },

    onDeselect(comp, record, supressed){
        record.set("importe", 0);
    },

    loadTable(certificado){
        let tablaStore = this.view.getStore();
        this.certificado = certificado;
        tablaStore.load({
            params: {
                certificadoId: certificado.id
            }
        })
    },

    onAfectarCartera(){
        
        let selectionModel = this.view.getSelectionModel();
        let seleccion = selectionModel.getSelection();
        if(seleccion.length == 0){
            Ext.Msg.alert('Advertencia!', 'No se ha seleccionado ningun registro');
            return;
        }
        if(this.importeTotal > this.certificado.get('saldo')){
            Ext.Msg.alert('Advertencia!', 'El importe total no puede ser mayor al saldo del certificado');
            return;
        }

        Ext.Msg.confirm(
            'Afectar cartera',
            `¿Seguro que deseas afectar cartera?`,
            ans => {
                if(ans === 'yes'){
                    //Afectar cartera
                    let jsonData = {
                        avisos: [],
                        certificadoId: this.certificado.id
                    }
                    seleccion.forEach(sel => {
                        if(sel.get('importe') != 0){

                            jsonData.avisos.push(sel.data);
                        }
                    });

                    Ext.getBody().mask('Afectando cartera...');
                    Ext.Ajax.request({
                        url: '/api/ingresos/certificado/afectar-cartera',
                        method: 'POST',
                        jsonData,
                        scope: this, 
                        success(response) {
                            let res = JSON.parse(response.responseText);
                            Ext.getBody().unmask();
                            if (res.exito) {
                                this.loadTable(this.certificado);
                                selectionModel.clearSelections();
                                
                                Ext.MessageBox.show({
                                    title:'Operación completada',
                                    msg: `La operación se completo correctamente y genero las siguientes transacciones: <b>${res.informacion.join(',')}</b>`,
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.Msg.INFO,
                                    fn:function(btn) {
                                        location.reload();
                                    }
                                });
                            } else {
                                Ext.Msg.alert(
                                    'Ocurrió un error',
                                    `${res.mensaje}`
                                );
                            }
                        },
                        failure(response) {
                            let res = JSON.parse(response.responseText);
                            Ext.getBody().unmask();
                            Ext.Msg.alert(
                                'Ocurrió un error',
                                `${res.mensaje}`
                            );
                        }
                    });
                }
            }
        );
    }
});