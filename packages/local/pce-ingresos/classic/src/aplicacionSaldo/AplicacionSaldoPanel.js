Ext.define('Pce.ingresos.aplicacionSaldo.AplicacionSaldoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'ingresos-aplicacion-saldo',

    requires: [
        'Pce.ingresos.aplicacionSaldo.AplicacionSaldoTabla',
        'Pce.ingresos.aplicacionSaldo.AplicacionSaldoController',
        'Pce.ingresos.certificado.CertificadoIngresosSelector'
    ],

    controller: 'aplicacion-saldo-controller',
    viewModel: {
        data: {
            dependencia: null,
            certificado: null
        },
    },

    title: 'Aplicación de saldos',
    layout: {
        type: 'fit'
    },

    items: [
        {
            xtype: 'aplicacion-saldo-tabla',
            reference: 'aplicacionSaldoTabla',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'dependencia-selector',
                    name: 'dependencia',
                    id: 'dependencia',
                    width: '50%',
                    allowBlank: false,
                    bind: {
                        selection: '{dependencia}'
                    },
                    listeners:{
                        select: 'onDependenciaSelection'
                    }
                },{
                    xtype: 'certificado-ingresos-selector',
                    name: 'certificados',
                    reference: 'certificadoSelector',
                    id: 'certificados',
                    width: '50%',
                    allowBlank: false,
                    bind: {
                        disabled: '{!dependencia}',
                        selection: '{certificado}'
                    },
                    listeners:{
                        select: 'onCertificadoSelection'
                    }
                },]
            },
        
            {
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'Concepto',
                    flex: 1.5,
                    readOnly: true,
                    bind: {
                        value: '{certificado.descripcionConcepto}'
                    }
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'Fecha',
                    flex: 1,
                    readOnly: true,
                    bind: {
                        value: '{certificado.fecha}'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Importe',
                    flex: 1,
                    readOnly: true,
                    bind: {
                        value: '{certificado.importeString}'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Saldo',
                    flex: 1,
                    readOnly: true,
                    bind: {
                        value: '{certificado.saldoString}'
                    }
                }]
            }],
            buttons: [{
                text: 'Afectar cartera',
                handler: 'onAfectarCartera',
                bind: {
                }
            }]
        }]
});