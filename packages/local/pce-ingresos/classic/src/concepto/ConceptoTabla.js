Ext.define('Pce.ingresos.concepto.ConceptoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'concepto-tabla',

    title: 'Catálogo de conceptos',

    requires: [
        'Pce.ingresos.concepto.ConceptoStore',

        'Pce.view.cartera.concepto.ConceptoTablaController'
    ],

    controller: 'concepto-tabla',
    store: {
        type: 'conceptos',
        autoLoad: true
    },

    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarConcepto'
    }],
    columns: [{
        text: 'Clave',
        dataIndex: 'clave',
        tooltip: `Valor alfanúmerico único para identificar al concepto`,
        flex: 1
    }, {
        text: 'Descripción',
        dataIndex: 'descripcion',
        tooltip:`Titulo corto del concepto.`,
        flex: 4
    }, {
        text: 'Estatus',
        dataIndex: 'estatus',
        tooltip:`Estado del concepto, es utilizado actualmente o dejo de usarse.`,
        renderer(v) {
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        },
        flex: 1
    }, {
        text: 'Clasificación',
        dataIndex: 'clasificacion',
        tooltip: `Indica si el concepto corresponde a una retención del salario 
                    del asegurado o a una aportación de la dependencia.`,
        renderer(v) {
            if (v === 'RT') {
                return 'RETENCIÓN';
            } else if (v === 'AP') {
                return 'APORTACIÓN';
            } else if(v === 'AC') {
                return 'AVISO DE CARGO';
            }
            return v;
        },
        flex: 1
    }, {
        text: 'Destino',
        dataIndex: 'destino',
        tooltip:`Corresponde si es un cargo de retención al salario de un asegurado, 
        una aportación de dependencia o ambas.`,
        renderer(v) {
            if (v === 'AMB') {
                return 'AMBAS CUENTAS';
            } else if (v === 'DEP') {
                return 'DEPENDENCIA';
            } else if (v === 'AFI') {
                return 'AFILIADO';
            }
            return v;
        },
        flex: 1
    }, {
        text: 'Creado en',
        tooltip:`Fecha en que fue creado el concepto.`,
        dataIndex: 'fechaCreacion',
        xtype: 'datecolumn',
        format: 'd/m/Y',
        flex: 1
    }, {
        text: 'Creado por',
        dataIndex: 'usuarioCreacion',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar concepto',
            handler: 'onEditarConcepto'
        }, {
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar concepto',
            handler: 'onEliminarConcepto'
        }]
    }]
});