Ext.define('Pce.ingresos.concepto.ConceptoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'concepto-forma',

    requires: [
        'Ext.form.field.Text',
        'Ext.form.field.TextArea'
    ],

    layout: 'form',
    defaultListenerScope: true,
    viewModel: {},

    items: [{
        xtype: 'textfield',
        name: 'clave',
        fieldLabel: 'Clave',
        tooltip: `Valor alfanúmerico único para identificar el concepto.`,
        allowBlank: false
    }, {
        xtype: 'textarea',
        name: 'descripcion',
        fieldLabel: 'Descripción',
        tooltip: `Titulo corto del concepto.`,
        allowBlank: false
    }, {
        xtype: 'checkbox',
        name: 'estatus',
        fieldLabel: 'Activo',
        uncheckedValue: false
    }, {
        xtype: 'combobox',
        name: 'clasificacion',
        reference: 'clasificacionSelector',
        fieldLabel: 'Clasificación',
        allowBlank: false,
        valueField: 'Clave',
        displayField: 'Texto',
        editable: false,
        store: {
            type: 'store',
            fields: ['Clave', 'Texto'],
            data: [
                {Clave: 'RT', Texto: 'Retención'},
                {Clave: 'AP', Texto: 'Aportación'},
                {Clave: 'AC', Texto: 'Aviso de cargo'}
            ]
        },
    }, {
        xtype: 'combobox',
        name: 'destino',
        reference: 'destinoSelector',
        fieldLabel: 'Destino',
        allowBlank: false,
        valueField: 'Clave',
        displayField: 'Texto',
        editable: false,
        store: {
            type: 'store',
            fields: ['Clave', 'Texto'],
            data: [
                {Clave: 'AMB', Texto: 'Ambas cuentas'},
                {Clave: 'DEP', Texto: 'Dependencia'},
                {Clave: 'AFI', Texto: 'Afiliado'}
            ]
        },
        bind: {
            disabled: '{clasificacionSelector.selection.Clave == "AC"}'
        }
    }, {
        xtype: 'checkbox',
        name: 'derivaAportacionPatronal',
        fieldLabel: 'Deriva ap. patronal',
        bind:{
            disabled: `{clasificacionSelector.selection.Clave != 'RT'}`
        }
    }, {
        xtype: 'combobox',
        name: 'conceptoOrigenId',
        reference: 'origenSelector',
        fieldLabel: 'Concepto origen',
        valueField: 'id',
        displayField: 'descripcion',
        queryMode: 'local',
        store: {
            type: 'conceptos',
            autoLoad: true,
            filters: [{
                    property: 'clasificacion',
                    value: 'RT'
                }, {
                    property: 'derivaAportacionPatronal',
                    value: true
                }]
            
        },
        bind:{
            disabled: `{clasificacionSelector.selection.Clave !== 'AP'}`
        }
    }],

    config: {
        concepto: undefined
    },

    afterRender() {
        this.callParent();
        if (this.concepto) {
            this.loadRecord(this.concepto);
        }
    },
    loadRecord(record) {
        this.callParent([record]);

        ['clave', 'clasificacion', 'destino', 'conceptoOrigenId'].forEach(name => {
            this.down(`[name="${name}"]`).setDisabled(!record.phantom);
        });
        this.down('[name="derivaAportacionPatronal"]').setHidden(!record.phantom);
    }
});