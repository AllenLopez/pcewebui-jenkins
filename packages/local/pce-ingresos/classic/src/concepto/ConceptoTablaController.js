Ext.define('Pce.view.cartera.concepto.ConceptoTablaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.concepto-tabla',
    requires: [
        'Pce.ingresos.concepto.Concepto',
        'Pce.ingresos.concepto.ConceptoForma',
        'Pce.view.dialogo.FormaContenedor'
    ],

    onEditarConcepto(grid, row, col, cmp, e, concepto) {
        this.mostrarDialogoEdicion(concepto, 'Editar');
    },
    onAgregarConcepto() {
        this.mostrarDialogoEdicion(new Pce.ingresos.concepto.Concepto({
            estatus: true
        }), 'Nuevo');
    },
    onEliminarConcepto(grid, row, col, cmp, e, concepto) {
        let store = grid.getStore();
        // let store = this.getView().getStore();
        let msg = Ext.String.format(
            '¿Seguro que deseas eliminar el concepto <i>{0}</i>?',
            concepto.get('descripcion')
        );
        Ext.Msg.confirm(
            'Eliminar',
            msg,
            function(ans) {
                if (ans === 'yes') {
                    store.remove(concepto);
                    store.sync({
                        succes() {
                            Ext.toast('El concepto se ha eliminado correctamente');
                        }
                    });
                }
            }
        );
    },

    mostrarDialogoEdicion(concepto, titulo) {
        let grid = this.view;
        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title: `${titulo} concepto`,
            items: {xtype: 'concepto-forma'},
            width: 500,
            record: concepto,
            listeners: {
                guardar(dialogo) {
                    Ext.Msg.confirm(
                        'Guardar concepto',
                        `¿Seguro que deseas guardar el concepto <i>${concepto.get('descripcion')}</i>?`,
                        ans => {
                            if(ans === 'yes') {
                                concepto.save({
                                    success() {
                                        let store = grid.getStore();
                                        if(!store.contains(concepto)){
                                            grid.getStore().add(concepto);
                                        }
                                        dialogo.close();
                                    },
                                });
                            }
                        });
                }
            }
        });
    }
});