Ext.define('Pce.ingresos.concepto.ConceptoSelectorMultiple', {
    extend: 'Ext.form.field.Tag',
    xtype: 'concepto-selector-multiple',

    requires: [
        'Pce.ingresos.concepto.ConceptoStore'
    ],

    queryMode: 'local',
    valueField: 'id',
    displayField: 'descripcion',
    // fieldLabel: 'Concepto',
    labelWidth: 70,
    forceSelection: true,
    filterPickList: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{descripcion} [{clave}]</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{descripcion} [{clave}]',
        '</tpl>'
    ],
    
    store: {
        type: 'conceptos',
        autoLoad: true,
    }
});