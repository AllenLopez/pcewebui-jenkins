/**
 * @class Pce.ingresos.quincena.GeneraQuincenaController
 * @extends Ext.app.ViewController
 * Controlador para pantalla de generación de quincenas
 */
Ext.define('Pce.ingresos.quincena.GeneraQuincenaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.genera-quincenas-panel',
  
    onGenerarQuincena() {
        let me = this;
        let store = me.lookup('quincenasTabla').getStore();
        let ejercicio = me.lookup('ejercicioField').getValue();
        
        if(store.getCount() > 0){
            Ext.toast('Ya existen quincenas para este ejercicio');
        } else {
            Ext.Msg.confirm(
                'Generar quincenas',
                `¿Seguro que deseas generar las quincenas para el ejercicio ${ejercicio}?`,
                ans => {
                    if(ans === 'yes') {
                        Ext.getBody().mask('Generando quincenas...');
                        Ext.Ajax.request({
                            url: '/api/ingresos/quincena/generar',
                            scope: this,
                            params: {
                                ejercicio: ejercicio
                            },
                            callback(op, success, response) {
                                Ext.getBody().unmask();
                                let resultado = Ext.decode(response.responseText);
                                let mensaje = `${resultado.mensaje}`;
                                if(success) {
                                    me.mostrarResultado(resultado, 'Quincenas generadas con éxito', Ext.Msg.INFO);
                                    me.onBuscarQuincenas(ejercicio);
                                } else {
                                    me.mostrarResultado(resultado, 'Error al generar quincenas: ' + mensaje, Ext.Msg.ERROR);
                                }
                            }
                        });
                    }
                }
            );
        }
    },

    mostrarResultado(resultado, mensaje, icono) {
        Ext.Msg.show({
            title: 'Proceso terminado',
            message: mensaje,
            buttons: Ext.Msg.OK,
            icon: icono,
            fn: function(btn) {
                if(btn === 'ok'){
                    console.log(resultado);
                }
            }
        });
    },

    onBuscarQuincenas() {
        let ejercicio = this.lookup('ejercicioField').getValue();
        let store = this.lookup('quincenasTabla').getStore();
        
        store.load({
            params: {
                ejercicio: ejercicio
            }
        });
    }
});