/**
 * @class Pce.ingresos.quincena.QuincenaTabla
 * @extends Ext.grid.Panel
 * @xtype quincena-tabla
 * Tabla de valores para catalogo de quincenas
 */
Ext.define('Pce.ingresos.quincena.QuincenaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'quincena-tabla',

    requires: [
        'Pce.ingresos.quincena.QuincenaStore'
    ],

    store: {
        type: 'quincena'
    },

    columns: [{
        text: 'ID',
        dataIndex: 'id',
        tooltip:'Identificador de la quincena.',
        flex: 1
    }, {
        text: 'No. Quincena',
        dataIndex: 'numQuincena',
        tooltip:'Indica el numero de la quincena en el año.',
        flex: 1
    }, {
        text: 'Descripcion',
        dataIndex: 'descripcion',
        tooltip:'Descripción de la quincena.',
        flex: 3
    }, {
        text: 'Fecha',
        dataIndex: 'fecha',
        tooltip:'Fecha quincenal.',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
        flex: 1
    }, {
        text: 'Ejercicio',
        dataIndex: 'ejercicio',
        tooltip:'Año correspondiente a la fecha de la quincena.',
        flex: 1
    }]

});