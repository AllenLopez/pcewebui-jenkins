/*!
 * @author Jorge Escamilla
 * 09/13/2018
 * Clase que extienede ComboBox. Enlista y muestra las quincenas con formato fecha corto y descripción
 * filtradas por el año en curso
 */

Ext.define('Pce.ingresos.quincena.QuincenaSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'quincena-selector',

    requires: [
        'Pce.ingresos.quincena.QuincenaStore'
    ],

    valueField: 'fecha',
    displayField: 'fecha',
    fieldLabel: 'Quincena',
    forceSelection: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
            '<tbody>',
                '<tpl for=".">',
                    '<tr role="option" class="x-boundlist-item">',
                        '<td><b>{[Ext.util.Format.date(values.fecha, "d/m/Y")]}</b> - [{descripcion}]   </td>',
                    '</tr>',
                '</tpl>',
            '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{[Ext.util.Format.date(values.fecha, "d/m/Y")]} - [{descripcion}]',
        '</tpl>'
    ],

    store: {
        type: 'quincena',
        autoLoad: true
    }
});