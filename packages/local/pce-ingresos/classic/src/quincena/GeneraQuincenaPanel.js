/**
 * @class Pce.ingresos.quincena.GeneraQuincenaPanel
 * @extends Ext.form.Panel
 * @xtype genera-quincena-panel
 * Pantalla para generar catalogo de quincenas por año
 */
Ext.define('Pce.ingresos.quincena.GeneraQuincenaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'genera-quincena-panel',

    requires: [
        'Ext.form.field.Number',

        'Pce.ingresos.quincena.GeneraQuincenaController',
        'Pce.ingresos.quincena.QuincenaTabla',
    ],

    controller: 'genera-quincenas-panel',

    title: 'Catálogo de quincenas',

    layout: 'fit',

    viewModel: {
        data: {
            ejercicio: null,
            totales: 0
        }
    },

    items: [{
        xtype: 'quincena-tabla',
        reference: 'quincenasTabla',
        emptyText: 'no existen registros',
        store: {
            type: 'quincena'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'numberfield',
                fieldLabel: 'Ejercicio',
                reference: 'ejercicioField',
                allowBlank: false,
                minValue: 1980,
                maxValue: 2100,
                bind: {
                    value: '{ejercicio}'
                }
            }, {
                xtype: 'button',
                text: 'Buscar',
                bind: {
                    disabled: '{!ejercicio}'
                },
                handler: 'onBuscarQuincenas'
            }, {
                xtype: 'button',
                text: 'Generar',
                tooltip:'Crear un nuevo ejercicio.',
                bind: {
                    disabled: '{sc}'
                },
                handler: 'onGenerarQuincena'
            }]
        }]
    }]

    //   initComponent: function (){
    //     var me = this;
    //     console.log('componente iniciado');
    //     me.callParent(arguments);
    //   }
});