/**
 * @class Pce.ingresos.nomina.NominaDinamicaPanel
 * @extends Ext.panel.Panel
 * @xtype carga-nomina-panel
 * Panel de listado de nómina dinámica que se ha cargado por proceso y agrupadas por fecha de nómina
 * y en espera de ser procesadas para afectar cartera
 */
Ext.define('Pce.ingresos.nomina.CargaNominaDinamicaPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'carga-nomina-dinamica-panel',
    
    requires: [
        'Pce.ingresos.nomina.CargaNominaDinamicaStore',
        'Pce.ingresos.nomina.CargaNominaDinamicaTabla',

        'Pce.ingresos.nomina.CargaNominaDinamicaPanelController'
    ],

    layout: 'fit',
    title: 'Carga de nómina dinámica pensionados',
    
    controller: 'carga-nomina-dinamica',
    viewModel: {
        data: {
            nominas: []
        }
    },

    tbar: [{
        text: 'Carga nómina dinámica',
        handler: 'onCargaNominaDinamica',
        bind: {
            disabled: '{!nominas.length}',
            text: 'Cargar detalles de nómina',
            tooltip:'Cargar detalles de nómina'
        }
    }, {
        text: 'Reporte de saldo de carga',
        handler: 'onMostrarReporteSaldo',
        tooltip:'Reporte de saldo de carga',
        // bind: {
        //     disabled: '{}'
        // }
    }],
    items: [{
        xtype: 'carga-nomina-dinamica-tabla',
        reference: 'cargaNominaDinamicaTabla',
        store: {
            type: 'carga-nomina-dinamica',
            autoLoad: true,
            groupField: 'fechaNomina'
        },
        listeners: {
            selectionChange: 'onNominaSeleccionCambio'
        }
    }]
});