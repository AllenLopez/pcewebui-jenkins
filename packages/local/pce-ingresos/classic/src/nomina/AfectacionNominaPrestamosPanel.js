Ext.define('Pce.ingresos.nomina.AfectacionNominaPrestamosPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'afectacion-nomina-prestamos-panel',

    requires: [
        'Pce.ingresos.nomina.AfectacionNominaPrestamosController',
        'Pce.ingresos.nomina.AfectacionNominaAdminToolbar'
    ],

    layout: 'fit',

    title: 'Afectacion Cartera Nomina Prestamos',

    controller: 'afectacion-nomina-prestamos',
    viewModel: {},

    tbar: {
        xtype: 'afectacion-nomina-admin-toolbar'
    },
    dockedItems: [{
        xtype: 'afectacion-prestamos-filtros-toolbar',
        dock: 'top',
        reference: 'toolbarFiltros',
    }],
    items: [{
        xtype: 'transaccion-tabla',
        reference: 'transaccionTabla',
        store: {
            type: 'transacciones',
            proxy: {
                type: 'rest',
                url: '/api/prestamos/nomina/carga/transaccion-carga-nomina'
            },
            autoLoad: true,
            porFechaMovimientoPrestamos(params) {
                this.load({
                    params: {
                        desde: params.desde,
                        hasta: params.hasta,
                        sistema: params.sistema
                    },
                    url: '/api/prestamos/nomina/carga/transaccion-carga-nomina'
                });
            },
        }
    }]
    
});



