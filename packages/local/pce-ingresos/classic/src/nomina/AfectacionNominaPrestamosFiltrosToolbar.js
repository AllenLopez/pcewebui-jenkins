Ext.define('Pce.ingresos.nomina.AfectacionNominaPrestamosFiltrosToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'afectacion-prestamos-filtros-toolbar',


    items: [{
        xtype: 'datefield',
        reference: 'fechaInicio',
        fieldLabel: 'Desde',
        tooltip:'Fecha inicio',
        value: new Date(new Date().getFullYear(), new Date().getMonth(),1),
        listeners: {
            change: 'onSeleccionDesde'
        }
    }, {
        xtype: 'datefield',
        reference: 'fechaFin',
        fieldLabel: 'Hasta',
        tooltip:'Fecha fin',
        value: new Date(new Date().getFullYear(), new Date().getMonth()+ 1, 0),
        listeners: {
            change: 'onSeleccionHasta'
        }
    }, {
        text: 'Buscar',
        flex: 0.5,
        handler: 'onBuscar'
    }]
});