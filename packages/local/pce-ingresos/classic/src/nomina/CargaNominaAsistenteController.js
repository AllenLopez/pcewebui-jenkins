Ext.define('Pce.ingresos.nomina.CargaNominaAsistenteController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.carga-nomina-asistente',

    onCalcularRetenciones() {
        let cargas = this.lookup('cargaNominaTabla').getSelection();
        this.iniciarCargaRetenciones(cargas);
    },

    iniciarCargaRetenciones(cargas) {
        let me = this;
        me.cargarRetenciones(cargas, {});
    },

    cargarRetenciones(cargas, parametros) {
        Ext.Msg.confirm(
            'Carga de de retenciones',
            `Se cargarán las retenciones de ${cargas.length} nominas. ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    this.ejecutarCargaRetenciones(cargas, parametros);
                }
            }
        );

    },

    ejecutarCargaRetenciones(cargas) {
        let ids = cargas.map(c => c.getId());
        let fechaNomina = cargas[0].data.fechaNomina;
        Ext.getBody().mask('Cargando retenciones...');
        Ext.Ajax.request({
            url: '/api/ingresos/nomina/carga/cargar-retenciones',
            method: 'POST',
            params: { 
                cargasIds: ids,
                fechaMovimiento: fechaNomina
            },
            callback: (opt, success, response) => {
                let transaccion = JSON.parse(response.responseText);
                if (transaccion.exito == true) {
                    Ext.Msg.alert(
                        'Operación completada',
                        `Carga de retenciones exitosa bajo la transacción: <b>${transaccion.transaccionId}</b>`
                    ); 
                    this.lookup('cargaNominaTabla').getStore().reload();
                    Ext.getBody().unmask();
                }
                else {
                    Ext.Msg.alert(
                        'Error al Completar Operación',
                        `Mensaje de error: <b>${transaccion.mensaje}</b>`
                    );
                    this.lookup('cargaNominaTabla').getStore().reload();
                    Ext.getBody().unmask();
                }
                
            }
        });
    },

    onNominaSeleccionCambio(tabla, seleccion) {
        this.getViewModel().set('nominas', seleccion);
    }
});