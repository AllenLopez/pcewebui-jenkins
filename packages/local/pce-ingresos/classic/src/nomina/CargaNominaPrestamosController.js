Ext.define('Pce.ingresos.nomina.CargaNominaPrestamosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.carga-nomina-prestamos',

    onCalcularRetenciones() {
        let cargas = this.lookup('cargaNominaTabla').getSelection();
        this.iniciarCargaRetenciones(cargas);
    },

    iniciarCargaRetenciones(cargas) {
        let me = this;
        me.cargarDetallesNomina(cargas, {});
    },

    cargarDetallesNomina(cargas, parametros) {
        Ext.Msg.confirm(
            'Carga de detalle de nómina',
            `Se cargarán los detalles de ${cargas.length} nominas. ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    this.ejecutarCargaDetalles(cargas, parametros);
                }
            }
        );

    },

    ejecutarCargaDetalles(cargas) {
        let ids = cargas.map(c => c.getId());
        let fechaNomina = cargas[0].data.fechaNomina;
        Ext.getBody().mask('Cargando retenciones...');
        
        Ext.Ajax.request({
            url: '/api/prestamos/nomina/carga',
            method: 'POST',
            params: {
                cargasIds: ids,
                fechaMovimiento: fechaNomina
            },
            callback: (opt, success, response) => {
                if (success) {
                    let transaccion = JSON.parse(response.responseText);
                    Ext.Msg.alert(
                        'Operación completada',
                        `La carga de retenciones se completó exitósamente bajo la transacción <b>${transaccion.id}</b>`
                    );
                }
                this.lookup('cargaNominaTabla').getStore().reload();
                Ext.getBody().unmask();
            }
        });
    },


    onMostrarReporteSaldoPrestamos() {
        let registro = this.lookup('cargaNominaTabla').getSelection()[0];
        let fechaNomina = Ext.util.Format.date(registro.data.fechaNomina, 'd/m/Y');
        
        if(fechaNomina !== undefined){
            window.open(`/api/prestamos/nomina/carga/reporte-saldo?fechaNomina=${fechaNomina}`); 
        }
    },

    onNominaSeleccionCambio(tabla, seleccion) {
        this.getViewModel().set('nominas', seleccion);
    }
});