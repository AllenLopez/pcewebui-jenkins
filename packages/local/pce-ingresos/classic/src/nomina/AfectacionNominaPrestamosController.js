Ext.define('Pce.ingresos.nomina.AfectacionNominaPrestamosController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.afectacion-nomina-prestamos',
    params:{
        desde: null,
        hasta: null
    },

    onAfectarCartera(cargas, parametros) {
        let registro = this.lookup('transaccionTabla').getSelection()[0];
        let transaccion = registro.getId();
        Ext.Msg.confirm(
            'Afectar Cartera',
            `Se afectará cartera con el numero de transacción <b>${transaccion}</b> ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    this.ejecutarAfectarCartera(cargas, parametros);
                }
            }
        );

    },

    ejecutarAfectarCartera(cargas) {
        let registro = this.lookup('transaccionTabla').getSelection()[0];
        let fecha = registro.data.fechaMovimiento;
        let transaccion = registro.getId();
        Ext.getBody().mask('Realizando Afectación...');
        
        Ext.Ajax.request({
            url: '/api/prestamos/nomina/carga/afectar-cartera',
            method: 'POST',
            params: {
                transaccion: transaccion
            },     
            callback: (opt, success, response) => {
                const resp = JSON.parse(response.responseText);
                if (resp.exito == true) {
                    Ext.Msg.alert(
                        'Operación completada',
                        `La afectación se completó exitósamente`
                    );
                    this.lookup('transaccionTabla').getStore().reload();
                    Ext.getBody().unmask();
                    this.onReporteAfectacionCartera();
                }
                else{
                    Ext.Msg.alert(
                        'Error en el proceso de afectación',
                        `La transacción ${transaccion} no pudo ser procesada`
                    );
                    this.mostrarResultados();
                    this.lookup('transaccionTabla').getStore().reload();
                    Ext.getBody().unmask();
                }
            }
        });
    },
    onReporteDetalleNomina(){
        let registro = this.lookup('transaccionTabla').getSelection()[0];
        let transaccion = registro.getId();
        let fecha = Ext.util.Format.date(registro.data.fechaMovimiento, 'd/m/Y');
        if(fecha !== undefined){
        window.open(`/api/prestamos/nomina/carga/reporte-detalle-nomina?fecha=${fecha}&&transaccion=${transaccion}`); 
        }
    },
    onReporteAfectacionCartera(){
        let registro = this.lookup('transaccionTabla').getSelection()[0];
        let transaccion = registro.getId();
        let fecha = Ext.util.Format.date(registro.data.fechaMovimiento, 'd/m/Y');
        if(fecha !== undefined){
        window.open(`/api/prestamos/nomina/carga/reporte-afectacion-cartera?fecha=${fecha}&&transaccion=${transaccion}`); 
        }
    },

   
    onMostrarDetalles(tabla, i, j, item, e, record) {
        this.mostrarDialogoDetallesTabla(record);
    },

    mostrarDialogoDetallesTabla(record){
        Ext.create('Ext.window.Window',{
            title: 'Detalles de transacción',
            autoShow: true,
            modal: true,
            width: 1300,
            height: 600,
            layout: 'fit',
            items: {
                xtype: 'detalle-dep-transaccion-tabla',
                transaccion: record,
            },
            listeners: {
                beforeClose(panel) {
                    panel.down('detalle-dep-transaccion-tabla').getStore().removeAll();
                }
            }
        });
    },
    onMostrarResultados() {
        this.mostrarResultados();
    },
    mostrarResultados(){
        Ext.create({
            xtype: 'transaccion-resultado-dialogo',
            transaccion: this.getTransaccion(),
            autoShow: true,
            modal: true
        });
    },

    getTransaccion() {
        return this.lookup('transaccionTabla').getSelection()[0];
    },
    onSeleccionDesde(comp, valor){
        if( !comp.validate() || !valor ){
            this.params.desde = null;
            return;
        }
        this.params.desde = valor;
        this.params.hasta = this.lookup('fechaFin').value;
        this.validarFechas();
    },

    onSeleccionHasta(comp, valor){
        if( !comp.validate() || !valor ){
            this.params.hasta = null;
            return;
        }
        this.params.hasta = valor;
        this.params.desde = this.lookup('fechaInicio').value;
        this.validarFechas();
    },

    validarFechas(){
        if( !this.params.hasta || !this.params.desde){
            return;
        }

        if(this.params.desde > this.params.hasta){
            Ext.Msg.alert('Advertencia!', 'No es un rango de fechas valido');
            return;
        }
        this.params.desde = this.formatDate(this.params.desde, 'd/m/Y');
        this.params.hasta = this.formatDate(this.params.hasta, 'd/m/Y');

        this.lookup('transaccionTabla').getStore().porFechaMovimientoPrestamos(this.params);
    },
    formatDate(date, format){
        return  Ext.util.Format.date(date, format);
    },
    onBuscar(){
        this.params.desde = this.lookup('fechaInicio').value;
        this.params.hasta = this.lookup('fechaFin').value;
        this.params.desde = this.formatDate(this.params.desde, 'd/m/Y');
        this.params.hasta = this.formatDate(this.params.hasta, 'd/m/Y');
        this.lookup('transaccionTabla').getStore().porFechaMovimientoPrestamos(this.params);
    },

});