/**
 * @class Pce.ingresos.nomina.CargaNominaDinamicaTabla
 * @extends Ext.grid.Panel
 * @xtype carga-nomina-dinamica-tabla
 * Tabla de listado de nóminas dinámicas cargadas a DB y dispuestas para procesamiento
 */
Ext.define('Pce.ingresos.nomina.CargaNominaDinamicaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'carga-nomina-dinamica-tabla',
  
    requires: [
        'Ext.grid.feature.Grouping',

        'Pce.ingresos.nomina.CargaNominaDinamicaPanelController'
    ],

    controller: 'carga-nomina-dinamica',

    columns: [{
        text: 'Fecha nómina',
        dataIndex: 'fechaNomina',
        tooltip: 'Fecha nómina',
        renderer: v => Ext.Date.format(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Usuario de carga',
        dataIndex: 'usuarioCarga',
        tooltip: 'Usuario que realiza la carga',
        flex: 2
    }, {
        text: 'Fecha de carga',
        dataIndex: 'fechaCarga',
        tooltip: 'Fecha de carga',
        renderer: v => Ext.Date.format(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Registros',
        tooltip: 'Registros',
        dataIndex: 'detallesTotal',
        flex: 1
    },{
        xtype: 'actioncolumn',
        width: 80,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'ver registros',
            handler: 'onMostrarDetallesNominaDinamica'
        }]
    }]
});