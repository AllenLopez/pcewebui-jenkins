Ext.define('Pce.ingresos.nomina.CargaNominaAsistente', {
    extend: 'Ext.panel.Panel',
    xtype: 'carga-nomina-asistente',

    requires: [
        'Pce.ingresos.nomina.CargaNominaAsistenteController',

        'Pce.ingresos.nomina.CargaNominaTabla',
        'Pce.ingresos.nomina.CargaNominaStore'
    ],

    layout: 'fit',
    title: 'Carga de retenciones',
    controller: 'carga-nomina-asistente',
    viewModel: {
        data: {
            nominas: []
        }
    },
    tbar: [{
        text: 'Calcular retenciones',
        handler: 'onCalcularRetenciones',   
         bind: {
            disabled: '{!nominas.length}',
            text: 'Cargar retenciones de {nominas.length} nomina(s)',
            tooltip:'Carga de retenciones de las dependencias seleccionadas'
        }
    }],
    items: [{
        xtype: 'carga-nomina-tabla',
        reference: 'cargaNominaTabla',
        store: {
            type: 'carga-nomina',
            autoLoad: true,
            sorters: 'dependenciaDescripcion',
            groupField: 'fechaNomina'
        },
        listeners: {
            selectionchange: 'onNominaSeleccionCambio'
        }
    }]
});