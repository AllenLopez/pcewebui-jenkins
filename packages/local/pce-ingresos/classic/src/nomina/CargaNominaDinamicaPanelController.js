/**
 * @class Pce.ingresos.nomina.CargaNominaDinamicaPanelController
 * @extends Ext.app.ViewController
 * @alias controller.carga-nomina-dinamica
 * Controllador para panel de carga de nómina dinámica
 */
Ext.define('Pce.ingresos.nomina.CargaNominaDinamicaPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.carga-nomina-dinamica',

    requres: [
        'Pce.ingresos.nomina.NominaDinamicaDetallesTabla'
    ],
    
    onCargaNominaDinamica() {
        let registro = this.onSeleccionaRegistro();
        this.cargaNominaSeleccionada(registro);
    },

    cargaNominaSeleccionada(registro) {
        let fechaNomina = Ext.util.Format.date(registro.data.fechaNomina, 'd/m/Y');
        Ext.Msg.confirm(
            'Carga de registro de nómina dinámica',
            `Se cargarán los detalles de la nómina del ${fechaNomina}. ¿Deseas proceder?`,
            ans => {
                if(ans === 'yes'){
                    this.ejecutaCargaNominaDinamica(registro);
                }
            }
        );
    },

    ejecutaCargaNominaDinamica(registro){
        let me = this;
        if (registro.isModel) {
            let id = registro.id;
            let fechaNomina = registro.data.fechaNomina;

            Ext.getBody().mask('Cargando detalles...');
            Ext.Ajax.request({
                url: '/api/ingresos/nomina-dinamica/carga/cargar-detalles',
                method: 'POST',
                params: {
                    fechaNomina: fechaNomina,
                    registroId: id
                },
                callback(opt, success, response) {
                    if(success){
                        let result = JSON.parse(response.responseText);
                        let transaccion = result.transaccionId;
                        console.log(result);
                        if(result.exito){
                            Ext.Msg.alert(
                                'Operación completada',
                                `Carga de detalles exitosa en transaccion: <b>${transaccion}</b>`
                            ).setWidth(350);
                        }
                    } else {
                        let result = JSON.parse(response.responseText);
                        console.log(result);
                        Ext.Msg.alert(
                            'Ocurrió un error al iniciar la operación',
                            `${response.statusText}`
                        );
                    }
                    me.lookup('cargaNominaDinamicaTabla').getStore().reload();
                    Ext.getBody().unmask();
                }
            });
        } else {
            Ext.Msg.alert(
                'seleccion incorrecta',
                'El registro seleccionado no puede ser procesado ya que no pertenece a un registro de tipo Nómina dinámica'
            );
        }
    },

    onMostrarDetallesNominaDinamica(view, row, col, item, e, record) {
        Ext.Ajax.request({
            url: '/api/ingresos/nomina-dinamica/carga/detalles',
            method: 'GET',
            params: {
                nominaMstId: record.id
            },
            callback(op, success, response) {
                if(success){
                    let resultado = JSON.parse(response.responseText);
                    console.log(resultado);
                    Ext.create('Ext.window.Window', {
                        autoShow: true,
                        title: 'Detalles de nómina dinámica',
                        modal: true,
                        width: 1000,
                        height: 500,
                        layout: 'fit',
                        items: {
                            xtype: 'nomina-dinamica-detalles-tabla',
                            detalles: resultado
                        }
                    });
                }
            }
        });

    },

    onNominaSeleccionCambio(tabla, seleccion) {
        this.getViewModel().set('nominas', seleccion);
    },

    onMostrarReporteSaldo() {
        let registro = this.lookup('cargaNominaDinamicaTabla').getSelection()[0];
        let fechaNomina = Ext.util.Format.date(registro.data.fechaNomina, 'd/m/Y');
        
        if(fechaNomina !== undefined){
            window.open(`/api/ingresos/nomina-dinamica/carga/reporte-saldo?fechaNomina=${fechaNomina}`); 
        }
    },

    onSeleccionaRegistro() {
        return this.lookup('cargaNominaDinamicaTabla').getSelection()[0];
    }
    
});