Ext.define('Pce.ingresos.nomina.CargaNominaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'carga-nomina-tabla',

    requires: [
        'Ext.grid.feature.Grouping'
    ],

    features: [{
        ftype:'grouping',
        startCollapsed: true
    }],

    selModel: {
        selType: 'checkboxmodel'
    },

    columns: [{
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        tooltip: 'Dependencia',
        flex: 2
    }, {
        text: 'Fecha nómina',
        dataIndex: 'fechaNomina',
        tooltip: 'Fecha nómina',
        renderer: v => Ext.Date.format(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Fecha carga',
        dataIndex: 'fechaCarga',
        tooltip: 'Fecha carga',
        renderer: v => Ext.Date.format(v, 'd/m/Y H:i:s'),
        flex: 1
    }, {
        text: 'Total de registros',
        dataIndex: 'registrosTotal',
        tooltip: 'Total de registros',
        flex: 1
    }]
});