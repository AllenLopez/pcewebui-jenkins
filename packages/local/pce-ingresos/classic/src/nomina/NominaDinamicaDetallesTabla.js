/**
 * @class Pce.ingresos.nomina.NominaDinamicaDetallesTabla
 * @extends Ext.grid.Panel
 * @xtype detalles-nomina-dinamica-tabla
 * description
 */
Ext.define('Pce.ingresos.nomina.NominaDinamicaDetallesTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'nomina-dinamica-detalles-tabla',

    requires: [
        'Pce.ingresos.nomina.NominaDinamicaDetalleStore'
    ],

    store: {
        type: 'nomina-dinamica-detalle-store'
    },

    emtpyText: 'No existen registros que mostrar',

    columns:[{
        text: 'Id dependencia',
        dataIndex: 'dependenciaId',
        flex: 1
    }, {
        text: 'Descripción',
        dataIndex: 'dependenciaDescripcion',
        flex: 3
    }, {
        text: 'Id nómina dinámica',
        dataIndex: 'nominaDinamicaMstId',
        flex: 1
    }, {
        text: 'Fecha nómina',
        dataIndex: 'fechaNomina',
        renderer (v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        },
        flex: 1
    }],

    setDetalles(detalles){
        this.detalles = detalles;
        this.store.setData(detalles);
    },

    afterRender() {
        this.callParent();
        if(this.detalles){
            this.setDetalles(this.detalles);
        }
    }
});