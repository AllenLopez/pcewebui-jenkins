Ext.define('Pce.ingresos.nomina.CargaNominaPrestamosPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'carga-nomina-prestamos-panel',

    requires: [
        'Pce.ingresos.nomina.CargaNominaPrestamosController',
        'Pce.ingresos.nomina.CargaNominaPrestamosTabla',
        'Pce.ingresos.nomina.NominaPrestamosStore'
    ],

    layout: 'fit',
    title: 'Carga Nomina Prestamos',
    controller: 'carga-nomina-prestamos',
    viewModel: {
        data: {
            nominas: []
        }
    },
    tbar: [{
        text: 'Cargar detalle nómina',
        handler: 'onCalcularRetenciones',
         bind: {
            disabled: '{!nominas.length}',
            text: 'Cargar detalle de {nominas.length} nómina(s)',
            tooltip:'Carga de detalle de nómina de las dependencias seleccionadas'
        }

    }, {
        text: 'Reporte de saldo de carga',
        handler: 'onMostrarReporteSaldoPrestamos',
        tooltip:'Reporte de saldo de carga',
        bind: {
            disabled: '{!nominas.length}',
        }
    }],
    items: [{
                
        xtype: 'carga-nomina-prestamos-tabla',
        reference: 'cargaNominaTabla',
        store: {
            type: 'nominaPrestamos',
            autoLoad: true,
            sorters: 'dependenciaDescripcion',
            groupField: 'fechaNomina'
        },
        listeners: {
            selectionchange: 'onNominaSeleccionCambio'
        }
    }]
});



