Ext.define('Pce.ingresos.nomina.AfectacionNominaAdminToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'afectacion-nomina-admin-toolbar',

   
    items: [{
        text: 'Afectar cartera',
        handler: 'onAfectarCartera',
        hidden: true,
        bind: {
            hidden: '{!(transaccionTabla.selection && transaccionTabla.selection.etapa == "CT")}'
        }
    }, {
        text: 'Reporte detalle nómina',
        iconCls: 'x-fa fa-file-pdf-o',
        handler: 'onReporteDetalleNomina',
        tooltip:'Generar reporte de detalle de nomina',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    }, {
        text: 'Reporte de afectación de cartera',
        handler: 'onReporteAfectacionCartera',
        tooltip:'Generar reporte de afectación a cartera',
        iconCls: 'x-fa fa-file-pdf-o',
        hidden: true,
        bind: {
            hidden: '{!(transaccionTabla.selection && transaccionTabla.selection.etapa == "AU")}'
        }
    }, {
        text: 'Mostrar errores de la transacción',
        handler: 'onMostrarResultados',
        tooltip:'Mostrar errores generados en la transacción',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    }]

});