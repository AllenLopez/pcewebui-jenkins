/**
 * @class Pce.ingresos.Inicio
 * @extends Ext.container.Container
 * @xtype inicio
 * Página de inicio de módulo Ingresos
 */
Ext.define('Pce.ingresos.Inicio', {
    extend: 'Ext.Component',
    xtype: 'inicio',
    
    margin: 10,
    html: `<b>Bienvenido al Sistema de Ingresos de Pensiones Civiles del Estado de Chihuahua</b>
        <p>El objetivo del Sistema de Ingresos es administrar la cartera de Dependencias afiliadas y asegurados en base a las retenciones de seguridad social y préstamos de los empleados así como las aportaciones patronales.</p>`,

  
});