﻿/**
 * @class Pce.ingresos.poliza.GeneraPolizaController
 * @extends Ext.app.ViewController
 * @alias controller.genera-poliza-panel
 * ejecuta procedimiento para generar póliza según el registro de transacción seleccionado
 */
Ext.define('Pce.ingresos.poliza.GeneraPolizaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.genera-poliza-panel',

    requires: [
        'Pce.ingresos.poliza.PolizaTabla',
        'Pce.ingresos.poliza.PolizaDialogo',
        'Pce.ingresos.transaccion.Transaccion'
    ],

    polizas: undefined,
    transaccion: undefined,
    tipoMovimiento: undefined,
    fechaInicio: undefined,
    fechaFin: undefined,
    tabla: undefined,

    onCrearPoliza() {
        const me = this;
        const tabla = this.getTablaTransacciones();
        const seleccion = tabla.getSelection();
        if (seleccion.length === 0) {
            Ext.Msg.alert('Advertencia', 'No se ha seleccionado ningun registro');
            return;
        }
        const vm = this.getViewModel().getData();
        const tipoMov = vm.movimientoContable;
        const instrucciones = this.creaObjetoInstruccion(seleccion, tipoMov);

        Ext.Msg.confirm(
            'Generación de póliza',
            `Se efectuará la generación de póliza para esta transacción, ¿Desea proceder?`,
            ans => {
                if (ans === 'yes') {
                    console.log(instrucciones);
                    me.onGenerarPoliza(instrucciones, tipoMov);
                }
            }
        );
    },

    cargaTablaTransacciones(tipoId, fechaInicio, fechaFin) {
        const me = this;
        this.getTablaTransacciones().getStore().load({
            params: {
                tipoId: tipoId,
                fechaInicial: fechaInicio,
                fechaFinal: fechaFin
            },
            success() {
                me.onSeleccionarTodo();
            }
        });
    },

    onGenerarPoliza(instrucciones, tipoMov) {
        let me = this;
        let tipoMovimientoFiltro = me.lookup('tipoMovimiento');
        Ext.getBody().mask('Generando pólizas...');

        Ext.Ajax.request({
            url: '/api/ingresos/poliza/crear',
            method: 'POST',
            jsonData: Ext.JSON.encode(instrucciones),
            success(response) {
                Ext.getBody().unmask();
                const resultado = JSON.parse(response.responseText);
                let mensaje = `${resultado.mensaje}`;
                let icono = Ext.Msg.ERROR;

                if (resultado.exito) {
                    mensaje = `Se generaron exitosamente ${resultado.totalPolizas} pólizas. Por favor verifique el detalle de cada póliza`;
                    icono = Ext.Msg.INFO;
                    Ext.Msg.alert('Proceso terminado',mensaje);
                } else {
                    let transaccion = new Pce.ingresos.transaccion.Transaccion(resultado.transaccionId);
                    me.mostrarRespuesta(transaccion);
                }
                me.onQuitarTodo();
                tipoMovimientoFiltro.reset();
                me.onSeleccionaTipoMovimiento(null, tipoMov.tipoMovimientoId);
            }
        });
    },

    onMostrarDetallesPolizas(tabla, i, j, item, e, transaccion) {
        Ext.create('Pce.ingresos.poliza.PolizaDialogo', {
            transaccionId: transaccion.getId(),
        });
    },

    onVerDetalles(tabla, i, j, item, e, poliza) {
        this.mostrarDialogoTabla(poliza);
    },

    onSeleccionarTodo(){
        const tabla = this.getTablaTransacciones();
        tabla.getSelectionModel().selectAll();
    },

    onQuitarTodo(){
        const tabla = this.getTablaTransacciones();
        tabla.getSelectionModel().deselectAll();
    },


    onImprimirPoliza(tabla, i, j, item, e, poliza) {
        let params = `?Pidpoliza=${poliza.getId()}&serie=${poliza.get('seriePoliza')}&tipo=${poliza.get('tipoPoliza')}&folio=${poliza.get('folio')}`;

        window.open(`/api/ingresos/poliza/descargar${params}`);
    },

    mostrarDialogoTabla(poliza) {
        Ext.create({
            xtype: 'window',
            title: `Detalle de póliza - ${poliza.get('descripcion')}`,
            autoShow: true,
            modal: true,
            height: '90vh',
            width: '100vw',
            layout: 'fit',
            items: {
                xtype: 'poliza-detalle-tabla',
                store: {
                    type: 'poliza-detalle',
                    autoLoad: {
                        params: {
                            polizaId: poliza.getId()
                        }
                    }
                }
            }
        });
    },
    mostrarRespuesta(transaccion) {
        Ext.Msg.alert(
            'Proceso terminado',
                `Ocurrio un error al intentar crear la póliza, en la transacción: <b>${transaccion.data}</b>`,
            () => this.mostrarResultadoPoliza(transaccion)
        );
    },

    mostrarResultadoPoliza(transaccion) {
        Ext.create({
            xtype: 'poliza-resultado-dialogo',
            transaccion: transaccion,
            autoShow: true,
            modal: true
        });
    },

    onBuscarTransacciones() {
        this.cargaTablaTransacciones(this.tipoMovimiento, this.fechaInicio, this.fechaFin);
    },

    onSeleccionaTipoMovimiento(selector, tipoId) {
        this.tipoMovimiento = tipoId;
        this.cargaTablaTransacciones(tipoId, this.fechaInicio, this.fechaFin);
    },

    onSeleccionaFechaInicial(control, seleccion) {
        this.fechaInicio = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    onSeleccionaFechaFinal(control, seleccion) {
        this.fechaFin = Ext.util.Format.date(seleccion, 'd/m/Y');
    },

    creaObjetoInstruccion(seleccion, tipoMov) {
        let datos = [];
        seleccion.forEach(el => {
            let instruccion = {};
            instruccion.transaccionId = el.getId();
            instruccion.usuario = el.get('usuarioCreacion');
            instruccion.origen = tipoMov.get('origenMovimiento');
            instruccion.tipoAfectacion = tipoMov.get('tipoAfectacion');
            instruccion.fechaPoliza = el.get('fechaPoliza');
            datos.push(instruccion);
        });

        return datos;
    },

    getTablaTransacciones() {
        const tabla = this.lookup('transaccionTabla');
        if (tabla) { return tabla; }
    }
});