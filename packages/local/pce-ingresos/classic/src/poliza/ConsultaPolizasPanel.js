let meses = Ext.create('Ext.data.Store', {
    fields: ['nombre', 'mes'],
    data: [
        { 'nombre': 'Enero', 'mes': 1 },
        { 'nombre': 'Febrero', 'mes': 2 },
        { 'nombre': 'Marzo', 'mes': 3 },
        { 'nombre': 'Abril', 'mes': 4 },
        { 'nombre': 'Mayo', 'mes': 5 },
        { 'nombre': 'Junio', 'mes': 6 },
        { 'nombre': 'Julio', 'mes': 7 },
        { 'nombre': 'Agosto', 'mes': 8 },
        { 'nombre': 'Septiembre', 'mes': 9 },
        { 'nombre': 'Octubre', 'mes': 10 },
        { 'nombre': 'Noviembre', 'mes': 11 },
        { 'nombre': 'Diciembre', 'mes': 12 },
    ]
});

function exercices() {
    let val = [];
    let year = (new Date()).getFullYear();
    for (let i = 1969; i <= year; i++) {
        val.push({ 'ejercicio': i });
    }
    return val;
}

let ejercicios = Ext.create('Ext.data.Store', {
    fields: ['ejercicio'],
    data: exercices(),
    sorters: [{
        property: 'ejercicio',
        direction: 'DESC'
    }]
});

Ext.define('Pce.ingresos.poliza.ConsultaPolizaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'consulta-poliza-panel',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',

        'Pce.ingresos.poliza.PolizaSelector',
        'Pce.ingresos.poliza.PolizaStore',
        'Pce.ingresos.poliza.PolizaDetalleTabla',
        'Pce.ingresos.poliza.TipoPolizaStore',
        'Pce.ingresos.poliza.SeriePolizaStore',
        'Pce.ingresos.quincena.QuincenaStore'
    ],

    controller: 'consulta-poliza-controller',
    viewModel: {
        data: {
            folio: null,
            poliza: null,
            serie: null,
            tipo: null,
            fechaInicio: null,
            fechaFinal: null,
        }
    },
    title: 'Consulta de póliza',
    layout: 'fit',

    items: [{
        xtype: 'poliza-tabla',
        reference: 'poliza-tabla',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                name: 'desde',
                fieldLabel: 'A partir de',
                labelWidth: 80,
                flex: 1,
                listeners: {
                    change: 'onSeleccionDesde'
                }
            }, {
                xtype: 'datefield',
                name: 'hasta',
                fieldLabel: 'Hasta',
                labelWidth: 80,
                flex: 1,
                listeners: {
                    change: 'onSeleccionHasta'
                }
            }, {
                xtype: 'numberfield',
                fieldLabel: 'Transacción',
                valueField: 'transaccion',
                // reference: 'polizaSelector',
                name: 'transaccion',
                labelWidth: 100,
                // allowBlank: false,
                // emptyText: 'No existen folios',
                flex: 1,
                listeners: {
                    change: 'onChangeTransaccion'
                }
            }, {
                text: 'Buscar',
                flex: 0.5,
                bind: {
                    // disabled: '{!folio}'
                },
                handler: 'onBuscarPoliza'
            }]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'combobox',
                fieldName: 'Tipo',
                flex: 1,
                fieldLabel: 'Tipo póliza',
                labelWidth: 80,
                reference: 'tipo',
                // allowBlank: false,
                // forceSelection: true,
                valueField: 'tipo',
                displayField: 'nombre',
                queryMode: 'local',
                value: 'D',
                store: {
                    type: 'tipos-poliza',
                    autoLoad: true,
                    filters: [{
                        property: 'estatus',
                        value: true
                    }],
                    listeners: {
                        load: (store, records, success) =>{
                            store.insert(0,{
                                estatus: true,
                                nombre: "TODOS",
                                tipo: null});
                        }
                    }
                },
                flex: 1,
                listeners: {
                    change: 'onSeleccionTipoPoliza'
                }
            }, {
                xtype: 'combobox',
                fieldName: 'Serie',
                fieldLabel: 'Serie póliza',
                reference: 'serie',
                // allowBlank: false,
                // forceSelection: true,
                flex: 1,
                labelWidth: 80,
                valueField: 'serie',
                value: 'I',
                displayField: 'nombre',
                queryMode: 'local',
                store: {
                    type: 'series-poliza',
                    autoLoad: true,
                    filters: [{
                        property: 'estatus',
                        value: true
                    }],
                    listeners: {
                        load: (store, records, success) =>{
                            store.insert(0,{
                                estatus: true,
                                nombre: "TODOS",
                                tipo: undefined});
                        }
                    }
                },
                flex: 1,
                listeners: {
                    change: 'onSeleccionSeriePoliza'
                }
            }, {
                xtype: 'numberfield',
                fieldLabel: 'Folio',
                valueField: 'folio',
                // reference: 'polizaSelector',
                name: 'folio',
                labelWidth: 100,
                // allowBlank: false,
                // emptyText: 'No existen folios',
                flex: 1,
                listeners: {
                    change: 'onChangeFolio'
                }
            }, {
                text: 'Imprimir Pólizas',
                flex: 0.5,
                id: 'btnImprimirPolizas',
                disabled: true,
                handler: 'onImprimirPolizas'
            }]
        }]
    }],
    afterRender() {
        this.callParent();
        this.inicializarFecha();
    },

    inicializarFecha() {
        let fecha = new Date();
        var firstDay = new Date(fecha.getFullYear(), fecha.getMonth(), 1);
        var lastDay = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
        let campo = Ext.ComponentQuery.query('[name=desde]')[0];
        let hasta = Ext.ComponentQuery.query('[name=hasta]')[0];

        campo.setValue(firstDay);
        
        hasta.setValue(lastDay);
    },
    // buttons: [{
    //     text: 'Imprimir',
    //     bind: {
    //         disabled: '{!folio}'
    //     },
    //     handler: 'onImprimirPoliza'
    // }]
});