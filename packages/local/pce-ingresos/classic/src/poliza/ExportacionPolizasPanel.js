Ext.define('Pce.ingresos.poliza.ExportacionPolizaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'exportacion-poliza-panel',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',

        'Pce.ingresos.poliza.PolizaSelector',
        'Pce.ingresos.poliza.PolizaStore',
        'Pce.ingresos.poliza.PolizaDetalleTabla',
        'Pce.ingresos.poliza.TipoPolizaStore',
        'Pce.ingresos.poliza.SeriePolizaStore',
        'Pce.ingresos.quincena.QuincenaStore'
    ],

    controller: 'exportacion-poliza-controller',
    viewModel: {
        data: {
            folio: null,
            poliza: null
        }
    },
    title: 'Exportación de póliza',
    layout: 'fit',

    items: [{
        xtype: 'poliza-tabla',
        reference: 'poliza-tabla',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                name: 'desde',
                fieldLabel: 'A partir de',
                labelWidth: 80,
                flex: 1,
                listeners: {
                    change: 'onSeleccionDesde'
                }
            }, {
                xtype: 'datefield',
                name: 'hasta',
                fieldLabel: 'Hasta',
                labelWidth: 80,
                flex: 1,
                listeners: {
                    change: 'onSeleccionHasta'
                }
            }, {
                xtype: 'combobox',
                fieldName: 'Tipo',
                flex: 1,
                fieldLabel: 'Tipo póliza',
                labelWidth: 80,
                // allowBlank: false,
                // forceSelection: true,
                valueField: 'tipo',
                displayField: 'nombre',
                queryMode: 'local',
                value: 'D',
                store: {
                    type: 'tipos-poliza',
                    autoLoad: true,
                    filters: [{
                        property: 'estatus',
                        value: true
                    }],
                    listeners: {
                        load: (store, records, success) =>{
                            store.insert(0,{
                                estatus: true,
                                nombre: "TODOS",
                                tipo: null});
                        }
                    }
                },
                flex: 1,
                listeners: {
                    change: 'onSeleccionTipoPoliza'
                }
            }, {
                xtype: 'combobox',
                fieldName: 'Serie',
                fieldLabel: 'Serie póliza',
                // allowBlank: false,
                // forceSelection: true,
                flex: 1,
                labelWidth: 80,
                valueField: 'serie',
                value: 'I',
                displayField: 'nombre',
                queryMode: 'local',
                store: {
                    type: 'series-poliza',
                    autoLoad: true,
                    filters: [{
                        property: 'estatus',
                        value: true
                    }],
                    listeners: {
                        load: (store, records, success) =>{
                            store.insert(0,{
                                estatus: true,
                                nombre: "TODOS",
                                tipo: undefined});
                        }
                    }
                },
                flex: 1,
                listeners: {
                    change: 'onSeleccionSeriePoliza'
                }
            },{
                text: 'Buscar',
                flex: 0.5,
                bind: {
                    // disabled: '{!folio}'
                },
                handler: 'onBuscarPoliza'
            }, {
                text: 'Generar Archivo Exportar',
                flex: 0.5,
                bind: {
                    // disabled: '{!folio}'
                },
                handler: 'onGenerarArchivo'
            }, {
                text: 'Imprimir',
                flex: 0.5,
                bind: {
                    // disabled: '{!folio}'
                },
                handler: 'onImprimirPolizas'
            }]
        }]
    }],
    afterRender() {
        this.callParent();
        this.inicializarFecha();
    },

    inicializarFecha() {
        let fecha = new Date();
        var firstDay = new Date(fecha.getFullYear(), fecha.getMonth(), 1);
        var lastDay = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
        let campo = Ext.ComponentQuery.query('[name=desde]')[0];
        let hasta = Ext.ComponentQuery.query('[name=hasta]')[0];

        campo.setValue(firstDay);
        
        hasta.setValue(lastDay);
    },
    // buttons: [{
    //     text: 'Imprimir',
    //     bind: {
    //         disabled: '{!folio}'
    //     },
    //     handler: 'onImprimirPoliza'
    // }]
});

/*

*/