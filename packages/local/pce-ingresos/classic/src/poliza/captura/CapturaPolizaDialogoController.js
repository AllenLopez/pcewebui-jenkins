/**
 * @class Pce.ingresos.poliza.captura.CapturaPolizaDialogoController
 * @extends Ext.app.ViewController
 * @alias captura-poliza-dialogo-forma
 * Controlador para pantalla de captura poliza dialogo en donde se ejecutan validaciones y obtiene datos de formulario para procesar su inserción o actualización
 */
Ext.define('Pce.ingresos.poliza.captura.CapturaPolizaDialogoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.captura-poliza-dialogo-forma',

    onAceptar() {
        Ext.Msg.alert('Captura poliza', 'I rule this world');
    },

    onCancelar() {
        this.close();
    }
});