/**
 * @class Pce.ingresos.poliza.CuentaPolizaForma
 * @extends Ext.grid.Panel
 * @xtype cuentaa-poliza-forma
 * Pantalla que filtra y enlista pólizas manuales o capturadas sin pertenecer a una transacción concreta
 */
Ext.define('Pce.ingresos.poliza.captura.CuentaPolizaDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'dialogo-cuenta-poliza',
    
    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',

        'Pce.ingresos.poliza.CuentaPoliza',
        'Pce.ingresos.poliza.captura.CuentaPolizaSelector'
    ],

    autoShow: true,
    modal: true,
    width: 600,
    height: 360,
    title: 'Cuentas para póliza',

    defaultListenerScope: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    viewModel: {
        data: {
            cuentaDetalle: null
        }
    },

    polizaDetalle: null,

    items: [{
        xtype: 'form',
        bodyPadding: 10,
        layout: 'form',
        items: [{
            xtype: 'cuenta-poliza-selector',
            reference: 'cuentaSelector',
            bind: {
                selection: '{cuentaDetalle}'
            }
        }, {
            xtype: 'displayfield',
            fieldLabel: 'SubCuenta01',
            readOnly: true,
            bind: {
                value: '{cuentaSelector.selection.subCuenta01}'
            },
            value: '',
            renderer(v) {
                return `<b>${v}</b>`;
            }
        }, {
            xtype: 'displayfield',
            fieldLabel: 'SubCuenta02',
            readOnly: true,
            bind: {
                value: '{cuentaSelector.selection.subCuenta02}'
            },
            value: '',
            renderer(v) {
                return `<b>${v}</b>`;
            }
        }, {
            xtype: 'displayfield',
            fieldLabel: 'SubCuenta03',
            readOnly: true,
            bind: {
                value: '{cuentaSelector.selection.subCuenta03}'
            },
            value: '',
            renderer(v) {
                return `<b>${v}</b>`;
            }
        }, {
            xtype: 'displayfield',
            fieldLabel: 'SubCuenta04',
            readOnly: true,
            bind: {
                value: '{cuentaSelector.selection.subCuenta04}'
            },
            value: '',
            renderer(v) {
                return `<b>${v}</b>`;
            }
        }],

        buttons: [{
            text: 'Aceptar',
            handler: 'onAceptarTap'
        }, {
            text: 'Cancelar',
            handler: 'onCancelarTap'
        }],
    }],

    onAceptarTap() {
        let form = this.down('form');
        if(form.isValid()) {
            let cuentaDet = this.viewModel.get('cuentaDetalle');
            if (!cuentaDet) {
                Ext.Msg.alert('Error de seleccion', 'Ocurrió un error al seleccionar la cuenta');
                return;
            }
            this.polizaDetalle.set(cuentaDet.data);
            this.close();
        }
    },

    onCancelarTap() {
        this.close();
    },
});