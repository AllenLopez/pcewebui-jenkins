/**
 * @class Pce.ingresos.poliza.PolizaCuentaTabla
 * @extends Ext.grid.Panel
 * @xtype poliza-cuenta-tabla
 * Tabla que enlista los detalles y cuentas de la póliza maestra que se captura manualmente
 */
Ext.define('Pce.ingresos.poliza.captura.PolizaCuentaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'poliza-cuenta-tabla',
    
    requires: [
        'Pce.ingresos.poliza.PolizaDetalleStore',

        'Ext.plugin.Abstract',
        'Ext.grid.plugin.CellEditing'
    ],

    store: {
        type: 'poliza-detalle'
    },

    layout: 'fit',

    features: [{
        ftype: 'summary'
    }],

    plugins: [{
        ptype: 'cellediting',
        reference: 'captura-editor',
        clicksToEdit: 1
    }],

    listeners: [{
        beforeedit: 'onBeforeEdit',
        validateedit: 'onValidateEdit',
        edit: 'onEditImporte'
    }],

    columns: [{
        text: 'Cuenta',
        dataIndex: 'cuenta',
        flex: 2,
        
    },{
        text: 'Nombre cuenta',
        dataIndex: 'descripcion',
        flex: 2,
        
    }, {
        text: 'Subcuenta 1',
        dataIndex: 'subCuenta01',
        flex: 1,
    }, {
        text: 'Subcuenta 2',
        dataIndex: 'subCuenta02',
        flex: 1,
    }, {
        text: 'Subcuenta 3',
        dataIndex: 'subCuenta03',
        flex: 1,
    }, {
        text: 'Subcuenta 4',
        dataIndex: 'subCuenta04',
        flex: 1,
    }, {
        text: 'Debe',
        dataIndex: 'debe',
        reference: 'editorDebe',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        summaryType: 'sum',
        summaryRenderer: 'onSummary',
        editor: {
            xtype: 'numberfield',
            hideTrigger: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false
        },
        flex: 2,
    }, {
        text: 'Haber',
        dataIndex: 'haber',
        reference: 'editorHaber',
        renderer(v){
            return v === 0 ? '' : Ext.util.Format.usMoney(v);
        },
        summaryType: 'sum',
        summaryRenderer: 'onSummary',
        editor: {
            xtype: 'numberfield',
            hideTrigger: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false
        },
        flex: 2,
    }],
    
});