/**
 * @class Pce.ingreos.poliza.CuentaPolizaSelector
 * @extends Ext.form.field.ComboBox
 * @xtype cuenta-poliza-selector
 * description
 */


Ext.define('Pce.ingresos.poliza.captura.CuentaPolizaSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'cuenta-poliza-selector',

    require: [
        'Pce.ingresos.poliza.CuentaPolizaStore'
    ],

    store: {
        type: 'cuenta-poliza'
    },

    fieldLabel: 'Cuenta',
    valueField: 'cuenta',
    displayField: 'cuentaMaestra',
    forceSelection: true,
    minChars: 4,
    queryParam: 'filtro',
    emptyText: 'número de cuenta contable',
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{cuentaMaestra} [{descripcion}]</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{cuentaMaestra} [{descripcion}]',
        '</tpl>'
    ],

});