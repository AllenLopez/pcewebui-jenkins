/**
 * @class Pce.ingresos.poliza.captura.CapturaPolizaDialogo
 * @extends Ext.window.Window
 * @xtype dialogo-captura-poliza
 * 
 */
Ext.define('Pce.ingresos.poliza.captura.CapturaPolizaDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'dialogo-captura-poliza',
    
    requires: [
        'Ext.window.MessageBox',
        'Pce.ingresos.poliza.captura.CapturaPolizaPanelController',
        // 'Pce.ingresos.poliza.captura.Panel',

    ],

    defaultListenerScope: true,

    autoShow: true,
    modal: true,
    title: 'Cuentas para póliza',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    record: undefined,
    detallesGuardados: undefined,

    viewModel: {
        data: {}
    },

    items: [{
        xtype: 'captura-poliza-panel',
    }],

    buttons: [{
        text: 'Aceptar',
        reference: 'comandoAceptar',
        handler: 'onAceptar'
    }, {
        text: 'Cancelar',
        handler: 'onCancelar'
    }],

    afterRender() {
        this.callParent();
        if(this.record) {
            this.down('form').loadRecord(this.record);
        }
    },

    onAceptar() {
        let me = this;
        let form = me.down('form');
        let tabla = me.down('grid');
        let registrosDetalle = tabla.getStore().count();

        if (registrosDetalle < 1) {
            me.mensajeFormulario(
                'Formulario inválido',
                'Revise cuidadosamente su captura, existen datos que debe llenar');
            return;
        }
        if (!me.validaFormulario(form)) {
            return;
        }

        let objetoPoliza = me.obtieneDatosFormulario(form);
        let exitoPoliza;

        console.log('objeto a procesar:');
        console.log(objetoPoliza);

        if(!me.validaObjetoPoliza(objetoPoliza)) {
            return;
        }
        me.confirmaGuardarPoliza().then( ans => {
            if(ans === 'yes') {
                me.guardarPoliza(objetoPoliza.record)
                    .then(
                        exito => {
                            console.log(`poliza guardada : ${exito}`);
                            console.log(objetoPoliza.detalles);

                            if(!me.preparaDetalles(objetoPoliza.detalles, objetoPoliza.record.getId())) {
                                me.mensajeFormulario('Proceso incompleto', 'no fue posible completar la operación');
                            } else {
                                me.mensajeFormulario('Proceso exitoso', 'La captura de póliza ha sido guardada con éxito');
                            }
                        },
                        error => {
                            me.mensajeFormulario('Error al guardar poliza', error);
                            exitoPoliza = false;
                        }
                );
            }
        });
    },

    validaObjetoPoliza(objetoPoliza) {
        if (objetoPoliza.valido) {
            return true;
        } else {
            this.mensajeFormulario(
                'Procesamiento incompleto',
                'no fue posible completar la operación'
            );

            return false;
        }
    },

    confirmaGuardarPoliza() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Guardar poliza',
                `Se guardará la póliza capturada. ¿Deseas proceder?`,
                ans => resolve(ans)
            ).setAlwaysOnTop(true, 'status', 100);
        });
    },

    guardarPoliza(record){
        if(record.phantom) {
            console.log('poliza perparada para guardar...');
            return new Promise((resolve, reject) => {
                record.save({
                    success(record, oper) {
                        console.log(`Póliza id: ${record.getId()}`);
                        resolve(oper.success);
                    },
                    failure(record, operation) {
                        console.log(operation.error);
                        let response = operation.error.response;
                        let msjError = response.statusText;
                        if (operation.error.response.responseText) {
                            msjError = response.responseText;
                        }
                        reject(msjError);
                    }
                });
            });
        }
        console.log('saliendo de guardar póliza');
    },

    preparaDetalles(detalles, polizaId) {
        let me = this;
        let consecutivo = 0;
        console.log(`preparando ${detalles.length} detalles para guardar...`);

        detalles.forEach( el => {
            el.set('consecutivo', consecutivo++);
            el.set('polizaId', polizaId);
            console.log(`id poliza detalle: ${el.get('polizaId')}`);

            me.guardarDetalles(el)
                .then(
                    valor => {
                        me.detallesGuardados += valor;
                        console.log(`registros guardados: ${me.detallesGuardados}`);
                    },
                    error => console.log(`error con registro: ${error}`));
        });

        return me.detallesGuardados === detalles.length;

    },

    guardarDetalles(record) {
        return new Promise((resolve, reject) => {
            if(record.phantom) {
                let cuenta = record.get('cuenta');
                let id = record.getId();

                record.save({
                    success() {
                        console.log(`detalle ${id} - ${cuenta}. guardado`);
                        resolve(1);
                    },
                    failure(rec, oper) {
                        console.log(`detalle ${id} - ${cuenta}. ${oper.error}`);
                        let response = oper.error.response;
                        let msjError = response.statusText;
                        if (oper.error.response.responseText) {
                            msjError = response.responseText;
                        }
                        reject(msjError);
                    }
                });
            }
        });
    },

    validaFormulario(form) {
        if(!form.isValid()) {
            this.mensajeFormulario(
                'formulario inválido',
                'Revise cuidadosamente su captura, existen datos inválidos en el formulario');
            return false;
        }

        return true;
    },
    
    onCancelar() {
        this.close();
    },

    mensajeFormulario(cabecero, mensaje) {
        Ext.Msg.alert(
            cabecero,
            mensaje
        ).setAlwaysOnTop(true, 100);
    },

    obtieneDatosFormulario(forma) {
        let controller = forma.getController();
        let refItems = forma.getRefItems();
        let grid = refItems[0];
        let detalles = [];
        let valoresForm = forma.getForm().getValues();
        
        valoresForm.debe = controller.totalHaber;
        valoresForm.haber = controller.totalDebe;

        if(grid.xtype === 'poliza-cuenta-tabla') {
            detalles = grid.getStore().getData();
        }

        return this.creaObjetos(detalles.items, valoresForm);
    },

    creaObjetos(detalles, datos) {
        let objeto = {};
        
        if(detalles) {
            let polizaDetalles = detalles;
            this.record.set(datos);

            if(datos.debe === datos.haber) {
                objeto.valido = true;
                objeto.record = this.record;
                objeto.detalles = polizaDetalles;
            } else {
                objeto.valido = false;

                this.mensajeFormulario(
                    'Importes incorrectos',
                    'Los importes de "debe" y "haber" tienen que coincidir'
                );
            }
        }

        return objeto;
    },
});