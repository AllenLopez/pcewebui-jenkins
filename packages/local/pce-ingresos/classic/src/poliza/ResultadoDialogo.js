/**
 * @class Pce.ingresos.poliza.ResultadoDialogo
 * @extends Ext.panel.panel
 * @xtype poliza-resultado-dialogo
 * Cuadro de diálogo modal para desplegar listado de resultados/errores para cierta póliza
 */
Ext.define('Pce.ingresos.poliza.ResultadoDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'poliza-resultado-dialogo',
    
    requires: [
        'Pce.ingresos.poliza.ResultadoTabla',
        'Pce.ingresos.poliza.ResultadoPolizaStore'
    ],

    layout: 'fit',
    maximizable: true,
    maximized: true,

    title: 'Errores del proceso de generación de póliza',
    width: 800,
    height: 600,

    config: {
        transaccion: undefined
    },

    items: {
        xtype: 'poliza-resultado-tabla',
        store: {
            type: 'resultado-poliza'
        }
    },

    afterRender() {
        this.callParent();

        if (this.transaccion) {
            this.cargarResultados(this.transaccion);
        }
    },

    setPoliza(transaccion) {
        this.transaccion = transaccion;
        
        if (this.rendered) {
            this.cargarResultados(transaccion);
        }
    },

    getPoliza() {
        return this.transaccion;
    },

    cargarResultados(transaccion) {        
        this.down('grid').getStore().load({
            params: {transaccionId: transaccion.data}
        });
    }
    
});