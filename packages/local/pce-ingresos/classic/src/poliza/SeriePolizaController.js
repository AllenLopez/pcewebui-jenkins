/**
 * @class Pce.ingresos.poliza.SeriePolizaController
 * @extends Ext.app.ViewController
 * @xtype controller.serie-poliza-controller
 * description
 */
Ext.define('Pce.ingresos.poliza.SeriePolizaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.serie-poliza-controller',

    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.ingresos.poliza.SeriePolizaForma'
    ],

    onAgregarSerie(){
        this.mostrarDialogoEdicion(new Pce.ingresos.poliza.SeriePoliza());
    },

    onGuardarSerie(dialogo, serie){
        let me = this;
        let form = dialogo.down('serie-poliza-forma').getForm();
        let estatus = form.findField('estatus').getValue();
        let nuevo = serie.phantom;
        
        serie.data.estatus = estatus;

        Ext.Msg.confirm(
            'Guardar serie',
            `¿Seguro que deseas guardar esta serie de póliza
            <i>${serie.get('nombre')}</i>`,
            ans => {
                if(ans === 'yes') {
                    serie.save({
                        success() {
                            if(nuevo){
                                me.view.getStore().reload();
                            }

                            Ext.toast('Los datos se han guardado correctamente');
                            dialogo.close();
                        }
                    });
                }
            }
        );
    },

    onEditarSerie(tabla, i, j, item, e, serie){
        this.mostrarDialogoEdicion(serie);
    },

    onEliminarSerie(tabla, i, j, item, e, serie){

        let store = this.view.getStore();

        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar esta serie de póliza
            <i>${serie.get('nombre')}</i>`,
            ans => {
                if(ans === 'yes'){
                    serie.erase({
                        success() {
                            Ext.toast('La serie seleccionada se ha eliminado');
                        },
                        failure(batch, operations){
                            store.reload();
                            Ext.toast(operations.error.response.responseText);
                        }
                    });
                }
            }
        );
    },

    mostrarDialogoEdicion(serie) {
        let title = serie.phantom ? 'Nueva serie de póliza' : 'Editar serie';

        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title,
            record: serie,
            width: 400,
            items: {
                xtype: 'serie-poliza-forma'
            },
            listeners: {
                guardar: this.onGuardarSerie.bind(this)
            }
        });
    }

});