/**
 * @class Pce.ingresos.poliza.SeriePolizaTabla
 * @extends Ext.grid.Panel
 * @xtype serie-poliza-tabla
 * @description Componente tipo table que enlista todos los tipos de serie para pólizas
 */
Ext.define('Pce.ingresos.poliza.SeriePolizaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'serie-poliza-tabla',

    requires: [
        'Pce.ingresos.poliza.SeriePolizaStore',
        'Pce.ingresos.poliza.SeriePolizaController'
    ],

    controller: 'serie-poliza-controller',

    store: {
        type: 'series-poliza',
        autoLoad: true
    },

    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarSerie'
    }],
    columns: [{
        text: 'Serie',
        dataIndex: 'serie',
        flex: 1,
        tooltip: `valor numérico único que indica seriado de póliza`,
    }, {
        text: 'Nombre',
        dataIndex: 'nombre',
        flex: 4,
        tooltip: `Texto que describe la serie de póliza`,
    }, {
        text: 'Estatus',
        dataIndex: 'estatus',
        flex: 1,
        tooltip: `Estado del registro ya sea Activo o Inactivo`,
        renderer(v) {
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        }
    }, {
        text: 'Creado en',
        dataIndex: 'fechaCreacion',
        flex: 2,
        tooltip: `Fecha en que fue creado el registro`,
        xtype: 'datecolumn',
        format: 'd/m/Y'
    }, {
        text: 'Creado por',
        dataIndex: 'usuarioCreacion',
        flex: 3,
        tooltip: `Usuario que creo el registro`,
    }, {
        xtype: 'actioncolumn',
        // flex: 3,
        width: 80,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar Serie',
            handler: 'onEditarSerie'
        }, {
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar Serie',
            handler: 'onEliminarSerie'
        }]
    }],

});