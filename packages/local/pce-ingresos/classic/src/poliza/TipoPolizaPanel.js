/**
 * @class Pce.ingresos.poliza.TipoPolizaPanel
 * @extends Ext.form.panel
 * @xtype tipo-poliza-panel
 * @description Formulario para realizar operaciones CRUD con el catálogo de Tipo de Polizas
 */
Ext.define('Pce.ingresos.poliza.TipoPolizaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'tipo-poliza-panel',

    requires: [
        'Pce.ingresos.poliza.TipoPolizaTabla'
    ],

    title: 'Catálogo de Tipos de pólizas',

    items: [{
        xtype: 'container',
        items: [{
            xtype: 'tipo-poliza-tabla'
        }]
    }]
  
});