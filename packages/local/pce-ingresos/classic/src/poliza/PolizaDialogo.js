/**
 * @class Pce.ingresos.poliza.PolizaDialogo
 * @extends Ext.window.Window
 * @xtype poliza-dialogo
 * Cuadro de diálogo modal para desplegar listado de póliza asociado a transacción
 */
Ext.define('Pce.ingresos.poliza.PolizaDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'poliza-dialogo',

    requires: [
        'Pce.ingresos.poliza.PolizaStore',
        'Pce.ingresos.poliza.PolizaTabla'
    ],

    defaultListenerScope: true,

    transaccionId: null,

    title: 'Dialogo de pólizas',
    layout: 'fit',
    autoShow: true,
    modal: true,
    width: 1200,
    height: 600,

    items: {
        xtype: 'poliza-tabla',
        reference: 'polizaTabla',
        store: { type: 'poliza' },
        emptyText: 'No existen registros',
        columns: [{
            text: 'Tipo',
            dataIndex: 'tipoPoliza',
            flex: 0.9
        }, {
            text: 'Serie',
            dataIndex: 'seriePoliza',
            flex: 1.5
        }, {
            text: 'Folio',
            dataIndex: 'folio',
            flex: 0.6
        }, {
            text: 'Descripcion',
            dataIndex: 'descripcion',
            flex: 2
        }, {
            text: 'Debe',
            dataIndex: 'debe',
            renderer: Ext.util.Format.usMoney,
            flex: 1.5
        }, {
            text: 'Haber',
            dataIndex: 'haber',
            renderer: Ext.util.Format.usMoney,
            flex: 1.5
        }, {
            text: 'Trans.',
            dataIndex: 'transaccion',
            flex: 0.6
        }, {
            text: 'Exp. Contab.',
            dataIndex: 'expContabilidad',
            flex: 1
        }, {
            text: 'Activo',
            dataIndex: 'estatus',
            renderer(v){
                let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
                return `<span class="${cls}"></span>`;
            } ,
            flex: 1
        }, {
            text: 'Fecha',
            dataIndex: 'fecha',
            flex: 1.1
        }, {
            text: 'Usuario',
            dataIndex: 'usuarioCreacion',
            flex: 1
        }]
    },

    afterRender() {
        this.callParent();

        if (this.transaccionId) {
            this.cargarResultados(this.transaccionId);
        }
    },

    cargarResultados(transaccionId) {
        this.down('grid')
            .getStore()
            .setProxy({
                type: 'rest',
                url: '/api/ingresos/poliza/por-transaccion'
            })
            .load({
            params: { transaccionId: transaccionId }
        });
    }
});