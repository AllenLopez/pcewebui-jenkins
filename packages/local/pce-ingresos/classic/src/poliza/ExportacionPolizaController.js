Ext.define('Pce.ingresos.poliza.ExportacionPolizaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.exportacion-poliza-controller',

    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.ingresos.poliza.PolizaDetalleTabla',
        'Pce.ingresos.poliza.PolizaDetalleStore'
    ],

    parametros: {
        tipo: 'D',
        serie: 'I',
        transaccion: undefined,
        folio: undefined,
        fechaInicio: undefined,
        fechaFinal: undefined,
        polizaId: undefined
    },

    validarFiltros() {
        let boton = Ext.get('btnImprimirPolizas').component;
        if (this.parametros.fechaInicio != undefined
            && this.parametros.fechaFinal != undefined) {
            boton.setDisabled(false);
            return;
        }

        boton.setDisabled(true);


        return true;

    },

    onSeleccionDesde(comp, valor) {
        this.parametros.fechaInicio = Ext.util.Format.date(valor, 'm/d/Y');

    },

    onSeleccionHasta(comp, valor) {
        this.parametros.fechaFinal = Ext.util.Format.date(valor, 'm/d/Y');

    },

    onSeleccionTipoPoliza(selector, valor) {
        this.parametros.tipo = valor;
    },
    
    onSeleccionSeriePoliza(selector, valor) {
        this.parametros.serie = valor;
    },

    onGenerarArchivo(comp, valor) {
        // this.parametros.fechaFinal = Ext.util.Format.date(valor, 'm/d/Y');
        let store = this.lookup('poliza-tabla').getStore();
        if (store.data.length == 0) {
            Ext.Msg.alert('Advertencia!', 'No hay pólizas para generar el archivo de exportación');
            return;
        }

        Ext.Msg.confirm('Generar archivo de exportación', '¿Seguro que desea generar el archivo de las pólizas en el rango de fechas capturado?',
            (choice) => {
                if (choice === 'yes') {
                    let params = `fecha1=${Ext.util.Format.date(this.parametros.fechaInicio, 'd/m/Y')}&fecha2=${Ext.util.Format.date(this.parametros.fechaFinal, 'd/m/Y')}&pTipo=${this.parametros.tipo}&pSerie=${this.parametros.serie}`;
                    let me = this;
                    Ext.getBody().mask('Exportando pólizas...');
                    Ext.Ajax.request({
                        url: '/api/ingresos/poliza/archivo-exportacion',
                        method: 'GET',
                        params: params,
                        callback(opt, success, response) {
                            let resultado = Ext.decode(response.responseText);
                            let mensaje = `${resultado.mensaje}`;
                            let icono = Ext.Msg.ERROR;
                            if (success) {
                                Ext.getBody().unmask();

                                if (resultado.exito) {
                                    mensaje = `El archivo de la exportación de pólizas ha sido enviado; por favor verifique su correo electrónico.`;
                                    icono = Ext.Msg.INFO;
                                    Ext.Msg.alert(
                                        'Proceso terminado',
                                        mensaje
                                    );
                                } else {
                                    mensaje = ``;
                                    icono = Ext.Msg.INFO;
                                    Ext.Msg.alert(
                                        'Proceso terminado',
                                        mensaje
                                    );
                                }
                            }
                        }
                    });
                }
            }
        );
    },

    onImprimirPoliza(tabla, i, j, item, e, poliza) {
        let params = `?Pidpoliza=${poliza.getId()}&serie=${poliza.get('seriePoliza')}&tipo=${poliza.get('tipoPoliza')}&folio=${poliza.get('folio')}`;

        window.open(`/api/ingresos/poliza/descargar${params}`);
    },

    onImprimirPolizas(tabla, i, j, item, e, poliza) {
        let store = this.lookup('poliza-tabla').getStore();
        if (store.data.length == 0) {
            Ext.Msg.alert('Advertencia!', 'No hay pólizas para imprimir');
            return;
        }
        let fecha1 = Ext.util.Format.date(this.parametros.fechaInicio, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(this.parametros.fechaFinal, 'd/m/Y');
        let params = `?fecha1=${fecha1}&fecha2=${fecha2}`;
        window.open(`/api/ingresos/poliza/descargar-polizas-exportacion${params}`);
    },

    onBuscarPoliza() {
        let fecha1 = new Date(this.parametros.fechaInicio);
        let fecha2 = new Date(this.parametros.fechaFinal);
        if (fecha2 < fecha1) {
            Ext.Msg.alert('Advertencia!', 'La fecha final tiene que ser mayor a la fecha inicial');
            return;
        }
        let store = this.lookup('poliza-tabla').getStore();
        store.load({
            params: this.parametros
        });
        // this.validarFiltros();
    },

    onVerDetalles(tabla, i, j, item, e, poliza) {
        this.mostrarDialogoTabla(poliza);
    },

    onSelectItem(comp, record, item, index, e, eOpts) {
        this.view.viewModel.data.poliza = record;
    },

    mostrarDialogoTabla(poliza) {
        Ext.create({
            xtype: 'window',
            title: `Detalle de póliza - ${poliza.get('descripcion')}`,
            autoShow: true,
            modal: true,
            height: '90vh',
            width: '100vw',
            layout: 'fit',
            items: {
                xtype: 'poliza-detalle-tabla',
                store: {
                    type: 'poliza-detalle',
                    autoLoad: {
                        params: {
                            polizaId: poliza.getId()
                        }
                    }
                }
            }
        })
    }
});