/**
 * @class Pce.ingresos.poliza.GeneraPolizaPanel
 * @extends Ext.grid.Panel
 * @xtype genera-poliza-panel
 * Pantalla para generar polizas según el tipo de movimiento y transacción
 */

 Ext.create('Ext.data.Store', {
    alias: 'store.tipoEfecto',
    storeId: 'tipoEfecto',
    fields:[ 'nombre', 'valor'],
    data: [
        { nombre: 'Afectación', valor: 'AFECTACION' },
        { nombre: 'Cancelación', valor: 'CANCELACION' },
    ]
});

Ext.define('Pce.ingresos.poliza.GeneraPolizaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'genera-poliza-panel',

    requires: [
        'Ext.form.field.ComboBox',

        'Pce.ingresos.movimientoContable.MovimientoContable',
        'Pce.ingresos.movimientoContable.MovimientoContableStore',
        'Pce.ingresos.transaccion.TransaccionTabla',
        'Pce.ingresos.transaccion.TransaccionPolizaStore',
        'Pce.ingresos.polzia.GeneraPolizaTabla'
    ],

    controller: 'genera-poliza-panel',

    viewModel: {
        data: {
            movimientoContable: null,
            transaccion: null,
            fechaInicial: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            fechaFinal: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
            fecha: null
        }
    },

    title: 'Generar pólizas',
    layout: 'fit',
    emtpyText: 'no hay datos para mostar',

    items: [{
        xtype: 'genera-poliza-tabla',
        reference: 'transaccionTabla',
        store: {
            type: 'transacciones-polizas',
            autoLoad: true
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Desde',
                allowBlank: false,
                name: 'fechaInicio',
                bind: {
                    value: '{fechaInicial}',
                    maxValue: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaInicial'
                }
            }, {
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                reference: 'fechaFinal',
                allowBlank: false,
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicial}',
                    disabled: '{!fechaInicial}',
                    value: '{fechaFinal}'
                },
                listeners: {
                    change: 'onSeleccionaFechaFinal'
                }
            }, {
                xtype: 'combobox',
                name: 'movimientoContable',
                reference: 'tipoMovimiento',
                fieldLabel: 'Tipo de movimiento',
                displayField: 'movimientoDescripcion',
                valueField: 'tipoMovimientoId',
                emptyText: 'Selecciona el tipo de movimiento',
                width: 600,
                labelWidth: 150,
                store: {
                    type: 'movimiento-contable',
                    autoLoad: true
                },
                listeners: {
                    change: 'onSeleccionaTipoMovimiento'
                },
                disabled: true,
                bind: {
                    disabled: '{!fechaFinal}',
                    selection: '{movimientoContable}'
                }
            }, {
                text: 'Buscar',
                handler: 'onBuscarTransacciones'
            }]
        },{
            xtype: 'toolbar',
            dock: 'top',
            items: [   {
                text: 'Seleccionar todo',
                handler: 'onSeleccionarTodo'
            }, {
                text: 'Quitar todo',
                handler: 'onQuitarTodo'
            }]
        }],
        bind: {
            selection: '{transaccion}'
        },
        
    }],
    buttons: [{
        text: 'Generar póliza',
        handler: 'onCrearPoliza',
        bind: {
            disabled: '{!transaccion}'
        }
    }]
});