/**
 * @class Pce.ingresos.poliza.TipoPolizaForma
 * @extends Ext.form.Panel
 * @xtype tipo-poliza-forma
 * @description Formulario para Agregar/Editar tipos de pólizas
 */
Ext.define('Pce.ingresos.poliza.TipoPolizaForma', {
    extend: 'Ext.form.Panel',
    xtype: 'tipo-poliza-forma',
    
    requires: [
      'Ext.layout.container.Form',
      'Ext.form.field.Text',
      'Ext.form.field.Checkbox'
    ],
  
    layout: 'form',
  
    items: [{
        xtype: 'textfield',
        name: 'tipo',
        fieldLabel: 'Clave de tipo',
        allowBlank: false
    }, {
        xtype: 'textfield',
        name: 'nombre',
        fieldLabel: 'Nombre',
        allowBlank: false
    }, {
        xtype: 'checkbox',
        name: 'estatus',
        fieldLabel: 'Activo'
    }]
  
  });