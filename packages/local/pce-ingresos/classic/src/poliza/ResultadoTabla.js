/**
 * @class Pce.ingresos.poliza.ResultadoTabla
 * @extends Ext.grid.Panel
 * @xtype resultado-poliza-tabla
 * Tabla que despliega datos del resultado/error al intentar generar póliza
 */
Ext.define('Pce.ingresos.poliza.ResultadoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'poliza-resultado-tabla',
    
    emptyText: 'No existen errores para esta póliza',

    columns: [{
        text: 'Id',
        dataIndex: 'id',
        flex: 1
    }, {
        text: 'Descripcion',
        dataIndex: 'resultadoDescripcion',
        flex: 4
    }]
});