/**
 * @class Pce.ingresos.poliza.SeriePolizaForma
 * @extends Ext.form.Panel
 * @xtype serie-poliza-forma
 * @description Formulario para Agregar/Editar series de pólizas
 */
Ext.define('Pce.ingresos.poliza.SeriePolizaForma', {
  extend: 'Ext.form.Panel',
  xtype: 'serie-poliza-forma',
  
  requires: [
    'Ext.layout.container.Form',
    'Ext.form.field.Text',
    'Ext.form.field.Checkbox'
  ],

  layout: 'form',

  items: [{
      xtype: 'textfield',
      name: 'serie',
      fieldLabel: 'Clave de Serie',
      allowBlank: false
  }, {
    xtype: 'textfield',
    name: 'nombre',
    fieldLabel: 'Nombre',
    allowBlank: false
  }, {
    xtype: 'checkbox',
    name: 'estatus',
    fieldLabel: 'Activo'
  }]

});