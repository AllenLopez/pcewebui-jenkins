/**
 * @class Pce.ingresos.poliza.TipoPolizaController
 * @extends Ext.app.ViewController
 * @xtype controller.tipo-poliza-controller
 * description
 */
Ext.define('Pce.ingresos.poliza.TipoPolizaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.tipo-poliza-controller',

    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.ingresos.poliza.TipoPolizaForma',
        'Pce.ingresos.poliza.SeriePolizaForma'
    ],

    onAgregarTipo(){
        this.mostrarDialogoEdicion(new Pce.ingresos.poliza.TipoPoliza());
    },

    onGuardarTipo(dialogo, tipo){
        let me = this;
        let form = dialogo.down('tipo-poliza-forma').getForm();
        let estatus = form.findField('estatus').getValue();
        let nuevo = tipo.phantom;

        tipo.data.estatus = estatus;
        
        Ext.Msg.confirm(
            'Guardar tipo',
            `¿Seguro que deseas guardar este tipo de póliza
            <i>${tipo.get('nombre')}</i>`,
            ans => {
                if(ans === 'yes') {
                    tipo.save({
                        success() {
                            if(nuevo){
                                me.view.getStore().reload();
                            }

                            Ext.toast('Los datos se han guardado correctamente');
                            dialogo.close();
                        }
                    });          
                }
            }
        );
    },

    onEditarTipo(tabla, i, j, item, e, tipo){
        this.mostrarDialogoEdicion(tipo);
    },

    onEliminarTipo(tabla, i, j, item, e, tipo){

        let store = this.view.getStore();

        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar este tipo de póliza? 
            <i>${tipo.get('nombre')}</i>`,
            ans => {
                if(ans === 'yes'){
                    tipo.erase({
                        success(){
                            Ext.toast('El tipo de póliza seleccionado se ha eliminado ');
                        },
                        failure(batch, operations){
                            store.reload();
                            Ext.toast(operations.error.response.responseText);
                        }
                    });
                }
            }
        );
    },

    mostrarDialogoEdicion(tipo){
        let titulo = tipo.phantom ? 'Nuevo tipo de póliza'
        : 'Editar tipo';

        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            titulo,
            record: tipo,
            width: 400,
            items: {
                xtype: 'tipo-poliza-forma'
            },
            listeners: {
                guardar: this.onGuardarTipo.bind(this)
            }
        });
    }
  
});