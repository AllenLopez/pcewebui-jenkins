Ext.define('Pce.ingresos.polzia.GeneraPolizaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'genera-poliza-tabla',

    requires: [
        'Ext.grid.plugin.CellEditing'
    ],
    // selModel: Ext.create('Ext.selection.CheckboxModel', {
    //     checkOnly : true,
    //     checkbox: true,
    //     showHeaderCheckbox: false
    // }),
    // renderTo: Ext.getBody(),

    selModel: {
    selType: 'checkboxmodel'
    },
    
    listeners: {
        beforeselect: function(grid, record) {
            if (record.get('polizasFolios').length > 0 || record.get('etapa') !== 'AU') {
                return false;
            }
        },      
    },

    columns: [{
        text: 'Transacción',
        dataIndex: 'id',
        width: 80,
        flex: 0.8
    }, {
        text: 'Fecha de movimiento',
        dataIndex: 'fechaMovimiento',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Etapa',
        dataIndex: 'etapaDescripcion',
        
        flex: 1
    }, {
        text: 'Polizas',
        dataIndex: 'polizasFolios',
        flex: 1,
        renderer(v) {
            return v.length;
        }
    }, {
        text: 'Origen',
        dataIndex: 'origenClave',
        flex: 1
    },{
        text: 'Fecha Póliza',
        dataIndex: 'fechaPoliza',
        flex: 1,            
        renderer(v) {
            return `<b style="color: #2862a0">
            <i class="fa fa-caret-right"></i>
            ${Ext.util.Format.date(v, 'd/m/Y')}
            </b>`;
        },
        editor: {
            xtype: 'datefield',
            allowBlank: false,
            hideTrigger: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false,
        },
    }, {
        xtype: 'actioncolumn',
        width: 80,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'Ver pólizas',
            handler: 'onMostrarDetallesPolizas'
        }]
    }],
    plugins: [{
        ptype: 'cellediting',
        clicksToEdit: 1
    }],
});