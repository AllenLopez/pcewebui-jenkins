/**
 * @class Pce.ingresos.poliza.PolizaTabla
 * @extends Ext.grid.Panel
 * @xtype poliza-tabla
 * Tabla que enlista pólizas a nivel maestro
 */
Ext.define('Pce.ingresos.poliza.PolizaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'poliza-tabla',

    requires: [
        'Pce.ingresos.poliza.PolizaStore'
    ],

    store: {
        type: 'poliza'
    },

    emptyText: 'No existen registros',

    columns: [{
        text: 'Tipo',
        dataIndex: 'tipoPoliza',
        flex: 0.9
    }, {
        text: 'Serie',
        dataIndex: 'seriePoliza',
        flex: 1.5
    }, {
        text: 'Folio',
        dataIndex: 'folio',
        flex: 0.6
    }, {
        text: 'Descripcion',
        dataIndex: 'descripcion',
        flex: 2
    }, {
        text: 'Debe',
        dataIndex: 'debe',
        renderer: Ext.util.Format.usMoney,
        flex: 1.5
    }, {
        text: 'Haber',
        dataIndex: 'haber',
        renderer: Ext.util.Format.usMoney,
        flex: 1.5
    }, {
        text: 'Trans.',
        dataIndex: 'transaccion',
        flex: 0.6
    }, {
        text: 'Exp. Contab.',
        dataIndex: 'expContabilidad',
        flex: 1
    }, {
        text: 'Activo',
        dataIndex: 'estatus',
        renderer(v){
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        } ,
        flex: 1
    }, {
        text: 'Fecha',
        dataIndex: 'fecha',
        flex: 1.1
    }, {
        text: 'Usuario',
        dataIndex: 'usuarioCreacion',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        flex: 1,
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'Ver detalles',
            handler: 'onVerDetalles'
        },{
            iconCls: 'x-fa fa-print',
            tooltip: 'Imprimir poliza',
            handler: 'onImprimirPoliza'
        }]
    }],

    setPolizas(polizas){
        this.polizas = polizas;
        this.store.setData(polizas);
    },

    afterRender(){
        this.callParent();
        if(this.polizas){
            this.setPolizas(this.polizas);
        }
    }
  
});