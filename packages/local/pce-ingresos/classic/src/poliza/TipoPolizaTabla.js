/**
 * @class Pce.ingresos.poliza.TipoPolizaTabla
 * @extends Ext.grid.Panel
 * @xtype tipo-poliza-tabla
 * @description Componente tipo table que enlista todos los tipos de pólizas
 */
Ext.define('Pce.ingresos.poliza.TipoPolizaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'tipo-poliza-tabla',

    requires: [
        'Pce.ingresos.poliza.TipoPolizaStore',
        'Pce.ingresos.poliza.TipoPolizaController'
    ],

    controller: 'tipo-poliza-controller',

    store: {
        type: 'tipos-poliza',
        autoLoad: true
    },

    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarTipo',
        tooltip:'Crear nuevo tipo de póliza.'
    }],
    columns: [{
        text: 'Tipo',
        dataIndex: 'tipo',
        flex: 1,
        tooltip: `Letra que indica el tipo de póliza.`
    }, {
        text: 'Nombre',
        dataIndex: 'nombre',
        flex: 4,
        tooltip: `Nombre del tipo de póliza.`
    }, {
        text: 'Estatus',
        dataIndex: 'estatus',
        flex: 1,
        tooltip: `Estado en el que se encuentra el tipo de póliza.`,
        renderer(v) {
            let cls = v ? 'x-fa fa-check': 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        }
    }, {
        text: 'Creado en',
        dataIndex: 'fechaCreacion',
        flex: 2,
        tooltip: `Fecha en que fue creado el registro`,
        xtype: 'datecolumn',
        format: 'd/m/Y'
    }, {
        text: 'Creado por',
        dataIndex: 'usuarioCreacion',
        flex: 3,
        tooltip: `Usuario que creo el registro`,
    }, {
        xtype: 'actioncolumn',
        flex: 3,
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar Tipo',
            handler: 'onEditarTipo'
        }, {
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar Tipo',
            handler: 'onEliminarTipo'
        }]
    }]
  
});