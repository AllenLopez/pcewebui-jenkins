/**
 * @class Pce.ingresos.poliza.SeriePolizaPanel
 * @extends Ext.form.Panel
 * @xtype serie-poliza-panel
 * @description Formulario para realizar operaciones CRUD con el catálogo de Serie de Polizas
 */
Ext.define('Pce.ingresos.poliza.SeriePolizaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'serie-poliza-panel',

    requires: [
        'Pce.ingresos.poliza.SeriePolizaTabla',
    ],

    title: 'Catálogo de serie de pólizas',

    items: [{
        xtype: 'container',
        items: [{
            xtype: 'serie-poliza-tabla'
        }]
    }]
});