Ext.define('Pce.ingresos.poliza.PolizaSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'poliza-selector',

    require: [
        'Pce.ingresos.poliza.PolizaStore'
    ],

    fieldLabel: 'Folio',
    valueField: 'id',
    displayField: 'folio',
    queryMode: 'local',
    forceSelection: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{folio}</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],
    displayTpl: [
        '<tpl for=".">',
            '{folio}',
        '</tpl>'
    ],

    store: {
        type: 'poliza',
        autoLoad: true,
        // filters: [{
        //     property: 'estatus',
        //     value: true
        // }]
    }
});