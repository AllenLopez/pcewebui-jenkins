Ext.define('Pce.ingresos.dependencia.DiferencialServicioMedicoForma', {
    extend: 'Ext.form.Panel',

    layout: 'hbox',
    items: [{
        xtype: 'numberfield',
        name: 'diferencialSM',
        fieldLabel: 'Diferencial SM',
        allowBlank: false
    }, {
        xtype: 'datefield',
        name: 'fecha',
        fieldLabel: 'Fecha',
        allowBlank: false,
        listeners: {
            change: 'onFechaChange'
        }
    }],

    bbar: [{
        text: 'Calcular prorrateo',
        handler: 'onCalcularProrrateoTap'
    }],

    url: '/api/ingresos/diferencial-servicio-medico/calcular'
});