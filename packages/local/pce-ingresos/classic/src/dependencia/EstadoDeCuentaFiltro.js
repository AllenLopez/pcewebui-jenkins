Ext.define('Pce.ingresos.dependencia.EstadoDeCuentaFiltro', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'dependencia-estado-de-cuenta-filtro',

    defaultListenerScope: true,
    filtro: {},

    requires: [
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.ingresos.concepto.ConceptoSelector',

        'Pce.afiliacion.dependencia.DependenciaStore',
        'Pce.ingresos.concepto.ConceptoStore',

        'Ext.form.field.Date'
    ],
    items: [{
        xtype: 'dependencia-selector',
        name: 'dependenciaId',
        flex: 1.7,
        store: {type: 'dependencias', autoLoad: true},
        listeners: {
            change: 'onFiltroCambio'
        }
    }, {
        xtype: 'concepto-selector',
        name: 'conceptoId',
        flex: 1.7,
        store: {
            type: 'conceptos',
            autoLoad: true,
            filters: [{
                property: 'estatus',
                value: true
            }, {
                filterFn(concepto) {
                    return /amb|dep/i.test(concepto.get('destino'));
                }
            }]
        },
        listeners: {
            change: 'onFiltroCambio'
        }
    }, {
        xtype: 'datefield',
        name: 'desde',
        fieldLabel: 'A partir de',
        labelWidth: 80,
        flex: 1,
        listeners: {
            change: 'onFiltroCambio'
        }
    }, {
        xtype: 'datefield',
        name: 'hasta',
        fieldLabel: 'Hasta',
        labelWidth: 40,
        flex: 1,
        listeners: {
            change: 'onFiltroCambio'
        }
    }],

    afterRender() {
        this.callParent();

        this.inicializarFecha();
    },

    inicializarFecha() {
        let fecha = new Date();
        let campo = this.down('datefield');
        let hasta = Ext.ComponentQuery.query('[name=hasta]')[0];
        
        fecha.setDate(1);
        fecha.setMonth(0);

        campo.setValue(fecha);
        hasta.setValue(new Date());
    },

    onFiltroCambio(campo, valor) {
        if (valor instanceof Date) {
            valor = Ext.Date.format(valor, 'Y-m-d');
        }

        this.filtro[campo.name] = valor;
        this.fireEvent('filtrocambio', this, Object.assign({}, this.filtro));
    }
});