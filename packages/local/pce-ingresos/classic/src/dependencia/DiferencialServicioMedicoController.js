/**
 * @class Pce.ingresos.dependencia.DiferencialServicioMedicoController
 * @extends Ext.app.ViewController
 * @alias diferencial-servicio-medico
 * Controlador encargada de generar prorrateos y afectación de cartera para servicio médico
 */

Ext.define('Pce.ingresos.dependencia.DiferencialServicioMedicoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.diferencial-servicio-medico',

    requires: [

    ],

    onFechaChange(control, newValue) {
        console.log(newValue);
    },

    onCalcularProrrateoTap() {
        console.log('tap');
        this.down('form').submit({
            scope: this,
            success: this.onCalcularProrrateoExito
        });
    },

});