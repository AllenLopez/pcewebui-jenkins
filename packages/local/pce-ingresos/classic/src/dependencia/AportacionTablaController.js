Ext.define('Pce.ingresos.dependencia.AportacionTablaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dependencia-aportacion-tabla',

    requires: [
        'Pce.view.dialogo.FormaContenedor',
        'Pce.ingresos.dependencia.AportacionForma'
    ],

    dependencia: null,

    onDependenciaSeleccion(selector, dependencia) {
        if (this.dependencia === dependencia) {
            return;
        }
        this.dependencia = dependencia;
        this.view.getStore().load({
            params: {
                dependencia: dependencia.getId()
            }
        });
    },

    onNuevaAportacion() {
        this.mostrarDialogoEdicion(
            new Pce.ingresos.dependencia.Aportacion(),
            'Nueva aportación de dependencia'
        );
    },

    onEditarAportacion(a,b,c,d,e, aportacion) {
        console.log(arguments);
        this.mostrarDialogoEdicion(aportacion, 'Editar aportación');
    },

    mostrarDialogoEdicion(aportacion, titulo) {
        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title: titulo,
            autoShow: true,
            modal: true,
            width: 500,
            record: aportacion,
            items: {
                xtype: 'dependencia-aportacion-forma'
            },
            listeners: {
                guardar: this.onGuardarAportacion.bind(this)
            }
        });
    },

    onGuardarAportacion(dialogo, aportacion) {
        Ext.Msg.confirm(
            'Guardar tipo de aportación',
            `¿Seguro que deseas guardar la aportacion con clave <i>${aportacion.get('conceptoClave')}</i>?`,
            ans => {
                if(ans === 'yes') {
                    aportacion.save({
                        success: () => {
                            this.view.getStore().reload();

                            Ext.toast('Los cambios se han guardado correctamente');
                            dialogo.close();
                        }
                    });
                }
            }
        );
    }
});