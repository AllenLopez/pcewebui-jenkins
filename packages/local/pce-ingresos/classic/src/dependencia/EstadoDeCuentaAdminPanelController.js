Ext.define('Pce.ingresos.dependencia.EstadoDeCuentaAdminPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dependencia-estado-de-cuenta-admin-panel',

    requires: [
        'Pce.ingresos.dependencia.MovimientoTabla'
    ],
    onFiltroCambio(tbar, filtro) {
        if (filtro.desde && filtro.conceptoId && filtro.dependenciaId && filtro.hasta) {
            this.cargarEstadosDecuenta(filtro);
        }
    },

    cargarEstadosDecuenta(filtro) {
        this.lookup('estadoDeCuentaTabla').getStore().load({
            params: filtro
        });
    },

    onMostrarMovimientosTap(view, row, col, item, e, edoCta) {
        Ext.create('Ext.window.Window', {
            title: 'Movimientos',
            autoShow: true,
            modal: true,
            width: 1200,
            height: 600,
            layout: 'fit',
            items: {
                xtype: 'movimiento-dependencia-tabla',
                reference: 'movimientosDepTabla',
                estadoDeCuenta: edoCta,
            },
        });
    },
});