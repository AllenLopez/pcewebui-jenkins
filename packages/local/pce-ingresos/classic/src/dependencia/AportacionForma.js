Ext.define('Pce.ingresos.dependencia.AportacionForma', {
    extend: 'Ext.form.Panel',
    xtype: 'dependencia-aportacion-forma',

    layout: 'form',

    requires: [
        'Pce.ingresos.concepto.ConceptoStore',
        'Ext.form.field.Date'
    ],

    items: [{
        xtype: 'dependencia-selector',
        name: 'dependenciaId',
        allowBlank: false
    }, {
        xtype: 'concepto-selector',
        name: 'conceptoId',
        allowBlank: false,
        store: {type: 'conceptos', autoLoad: true}
    }, {
        xtype: 'numberfield',
        name: 'porcentaje',
        fieldLabel: 'Aportación',
        allowBlank: false,
        minValue: 0,
        maxValue: 100
    }, {
        xtype: 'datefield',
        name: 'fechaEfectiva',
        fieldLabel: 'Fecha efectiva',
        allowBlank: false
    }, {
        xtype:'checkbox',
        name: 'activo',
        fieldLabel: 'Activa',
        allowBlank: false,
        uncheckedValue: false
    }],

    loadRecord(record) {
        this.callParent([record]);

        ['dependenciaId', 'conceptoId', 'fechaEfectiva'].forEach(name => {
            this.down(`[name=${name}]`).setDisabled(!record.phantom);
        });
    }
});