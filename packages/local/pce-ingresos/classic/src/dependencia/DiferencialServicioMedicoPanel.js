Ext.define('Pce.ingresos.dependencia.DiferencialServicioMedicoPanel', {
    extend: 'Ext.panel.Panel',

    requires: [
        'Pce.ingresos.dependencia.DiferencialServicioMedicoController',
        'Pce.ingresos.dependencia.DiferencialServicioMedicoForma'
    ],

    controller: 'diferencial-servicio-medico',

    title: 'Cálculo de prorrateo de diferencial de sevicio médico',
    layout: 'hbox',
    bodyPadding: 10,

    referenceHolder: true,
    // defaultListenerScope: true,

    items: [{
        xtype: 'diferencial-servico-medico-forma',
        flex: 1
    }],

    // onCalcularProrrateoTap() {
    //     this.down('form').submit({
    //         scope: this,
    //         success: this.onCalcularProrrateoExito
    //     });
    // },

    onCalcularProrrateoExito() {

    }
});