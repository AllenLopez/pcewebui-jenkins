Ext.define('Pce.ingresos.dependencia.MovimientoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'movimiento-dependencia-tabla',

    requires: [
        'Pce.ingresos.dependencia.MovimientoStore'
    ],

    store: {
        type: 'movimiento-dependencia'
    },

    features: [{
        ftype: 'summary'
    }],

    columns: [{
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        flex: 2
    }, {
        text: 'Folio certificado',
        dataIndex: 'folioCertificado',
        flex: 1
    }, {
        text: 'Fecha nómina',
        dataIndex: 'fechaNomina',
        renderer(v) { return Ext.util.Format.date(v, 'd/m/Y'); },
        flex: 1
    }, {
        text: 'Cargo',
        dataIndex: 'importe',
        flex: 1,
        renderer(v, meta, movimiento) {
            if(movimiento.get('tipo') === 'CARGO') {
                return Ext.util.Format.usMoney(v);
            } else {
                return '';
            }
        },
        summaryType(data) {
            let importe = 0;
            data.forEach(el => {
                if(el.get('tipo') === 'CARGO') {
                    importe += el.get('importe');
                }
            });
            return importe;
        },
        summaryRenderer(val, summaryData){
            return `<b>${Ext.util.Format.usMoney(Object.values(summaryData)[3])}</b>`;
        },
    }, {
        text: 'Abono',
        dataIndex: 'importe',
        flex: 1,
        renderer(v, meta, movimiento) {
            if(movimiento.get('tipo') === 'ABONO') {
                return Ext.util.Format.usMoney(v);
            } else {
                return '';
            }
        },
        summaryType(data) {
            let sumAbono = 0;
            data.forEach(el => {
                if(el.get('tipo') === 'ABONO') {
                    sumAbono += el.get('importe');
                }
            });
            return sumAbono;
        },
        summaryRenderer(val, summaryData){
            return `<b>${Ext.util.Format.usMoney(Object.values(summaryData)[4])}</b>`;
        },
    }, {
        text: 'Saldo',
        dataIndex: 'importe',
        flex: 1,
        renderer() {
            return '';
        },
        summaryRenderer(val, summaryData){
            let totalCargo = Object.values(summaryData)[3];
            let totalAbono = Object.values(summaryData)[4];

            return `<b>${Ext.util.Format.usMoney(totalCargo - totalAbono)}</b>`;
        },
    }],

    setEstadoDeCuenta(edoCta) {
        this.estadoDeCuenta = edoCta;
        this.store.load({
            params: {
                estadoDeCuentaId: edoCta.getId()
            }
        });
    },

    afterRender() {
        this.callParent();
        if (this.estadoDeCuenta) {
            this.setEstadoDeCuenta(this.estadoDeCuenta);
        }
    }
});