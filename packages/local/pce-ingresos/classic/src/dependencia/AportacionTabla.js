Ext.define('Pce.ingresos.dependencia.AportacionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'dependencia-aportacion-tabla',

    requires: [
        'Pce.ingresos.dependencia.AportacionTablaController',
        'Pce.ingresos.dependencia.AportacionStore',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    controller: 'dependencia-aportacion-tabla',

    title: 'Catálogo de aportaciones por dependencia',

    store: {
        type: 'dependencias-aportaciones'
    },
    tbar: [{
        xtype: 'dependencia-selector',
        fieldLabel: 'Dependencia',
        flex: 1,
        store: {
            type: 'dependencias',
            autoLoad: true
        },
        listeners: {
            select: 'onDependenciaSeleccion'
        }
    }, {
        text: 'Agregar',
        handler: 'onNuevaAportacion',
        tooltip: 'Agregar una nueva especificación de aportación'
    }],
    columns: [{
        text: 'Clave',
        dataIndex: 'conceptoClave',
        tooltip:'Letra y numero que identifica el concepto.',
        flex: 1
    }, {
        text: 'Descripción',
        dataIndex: 'conceptoDescripcion',
        tooltip:'Descripción de la aportación.',
        flex: 3
    }, {
        text: 'Aportacion',
        dataIndex: 'porcentaje',
        tooltip:'Porcentaje que le corresponde pagar al asegurado, segun lo estipulado en la ley de pensiones civiles del estado.',
        renderer(v) {
            return Ext.util.Format.percent((v/100), '0.00##');
        },
        flex: 1
    }, {
        text: 'Fecha efectiva',
        dataIndex: 'fechaEfectiva',
        tooltip:'Fecha en que cambio la aportación de la dependencia (Ley anterior, reforma, cuenta individual).',
        flex: 1,
        renderer(v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        }
    }, {
        text: 'Estatus',
        dataIndex: 'activo',
        tooltip:'Estado del porcentaje de la aportación (Activo o inactivo).',
        flex: 1,
        renderer(v) {
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        }
    }, {
        xtype: 'actioncolumn',
        flex: 1,
        items: [{
            iconCls: 'x-fa fa-edit',
            handler: 'onEditarAportacion'
        }]
    }]
    
});