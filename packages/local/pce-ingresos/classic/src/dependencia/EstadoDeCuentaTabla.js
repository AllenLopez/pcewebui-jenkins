Ext.define('Pce.ingresos.dependencia.EstadoDeCuentaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'dependencia-estado-de-cuenta-tabla',

    columns: [{
        text: 'Concepto',
        dataIndex: 'conceptoDescripcion',
        tooltip: `Nombre del concepto donde se carga el estado de cuenta de la dependencia.`,
        flex: 3
    }, {
        text: 'Fecha nómina',
        dataIndex: 'fechaNomina',
        tooltip:`Fecha de la quincena en que se cargo la nómina.`,
        flex: 2,
        renderer(v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        }
    }, {
        text: 'Saldo',
        dataIndex: 'saldo',
        tooltip: `Muestra el total de cada concepto descontado del salario del afiliado.`,
        flex: 1,
        renderer: Ext.util.Format.usMoney
    }, {
        text: 'Importe inicial',
        dataIndex: 'importeInicial',
        tooltip: `Muestra el importe inicial de cada concepto descontado del salario del afiliado.`,
        flex: 1,
        renderer: Ext.util.Format.usMoney
    }, {
        text: 'Folio aviso cargo',
        dataIndex: 'emisionAvisoCargoFolio',
        tooltip: `Número asignado al aviso de cargo de la dependencia.`,
        flex: 2
    }, {
        xtype: 'actioncolumn',
        items: [{
            iconCls: 'x-fa fa-search',
            handler: 'onMostrarMovimientosTap'
        }]
    }]
});