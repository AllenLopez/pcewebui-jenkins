/**
 * @class Pce.ingresos.dependencia.diferencialsm.DiferencialSmController
 * @extends Ext.app.ViewController
 * @alias diferencial-servicio-medico
 * Controlador encargada de generar prorrateos y afectación de cartera para servicio médico
 *
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.dependencia.diferencialsm.DiferencialSmController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.diferencial-sm',

    diferencialSm: null,
    fecha: null,
    costo: null,

    onCalcularProrrateosTap() {
        let me = this;
        let valores = this.getFormProrrateo().getValues();
        let store = this.getProrrateosTabla().getStore();
        store.setProxy({
            type: 'rest',
            url: '/api/ingresos/diferencial-sm/prorrateos'
        });

        if(valores.total === '' || valores.total === '0') {
            Ext.Msg.alert('Costo indefinido',
                'Debe escribir un costo de servicio médico para poder calcular prorrateo');
            return;
        }
        store.load({
            params: this.getValores(),
            callback(records, operation, success) {
                if(success) {
                    me.lookup('afectaTap').enable();
                    me.lookup('reporteTap').enable();
                    me.onEjecutarReporteServicioMedico();
                } else {
                    me.lookup('reporteTap').disable();
                    me.lookup('afectaTap').disable();
                }
            },
        });

    },

    onAfectarCarteraTap() {
        let store = this.getProrrateosTabla().getStore();

        if(store.count() > 0) {
            this.confirmarAfectacion().then(ans => {
                if (ans === 'yes') {
                    this.realizarAfectacionExistente()
                        .then(transaccion => {
                                console.log(transaccion);
                                this.setFormViewModelProp('transaccionId', transaccion);
                                Ext.Msg.alert(
                                    'Operación completada',
                                    `La cartera se ha actualizado exitosamente bajo la transacción: <b>${transaccion.id}</b>`
                                );

                                Ext.getBody().unmask();
                                this.lookup('afectaTap').disable();

                                this.onEjecutarReporteServicioMedico();
                            },
                            fallo => {
                                console.log(fallo);
                            });
                }
            });

            // this.realizarAfectacion();
        }
        // let me = this;
        // let form = this.getFormProrrateo();
        // if(this.diferencialSm) {
        //     this.confirmarAfectacion(form.getValues()).then(ans => {
        //         if (ans === 'yes') {
        //             Ext.getBody().mask('Realizando afectación');
        //             this.realizarAfectacion(form);
        //         }
        //     });
        // }

        // if (form.isValid()) {
        //     me.confirmarAfectacion(form.getValues()).then(ans => {
        //         if (ans === 'yes') {
        //             Ext.getBody().mask('Realizando afectación');
        //             me.realizarAfectacion(form);
        //         }
        //     });
        // }
    },

    onFechaChange(component, value) {
        let me = this;
        me.fecha = value;

        if(value) {
            me.lookup('calculoTap').enable();
        } else {
            me.lookup('calculoTap').disable();
        }

        me.diferencialSm = null;
        me.validaProrrateoPorFecha(value, this.costo)
            .then(diferencial => {
                if(diferencial) {
                    let transaccionId = diferencial.get('transaccionId');

                    if(transaccionId) {
                        if(transaccionId !== 0) {
                            me.setFormViewModelProp('transaccionId', transaccionId);
                            Ext.Msg.alert('Prorrateo existente',
                                `Ya existe cálculo de prorrateo que ha afectado cartera para esta fecha.
                                Ver transaccion <b>${transaccionId}</b>`);
                        }
                        me.lookup('afectaTap').disable();
                        me.lookup('calculoTap').disable();
                    } else {
                        me.setFormViewModelProp('transaccionId', 0);
                        me.lookup('afectaTap').enable();
                    }
                }
            });
    },

    onCostoChange(component, value) {
        let me = this;
        this.costo = value;

        this.obtieneDetalleProrrateos();
        this.validaProrrateoPorFecha(this.fecha, value)
            .then(diferencial => {
                if(diferencial) {
                    let transaccionId = diferencial.get('transaccionId');

                    if(transaccionId) {
                        if(transaccionId !== 0) {
                            me.setFormViewModelProp('transaccionId', transaccionId);
                            Ext.Msg.alert('Prorrateo existente',
                                `Ya existe cálculo de prorrateo que ha afectado cartera para esta fecha.
                                Ver transaccion <b>${transaccionId}</b>`);
                        }
                        me.lookup('afectaTap').disable();
                        me.lookup('calculoTap').disable();
                    } else {
                        me.setFormViewModelProp('transaccionId', 0);
                        me.lookup('afectaTap').enable();
                    }
                }
            });
    },

    onEjecutarReporteServicioMedico() {
        if(this.diferencialSm) {
            window.open(`/api/ingresos/diferencial-sm/reporte-sm?diferencialId=${this.diferencialSm.getId()}`);
        }
    },
    realizarAfectacion(form) {
        return new Promise((resolve, reject) => {
            form.submit({
                scope: this,
                success: (form, action) => {
                    this.onAfectarCarteraSuccess(form, action);
                    resolve(action.result);
                },
                failure: (form, action) => reject(action)
            });
        });
    },
    onAfectarCarteraSuccess(form, action) {
        let transaccion = action.result.transaccion;
        this.setFormViewModelProp('transaccionId', transaccion.id);
        Ext.Msg.alert(
            'Operación completada',
            `La cartera se ha actualizado exitosamente bajo la transacción: 
            <b>${transaccion.id}</b>`
        );

        Ext.getBody().unmask();
        this.lookup('afectaTap').disable();

        this.onEjecutarReporteServicioMedico();
    },


    realizarAfectacionExistente() {
        let me = this;
        console.log(me.diferencialSm);
        return new Promise((resolve, reject) => {
            Ext.Ajax.request({
                url: '/api/ingresos/diferencial-sm/afectar-cartera',
                method: 'POST',
                params: {
                    diferencialSmId: me.diferencialSm.getId()
                },
                success(response) {
                    if(response.responseText) {
                        let resp = JSON.parse(response.responseText);
                        resolve(resp.transaccion);
                    } else {
                        let resp = JSON.parse(response.responseText);
                        reject(resp.success);
                    }
                }
            });
        });
    },
    onAfectarCarteraExistenteSuccess(form, action) {
        let transaccion = action.result.transaccion;
        this.setFormViewModelProp('transaccionId', transaccion.id);
        Ext.Msg.alert(
            'Operación completada',
            `La cartera se ha actualizado exitosamente bajo la transacción: 
            <b>${transaccion.id}</b>`
        );

        Ext.getBody().unmask();
        this.lookup('afectaTap').disable();

        this.onEjecutarReporteServicioMedico();
    },

    validaProrrateoPorFecha(fecha, costo) {
        let me = this;
        return new Promise(resolve => {
            Ext.Ajax.request({
                url: '/api/ingresos/diferencial-sm/',
                method: 'GET',
                params: {
                    fecha: fecha,
                    costo:costo
                },
                success(response) {
                    if(response.responseText) {
                        let diferencialSm = new Pce.ingresos.dependencia.DiferencialServicioMedico(
                            JSON.parse(response.responseText));

                        me.diferencialSm = diferencialSm;
                        console.log(me.diferencialSm);
                        me.lookup('reporteTap').enable();

                        resolve(diferencialSm);
                    } else {
                        me.lookup('reporteTap').disable();
                        me.lookup('afectaTap').disable();

                        me.setFormViewModelProp('transaccionId', 0);
                        me.setFormViewModelProp('diferencial', null);
                    }
                }
            });
        });
    },

    obtieneDetalleProrrateos() {
        let tabla = this.getProrrateosTabla();
        let store = tabla.getStore();
        let fecha = this.fecha;
        let costo = this.costo;

        tabla.getStore().setProxy({
            type: 'rest',
            url: '/api/ingresos/diferencial-sm/por-detalle'
        });

        store.load({
            params: {
                fecha: fecha,
                costo: costo
            }
        });
    },

    confirmarAfectacion() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se realizará a afectación de cartera. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    getValores() {
        return this.getFormProrrateo().getForm().getFieldValues();
    },
    getProrrateosTabla() {
        return this.lookup('prorrateosTabla');
    },
    getFormProrrateo() {
        return this.lookup('diferencialSmForma');
    },
    setFormViewModelProp(propiedad, valor) {
        return this.getFormProrrateo().getViewModel().set(propiedad, valor);
    },
});