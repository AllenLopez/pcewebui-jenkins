/**
 * @author Rubén Maguregui
 */
 Ext.define('Pce.ingresos.dependencia.diferencialsm.ProrrateosTabla', {
     extend: 'Ext.grid.Panel',
     xtype: 'diferencial-sm-prorrateos-tabla',

     require: [
         'Pce.ingresos.dependencia.DiferencialServicioMedicoDetalleStore'
     ],

     title: 'Desglose de prorrateos',
     features: [{
         ftype: 'summary'
     }],

     store: {
         type: 'diferencial-sm-detalle'
     },

     columns: [{
         text: 'Dependencia',
         dataIndex: 'dependenciaDescripcion',
         tooltip:'Dependencia a la que se le realizo el prorrateo',
         flex: 6
     }, {
         text: 'Asegurados',
         dataIndex: 'totalAsegurado',
         tooltip: 'Conteo total de asegurados',
         flex: 1
     }, {
         text: 'Beneficiarios',
         dataIndex: 'totalBeneficiario',
         tooltip: 'Conteo total de beneficiarios',
         flex: 1
     }, {
         text: 'Total',
         dataIndex: 'totalAseguradoBeneficiario',
         tooltip:'Conteo total de asegurados y beneficiarios',
         flex: 1
     }, {
         text: 'Importe',
         dataIndex: 'importeProrrateo',
         tooltip:'Total de prorrateo',
         flex: 1,
         renderer: v => Ext.util.Format.usMoney(v),
         summaryType: 'sum',
         summaryRenderer: v => `<b>Total: ${Ext.util.Format.usMoney(v)}</b>`,
     }]
 });