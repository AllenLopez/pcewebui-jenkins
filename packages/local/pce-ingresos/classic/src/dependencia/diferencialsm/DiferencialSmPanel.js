/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.dependencia.diferencialsm.DiferencialSmPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'diferencial-sm-panel',

    requires: [
        'Pce.ingresos.dependencia.diferencialsm.DiferencialSmController',

        'Pce.ingresos.dependencia.diferencialsm.ProrrateosTabla',
        'Pce.ingresos.dependencia.diferencialsm.DiferencialSmForma'
    ],

    title: 'Prorrateo de diferencial de servicio médico',
    controller: 'diferencial-sm',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [{
        xtype: 'diferencial-sm-forma',
        reference: 'diferencialSmForma',
        bodyPadding: 10,
    }, {
        xtype: 'diferencial-sm-prorrateos-tabla',
        reference: 'prorrateosTabla',
        flex: 1
    }]
});