/**
 * @author Rubén Maguregui
 */
 Ext.define('Pce.ingresos.dependencia.diferencialsm.DiferencialSmForma', {
     extend: 'Ext.form.Panel',
     xtype: 'diferencial-sm-forma',

     url: '/api/ingresos/diferencial-sm/afectar-cartera',
     method: 'POST',

     viewModel: {
         data: { transaccionId: 0 }
     },

     layout: {
         type: 'table',
         columns: 6
     },

     defaults: {
         width: '100%'
     },

     items: [{
         xtype: 'datefield',
         name: 'fecha',
         fieldLabel: 'Fecha',
         allowBlank: false,
         tooltip:'Fecha a considerar para afectar estado de cuenta',
         submitFormat: 'Y-m-d',
         listeners: {
             change: 'onFechaChange'
         },
         colspan: 1
     }, {
         xtype: 'numberfield',
         name: 'total',
         fieldLabel: 'Costo de S.M.',
         tooltip:'Importe mensual para prorrateo de diferencial de servicio médico',
         allowBlank: false,
         listeners: {
             change: 'onCostoChange'
         },
         colspan: 1
     }, {
         xtype: 'displayfield',
         name: 'transaccionId',
         readOnly: true,
         fieldLabel: 'Folio transaccion',
         labelWidth: 120,
         bind: {
             value: '{transaccionId}'
         },
         colspan: 1
     }],
     bbar: [{
         text: 'Calcular prorrateos',
         reference: 'calculoTap',
         handler: 'onCalcularProrrateosTap',
         tooltip: 'Realizar calculo de prorrateos de diferencial de servicio médico',
         disabled: true

     }, {
         text: 'Reporte diferencial de servicio médico',
         reference: 'reporteTap',
         handler: 'onEjecutarReporteServicioMedico',
         tooltip: 'Ejecuta y descarga reporte de diferencial de servicio médico luego de calcular prorrateos',
         disabled: true
     }, {
         text: 'Afectar cartera',
         reference: 'afectaTap',
         handler: 'onAfectarCarteraTap',
         tooltip:'Realizar afectación a cartera con datos calculados',
         disabled: true
     }]
 });