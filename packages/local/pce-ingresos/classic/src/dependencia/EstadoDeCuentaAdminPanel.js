Ext.define('Pce.ingresos.dependencia.EstadoDeCuentaAdminPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'dependencia-estado-de-cuenta-admin-panel',

    requires: [
        'Pce.ingresos.dependencia.EstadoDeCuentaAdminPanelController',

        'Pce.ingresos.dependencia.EstadoDeCuentaStore',
        'Pce.ingresos.dependencia.EstadoDeCuentaTabla',
        'Pce.ingresos.dependencia.EstadoDeCuentaFiltro'

    ],

    controller: 'dependencia-estado-de-cuenta-admin-panel',

    layout: 'fit',
    title: 'Estado de cuenta de dependencia',

    tbar: {
        xtype: 'dependencia-estado-de-cuenta-filtro',
        listeners: {
            filtrocambio: 'onFiltroCambio'
        }
    },
    items: [{
        xtype: 'dependencia-estado-de-cuenta-tabla',
        reference: 'estadoDeCuentaTabla',
        store: {type: 'dependencia-estados-de-cuenta'}
    }]
});