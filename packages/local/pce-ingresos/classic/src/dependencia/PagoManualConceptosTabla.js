Ext.define('Pce.ingresos.dependencia.PagoManualConceptosTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'ingresos-pago-manual-concepto-tabla',

    requires: [
        'Ext.plugin.Abstract',
        'Ext.grid.plugin.CellEditing'
    ],

    emptyText: 'Sin datos que mostrar',

    features: [{
        ftype: 'summary'
    }],

    viewConfig: {
        markDirty: false
    },

    columns: [{
        text: 'Concepto',
        dataIndex: 'conceptoDescripcion',
        flex: 3
    }, {
        text: 'Saldo',
        dataIndex: 'saldo',
        renderer: Ext.util.Format.usMoney,
        summaryType: 'sum',
        summaryRenderer: function(value) {
            return `<b>Saldo total: ${Ext.util.Format.usMoney(value)}</b>`;
        },
        flex: 2
    }, {
        text: 'Importe a pagar',
        dataIndex: 'porPagar',
        flex: 2,
        renderer(v) {
            return `<b style="color: #2862a0">
            <i class="fa fa-caret-right"></i>
            ${Ext.util.Format.usMoney(v)}
            </b>`;
        },
        summaryType: 'sum',
        summaryRenderer: 'onSummary',
        editor: {
            xtype: 'numberfield',
            allowBlank: false,
            hideTrigger: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false,
            minValue: 0,
            validator: 'validaImporte'
        },
    }, {
        dataIndex: 'saldoRestante',
        flex: 2,
        renderer(v) {
            return `Saldo restante: ${Ext.util.Format.usMoney(v)}`;
        },
        summaryType: 'sum',
        summaryRenderer(value) {
            return `<b>Saldo restante: ${Ext.util.Format.usMoney(value)}</b>`;
        }
    }],

    plugins: [{
        ptype: 'cellediting',
        clicksToEdit: 1
    }],

    listeners: [{
        beforeedit: 'onBeforeEdit',
        validateedit: 'onValidateEdit'
    }]
});