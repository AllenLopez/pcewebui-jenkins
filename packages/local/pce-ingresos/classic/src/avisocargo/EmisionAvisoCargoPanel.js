Ext.define('Pce.ingresos.avisocargo.EmisionAvisoCargoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'emision-aviso-cargo-panel',

    requires: [
        'Pce.ingresos.avisocargo.EmisionAvisoCargoPanelController',

        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.ingresos.avisocargo.EmisionAvisoCargoSelector',
        'Pce.ingresos.avisocargo.AvisoCargoSelector',
        'Pce.ingresos.dependencia.EstadoDeCuentaTabla',
        'Pce.ingresos.dependencia.EstadoDeCuentaStore'
    ],

    controller: 'emision-aviso-cargo-panel',
    defaultType: 'container',
    defaults: {
        layout: 'hbox'
    },
    items: [{
        items: [{
            xtype: 'dependencia-selector',
            flex: 1,
            listeners: {
                change: 'onDependenciaChange'
            }
        }, {
            xtype: 'avisocargo-selector',
            reference: 'avisoCargoSelector',
            flex: 1,
            listeners: {
                change: 'onAvisoCargoChange'
            }
        }]
    }, {
        items: [{
            xtype: 'emision-avisocargo-selector',
            reference: 'emisionAvisoCargoSelector',
            queryMode: 'local',
            flex: 1,
            store: {
                type: 'emision-aviso-cargo',
                proxy: {
                    type: 'rest',
                    url: '/api/ingresos/emision-aviso-cargo/por-grupo'
                }
            },
            listeners: {
                change: 'onEmisionAvisoCargoChange'
            }
        }]
    }, {
        items: [{
            xtype: 'dependencia-estado-de-cuenta-tabla',
            reference: 'estadoDeCuentaTabla',
            width: '100%',
            store: {
                type: 'dependencia-estados-de-cuenta',
                proxy: {
                    type: 'rest',
                    url: '/api/ingresos/dependencia/estado-de-cuenta/emision-aviso-cargo'
                }
            },
            buttons: [{
                iconCls: 'x-fa fa-print',
                text: 'Imprimir aviso de cargo',
                handler: 'onImprimirAvisoCargo'
            }]
        }]
    }]
});