/**
 * @author Jorge Escamilla
 * @class AvisoCargoManualPanel
 * @extends Panel
 * @xtype aviso-cargo-manual-panel
 * @desc Formulario para generar aviso de cargo manual para Dependencias
 */

Ext.define('Pce.ingresos.avisocargo.AvisoCargoManualPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'aviso-de-cargo-manual-panel',

    requires: [
        'Pce.ingresos.avisocargo.AvisoCargoManualController',

        'Ext.form.field.Number',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.form.field.Tag',

        'Pce.ingresos.quincena.QuincenaSelector',
        'Pce.ingresos.quincena.QuincenaStore',
        'Pce.ingresos.concepto.ConceptoSelector',
        'Pce.ingresos.movimiento.TipoMovimientoSelector',
        'Pce.ingresos.concepto.ConceptoStore',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaStore',
        'Pce.ingresos.movimiento.TipoMovimientoStore',
        'Pce.ingresos.avisocargo.AvisoCargoManualStore'
    ],

    controller: 'aviso-de-cargo-manual-panel',

    viewModel: {
        data: {
            dependencia: null,
            concepto: null,
            fechaNomina: new Date(),
            fechaProceso: new Date(),
            importe: null
        }
    },


    title: 'Emisión de Aviso Cargo',

    defaults: {
        style: 'margin: 5px',
    },

    items: [{
        xtype: 'container',
        items: [{
                xtype: 'dependencia-selector',
                reference: 'dependenciaSelector',
                emptyText: 'Selecciona una dependencia',
                store: {
                    proxy: {
                        type: 'rest',
                        url: '/api/afiliacion/dependencia/por-concentradora'
                    },
                    type: 'dependencias',
                    autoLoad: true,
                },
                bind: {
                    selection: '{dependencia}'
                },
                width: 800
            },{
                xtype: 'concepto-selector',
                fieldLabel: 'Conceptos',
                valueField: 'clave',
                reference: 'conceptosSelector',
                displayField: 'clave',
                forceSelection: true,
                emptyText: 'Selecciona el concepto',
                filterPickList: true,
                store: {
                    type: 'conceptos',
                    autoLoad: true,
                    proxy: {
                        type: 'rest',
                        url: '/api/ingresos/concepto/por-nuevo-aviso'
                    },
                },
                bind: {
                    disabled: '{!dependencia}',
                    selection: '{concepto}'
                },
                labelWidth: 100,
                width: 500
            }
        ]
    }, {
        xtype: 'container',
        items: [{
            xtype: 'quincena-selector',
            reference: 'quincenaSelector',
            flex: 3,
            valueField: 'fecha',
            store: {
                type: 'quincena',
                autoLoad: true,
                proxy: {
                    type: 'rest',
                    url: '/api/ingresos/quincena/ejercicio-actual',
                },
            },
            bind: {
                disabled: '{!concepto}',
                value: '{fechaNomina}'
            },
            width: 500
        }, {
            xtype: 'datefield',
            reference: 'fechaProceso',
            fieldLabel: 'Fecha proceso',
            maxValue: new Date(),
            flex: 2,
            allowBlank: false,
            format: 'd/m/Y',
            bind: {
                disabled: '{!concepto}',
                value: '{fechaProceso}'
            },
            width: 250
        }, {
            xtype: 'numberfield',
            fieldLabel: 'Importe',
            reference: 'importe',
            minValue: 0.01,
            flex: 3,
            valueField: 0,
            allowBlank: false,
            bind: {
                disabled: '{!concepto}',
                value: '{importe}'
            },
            width: 250
        }]
    }],
    buttons: [{
        text: 'Crear emisión',
        handler: 'onCrearAvisoCargo',
        bind: {
            disabled: '{!importe}'
        }
    }]
});