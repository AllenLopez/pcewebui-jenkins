Ext.define('Pce.ingresos.avisocargo.AvisoCargoTablaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.aviso-cargo-tabla',

    requires: [
        'Pce.ingresos.avisocargo.AvisoCargoForma',

        'Pce.ingresos.concepto.ConceptoStore',
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    /**
     * @property {Pce.ingresos.concepto.ConceptoStore} conceptos
     */
    conceptos: undefined,

    dependencias: undefined,

    dependenciaId: undefined,

    init() {
        this.conceptos = Ext.create('Pce.ingresos.concepto.ConceptoStore', {
            autoLoad: true
        });
        this.dependencias = Ext.create('Pce.afiliacion.dependencia.DependenciaStore', {
            autoLoad: true
        });
    },

    destroy() {
        this.conceptos.destroy();
        this.conceptos = null;
        this.callParent();
    },

    onNuevoAvisoCargo() {
        this.mostrarDialogoEdicion(new Pce.ingresos.avisocargo.AvisoCargo({
            dependenciaId: this.dependenciaId,
            conceptosIds: [],
            dependenciasIds: []
        }));
    },

    onEditarAvisoCargo(tabla, i, j, item, e, avisoCargo) {
        this.mostrarDialogoEdicion(avisoCargo);
    },

    onGuardarAvisoCargo(dialogo, avisoCargo) {
        let me = this;
        let nuevo = avisoCargo.phantom;

        Ext.Msg.confirm(
            'Guardar grupo de aviso de cargo',
            `¿Seguro que deseas guardar el concepto <i>${avisoCargo.get('descripcion')}</i>?`,
            ans => {
                if(ans === 'yes') {
                    avisoCargo.save({
                        success(record, operation) {
                            if (record.data.hasOwnProperty('exito')) {
                                Ext.toast(record.data.mensaje);
                            } else {
                                me.view.getStore().add(avisoCargo);
                            }
            
                            dialogo.close();
                        },
                        failure(err, action) {
                            Ext.toast(action.error.response.responseText);
                        }
                    });
                }
            }
        );
    },

    onEliminarAvisoCargo(tabla, i, j, item, e, avisoCargo) {
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar el aviso de cargo "${avisoCargo.get('descripcion')}"`,
            ans => {
                if (ans === 'yes') {
                    avisoCargo.erase({
                        success() {
                            Ext.toast('El aviso de cargo se ha eliminado');
                        }
                    });
                }
            });
    },

    mostrarDialogoEdicion(avisoCargo) {
        const title = avisoCargo.phantom ? 'Nuevo aviso de cargo' :
            'Editar aviso de cargo';
        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title: title,
            record: avisoCargo,
            width: '50vw',
            items: {
                xtype: 'aviso-cargo-forma',
                viewModel: {
                    stores: {
                        conceptos: this.conceptos,
                        dependencias: this.dependencias,
                        dependenciasMultiple: Ext.create('Ext.data.ChainedStore', {
                            source: this.dependencias
                        })
                    }
                }
            },
            listeners: {
                guardar: this.onGuardarAvisoCargo.bind(this)
            }
        });
    },

    onDependenciaSeleccion(campo, dependencia) {
        if (this.dependenciaId === dependencia.getId()) {
            return;
        }
        this.dependenciaId = dependencia.getId(0);
        this.getViewModel().set('dependencia', dependencia);
        let store = this.view.getStore().load({
            params: {
                dependenciaId: this.dependenciaId
            }
        });
    },

    onMostrarInactivosCambio(campo, mostrarInactivos) {
        let store = this.view.getStore();
        if (mostrarInactivos) {
            store.removeFilter('activo');
        } else {
            store.filter('activo', true);
        }
    }
});