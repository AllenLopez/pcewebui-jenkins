Ext.define('Pce.ingresos.avisocargo.ImpresionAvisoCargoPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'impresion-aviso-de-cargo-panel',

    requires: [
        'Ext.layout.container.Form',

        'Pce.ingresos.avisocargo.ImpresionAvisoCargoController',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.ingresos.avisocargo.EmisionAvisoCargoSelector',
        'Pce.ingresos.avisocargo.EmisionAvisoCargoTabla'
    ],

    viewModel: {
        data: {
            avisoCargo: null
        }
    },

    defaults: {
        style: 'margin: 5px',
    },

    title: 'Consulta de emisión de Aviso Cargo',

    controller: 'impresion-aviso-cargo-controller',

    items: [{
        margin: 5,
        items: [{
            layout: 'hbox',
            items: [{
                xtype: 'dependencia-selector',
                reference: 'dependenciaSelector',
                name: 'dependencia',
                allowBlank: false,
                margin: 5,
                flex: 4,
                labelWidth: 120,
                listeners: {
                    change: 'onSeleccionDependencia'
                }
            }, {
                xtype: 'emision-avisocargo-selector',
                reference: 'emisionAvisoCargoSelector',
                name: 'avisocargo',
                emtpyText: 'Sin datos encontrados',
                labelWidth: 60,
                margin: 5,
                flex: 2,
                listeners: {
                    change: 'onSeleccionAvisoCargo'
                },
                bind: {
                    selection: '{avisoCargo}'
                }
            }]
        }]
    }, {
        items: [{
            xtype: 'emision-aviso-cargo-tabla',
            reference: 'tablaEstadosdeCuenta',
            store: {
                type: 'dependencia-estados-de-cuenta',
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/api/ingresos/dependencia/estado-de-cuenta/edo-cta-transaccion'
                }
            }
        }]
    }],

    buttons: [{
        text: 'Imprimir',
        reference: 'botonImprimir',
        bind: {
            disabled: '{!avisoCargo}'
        },
        handler: 'onImprimirAvisoCargo',
    }]
});