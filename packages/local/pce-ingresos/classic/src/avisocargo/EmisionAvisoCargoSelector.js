/*!
 * @author Jorge Escamilla
 * 09/13/2018
 * Clase que extiende ComboBox. Muestra y enlista las emisiones de aviso de argo con formato de folio-fecha 
 */

Ext.define('Pce.ingresos.avisocargo.EmisionAvisoCargoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'emision-avisocargo-selector',

    requires: [
        'Pce.ingresos.avisocargo.EmisionAvisoCargoStore'
    ],

    valueField: 'id',
    displayField: 'folio',
    fieldLabel: 'Folio',
    labelWidth: 120,
    forceSelection: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
        '<tpl for=".">',
        '<tr role="option" class="x-boundlist-item">',
        '<td><b>{folio}</b> {[Ext.util.Format.date(values.fechaEmision, "d/m/Y")]}</td>',
        '</tr>',
        '</tpl>',
        '</tbody>',
        '</table>'
    ],

    displayTpl: [
        '<tpl for=".">',
        '{folio} [{[Ext.util.Format.date(values.fechaEmision, "d/m/Y")]}]',
        '</tpl>'
    ],

    store: {
        type: 'emision-aviso-cargo'
    }
});