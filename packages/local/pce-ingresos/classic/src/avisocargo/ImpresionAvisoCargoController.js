Ext.define('Pce.ingresos.avisocargo.ImpresionAvisoCargoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.impresion-aviso-cargo-controller',

    avisoCargoFolio: undefined,
    transaccionId: undefined,

    onSeleccionDependencia(component, dependenciaId) {
        let selector = this.lookup('emisionAvisoCargoSelector');
        selector.getStore().getProxy().setExtraParams({
            dependenciaId: dependenciaId
        });
        selector.getStore().load();
    },

    onSeleccionAvisoCargo(selector, avisoCargoId){
        let avisoCargo = selector.getSelection();
        if (avisoCargo) {
            this.avisoCargoFolio = avisoCargo.data.folio;
            this.lookup('tablaEstadosdeCuenta').getStore().load({
                params:{
                    folio: this.avisoCargoFolio
                }
            });
        }
    },

    onImprimirAvisoCargo() {
        const store = this.lookup('tablaEstadosdeCuenta').getStore();
        for (let i = 0; i < store.data.items.length; i++) {
            let item = store.data.items[i].data;
            this.transaccionId = item.transaccionId;
            if(this.transaccionId > 0){
                break;
            }
        }
        window.location = `/api/ingresos/aviso-cargo/descargar?Pfolio=${this.avisoCargoFolio}&&Ptransaccion=${this.transaccionId}`;
    }
});