Ext.define('Pce.ingresos.avisocargo.AvisoCargoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'aviso-cargo-forma',

    requires: [
        'Ext.layout.container.Form',

        'Ext.form.field.Text',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Tag',

        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaSelectorMultiple'
    ],

    layout: 'form',

    items: [{
        xtype: 'textfield',
        name: 'descripcion',
        fieldLabel: 'Descripción',
        allowBlank: false
    }, {
        xtype: 'dependencia-selector',
        name: 'dependenciaId',
        fieldLabel: 'Emitir a',
        allowBlank: false,
        readOnly: true,
        bind: {
            store: '{dependencias}'
        }
    }, {
        xtype: 'dependencia-selector-multiple',
        name: 'dependenciasIds',
        fieldLabel: 'Dependencias incluidas',
        valueField: 'id',
        displayField: 'descripcion',
        bind: {
            store: '{dependenciasMultiple}'
        }
    }, {
        xtype: 'tagfield',
        name: 'conceptosIds',
        fieldLabel: 'Conceptos',
        allowBlank: false,
        filterPickList: true,
        queryMode: 'local',
        displayField: 'descripcion',
        valueField: 'id',
        bind: {
            store: '{conceptos}'
        }
    }, {
        xtype: 'checkbox',
        name: 'activo',
        uncheckedValue: false,
        fieldLabel: 'Activo'
    }]
});