/**
 * @author: Jorge Escamilla
 * @class
 * @description Controlador de formulario de Emisión de Aviso de Cargo manual 
 * que intenta crear un registro de emisión aviso de cargo para dependencia
 * dado un concepto, importe y fecha de nómina. Imprime documento de
 * emisión de aviso de cargo al emitirse correctamente
 */

Ext.define('Pce.ingresos.avisocargo.AvisoCargoManualController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.aviso-de-cargo-manual-panel',

    requires: [
        'Ext.window.Toast',

        'Pce.ingresos.transaccion.Transaccion'
    ],

    onCrearAvisoCargo() {
        let viewModel = this.getViewModel();
        let avisoCargo = new Pce.ingresos.avisocargo.AvisoCargoManual({
            dependenciaId: viewModel.get('dependencia').getId(),
            conceptoId: viewModel.get('concepto').getId(),
            importe: viewModel.get('importe'),
            fechaNomina: viewModel.get('fechaNomina')
        });
        Ext.Msg.confirm(
            'Confirmar operación',
            `Se generará un nuevo aviso de cargo y se afectará el estado de 
            cuenta para la dependencia y el concepto correspondiente. 
            ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    Ext.getBody().mask('Creando emisión de aviso de cargo...');
                    this.crearAvisoCargo(avisoCargo);
                }
            }
        );
    },

    crearAvisoCargo(avisoCargo) {
        let me = this;
        avisoCargo.save({
            success: (record) => {
                if (!record.data.exito) {
                    let transaccion = new Pce.ingresos.transaccion.Transaccion(record.data.transaccion);
                    
                    Ext.Msg.alert(
                        'Proceso terminado',
                        'Ocurrio un error al intentar crear la emisión de aviso de cargo',
                        () => me.mostrarResultados(transaccion)
                    );
                } else {
                    Ext.Msg.show({
                        title: 'Proceso exitoso',
                        message: `Aviso de cargo generado exitosamente con No. transaccion: ${record.data.transaccionId}`,
                        buttons: Ext.Msg.OK,
                        fn: function(btn) {
                            if(btn === 'ok') {
                                me.onImprimirEmisionAvisoCargo(
                                    record.data.folioEmision,
                                    record.data.transaccionId
                                );
                            }
                        }
                    });
                    this.lookup('dependenciaSelector').reset();
                    this.lookup('conceptosSelector').reset();
                    this.lookup('quincenaSelector').reset();
                    this.lookup('fechaProceso').reset();
                    this.lookup('importe').reset();
                }
                Ext.getBody().unmask();
            }
        });
    },

    onImprimirEmisionAvisoCargo(folio, transaccionId) {
        window.open(`/api/ingresos/aviso-cargo/descargar?Pfolio=${folio}&Ptransaccion=${transaccionId}`);
    },

    mostrarResultados(transaccion) {
        Ext.create({
            xtype: 'transaccion-resultado-dialogo',
            transaccion: transaccion,
            autoShow: true,
            modal: true
        });
    }
});