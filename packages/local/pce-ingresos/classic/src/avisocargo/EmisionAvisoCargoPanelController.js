Ext.define('Pce.ingresos.avisocargo.EmisionAvisoCargoPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emision-aviso-cargo-panel',

    onDependenciaChange(selector, dependenciaId) {
        this.lookup('avisoCargoSelector').getStore().load({
            params: {
                dependenciaId
            }
        });
    },

    onAvisoCargoChange(selector, grupoId) {
        this.lookup('emisionAvisoCargoSelector').getStore().load({
            params: {
                grupoId
            }
        });
    },

    onEmisionAvisoCargoChange(selector, avisoCargoId) {
        this.lookup('estadoDeCuentaTabla').getStore().load({
            params: {
                avisoCargoId
            }
        });
    },

    onImprimirAvisoCargo() {
        let emisionId = this.lookup('emisionAvisoCargoSelector').getValue();
        console.log(emisionId);
        if (!emisionId) {
            return;
        }

        window.open(`/api/ingresos/emision-aviso-cargo/imprimir?emisionId=${emisionId}`);
    }
});