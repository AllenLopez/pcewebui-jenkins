Ext.define('Pce.ingresos.avisocargo.AvisoCargoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'aviso-cargo-tabla',

    requires: [
        'Ext.grid.feature.Grouping',

        'Pce.ingresos.avisocargo.AvisoCargoTablaController',
        'Pce.ingresos.avisocargo.AvisoCargoStore',

        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    controller: 'aviso-cargo-tabla',
    viewModel: {
        data: {
            dependencia: false
        }
    },

    grouped: true,
    features: [{ftype:'grouping', enableGroupingMenu: false}],
    store: {
        type: 'avisos-cargo',
        filters: [{
            id: 'activo',
            property: 'activo',
            value: true
        }]
    },

    title: 'Catálogo de grupo de aviso de cargo',
    emptyText: 'No se encontraron avisos de cargo',
    viewConfig: {
        loadingText: 'Cargando avisos de cargo...',
        getRowClass(avisoCargo) {
            if (!avisoCargo.get('activo')) {
                return 'fila-inactiva';
            }
            return '';
        }
    },

    tbar: [{
        xtype: 'dependencia-selector',
        fieldLabel: 'Dependencia aviso',
        labelWidth: 150,
        emptyText: 'Selecciona una dependencia para mostrar sus grupos de avisos de cargo',
        flex: 1,
        store: {type: 'dependencias', autoLoad: true},
        listeners: {
            select: 'onDependenciaSeleccion'
        }
    }, {
        text: 'Agregar',
        handler: 'onNuevoAvisoCargo',
        tooltip: 'Crear un nuevo grupo de aviso de cargo emitido a la dependencia seleccionada.',
        bind: {
            disabled: '{!dependencia}'
        }
    }, {
        xtype: 'checkbox',
        fieldLabel: 'Mostrar inactivos',
        tooltip: `Muestra los avisos de cargo inactivos de cada dependencia.`,
        labelWidth: 150,
        bind: {disabled: '{!dependencia}'},
        listeners: {
            change: 'onMostrarInactivosCambio'
        }
    }],

    columns: [{
        text: 'Dependencia(s)',
        dataIndex: 'dependenciasDescripciones',
        tooltip:`Nombre de la dependencia afiliada a PCE.`,
        flex: 5,
        renderer(v, meta, avisoCargo) {
            if (v.length === 0) {
                v.push(avisoCargo.get('dependenciaDescripcion'));
            }

            return '<ul style="margin: 0">' +
                v.map(x => `<li>${x}</li>`).join('') +
                '</ul>';
        }
    }, {
        text: 'Conceptos',
        dataIndex: 'conceptosDescripciones',
        tooltip: `Tipo de conceptos que le corresponden a la dependencia afiliada a PCE.`,
        flex: 4,
        renderer(v) {
            return '<ul style=" margin: 0">' +
                v.map(x => `<li>${x}</li>`).join('') +
                '</ul>';
        }
    }, {
        text: 'Descripción',
        dataIndex: 'descripcion',
        tooltip: `Agregar claves de los tipos de conceptos`,
        flex: 3
    }, {
        text: 'Creado en',
        tooltip:`Fecha en que fue creado el concepto.`,
        dataIndex: 'fechaCreacion',
        xtype: 'datecolumn',
        format: 'd/m/Y'
    }, {
        text: 'Creado por',
        dataIndex: 'usuarioCreacion',
        tooltip:`Usuario por el que fue creado el concepto.`,
    }, {
        text: 'Estatus',
        dataIndex: 'activo',
        tooltip:`Estatus en el que se encuentra el concepto.`,
        renderer(v) {
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        }
    }, {
        xtype: 'actioncolumn',
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            handler: 'onEditarAvisoCargo',
            tooltip: 'Editar aviso de cargo'
        }, {
            iconCls: 'x-fa fa-trash',
            handler: 'onEliminarAvisoCargo',
            tooltip: 'Eliminar aviso de cargo'
        }]
    }]
});