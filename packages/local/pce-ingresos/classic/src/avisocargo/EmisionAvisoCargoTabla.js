/**
 * @author: Jorge Escamilla
 * @class Pce.ingresos.avisocargo.EmisionAvisoCargoTabla
 * @extends Ext.form.Panel
 * @xtype emision-aviso-cargo-tabla
 * @description: Componente extjs de tipo grid para cargar y mostar
 * emisiones de aviso de cargo según dependencia y folio dados
 */
Ext.define('Pce.ingresos.avisocargo.EmisionAvisoCargoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'emision-aviso-cargo-tabla',
    requires: [
        'Pce.ingresos.avisocargo.EmisionAvisoCargoStore',
        'Pce.ingresos.dependencia.EstadoDeCuentaStore'
    ],

    store: {
        type: 'dependencia-estados-de-cuenta',
        autoLoad: true,

        proxy: {
            type: 'ajax',
            api: {
                create: undefined,
                read: '/api/ingresos/dependencia/estado-de-cuenta/emision-aviso-cargo',
                update: '/api/ingresos/dependencia/estado-de-cuenta/',
                destroy: undefined
            }
        }
    },
    
    emptyText: 'No se encontraron emisiones de aviso de cargo',

    reference: 'tablaEstadosDeCuenta',

    columns: [{
        text: 'Clave',
        dataIndex: 'clave',
        tooltip:'Valor alfanúmerico único para identificar la emisión de aviso de cargo.',
        flex: 1,
    }, {
        text: 'Concepto',
        dataIndex: 'conceptoDescripcion',
        tooltip:'Titulo del concepto de la emisión de aviso de cargo.',
        flex: 3
    }, {
        text: 'Importe inicial',
        dataIndex: 'importeInicial',
        tooltip:'Muestra el importe inicial de la emisión de aviso de cargo.',
        flex: 1,
        renderer: Ext.util.Format.usMoney
    }]

});