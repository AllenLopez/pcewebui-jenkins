Ext.define('Pce.ingresos.avisocargo.AvisoCargoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'avisocargo-selector',

    requires: [
        'Pce.ingresos.avisocargo.AvisoCargoStore'
    ],

    queryMode: 'local',
    valueField: 'id',
    displayField: 'descripcion',
    fieldLabel: 'Aviso de cargo',
    labelWidth: 120,
    forceSelection: true,
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td>{descripcion}</td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
        '</table>'
    ],

    displayTpl: [
        '<tpl for=".">',
            '{descripcion}',
        '</tpl>'
    ],

    store: {
        type: 'avisos-cargo',
        autoLoad: true,
        filters: [{
            property: 'activo',
            value: true
        }]
    }
});