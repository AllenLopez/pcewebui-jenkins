Ext.define('Pce.ingresos.factor80.Factor80Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.factor-80', 
    requires: [
        'Pce.ingresos.factor80.Factor80',
        'Pce.ingresos.factor80.Factor80Store',
        'Pce.ingresos.factor80.Factor80Forma',
        'Pce.view.dialogo.FormaContenedor'
    ],

    onAgregarFactor80() {
        this.mostrarDialogoEdicion(new Pce.ingresos.factor80.Factor80({
            estatus: true
        }),'Nuevo');
    },


    mostrarDialogoEdicion(factor80, titulo) {
        let grid = this.view;
        Ext.create({
            xtype: 'dialogo-forma-contenedor',
            title: `${titulo} factor 80`,
            items: {xtype: 'factor-80-forma'},
            width: 400,
            record: factor80,
            listeners: {
                guardar(dialogo) {
                    Ext.Msg.confirm(
                        'Guardar factor80',
                        `¿Seguro que deseas guardar un nueva esperanza de vida (Factor80) a número de años de <i>${factor80.get('edad')}</i>?`,
                        ans => {
                            if(ans === 'yes') {
                                factor80.save({
                                    success() {
                                        let store = grid.getStore();
                                        if(!store.contains(factor80)){
                                            grid.getStore().add(factor80);
                                        }
                                        Ext.toast('Los datos se han guardado correctamente');
                                        dialogo.close();
                                    },
                                });
                            }
                        });
                }
            }
        });
    },

});