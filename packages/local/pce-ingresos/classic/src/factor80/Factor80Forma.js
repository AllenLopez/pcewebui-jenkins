Ext.define('Pce.ingresos.factor80.Factor80Forma', {
    extend: 'Ext.form.Panel',
    xtype: 'factor-80-forma',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox'
    ],

    layout: 'form',
    defaultListenerScope: true,
    viewModel: {},


    items: [{
        xtype: 'textfield',
        name: 'edad',
        fieldLabel: 'Edad',
        tooltip: `Número de años que en promedio se espera que viva una persona`,
        allowBlank: false
    }, {
        xtype: 'datefield',
        name: 'fechaEfectiva',
        fieldLabel: 'Fecha efectiva',
        allowBlank: false
    }],

    config: {
        factor80: undefined
    },

    afterRender() {
        this.callParent();
        if (this.factor80) {
            this.loadRecord(this.factor80);
        }
    },

    loadRecord(record) {
        this.callParent([record]);

            
    },

});