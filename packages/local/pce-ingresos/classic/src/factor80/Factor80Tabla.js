Ext.define('Pce.ingresos.factor80.Factor80Tabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'factor-80-tabla',
    
title: 'Catálogo de Esperanza de vida (Factor80)',

    requires: [
        'Pce.ingresos.factor80.Factor80Store',

        'Pce.ingresos.factor80.Factor80Controller'
    ],

    controller: 'factor-80',
    store: {
        type: 'factor80',
        autoLoad: true
    },

    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarFactor80'
    }],
    columns: [{
        text: 'Edad',
        dataIndex: 'edad',
        tooltip: `Número de años que en promedio se espera que viva una persona`,
        flex: 1
    }, {
        text: 'Fecha efectiva',
        dataIndex: 'fechaEfectiva',
        tooltip:`Fecha efectiva`,
        flex: 2,
        renderer(v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        }
    }, {
        text: 'Estatus',
        dataIndex: 'estatus',
        tooltip:`Estado del factor80, es utilizado actualmente o dejo de usarse.`,
        renderer(v) {
            let cls = v ? 'x-fa fa-check' : 'x-fa fa-times';
            return `<span class="${cls}"></span>`;
        },
        flex: 1
    }, {
        text: 'Creado en',
        tooltip:`Fecha en que fue creado.`,
        dataIndex: 'fechaCreacion',
        xtype: 'datecolumn',
        format: 'd/m/Y',
        flex: 1
    }, {
        text: 'Creado por',
        dataIndex: 'usuarioCreacion',
        flex: 1
    }]
});