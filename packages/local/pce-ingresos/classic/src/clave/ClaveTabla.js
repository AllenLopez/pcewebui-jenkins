Ext.define('Pce.ingresos.clave.ClaveTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'clave-tabla',

    requires: [
        'Pce.ingresos.clave.ClaveTablaController',
        'Pce.clave.ClaveStore'
    ],

    controller: 'clave-tabla',

    store: {
        type: 'claves',
        autoLoad: true
    },

    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarClave'
    }],
    columns: [{
        text: 'Tabla',
        dataIndex: 'tabla',
        flex: 2
    }, {
        text: 'Columna',
        dataIndex: 'columna',
        flex: 2
    }, {
        text: 'Valor',
        dataIndex: 'valor',
        flex: 1
    }, {
        text: 'Descripción',
        dataIndex: 'descripcion',
        flex: 2
    }, {
        xtype: 'actioncolumn',
        items: [{
            iconCls: 'x-fa fa-edit',
            handler: 'onEditarClave'
        }, {
            iconCls: 'x-fa fa-trash',
            handler: 'onEliminarClave'
        }]
    }]
});