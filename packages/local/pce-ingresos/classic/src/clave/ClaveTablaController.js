Ext.define('Pce.ingresos.clave.ClaveTablaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.clave-tabla',

    requires: [
        'Pce.ingresos.clave.ClaveForma',
        'Pce.view.dialogo.FormaContenedor'
    ],

    onAgregarClave() {
        this.mostrarDialogoEdicion(
            new Pce.clave.Clave({})
        );
    },

    onEditarClave(view, row, col, item, e, clave) {
        this.mostrarDialogoEdicion(clave);
    },

    onEliminarClave(view, row, col, item, e, clave) {
        clave = clave.data;
        Ext.Msg.confirm(
            'Eliminar clave',
            `La clave ${clave.tabla}.${clave.columna}.${clave.valor} será eliminada permanentemente. ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    clave.erase();
                }
            }
        );
    },

    mostrarDialogoEdicion(clave) {
        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'Clave',
            record: clave,
            items: {
                xtype: 'clave-forma'
            },
            listeners: {guardar: this.onGuardarClave.bind(this)}
        });
    },

    onGuardarClave(dialogo, clave) {
        clave.save({
            success() {dialogo.close()}
        });
    }
});