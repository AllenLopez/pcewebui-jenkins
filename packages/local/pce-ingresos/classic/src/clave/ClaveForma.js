Ext.define('Pce.ingresos.clave.ClaveForma', {
    extend: 'Ext.form.Panel',
    xtype: 'clave-forma',

    items: [{
        xtype: 'textfield',
        name: 'tabla',
        fieldLabel: 'Tabla',
        allowBlank: false
    }, {
        xtype: 'textfield',
        name: 'columna',
        fieldLabel: 'Columna',
        allowBlank: false
    }, {
        xtype: 'textfield',
        name: 'valor',
        fieldLabel: 'Valor',
        allowBlank: false
    }, {
        xtype: 'textarea',
        name: 'descripcion',
        fieldLabel: 'Descripción'
    }]
});