Ext.define('Pce.ingresos.movimiento.TipoMovimientoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'tipo-movimiento-forma',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox'
    ],

    layout: 'form',

    items: [{
        xtype: 'textfield',
        name: 'descripcion',
        fieldLabel: 'Descripción',
        allowBlank: false
    }, {
        xtype: 'textfield',
        name: 'clave',
        fieldLabel: 'Clave',
        allowBlank: false
    }, {
        xtype: 'checkbox',
        name: 'activo',
        fieldLabel: 'Activo'
    }],

    loadRecord(record) {
        this.callParent([record]);

        this.down('[name="clave"]').setDisabled(!record.phantom);
    }
});