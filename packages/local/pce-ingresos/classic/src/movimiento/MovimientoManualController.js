Ext.define('Pce.ingresos.movimiento.MovimientoManualController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.movimiento-manual-controller',

    requires: [
        'Pce.ingresos.movimiento.PagoManualConceptosTabla'
    ],

    sumaTotal: undefined,
    folio: undefined,
    formaPagoId: undefined,
    bancoId: undefined,
    cuentaBancoId: undefined,
    bancoEmisor: undefined,
    numeroCheque: undefined,
    transaccionId: undefined,


    onProcesarPago() {
        let me = this;
        let montoCheque = me.lookup('montoCheque').value;
        let storeConceptos = me.lookup('tablaEstadosDeCuenta').getStore();
        let dependenciaForma = this.lookup('movimientoManualDependenciaForma');
        let panel = this.lookup('movimientoManualPanel');

        for (let i = 0; i < storeConceptos.data.items.length; i++) {
            let item = storeConceptos.data.items[i].data;
            item.bancoEmisor = me.bancoEmisor;
            item.cheque = me.numeroCheque;
            item.cuentaBancoId = me.cuentaBancoId;
            item.bancoId = me.bancoId;
            item.formaPagoId = me.formaPagoId;
        }

        if(montoCheque == me.sumaTotal && montoCheque > 0){
            storeConceptos.sync({
                success: function(batch, operations) {
                    operations.operations.update.forEach(el => {
                        Ext.toast(`Registro actualizado con exito, transaccion:${el.data.resultado.transaccionId}`);
                        me.transaccionId = el.data.resultado.transaccionId;
                    });
                    
                    me.imprimirCertificado(me.transaccionId);

                    dependenciaForma.reset();
                    panel.reset();
    
                    return true;
                },
                failure: function(batch, operations) {
                    storeConceptos.rejectChanges();
                    batch.exceptions.forEach(el => {
                        Ext.toast(el.error.response.statusText);
                    });
    
                    return false;
                }
            });
    
            // storeConceptos.loadData({});
        } else {
            Ext.Msg.alert(
                'Importe incorrecto',
                `El monto a pagar debe coincidir con el monto del cheque`
            );
        }
    },
    
    onSeleccionAvisoCargo(selector, avisoCargoId) {
        let avisoCargo = selector.getSelection();
        if (avisoCargo) {
            let tablaConceptos = this.lookup('tablaEstadosDeCuenta');
            this.folio = avisoCargo.data.folio;
            
            tablaConceptos.getStore().load({
                params: {
                    avisoCargoId: avisoCargoId
                },
                callback: function() {
                    success: {
                        for (let i = 0; i < this.data.items.length; i++) {
                            const el = this.data.items[i];
                            
                            el.set('porPagar', el.data.saldo);
                        }
                    }
                }
            });
            
            let saldo = Ext.util.Format.usMoney(avisoCargo.data.saldo);
            let fecha = Ext.util.Format.date(avisoCargo.data.fechaEmision);
            
            this.lookup('fechaEmisionAvisoCargo').setValue(fecha);
            this.lookup('saldoAvisoCargo').setValue(saldo);
        }
    },
    
    onSeleccionBanco(selector, bancoId) {
        this.bancoId = bancoId;        
        this.lookup('cuentaBancoSelector').getStore().load({
            params: {
                bancoId
            }
        });
    },

    onSeleccionCuentaBanco(selector, cuentaBancoId){
        this.cuentaBancoId = cuentaBancoId;
    },

    onDefineBancoEmisor(componente, bancoEmisor, oldValue, eOpts){
        this.bancoEmisor = bancoEmisor;
    },

    onDefineCheque(componente, numeroCheque, oldValue, eOpts) {
        this.numeroCheque = numeroCheque;
    },
    
    onSeleccionDependencia(component, dependenciaId) {
        let selector = this.lookup('emisionAvisoCargoSelector');
        selector.getStore().getProxy().setExtraParams({
            dependenciaId: dependenciaId
        });
        selector.getStore().load();
    },

    onSeleccionFormaPago(selector, formaPagoId){
        this.formaPagoId = formaPagoId;
    },

    validaImporte(val) {
        let edoCta = this.ownerCt.editingPlugin.estadoDeCuenta;
        let saldo = edoCta.get('saldo');
        if (val > saldo) {
            return `El importe a pagar no puede ser mayor a 
            ${Ext.util.Format.usMoney(saldo)}`;
        }
        return true;
    },

    onBeforeEdit(editor, context) {
        editor.estadoDeCuenta = context.record;
    },

    onValidateEdit(editor, context) {
        let saldo = context.record.get('saldo');
        if (context.value > saldo) {
            Ext.Msg.alert(
                'Importe incorrecto',
                `El importe a pagar no puede ser mayor a ${Ext.util.Format.usMoney(saldo)}`
            );
        }
    },

    onSummary(value, summaryData, dataIndex) {
        if(value !== undefined && value > 0){
            this.sumaTotal = Math.round(value * 100) / 100;
        }
        return `<b>Total a pagar: ${Ext.util.Format.usMoney(value)}</b>`;
    },

    imprimirCertificado(transaccionId) {
        window.open(`/api/ingresos/certificado/descargar?transaccionId=${transaccionId}`);
    }

});