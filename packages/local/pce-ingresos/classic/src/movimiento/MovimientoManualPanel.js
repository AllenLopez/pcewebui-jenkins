Ext.define('Pce.ingresos.movimiento.MovimientoManualPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'movimiento-manual-panel',
    reference: 'movimientoManualPanel',

    requires: [
        'Pce.ingresos.movimiento.forma.MovimientoFormaContenedor',
        'Pce.ingresos.movimiento.MovimientoManualDependenciaForma',
        'Pce.ingresos.avisocargo.EmisionAvisoCargoStore',
        'Pce.ingresos.movimiento.MovimientoManualController'
    ],

    controller: 'movimiento-manual-controller',

    title: 'captura de movimiento manual',

    viewModel: {
        data: {
            tipoForma: false
        }
    },
    
    items: [{
        xtype: 'container',
        items: [{
            xtype: 'combobox',
            name: 'movimiento',
            reference: 'movimientoSelector',
            fieldLabel: 'Movimiento por',
            labelWidth: 110,
            allowBlank: false,
            flex: 3,
            margin: 5,
            store: {
                fields: ['valor', 'display'],
                data : [
                    {'valor': 'D', 'display': 'DEPENDENCIA'},
                    {'valor': 'A', 'display': 'AFILIADO'}
                ]
            },
            queryMode: 'local',
            displayField: 'display',
            valueField: 'valor',
            bind: {
                value: '{tipoForma}'
            }
        }, {
            xtype: 'movimiento-forma-contenedor',
            hidden: true,
            bind: {
                hidden: '{tipoForma != "A"}'
            }
        }, {
            xtype: 'movimiento-manual-dependencia-forma',
            hidden: true,
            bind: {
                hidden: '{tipoForma != "D"}'
            }
        }],
    }]
});