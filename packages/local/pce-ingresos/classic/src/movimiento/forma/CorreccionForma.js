Ext.define('Pce.ingresos.movimiento.forma.CorreccionForma', {
    extend: 'Ext.form.Panel',
    xtype: 'movimiento-correccion-forma',

    requires: [
        'Pce.ingresos.concepto.ConceptoSelector',
        'Pce.afiliacion.asegurado.AseguradoSelector'
    ],

    defaultType: 'container',
    defaults: {
        width: '100%',
        layout: {
            type: 'hbox',
            align: 'stretch'
        }
    },
    style: 'padding: 10px',
    items: [{
        style: 'margin-bottom: 10px',
        items: [{
            xtype: 'asegurado-selector',
            name: 'numeroAfiliacionOrigen',
            fieldLabel: 'Número afil. incorrecto',
            labelWidth: 150,
            style: 'margin-right: 10px',
            flex: 1,
            displayTpl: [
                '<tpl for=".">',
                    '{numeroAfiliacion}',
                '</tpl>'
            ]
        }, {
            xtype: 'asegurado-selector',
            name: 'numeroAfiliacionDestino',
            fieldLabel: 'Número afil. correcto',
            labelWidth: 150,
            flex: 1,
            displayTpl: [
                '<tpl for=".">',
                    '{numeroAfiliacion}',
                '</tpl>'
            ],
        }]
    }, {
        defaults: {style: 'margin-right: 10px;'},
        items: [{
            xtype: 'concepto-selector',
            name: 'conceptoOrigen',
            fieldLabel: 'Del concepto',
            labelWidth: 90,
            flex: 2
        }, {
            xtype: 'concepto-selector',
            name: 'conceptoDestino',
            flex: 2,
            fieldLabel: 'Al concepto',
            labelWidth: 90,
        }, {
            xtype: 'textfield',
            name: 'importe',
            fieldLabel: 'Importe',
            allowBlank: false,
            flex: 1,
            style: 'margin-right: 0px;'
        }]
    }]
});