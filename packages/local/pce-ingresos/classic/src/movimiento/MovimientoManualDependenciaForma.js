Ext.define('Pce.ingresos.movimiento.MovimientoManualDependenciaForma', {
    extend: 'Ext.form.Panel',
    xtype: 'movimiento-manual-dependencia-forma',
    reference: 'movimientoManualDependenciaForma',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.Text',
        'Ext.form.field.ComboBox',
        'Ext.form.RadioGroup',

        'Pce.ingresos.movimiento.PagoManualConceptosTabla',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.ingresos.avisocargo.EmisionAvisoCargoSelector',

        'Pce.ingresos.formaPago.FormaPagoStore',
        // 'Pce.caja.banco.BancoStore',
        // 'Pce.caja.banco.CuentaBancoStore'
    ],

    viewModel: {
        data: {
            cuentaBancaria: null
        }
    },

    items: [{
        xtype: 'container',
        items: [{
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'hiddenfield',
                name: 'tipo',
                value: 'D'
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'dependencia-selector',
                reference: 'dependenciaSelector',
                name: 'dependencia',
                flex: 4,
                margin: 5,
                labelWidth: 120,
                listeners: {
                    change: 'onSeleccionDependencia'
                }
            }, {
                xtype: 'emision-avisocargo-selector',
                reference: 'emisionAvisoCargoSelector',
                name: 'avisocargo',
                emptyText: 'Seleccione aviso de cargo',
                labelWidth: 170,
                flex: 2,
                margin: 5,
                listeners: {
                    change: 'onSeleccionAvisoCargo'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{ 
                xtype: 'textfield',
                reference: 'fechaEmisionAvisoCargo',
                fieldLabel: 'Fecha de emision',
                name: 'fecha',
                readOnly: true,
                labelWidth: 120,
                margin: 5
            }, {
                xtype: 'textfield',
                name: 'saldo',
                reference: 'saldoAvisoCargo',
                fieldLabel: 'Saldo inicial',
                readOnly: true,
                labelWidth: 115,
                margin: 5
            }, {
                xtype: 'combobox',
                name: 'formaPagoId',
                fieldName: 'formaPagoId',
                fieldLabel: 'Forma de Pago',
                valueField: 'id',
                displayField: 'descripcion',
                forceSelection: true,
                emptyText: 'Seleccione forma de pago',
                queryMode: 'local',
                labelWidth: 120,
                margin: 5,
                store: {type: 'forma-pago', autoLoad: true},
                listeners:{
                    change: 'onSeleccionFormaPago'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'textfield',
                name: 'bancoEmisor',
                reference: 'bancoEmisor',
                fieldLabel: 'Banco Emisor',
                labelWidth: 120,
                margin: 5,
                listeners: {
                    change: 'onDefineBancoEmisor'
                }
            }, {
                xtype: 'textfield',
                name: 'cheque',
                reference: 'cheque',
                fieldLabel: 'Num. referencia',
                labelWidth: 115,
                margin: 5,
                listeners: {
                    change: 'onDefineCheque'
                }
            }, {
                xtype: 'combobox',
                name: 'banco',
                fieldName: 'BancoId',
                fieldLabel: 'Banco Receptor',
                valueField: 'id',
                displayField: 'bancoNombre',
                emptyText: 'Seleccione banco',
                queryMode: 'local',
                labelWidth: 120,
                margin: 5,
                forceSelection: true,
                store: {type: 'banco', autoLoad: true},
                listeners: {
                    change: 'onSeleccionBanco'
                }
            }, {
                xtype: 'combobox',
                reference: 'cuentaBancoSelector',
                name: 'numeroCuenta',
                fieldName: 'numeroCuenta',
                fieldLabel: 'Cuenta',
                valueField: 'id',
                displayField: 'numeroCuenta',
                emptyText: 'Seleccione cuenta',
                queryMode: 'local',
                allowBlank: false,
                forceSelection: true,
                margin: 5,
                store: {
                    type: 'cuenta-banco',
                    autoLoad: true,
                    proxy: {
                        type: 'rest',
                        url: '/api/caja/cuenta-banco/por-banco'
                    }
                },
                listeners: {
                    change: 'onSeleccionCuentaBanco'
                },
                bind: {
                    selection: '{cuentaBancaria}'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'numberfield',
                name: 'montoCheque',
                reference: 'montoCheque',
                fieldLabel: 'Monto de cheque',
                allowBlank: false,
                labelWidth: 120,
                maxValue: 999999999,
                width: 200,
                flex: 1,
                margin: 5
            }]
        }]
    }, {
        items: [{
            xtype: 'pago-manual-concepto-tabla',
            reference: 'tablaEstadosDeCuenta',
            bind: {
                disabled: '{!cuentaBancaria}'
            }
        }]
    }],

    buttons: [ {
        text: 'Procesar Movimiento',
        reference: 'botonProcesar',
        handler: 'onProcesarPago',
        bind: {
            disabled: '{!tipoForma}'
        }
    }]
});