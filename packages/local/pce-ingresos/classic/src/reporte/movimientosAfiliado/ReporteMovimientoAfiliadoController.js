/**
 * @class Pce.ingresos.reporte.movimientoAfiliado.ReporteMovimientoAfiliadoPanel
 * @extends Ext.app.ViewController
 * @xtype controller.reporte-movimiento-afil
 * Controlador de panel de captura de afiliado para ejecutar reporte de movimientos de afiliado
 *
 * @author Jorge Escamilla
 */
Ext.define('Pce.ingresos.reporte.movimientoAfiliado.ReporteMovimientoAfiliadoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reporte-movimiento-afil',

    conceptosIds: 0,

    onAseguradoChange(selector) {
        let asegurado = selector.getSelection();
        if (asegurado) {
            asegurado.getInfo().then(info => {
                this.getViewModel().set('aseguradoInfo', info);
            });
        }
    },

    onChangeConceptos(comp, values) {
        this.conceptosIds = values;
    },

    onTodosCheck(element, opt) {
        if(opt) {
            this.setViewModelDataProp('conceptosIds', null);
        }
    },

    onEjecutarReporte() {
        let asegurado = this.getViewModelDataProp('afiliado');
        let fechaInicial = this.getViewModelDataProp('fechaInicial');
        let fechaFinal = this.getViewModelDataProp('fechaFinal');

        console.log(this.conceptosIds);

        this.mostrarReporte(asegurado.getId(), fechaInicial, fechaFinal, this.conceptosIds);
    },

    mostrarReporte(aseguradoId, fechaInicial, fechaFinal, conceptos) {
        window.open(`/api/ingresos/reporte/reporte-movimiento-afil?aseguradoId=${aseguradoId}&fechaInicial=${fechaInicial}&fechaFinal=${fechaFinal}&conceptos=${conceptos.toString()}`);
    },

    getViewModelDataProp(prop) {
        let vm = this.obtieneViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },

    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },

    obtieneViewModel() {
        let vm = this.getViewModel();
        if(vm) {
            return vm;
        }
    },
});

