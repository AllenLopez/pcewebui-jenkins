/**
 * @class Pce.ingresos.reporte.movimientoAfiliado.ReporteMovimientoAfiliadoPanel
 * @extends Ext.panel.Panel
 * @xtype creporte-movimiento-afil
 * Formulario para ejecutar reporte de movimientos de afiliado
 *
 * @author Jorge Escamilla
 */

const getCrrntYear = () => {
    return new Date().getFullYear();
};

const getEjercicios = () => {
    let val = [];
    for (let i = 1900; i <= getCrrntYear(); i++) {
        val.push({ 'ejercicio': i });
    }
    return val;
};

const ejerciciosLocal = Ext.create('Ext.data.Store', {
    alias: 'store.excercices',
    fields: ['ejercicio'],
    data: getEjercicios(),
    sorters: [{
        property: 'ejercicio',
        direction: 'DESC'
    }]
});

Ext.define('Pce.ingresos.reporte.movimientoAfiliado.ReporteMovimientoAfiliadoPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'reporte-movimiento-afil-panel',

    requires: [
        'Pce.ingresos.reporte.movimientoAfiliado.ReporteMovimientoAfiliadoController',
        'Pce.afiliacion.asegurado.AseguradoSelector'
    ],

    title: 'Reporte de movimientos de afiliado',

    bodyPadding: 10,
    defaults: {
        width: '100%'
    },

    layout: {
        type: 'table',
        columns: 6
    },

    controller: 'reporte-movimiento-afil',

    viewModel: {
        data: {
            afiliado: null,
            aseguradoInfo: null,
            fechaInicial: null,
            fechaFinal: null,
            todos: true,
            conceptosIds: 0
        }
    },

    items: [{
        xtype: 'asegurado-selector',
        name: 'asegurado',
        reference: 'aseguradoSelector',
        allowBlank: false,
        listeners: {
            change: 'onAseguradoChange'
        },
        bind: {
            selection: '{afiliado}'
        },
        colspan: 6
    }, {
        xtype: 'asegurado-displayer',
        padding: '10 0',
        colspan: 6
    }, {
        xtype: 'combobox',
        reference: 'fechaReporte',
        name: 'fechaInicial',
        fieldLabel: 'A partir de',
        displayField: 'ejercicio',
        valueField: 'ejercicio',
        allowBlank: false,
        store: ejerciciosLocal,
        bind: {
            value: '{fechaInicial}'
        },
        colspan: 1
    }, {
        xtype: 'combobox',
        reference: 'fechaReporte',
        name: 'fechaFinal',
        fieldLabel: 'Hasta',
        displayField: 'ejercicio',
        valueField: 'ejercicio',
        allowBlank: false,
        store: ejerciciosLocal,
        bind: {
            value: '{fechaFinal}'
        },
        colspan: 1
    }, {
        xtype: 'checkbox',
        name: 'todos',
        fieldLabel: 'Todos los conceptos',
        labelWidth: 130,
        checked: true,
        listeners: {
            change: 'onTodosCheck'
        },
        bind: {
            value: '{todos}'
        },
        colspan: 1
    }, {
        xtype: 'concepto-selector-multiple',
        name: 'conceptos',
        reference: 'conceptoSelector',
        fieldLabel: 'Conceptos',
        displayField: 'descripcion',
        labelWidth: 100,
        height: 100,
        growMax: 150,
        autoScroll: true,
        store: {
            type: 'conceptos',
            autoLoad: true,
            filters: [{
                property: 'clave',
                operator: 'in',
                value: ['A','H','I']
            }]
        },
        bind: {
            disabled: '{todos}',
        },
        listeners: {
            change: 'onChangeConceptos'
        },
        colspan: 3
    }],

    buttons: [{
        text: 'Generar archivo',
        bind: {
            disabled: '{!asegurado || !fecha}'
        },
        handler: 'onEjecutarReporte'
    }]
});