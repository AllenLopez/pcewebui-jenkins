Ext.define('Pce.ingresos.reporte.dependencia.ReporteDependenciaPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reporte-dependencia-panel',

    requires: [

    ],

    data: {
        conceptosIds: 0,
        dependenciasIds: 0,
        fecha: undefined,
        desde: undefined,
        hasta: undefined,
        tipo: undefined,
    },

    init() {
        // console.log("reporteafiliadoCtrl INIT")
    },

    onGenerarArchivoExcel(a, b, c) {
        if (!this.validarCampos()) {
            return;
        }
        this.onGenerarReporte();
    },

    validarCampos() {
        let me = this;
        let cantidadDependencia = me.lookup('cantidadDependencia');
        let cantidadConcepto = me.lookup('cantidadConcepto');
        if (this.data.tipo == 1) {
            if (this.data.fecha == null || this.data.fecha == "") {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar una fecha');
                return false;
            }
        } else {
            if (((this.data.desde == null || this.data.desde == "") || (this.data.hasta == null || this.data.hasta == "")) && (this.data.fecha == null || this.data.fecha == "")) {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar una fecha o un rango de fechas');
                return false;
            }
        }
        if (cantidadDependencia.getValue() == 2) {
            if (this.data.dependenciasIds == null) {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar minimo una dependencia');
                return false;
            }
            if (this.data.dependenciasIds.length == 0) {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar minimo una dependencia');
                return false;
            }
        }
        if (cantidadConcepto.getValue() == 2) {
            if (this.data.conceptosIds == null) {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar minimo un concepto');
                return false;
            }
            if (this.data.conceptosIds.length == 0) {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar minimo un concepto');
                return false;
            }
        }


        return true;
    },

    onChangeConceptos(comp, values) {
        this.data.conceptosIds = values;
    },

    onChangeDependencias(comp, values) {
        this.data.dependenciasIds = values;
    },


    onChangeCantidadDependencias(comp, valor) {
        let me = this;
        let selector = me.lookup('dependenciaSelector');
        if (valor == 2) {
            selector.setDisabled(false);
            this.data.dependenciasIds = null;
        } else {
            selector.setDisabled(true);
            selector.setValue(null);
            this.data.dependenciasIds = 0;
        }
    },

    onChangeCantidadConceptos(comp, valor) {
        let me = this;
        let selector = me.lookup('conceptoSelector');
        if (valor == 2) {
            selector.setDisabled(false);
            this.data.conceptosIds = null;
        } else {
            selector.setDisabled(true);
            selector.setValue(null);
            this.data.conceptosIds = 0;
        }
    },

    onSeleccionFecha(comp, valor) {
        if ((this.data.hasta != null && this.data.hasta != '') || (this.data.desde != null && this.data.desde != '')) {
            comp.setValue(null);
            Ext.Msg.alert('Advertencia!', 'Ya se encuentra asignado un rango de fecha');
            return;
        }
        if (valor == null) {
            this.data.fecha = null;
            return;

        }
        this.data.fecha = Ext.util.Format.date(valor, 'd/m/Y');

    },

    onSeleccionFechaDesde(comp, valor) {
        if (this.data.fecha != null && this.data.fecha != '') {
            comp.setValue(null);
            Ext.Msg.alert('Advertencia!', 'Ya se encuentra asignado una fecha');
            return;
        }
        if (valor == null) {
            this.data.desde = null;
            return;

        }
        this.data.desde = Ext.util.Format.date(valor, 'd/m/Y');
    },

    onSeleccionFechaHasta(comp, valor) {
        if (this.data.fecha != null && this.data.fecha != '') {
            comp.setValue(null);
            Ext.Msg.alert('Advertencia!', 'Ya se encuentra asignado una fecha');
            return;
        }
        if (valor == null) {
            this.data.hasta = null;
            return;

        }
        this.data.hasta = Ext.util.Format.date(valor, 'd/m/Y');
    },

    onSeleccionTipoReporte(comp, valor) {
        this.data.tipo = valor;
        let me = this;
        let fechaDesde = me.lookup('fechaDesde');
        let fechaHasta = me.lookup('fechaHasta');
        if (valor == 1) {
            fechaDesde.setDisabled(true);
            fechaHasta.setDisabled(true);
            fechaDesde.setValue(null);
            fechaHasta.setValue(null);
            this.data.desde = null;
            this.data.hasta = null;
        } else {
            fechaDesde.setDisabled(false);
            fechaHasta.setDisabled(false);
        }

    },

    onGenerarReporte() {
        let reportes = ['IN_EDOCTA_DEP_EXCEL', 'IN_MOVTOS_DEP_EXCEL'];
        let PINC = this.data.conceptosIds != 0 ? JSON.stringify(this.data.conceptosIds) : null;
        let PIND = this.data.dependenciasIds != 0 ? JSON.stringify(this.data.dependenciasIds) : null;
        let tipoReporte = this.data.tipo;

        if(tipoReporte == 1){
            if (PINC != null) {
                PINC = PINC.substring(1, PINC.length - 1);
                PINC = `AND e.id_cat_concepto IN (${PINC})`;
            }
            if (PIND != null) {
                PIND = PIND.substring(1, PIND.length - 1);
                PIND = `AND e.num_dependencia IN (${PIND})`;
            }
        }

        if(tipoReporte == 2){
            if (PINC != null) {
                PINC = PINC.substring(1, PINC.length - 1);
                PINC = `AND d.id_cat_concepto IN (${PINC})`;
            }
            
            if (PIND != null) {
                PIND = PIND.substring(1, PIND.length - 1);
                PIND = `AND d.num_dependencia IN (${PIND})`;
            }
        }
        
        let data = {
            PFECHA: this.data.fecha,
            PFECHA1: this.data.desde,
            PFECHA2: this.data.hasta,
            PINC,
            PIND
        }
        let items = [{
            xtype: 'hiddenfield',
            name: 'reporte',
            value: reportes[this.data.tipo - 1]
        }];
        let i = 0;
        for (const prop in data) {
            items.push({
                xtype: 'hiddenfield',
                name: `parametros[${i}][Nombre]`,
                value: prop
            });
            items.push({
                xtype: 'hiddenfield',
                name: `parametros[${i}][Valor]`,
                value: data[prop]
            });
            i++;
        }

        let tempForm = Ext.create('Ext.form.Panel', {
            url: '/api/ingresos/reporte/reporte-afiliado',
            items,
            standardSubmit: true
        });
        tempForm.submit();
    }



});