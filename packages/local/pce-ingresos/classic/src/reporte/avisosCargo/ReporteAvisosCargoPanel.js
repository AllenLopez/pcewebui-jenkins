Ext.define('Pce.ingresos.reporte.avisosCargo.ReporteAvisosCargoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte-avisos-cargo-panel',

    title: 'Reporte de avisos de cargo',

    layout: { type: 'hbox' },

    items: [{
        xtype: 'toolbar',
        items: [{
            xtype: 'datefield',
            reference: 'fechaNomina',
            name: 'fechaNomina',
            labelWidth: 100,
            fieldLabel: 'Fecha nómina',
            width: 300,
            allowBlank: false,
        }, {
            text: 'Generar archivo excel',
            handler() {
                let fecha = Ext.util.Format.date(this.ownerCt.getRefItems()[0].getValue(), 'd/m/Y');
                console.log(typeof(fecha));
                if(fecha.length > 0) {
                    window.open(`/api/ingresos/reporte/reporte-avisos-cargo?pfechanomina=${fecha}`);
                } else {
                    Ext.Msg.alert('Seleccione fecha', 'Debe seleccionar fecha quincenal, revise por favor');
                    this.ownerCt.getRefItems()[0].focus();
                }
            }
        }]
    }]
});