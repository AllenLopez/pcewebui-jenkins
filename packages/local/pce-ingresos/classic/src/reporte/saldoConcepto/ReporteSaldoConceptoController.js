Ext.define('Pce.ingresos.reporte.saldoConcepto.ReporteSaldoConceptoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reporte-saldo-concepto',

    data: {
        dependenciasIds: 0,
        fecha: undefined,
    },

    init() {
        // console.log("reporteafiliadoCtrl INIT")
    },

    onGenerarArchivoTxt(a, b, c) {
        if (!this.validarCampos()) {
            return;
        }
        this.onGenerarReporte();
    },

    validarCampos() {
        let me = this;
        let cantidadDependencia = me.lookup('cantidadDependencia');
        if (this.data.tipo == 1) {
            if (this.data.fecha == null || this.data.fecha == "") {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar una fecha');
                return false;
            }
        } 
        if (cantidadDependencia.getValue() == 2) {
            if (this.data.dependenciasIds == null) {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar minimo una dependencia');
                return false;
            }
            if (this.data.dependenciasIds.length == 0) {
                Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar minimo una dependencia');
                return false;
            }
        }
        return true;
    },
    onChangeDependencias(comp, values) {
        this.data.dependenciasIds = values;
    },
    onChangeCantidadDependencias(comp, valor) {
        let me = this;
        let selector = me.lookup('dependenciaSelector');
        if (valor == 2) {
            selector.setDisabled(false);
            this.data.dependenciasIds = null;
        } else {
            selector.setDisabled(true);
            selector.setValue(null);
            this.data.dependenciasIds = 0;
        }
    },
    onSeleccionFecha(comp, valor) {
        if (valor == null) {
            this.data.fecha = null;
            return;

        }
        this.data.fecha = Ext.util.Format.date(valor, 'd/m/Y');
    },
    onGenerarReporte() {
        let reportes = ['IN_SALDO_CONCEPTO_FECHA_DETERMINADA'];
        let PIND = this.data.dependenciasIds != 0 ? JSON.stringify(this.data.dependenciasIds) : null;

            if (PIND != null) {
                PIND = PIND.substring(1, PIND.length - 1);
                PIND = `AND d.num_dependencia IN (${PIND})`;
            }
        
        let data = {
            P_FECHA: this.data.fecha,
            PIND
        }
        let items = [{
            xtype: 'hiddenfield',
            name: 'reporte',
            value: reportes
        }];
        let i = 0;
        for (const prop in data) {
            items.push({
                xtype: 'hiddenfield',
                name: `parametros[${i}][Nombre]`,
                value: prop
            });
            items.push({
                xtype: 'hiddenfield',
                name: `parametros[${i}][Valor]`,
                value: data[prop]
            });
            i++;
        }

        let tempForm = Ext.create('Ext.form.Panel', {
            url: '/api/ingresos/reporte/reporte-afiliado',
            items,
            standardSubmit: true
        });
        tempForm.submit();
        let fecha = this.lookup('fecha');
        let dependencias = this.lookup('cantidadDependencia');
        let depseleccion = this.lookup('dependenciaSelector');
        fecha.reset();
        dependencias.reset();
        depseleccion.reset();
    }

});