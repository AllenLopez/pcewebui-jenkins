Ext.define('Pce.ingresos.reporte.adeudoDependencia.ReporteAdeudoDependenciaPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte-adeudo-dependencia-panel',

    title: 'Reporte de adeudo de dependencias',

    layout: { type: 'hbox' },

    items: [{
        xtype: 'toolbar',
        items: [{
            xtype: 'datefield',
            reference: 'fechaRep',
            name: 'fecha',
            labelWidth: 50,
            fieldLabel: 'Fecha',
            width: 300,
            allowBlank: false,
            // value: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)
            value: new Date()
        }, {
            text: 'Generar archivo excel',
            handler() {
                let fecha = Ext.util.Format.date(this.ownerCt.getRefItems()[0].getValue(), 'd/m/Y');

                window.open(`/api/ingresos/reporte/reporte-adeudo-dep?pfecha=${fecha}`);
            }
        }]
    }]
});