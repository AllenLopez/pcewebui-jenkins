Ext.define('Pce.ingresos.reporte.ReportePanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte-panel',

    requires: [
        'Pce.ingresos.reporte.ReporteSelector',

        'Pce.ingresos.reporte.forma.NominaPorDependencia',
        'Pce.ingresos.reporte.forma.ConfiguracionAvisoCargo'
    ],

    defaultListenerScope: true,
    title: 'Generación de reportes',
    bodyPadding: 10,

    formaMap: {
        'IN_INGRE_NOMINA_DEP': 'reporte-nomina-por-dependencia',
        'IN_PARAMETROS_GPO_AC_DEP': 'reporte-configuracion-aviso-cargo'
    },

    tbar: [{
        xtype: 'reporte-selector',
        width: '50%',
        listeners: {
            select: 'onReporteSelect'
        }
    }],

    onReporteSelect(selector, reporte) {
        this.reporte = reporte;
        let forma = this.formaMap[reporte.get('nombre')];
        this.removeAll(true);
        this.add({
            xtype: forma,
            title: reporte.get('nombreSalida'),
            bodyPadding: 10,
            buttons: [{
                text: 'Generar',
                handler: 'onGenerarReporte'
            }]
        });
    },

    onGenerarReporte() {
        let form = this.down('form');
        if (!form.isValid()) {
            return;
        }

        let fields = form.getForm().getFields();
        let items = [{
            xtype: 'hiddenfield',
            name: 'reporte',
            value: this.reporte.get('nombre')
        }];

        fields.each((field, i) => {
            items.push({
                xtype: 'hiddenfield',
                name: `parametros[${i}][Nombre]`,
                value: field.getName()
            });
            items.push({
                xtype: 'hiddenfield',
                name: `parametros[${i}][Valor]`,
                value: field.getValue()
            });
        });

        let tempForm = Ext.create('Ext.form.Panel', {
            url: '/api/ingresos/reporte/generar',
            items: items,
            standardSubmit: true
        });
        tempForm.submit();
    }
});