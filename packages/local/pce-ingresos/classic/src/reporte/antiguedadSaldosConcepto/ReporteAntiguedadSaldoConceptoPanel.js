let seleccion = Ext.create('Ext.data.Store', {
    fields: ['tipo', 'id'],
    data: [
        { 'tipo': 'Todos', 'id': 1 },
        { 'tipo': 'Algunos', 'id': 2 }
    ]
});

Ext.define('Pce.ingresos.reporte.antiguedadSaldosConcepto.ReporteAntiguedadSaldoConceptoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte-antiguedad-saldo-concepto-panel',

    requires: [
        'Pce.ingresos.reporte.saldoConcepto.ReporteSaldoConceptoController',
        'Pce.afiliacion.dependencia.DependenciaSelectorMultiple',
        'Pce.ingresos.concepto.ConceptoSelectorMultiple',
        'Pce.afiliacion.dependencia.DependenciaStore',
        'Pce.ingresos.concepto.ConceptoStore',
    ],

    controller: 'reporte-antiguedad-saldo-concepto',

    viewModel: {
        data: {
            tipo: null,
            tipoDependencia: null,
            tipoConcepto: null,
            cantidadDep: null,
            cantidadCon: null
        }
    },

    autoScroll: true,


    title: 'Antigüedad de saldos por concepto - dependencia',
    bodyPadding: 10,

    formaMap: {
        'IN_INGRE_NOMINA_DEP': 'reporte-nomina-por-dependencia',
        'IN_PARAMETROS_GPO_AC_DEP': 'reporte-configuracion-aviso-cargo'
    },

    defaults: {
        style: 'margin: 5px',
    },

    items: [{
        xtype: 'container',
        layout: 'column',
        items: [{
            columnWidth: 0.33,
            items: [{
                xtype: 'datefield',
                name: 'fecha',
                reference: 'fecha',
                fieldLabel: 'Fecha',
                listeners: {
                    change: 'onSeleccionFecha'
                },
                width: '90%'
            }]
        }]
    },{
        items: [{
            columnWidth: 0.33,
            items: [{
                xtype: 'combobox',
                emptyText: 'Seleccione uno',
                fieldLabel: 'Dependencias',
                store: seleccion,
                displayField: 'tipo',
                valueField: 'id',
                value: 1,
                reference: 'cantidadDependencia',
                width: '90%',
                listeners: {
                    change:'onChangeCantidadDependencias'
                }
            }]
        }, {
            columnWidth: 0.63,
            items: [{
                xtype: 'dependencia-selector-multiple',
                displayField: 'descripcion',
                width: '90%',
                height: 100,
                growMax: 150,
                reference: 'dependenciaSelector',
                autoScroll: true,
                disabled: true,
                store: Ext.create('Pce.afiliacion.dependencia.DependenciaStore', {
                    autoLoad: true
                }),
                listeners: {
                    change:'onChangeDependencias'
                }
            }]
        }]
    }, {
        xtype: 'container',
        layout: 'column',
        bind: {
            disabled: '{!tipo}'
        },
    }],

    buttons: [{
        text: 'Generar archivo Excel',
        handler:  'onGenerarArchivoExcel'
    }]

});