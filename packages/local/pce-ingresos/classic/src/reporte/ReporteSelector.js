Ext.define('Pce.ingresos.reporte.ReporteSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'reporte-selector',

    requires: [
        'Pce.ingresos.reporte.ReporteStore'
    ],

    fieldLabel: 'Reporte',
    valueField: 'nombre',
    displayField: 'nombreSalida',
    queryMode: 'local',

    store: {
        type: 'reportes',
        autoLoad: true
    }
});