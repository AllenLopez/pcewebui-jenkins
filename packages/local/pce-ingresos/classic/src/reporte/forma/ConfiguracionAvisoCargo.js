Ext.define('Pce.ingresos.reporte.forma.ConfiguracionAvisoCargo', {
    extend: 'Ext.form.Panel',
    xtype: 'reporte-configuracion-aviso-cargo',

    requires: [
        'Pce.afiliacion.dependencia.DependenciaSelector'
    ],

    items: [{
        xtype: 'dependencia-selector',
        name: 'DEPEN_MOV',
        width: 500
    }]
});