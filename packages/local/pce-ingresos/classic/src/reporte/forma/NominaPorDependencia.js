Ext.define('Pce.ingresos.reporte.forma.NominaPorDependencia', {
    extend: 'Ext.form.Panel',
    xtype: 'reporte-nomina-por-dependencia',

    requires: [
        'Pce.ingresos.transaccion.TransaccionStore'
    ],

    items: [{
        xtype: 'combobox',
        name: 'PTRANSACCION',
        fieldLabel: 'Transacción',
        valueField: 'id',
        displayField: 'id',
        queryMode: 'local',
        store: {
            type: 'transacciones',
            autoLoad: true,
            filters: [{
                property: 'origenClave',
                value: 'NOMINA'
            }]
        },
        allowBlank: false,
        blankText: 'Por favor selecciona una transacción'
    }]
});