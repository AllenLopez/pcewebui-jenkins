/**
 * @class Pce.reporte.afiliado.ReporteAportacionFondoPorPagarPanel
 * @extends Ext.panel.Panel
 * @xtype reporte-aportacion-fondo-pagar
 * Panel de ejecución y descarga de reporte de aportaciones al fondo por pagar para afiliados
 */

Ext.define('Pce.reporte.afiliado.ReporteAportacionFondoPorPagarPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte-aportacion-fondo-pagar',

    require: [
        'Pce.reporte.afiliado.ReporteAportacionFondoPorPagarController'
    ],

    controller: 'reporte-aportacion-fondo-pagar',

    viewModel: {
        data: {
            fecha: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
            periodoInicio: 4.5,
            periodoFinal: 4.5
        }
    },

    title: 'Aportaciones al fondo por pagar',

    layout: { type: 'vbox' },

    items: [{
        xtype: 'toolbar',
        items: [{
            xtype: 'datefield',
            reference: 'fechaRep',
            name: 'fecha',
            labelWidth: 250,
            fieldLabel: 'Fecha última actualización de nómina',
            flex: 1,
            allowBlank: false,
            bind: {
                value: '{fecha}'
            }
        }, {
            xtype: 'numberfield',
            reference: 'periodoInicio',
            name: 'periodoInicio',
            labelWidth: 100,
            fieldLabel: 'Con más de',
            flex: 1,
            allowBlank: false,
            bind: {
                value: '{periodoInicio}'
            }
        }, {
            xtype: 'numberfield',
            reference: 'periodoFinal',
            name: 'periodoFinal',
            labelWidth: 50,
            fieldLabel: 'A',
            flex: 1,
            allowBlank: false,
            bind: {
                minValue: '{periodoInicio}',
                value: '{periodoFinal}'
            }
        }, {
            text: 'Generar reporte',
            handler: 'onGenerarReporteTap'
        }]
    }]
});