/**
 * @class Pce.reporte.afiliado.ReporteAportacionFondoPorPagarController
 * @extends Ext.app.ViewController
 * @alias reporte-aportacion-fondo-pagar
 * Panel de ejecución y descarga de reporte de aportaciones al fondo por pagar para afiliados
 */

Ext.define('Pce.reporte.afiliado.ReporteAportacionFondoPorPagarController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reporte-aportacion-fondo-pagar',

    onGenerarReporteTap() {
        Ext.getBody().mask('Generando...');
        let { fecha, periodoFinal, periodoInicio } = this.getViewModel().getData();
        fecha = Ext.util.Format.date(fecha, 'd/m/Y');

        console.log(fecha);
        console.log(periodoFinal);
        console.log(periodoInicio);

        if (periodoInicio > periodoFinal) {
            Ext.Msg.alert(
                'Alerta',
                `No es posible tener un periodo final mayor al inicial`);
            return;
        }

        window.open(`/api/ingresos/reporte/reporte-fondo-por-pagar?fecha=${fecha}&periodoInicio=${periodoInicio}&periodoFinal=${periodoFinal}`);

        Ext.getBody().unmask();
    }
});