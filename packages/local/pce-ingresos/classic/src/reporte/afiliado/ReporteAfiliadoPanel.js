let tiposReporte = Ext.create('Ext.data.Store', {
    fields: ['tipo', 'id'],
    data: [
        { 'tipo': 'Estado de cuenta', 'id': 1 },
        { 'tipo': 'Movimientos afiliado', 'id': 2 }
    ]
});
let cantidad = Ext.create('Ext.data.Store', {
    fields: ['tipo', 'id'],
    data: [
        { 'tipo': 'Todos', 'id': 1 },
        { 'tipo': 'Algunos', 'id': 2 }
    ]
});

Ext.define('Pce.ingresos.reporte.afiliado.ReporteAfiliadoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte-afiliado-panel',

    requires: [
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.afiliacion.asegurado.AseguradoSelectorMultiple',
        'Pce.afiliacion.dependencia.DependenciaSelectorMultiple',
        'Pce.ingresos.concepto.ConceptoSelectorMultiple',
        'Pce.afiliacion.dependencia.DependenciaStore',
        'Pce.ingresos.concepto.ConceptoStore',
    ],

    controller: 'reporte-afiliado-panel',

    viewModel: {
        data: {
            tipo: null,
            tipoAfiliado: null,
            tipoDependencia: null,
            tipoConcepto: null,
            cantidadAfil: null,
            cantidadDep: null,
            cantidadCon: null
        }
    },

    autoScroll: true,


    title: 'Reporte Afiliado',
    bodyPadding: 10,

    formaMap: {
        'IN_INGRE_NOMINA_DEP': 'reporte-nomina-por-dependencia',
        'IN_PARAMETROS_GPO_AC_DEP': 'reporte-configuracion-aviso-cargo'
    },

    defaults: {
        style: 'margin: 5px',
    },

    items: [{
        xtype: 'container',
        layout: 'column',
        items: [{
            columnWidth: 0.33,
            items: [{
                xtype: 'combobox',
                emptyText: 'Selecciona un tipo de reporte',
                reference: 'tipoReporte',
                fieldLabel: 'Tipo reporte',
                store: tiposReporte,
                displayField: 'tipo',
                bind: {
                    selection: '{tipo}'
                },
                valueField: 'id',
                listeners: {
                    change: 'onSeleccionTipoReporte'
                },
                width: '90%'
            }]
        }]
    }, {
        xtype: 'container',
        layout: 'column',
        bind: {
            disabled: '{!tipo}'
        },
        items: [{

            columnWidth: 0.33,
            items: [{
                xtype: 'datefield',
                name: 'fecha',
                fieldLabel: 'A la fecha',
                listeners: {
                    change: 'onSeleccionFecha'
                },
                width: '90%'
            }]
        }]
    }, {
        xtype: 'container',
        layout: 'column',
        bind: {
            disabled: '{!tipo}'
        },
        items: [{

            columnWidth: 0.33,
            items: [{
                xtype: 'datefield',
                name: 'desde',
                fieldLabel: 'Fecha desde',
                reference: 'fechaDesde',
                listeners: {
                    change: 'onSeleccionFechaDesde'
                },
                width: '90%'
            }]
        },{

            columnWidth: 0.33,
            items: [{
                xtype: 'datefield',
                name: 'hasta',
                fieldLabel: 'Fecha hasta',
                reference: 'fechaHasta',
                listeners: {
                    change: 'onSeleccionFechaHasta'
                },
                width: '90%'
            }]
        }]
    }, {
        xtype: 'container',
        layout: 'column',
        bind: {
            disabled: '{!tipo}'
        },
        items: [{

            columnWidth: 0.33,
            items: [{
                xtype: 'combobox',
                // reference: 'dependenciaSelector',
                emptyText: 'Seleccione uno',
                fieldLabel: 'Afiliados',
                store: cantidad,
                displayField: 'tipo',
                valueField: 'id',
                value: 1,
                reference: 'cantidadAsegurado',
                width: '90%',
                listeners: {
                    change:'onChangeCantidadAsegurado'
                }
            }]
        }, {
            columnWidth: 0.63,
            items: [{
                xtype: 'asegurado-selector-multiple',
                displayField: 'nombreCompleto',
                width: '90%',
                height: 100,
                growMax: 150,
                autoScroll: true,
                reference: 'aseguradoSelector',
                disabled: true,
                listeners: {
                    change:'onChangeAfiliados'
                }

            }]
        }]
    }, {
        xtype: 'container',
        layout: 'column',
        bind: {
            disabled: '{!tipo}'
        },
        items: [{

            columnWidth: 0.33,
            items: [{
                xtype: 'combobox',
                // reference: 'dependenciaSelector',
                emptyText: 'Seleccione uno',
                fieldLabel: 'Dependencias',
                store: cantidad,
                displayField: 'tipo',
                valueField: 'id',
                value: 1,
                reference: 'cantidadDependencia',
                width: '90%',
                listeners: {
                    change:'onChangeCantidadDependencias'
                }
            }]
        }, {
            columnWidth: 0.63,
            items: [{
                xtype: 'dependencia-selector-multiple',
                displayField: 'descripcion',
                width: '90%',
                height: 100,
                growMax: 150,
                reference: 'dependenciaSelector',
                autoScroll: true,
                disabled: true,
                store: Ext.create('Pce.afiliacion.dependencia.DependenciaStore', {
                    autoLoad: true
                }),
                listeners: {
                    change:'onChangeDependencias'
                }
            }]
        }]
    }, {
        xtype: 'container',
        layout: 'column',
        bind: {
            disabled: '{!tipo}'
        },
        items: [{

            columnWidth: 0.33,
            items: [{
                xtype: 'combobox',
                // reference: 'dependenciaSelector',
                emptyText: 'Seleccione uno',
                fieldLabel: 'Conceptos',
                store: cantidad,
                value: 1,
                displayField: 'tipo',
                reference: 'cantidadConcepto',
                valueField: 'id',
                width: '90%',
                listeners: {
                    change:'onChangeCantidadConceptos'
                }
            }]
        }, {
            columnWidth: 0.63,
            items: [{
                xtype: 'concepto-selector-multiple',
                displayField: 'descripcion',
                width: '90%',
                height: 100,
                growMax: 150,
                reference: 'conceptoSelector',
                autoScroll: true,
                disabled: true,
                listeners: {
                    change:'onChangeConceptos'
                }
            }]
        }]
    }],

    buttons: [{
        text: 'Generar archivo Excel',
        handler:  'onGenerarArchivoExcel'
    }]

});