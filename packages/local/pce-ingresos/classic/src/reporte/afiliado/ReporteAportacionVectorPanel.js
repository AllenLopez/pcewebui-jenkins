/**
 * @class Pce.reporte.afiliado.ReporteAportacionVectorPanel
 * @extends Ext.panel.Panel
 * @xtype reporte-afil-vector
 * Panel de ejecución y descarga de reporte de aportacionesde afiliados
 * de cuenta individual para envío a proveedor Vector, segpun fecha específica
 */

Ext.define('Pce.reporte.afiliado.ReporteAportacionVectorPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte-afil-vector',

    title: 'Reporte de aportaciones para Vector',

    defaults: {
        boddyPadding: 10,
        width: '100%'
    },

    items: [{
        xtype: 'toolbar',
        border: false,
         items: [{
            xtype: 'datefield',
            reference: 'fechainicio',
            name: 'fechainicio',
            labelWidth: 100,
            fieldLabel: 'Fecha desde:',
            width: 300,
            allowBlank: false,
            value: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)
        }, {
            xtype: 'datefield',
            reference: 'fechafinal',
            name: 'fechafinal',
            labelWidth: 100,
            fieldLabel: 'Fecha hasta:',
            width: 300,
            allowBlank: false,
            value: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)
         }],
    }, {
        xtype: 'toolbar',
        border: false,
        items: [{
            text: 'Aportacion Cargo',
            
            handler() {
                let fechainicio = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[0].value, 'd/m/Y');
                let fechafinal = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[1].value, 'd/m/Y');
                if(fechainicio <= fechafinal){
                    Ext.getBody().mask('Generando y enviando...');
                    Ext.Msg.confirm(
                    'Generar Archivo',
                    `¿Seguro que desea generar el reporte?`,
                    ans => {
                        if (ans === 'yes') {
                            Ext.Ajax.request({
                                method: 'GET',
                                url: '/api/ingresos/reporte/reporte-afil-vector-aportacion-cargo',
                                params: {fechainicio: fechainicio, fechafinal: fechafinal},
                                success(resp) {
                                    let {success, mensaje} = JSON.parse(resp.responseText);
                                    if (success) {
                                        Ext.Msg.alert(
                                            'Operación completada',
                                            `El archivo fue enviado, por favor verifique su correo electrónico`);
                                    } else {
                                        Ext.Msg.alert(
                                            'Operación completada',
                                            `Ocurrio un error al intentar generar el archivo: ${mensaje}`);
                                    }
                                    Ext.getBody().unmask();
                                },
                                failure(resp) {
                                    let {mensaje} = JSON.parse(resp.responseText);
                                    Ext.Msg.alert(
                                        'Operación completada',
                                        `Ocurrio un error al intentar generar el archivo: ${mensaje}`);
                                    Ext.getBody().unmask();
                                }
                            });
                        }
                        Ext.getBody().unmask();
                    });
                }else{
                        Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar una fecha desde menor a la Fecha hasta');
                    }
            }
        },{
            text: 'Aportación abono',
            handler() { 
                let fechainicio = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[0].value, 'd/m/Y');
                let fechafinal = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[1].value, 'd/m/Y');
                if(fechainicio <= fechafinal){
                Ext.getBody().mask('Generando y enviando...');
                Ext.Msg.confirm(
                    'Generar Archivo',
                    `¿Seguro que desea generar el reporte?`,
                    ans => {
                        if (ans === 'yes') {
                            Ext.Ajax.request({
                                method: 'GET',
                                url: '/api/ingresos/reporte/reporte-afil-vector-aportacion-abono',
                                params: {fechainicio: fechainicio, fechafinal: fechafinal},
                                success(resp) {
                                    let {success, mensaje} = JSON.parse(resp.responseText);
                                    if (success) {
                                        Ext.Msg.alert(
                                            'Operación completada',
                                            `El archivo fue enviado, por favor verifique su correo electrónico`);
                                    } else {
                                        Ext.Msg.alert(
                                            'Operación completada',
                                            `Ocurrio un error al intentar generar el archivo: ${mensaje}`);
                                    }

                                    Ext.getBody().unmask();
                                },
                                failure(resp) {
                                    let {mensaje} = JSON.parse(resp.responseText);
                                    Ext.Msg.alert(
                                        'Operación completada',
                                        `Ocurrio un error al intentar generar el archivo: ${mensaje}`);
                                    Ext.getBody().unmask();
                                }
                            });
                        }
                        Ext.getBody().unmask();
                    });
                }else{
                    Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar una fecha desde menor a la Fecha hasta');
                }
            }
        }, {
            text: 'Facturación',
            handler() {
                let fechainicio = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[0].value, 'd/m/Y');
                let fechafinal = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[1].value, 'd/m/Y');
                if(fechainicio <= fechafinal){
                Ext.Msg.confirm(
                    'Generar Archivo',
                    `¿Seguro que desea generar el reporte?`,
                    ans => {
                        if (ans === 'yes') {
                            Ext.getBody().mask('Generando archivo...');
                            window.open(`/api/ingresos/reporte/reporte-afil-vector-facturacion-cargo?fechainicio=${fechainicio}&&fechafinal=${fechafinal}`);
                            Ext.getBody().unmask();
                        }
                    });
                }else{
                    Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar una fecha desde menor a la Fecha hasta');
                }
            }
        }, {
            text: 'Facturación abono',
            handler() {
                let fechainicio = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[0].value, 'd/m/Y');
                let fechafinal = Ext.util.Format.date(this.ownerCt.ownerCt.items.items[0].items.items[1].value, 'd/m/Y');
                if(fechainicio <= fechafinal){
                Ext.Msg.confirm(
                    'Generar Archivo',
                    `¿Seguro que desea generar el reporte?`,
                    ans => {
                        if (ans === 'yes') {
                            Ext.getBody().mask('Generando archivo...');
                            window.open(`/api/ingresos/reporte/reporte-afil-vector-facturacion-abono?fechainicio=${fechainicio}&&fechafinal=${fechafinal}`);
                            Ext.getBody().unmask();
                        }
                    });
                }else{
                    Ext.Msg.alert('Advertencia!', 'Se debe de seleccionar una fecha desde menor a la Fecha hasta');
                }
            }
        }]
    }]
});