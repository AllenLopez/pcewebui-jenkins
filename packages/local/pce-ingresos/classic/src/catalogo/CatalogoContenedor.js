Ext.define('Pce.ingresos.catalogo.CatalogoContenedor', {
    extend: 'Ext.container.Container',
    xtype: 'catalogo-contenedor',

    layout: 'fit',

    requires: [
        'Pce.ingresos.concepto.ConceptoTabla',
        'Pce.ingresos.avisocargo.AvisoCargoTabla',
        'Pce.ingresos.movimiento.TipoMovimientoTabla',
        'Pce.ingresos.dependencia.AportacionTabla',
        'Pce.ingresos.clave.ClaveTabla',
        'Pce.ingresos.transaccion.OrigenTabla',
        'Pce.ingresos.transaccion.TipoResultadoTabla',
        'Pce.afiliacion.dependencia.DependenciaTabla',
        'Pce.ingresos.factor80.Factor80Tabla'

    ],

    mapaCatalogos: {
        'conceptos' : 'concepto-tabla',
        'avisos-cargo': 'aviso-cargo-tabla',
        'tipos-movimiento': 'tipo-movimiento-tabla',
        'dependencia-aportacion': 'dependencia-aportacion-tabla',
        'clave': 'clave-tabla',
        'transaccion-origen': 'transaccion-origen-tabla',
        'transaccion-tipo-resultado': 'transaccion-tipo-resultado-tabla',
        'dependencias': 'dependencia-tabla',
        'factor80': 'factor-80-tabla'
    },

    catalogo: undefined,

    afterRender() {
        this.callParent(arguments);
        if (this.catalogo) {
            this.removeAll(true);
            this.add({
                xtype: this.mapaCatalogos[this.catalogo]
            });
        }
    }
});