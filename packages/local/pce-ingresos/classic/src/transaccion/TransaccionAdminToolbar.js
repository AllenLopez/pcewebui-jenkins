Ext.define('Pce.ingresos.transaccion.TransaccionAdminToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'transaccion-admin-toolbar',

    items: [{
        text: 'Calcular aportaciones',
        handler: 'onCalcularAportaciones',
        tooltip:'Realizar calculo de aportaciones',
        hidden: true,
        bind: {
            hidden: '{!(transaccionTabla.selection && (transaccionTabla.selection.etapa == "TR" || transaccionTabla.selection.etapa == "RE"))}'
        }
    }, {
        text: 'Afectar cartera',
        handler: 'onCompletarTransaccion',
        hidden: true,
        bind: {
            hidden: '{!(transaccionTabla.selection && transaccionTabla.selection.etapa == "CT")}'
        }
    }, {
        text: 'Reporte de retenciones',
        iconCls: 'x-fa fa-file-pdf-o',
        handler: 'onMostrarReporteRetenciones',
        tooltip:'Generar reporte de retenciones',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    }, {
        text: 'Resumen de retenciones y aportaciones',
        iconCls: 'x-fa fa-file-pdf-o',
        handler: 'onMostrarReporteTotales',
        tooltip:'Mostrar resumen de retenciones y aportaciones',
        hidden: true,
        bind: {
            hidden: '{!(transaccionTabla.selection && transaccionTabla.selection.etapa != "TR")}'
        }
    }, {
        text: 'Reporte de afectación de cartera',
        handler: 'onMostrarReporteDeAfectacion',
        tooltip:'Generar reporte de afectación a cartera',
        iconCls: 'x-fa fa-file-pdf-o',
        bind: {
            hidden: '{!(transaccionTabla.selection && transaccionTabla.selection.etapa == "AU")}'
        }
    }, {
        text: 'Mostrar errores de la transacción',
        handler: 'onMostrarResultados',
        tooltip:'Mostrar errores generados en la transacción',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    }]
});