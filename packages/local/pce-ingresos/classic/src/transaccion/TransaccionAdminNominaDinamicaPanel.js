/**
 * @class Pce.ingresos.transaccion.TransaccionAdminNominaDinamicaPanel
 * @extends Ext.panel.Panel
 * @xtype transaccion-admin-nomina-dinamica-panel
 * Pantalla de administración de transacciones pendientes de procesar pertenecientes a carga
 * de nómina dinámica
 */
Ext.define('Pce.ingresos.transaccion.TransaccionAdminNominaDinamicaPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'transaccion-admin-nomina-dinamica-panel',
    
    requires: [
        'Pce.ingresos.transaccion.TransaccionTabla',
        'Pce.ingresos.transaccion.TransaccionStore',

        'Pce.ingresos.transaccion.TransaccionAdminNominaDinamicaPanelController'
    ],

    title: 'Cálculo de aportaciones de nómina dinámica',
    layout: 'fit',

    controller: 'transaccion-admin-nomina-dinamica-controller',
    viewModel: {},

    tbar: [{
        text: 'Afectar cartera',
        handler: 'onCompletarTransaccion',
        hidden: true,
        bind: {
            hidden: '{!(transaccionTabla.selection && transaccionTabla.selection.enControl)}'
        }
    }, {
        text: 'Reporte de detalle de nómina',
        handler: 'onMostrarReporteDetallesNominaDinamica',
        tooltip: 'Reporte de detalle de nómina',
        bind: {
            disabled: '{!transaccionTabla.selection}'
        }
    }, {
        text: 'Reporte de afectación de cartera',
        handler: 'onMostrarReporteEstadoCuenta',
        tooltip: 'Reporte de afectación de cartera'
,        bind: {
            disabled: '{!(transaccionTabla.selection && !transaccionTabla.selection.enControl)}'
        }
    }, {
        text: 'Mostrar errores de la transacción',
        handler: 'onMostrarResultados',
        tooltip: 'Mostrar errores de la transacción',
        bind: {
            disabled: '{!transaccionTabla.selection}'
        }
    }],
    items: [{
        xtype: 'transaccion-tabla',
        reference: 'transaccionTabla',
        columns: [{
            text: 'Transacción',
            dataIndex: 'id',
            tooltip:'Numero de transacción',
            width: 50,
            flex: 0.8
        }, {
            text: 'Fecha de movimiento',
            dataIndex: 'fechaMovimiento',
            tooltip:'Fecha en la que se realizo el movimiento',
            renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
            flex: 1.5
        }, {
            text: 'Etapa',
            dataIndex: 'etapaDescripcion',
            tooltip:'Etapa en la que se encuentra la transacción',
            flex: 1
        }, {
            text: 'Descripción de origen',
            dataIndex: 'origenDescripcion',
            tooltip:'Descripción de origen de la transacción',
            flex: 2
        }, {
            text: 'Usuario',
            dataIndex: 'usuarioCreacion',
            tooltip:'Usuario que genero la transacción',
            flex: 2
        }, {
            xtype: 'actioncolumn',
            width: 60,
            items: [{
                iconCls: 'x-fa fa-search',
                tooltip: 'ver registros',
                handler: 'onMostrarDetalles'
            }]
        }],
        store: {
            proxy: {
                type: 'rest',
                url: '/api/ingresos/transaccion/pendientes-nomina-dinamica'
            },
            type: 'transacciones',
            autoLoad: true
        }
    }]
});