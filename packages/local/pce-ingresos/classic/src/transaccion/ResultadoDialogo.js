Ext.define('Pce.ingresos.transaccion.ResultadoDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'transaccion-resultado-dialogo',

    requires: [
        'Pce.ingresos.transaccion.ResultadoTabla',
        'Pce.ingresos.transaccion.ResultadoStore'
    ],

    layout: 'fit',
    maximizable: true,
    maximized: true,

    title: 'Errores del proceso de transacción',
    width: 800,
    height: 600,

    config: {
        transaccion: undefined
    },

    items: {
        xtype: 'transaccion-resultado-tabla',
        store: {
            type: 'transaccion-resultados'
        }
    },

    afterRender() {
        this.callParent();

        if (this.transaccion) {
            this.cargarResultados(this.transaccion);
        }
    },

    setTransaccion(transaccion) {
        this.transaccion = transaccion;
        
        if (this.rendered) {
            this.cargarResultados(transaccion);
        }
    },

    getTransaccion() {
        return this.transaccion;
    },

    cargarResultados(transaccion) {
        this.down('grid').getStore().load({
            params: {transaccion: transaccion.getId()}
        });
    }
});