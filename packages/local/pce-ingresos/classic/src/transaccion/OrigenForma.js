Ext.define('Pce.ingresos.transaccion.OrigenForma', {
    extend: 'Ext.form.Panel',
    xtype: 'transaccion-origen-forma',

    items: [{
        xtype: 'textfield',
        name: 'clave',
        fieldLabel: 'Clave',
        allowBlank: false
    }, {
        xtype: 'textfield',
        name: 'descripcion',
        fieldLabel: 'Descripción'
    }]
});