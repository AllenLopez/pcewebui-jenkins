Ext.define('Pce.ingresos.transaccion.cancelacionTransaccion.CancelacionTransaccionFiltrosToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'cancelacion-transaccion-filtros-toolbar',

    viewModel: {
        data: {
            idTransaccion: null
        }
    },

    items: [{
        xtype: 'datefield',
        name: 'desde',
        fieldLabel: 'A partir de',
        labelWidth: 80,
        flex: 1,
        listeners: {
            change: 'onSeleccionDesde'
        }
    }, {
        xtype: 'datefield',
        name: 'hasta',
        fieldLabel: 'Hasta',
        labelWidth: 80,
        flex: 1,
        listeners: {
            change: 'onSeleccionHasta'
        }
    }, {
        xtype: 'numberfield',
        fieldLabel: 'Transacción',
        valueField: 'transaccion',
        name: 'transaccion',
        labelWidth: 100,
        bind: {
            value: '{idTransaccion}'
        },
        flex: 1
    }, {
        text: 'Buscar',
        flex: 0.5,
        bind: {
            disabled: '{!idTransaccion}'
        },
        handler: 'onBuscar'
    }]
});