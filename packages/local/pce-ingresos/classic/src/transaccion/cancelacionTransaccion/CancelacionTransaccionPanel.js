Ext.define('Pce.ingresos.transaccion.cancelacionTransaccion.CancelacionTransaccionPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'cancelacion-transaccion-panel',

    requires: [
        
    ],

    title: 'Cancelación transacción',
    layout: 'fit',

    controller: 'cancelacion.transaccion-panel',
    
    viewModel: {},

    dockedItems: [{
        xtype: 'cancelacion-transaccion-filtros-toolbar',
        dock: 'top',
        reference: 'toolbarFiltros',
    }, {
        xtype: 'cancelacion-transaccion-toolbar',
        dock: 'top',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    },],

    items: [{
        xtype: 'cancelacion-transaccion-tabla',
        reference: 'transaccionTabla',
        store: {
            type: 'transacciones'
        }
    }]
});