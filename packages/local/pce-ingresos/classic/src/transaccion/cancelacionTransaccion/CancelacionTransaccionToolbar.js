Ext.define('Pce.ingresos.transaccion.cancelacionTransaccion.CancelacionTransaccionToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'cancelacion-transaccion-toolbar',

    items: [{
        text: 'Cancelar transacción 882',
        handler: 'onCancelarTransaccion',
        tooltip:'Realizar la cancelación de la transacción',
        bind: {
            disabled: '{transaccionTabla.selection.cancelado == "SI" || transaccionTabla.selection.origenClave == "CANCELAR"}',
            text: 'Cancelar transaccion {transaccionTabla.selection.id}'
        }
    }, {
        text: 'Reporte de afectación de cartera de nómina convencional',
        handler: 'onMostrarReporteNomiaConvencional',
        iconCls: 'x-fa fa-file-pdf-o',
        tooltip: 'Reporte de afectación de cartera de nómina convencional',
        bind: {
            hidden: '{transaccionTabla.selection.origenClave != "NOMINA" && transaccionTabla.selection.origenClaveTransaccionOrigen != "NOMINA"}'
        }
    },{
        text: 'Reporte de cancelación nómina convencional',
        iconCls: 'x-fa fa-file-pdf-o',
        handler: 'onMostrarReporteCancelacionNomiaConvencional',
        tooltip:'Generar reporte de cancelación nómina convencional',
        bind: {
            hidden: '{ transaccionTabla.selection.origenClaveTransaccionOrigen != "NOMINA"}'
        }
    }, {
        text: 'Reporte de afectación de cartera nómina dinámica ',
        // handler: 'onMostrarReporteDeAfectacion',
        tooltip:'Generar reporte de afectación de cartera nómina dinámica',
        iconCls: 'x-fa fa-file-pdf-o',
        bind: {
            hidden: '{transaccionTabla.selection.origenClave != "NOM_DIN_JPV"}'
        }
    }, {
        text: 'Reporte de afectación de cartera nómina préstamos',
        // handler: 'onMostrarReporteDeAfectacion',
        tooltip:'Generar reporte de afectación de cartera nómina préstamos',
        iconCls: 'x-fa fa-file-pdf-o',
        bind: {
            hidden: '{transaccionTabla.selection.origenClave != "NOMINA_PR"}'
        }
    }, 
    // {
    //     text: 'Reporte de transacción',
    //     handler: 'onMostrarReporteDetalleTransaccion',
    //     tooltip:'Generar reporte de detalles de transacción',
    //     iconCls: 'x-fa fa-file-pdf-o',
    //     bind: {
    //         hidden: '{!transaccionTabla.selection}'
    //     }
    // },  
    {
        text: 'Mostrar errores de la transacción',
        handler: 'onMostrarResultados',
        tooltip:'Mostrar errores generados en la transacción',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    }]
});