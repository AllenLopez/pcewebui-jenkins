Ext.define('Pce.ingresos.transaccion.cancelacionTransaccion.CancelacionTransaccionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'cancelacion-transaccion-tabla',

    columns: [{
        text: 'Transacción',
        dataIndex: 'id',
        tooltip:'Numero de transacción',
        width: 50,
        flex: 0.8
    }, {
        text: 'Fecha de movimiento',
        dataIndex: 'fechaMovimiento',
        tooltip:'Fecha en la que se realizo el movimiento',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
        flex: 1
    }, {
        text: 'Etapa',
        dataIndex: 'etapaDescripcion',
        tooltip:'Etapa en la que se encuentra la transacción',
        flex: 1
    }, {
        text: 'Descripción de origen',
        dataIndex: 'origenDescripcion',
        tooltip:'Descripción de origen de la transacción',
        flex: 2
    }, {
        text: 'Trans. origen',
        dataIndex: 'idTransaccionOrigen',
        tooltip:'Origen de la transacción',
        flex: 1
    }, {
        text: 'Origen',
        dataIndex: 'descripcionTransaccionOrigen',
        tooltip:'Origen de la transacción',
        flex: 2
    }, {
        text: 'Generó Reversa',
        dataIndex: 'cancelado',
        tooltip:'Folio Aviso Cargo de la transacción',
        flex: 1
    }, {
        text: 'Usuario',
        dataIndex: 'usuarioCreacion',
        tooltip:'Usuario que genero la transacción',
        flex: 2
    },{ 
        text: 'Reporte Detalle',
        xtype: 'actioncolumn',
        flex: 2,
        items: [{
            iconCls: 'x-fa fa-file-pdf-o',
            tooltip: 'Generar reporte de detalles de transacción',
            handler: 'onMostrarReporteDetalleTransaccion',
            bind: {
                hidden: '{!transaccionTabla.selection}'
            }
        }]
    }]
   
});