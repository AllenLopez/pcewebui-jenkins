Ext.define('Pce.ingresos.transaccion.TransaccionAdminPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.transaccion-admin-panel',

    requires: [
        'Pce.ingresos.transaccion.TransaccionServicio',
        'Pce.ingresos.transaccion.ResultadoDialogo'
    ],

    /**
     * @property {Pce.ingresos.transaccion.TransaccionServicio}
     */
    servicio: undefined,

    params:{
        desde: null,
        hasta: null
    },

    init() {
        this.servicio = new Pce.ingresos.transaccion.TransaccionServicio();
    },
 
    onSeleccionDesde(comp, valor){
        if( !comp.validate() || !valor ){
            this.params.desde = null;
            return;
        }
        this.params.desde = valor;
        this.params.hasta = this.lookup('fechaFin').value;
        this.validarFechas();
    },

    onSeleccionHasta(comp, valor){
        if( !comp.validate() || !valor ){
            this.params.hasta = null;
            return;
        }
        this.params.hasta = valor;
        this.params.desde = this.lookup('fechaInicio').value;
        this.validarFechas();
    },

    validarFechas(){
        if( !this.params.hasta || !this.params.desde){
            return;
        }

        if(this.params.desde > this.params.hasta){
            Ext.Msg.alert('Advertencia!', 'No es un rango de fechas valido');
            return;
        }
        this.params.desde = this.formatDate(this.params.desde, 'd/m/Y');
        this.params.hasta = this.formatDate(this.params.hasta, 'd/m/Y');

        this.lookup('transaccionTabla').getStore().porFechaMovimiento(this.params);
    },
    formatDate(date, format){
        return  Ext.util.Format.date(date, format);
    },

    onBuscar(){
        this.params.desde = this.lookup('fechaInicio').value;
        this.params.hasta = this.lookup('fechaFin').value;
        this.params.desde = this.formatDate(this.params.desde, 'd/m/Y');
        this.params.hasta = this.formatDate(this.params.hasta, 'd/m/Y');
        this.lookup('transaccionTabla').getStore().porFechaMovimiento(this.params);
    },

    onCalcularAportaciones() {
        let transaccion = this.getTransaccion();

        Ext.Msg.confirm(
            'Cálculo de aportaciones',
            `Se efectuará el cálculo de aportaciones. ¿Deseas proceder?`,
            resp => {
                if(resp === 'yes'){
                    Ext.getBody().mask('Calculando aportaciones...');
                    this.servicio.calcularAportaciones(transaccion)
                        .then((resp) => {
                            if (!resp.exito) {
                                if (resp.excepcion) {
                                    Ext.Msg.show({
                                        title: 'Error en el proceso',
                                        message: resp.mensaje,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.Msg.ERROR
                                    });
                                } else {
                                    this.mostrarResultados();
                                }
                            } else {
                                Ext.Msg.alert(
                                    'Operación completada',
                                    'Las aportaciones se han calculado exitósamente'
                                );
                            }
                        })
                        .finally(() => {
                            this.lookup('transaccionTabla').getStore().reload();
                            Ext.getBody().unmask();
                        });
                }
            }
        );
    },

    onCompletarTransaccion() {
        let transaccion = this.getTransaccion();
        Ext.Msg.confirm(
            'Confirmar actualizacón de cartera',
            'Se llevará a cabo la actualización de ' +
            'cartera para la transacción seleccionada. ' +
            '¿Deseas proceder?',
            ans => {
                if(ans === 'yes') {
                    this.afectarCartera(transaccion);
                }
            }
        );
    },

    afectarCartera(transaccion) {
        let transaccionId = transaccion.getId();
        Ext.getBody().mask('Completando afectación de cartera...');
        Ext.Ajax.request({
            url: '/api/ingresos/actualizacion-cartera/actualizar',
            scope: this,
            params: {
                transaccionId: transaccionId
            },
            callback(op, success, response) {
                let resultado = JSON.parse(response.responseText);
                Ext.getBody().unmask();

                if (resultado.exito) {
                    this.lookup('transaccionTabla').getStore().reload();

                    Ext.Msg.alert(
                        'Transacción procesada',
                        'La afectación de cartera se ha completado exitósamente',
                        () => {
                            this.mostrarReporteAvisosCargo(transaccionId);
                        }
                    );
                } else {
                    Ext.Msg.alert(
                        'Error en el proceso de afectación',
                        `La transacción ${transaccionId} no pudo ser procesada`,
                        () => this.mostrarResultados());
                }
            }
        });
    },

    onMostrarDetalles(tabla, i, j, item, e, record) {
        this.mostrarDialogoDetallesTabla(record);
    },

    mostrarDialogoDetallesTabla(record){
        Ext.create('Ext.window.Window',{
            title: 'Detalles de transacción',
            autoShow: true,
            modal: true,
            width: 1300,
            height: 600,
            layout: 'fit',
            items: {
                xtype: 'detalle-dep-transaccion-tabla',
                transaccion: record,
            },
            listeners: {
                beforeClose(panel) {
                    panel.down('detalle-dep-transaccion-tabla').getStore().removeAll();
                }
            }
        });
    },

    onMostrarResultados() {
        this.mostrarResultados();
    },

    mostrarResultados() {
        Ext.create({
            xtype: 'transaccion-resultado-dialogo',
            transaccion: this.getTransaccion(),
            autoShow: true,
            modal: true
        });
    },

    mostrarReporteAvisosCargo(transaccionId) {
        window.open(`/api/ingresos/emision-aviso-cargo/por-transaccion?transaccionId=${transaccionId}`);
    },

    onMostrarReporteRetenciones() {
        let id = this.getTransaccion().getId();
        window.location = `/api/ingresos/transaccion/reporte-retenciones?transaccionId=${id}`;
    },

    onMostrarReporteTotales() {
        let id = this.getTransaccion().getId();
        window.location = `/api/ingresos/transaccion/reporte-totales?transaccionId=${id}`;
    },
    onMostrarReporteDeAfectacion() {
        let id = this.getTransaccion().getId();
        window.location = `/api/ingresos/transaccion/reporte-afectacion?transaccionId=${id}`;
    },

    getTransaccion() {
        return this.lookup('transaccionTabla').getSelection()[0];
    }
});