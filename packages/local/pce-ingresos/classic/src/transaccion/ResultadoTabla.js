Ext.define('Pce.ingresos.transaccion.ResultadoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'transaccion-resultado-tabla',

    emptyText: 'No existen errores para esta transacción',
    
    columns: [{
        text: 'Tipo de error',
        dataIndex: 'descripcion',
        flex: 3
    }, {
        text: 'Número afiliación',
        dataIndex: 'numeroAfiliacion',
        flex: 1
    }, {
        text: 'Dependencia',
        dataIndex: 'dependencia',
        flex: 3
    }, {
        text: 'Concepto',
        dataIndex: 'conceptoClave',
        flex: 1
    },{
        text: 'Descripción concepto',
        dataIndex: 'conceptoDescripcion',
        flex: 3
    }, {
        text: 'Importe',
        dataIndex: 'importe',
        flex: 1
    }]
});