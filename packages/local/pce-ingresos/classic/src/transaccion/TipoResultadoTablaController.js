Ext.define('Pce.ingresos.transaccion.TipoResultadoTablaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.transaccion-tipo-resultado-tabla',

    requires: [
        'Pce.ingresos.transaccion.TipoResultadoForma'
    ],
    onAgregarTipoTransaccion() {
        this.mostrarDialogoEdicion(
            new Pce.ingresos.transaccion.TipoResultado({})
        );
    },

    onEditarTipoResultado(view, row, col, item, e, tipoResultado) {
        this.mostrarDialogoEdicion(tipoResultado);
    },

    onEliminarTipoResultado() {

    },

    mostrarDialogoEdicion(tipoResultado) {
        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'Tipo de resultado de transacción',
            record: tipoResultado,
            width: 500,
            items: {xtype: 'transaccion-tipo-resultado-forma'},
            listeners: {
                guardar: this.onGuardarTipoResultado.bind(this)
            }
        });
    },

    onGuardarTipoResultado(dialog, tipoResultado) {
        tipoResultado.save({
            success() {dialog.close();}
        });
    }

});