Ext.define('Pce.ingresos.transaccion.TipoResultadoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'transaccion-tipo-resultado-forma',

    items: [{
        xtype: 'textarea',
        name: 'descripcion',
        fieldLabel: 'Descripción',
        allowBlank: false,
        width: '100%'
    }]
});