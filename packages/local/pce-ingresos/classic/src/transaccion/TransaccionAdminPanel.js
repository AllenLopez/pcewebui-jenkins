Ext.define('Pce.ingresos.transaccion.TransaccionAdminPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'transaccion-admin-panel',

    requires: [
        'Pce.ingresos.transaccion.TransaccionAdminPanelController',
        'Pce.ingresos.transaccion.TransaccionAdminToolbar',
        'Pce.ingresos.transaccion.TransaccionTabla',
        'Pce.ingresos.transaccion.TransaccionStore'
    ],

    title: 'Cálculo de aportaciones y afectación de cartera',
    layout: 'fit',

    controller: 'transaccion-admin-panel',
    viewModel: {},

    dockedItems: [{
        xtype: 'transaccion-filtros-toolbar',
        dock: 'top',
        reference: 'toolbarFiltros',
    }, {
        xtype: 'transaccion-admin-toolbar',
        dock: 'top',
        bind: {
            hidden: '{!transaccionTabla.selection}'
        }
    },],
    // tbar: {xtype: 'transaccion-admin-toolbar'},
    items: [{
        xtype: 'transaccion-tabla',
        reference: 'transaccionTabla',
        store: {
            type: 'transacciones',
            autoLoad: true
        }
    }]
});