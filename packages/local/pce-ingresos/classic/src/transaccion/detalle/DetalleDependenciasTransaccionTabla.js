/**
 * @class Pce.ingresos.transaccion.detalle.DetalleDependenciasTransaccionTabla
 * @extends Ext.grid.Panel
 * @xtype detalle-dep-transaccion-tabla
 * Enlista los registros de las dependencias que generaron detalle para la transacción seleccionada
 */
Ext.define('Pce.ingresos.transaccion.detalle.DetalleDependenciasTransaccionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'detalle-dep-transaccion-tabla',

    requires: [
        'Pce.ingresos.transaccion.DetalleDependenciaTransaccionStore'
    ],

    store: {
        type: 'detalles-dep-transaccion'
    },

    columns: [{
        text: 'Transaccion',
        dataIndex: 'transaccion',
        flex: 1
    } ,{
        text: 'Etapa',
        dataIndex: 'etapa',
        flex: 1
    } ,{
        text: 'Clave dependencia',
        dataIndex: 'claveDependencia',
        flex: 1
    } ,{
        text: 'Dependencia',
        dataIndex: 'dependencia',
        flex: 3
    } ,{
        text: 'Submotivo',
        dataIndex: 'submotivo',
        flex: 2
    } ,{
        text: 'Descripción origen',
        dataIndex: 'origen',
        flex: 2
    } ,{
        text: 'Fecha movimiento',
        dataIndex: 'fechaMovimiento',
        renderer(v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        },
        flex: 1
    } ,{
        text: 'Usuario',
        dataIndex: 'usuario',
        flex: 1
    }],

    setTransaccion(transaccion) {
        console.log(transaccion);
        this.transaccion = transaccion;
        this.store.load({
            params: {
                transaccionId: transaccion.getId()
            }
        });
    },

    afterRender() {
        this.callParent();
        if(this.transaccion) {
            this.setTransaccion(this.transaccion);
        }
    }
});