Ext.define('Pce.ingresos.transaccion.TransaccionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'transaccion-tabla',

    columns: [{
        text: 'Transacción',
        dataIndex: 'id',
        tooltip:'Numero de transacción',
        flex: 0.7
    }, {
        text: 'Dependencia',
        dataIndex: 'dependencia',
        tooltip:'Dependencia en donde se realizo el movimiento',
        flex: 5
    },{
        text: 'Fecha de movimiento',
        dataIndex: 'fechaMovimiento',
        tooltip:'Fecha en la que se realizo el movimiento',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y H:i:s'),
        flex: 1.5
    }, {
        text: 'Etapa',
        dataIndex: 'etapaDescripcion',
        tooltip:'Etapa en la que se encuentra la transacción',
        flex: 1
    }, {
        text: 'Descripción de origen',
        dataIndex: 'origenDescripcion',
        tooltip:'Descripción de origen de la transacción',
        flex: 2
    }, {
        text: 'Usuario',
        dataIndex: 'usuarioCreacion',
        tooltip:'Usuario que genero la transacción',
        flex: 2
    }, {
        xtype: 'actioncolumn',
        width: 60,
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'ver registros',
            handler: 'onMostrarDetalles'
        }]
    }]
});