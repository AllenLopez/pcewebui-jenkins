Ext.define('Pce.ingresos.transaccion.OrigenTablaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.transaccion-origen-tabla',

    requires: [
        'Pce.ingresos.transaccion.OrigenForma',
        'Pce.view.dialogo.FormaContenedor'
    ],

    onAgregarOrigen() {
        this.mostrarDialogoEdicion(
            new Pce.ingresos.transaccion.Origen({})
        )
    },

    onEditarOrigen(view, row, col, item, e, origen) {
        this.mostrarDialogoEdicion(origen);
    },

    onEliminarOrigen(view, row, col, item, e, origen) {
        Ext.Msg.confirm(
            'Eliminar origen de transacción',
            `El origen ${origen.data.clave} será eliminado permanentemente. ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    origen.erase();
                }
            }
        );
    },

    mostrarDialogoEdicion(origen) {
        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'Origen de transacción',
            record: origen,
            items: [{xtype: 'transaccion-origen-forma'}],
            listeners: {
                guardar: this.onGuardarOrigen.bind(this)
            }
        });
    },

    onGuardarOrigen(dialogo, origen) {
        origen.save({
            success() {
                dialogo.close();
            }
        });
    }
});