/**
 * @class Pce.ingresos.transaccion.TransaccionAdminNominaDinamicaPanelController
 * @extends Ext.app.ViewController
 * @alias controller.transaccion-admin-nomina-dinamica-controller
 * Controlador de pantalla de administración de transacciones pendientes de nómina dinámica
 */
Ext.define('Pce.ingresos.transaccion.TransaccionAdminNominaDinamicaPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.transaccion-admin-nomina-dinamica-controller',
    
    requires: [
        'Pce.ingresos.transaccion.ResultadoDialogo'
    ],

    onCompletarTransaccion() {
        let transaccion = this.getTransaccion();
        
        Ext.Msg.confirm(
            'Confirmar actualización de cartera',
            'Se llevará a cabo la actualización de ' +
            'cartera para la transacción seleccionada. ' +
            '¿Deseas proceder?',
            ans => {
                if(ans === 'yes') {
                    this.afectaCartera(transaccion);
                }
            }
        );
    },

    afectaCartera(transaccion) {
        let transaccionId = transaccion.getId();
        
        Ext.getBody().mask('Completando afectación de cartera...');
        Ext.Ajax.request({
            url: '/api/ingresos/nomina-dinamica/carga/afecta-cartera-nomina-dinamica',
            scope: this,
            params: {
                transaccionId: transaccionId
            },
            callback(op, success, response) {
                let resultado = JSON.parse(response.responseText);
                Ext.getBody().unmask();

                if(resultado.exito){
                    this.lookup('transaccionTabla').getStore().reload();

                    Ext.Msg.alert(
                        'Transacción procesada',
                        'La afectación de cartera se ha completado exitosamente',
                        () => {
                            this.mostrarReporteAvisosCargo(transaccionId);
                        }
                    );
                } else {
                    Ext.Msg.alert(
                        'Error en el proceso de afectación',
                        `La transacción ${transaccionId} no pudo ser procesada. ${resultado.mensaje}`,
                        () => this.mostrarResultados()
                    );
                }
            }
        });
    },

    onMostrarResultados() {
        this.mostrarResultados();
    },

    mostrarResultados() {
        let transaccion = this.getTransaccion();
        console.log(transaccion);
        Ext.create({
            xtype: 'transaccion-resultado-dialogo',
            transaccion: transaccion,
            autoShow: true,
            modal: true
        });
    },

    mostrarReporteAvisosCargo(transaccionId) {
        window.open(`/api/ingresos/emision-aviso-cargo/por-transaccion?transaccionId=${transaccionId}`);
    },

    onMostrarReporteEstadoCuenta() {
        let transaccionId = this.getTransaccion().getId();
        
        if(transaccionId) {
            window.open(`/api/ingresos/transaccion/reporte-estado-cuenta-nomina-dinamica?transaccionId=${transaccionId}`);
        }
    },

    onMostrarReporteDetallesNominaDinamica() {
        let transaccionId = this.getTransaccion().getId();
        
        if(transaccionId) {
            window.open(`/api/ingresos/transaccion/reporte-detalle-nomina-dinamica?transaccionId=${transaccionId}`);
        }
    },

    mostrarReporteDeAfectacion(id) {
        if(!id) {
            id = this.getTransaccion().getId();
        }
        window.open(`/api/ingresos/transaccion/reporte-estado-cuenta-nomina-dinamica?transaccionId=${id}`);
    },

    getTransaccion() {
        return this.lookup('transaccionTabla').getSelection()[0];
    },
    
    onMostrarDetalles(tabla, i, j, item, e, record) {
        this.mostrarDialogoDetallesTabla(record);
    },

    mostrarDialogoDetallesTabla(record){
        Ext.create('Ext.window.Window',{
            title: 'Detalles de transacción',
            autoShow: true,
            modal: true,
            width: 1300,
            height: 600,
            layout: 'fit',
            items: {
                xtype: 'detalle-dep-transaccion-tabla',
                transaccion: record,
            },
            listeners: {
                beforeClose(panel) {
                    panel.down('detalle-dep-transaccion-tabla').getStore().removeAll();
                }
            }
        });
    },


});