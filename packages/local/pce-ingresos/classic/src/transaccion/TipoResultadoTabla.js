Ext.define('Pce.ingresos.transaccion.TipoResultadoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'transaccion-tipo-resultado-tabla',

    requires: [
        'Pce.ingresos.transaccion.TipoResultadoTablaController',
        'Pce.ingresos.transaccion.TipoResultadoStore'
    ],

    controller: 'transaccion-tipo-resultado-tabla',
    store: {
        type: 'transaccion-tipo-resultado',
        autoLoad: true
    },

    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarTipoTransaccion'
    }],
    columns: [{
        text: 'Descripción',
        dataIndex: 'descripcion',
        flex: 1
    }, {
        xtype: 'actioncolumn',
        items: [{
            iconCls: 'x-fa fa-edit',
            handler: 'onEditarTipoResultado'
        }, {
            iconCls: 'x-fa fa-trash',
            handler: 'onEliminarTipoResultado'
        }]
    }]
});