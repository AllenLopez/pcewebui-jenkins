Ext.define('Pce.ingresos.transaccion.OrigenTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'transaccion-origen-tabla',

    requires: [
        'Pce.ingresos.transaccion.OrigenStore',
        'Pce.ingresos.transaccion.OrigenTablaController'
    ],

    controller: 'transaccion-origen-tabla',
    store: {
        type: 'transaccion-origenes',
        autoLoad: true
    },

    title: 'Catálogo de origen de transacción',
    tbar: [{
        text: 'Agregar',
        handler: 'onAgregarOrigen'
    }],
    columns: [{
        text: 'Clave',
        dataIndex: 'clave',
        flex: 1
    }, {
        text: 'Descripción',
        dataIndex: 'descripcion',
        flex: 3
    }, {
        xtype: 'actioncolumn',
        items: [{
            iconCls: 'x-fa fa-edit',
            handler: 'onEditarOrigen'
        }, {
            iconCls: 'x-fa fa-trash',
            handler: 'onEliminarOrigen'
        }]
    }]
});