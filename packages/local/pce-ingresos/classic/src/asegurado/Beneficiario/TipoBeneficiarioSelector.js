Ext.define('Pce.ingresos.asegurado.beneficiario.TipoBeneficiarioSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'tipo-beneficiario-selector',

    requires: [
        'Pce.ingresos.asegurado.beneficiario.TipoBeneficiarioStore'
    ],

    fieldLabel: 'Tipo',
    valueField: 'id',
    displayField: 'descripcion',
    queryMode: 'local',
    allowBlank: false,

    store: {
        type: 'beneficiario-tipo',
        autoLoad: true
    },
});