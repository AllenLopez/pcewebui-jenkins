Ext.define('Pce.ingresos.asegurado.BeneficiarioTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'beneficiario-tabla',

    require:[
        'Pce.ingresos.asegurado.beneficiario.BeneficiarioStore'
    ],

    title: 'Datos de beneficiarios',

    store: [
        'beneficiario'
    ],

    tbar: [{
        text: 'Nuevo beneficiario',
        reference: 'nuevoBeneficiarioBoton',
        ui: 'default',
        handler: 'onAgregarNuevoBenecificiario',
        bind: {
            disabled: '{!fallecidoCheck.checked && importeFondo > 0}'
        }
    }],

    columns:[{
        text: 'Nombre',
        dataIndex: 'nombrePersona',
        flex: 4,
    }, {
        text: 'Parentesco',
        dataIndex: 'parentesco',
        flex: 1,
    }, {
        text: 'Origen de alta',
        dataIndex: 'origen',
        flex: 1
    }, {
        text: '%',
        dataIndex: 'porcentaje',
        flex: 1,
    }, {
        xtype: 'actioncolumn',
        width: 60,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-edit',
            tooltip: 'Editar Beneficiario',
            handler: 'onEditarBeneficiario',
            isDisabled(view, rowIndex, collIndex, item, record) {
                return (record.get('origen') === 'AFILIACION');
            }
        }, {
            iconCls: 'x-fa fa-trash',
            tooltip: 'Eliminar beneficiario',
            handler: 'onEliminarBeneficiario',
            isDisabled(view, rowIndex, collIndex, item, record) {
                return (record.get('origen') === 'AFILIACION');
            }
        }]
    }]
});