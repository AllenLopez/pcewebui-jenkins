Ext.define('Pce.ingresos.asegurado.beneficiario.ParentescoBeneficiarioSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'parentesco-beneficiario-selector',

    requires: [
        'Pce.ingresos.asegurado.beneficiario.ParentescoBeneficiarioStore'
    ],

    fieldLabel: 'Parentesco',
    valueField: 'id',
    displayField: 'descripcion',
    queryMode: 'local',
    allowBlank: false,

    store: {
        type: 'parentesco-beneficiario',
        autoLoad: true
    },
});