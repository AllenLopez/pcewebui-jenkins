Ext.define('Pce.ingresos.asegurado.beneficiario.BeneficiarioForma', {
    extend: 'Ext.form.Panel',
    xtype: 'beneficiario-test-forma',

    requires: [
        'Pce.ingresos.asegurado.BeneficiarioTestamentStore',
        'Pce.ingresos.asegurado.beneficiario.TipoBeneficiarioSelector',
        'Pce.ingresos.asegurado.beneficiario.ParentescoBeneficiarioSelector'
    ],

    layout: {
        type: 'table',
        columns: 2
    },

    bodyPadding: 10,
    defaults: {
        width: '100%'
    },

    items:[{
        xtype: 'textfield',
        name: 'nombrePersona',
        fieldLabel: 'Nombre',
        allowBlank: false,
        minLength: 10,
        fieldStyle: 'text-transform:uppercase',
        colspan: 3,
    }, {
        xtype: 'textfield',
        name: 'direccion',
        fieldLabel: 'Direccion',
        minLength: 10,
        fieldStyle: 'text-transform:uppercase',
        colspan: 3,
    }, {
        xtype: 'textfield',
        name: 'telefono',
        fieldLabel: 'Telefono',
        maxLength: 10,
        minLength: 10,
        colspan: 1,
    }, {
        xtype: 'textfield',
        name: 'rfc',
        fieldLabel: 'RFC',
        maxLength: 13,
        fieldStyle: 'text-transform:uppercase',
        colspan: 1,
    }, {
        xtype: 'textfield',
        name: 'ciudad',
        fieldLabel: 'Ciudad',
        fieldStyle: 'text-transform:uppercase',
        colspan: 1,
    }, {
        xtype: 'numberfield',
        name: 'porcentaje',
        reference: 'porcentaje',
        allowBlank: false,
        fieldLabel: '%',
        maxValue: 100,
        minValue: 1,
        validator: (val) => {
            let errMsg = 'El valor no debe execer el 100%';
            return (val <= 100) ? true : errMsg;
        },
        colspan: 1
    }, {
        xtype: 'parentesco-beneficiario-selector',
        name: 'parentescoBeneficiarioId',
        colspan: 1
    }, {
        xtype: 'tipo-beneficiario-selector',
        name: 'tipoBeneficiarioId',
        colspan: 1
    }, ]
});