Ext.define('Pce.ingresos.asegurado.EstadoDeCuentaAdminPanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.asegurado-estado-de-cuenta-admin-panel',

    requires: [
        'Pce.ingresos.asegurado.MovimientoTabla',
        'Pce.ingresos.asegurado.MovimientoStore'
    ],

    numeroAfiliacion: undefined,

    afterRender() {
        this.cargarAfiliado(this.view.numeroAfiliacion);
    },

    cargarAfiliado(numeroAfiliacion) {
        let selectorAsegurado = this.lookup('aseguradoSelector');
        let tabla = this.lookup('tablaEstadosDeCuenta');

        this.view.numeroAfiliacion = numeroAfiliacion;
        if (!numeroAfiliacion) {
            return;
        }

        Pce.afiliacion.asegurado.Asegurado.load(numeroAfiliacion, {
            success: (record) => {
                this.getViewModel().set('afiliado', record);

                selectorAsegurado.getStore().loadData([record]);
                selectorAsegurado.select(record);
            }
        });

        tabla.getStore().loadData([]);
        this.cargarEstadosDeCuenta(this.view.numeroAfiliacion);
    },

    onCambioAfiliado(selector, numeroAfiliacion) {
        this.cargarAfiliado(numeroAfiliacion);
    },

    cargarEstadosDeCuenta(afiliado) {
        if (!afiliado) {
            return;
        }
        let store = this.lookup('tablaEstadosDeCuenta').getStore();
        store.load({
            params: {
                numeroAfiliacion: afiliado
            }
        });
    },

    onConsultarMovimientos(grid, row) {
        let edoCta = grid.getStore().getAt(row);

        Ext.create({
            xtype: 'window',
            title: edoCta.get('conceptoDescripcion'),
            defaultListenerScope: true,
            autoShow: true,
            modal: true,
            height: '90vh',
            width: '95vw',
            layout: 'fit',
            items: {
                xtype: 'movimiento-tabla',
                edoCta: edoCta,
                numeroAfiliacion: this.lookup('aseguradoSelector').getValue()
            },
            listeners: {
                beforeClose(panel) {
                    panel.down('movimiento-tabla').getStore().removeAll();
                }
    }
        });
    },


});