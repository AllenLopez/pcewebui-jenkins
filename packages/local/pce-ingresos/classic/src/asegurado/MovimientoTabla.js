const getCurrentYear = () => {
    return new Date().getFullYear();
};

const getExercices = () => {
    let val = [];
    for (let i = 1900; i <= getCurrentYear(); i++) {
        val.push({ 'ejercicio': i });
    }
    return val;
};

const localExcercices = Ext.create('Ext.data.Store', {
    alias: 'store.excercices',
    fields: ['ejercicio'],
    data: getExercices(),
    sorters: [{
        property: 'ejercicio',
        direction: 'DESC'
    }]
});

Ext.define('Pce.ingresos.asegurado.MovimientoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'movimiento-tabla',

    sortableColumns: false,

    edoCta: null,
    numeroAfiliacion: null,

    dockedItems: [{
        xtype: 'toolbar',
        reference: 'consultaEdoCtaForma',
        items: [{
            xtype: 'combobox',
            store: localExcercices,
            name: 'desde',
            queryMode: 'local',
            fieldLabel: 'A partir de',
            displayField: 'ejercicio',
            valueField: 'ejercicio',
            allowBlank: false,
            blankText: 'Este campo es obligatorio',
            value: getCurrentYear() - 2,
        }, {
            xtype: 'combobox',
            store: localExcercices,
            name: 'hasta',
            fieldLabel: 'Hasta',
            displayField: 'ejercicio',
            valueField: 'ejercicio',
            allowBlank: false,
            blankText: 'Este campo es obligatorio',
            value: getCurrentYear(),
        }, {
            text: 'Buscar',
            handler() {
                let grid = this.up('movimiento-tabla');
                let store = grid.getStore();
                let desde = Ext.ComponentQuery.query('[name=desde]')[0].getValue();
                let hasta = Ext.ComponentQuery.query('[name=hasta]')[0].getValue();

                store.setProxy({
                    type: 'rest',
                    url: '/api/ingresos/asegurado/movimiento/por-asegurado-concepto-periodo',
                });

                grid.setLoading(true);

                store.load({
                    params: {
                        conceptoId: grid.edoCta.get('conceptoId'),
                        subcuentaId: grid.edoCta.get('subcuentaId'),
                        numeroAfiliacion: grid.numeroAfiliacion,
                        ejercicioInicial: desde,
                        ejercicioFinal: hasta
                    },
                    callback(store, records, success) {
                        if(success) {
                            grid.setLoading(false);
                        }
                    }
                });
            },
            width: 110
        }, {
            text: 'Generar reporte',
            handler() {
                let grid = this.up('movimiento-tabla');
                let numAfil = grid.numeroAfiliacion;
                let conceptoId = grid.edoCta.get('conceptoId');
                let concepto = grid.edoCta.get('conceptoDescripcion');

                let desde = Ext.ComponentQuery.query('[name=desde]')[0].getValue();
                let hasta = Ext.ComponentQuery.query('[name=hasta]')[0].getValue();

                window.open(`/api/afiliacion/asegurado/reporte-edo-cuenta?numAfil=${numAfil}&conceptoId=${conceptoId}&concepto=${concepto}&fecha1=${desde}&fecha2=${hasta}`);
            }
        }, {
            text: 'Generar reporte por ejercicio',
            handler() {
                let grid = this.up('movimiento-tabla');
                let numAfil = grid.numeroAfiliacion;
                let conceptoId = grid.edoCta.get('conceptoId');
                let concepto = grid.edoCta.get('conceptoDescripcion');

                let desde = Ext.ComponentQuery.query('[name=desde]')[0].getValue();
                let hasta = Ext.ComponentQuery.query('[name=hasta]')[0].getValue();

                window.open(`/api/afiliacion/asegurado/reporte-edo-cuenta-ejercicio?numAfil=${numAfil}&conceptoId=${conceptoId}&concepto=${concepto}&fecha1=${desde}&fecha2=${hasta}`);
            }
        },{
            text: 'Generar reporte por ejercicio excel',
            handler() {
                let grid = this.up('movimiento-tabla');
                let numAfil = grid.numeroAfiliacion;
                let conceptoId = grid.edoCta.get('conceptoId');
                let concepto = grid.edoCta.get('conceptoDescripcion');

                let desde = Ext.ComponentQuery.query('[name=desde]')[0].getValue();
                let hasta = Ext.ComponentQuery.query('[name=hasta]')[0].getValue();

                window.open(`/api/afiliacion/asegurado/reporte-edo-cuenta-ejercicio-excel?numAfil=${numAfil}&conceptoId=${conceptoId}&concepto=${concepto}&fecha1=${desde}&fecha2=${hasta}`);
            }
        }]
    }],

    columns: [{
        text: 'Tipo Movimiento',
        dataIndex: 'tipoMovimientoDescripcion',
        flex: 2
    }, {
        text: 'Folio transacción',
        dataIndex: 'transaccionId',
        flex: 1
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaMovimientoDescripcionCompleta',
        flex: 3
    }, {
        text: 'Ejercicio',
        dataIndex: 'ejercicio',
        width: 80
    }, {
        text: 'Fecha creación',
        dataIndex: 'fechaCreacion',
        renderer(v) { return Ext.util.Format.date(v, 'd/m/Y H:i:s'); },
        flex: 1
    }, {
        text: 'Fecha movimiento',
        dataIndex: 'fechaMovimiento',
        flex: 1,
        renderer(v) { return Ext.util.Format.date(v, 'd/m/Y'); }
    }, {
        text: 'Cargo',
        dataIndex: 'importe',
        renderer(v, meta, movimiento) {
            if(movimiento.get('tipo') === 'CARGO') {
                return Ext.util.Format.usMoney(v);
            } else {
                return '';
            }
        },
        flex: 1
    }, {
        text: 'Abono',
        dataIndex: 'importe',
        renderer(v, meta, movimiento) {
            if(movimiento.get('tipo') === 'ABONO') {
                return Ext.util.Format.usMoney(v);
            } else {
                return '';
            }
        },
        flex: 1,
    }, {
        text: 'Saldo',
        dataIndex: 'importeNeto',
        renderer(v, meta, movimiento) {
            if (!movimiento.saldo) {
            
                let store = this.store.data.length;
                if(this.contador == NaN || this.contador == undefined){
                  this.contador = 0;
                }
                this.contador += 1;
                if (movimiento.get('tipoMovimientoDescripcion') !== 'SALDO ANTERIOR') {
                    this.saldoAnterior = this.saldoAnterior || 0;
                    movimiento.saldo = this.saldoAnterior + v;
                }
                else {
                    movimiento.saldo = v;
                }
                this.saldoAnterior = movimiento.saldo;
                if(store == this.contador){
                this.contador = 0;
                this.saldoAnterior = 0;
                }
            }
            return Ext.util.Format.usMoney(movimiento.saldo);
        },
        flex: 1
    }],
});