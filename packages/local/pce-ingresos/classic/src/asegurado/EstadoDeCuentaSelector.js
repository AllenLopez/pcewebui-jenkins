/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.EstadoDeCuentaSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'asegurado-estado-de-cuenta-selector',

    name: 'estadoDeCuentaId',

    fieldLabel: 'Estado de cuenta',
    labelWidth: 120,
    valueField: 'id',
    displayField: 'conceptoDescripcion',
    queryMode: 'local',
    forceSelection: true,
    editable: false,

    displayTpl: [
        '<tpl for=".">',
            '{conceptoDescripcion} [Subcuenta: {subcuentaDescripcion}] (Saldo: {saldo:usMoney})',
        '</tpl>'
    ],
    tpl: [
        '<tpl for=".">',
            '<li role="option" class="x-boundlist-item">',
                '<b>{conceptoDescripcion}</b> [Subcuenta: {subcuentaDescripcion}] (Saldo: {saldo:usMoney})',
            '</li>',
        '</tpl>',
    ]

});