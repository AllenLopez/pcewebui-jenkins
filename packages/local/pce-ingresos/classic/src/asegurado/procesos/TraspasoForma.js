/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.TraspasoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'asegurado-traspaso-forma',

    title: 'Movimiento de traspaso',
    layout: 'vbox',
    items: [{
        xtype: 'hiddenfield',
        name: 'tipoMovimiento',
        value: 'TRA'
    }, {
        xtype: 'asegurado-selector',
        name: 'numeroAfiliacion',
        width: '50%',
        allowBlank: false,
        bind: {
            selection: '{afiliado}'
        },
        listeners:{
            change(comp,numAfil){
                if(!numAfil){
                    return;
                }
                let numDerechohabiente = comp.lastSelection[0].getData().numDerechohabiente;
                Ext.Ajax.request({
                    url: `/api/afiliacion/asegurado/empleos/${numDerechohabiente}`,
                    method: 'GET',
                    callback(op, success, response){
                        let storeDep = Ext.getCmp('dependencia').getStore();
                        let dependencias = JSON.parse(response.responseText);
                        let selectedItems = [];
                        dependencias.forEach(dep => {
                            let found = selectedItems.find(x=> x === dep.idDependencia);
                            if(!found){
                                selectedItems.push(dep.idDependencia);
                            }
                        });
                        storeDep.clearFilter();
                        storeDep.filterBy(record => Ext.Array.indexOf(selectedItems, record.get('id')) !== -1, this);
                        
                    }   
                });
            }
        }
    }, {
        xtype: 'toolbar',
        hidden: true,
        bind: {
            hidden: '{!afiliado}'
        },
        items: [{
            xtype: 'asegurado-displayer'
        }]
    }, {
        xtype: 'dependencia-selector',
        name: 'dependencia',
        id: 'dependencia',
        width: '50%',
        allowBlank: false
    }, {
        xtype: 'concepto-selector',
        name: 'conceptoOrigen',
        fieldLabel: 'Del concepto',
        labelWidth: 100,
        width: '50%',
        allowBlank: false
    }, {
        xtype: 'concepto-selector',
        name: 'conceptoDestino',
        fieldLabel: 'Al concepto',
        labelWidth: 100,
        width: '50%',
        allowBlank: false
    }, {
        xtype: 'numberfield',
        name: 'importe',
        fieldLabel: 'Importe',
        labelWidth: 100,
        width: 280,
        allowBlank: false
    }]
});