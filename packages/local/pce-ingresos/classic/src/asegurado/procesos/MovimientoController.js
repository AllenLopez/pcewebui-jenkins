/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.MovimientoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.asegurado-movimiento',

    requires: [
        'Pce.ingresos.transaccion.ResultadoDialogo',
        'Pce.ingresos.transaccion.Transaccion'
    ],

    onProcesarMovimientoTap() {
        let forma = this.lookup('movimientoForma');
        if (forma.isValid()) {
            this.confirmarProcesoDeMovimiento().then(ans => {
                if (ans === 'yes') {
                    forma.submit({
                        success: this.onProcesarMovimientoSuccess,
                        failure: this.onProcesarMovimientoFailure
                    });
                }
            });
        }
    },

    onProcesarMovimientoSuccess(form, action) {
        let transaccionId = action.result.transaccionId;
        Ext.Msg.alert(
            'Movimiento procesado',
            `Movimiento procesado exitosamente bajo la transacción <b>${transaccionId}</b>`
        );
        form.reset();
    },
    onProcesarMovimientoFailure(form, action) {
        let transaccion = new Pce.ingresos.transaccion.Transaccion({
            id: action.result.transaccionId
        });
        Ext.Msg.alert(
            'Error',
            `Error al intentar procesar el movimiento: ${action.result.mensaje}`,
            () => {
                Ext.create('Pce.ingresos.transaccion.ResultadoDialogo', {
                    transaccion
                });
            }
        );
    },
    confirmarProcesoDeMovimiento() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                'Se efectuara el proceso del movimiento y se afectarán los ' +
                'estados de cuenta que correspondan. ¿Deseas proceder?',
                ans => resolve(ans)
            );
        });
    }
});