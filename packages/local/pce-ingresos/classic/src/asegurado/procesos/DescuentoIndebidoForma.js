/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.DescuentoIndebidoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'descuento-indebido-forma',

    requires: [
        'Pce.ingresos.asegurado.EstadoDeCuentaSelector',
        'Pce.ingresos.asegurado.EstadoDeCuentaStore'
    ],

    layout: {
        type: 'vbox'
    },
    defaults: {
        width: 600
    },
    bodyPadding: 10,
    items: [{
        xtype: 'hiddenfield',
        name: 'tipoMovimiento',
        value: 'DIN'
    }, {
        xtype: 'asegurado-selector',
        name: 'numeroAfiliacion',
        allowBlank: false,
        listeners: {
            change: 'onAseguradoChange'
        }
    }, {
        xtype: 'dependencia-selector',
        name: 'dependencia',
        reference: 'dependenciaSelector',
        allowBlank: false,
        store: {
            type: 'dependencias',
            proxy: {type: 'ajax', url: '/api/afiliacion/dependencia/por-asegurado'}
        }
    }, {
        xtype: 'asegurado-estado-de-cuenta-selector',
        name: 'subEstadoDeCuenta',
        reference: 'estadoDeCuentaSelector',
        allowBlank: false,
        valueField: 'subcuentaId',
        store: {
            type: 'asegurado-estado-de-cuenta',
            filters: [{
                property: 'conceptoClave',
                value: 'C'
            }]
        }
    }, {
        xtype: 'combobox',
        name: 'motivo',
        fieldLabel: 'Motivo',
        displayField: 'desc',
        valueField: 'desc',
        allowBlank: false,
        store: {
            fields: ['desc'],
            data: [
                {desc: 'Préstamo liquidado'},
                {desc: 'Descuento mayor'},
                {desc: 'Doble plaza'},
                {desc: 'Bonificación de intereses'},
                {desc: 'Cobro por aval'},
                {desc: 'Plaza interna'},
                {desc: 'No tiene préstamo'},
                {desc: 'Otros'}
            ]
        }
    }, {
        xtype: 'datefield',
        name: 'quincenaInicio',
        fieldLabel: 'Descontado de la quincena',
        tooltip:'Quincena en la que se inicio el descuento',
        labelWidth: 200,
        width: 350,
        allowBlank: false
    }, {
        xtype: 'datefield',
        name: 'quincenaFin',
        fieldLabel: 'A la quincena',
        labelWidth: 200,
        width: 350,
        allowBlank: false
    }, {
        xtype: 'textarea',
        name: 'observaciones',
        fieldLabel: 'Observaciones',
        tooltip:'Observaciones para la devolución del descuento indebido',
    }, {
        xtype: 'numberfield',
        name: 'importe',
        fieldLabel: 'Importe',
        allowBlank: false,
        tooltip:'Importe de descuento indebido',
        width: 250
    }],
    buttons: [{
        text: 'Procesar',
        handler: 'onProcesarTap',
        tooltip:'Realizar descuento indebido'
    }]
});