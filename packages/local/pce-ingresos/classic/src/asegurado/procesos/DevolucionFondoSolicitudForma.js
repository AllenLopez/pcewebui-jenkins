/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.DevolucionFondoSolicitudForma', {
    extend: 'Ext.form.Panel',
    xtype: 'fondo-devolucion-forma',

    title: 'Solicitud de devolución de fondo propio',

    bodyPadding: 10,
    layout: {
        type: 'table',
        columns: 6
    },

    defaults: {
        width: '100%'
    },
    items: [{
        xtype: 'asegurado-selector',
        allowBlank: false,
        name: 'asegurado',
        colspan: 4,
        bind: {
            selection: '{asegurado}'
        },
        listeners: {
            change: 'onAseguradoChange'
        }
    }, {
        xtype: 'textfield',
        readOnly: true,
        cls: 'read-only',
        fieldLabel: 'Núm. afil.',
        labelWidth: 80,
        colspan: 1,
        bind: {
            value: '{asegurado.id}'
        }
    }, {
        xtype: 'textfield',
        readOnly: true,
        cls: 'read-only',
        fieldLabel: 'RFC',
        labelWidth: 40,
        colspan: 1,
        bind: {
            value: '{asegurado.rfc}'
        }
    }, {
        xtype: 'textfield',
        readOnly: true,
        cls: 'read-only',
        fieldLabel: 'Dirección',
        colspan: 4,
        bind: {
            value: '{asegurado.direccion}'
        }
    }, {
        xtype: 'textfield',
        readOnly: true,
        cls: 'read-only',
        fieldLabel: 'Teléfono',
        colspan: 1,
        bind: {
            value: '{asegurado.telefono2}'
        }
    }, {
        xtype: 'textfield',
        readOnly: true,
        cls: 'read-only',
        fieldLabel: 'Delegación',
        colspan: 1,
        bind: {
            value: '{aseguradoInfo.delegacion}'
        }
    }, {
        xtype: 'concepto-selector',
        name: 'concepto',
        labelWidth: 100,
        colspan: 2,
        allowBlank: false,
        store: {
            type: 'conceptos',
            autoLoad: true,
            filters: [{
                property: 'clave',
                operator: 'in',
                value: ['A', 'H', 'I']
            }]
        },
        bind: {
            disabled: '{!asegurado}'
        }
    }],

    bbar: [{
        text: 'Guardar nueva solicitud',
        handler: 'onGuardarNuevaSolicitudTap',
        tooltip:'Realizar nueva solicitud',
        formBind: true
    }]
});