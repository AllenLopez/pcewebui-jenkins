/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.SolicitudDevolucionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'fondo-solicitud-devolucion-tabla',

    requires: [
        'Pce.ingresos.asegurado.fondo.SolicitudDevolucionStore'
    ],

    columns: [{
        text: 'Folio',
        dataIndex: 'folio',
        tootip:'Folio de la solicitud',
        flex: 1
    }, {
        text: 'Numero de afiliación',
        dataIndex: 'asegurado',
        tootip:'Numero de afiliación del asegurado',
        flex: 1
    }, {
        text: 'Concepto',
        dataIndex: 'conceptoDesc',
        tootip:'Concepto de la solicitud de devolución de fondo propio',
        flex: 2
    }, {
        text: 'Etapa',
        dataIndex: 'etapa',
        tootip:'Etapa de la solicitud de fondo propio',
        flex: 1
    }]
});