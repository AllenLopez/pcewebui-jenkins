/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.CertificadoDependenciaDialogo
 * @extends Ext.window.Window
 * @xtype dialogo-certificado-dependencia
 * Pantalla de captura de adeudo en dependencia donde el asegurado ha sido afiliado
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.CertificadoDependenciaDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'dialogo-certificado-dependencia',

    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text'
    ],

    autoShow: true,
    modal: true,
    width: 700,

    defaultListenerScope: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    viewModel: {
        data: {
            dependencia: null
        }
    },

    record: null,
    aseguradoId: null,

    items: [{
        xtype: 'form',
        bodyPadding: 10,
        layout: 'form',
        items: [{
            xtype: 'combobox',
            reference: 'dependenciaAseguradoSelector',
            name: 'numeroDependencia',
            fieldLabel: 'Dependencia',
            displayField: 'dependenciaDescripcion',
            valueField:'numeroDependencia',
            queryMode: 'local',
            store: {
                type: 'dependencia-asegurado',
            },
            bind: {
                selection: '{dependencia}'
            }
        },{
            xtype: 'hidden',
            name: 'dependenciaDescripcion',
            bind: {
                value: '{dependenciaAseguradoSelector.selection.dependenciaDescripcion}'
            }
        }, {
            xtype: 'numberfield',
            name: 'importe',
            fieldLabel: 'Importe'
        }, {
            xtype: 'datefield',
            name: 'fechaUltimosRegistros',
            fieldLabel: 'Registros al',
            maxValue: new Date(),
            renderer(v) {
                return Ext.util.Format.date(v, 'd/m/Y');
            }
        }],

        buttons: [{
            text: 'Aceptar',
            handler: 'onAceptarTap'
        }, {
            text: 'Cancelar',
            handler: 'onCancelarTap'
        }],
    }],

    beforeRender() {
        let store = this.down('form').getForm().findField('numeroDependencia').getStore();
        store.load({
            params: {
                numeroAfiliacion: this.aseguradoId
            }
        });
    },

    afterRender() {
        this.callParent();
        if (this.record) {
            this.down('form').loadRecord(this.record);
        }
    },

    onAceptarTap() {
        let form = this.down('form');
        if(form.isValid()) {
            form.updateRecord();
            this.fireEvent('guardar', this, this.record, form);
        }
    },

    onCancelarTap() {
        this.close();
    },
});