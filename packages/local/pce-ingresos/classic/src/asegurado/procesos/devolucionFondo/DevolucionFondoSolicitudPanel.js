Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'fondo-solicitud-devolucion-panel',

    require: [
        'Pce.ingresos.asegurado.beneficiario.BeneficiarioForma',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudForma',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.CertificacionDependenciaForma',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.CertificacionServicioMedico',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.EmisionChequeFondoTabla',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.ChequeFondoForma',
        'Pce.ingresos.asegurado.BeneficiarioTabla',
        'Pce.ingresos.asegurado.EstadoDeCuentaTabla',
        'Pce.ingresos.asegurado.EstadoDeCuentaStore',

        'Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudController',
    ],

    controller: 'devolucion-solicitud',

    viewModel: {
        data: {
            asegurado: null,
            aseguradoInfo: null,
            fallecido: false,
            adeudaDep: false,
            adeudaSM: false,
            totalBeneficiarios: 0,
            importeDep: 0,
            importeSM: 0,
            importeRestante: 0,
            usuarioCertificaDependencia: null,
            usuarioCertificaSM: null,
            importeFondo: 0,
            totalFondo: 0,
            fechaRegistrosDep: null,
            fechaCaptura: new Date(),
            solicitudValida: false,
            bajaDependencias: false
        },
        formulas: {
            totalFondo(get) {
                if(!get('fallecido')){
                    return get('importeFondo') - get('importeDep') - get('importeSM');
                }
                else{  
                    return this.data.totalFondo;
                }
            }
        }
    },

    title: 'Solicitud de devolución de fondo propio',

    scrollable: 'y',

    defaults: {
        padding: '20 0'
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [{
        xtype: 'fondo-solicitud-devolucion-forma',
        reference: 'solicitudAseguradoForma',
        padding: 10
    }, {
        xtype: 'certificacion-dependencia-forma',
        reference: 'certificacionDependenciaForma',
        bind:{
            disabled: '{!solicitudValida}'
        },
        collapsible: true,
    }, {
        xtype: 'certificacion-servicio-medico-forma',
        reference: 'certificacionServicioMedico',
        bind: {
            disabled: '{!solicitudValida}'
        },
        collapsible: true,
    }, {
        xtype: 'asegurado-estado-de-cuenta-tabla',
        reference: 'aseguradoEstadoCuentaTabla',
        title: 'Estado de cuenta del asegurado',
        store: {
            type: 'asegurado-estado-de-cuenta',
        },
        emptyText: 'No existen datos para este asegurado con este concepto',
        columns: [{
            text: 'Clave',
            dataIndex: 'conceptoClave',
            tooltip: `Identificador del concepto`,
        }, {
            text: 'Concepto',
            dataIndex: 'conceptoDescripcion',
            tooltip: `Nombre del concepto que se carga al estado de cuenta del afiliado`,
            flex: 1
        }, {
            text: 'Subcuenta',
            dataIndex: 'subcuentaDescripcion',
            flex: 1,
        }, {
            text: 'Saldo',
            dataIndex: 'saldo',
            tooltip: `Total de cada concepto descontado del salario del afiliado.`,
            renderer: Ext.util.Format.usMoney,
            flex: 1
        }],
        collapsible: true,
        bind:{
            disabled: '{!solicitudValida}'
        },
        viewConfig: {
            listeners: {
                refresh: 'onValidaConceptoImportePorRegistro'
            }
        },
    }, {
        xtype: 'beneficiario-tabla',
        reference: 'beneficiarioTabla',
        store:{
            type: 'beneficiario-testament'
        },
        bind: {
            disabled: '{!fallecidoCheck.checked}'
        },
        collapsible: true,
    }, {
        xtype: 'emision-cheque-fondo-tabla',
        reference: 'emisionChequeTabla',
        bind:{
            disabled: '{!solicitudValida}'
        },
        collapsible: true,
    }],

    buttons: [{
        text: 'Guardar',
        reference: 'tapGuardar',
        handler: 'onGuardarNuevaSolicitudTap',
        bind: {
            disabled: '{!solicitudValida}'
        }
    }, {
        text: 'Cancelar',
        handler: 'onCancelarSolicitudTap',
    }],
});