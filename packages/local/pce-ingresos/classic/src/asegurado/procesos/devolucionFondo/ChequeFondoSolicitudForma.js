Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.ChequeFondoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'cheque-fondo-forma',

    requires: [
        'Ext.form.field.Text'
    ],

    layout: {
        type: 'table',
        columns: 4
    },

    bodyPadding: 10,
    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'textfield',
        name: 'beneficiario',
        fieldLabel: 'Nombre',
        allowBlank: false,
        minLength: 10,
        colspan: 4
    }, {
        xtype: 'textfield',
        name: 'rfc',
        fieldLabel: 'RFC',
        minLength: 12,
        maxLength: 13,
        colspan: 2
    }, {
        xtype: 'numberfield',
        name: 'importe',
        fieldLabel: 'Importe',
        allowBlank: false,
        minValue: 0.01,
        maxValue: 99999999999.99,
        colspan: 2
    }]
});