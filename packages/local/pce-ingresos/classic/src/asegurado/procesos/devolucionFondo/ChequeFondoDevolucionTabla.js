/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.ChequeFondoDevolucionTabla
 * @extends Ext.grid.Panel
 * @xtype cheque-devolucion-tabla
 * Tabla que enlista los detalles de las solicitudes realizadas
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.ChequeFondoDevolucionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'cheque-devolucion-tabla',

    requires: [
        'Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitudStore'
    ],

    store: {
        type: 'cheque-fondo-solicitud'
    },

    title: 'Detalles de emisión de cheques',

    defaults: {
        width: '100%'
    },

    emptyText: 'No hay cheques agregados',

    columns: [{
        text: 'Beneficiario',
        dataIndex: 'beneficiario',
        flex: 6
    },{
        text: 'RFC',
        dataIndex: 'rfc',
        flex: 3
    },{
        text: 'Importe',
        dataIndex: 'importe',
        renderer(v) {
            return Ext.util.Format.usMoney(v);
        },
        flex: 3
    }, {
        text: 'Estatus',
        dataIndex: 'estatus',
        renderer(v) {
            switch (v) {
                case 'TR': return 'EN TRAMITE';
                case 'CA': return 'CANCELADO';
                case 'AU': return 'AUTORIZADO';
                default: return 'OTRO';
            }
        },
        flex: 2
    }]
});