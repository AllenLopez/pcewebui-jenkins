/**
 * @author Jorge Escamilla, Alan López
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudForma', {
    extend: 'Ext.form.Panel',
    xtype: 'fondo-solicitud-devolucion-forma',

    requires: [
        'Pce.afiliacion.delegacion.DelegacionStore'
    ],

    layout: {
        type: 'table',
        columns: 7
    },

    bodypadding: 10,

    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'textfield',
        fieldLabel: 'Núm. afil.',
        readOnly: true,
        cls: 'read-only',
        colspan: 2,
        bind: {
            value: '{asegurado.id}'
        }
    }, {
        xtype: 'asegurado-selector',
        reference: 'aseguradoSelector',
        allowBlank: false,
        name: 'asegurado',
        bind: {
            selection: '{asegurado}'
        },
        listeners: {
            change: 'onAseguradoChange'
        },
        width: '569px',
        colspan: 4,
    },{
        xtype: 'textfield',
        fieldLabel: 'Dirección',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.direccion}'
        },
        width: '400px',
        colspan: 3,
    }, {
        xtype: 'textfield',
        name: 'rfc',
        fieldLabel: 'RFC',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.rfc}'
        },
        colspan: 2,
    },{
        xtype: 'checkbox',
        name: 'defuncion',
        reference: 'fallecidoCheck',
        fieldLabel: 'Fallecido',
        bind: {
            disabled: '{!asegurado}'
        },
        listeners: {
            change: 'onFallecidoCheck'
        }
        // colspan: 1
    },{
        xtype: 'numberfield',
        name: 'telefono',
        fieldLabel: 'Teléfono',
        maxLength: 10,
        bind: {
            disabled: '{!asegurado}',
            value: '{asegurado.telefono2}'
        },
        width: '280px',
        colspan: 2
    },{
        xtype: 'dependencia-default-selector',
        reference: 'dependenciaSelector',
        name: 'ultimaDependencia',
        displayField: 'descripcion',
        allowBlank: false,
        listeners: {
            change: 'onSeleccionDependencia'
        },
        bind: {
            value: '{ultimaDependenciaDesc}'
        },
        // width: '695px',
        width: '600px',
        colspan: 3
    },{
        xtype: 'textfield',
        name: 'jubilado',
        readOnly: true,
        cls: 'read-only',
        fieldStyle: 'font-weight: bold',
        fieldLabel: 'Estatus asegurado',
        colspan: 2,
        bind: {
            value: '{aseguradoInfo.estatus}'
        }
    },{
        xtype: 'datefield',
        name: 'fechaCaptura',
        reference: 'fechaCapturaSolicitud',
        fieldLabel: 'Fecha captura',
        bind: {
            disabled: '{!asegurado}',
            value: '{fechaCaptura}'
        },
        width: 280,
        colspan: 2
    },{
        xtype: 'textfield',
        readOnly: true,
        cls: 'read-only',
        fieldStyle: 'font-weight: bold',
        fieldLabel: 'Régimen',
        colspan: 2,
        bind: {
            value: '{aseguradoInfo.regimen}'
        }
    },{ 
        xtype: 'textfield',
        name: 'jubilado',
        readOnly: true,
        cls: 'read-only',
        fieldLabel: 'Jubilado',
        colspan: 2,
        bind: {
            value: '{aseguradoInfo.jubilado}'
        }
    },{
        xtype: 'combobox',
        name: 'delegacion',
        valueField: 'id',
        displayField: 'descripcion',
        fieldLabel: 'Delegación',
        queryMode: 'local',
        allowBlank: false,
        store: {
            type: 'delegacion',
            autoLoad: true
        },
        bind: {
            value: '{aseguradoInfo.delegacionId}'
        },
        colspan: 3,
    },{
        xtype: 'textfield',
        reference: 'observaciones',
        name: 'observaciones',
        fieldLabel: 'Observaciones',
        bind: {
            disabled: '{!asegurado}'
        },
        colspan: 4
    }]
});