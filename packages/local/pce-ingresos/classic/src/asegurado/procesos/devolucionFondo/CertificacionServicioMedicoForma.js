Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.CertificacionServicioMedico', {
    extend: 'Ext.form.Panel',
    xtype: 'certificacion-servicio-medico-forma',

    title: 'Certificación de servicio médico',

    layout: {
        type: 'hbox',
        // columns: 5
    },

    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'combobox',
        name: 'adeudaServicioMedico',
        reference: 'ServicioMedico',
        fieldLabel: 'Adeudo',
        displayField: 'adeudo',
        valueField: 'valor',
        allowBlank: false,
        store: {
            data: [
                {adeudo: 'NO', valor: false},
                {adeudo: 'SI', valor: true}
            ]
        },
        bind: {
            value: '{adeudaSM}',
        },
        value: false,
        padding: 10,
        colspan: 2,
        width: '200px'
    }, {
        xtype: 'numberfield',
        name: 'importeSM',
        reference: 'importeSM',
        fieldLabel: 'Importe',
        maxValue: 999999999.99,
        bind: {
            disabled: '{!adeudaSM}',
            value: '{importeSM}'
        },
        listeners: {
            change: 'onTotalFondoChange'
        },
        renderer(v) {
            return `<b>${Ext.util.Format.usMoney(v)}</b>`;
        },
        padding: 10,
        colspan: 2,
        width: '200px'
    }, {
        xtype: 'textfield',
        name: 'usuarioCertificaSM',
        fieldLabel: 'Usuario certificador',
        bind: {
            value: '{usuarioCertificaSM}'
        },
        labelWidth: 150,
        width: '400px',
        padding: 10,
        colspan: 5
    },{
        xtype: 'button',
        reference: 'botonGenerarChequeSm',
        text: 'Generar cheque',
        ui: 'default',
        handler: 'onGenerarChequeSM',
        bind: {
            disabled: '{!adeudaSM}'
        },
        // padding: 10,
        colspan: 2,
        margin: 10,
        width: '130px'
    }]
});