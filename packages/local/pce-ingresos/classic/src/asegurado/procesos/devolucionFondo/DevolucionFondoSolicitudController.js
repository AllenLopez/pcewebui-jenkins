/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudController
 * @extends Ext.app.ViewController
 * @alias controller.devolucion-solicitud
 * @author Alan López
 * Controlador de pantalla de captura de datos de solicitud de devolución de fondo propio
 */

Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.devolucion-solicitud',

    require: ['Pce.view.dialogo.FormaContenedor'],

    helperDevolucion: Pce.asegurado.procesos.devolucionFondo.ActualizaSolicitudDevolucionHelper,
    conceptosTab: {'H': 'TR', 'A': 'LA', 'I': 'CI', 'J': 'CI'},
    registros: 0,
    porcentajeTotal: 0,
    solicitud: null,
    fallecido: false,
    datosPce: null,
    asegurado: null,
    validado: false,

    init() {
        // let storeCheques = this.obtieneStoreChequesTabla();
        // storeCheques.addListener('update', 'onActualizaCheques', this);
    },

    onAgregarNuevoBenecificiario() {
        if(this.registros.length <= 0){
            this.onEditarTotalDevolucionXbenef();
        }else{
            let beneficiario = new Pce.ingresos.asegurado.BeneficiarioTestament();
            this.mostrarDialogoBeneficiario(beneficiario); 
        }        
    },

    onEditarTotalDevolucionXbenef() {
        let totalFondo = this.getViewModelDataProp('totalFondo');
        let formPanel = Ext.create('Ext.form.Panel', {
            reference: 'formTotalFondo',
            items: [{
                xtype: 'numberfield',
                name: 'totalFondo',
                allowBlank: false,
                minValue: 1,
                fieldLabel: 'NETO A PAGAR',
                value: totalFondo
            }]
        });

        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'CONFIRMAR NETO A PAGAR',
            
            defaultListenerScope: true,
            modal: true,
            autoShow: true,
            layout: 'fit',
            bodyPadding: 10,
            buttons: [{
                text: 'Aceptar',
                handler: 'onAceptar'
            }, {
                text: 'Cancelar',
                handler: 'onCancelar'
            }],
            items: [{
                xtype: formPanel
            }],
            onAceptar() {
                let form = this.down('form');
                if (form.isValid()) {
                    this.fireEvent('guardar', this, form.getForm().getFieldValues().totalFondo);
                }
            },
            onCancelar() {
                this.close();
            },
            listeners: {
                guardar: this.onEditarTotalFondoxBenef.bind(this)
            }
        });
    },

    onEditarTotalFondoxBenef(dialogo, valor) {
        let importeFondo = this.getViewModelDataProp('importeFondo');

        if(valor <= importeFondo) {
            this.setViewModelDataProp('totalFondo', valor);
            // this.editarImporteBeneficiario(valor);
            let beneficiario = new Pce.ingresos.asegurado.BeneficiarioTestament();
            this.mostrarDialogoBeneficiario(beneficiario); 
        }else{
        this.mostrarMensajeAlerta('Importe Invalido', 'Favor de ingresar un importe valido' );
        }

        dialogo.close();
        
    },
    onAgregarNuevoAdeudoDependencia() {
        let adeudo = new Pce.ingresos.asegurado.fondo.CertificacionDependenciaDevolucion();
        let aseguradoId = this.getViewModelDataProp('asegurado').getId();

        this.mostrarDialogoCertificacionDependencia(adeudo, aseguradoId);
    },

    onEditarCheque(view, row, col, item, e, cheque) {
        this.mostrarDialogoCheque(cheque);
    },

    // onActualizaCheques(store) {
    //     let monto = 0;
    //     store.each((record) => { monto += record.get('importe'); }, this);
    //     if(monto > 0) {
    //         this.setViewModelDataProp('totalFondo', monto);
    //     }
    // },

    onGuardarNuevaSolicitudTap() {
        let me = this;
        if(me.getViewModelDataProp('solicitudValida')) {
            if(!me.solicitud) {
                me.solicitud = new Pce.ingresos.asegurado.fondo.SolicitudDevolucion();
            }
            if(me.obtieneValoresFormularios()) {
                let asegurado = me.getViewModelDataProp('asegurado');
                let claveRegimen = me.getViewModelDataProp('aseguradoInfo').data.claveRegimen;
                let fallecido = me.getViewModelDataProp('fallecido');

                if(!me.solicitud.phantom) {
                    Ext.Msg.alert('Solicitud existente',
                        `La solicitud con folio: <b>${me.solicitud.getId()}</b> ya ha sido procesada`);
                    return;
                }
                if(this.porcentajeTotal < 100) {
                    Ext.Msg.alert('Solicitud Invalida',
                        `El porcentaje divido entre los beneficiarios debe ser el 100% del importe a devolver`);
                    return;
                }
                me.obtieneConceptoPorClaveRegimen(claveRegimen)
                .then(ans => {
                    me.solicitud.set('concepto', ans.id);

                me.confirmarCreacionDeSolicitud(asegurado).then(ans => {
                    if (ans === 'yes') {
                Ext.getBody().mask('Procesando solicitud...');
                me.crearSolicitud(me.solicitud)
                    .then(
                        solicitudId => {
                            me.guardarCheques(me.solicitud, solicitudId)
                                .then(
                                    totalSuccess => {
                                        let storeBenef = me.lookup('beneficiarioTabla').getStore();
                                        if(fallecido && storeBenef.getCount() > 0) {
                                            console.log('prepara beneficiarios...');
                                            me.guardarBeneficiarios(storeBenef, me.solicitud, solicitudId)
                                            .then(
                                                totalExito => {
                                                Ext.toast(`Nuevos beneficiarios procesados con exito: ${totalExito}`);
                                                Ext.toast(`Pagos procesados con exito: ${totalSuccess}`);
                                                Ext.Msg.alert(
                                                    'Solicitud completada',
                                                    `La solicitud se guardó exitósamente con el folio: <b>${me.solicitud.getId()}</b>`
                                                );
                                                Ext.getBody().unmask();
                                                me.imprimirSolicitudDevolucion(me.solicitud.get('asegurado'), solicitudId);
                                                
                                                this.validado = true;
                                                me.lookup('solicitudAseguradoForma').getForm().reset();
                                                me.resetViewModel();
                                                },
                                                failed => {
                                                    console.log(failed);
                                                    failed.forEach(ex => {
                                                        Ext.toast(ex);
                                                    });
                                                    Ext.getBody().unmask();
                                                
                                                });
                                        } else {
                                            Ext.toast(`Pagos procesados con exito: ${totalSuccess}`);
                                            Ext.Msg.alert(
                                                'Solicitud completada',
                                                `La solicitud se guardó exitósamente con el folio: <b>${me.solicitud.getId()}</b>`
                                            );

                                            me.imprimirSolicitudDevolucion(me.solicitud.get('asegurado'), solicitudId);
                                            this.validado = true;
                                            me.lookup('solicitudAseguradoForma').getForm().reset();
                                            me.resetViewModel();

                                            Ext.getBody().unmask();
                                        }
                                    });
                                    failure => {
                                        console.log(failure);
                                        failure.forEach(ex => {
                                            Ext.toast(ex);
                                        });
                                        Ext.getBody().unmask();
                                    }
                                
                            },
                        failure => {
                            Ext.Msg.alert('Error al procesar',
                                `Ocurrio un error al procesar solicitud: ${failure}`);
                            Ext.getBody().unmask();
                        }
                    );
                }
            });
        });
    }
        } else {
            this.mostrarMensajeAlerta(
                'Solicitud inválida',
                'La solicitud no cumple con las validaciones necesarias para proceder');
        }
    },

    confirmarCreacionDeSolicitud(asegurado) {
        let fechaCaptura = Ext.util.Format.date(this.getViewModelDataProp('fechaCaptura'), 'd/m/Y');
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se creará una nueva solicitud de devolución de fondo 
                propio para el asegurado <b>${asegurado.getId()}</b> (${asegurado.get('nombreCompleto')})
                con la fecha <b>${fechaCaptura}</b>
                ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    crearSolicitud(solicitud) {
        if(solicitud.phantom) {
            console.log('solicitud nueva');
            return new Promise((resolve, reject) => {
                solicitud.save({
                    success(record) { resolve(record.getId()); },
                    failure(record, oper) {
                        console.log(oper);
                        let error = oper.getError().responseText;
                        reject(error);
                    }
                });
            });
        }
    },
    onGenerarChequeSM(boton) {
        const importeSM = this.lookup('importeSM').value;
        let importeFondo = this.getViewModelDataProp('importeFondo');
        if(importeFondo >= importeSM){  
        if (!this.getStoreCheques().findRecord('tipoEmision', 'PCE')) {
 
            if (!this.datosPce) {
                this.cargaDatosPensiones()
                    .then(pce => {
                        this.datosPce = pce;
                        const chequeSM = this.generaChequeSm(this.datosPce, importeSM);
                        this.insertaNuevoCheque(chequeSM);
                    });
            } else {
                const chequeSM = this.generaChequeSm(this.datosPce, importeSM);
                this.insertaNuevoCheque(chequeSM);
            }

            boton.disable();
            this.lookup('importeSM').disable();
        } else {
            this.mostrarMensajeAlerta('Cheque existente','ya existe cheque para servicio médico');
        }
        }else{
            this.mostrarMensajeAlerta('Importe Invalido','El importe del cheque no puede ser mayor que el importe total del fondo');

        }
    },

    generaChequeSm(datosPce, importeSM) {
        return this.creaCheque({
            rfc: datosPce.get('rfc'),
            nombrePersona: datosPce.get('descripcion'),
            razonSocial: datosPce.get('razonSocial'),
            porcentaje: 100,
            importe: importeSM,
            tipoEmision: 'PCE',
        });
    },

    cargaDatosPensiones() {
        const PceId = 8; // Id de dependencia PCE
        return new Promise((resolve) => {
            Pce.afiliacion.dependencia.Dependencia.load(PceId, {
                success: model => { resolve(model); }
            });
        });
    },

    onGenerarChequeDependencia(boton) {
        const importeDependencia = this.lookup('importeDependencia').value;
        const dependenciaAdeudo = this.lookup('dependenciaSelector').getSelection().getData().id;
        let importeFondo = this.getViewModelDataProp('importeFondo');
        if(importeFondo >= importeDependencia){  
        if (!this.getStoreCheques().findRecord('tipoEmision', 'DEP')) {

            this.getDependencia(dependenciaAdeudo)
                .then(dependencia => {
                    const chequeDep = this.generaChequeDependencia(dependencia, importeDependencia);
                    this.insertaNuevoCheque(chequeDep);
                });

            boton.disable();
            this.lookup('importeDependencia').disable();

        } else {
            this.mostrarMensajeAlerta('Cheque existente','ya existe cheque para ésta dependencia');
        }
        }else{
            this.mostrarMensajeAlerta('Importe Invalido','El importe del cheque no puede ser mayor que el importe total del fondo');
        }
    },
    insertaNuevoCheque(cheque) {
        const store = this.getStoreCheques();

        store.insert(store.getCount() + 1, [cheque]);
    },
    preparaChequesStore() {
        const store = this.getStoreCheques();
        store.setProxy({
            type: 'rest',
            url: '/api/cajas/chequera/cheque-solicitud',
            writer: { writeAllFields: true },
            api: {
                create: '/api/cajas/chequera/cheque-solicitud/',
                read: undefined,
                destroy: undefined,
                update: '/api/cajas/chequera/cheque-solicitud/actualizar'
            }
        });
        return store;
    },

    getStoreCheques() {
        const store = this.lookup('emisionChequeTabla').getStore();
        if (store) { return store; }
    },
    getDependencia(dependenciaId) {
        return new Promise((resolve) => {
            Pce.afiliacion.dependencia.Dependencia.load(dependenciaId, {
                success: dependencia => {
                    if (dependencia) { resolve(dependencia); }
                }
            });
        });
    },
    generaChequeDependencia(dependencia, importeDependencia) {
        return this.creaCheque({
            rfc: dependencia.get('rfc') ? dependencia.get('rfc') : 'XAXX010101000',
            nombrePersona: dependencia.get('descripcion'),
            razonSocial: dependencia.get('razonSocial') ? dependencia.get('razonSocial') : 'PUBLICO EN GENERAL',
            porcentaje: 100,
            importe: importeDependencia,
            tipoEmision: 'DEP',
        });
    },
    creaCheque(datos) {
        return new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: datos.nombrePersona,
            porcentaje: datos.porcentaje,
            rfc: datos.rfc,
            importe: datos.importe,
            tipoEmision: datos.tipoEmision,
            // solicitudDevolucionId: datos.solicitudId
        });
    },

    guardarCheques(solicitud, solicitudId) {
        let me = this;
        console.log(`solicitud id: ${solicitud.getId()}`);
        let storeCheques = me.lookup('emisionChequeTabla').getStore();
        let chequesData = storeCheques.getData();

        chequesData.items.forEach(el => {
            el.set('solicitudDevolucionId', solicitudId);
            solicitud.cheques().add(el);
        });
        console.log('procesando cheques...');
        return new Promise((resolve, reject) => {
            storeCheques.sync({
                success(batch){
                    console.log('success');
                    resolve(batch.getTotal());
                },
                failure(batch, options){
                    console.log('failure');
                    console.log(batch);
                    console.log(options);
                    me.continuar = false;
                    reject(batch.getExceptions());
                }
            });
        });
    },

    guardarBeneficiarios(storeBenef, solicitud, solicitudId) {
        let me = this;
        let BenefData = storeBenef.getData();
        console.log(`solicitud id: ${solicitudId}`);
        console.log('procesando beneficiarios...');

        BenefData.items.forEach(el => {
            el.set('solicitudDevolucionId', solicitudId);
        });
        return new Promise((resolve, reject) => {
            storeBenef.sync({
                success(batch){
                    console.log('beneficiario success');
                    resolve(batch.getTotal());
                },
                failure(batch){
                    console.log('beneficiario failure');
                    me.continuar = false;
                    reject(batch.getExceptions());
                }
            }); 
        });
    },

    onSolicitudSaveFailure() {
        Ext.Msg.alert('Error', 'Ha ocurrido un error y la solicitud no pudo ser guardada');
    },

    onSolicitudAfectacionSuccess(solicitud) {
        Ext.Msg.alert(
            'Operación completada',
            `Se ha realizado la afectación correspondiente a la solicitud ${solicitud.data.folio} bajo la transacción
            <b>${solicitud.data.resultado.transaccionId}</b>.`
        );
    },

    onSolicitudAfectacionFailure(solicitud) {
        Ext.Msg.alert(
            'Error',
            `Error al intentar la afectación correspondiente a la solicitud ${solicitud.data.folio}`
        );
    },

    onBeneficiarioTestamentSuccess(beneficiario) {
        Ext.Msg.alert(
            'Operación completada',
            `Se ha guardado correctamente el usuario <b>${beneficiario.data.nombre}</b> como beneficiario`
        );
    },

    onBeneficiarioTestamentFailure(beneficiario) {
        Ext.Msg.alert(
            'Error',
            `Error al intentar guardar al usuario: <b>${beneficiario.data.nombre}</b> como beneficiario`
        );
    },

    onAseguradoChange(selector,afiliado) {
        let asegurado = selector.getSelection();
        if (asegurado) {
            this.resetViewModel();
            this.cargarEstadoDeCuenta(asegurado);
            this.cargaInfoAsegurado(asegurado);
            this.actualizaTotalBeneficiarios(1);
            this.cargarBeneficiarios(asegurado);
            this.cargaUltimaDependenciaAsegurado(asegurado);
            this.validaBajaDependencias(asegurado);
            this.validaFactor80(asegurado);
        }
        
    },
    validaFactor80(asegurado){
        Pce.afiliacion.asegurado.AseguradoInfo.load(asegurado.get('numeroAfiliacion'), {
            success: info => {
                if (info.get('edad') < info.get('factor80') && info.get('conceptoId') === 14) {
                    this.invalidaSolicitud(`El afiliado es regimen cuenta individual y no cumple con la ley del factor 80.`);
                }
            }
        });
    },

    onGuardarBeneficiarioTest(dialogo, beneficiario) {
        let form = dialogo.down('beneficiario-test-forma').getForm();
        let store = this.obtieneStoreChequesTabla();
        let importeFondo = this.getViewModelDataProp('importeFondo');
        var idBeneficiario = this.registros.length + 1; 

        if(!form.isValid()) { return; }

        beneficiario.set('parentesco', dialogo.down('parentesco-beneficiario-selector').getRawValue());
        beneficiario.set('tipo', dialogo.down('tipo-beneficiario-selector').getRawValue());
        beneficiario.set('numeroBeneficiario',idBeneficiario);

        if(!store.findRecord('beneficiario',beneficiario.data.nombrePersona)) {
            this.registros.push(beneficiario.getData());
            this.insertarBeneficiariosTabla(beneficiario);
            this.actualizaTotalBeneficiarios(this.registros);
            this.preparaCalculoPorcentajeTotalDevolucion();
            this.validaPorcentajeDevolucionUsado();
            // this.calculaNetoaPagar(importeFondo,beneficiario.data.porcentaje);
        }
        else
        {
            this.mostrarMensajeAlerta(
                'Beneficiario Existente',
                'Este beneficiario ya fué dado de alta');
        }
        dialogo.close();
    },

    calculaNetoaPagar(importeFondo,porcentaje){
        let importeDep = this.lookup('importeDependencia').value;
        let importeSM = this.lookup('importeSM').value;

        if(importeDep === null || importeDep === undefined)
        importeDep = 0;
        if(importeSM === null || importeSM === undefined)
        importeSM = 0;

        importeFondo - importeDep - importeSM;

        let netoPagar = this.getViewModelDataProp('totalFondo');
        if(netoPagar === undefined || netoPagar === null)
        netoPagar = 0;

        netoPagar += (importeFondo * porcentaje/100);
        this.setViewModelDataProp('totalFondo',netoPagar);
    },
    restaNetoaPagar(importeFondo,oldPorcentaje){
        let netoPagar = this.getViewModelDataProp('totalFondo');
        if(netoPagar === undefined || netoPagar === null)
        netoPagar = 0;

        netoPagar -= (importeFondo * oldPorcentaje/100);
        this.setViewModelDataProp('totalFondo',netoPagar);
    },

    onGuardarAdeudoDependencia(dialogo, adeudo) {
        let form = dialogo.down('form').getForm();
        if(!form.isValid()) {
            return;
        }

        let store = this.lookup('certificacionDependenciaTabla').getStore();
        store.insert(0, [adeudo]);

        dialogo.close();
    },

    onGuardarChequeSolicitud(dialogo) {
        let form = dialogo.down('form').getForm();
        if(!form.isValid()) {
            return;
        }
        dialogo.close();
    },

    onFallecidoCheck(element, opt) {
        this.fallecido = opt;
        let asegurado = this.getViewModelDataProp('asegurado');
        let store = this.obtieneStoreChequesTabla();
        let importefondo = this.getViewModelDataProp('importeFondo');
        // observaciones.allowBlank = !opt;

        if(asegurado) {
            if(opt) {
                store.removeAll();
                this.setViewModelDataProp('bajaDependencias', true);
                this.setViewModelDataProp('solicitudValida', true);
                this.setViewModelDataProp('fallecido', true);
                this.lookup('adeudoDependencia').setValue('false');
                this.lookup('certificacionServicioMedico').getForm().reset();
                this.cargaBeneficiariosTabla();
                this.actualizaTotalBeneficiarios(this.registros.length);
                this.setViewModelDataProp('totalFondo',0);
            } else {
                store.removeAll();
                this.actualizaTotalBeneficiarios(1);
                this.cargarBeneficiarios(asegurado);     
                this.validaBajaDependencias(asegurado);
                this.validaFactor80(asegurado);
                this.cargaInfoAsegurado(asegurado);
                this.cargaUltimaDependenciaAsegurado(asegurado);
                this.InsertaBeneficiarioAgain(100, asegurado);
                this.lookup('adeudoDependencia').setValue('false');
                this.lookup('certificacionServicioMedico').getForm().reset();
                this.lookup('beneficiarioTabla').getStore().reload();
                this.setViewModelDataProp('totalFondo',importefondo);
            }
        }
    },
    
    InsertaBeneficiarioAgain(porcentaje, asegurado){
        this.porcentajeTotal = porcentaje;
        let store = this.obtieneStoreChequesTabla();
        let importe = this.calculaImportePorPorcentaje(porcentaje);

        store.data.items.forEach(benef => {
        if(benef.data.beneficiario == asegurado.data.nombreCompleto){
            store.removeAll();
        }});
        let chequeBeneficiario = this.creaChequeAsegurado(porcentaje, importe);
        store.insert(0, [chequeBeneficiario]);
        this.agregaNuevoBeneficiarioSolicitud(store.count());
    },

    onValidaConceptoImportePorRegistro(component) {
        let numrecords = component.getStore().count();
        let nodes = component.getNodes(0, numrecords);
        let rowError = ' row-error';

        let ids = this.buscaConceptoImporteValidos(nodes);
        this.marcaRegistrosInvalidos(nodes, ids, rowError);
    },

    onEditarBeneficiario(view, rowIndex, collIndex, item, e, record) {
        this.mostrarDialogoBeneficiarioEdicion(record);
    },

    onEliminarCheque(view, rowIndex, collIndex, item, e, record){
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que desea eliminar este cheque a 
            <b><i>${record.get('beneficiario')}</i></b>?`,
            ans => {
                if (ans === 'yes'){
                    let storeCheques = this.getStoreCheques();
                    let chequeIndex = storeCheques.find('beneficiario', record.get('beneficiario'), null, null, null, true);
                    if (chequeIndex >= 0) {
                        let cheque = storeCheques.getAt(chequeIndex);
                        // this.calculaImporteTotalCheques(cheque.get('importe'), 'resta');
                        view.getStore().removeAt(rowIndex);
                        // storeCheques.removeAt(chequeIndex);
                        this.setViewModelDataProp('solicitudValida', true);
                        if(cheque.data.tipoEmision == 'DEP'){
                            this.lookup('adeudoDependencia').setValue('false');
                            this.lookup('importeDependencia').setValue(0);
                        }
                        else{
                            this.lookup('certificacionServicioMedico').getForm().reset();
                            this.lookup('importeSM').setValue(0);
                        }
                        
                    } else {
                        this.mostrarMensajeAlerta('Error',
                            'No fue posible encontrar el pago correspondiente al cheque seleccionado');
                    }
                    this.validaPorcentajeDevolucionUsado()
                }
            }
        );
    },
    onEliminarBeneficiario(view, rowIndex, collIndex, item, e, record) {
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar este beneficiario? 
            <b><i>${record.get('nombrePersona')}</i></b>`,
            ans => {
                if (ans === 'yes'){
                    let registro = null;
                    let storeCheques = this.obtieneStoreChequesTabla();
                    let chequeIndex = storeCheques.find('beneficiario', record.get('nombrePersona'), null, null, null, true);
                    let importeFondo = this.getViewModelDataProp('importeFondo');
                    // this.restaNetoaPagar(importeFondo,record.data.porcentaje);

                    view.getStore().removeAt(rowIndex);
                    if(this.registros.length > 0){
                        for (var i = 0; i < this.registros.length; i++){
                            if (this.registros[i].nombrePersona == record.get('nombrePersona')){
                                registro = this.registros[i];
                                this.porcentajeTotal -= registro.porcentaje;    
                                this.registros.splice(i,1);
                            }
                        }    
                    }
                    this.lookup('nuevoBeneficiarioBoton').enable();
                    if (chequeIndex >= 0) {
                        let cheque = storeCheques.getAt(chequeIndex);
                        this.calculaImporteTotalCheques(cheque.get('importe'), 'resta');

                        storeCheques.removeAt(chequeIndex);
                        this.setViewModelDataProp('solicitudValida', true);
                    } else {
                        this.mostrarMensajeAlerta('Error',
                            'No fue posible encontrar el pago correspondiente al beneficiario seleccionado');
                    }
                    this.validaPorcentajeDevolucionUsado()
                }
            }
        );
    },

    onCancelarSolicitudTap() {
        Ext.Msg.confirm('Cancelar captura',
            `Esta operación refrescará por completo la pantalla y los datos capturados se perderán, ¿Deseas continuar?`,
            resp => {
            if(resp === 'yes') {
                window.location.reload();
            }
        });
    },

    onTotalFondoChange(field, newValue) {
        let asegurado = this.getViewModelDataProp('asegurado');

        if (asegurado) {
            let storeBenefs = this.obtieneStoreBeneficiarioTabla();

            storeBenefs.removeAll();

            if (this.fallecido) {
                if (this.registros.length > 0) {
                    this.cargaBeneficiariosTabla();
                    this.actualizaTotalBeneficiarios(this.registros.length);
                }
            } else {
                this.actualizaAseguradoCheque(newValue,100);
            }
        }
    },

    actualizaAseguradoCheque(newValue, porcentaje){
        this.porcentajeTotal = porcentaje;
        let store = this.obtieneStoreChequesTabla();
        // let importe = this.calculaImportePorPorcentaje(porcentaje);
        let chequeBeneficiario = undefined;

        if(newValue > 0){
         chequeBeneficiario = this.creaChequeAsegurado(porcentaje, newValue);
         this.actualizaChequeBeneficiario(store, chequeBeneficiario);
        }
        else{
         store.splice(0,1);   
        }
    },

    actualizaChequeBeneficiario(store, nuevoCheque) {
        if(nuevoCheque.get('importe') > 0) {
            if(store.data.items.length > 0){
                store.splice(0,1,[nuevoCheque]);
                this.agregaNuevoBeneficiarioSolicitud(store.count());
            }
            else{
                store.insert(0, [nuevoCheque]);
                this.agregaNuevoBeneficiarioSolicitud(store.count());
            }
        }
    },
    confirmarImporteCheques() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `No existen cheques de adeudo a dependencia o servicio Medico, 
                debe revisar si existe primero un adeudo antes de editar el total del fondo
                ¿Aun así deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    EditarTotalFondo(){
        const importeSM = this.lookup('importeSM').value;
        const importeDependencia = this.lookup('importeDependencia').value;

        if(0 < importeSM  ||  0 < importeDependencia){
         this.onEditarTotalDevolucion();
        }else{
            this.confirmarImporteCheques().then(ans => {
                if (ans === 'yes'){
                   this.onEditarTotalDevolucion();
                }
        });
        }
    },

    onEditarTotalDevolucion() {  
        let totalFondo = this.getViewModelDataProp('totalFondo');
        let formPanel = Ext.create('Ext.form.Panel', {
            reference: 'formTotalFondo',
            items: [{
                xtype: 'numberfield',
                name: 'totalFondo',
                allowBlank: false,
                // minValue: 1,
                fieldLabel: 'Total fondo',
                value: totalFondo
            }]
        });

        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'Ingresar Importe',
            defaultListenerScope: true,
            modal: true,
            autoShow: true,
            layout: 'fit',
            bodyPadding: 10,
            buttons: [{
                text: 'Aceptar',
                handler: 'onAceptar'
            }, {
                text: 'Cancelar',
                handler: 'onCancelar'
            }],
            items: [{
                xtype: formPanel
            }],
            onAceptar() {
                let form = this.down('form');
                if (form.isValid()) {
                    this.fireEvent('guardar', this, form.getForm().getFieldValues().totalFondo);
                }
            },
            onCancelar() {
                this.close();
            },
            listeners: {
                guardar: this.onEditarTotalFondo.bind(this)
            }
        });
    },

    onEditarTotalFondo(dialogo, valor) {
        let importeFondo = this.getViewModelDataProp('importeFondo');

        if(valor <= importeFondo) {
            this.setViewModelDataProp('totalFondo', valor);
            // this.editarImporteBeneficiario(valor);
        }else{
        this.mostrarMensajeAlerta('Importe Invalido', 'Favor de ingresar un importe valido' );
        }

        dialogo.close();
    },

    creaChequeAfiliado(cheque, valor){
        return new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: cheque.data.beneficiario,
            porcentaje: cheque.data.porcentaje,
            rfc: cheque.data.rfc,
            importe: valor,
            solicitudDevolucionId: 0,
            numeroBeneficiario: cheque.data.numeroBeneficiario
        });
    },

    buscaConceptoImporteValidos(nodes) {
        const ids = [];
        const conceptos = ['A','H','I'];
        let importeAsociado = 0;
        let importeJ = 0;
        let importeI = 0;

        const conceptoSaldo = this.obtieneConceptoSaldoDesdeUI(nodes);

        conceptoSaldo.forEach(({ concepto, elementId, importe }) => {
            importe = Math.abs(importe);

            if (conceptos.includes(concepto) && importe > 0) {
                ids.push(elementId);
                importeAsociado = importe;
            }
            if (concepto === 'J') { importeJ = importe; }
            if (concepto === 'I') { importeI = importe; }
        });

        importeAsociado = (importeJ > 0 && importeI > 0) ? importeJ + importeI : importeAsociado ;

        if (importeAsociado !== 0) { this.estableceImporte(importeAsociado); }

        return ids;
    },

    obtieneConceptoSaldoDesdeUI(nodos) {
        const rowError = ' row-error';

        return nodos.map(item => {
            const elementId = item.id;
            const concepto = item.rows[0].cells[0].innerText;
            const className = item.className;
            const importe = this.convierteValorNumerico(item.rows[0].cells[3].textContent);

            if(concepto === 'C' && importe !== 0) { // Marca de color el registro que no aplica para devolución
                item.className = item.className.concat(rowError);
            }

            return { concepto, elementId, importe, className };
        });
    },

    estableceImporte(importe) {
        let importeAbs = Math.abs(importe);
        let claveRegimen = this.getViewModelDataProp('aseguradoInfo').data.claveRegimen;

        // busca concepto en tabulador H(TR), I(CI), A(LA), si encuentra
        // agrega importe del concepto al ImporteFondo
        let claves = Object.keys(this.conceptosTab)
            .find(key =>
                this.conceptosTab[key] === claveRegimen
            );

        if(!claves) { importeAbs = 0; }

        this.setViewModelDataProp('importeFondo', importeAbs);
    },

    marcaRegistrosInvalidos(nodes, ids, rowError) {
        if(ids.length > 1) {
            nodes.forEach(el => {
                if(ids.includes(el.id)) {
                    el.className = el.className.concat(rowError);

                    this.invalidaSolicitud(`El usuario tiene saldo en dos o mas cuentas.
                    Primero debe realizar un traspaso antes de continuar con la solicitud`);
                }
            });
        }
    },

    actualizaTotalBeneficiarios(numero) {
        this.setViewModelDataProp('totalBeneficiarios', numero);
    },

    cargaInfoAsegurado(asegurado) {
        Pce.afiliacion.asegurado.AseguradoInfo.load(asegurado.get('numeroAfiliacion'), {
            success: info => {
                this.setViewModelDataProp('aseguradoInfo', info);
                this.helperDevolucion
                    .determinaJubilacionAsegurado(asegurado.get('numeroAfiliacion'), info)
                    .then(info => {
                        if (info.get('jubilado') === 'SI') {
                            this.invalidaSolicitud('Los asegurados jubilados no pueden solicitar la devolución de su fondo');
                        }
                });
            }
        });
        // asegurado.getInfo().then(info => {
        //     this.setViewModelDataProp('aseguradoInfo', info);
        //     if(info.get('jubilado') === 'SI') {
        //         this.invalidaSolicitud('Los asegurados jubilados no pueden solicitar la devolución de su fondo');
        //     }
        // });
    },

    cargarEstadoDeCuentaAseguradoConcepto(datos) {
        return Pce.ingresos.asegurado.EstadoDeCuenta.porAseguradoYConcepto(
            datos.asegurado,
            datos.concepto
        );
    },

    cargarEstadoDeCuenta(asegurado) {
        let store = this.lookup('aseguradoEstadoCuentaTabla').getStore();
        store.load({
            params: {
                numeroAfiliacion: asegurado.getId()
            }
        });
    },

    cargaUltimaDependenciaAsegurado(asegurado) {
        if(this.validado == false){
        let me = this;
        let store = me.lookup('dependenciaAseguradoSelector').getStore();
        store.load({
            params: {
                numeroAfiliacion: asegurado.getId()
            },
            callback(record, operation, success) {
                if(success) {
                    if(record.length > 0) {
                        me.estableceUltimaDependencia(
                            record[0].get('numeroDependencia'),
                            record[0].get('descripcion')
                        );
                    } else {
                        me.estableceUltimaDependencia(0, null);
                        me.invalidaSolicitud(
                            'No existe ninguna baja registrada en la(s) dependencia(s) donde ha laborado. Favor de comunicarse con el departamento de afiliación'
                        );
                    }
                }
            }
        });
        }
    },

    onSeleccionDependencia(){
        let dependencia = this.getViewModelDataProp('dependenciaSelector').selection;
        if(dependencia){
            this.estableceUltimaDependencia(dependencia.data.id, dependencia.data.descripcion);
        }
        console.log(this.getViewModelDataProp('ultimaDependenciaDesc'))
    },

    invalidaSolicitud(msg) {
        this.mostrarMensajeAlerta('Solicitud inválida', msg);
        this.setViewModelDataProp('solicitudValida', false);
        this.lookup('tapGuardar').disable();
    },

    validaBajaDependencias(asegurado) {
        if(this.validado == false){
        let me = this;
        let store = this.lookup('dependencia-tabla').getStore();
        store.removeAll();

        Ext.Ajax.request({
            url: '/api/afiliacion/dependencia-asegurado/baja-dependencia',
            method: 'GET',
            params: { numeroAfiliacion: asegurado.data.id },
            callback(operation, success, response) {
                if(success) {
                    if(response.status !== 204) {
                        // Enlista dependencias que no registran BAJA. Se debe generar baja
                        // en todas y cada una antes de proceder.
                        // EXCEPCION: cuando es fallecido esta validacion es innecesaria
                        me.estableceDependenciasSinBaja(store, JSON.parse(response.responseText));
                    } else {
                        me.setViewModelDataProp('bajaDependencias', true);
                        me.setViewModelDataProp('solicitudValida', true);
                    }
                }
            }
        });
    }
    },

    estableceUltimaDependencia(numDep, desc) {
        this.setViewModelDataProp('ultimaDependenciaAsegurado', numDep);
        this.setViewModelDataProp('ultimaDependenciaDesc', desc);
    },

    estableceDependenciasSinBaja(store, datos) {
        store.loadData(datos, false);

        this.setViewModelDataProp('bajaDependencias', false);
        this.invalidaSolicitud(`Asegurado aún no registra bajas en la(s) dependencia(s) que labora.
                            Debe registrar todas las bajas o ser fallecido antes de proceder con la solicitud
                            Favor de comunicarse con el departamento de afiliación.`);
    },

    obtieneValoresFormularios() {
        let viewModel = this.getViewModel().getData();
        let datosAseguradoForma = this.obtieneAseguradoForma();
        let datosAseguradoInputs = datosAseguradoForma.getForm().getValues();
        let certifDependenciaForma = this.lookup('certificacionDependenciaForma').getForm();
        let certifServicioMedicoForma = this.lookup('certificacionServicioMedico').getForm();

        if(!datosAseguradoForma.isValid()) { return false; }
        if(datosAseguradoInputs.jubilado === 'SI'){ return false; }
        if(!certifDependenciaForma.isValid()) { return false; }
        if(!certifServicioMedicoForma.isValid()) { return false; }

        this.solicitud.set('delegacion', datosAseguradoInputs.delegacion);
        this.solicitud.set('observaciones', datosAseguradoInputs.observaciones);
        this.solicitud.set('telefono', datosAseguradoInputs.telefono);
        this.solicitud.set('fechaCaptura', viewModel.fechaCaptura);
        this.solicitud.set('usuarioCertificaDependencia', viewModel.usuarioCertificaDependencia);
        this.solicitud.set('importeDependencia', viewModel.importeDep);
        this.solicitud.set('fechaRegistrosDep', viewModel.fechaRegistrosDep);
        this.solicitud.set('importeServicioMedico', viewModel.importeSM);
        this.solicitud.set('importeFondo', viewModel.importeFondo);
        this.solicitud.set('usuarioCertificaServicioMedico', viewModel.usuarioCertificaSM);
        this.solicitud.set('asegurado', viewModel.asegurado.getId());
        this.solicitud.set('importePagar', viewModel.totalFondo);
        this.solicitud.set('defuncion', this.fallecido);
        this.solicitud.set('dependenciaAdeudo', viewModel.dependenciaSelector.selection.data.id);
        this.solicitud.set('dependenciaMovimiento', viewModel.ultimaDependenciaAsegurado);
        this.solicitud.set('dependenciaMovimientoDesc', viewModel.ultimaDependenciaDesc);
        this.solicitud.set('dependenciaBeneficiarioDesc', viewModel.ultimaDependenciaDesc);

        return true;
    },

    cargarBeneficiarios(asegurado) {
        let me = this;
        Ext.Ajax.request({
            url: '/api/ingresos/asegurado/beneficiario-testament/por-num-afiliacion',
            method: 'GET',
            params: { numeroAfiliacion: asegurado.getId() },
            callback: (opt, success, response) => {
                if(success) {   // obtiene y almacena el numero de beneficiarios de afiliación
                    me.registros = JSON.parse(response.responseText);
                } else {
                    Ext.Msg.alert(
                        'Error al cargar beneficiarios',
                        `Ocurrió un error al cargar los beneficiarios asociados a ${asegurado.get('nombre')}`
                    );
                }
            }
        });
    },

    cargaBeneficiariosTabla() {
        let store = this.obtieneStoreBeneficiarioTabla();
        store.loadData(this.registros);

        if (this.fallecido) {
            if (this.sumaPorcentajesBeneficiariosAfil(this.registros) > 100) {
                this.invalidaSolicitud('La suma de porcentaje de beneficiarios excede el 100%. Es indispensable corregir datos en afiliación para poder proceder');
                let boton = this.lookup('onGuardarNuevaSolicitudTap');
                if (boton) { boton.disable(); } //Previene error al ejecutar onDestroy (cambio de pagina)
            } else {
                this.preparaCalculoPorcentajeTotalDevolucion();
            }
        } else {
            this.preparaCalculoPorcentajeTotalDevolucion();
            this.agregaNuevoBeneficiarioSolicitud(1);
        }
    },

    sumaPorcentajesBeneficiariosAfil(items) {
        if(items.length > 0) {
            return items.map(item=> item.porcentaje).reduce((a,c)=>a+c);
        }
        return 100;
    },

    insertarBeneficiariosTabla(beneficiario) {
        this.obtieneStoreBeneficiarioTabla().insert(0, [beneficiario]);
    },

    insertaChequeBeneficiario(store, nuevoCheque) {
        if(nuevoCheque.get('importe') > 0) {
            store.insert(0, [nuevoCheque]);
            this.agregaNuevoBeneficiarioSolicitud(store.count());
        }
    },

    validaChequeBeneficiarioExistente(nuevoCheque) {
        let store = this.obtieneStoreChequesTabla();
        let cheques = store.getData().items;
        let nuevoBeneficiario = nuevoCheque.get('beneficiario');

        if(store.data.items.length > 0) {
                if(!store.findRecord('beneficiario',nuevoBeneficiario)) {
                    this.insertaChequeBeneficiario(store, nuevoCheque);
                }else {
                    let BenefExistente = store.findRecord('beneficiario',nuevoBeneficiario);
                    BenefExistente.set('porcentaje',nuevoCheque.get('porcentaje')); 
                    BenefExistente.set('importe',nuevoCheque.get('importe')); 

                }
        } else {
            this.insertaChequeBeneficiario(store, nuevoCheque);
        }
    },

    preparaChequeCalculoBenef(beneficiario) {
        let importeCheque = this.calculaImportePorPorcentaje(beneficiario.porcentaje);

        this.calculaImporteTotalCheques(importeCheque, 'sum');

        return this.creaChequeBeneficiario(beneficiario, importeCheque);
    },

    preparaAseguradoCheque(porcentaje) {
        this.porcentajeTotal = porcentaje;
        let store = this.obtieneStoreChequesTabla();
        let importe = this.calculaImportePorPorcentaje(porcentaje);

        let chequeBeneficiario = this.creaChequeAsegurado(porcentaje, importe);

        this.insertaChequeBeneficiario(store, chequeBeneficiario);
    },

    creaChequeBeneficiario(beneficiario, importeCheque) {
        return new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: beneficiario.nombrePersona,
            porcentaje: beneficiario.porcentaje,
            rfc: beneficiario.rfc,
            importe: importeCheque,
            solicitudDevolucionId: 0,
            numeroBeneficiario: beneficiario.numeroBeneficiario
        });
    },

    creaChequeAsegurado(porcentaje, importeCheque) {
        let asegurado = this.getViewModelDataProp('asegurado');
        return new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: asegurado.get('nombreCompleto'),
            porcentaje: porcentaje,
            rfc: asegurado.get('rfc'),
            importe: importeCheque,
            solicitudDevolucionId: 0
        });
    },

    preparaCalculoPorcentajeTotalDevolucion() {
        let chequeBeneficiario;
        let store = this.obtieneStoreChequesTabla();
        this.porcentajeTotal = 0;

        Ext.getBody().mask('calculando pagos por beneficiario...');

        if(this.fallecido) {
            if(this.registros.length > 0) {
                this.registros.forEach(benef => { // prepara cheques por cada beneficiario
                    this.porcentajeTotal += benef.porcentaje;

                    chequeBeneficiario = this.preparaChequeCalculoBenef(benef); // convierte beneficiario a cheque
                    this.validaChequeBeneficiarioExistente(chequeBeneficiario);
                });
            }
        }
        else {
            this.preparaAseguradoCheque(100);
        }

        this.validaPorcentajeDevolucionUsado();
        Ext.getBody().unmask();
    },
    preparaCalculoPorcentajeTotalDevolucionEdicion() {
        let chequeBeneficiario;
        let store = this.obtieneStoreChequesTabla();
        this.porcentajeTotal = 0;

        Ext.getBody().mask('calculando pagos por beneficiario...');

        if(this.fallecido) {
            if(this.registros.length > 0) {
                this.registros.forEach(benef => { // prepara cheques por cada beneficiario
                    this.porcentajeTotal += benef.porcentaje;

                    chequeBeneficiario = this.preparaChequeCalculoBenef(benef);
                    this.validaChequeBeneficiarioExistente(chequeBeneficiario);
                });
            }
        }
        else {
            this.preparaAseguradoCheque(100);
        }

        this.validaPorcentajeDevolucionUsado();
        Ext.getBody().unmask();
    },


    calculaImportePorPorcentaje(porcentaje) {
        let totalFondo = this.getViewModelDataProp('totalFondo');
        let importeDep = this.lookup('importeDependencia').value;
        let importeSM = this.lookup('importeSM').value;

        if(importeDep === null)
        importeDep = 0;
        if(importeSM === null)
        importeSM = 0;
        if(totalFondo <= (importeDep+importeSM)){
         return 0;   
        }else{
         return (totalFondo - importeDep - importeSM) * (porcentaje/100);
        }
        
    },

    calculaImporteTotalCheques(importeCheque, oper) {
        let totalFondo = this.getViewModelDataProp('totalFondo');
        let importeRestante = 0;

        if(oper === 'sum') {
            importeRestante = totalFondo - importeCheque;
        } else {
            importeRestante = totalFondo + importeCheque;
        }

        this.setViewModelDataProp('importeRestante', importeRestante);
    },

    validaFondoDevolucion() {
        if(this.getViewModelDataProp('importeFondo') <= 0) {
            Ext.Msg.alert('Fondos insuficientes',
                'No es posible agregar beneficiarios y pagos, no hay fondos suficientes');

            this.setViewModelDataProp('solicitudValida', false);
            return false;
        }
        return true;
    },

    validaPorcentajeDevolucionUsado() {
        if(this.porcentajeTotal > 100) {
            this.invalidaSolicitud(`El porcentaje aplicado excede del 100, por favor revise datos`);
            this.lookup('nuevoBeneficiarioBoton').disable();
        } else {
            if(this.porcentajeTotal === 100) {
                this.lookup('nuevoBeneficiarioBoton').disable();
                this.setViewModelDataProp('solicitudValida', true);
            }
            let fallecido = this.getViewModelDataProp('fallecido');
            if(fallecido && this.porcentajeTotal < 100) {
                this.setViewModelDataProp('solicitudValida', true);
                this.lookup('nuevoBeneficiarioBoton').enable();           
            }
        }
    },

    agregaNuevoBeneficiarioSolicitud(cant) {
        this.setViewModelDataProp('totalBeneficiarios', cant);
    },

    obtieneAseguradoForma() {
        return this.lookup('solicitudAseguradoForma');
    },

    obtieneViewModel() {
        let vm = this.getViewModel();
        if(vm) {
            return vm;
        }
    },

    obtieneStoreBeneficiarioTabla() {
        let tabla = this.obtieneBeneficiarioTabla();
        return tabla.getStore();
    },

    obtieneStoreChequesTabla() {
        return this.obtieneChequesTabla().getStore();
    },

    obtieneBeneficiarioTabla() {
        return this.lookup('beneficiarioTabla');
    },

    obtieneChequesTabla() {
        return this.lookup('emisionChequeTabla');
    },

    obtieneConceptoPorClaveRegimen(claveRegimen) {
        let claveConcepto = Object.keys(this.conceptosTab)
            .find(key =>
                this.conceptosTab[key] === claveRegimen
            );
        if(claveConcepto) {
            return new Promise(resolve => {
                Ext.Ajax.request({
                    url: '/api/ingresos/concepto/por-clave',
                    method: 'GET',
                    params: { clave: claveConcepto },
                }).then(response => {
                    resolve(JSON.parse(response.responseText));
                });
            });
        }
    },

    mostrarDialogoBeneficiario(beneficiario) {
        if(this.validaFondoDevolucion()) {
            let titulo =  'Nuevo beneficiario';

            Ext.create('Pce.view.dialogo.FormaContenedor', {
                title: titulo,
                record: beneficiario,
                width: 900,
                items: {
                    xtype: 'beneficiario-test-forma'
                },
                listeners: {
                    guardar: this.onGuardarBeneficiarioTest.bind(this)
                }
            });
        }
    },

    mostrarDialogoBeneficiarioEdicion(beneficiario) {
        if(this.validaFondoDevolucion()) {
            let titulo = 'Editar beneficiario';

            Ext.create('Pce.view.dialogo.FormaContenedor', {
                title: titulo,
                record: beneficiario,
                width: 900,
                items: {
                    xtype: 'beneficiario-test-forma'
                },
                listeners: {
                    guardar: this.onGuardarBeneficiarioTestEdicion.bind(this)
                }
            });
        }
    },

    onGuardarBeneficiarioTestEdicion(dialogo, beneficiario){
        let form = dialogo.down('beneficiario-test-forma').getForm();
        const store = this.getStoreCheques();
        let importeFondo = this.getViewModelDataProp('importeFondo');
        let oldPorcentaje = beneficiario.modified.porcentaje;

        if(!form.isValid()) { return; }

        beneficiario.set('parentesco', dialogo.down('parentesco-beneficiario-selector').getRawValue());
        beneficiario.set('tipo', dialogo.down('tipo-beneficiario-selector').getRawValue());
         
        store.data.items.forEach(data => {
            if(data.data.numeroBeneficiario == beneficiario.data.numeroBeneficiario){
                data.set('beneficiario', beneficiario.data.nombrePersona);
                data.set('direccion',beneficiario.data.direccion);
                data.set('telefono',beneficiario.data.telefono);
                data.set('ciudad',beneficiario.data.ciudad);
                data.set('rfc',beneficiario.data.rfc);
            }
        });
        for (var i = 0; i < this.registros.length; i++)
        if (this.registros[i].numeroBeneficiario == beneficiario.data.numeroBeneficiario){
            this.registros[i] = beneficiario.data;
        }
        this.insertarBeneficiariosTabla(beneficiario);
        // this.actualizaTotalBeneficiarios(this.registros);
        this.preparaCalculoPorcentajeTotalDevolucionEdicion();
        // this.restaNetoaPagar(importeFondo,oldPorcentaje);
        // this.calculaNetoaPagar(importeFondo,beneficiario.data.porcentaje);

        // this.validaPorcentajeDevolucionUsado();
        dialogo.close();
    },

    mostrarDialogoCertificacionDependencia(adeudo, aseguradoId) {
        let titulo = adeudo.phantom ? 'Nuevo adeudo' : 'Editar adeudo';
        Ext.create('Pce.ingresos.asegurado.procesos.devolucionFondo.CertificadoDependenciaDialogo', {
            xtype: 'dialogo-certificado-dependencia',
            title: titulo,
            record: adeudo,
            aseguradoId: aseguradoId,
            listeners: {
                guardar: this.onGuardarAdeudoDependencia.bind(this)
            }
        });
    },

    mostrarDialogoCheque(registro) {
        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'Editar pago',
            record: registro,
            width: 600,
            items: {
                xtype: 'cheque-fondo-forma'
            },
            listeners: {
                guardar: this.onGuardarChequeSolicitud.bind(this)
            }
        });
    },

    imprimirSolicitudDevolucion(numAfil, solicitudId) {
        window.open(`/api/ingresos/asegurado/fondo/solicitud-devolucion/descargar?pnum_afil=${numAfil}&pidsolicitud=${solicitudId}`);
    },

    resetViewModel() {
        let fechaCapturaSelector = this.lookup('fechaCapturaSolicitud');
        let fechaCaptura = fechaCapturaSelector.getValue();
        this.obtieneAseguradoForma().getForm().findField('defuncion').setValue(false);

        this.setViewModelDataProp('solicitudValida', false);
        this.setViewModelDataProp('totalBeneficiarios', 1);
        this.setViewModelDataProp('importeFondo', 0);
        this.setViewModelDataProp('totalFondo', 0);

        this.lookup('certificacionDependenciaForma').getForm().reset();
        this.lookup('certificacionServicioMedico').getForm().reset();
        this.lookup('emisionChequeTabla').getStore().reload();
        fechaCapturaSelector.setValue(fechaCaptura);

        this.registros = 0;
        this.porcentajeTotal = 0;
        this.solicitud = null;
    },

    convierteValorNumerico(dato) {
        let num = dato.replace(/[^0-9.-]+/g, '');
        return Number(num);
    },

    mostrarMensajeAlerta(titulo, mensaje) {
        Ext.Msg.alert(titulo, mensaje);
    },

    getViewModelDataProp(prop) {
        let vm = this.obtieneViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },

    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },
});