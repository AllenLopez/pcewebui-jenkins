/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.EditorDevolucionFondoDetallePanel
 * @extends Ext.form.Panel
 * @author Alan López
 * @xtype editor-devolucion-fondo-detalle-panel
 * Panel de edición de detalle de devolución de fondo
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.EditorDevolucionFondoDetallePanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'editor-devolucion-fondo-panel',

    requires: [
        'Pce.ingresos.asegurado.procesos.devolucionFondo.EditorDevolucionFondoDetalleController',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.ChequeFondoDevolucionTabla',
        'Pce.ingresos.asegurado.BeneficiarioTabla',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.EmisionChequeFondoTabla',
        'Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitudStore',
        'Pce.ingresos.asegurado.beneficiario.BeneficiarioStore',
        'Pce.ingresos.asegurado.EstadoDeCuentaTabla',
        'Pce.ingresos.asegurado.EstadoDeCuentaStore'
    ],

    controller: 'editor-devolucion-solicitud',

    viewModel: {
        data: {
            solicitud: null,
            aseguradoInfo: null,
            asegurado: null,
            adeudaDependencia: null,
            solicitudValida: true,
            adeudaSM: false,
            importeRestante: 0,
            importeDep: 0,
            importeSM: 0,
            importeFondo: 0,
            totalChequesDevolucion: 0,
            bajaDependencias: false
        },
        formulas: {
            importePagar(get) {
                if(!get('fallecido')){
                    return solicitud.importeFondo - solicitud.importeDependencia - solicitud.importeServicioMedico;
                }
                else{
                return solicitud.importePagar;
                }
            }
        }
    },

    title: 'Editor de solicitud de devolución de fondo propio',

    scrollable: 'y',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    defaults: { width: '100%' },

    items: [{
        xtype: 'form',
        layout: {
            type: 'table',
            columns: 7,
        },
        bodyPadding: 10,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Núm. afiliado',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{solicitud.asegurado}'
            },
            colspan: 2
        }, {
            xtype: 'textfield',
            fieldLabel: 'Nombre',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{solicitud.aseguradoNombreCompleto}'
            },
            width: '569px',
            colspan: 4
        }, {
            xtype: 'textfield',
            fieldLabel: 'Dirección',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.direccion}'
            },
            width: '400px',
            colspan: 3
        },{
            xtype: 'textfield',
            fieldLabel: 'RFC',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.rfc}'
            },
            colspan: 2
        },{
            xtype: 'displayfield',
            fieldLabel: 'Estatus solicitud',
            renderer(v) {
                switch (v) {
                    case 'TR': return '<b>EN TRAMITE</b>';
                    case 'CT': return '<b>EN CONTROL</b>';
                    case 'CA': return '<b>CANCELADA</b>';
                    case 'AU': return '<b>AUTORIZADA</b>';
                    case 'CP': return '<b>COMPLETADA</b>';
                    default: return '<b>OTRA</b>';
                }
            },
            bind: {
                value: '{solicitud.etapa}'
            },
            colspan: 2
        },{ 
            xtype: 'textfield',
            fieldLabel: 'Teléfono',
            bind: {
                value: '{solicitud.telefono}'
            },
            colspan: 2
        }, {
            xtype: 'textfield',
            fieldLabel: 'Concepto',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{solicitud.conceptoDesc}'
            },
            width: '400px',
            colspan: 3
        }, {
            xtype: 'textfield',
            labelWith: 130,
            readOnly: true,
            cls: 'read-only',
            fieldLabel: 'Estatus asegurado',
            bind: {
                value: '{aseguradoInfo.estatus}'
            },
            colspan: 2
        },{
            xtype: 'datefield',
            fieldLabel: 'Fecha captura',
            bind: {
                value: '{solicitud.fechaCaptura}'
            },
            colspan: 2
        },{
            xtype: 'checkbox',
            fieldLabel: 'Fallecido',
            reference: 'fallecidoCheck',
            renderer(v) {
                return v ? '<b>SI</b>': '<b>NO</b>';
            },
            bind: {
                value: '{solicitud.defuncion}'
            },
            listeners: {
                change: 'onFallecidoCheck'
            },
            colspan: 1
        },{
            xtype: 'combobox',
            name: 'delegacion',
            valueField: 'id',
            displayField: 'descripcion',
            fieldLabel: 'Delegación',
            width: '100%',
            queryMode: 'local',
            allowBlank: false,
            store: {
                type: 'delegacion',
                autoLoad: true
            },
            bind: {
                value: '{solicitud.delegacion}'
            },
            colspan: 3
        }, {
            xtype: 'textfield',
            fieldLabel: 'Observaciones',
            reference: 'observaciones',
            width: '100%',
            bind: {
                value: '{solicitud.observaciones}'
            },
            colspan: 6
        }]
        // {
        //     xtype: 'displayfield',
        //     fieldLabel: 'Importe devolución',
        //     labelWidth: 130,
        //     listeners: {
        //         change: 'onImporteDevolucionChange'
        //     },
        //     renderer(v) {
        //         return `<b>${Ext.util.Format.usMoney(v)}</b>`;
        //     },
        //     bind: {
        //         value: '{solicitud.importePagar}'
        //     },
        //     colspan: 2
        // }
        //ADEUDO A DEPENDENCIA
    }, {
        xtype: 'form',
        layout: {
            type: 'table',
            columns: 4
        },
        bodyPadding: 10,
        reference: 'AdeudoDepForma',
        title: 'Certificación de la institución',
        items: [{
            xtype: 'combobox',
            reference: 'adeudoDependenciaSelector',
            name: 'adeudaDependencia',
            fieldLabel: 'Adeudo',
            displayField: 'adeudoDependencia',
            valueField: 'valor',
            allowBlank: false,
            store: {
                data: [
                    {adeudoDependencia: 'NO', valor: false},
                    {adeudoDependencia: 'SI', valor: true}
                ]
            },
            // listeners: {
            //     change: 'onAdeudoDependenciaChange'
            // },
            // bind: {
            //     value: '{solicitud.importeDependencia > 0}',
            // },
            value: false,
            colspan: 1
        }, {
            xtype: 'datefield',
            name: 'fechaRegistrosDep',
            fieldLabel: 'Registros al',
            allowBlank: false,
            maxValue: new Date(),
            bind: {
                value: '{solicitud.fechaRegistrosDep}'
            },
            colspan: 1
        }, {
            xtype: 'numberfield',
            name: 'importe',
            reference: 'importeDep',
            fieldLabel: 'Importe',
            labelWidth: 130,
            maxValue: 999999999.99,
            bind: {
                disabled: '{!adeudoDependenciaSelector.selection}',
                value: '{solicitud.importeDependencia}'
            },
            listeners: {
                change: 'onTotalFondoChange'
            },
            colspan: 2
        }, {
            xtype: 'dependencia-default-selector',
            reference: 'dependenciaAseguradoSelector',
            name: 'numDependenciaAdeudo',
            fieldLabel: 'Dependencia',
            width: '100%',
            displayField: 'descripcion',
            queryMode: 'local',
            store: {
                type: 'dependencias',
            },
            bind: {
                disabled: '{!adeudoDependenciaSelector.selection}',
                value: '{solicitud.dependenciaMovimientoDesc}',
            },
            colspan: 2
        }, {
            xtype: 'textfield',
            name: 'usuarioCertificaDependencia',
            fieldLabel: 'Usuario certificador',
            width: '100%',
            bind: {
                value: '{solicitud.usuarioCertificaDependencia}'
            },
            labelWidth: 130,
            colspan: 2
        }, {
            xtype: 'dependencia-asegurado-tabla',
            reference: 'dependencia-tabla',
            colspan: 4
        }, {
            xtype: 'button',
            text: 'Generar cheque a dependencia',
            ui: 'default',
            handler: 'onGenerarChequeDependencia',
            bind: {
                disabled: '{!solicitud.importeDependencia}'
            },
            colspan: 2
        }]
        //ADEUDO A SERVICIO MEDICO
    }, {
        xtype: 'form',
        reference: 'AdeudoSMForma',
        layout: {
            type: 'table',
            columns: 3
        },
        bodyPadding: 10,
        title: 'Certificación de servicio médico',
        items: [{
            xtype: 'combobox',
            name: 'adeudaServicioMedico',
            reference: 'adeudoServicioMedicoSelector',
            fieldLabel: 'Adeudo',
            displayField: 'adeudo',
            valueField: 'valor',
            allowBlank: false,
            store: {
                data: [
                    {adeudo: 'NO', valor: false},
                    {adeudo: 'SI', valor: true}
                ]
            },
            listeners: {
                change: 'onImporteServicioMedicoChange'
            },
            // bind: {
            //     value: '{solicitud.importeServicioMedico > 0}',
            // },
            value: false,
            colspan: 1
        }, {
            xtype: 'datefield',
            name: 'fechaUltimosRegistros',
            fieldLabel: 'Registros al',
            maxValue: new Date(),
            renderer(v) {
                return Ext.util.Format.date(v, 'd/m/Y');
            },
            bind: {
                value: '{solicitud.fechaRegistrosSm}'
            },
            colspan: 1
        }, {
            xtype: 'numberfield',
            name: 'importeSM',
            reference: 'importeSM',
            fieldLabel: 'Importe',
            maxValue: 999999999.99,
            bind: {
                disabled: '{!adeudoServicioMedicoSelector.selection}',
                value: '{solicitud.importeServicioMedico}'
            },
            listeners: {
                change: 'onTotalFondoChange'
            },
            renderer(v) {
                return `<b>${Ext.util.Format.usMoney(v)}</b>`;
            },
            colspan: 1
        }, {
            xtype: 'textfield',
            name: 'usuarioCertificaSM',
            fieldLabel: 'Usuario certificador',
            width: '100%',
            bind: {
                value: '{usuarioCertificaServicioMedico}'
            },
            labelWidth: 150,
            colspan: 2
        }, {
            xtype: 'button',
            text: 'Generar cheque a PCE',
            ui: 'default',
            handler: 'onGenerarChequeSM',
            bind: {
                disabled: '{!solicitud.importeServicioMedico}'
            },
            colspan: 2
        }]
        //DATOS BENEFICIARIO
    }, {
        xtype: 'grid',
        reference: 'beneficiarioTabla',
        title: 'Datos de beneficiarios',
        defaults: { width: '100%' },
        store: { type: 'beneficiario-testament' },
        tbar: [{
            text: 'Nuevo beneficiario',
            ui: 'default',
            reference: 'nuevoBeneficiarioBoton',
            handler: 'onAgregarNuevoBenecificiario',
            bind: {
                disabled: '{!fallecidoCheck.checked && solicitud.importePagar > 0}'
            }
        }],
        columns:[{
            text: 'Nombre',
            dataIndex: 'nombrePersona',
            flex: 4,
        }, {
            text: 'Parentesco',
            dataIndex: 'parentesco',
            flex: 1,
        }, {
            text: 'Origen de alta',
            dataIndex: 'origen',
            flex: 1
        }, {
            text: '%',
            dataIndex: 'porcentaje',
            flex: 1,
        }, {
            xtype: 'actioncolumn',
            width: 60,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-edit',
                tooltip: 'Editar Beneficiario',
                handler: 'onEditarBeneficiario',
                isDisabled(view, rowIndex, collIndex, item, record) {
                    return (record.get('origen') === 'AFILIACION');
                }
            }, {
                iconCls: 'x-fa fa-trash',
                tooltip: 'Eliminar beneficiario',
                handler: 'onEliminarBeneficiario',
                isDisabled(view, rowIndex, collIndex, item, record) {
                    return (record.get('origen') === 'AFILIACION');
                }
            }]
        }]
        //ESTADO DE CUENTA
    }, {
        xtype: 'form',
        reference: 'estadoCuentapanel',
        layout: {
            type: 'table',
            columns: 4
        },
        title: 'Estado de cuenta',
        defaults: { width: '100%' },
        items: [{
            xtype: 'asegurado-estado-de-cuenta-tabla',
            reference: 'aseguradoEstadoCuentaTabla',
            store: {
                type: 'asegurado-estado-de-cuenta',
            },
            emptyText: 'No existen datos para este asegurado con este concepto',
            columns: [{
                text: 'Clave',
                dataIndex: 'conceptoClave',
                tooltip: `Identificador del concepto`,
            }, {
                text: 'Concepto',
                dataIndex: 'conceptoDescripcion',
                tooltip: `Nombre del concepto que se carga al estado de cuenta del afiliado`,
                flex: 1
            }, {
                text: 'Subcuenta',
                dataIndex: 'subcuentaDescripcion',
                flex: 1,
            }, {
                text: 'Saldo',
                dataIndex: 'saldo',
                tooltip: `Total de cada concepto descontado del salario del afiliado.`,
                renderer: Ext.util.Format.usMoney,
                flex: 1
            }],
            viewConfig: {
                listeners: {
                    refresh: 'onValidaConceptoImportePorRegistro'
                }
            },
            colspan: 4
        }]
        //DETALLES DE PAGO
    }, {
        xtype: 'grid',
        reference: 'emisionChequeTabla',
        store: {
            type: 'cheque-fondo-solicitud',
        },
        title: 'Detalles de pago',
        defaults: {
            width: '100%',
            bodyPadding: 0
        },
        width: '100%',
        emptyText: 'No hay pagos agregados',
        tbar: [{
            xtype: 'displayfield',
            fieldLabel: 'Total pagos a emitir',
            labelWidth: 180,
            padding: 10,
            bind: {
                value: '{totalBeneficiarios}'
            }
        }, {
            xtype: 'displayfield',
            fieldLabel: 'Importe de fondo',
            labelWidth: 120,
            padding: 10,
            bind: {
                value: '{solicitud.importeFondo}'
            },
            renderer: Ext.util.Format.usMoney
        }, {
            xtype: 'displayfield',
            reference: 'importePagar',
            padding: 10,
            fieldLabel: '<b>NETO A PAGAR</b>',
            bind: {
                value: '{solicitud.importePagar}'
            },
            renderer(v) { return `<b>${ Ext.util.Format.usMoney(v) }</b>`; },
            listeners: {
                change: 'onTotalFondoChange'
            }
        }, 
        {
            text: 'Editar Total/Neto',
            reference: 'btnEditar',
            tooltip: 'Permite editar el valor del total a devolver y recalcular importes',
            handler: 'onEditarTotalDevolucion',
        }
    ],
        columns: [
            {
            text: 'Beneficiario',
            dataIndex: 'beneficiario',
            flex: 4
        },{
            text: 'RFC',
            dataIndex: 'rfc',
            flex: 1
        },{
            text: 'Importe',
            dataIndex: 'importe',
            renderer(v) {
                return Ext.util.Format.usMoney(v);
            },
            flex: 1
        },
        {
            xtype: 'actioncolumn',
            width: 50,
            items: [{
                iconCls: 'x-fa fa-edit',
                tooltip: 'Permite editar los valores del pago a emitir',
                handler: 'onEditarCheque'
            },{
                iconCls: 'x-fa fa-trash',
                tooltip: 'permite eliminar los cheques',
                handler: 'onEliminarCheque',
                // isDisabled(view, rowIndex, collIndex, item, record) {
                //     const tipoEmision = record.get('tipoEmision');
                //     return (tipoEmision === 'AOB' || tipoEmision === undefined );
                // }
            }]
        }]
    }],

    buttons: [{
        text: 'Actualizar',
        reference: 'tapGuardar',
        handler: 'onActualizarSolicitud',
    }, {
        text: 'Cancelar',
        handler: 'onCancelarEdicion'
    }]
});