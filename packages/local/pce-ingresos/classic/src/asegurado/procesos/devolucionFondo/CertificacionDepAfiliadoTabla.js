/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.DependenciaTabla
 * @extends Ext.grid.Panel
 * @xtype dependencia-asegurado-tabla
 * Panel que enlista las dependencias donde ha trabajado/trabaja el asegurado
 * @author Jorge Escamilla
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.CertificacionDepAfiliadoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'dependencia-asegurado-tabla',

    requires: [
        'Pce.afiliacion.dependencia.DependenciaForma',
        'Pce.view.dialogo.FormaContenedor',
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    store: {
        type: 'dependencias'
    },

    columns: [{
        text: 'Descripción',
        dataIndex: 'descripcion',
        tooltip:'Descripción de la dependencia.',
        flex: 4
    }]
});