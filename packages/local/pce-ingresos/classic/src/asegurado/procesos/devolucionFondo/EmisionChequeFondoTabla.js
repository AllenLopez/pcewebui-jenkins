Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.EmisionChequeFondoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'emision-cheque-fondo-tabla',

    requires: [
        'Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitudStore'
    ],

    store: {
        type: 'cheque-fondo-solicitud',
    },


    title: 'Detalles de pago',

    defaults: {
        width: '100%'
    },

    emptyText: 'No hay pagos agregados',

    tbar: [{
        xtype: 'displayfield',
        fieldLabel: 'Total pagos a emitir',
        labelWidth: 180,
        padding: 10,
        bind: {
            value: '{totalBeneficiarios}'
        }
    }, {
        xtype: 'displayfield',
        fieldLabel: 'Importe de fondo',
        labelWidth: 120,
        padding: 10,
        bind: {
            value: '{importeFondo}'
        },
        renderer: Ext.util.Format.usMoney
    }, {
        xtype: 'displayfield',
        padding: 10,
        fieldLabel: '<b>NETO A PAGAR</b>',
        bind: {
            value: '{totalFondo}'
        },
        renderer(v) { return `<b>${ Ext.util.Format.usMoney(v) }</b>`; },
        listeners: {
            change: 'onTotalFondoChange'
        }
    }, 
    {
        text: 'Editar Total/Neto',
        reference: 'btnEditar',
        tooltip: 'Permite editar el valor del total a devolver y recalcular importes',
        handler: 'EditarTotalFondo',
    }
],

    columns: [{
        text: 'Beneficiario',
        dataIndex: 'beneficiario',
        flex: 4
    },{
        text: 'RFC',
        dataIndex: 'rfc',
        flex: 1
    },{
        text: 'Importe',
        dataIndex: 'importe',
        renderer(v) {
            return Ext.util.Format.usMoney(v);
        },
        flex: 1
    }, 
    {
        xtype: 'actioncolumn',
        width: 70,
        items: [{
            iconCls: 'x-fa fa-trash',
            tooltip: 'permite eliminar los cheques',
            handler: 'onEliminarCheque',
            isDisabled(view, rowIndex, collIndex, item, record) {
                const tipoEmision = record.get('tipoEmision');
                return (tipoEmision === undefined );
            }
        }]
    }
]
});