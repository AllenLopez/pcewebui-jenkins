/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.SolicitudDevolucionTabla
 * @extends Ext.grid.Panel
 * @xtype solicitud-devolucion-tabla
 * Tabla que enlista las solicitudes realizadas
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.SolicitudDevolucionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'solicitud-devolucion-tabla',

    requires: [
        'Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitudStore'
    ],

    store: {
        type: 'cheque-fondo-solicitud'
    },

    columns: [{
        text: 'Folio',
        dataIndex: 'id',
        flex: 1
    }, {
        text: 'Etapa',
        dataIndex: 'etapa',
        renderer(v) {
            switch (v) {
                case 'TR': return 'EN TRÁMITE';
                case 'CT': return 'EN CONTROL';
                case 'AU': return 'AUTORIZADA';
                case 'CA': return 'CANCELADA';
                case 'CP': return 'COMPLETADA';
                default: return 'OTRA';
            }
        },
        flex: 1
    }, {
        text: 'Nombre',
        dataIndex: 'aseguradoNombreCompleto',
        flex: 3
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaMovimientoDesc',
        flex: 3
    }, {
        text: 'Fallecido',
        dataIndex: 'fallecido',
        flex: 1
    }, {
        text: 'Fecha captura',
        dataIndex: 'fechaCaptura',
        renderer(v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        },
        flex: 1
    }, {
        text: 'Fecha creación',
        dataIndex: 'fechaCreacion',
        renderer(v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        },
        flex: 1
    }, {
        text: 'Fecha aprobacion',
        dataIndex: 'fechaAprobacion',
        renderer(v) {
            let fechaAprobacion = Ext.util.Format.date(v, 'd/m/Y');
            return fechaAprobacion !== '01/01/0001' ? fechaAprobacion : '';
        },
        flex: 1
    }, {
        text: 'Importe a pagar',
        dataIndex: 'importePagar',
        renderer(v) {
            return `${Ext.util.Format.usMoney(v)}`;
        },
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: 80,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'ver detalles',
            handler: 'onMostrarDetallesDevolucion'
        }, {
            iconCls: 'x-fa fa-print',
            tooltip: 'imprimir solicitud',
            handler: 'onImprimirSolicitud'
        }, {
            iconCls: 'x-fa fa-pencil',
            tooltip: 'editar solicitud',
            handler: 'onEditarSolicitud',
            // getClass(value, meta, record) {
            //     const etapa = record.get('etapa');
            //     console.log(etapa);
            //     return (etapa !== 'CP' && etapa !== 'CA') ?
            //         'x-fa fa-pencil' : // muestra icono editar
            //         'x-hide-display';
            // },
            isDisabled(view, rowIndex, collIndex, item, record) {
                // const disabled = true;
                const etapa = record.get('etapa');
                return (etapa === 'CP' || etapa === 'CA');
                // console.log(etapa);
                // switch (etapa) {
                //     case 'CP': return;
                //     case 'CA': return;
                // }
            }
        }]
    }]
});