Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.CertificacionDependenciaForma', {
    extend: 'Ext.form.Panel',
    xtype: 'certificacion-dependencia-forma',

    requires: [
        'Pce.ingresos.asegurado.procesos.devolucionFondo.CertificacionDepAfiliadoTabla',
        'Pce.afiliacion.dependencia.DependenciaAseguradoStore',
        'Pce.afiliacion.dependencia.DependenciaStore'
    ],

    title: 'Certificación de la institución Afiliada',

    layout: 'hbox',

    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'dependencia-asegurado-tabla',
        reference: 'dependencia-tabla',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'combobox',
                name: 'adeudaDependencia',
                reference: 'adeudoDependencia',
                fieldLabel: 'Adeudo',
                displayField: 'adeudoDependencia',
                valueField: 'valor',
                allowBlank: false,
                store: {
                    data: [
                        {adeudoDependencia: 'NO', valor: false},
                        {adeudoDependencia: 'SI', valor: true}
                    ]
                },
                bind: {
                    value: '{adeudaDep}',
                },
                value: false,
                flex: 1
            }, {
                xtype: 'combobox',
                reference: 'dependenciaAseguradoSelector',
                name: 'numDependenciaAdeudo',
                fieldLabel: 'Dependencia',
                displayField: 'descripcion',
                readOnly: true,
                cls: 'read-only',
                store: {
                    type: 'dependencias',
                    proxy: {
                        url: '/api/afiliacion/dependencia-asegurado/por-asegurado',
                        type: 'rest'
                    }
                },
                bind: {
                    disabled: '{!adeudaDep}',
                    value: '{ultimaDependenciaDesc}'
                },
                flex: 3
            }, ]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'numberfield',
                name: 'importe',
                reference: 'importeDependencia',
                fieldLabel: 'Importe',
                maxValue: 999999999.99,
                bind: {
                    disabled: '{!adeudaDep}',
                    value: '{importeDep}'
                },
                listeners: {
                    change: 'onTotalFondoChange'
                },
                flex: 1
            }, {
                xtype: 'datefield',
                name: 'fechaRegistrosDep',
                fieldLabel: 'Registros al',
                maxValue: new Date(),
                bind: {
                    value: '{fechaRegistrosDep}'
                },
                flex: 2
            }, {
                xtype: 'textfield',
                name: 'usuarioCertificaDependencia',
                fieldLabel: 'Usuario certificador',
                bind: {
                    value: '{usuarioCertificaDependencia}'
                },
                labelWidth: 130,
                flex: 2
            },{
                xtype: 'button',
                reference: 'botonGenerarChequeDep',
                text: 'Generar cheque',
                ui: 'default',
                handler: 'onGenerarChequeDependencia',
                bind: {
                    disabled: '{!adeudaDep}'
                },
                colspan: 2
            }]
        }]
    }, {

    }]


});