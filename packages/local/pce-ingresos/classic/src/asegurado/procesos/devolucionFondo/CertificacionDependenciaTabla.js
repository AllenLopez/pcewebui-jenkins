Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.CertificacionDependenciaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'certificacion-dependencia-tabla',

    requires: [
        'Pce.ingresos.asegurado.fondo.CertificacionDependenciaDevolucionStore'
    ],

    store: {
        type: 'certificacion-dependencia-store'
    },

    defaults: {
        width: '100%'
    },

    emptyText: 'No hay regisros de adeudo',

    tbar: [{
        text: 'Agregar adeudo en dependencia',
        handler: 'onAgregarNuevoAdeudoDependencia',
        bind: {
            disabled: '{!solicitudValida}'
        }
    }, {
        xtype: 'textfield',
        name: 'usuarioCertificacionDependencia',
        fieldLabel: 'Usuario certificador',
        allowBlank: false,
        bind: {
            value: '{usuarioCertificaDependencia}'
        },
        labelWidth: 150,
        width: '70%'
    }],
    columns: [{
        text: 'Número dependencia',
        dataIndex: 'numeroDependencia',
        flex: 1
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        flex: 3
    },{
        text: 'Importe',
        dataIndex: 'importe',
        renderer(v) {
            return Ext.util.Format.usMoney(v);
        },
        flex: 1
    }],
});