/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.ConsultaSolicitudDevolucionPanel
 * @extends Ext.form.Panel
 * @xtype consulta-solicitud-devolucion-panel
 * Pantalla que muestra y filtra solicitudes por numero de afiliado, solicitud, estatus y fecha inicio y final
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.ConsultaSolicitudDevolucionPanel' ,{
    extend: 'Ext.form.Panel',
    xtype: 'consulta-solicitud-devolucion-panel',

    requires: [
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.ingresos.asegurado.fondo.SolicitudDevolucionStore',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.SolicitudDevolucionTabla'
    ],

    controller: 'consulta-solicitud-devolucion-controller',

    viewModel: {
        data: {
            fechaInicial: null,
            fechaFinal: null,
            numeroAfiliacion: null,
            etapa: null,
            folioSolicitud: null,
            solicitud: null
        }
    },

    title: 'Consulta solicitudes de devolución de fondo propio',
    emptyText: 'No hay datos para mostrar',
    layout: 'fit',

    items: [{
        xtype: 'solicitud-devolucion-tabla',
        reference: 'solicitudDevolucionTabla',
        store: {
            type: 'fondo-solicitud-devolucion',
            proxy: {
                type: 'rest',
                url: '/api/ingresos/asegurado/fondo/solicitud-devolucion/por-filtro'
            }
        },
        bind: {
            selection: '{solicitud}'
        },
        emptyText: 'No hay solicitudes para mostrar',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'numberfield',
                name: 'id',
                fieldLabel: 'Folio solicitud',
                bind: {
                    value: '{folioSolicitud}'
                },
                flex: 1
            }, {
                xtype: 'asegurado-selector',
                name: 'numafil',
                fieldLabel: 'Asegurado',
                bind: {
                    value: '{numeroAfiliacion}'
                },
                flex: 3
            }, {
                xtype: 'combobox',
                name: 'estatus',
                fieldLabel: 'Estatus',
                displayField: 'etapa',
                valueField: 'value',
                store: {
                    data: [
                        { etapa: 'AUTORIZADA', value: 'AU' },
                        { etapa: 'CANCELADA', value: 'CA' },
                        { etapa: 'TRAMITE', value: 'TR' },
                        { etapa: 'COMPLETADA', value: 'CP' }
                    ]
                },
                queryMode: 'local',
                bind: {
                    value: '{etapa}'
                },
                flex: 1
            }]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Desde',
                name: 'fechaInicio',
                bind: {
                    value: '{fechaInicial}',
                    maxValue: '{fechaFinal}'
                },
                flex: 2
            }, {
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                reference: 'fechaFinal',
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicial}',
                    value: '{fechaFinal}'
                },
                flex: 2
            }, {
                // text: 'Buscar',
                iconCls: 'x-fa fa-search',
                handler: 'onBuscarSolicitud',
                flex: 1
            }, {
                text: 'Generación de reporte',
                handler: 'onGenerarReporte',
                flex: 1
            }],
        }, {
            xtype: 'toolbar',
            dock: 'top',
            bind: {
                disabled: '{!solicitud}'
            },
            items: [{
                text: 'Autorizar',
                tooltip:'Realizar autorización de solicitud',
                disabled: true,
                bind: {
                    disabled: '{!solicitud || solicitud.etapa !== "TR"}'
                },
                handler: 'onAutorizarTap'
            }, {
                text: 'Solicitud de cheque',
                tooltip: '',
                bind: {
                    disabled: '{!solicitud || solicitud.etapa !== "AU"}'
                },
                handler: 'onMostrarSolicitudChequeTap'
            }, {
                text: 'Cancelar',
                tooltip: 'Realizar cancelación de solicitud',
                disabled: true,
                bind: {
                    disabled: '{!solicitud || solicitud.etapa === "CP" || solicitud.etapa === "CA"}'
                },
                handler: 'onCancelarTap'
            }, {
                xtype: 'tbseparator'
            }, {
                text: 'Realiza afectación',
                disabled: true,
                tooltip: 'Realizar devolución de fondo propio',
                bind: {
                    disabled: '{!solicitud || solicitud.etapa !== "AU"}'
                },
                handler: 'onRealizarAfectacionTap'
            }]
        }]
    }]
});