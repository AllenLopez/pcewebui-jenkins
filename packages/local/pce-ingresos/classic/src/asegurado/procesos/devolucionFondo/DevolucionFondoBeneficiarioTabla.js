Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoBeneficiarioTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'fondo-beneficiario-tabla',

    requires: [
        // 'Pce.ingresos.asegurado.fondo.BeneficiarioStore'
    ],

    columns: [{
        text: 'Nombre',
        dataIndex: 'nombre',
        flex: 1
    }, {
        text: 'Parentesco',
        dataIndex: 'parentesco',
        flex: 1
    }, {
        text: 'Tutor',
        dataIndex: 'tutor',
        flex: 1
    }, {
        text: '%',
        dataIndex: 'porcentaje',
        flex: 1
    }]
});