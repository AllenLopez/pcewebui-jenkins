/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoDetallePanel
 * @extends Ext.form.Panel
 * @xtype devolucion-fondo-detalle-panel
 * @author Alan López
 * Panel que despliega y muestra como solo-lectura información del detalle de la solicitud
 */
Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoDetallePanel', {
    extend: 'Ext.form.Panel',
    xtype: 'devolucion-fondo-detalle',

    requires: [
        'Pce.ingresos.asegurado.procesos.devolucionFondo.ChequeFondoDevolucionTabla'
    ],

    defaults: {
        width: '100%'
    },

    padding: 10,

    emptyText: 'No hay cheques agregados',

    layout: {
        type: 'table',
        columns: 5
    },
    items: [{
        xtype: 'displayfield',
        bind: {
            value: '<b>{solicitud.asegurado}</b>'
        },
        fieldLabel: 'Núm. afiliado',
        colspan: 1
    }, {
        xtype: 'displayfield',
        bind: {
            value: '{solicitud.defuncion}'
        },
        renderer(v) {
            return v ? '<b>SI</b>': '<b>NO</b>';
        },
        fieldLabel: 'Fallecido',
        colspan: 1
    }, {
        xtype: 'displayfield',
        bind: {
            value: '<b>{solicitud.aseguradoNombreCompleto}</b>'
        },
        fieldLabel: 'Nombre completo',
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '<b>{solicitud.conceptoDesc}</b>'
        },
        fieldLabel: 'Concepto',
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '{solicitud.importePagar}'
        },
        renderer: v => `<b>${Ext.util.Format.usMoney(v)}</b>`,
        fieldLabel: 'Importe total',
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '{solicitud.etapa}'
        },
        renderer(v) {
            switch (v) {
                case 'TR': return '<b>EN TRAMITE</b>';
                case 'CA': return '<b>CANCELADA</b>';
                case 'AU': return '<b>AUTORIZADA</b>';
                default: return '<b>OTRA</b>';
            }
        },
        fieldLabel: 'Estatus',
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '<b>{solicitud.dependenciaMovimientoDesc}</b>'
        },
        fieldLabel: 'Dependencia',
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '<b>{solicitud.delegacionDescripcion}</b>'
        },
        fieldLabel: 'Delegacion',
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '<b>{solicitud.usuarioCertificaDependencia}</b>'
        },
        fieldLabel: 'Usuario certificador dependencia',
        labelWidth: 230,
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '{solicitud.importeDependencia}'
        },
        renderer: v => `<b>${Ext.util.Format.usMoney(v)}</b>`,
        fieldLabel: 'Importe deuda dependencia',
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '{solicitud.fechaRegistrosDep}'
        },
        renderer: v => `<b>${Ext.util.Format.date(v, 'd/m/Y')}</b>`,
        fieldLabel: 'Fecha registros',
        colspan: 2
    },{
        xtype: 'displayfield',
        bind: {
            value: '<b>{solicitud.usuarioCertificaServicioMedico}</b>'
        },
        fieldLabel: 'Usuario certificador servicio médico',
        labelWidth: 230,
        colspan: 2
    }, {
        xtype: 'displayfield',
        bind: {
            value: '{solicitud.importeServicioMedico}'
        },
        renderer(v) {
            return `<b>${Ext.util.Format.usMoney(v)}</b>`;
        },
        fieldLabel: 'Importe servicio médico',
        colspan: 2
    }, {
        xtype: 'cheque-devolucion-tabla',
        reference: 'chequeDevolucionTabla',
        columns: [{
            text: 'Núm. cheque',
            dataIndex: 'numCheque',
            renderer: v => { return v > 0 ? v : 'Pendiente emisión'; },
            flex: 2
        }, {
            text: 'Fecha emisión',
            dataIndex: 'fechaCheque',
            renderer: v => {
                let fechaCheque = Ext.util.Format.date(v, 'd/m/Y');
                return fechaCheque !== '01/01/0001' ? fechaCheque : '';
            },
            flex: 2
        }, {
            text: 'Estatus',
            dataIndex: 'estatus',
            renderer(v) {
                switch (v) {
                    case 'TR': return 'EN TRAMITE';
                    case 'CA': return 'CANCELADO';
                    case 'AU': return 'AUTORIZADO';
                    case 'CP': return 'COMPLETADO';
                    default: return 'OTRO';
                }
            },
            flex: 2
        }, {
            text: 'Beneficiario',
            dataIndex: 'beneficiario',
            flex: 6
        },{
            text: 'RFC',
            dataIndex: 'rfc',
            flex: 3
        },{
            text: 'Importe',
            dataIndex: 'importe',
            renderer(v) {
                return Ext.util.Format.usMoney(v);
            },
            flex: 3
        }],
        colspan: 6
    }, ],

    afterRender() {
        this.callParent();

        let solicitudId = this.getViewModel().get('solicitud.id');
        this.down('grid').getStore().load({
            params: {
                solicitudId: solicitudId
            }
        });
    },
});