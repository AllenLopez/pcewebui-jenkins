/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.EditorDevolucionFondoDetalleController
 * @extends Ext.app.ViewController
 * @alias controller.editor-devolucion-solicitud
 * @author Alan López
 * Controlador de pantalla de edición de solicitud de devolución de fondo propio
 */

Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.EditorDevolucionFondoDetalleController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.editor-devolucion-solicitud',

    helperDevolucion: Pce.asegurado.procesos.devolucionFondo.ActualizaSolicitudDevolucionHelper,
    porcentajeTotal: 0,
    registros: 0,
    solicitud: null,
    asegurado: null,
    datosPce: null,

    init() {
        this.cargaDatosSolicitud();
    },

    cargaDatosSolicitud() {
        const me = this;
        const solicitudId = parseInt(me.getView().solicitudId);
        if (solicitudId && solicitudId > 0) {
            me.cargarSolicitud(solicitudId, me);
        }
    },

    cargarSolicitud(solicitudId, me) {
        Pce.ingresos.asegurado.fondo.SolicitudDevolucion.load(solicitudId, {
            success(record) {
                const aseguradoId = record.get('asegurado');

                me.solicitud = record;
                me.setViewModelDataProp('solicitud', record);
                me.getEstadoDeCuenta(aseguradoId, me);
                me.getAsegurado(aseguradoId);
                me.getInfoAsegurado(aseguradoId);
                me.getUltimaDependenciaAsegurado(aseguradoId, me);
                me.getChequesSolicitud(solicitudId);
                me.getBeneficiarios(aseguradoId, solicitudId);
                me.updateTotalBeneficiarios(record.get('totalChequesDevolucion'));
            }
        });
    },

    cargaDatosPensiones() {
        const PceId = 8; // Id de dependencia PCE
        return new Promise((resolve) => {
            Pce.afiliacion.dependencia.Dependencia.load(PceId, {
                success: model => { resolve(model); }
            });
        });
    },

    mostrarDialogoCheque(registro) {
        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'Editar pago',
            record: registro,
            width: 600,
            items: { xtype: 'cheque-fondo-forma' },
            listeners: { guardar: this.onGuardarChequeSolicitud.bind(this) }
        });
    },

    buscaConceptoImporteValidos(nodes) {
        const ids = [];
        const conceptos = ['A','H','I'];
        let importeAsociado = 0;
        let importeJ = 0;
        let importeI = 0;

        const conceptoSaldo = this.obtieneConceptoSaldoDesdeUI(nodes);

        conceptoSaldo.forEach(({ concepto, elementId, importe }) => {
            importe = Math.abs(importe);

            if (conceptos.includes(concepto) && importe > 0) {
                ids.push(elementId);
                importeAsociado = importe;
            }
            if (concepto === 'J') { importeJ = importe; }
            if (concepto === 'I') { importeI = importe; }
        });

        importeAsociado = (importeJ > 0 && importeI > 0) ? importeJ + importeI : importeAsociado ;

        if (importeAsociado !== 0) { this.setImportePorClave(importeAsociado); }

        return ids;
    },

    obtieneConceptoSaldoDesdeUI(nodos) {
        const rowError = ' row-error';

        return nodos.map(item => {
            const elementId = item.id;
            const concepto = item.rows[0].cells[0].innerText;
            const className = item.className;
            const importe = this.convierteValorNumerico(item.rows[0].cells[3].textContent);

            if(concepto === 'C' && importe !== 0) { // Marca de color el registro que no aplica para devolución
                item.className = item.className.concat(rowError);
            }

            return { concepto, elementId, importe, className };
        });
    },

    marcaRegistrosInvalidos(nodes, ids, rowError) {
        if (ids.length > 1) {
            nodes.forEach(el => {
                if (ids.includes(el.id)) {
                    el.className = el.className.concat(rowError);

                    this.invalidaSolicitud(`El usuario tiene saldo en dos o mas cuentas.
                    Primero debe realizar un traspaso antes de continuar con la solicitud`);
                }
            });
        }
    },

    setImportePorClave(valor) {
        let importeAbs = Math.abs(valor);
        const claveRegimen = this.getViewModelDataProp('aseguradoInfo').data.claveRegimen;
        const clave = this.helperDevolucion
            .findHomologoClaveConcepto(claveRegimen);

        if (!clave) { importeAbs = 0; }

        const solicitud = this.getViewModelDataProp('solicitud');
        solicitud.set('importeFondo',importeAbs);
        this.setViewModelDataProp('solicitud', solicitud);
    },

    validaFondoDevolucion() {
        if (this.getViewModelDataProp('solicitud').get('totalFondo') <= 0) {
            Ext.Msg.alert('Fondos insuficientes',
                'No es posible agregar beneficiarios y pagos, no hay fondos suficientes');

            this.setViewModelDataProp('solicitudValida', false);
            return false;
        }
        return true;
    },

    validaPorcentajeDevolucionUsado(dialogo) {
        if (this.porcentajeTotal > 100) {
            // this.invalidaSolicitud(`El porcentaje aplicado excede del 100, por favor revise datos`);
            Ext.Msg.alert('Porcentaje Excedido',`El porcentaje aplicado excede del 100, por favor revise datos`);
            dialogo.close();
            Ext.getBody().unmask();
            // this.lookup('nuevoBeneficiarioBoton').disable();
        } else {
            if (this.porcentajeTotal === 100) {
                this.lookup('nuevoBeneficiarioBoton').disable();
                this.setViewModelDataProp('solicitudValida', true);
            }
            const fallecido = this.getViewModelDataProp('solicitud').get('fallecido');
            if (fallecido && this.porcentajeTotal < 100) {
                this.setViewModelDataProp('solicitudValida', true);
                this.lookup('nuevoBeneficiarioBoton').enable();
            }
        }
    },

    insertarBeneficiariosTabla(beneficiario) {
        this.getStoreBeneficiarioTabla().insert(0, [beneficiario]);
    },

    updateTotalBeneficiarios(cantidad) {
        const solicitud = this.getViewModelDataProp('solicitud');
        // solicitud.set('totalBeneficiarios', cantidad);
        solicitud.set('totalChequesDevolucion', cantidad);
        this.setViewModelDataProp('solicitud', solicitud);
    },

    updateImporteDeudaCheque(comp, tipoEmision) {
        if (comp.isDirty()) {
            const newValue = comp.getValue();
            if (newValue > 0 && newValue < this.getViewModelDataProp('solicitud').get('importePagar')) {
                const store = this.getStoreCheques();
                if (store.count() > 0) {
                    const chequeDep = store.getData().items.find(chq => chq.get('tipoEmision') === tipoEmision);
                    if (chequeDep) {
                        chequeDep.set('importe', newValue);
                    }
                }
                // this.actualizaAseguradoCheque(100);
            }
        }
    },
    obtieneStoreBeneficiarioTabla() {
        let tabla = this.obtieneBeneficiarioTabla();
        return tabla.getStore();
    },
    obtieneBeneficiarioTabla() {
        return this.lookup('beneficiarioTabla');
    },
    onTotalFondoChange(field, newValue) {
        let asegurado = this.getViewModelDataProp('asegurado');

        if (asegurado && newValue > 0) {
            let storeBenefs = this.obtieneStoreBeneficiarioTabla();

            storeBenefs.removeAll();

            if (this.fallecido) {
                if (this.registros.length > 0) {
                    this.cargaBeneficiariosTabla();
                    this.actualizaTotalBeneficiarios(this.registros.length);
                }
            } else {
                this.actualizaAseguradoCheque(100);
            }
        }
    },
    onEditarTotalDevolucion() {
        let solicitud = this.getViewModelDataProp('solicitud');
        let importePagar = solicitud.data.importePagar;
        let formPanel = Ext.create('Ext.form.Panel', {
            reference: 'formTotalFondo',
            items: [{
                xtype: 'numberfield',
                name: 'importePagar',
                allowBlank: false,
                minValue: 1,
                fieldLabel: 'Importe a pagar',
                value: importePagar
            }]
        });

        Ext.create('Pce.view.dialogo.FormaContenedor', {
            defaultListenerScope: true,
            modal: true,
            autoShow: true,
            layout: 'fit',
            bodyPadding: 10,
            buttons: [{
                text: 'Aceptar',
                handler: 'onAceptar'
            }, {
                text: 'Cancelar',
                handler: 'onCancelar'
            }],
            items: [{
                xtype: formPanel
            }],
            onAceptar() {
                let form = this.down('form');
                if (form.isValid()) {
                    this.fireEvent('guardar', this, form.getForm().getFieldValues().importePagar);
                }
            },
            onCancelar() {
                this.close();
            },
            listeners: {
                guardar: this.onEditarTotalFondo.bind(this)
            }
        });
    },
    onEditarTotalFondo(dialogo, valor) {
        const solicitud = this.getViewModelDataProp('solicitud');
        let importeFondo = solicitud.data.importeFondo;

        if(valor > 0 && valor <= importeFondo) {
            const aseguradoId = this.asegurado.getId();
            const solicitudId = parseInt(this.getView().solicitudId);
            let solicitud = this.getViewModelDataProp('solicitud');
            solicitud.data.importePagar = valor;
            solicitud.dirty = true;
            this.setViewModelDataProp('solicitud', solicitud);
            this.lookup('importePagar').setValue(valor);
            this.getBeneficiarios(aseguradoId,solicitudId);
        }else{
            this.mostrarMensajeAlerta('Importe Invalido', 'Favor de ingresar un importe valido' );
        }
        
        dialogo.close();
    },



    actualizaAseguradoCheque(newValue,porcentaje){
        this.porcentajeTotal = porcentaje;
        let store = this.getStoreCheques();
        let importe = this.calculaImportePorPorcentaje(porcentaje);

        let chequeBeneficiario = this.creaChequeAsegurado(porcentaje, importe);

        this.actualizaChequeBeneficiario(store, chequeBeneficiario);
    },

    actualizaChequeBeneficiario(store, nuevoCheque) {
        if(nuevoCheque.get('importe') > 0) {
            if(store.data.items.length > 0){
                store.splice(0,1,[nuevoCheque]);
                this.agregaNuevoBeneficiarioSolicitud(store.count());
            }
            else{
                store.insert(0, [nuevoCheque]);
                this.agregaNuevoBeneficiarioSolicitud(store.count());
            }
        }
    },

    preparaCalculoPorcentajeTotalDevolucion() {
        let chequeBeneficiario;
        let store = this.getStoreCheques();
        this.porcentajeTotal = 0;

        Ext.getBody().mask('calculando pagos por beneficiario...');

        if (this.fallecido) {
                if (this.registros.length > 0) {
                    this.registros.forEach(benef => { // prepara cheques por cada beneficiario
                        this.porcentajeTotal += benef.porcentaje;

                        chequeBeneficiario = this.preparaChequeCalculoBenef(benef); // convierte beneficiario a cheque
                        this.validaChequeBeneficiarioExistente(chequeBeneficiario);
                    });
                }
        }
        else {
            this.preparaAseguradoCheque(100);
        }

        this.validaPorcentajeDevolucionUsado();
        Ext.getBody().unmask();
    },

    mostrarDialogoBeneficiario(beneficiario) {
        if (this.validaFondoDevolucion()) {
            let titulo = 'Nuevo beneficiario';

            Ext.create('Pce.view.dialogo.FormaContenedor', {
                title: titulo,
                record: beneficiario,
                width: 900,
                items: {
                    xtype: 'beneficiario-test-forma'
                },
                listeners: {
                    guardar: this.onGuardarBeneficiarioTest.bind(this)
                }
            });
        }
    },

    onFallecidoCheck(element, opt) {
        this.fallecido = opt;
        const store = this.getStoreCheques();
        const solicitud = this.getViewModelDataProp('solicitud');

        if (this.asegurado) {
            if (opt) {
                store.removeAll();
                this.setViewModelDataProp('bajaDependencias', true);
                this.setViewModelDataProp('solicitudValida', true);
                this.cargaBeneficiariosTabla(opt);
                this.updateTotalBeneficiarios(this.registros.length);
                solicitud.set('fallecido', 'SI');
                this.setViewModelDataProp('solicitud', solicitud);
                this.lookup('adeudoDependenciaSelector').setValue('false');
                this.lookup('AdeudoSMForma').reset();
                
            } else {
                store.removeAll();
                this.updateTotalBeneficiarios(1);
                this.validaBajaDependencias(this.asegurado);
                solicitud.set('fallecido', 'NO');
                this.setViewModelDataProp('solicitud', solicitud);
                this.InsertaBeneficiarioAgain(100, this.asegurado,store);
                this.lookup('adeudoDependenciaSelector').setValue('false');
                this.lookup('AdeudoSMForma').reset();
                this.lookup('beneficiarioTabla').getStore().reload();
            }
        }
    },

    InsertaBeneficiarioAgain(porcentaje, asegurado,store){
        this.porcentajeTotal = porcentaje;
        let importe = this.calculaImportePorPorcentaje(porcentaje);

        store.data.items.forEach(benef => {
        if(benef.data.beneficiario == asegurado.data.nombreCompleto){
            store.removeAll();
        }});
        let chequeBeneficiario = this.creaChequeAsegurado(porcentaje, importe);
        store.insert(0, [chequeBeneficiario]);
        this.agregaNuevoBeneficiarioSolicitud(store.count());
    },


    onImporteDevolucionChange(control, newValue, oldValue) {
        if (newValue > 0 && newValue < oldValue) {
            const solicitud = this.getViewModelDataProp('solicitud');

            if (!solicitud.get('defuncion')) {
                const chequeAfiliado = this.getStoreCheques().getData()
                    .items.find(chq => chq.get('tipoEmision') === 'AFI');

                if (chequeAfiliado) {
                    chequeAfiliado.set('importe', newValue);
                }
            } else {
                    // storeCheques.removeAll();
                    // this.preparaChequeAsegurado(100, datosAsegurado);
                // if () {
                    this.cargaBeneficiariosTabla(solicitud.get('defuncion'));
                    this.updateTotalBeneficiarios(this.registros.length);
                // } else {
                // }
            }
        }
    },

    onImporteDependenciaChange(control, evnt) {
        const comp = evnt.fromComponent;
        if (comp) {
            this.updateImporteDeudaCheque(comp, 'DEP');
        }
    },

    onImporteServicioMedicoChange(control, evnt) {
        const comp = evnt.fromComponent;
        if (comp) {
            this.updateImporteDeudaCheque(comp, 'PCE');
        }
    },

    onGuardarBeneficiarioTest(dialogo, beneficiario) {
        const form = dialogo.down('beneficiario-test-forma').getForm();
        let store = this.getStoreCheques();
        const solicitud = this.getViewModelDataProp('solicitud');
        let importeFondo = solicitud.data.importeFondo;
        var idBeneficiario = this.registros.length + 1; 

        if (!form.isValid()) { return; }

        beneficiario.set('parentesco', dialogo.down('parentesco-beneficiario-selector').getRawValue());
        beneficiario.set('tipo', dialogo.down('tipo-beneficiario-selector').getRawValue());
        beneficiario.set('numeroBeneficiario',idBeneficiario);

        if (this.registros.length > 0) {
            this.porcentajeTotal = 0;
            this.registros.forEach(benef => { 
                this.porcentajeTotal += benef.porcentaje;
            });
            this.porcentajeTotal += beneficiario.getData().porcentaje;
            if(this.porcentajeTotal > 100){
                Ext.Msg.alert('Porcentaje Excedido',`El porcentaje aplicado excede del 100, por favor revise datos`);
                dialogo.close();
                Ext.getBody().unmask();
                return;
            }
        } 
        if(!store.findRecord('beneficiario',beneficiario.data.nombrePersona)) {
        this.registros.push(beneficiario.getData());
        this.insertarBeneficiariosTabla(beneficiario);
        this.updateTotalBeneficiarios(this.registros);
        this.preparaCalculoPorcentajeTotalDevolucion();
        this.validaPorcentajeDevolucionUsado(dialogo);
        this.calculaNetoaPagar(importeFondo,beneficiario.data.porcentaje, solicitud);
        this.guardarBeneficiarios(this.solicitud)
        .then(
            totalExito => {
            Ext.toast(`Nuevos beneficiarios procesados con exito: ${totalExito}`);
            Ext.Msg.alert(
                'Operación Completada',
                'Beneficiario registrado correctamente'
            );
            Ext.getBody().unmask();
            },
            failed => {
                console.log(failed);
                failed.forEach(ex => {
                    Ext.toast(ex);
                });
                Ext.getBody().unmask();
            
            });
        this.guardarCheques(this.solicitud)
        .then(
            totalExito => {
            Ext.toast(`Nuevos cheques procesados con exito: ${totalExito}`);
            Ext.Msg.alert(
                'Operación Completada',
                'Cheque registrado correctamente'
            );
            Ext.getBody().unmask();
            },
            failed => {
                console.log(failed);
                failed.forEach(ex => {
                    Ext.toast(ex);
                });
                Ext.getBody().unmask();
            
            });
        }else{
            this.mostrarMensajeAlerta(
                'Beneficiario Existente',
                'Este beneficiario ya fué dado de alta'
            );
        }
        dialogo.close();
    },

    guardarBeneficiarios(solicitud) {
        let storeBenef = this.obtieneStoreBeneficiarioTabla();
        let BenefData = storeBenef.getData();

        BenefData.items.forEach(el => {
            el.set('solicitudDevolucionId', solicitud.data.id);
        });
        return new Promise((resolve, reject) => {
            storeBenef.sync({
                success(batch){
                    console.log('beneficiario success');
                    resolve(batch.getTotal());
                },
                failure(batch){
                    console.log('beneficiario failure');
                    this.continuar = false;
                    reject(batch.getExceptions());
                }
            }); 
        });
    },
    guardarCheques(solicitud) {
        let me = this;
        console.log(`solicitud id: ${solicitud.getId()}`);
        let storeCheques = me.lookup('emisionChequeTabla').getStore();
        let chequesData = storeCheques.getData();

        chequesData.items.forEach(el => {
            el.set('solicitudDevolucionId', solicitud.data.id);
            solicitud.cheques().add(el);
        });
        console.log('procesando cheques...');
        return new Promise((resolve, reject) => {
            storeCheques.sync({
                success(batch){
                    console.log('success');
                    resolve(batch.getTotal());
                },
                failure(batch, options){
                    console.log('failure');
                    console.log(batch);
                    console.log(options);
                    me.continuar = false;
                    reject(batch.getExceptions());
                }
            });
        });
    },
    
    calculaNetoaPagar(importeFondo,porcentaje,solicitud){
        let importeDep = this.lookup('importeDep').value;
        let importeSM = this.lookup('importeSM').value;

        if(importeDep === null || importeDep === undefined)
        importeDep = 0;
        if(importeSM === null || importeSM === undefined)
        importeSM = 0;

        importeFondo - importeDep - importeSM;

        let netoPagar = this.getViewModelDataProp();
        if(netoPagar === undefined || netoPagar === null)
        netoPagar = 0;

        netoPagar += (importeFondo * porcentaje/100);
        
        // this.lookup('importePagar').setValue(netoPagar);
        solicitud.data.importePagar = netoPagar;
        this.setViewModelDataProp('solicitud',solicitud);
    },
    restaNetoaPagar(importeFondo,oldPorcentaje,solicitud){
        let importeDep = this.lookup('importeDep').value;
        let importeSM = this.lookup('importeSM').value;

        if(importeDep === null || importeDep === undefined)
        importeDep = 0;
        if(importeSM === null || importeSM === undefined)
        importeSM = 0;

        let importeTotal = importeFondo - importeDep - importeSM;

        
        let netoPagar = solicitud.data.importePagar;
        if(netoPagar === undefined || netoPagar === null)
        netoPagar = 0;
    
        netoPagar -= (importeTotal * oldPorcentaje/100);
        solicitud.data.importePagar = netoPagar;
        this.setViewModelDataProp('solicitud',solicitud);
    },
    mostrarDialogoBeneficiarioEdicion(beneficiario) {
        if(this.validaFondoDevolucion()) {
            let titulo = 'Editar beneficiario';

            Ext.create('Pce.view.dialogo.FormaContenedor', {
                title: titulo,
                record: beneficiario,
                width: 900,
                items: {
                    xtype: 'beneficiario-test-forma'
                },
                listeners: {
                    guardar: this.onGuardarBeneficiarioTestEdicion.bind(this)
                }
            });
        }
    },
    onGuardarBeneficiarioTestEdicion(dialogo, beneficiario){
        let form = dialogo.down('beneficiario-test-forma').getForm();
        let store = this.getStoreCheques();
        const solicitud = this.getViewModelDataProp('solicitud');
        let importeFondo = solicitud.data.importeFondo;
        let oldPorcentaje = beneficiario.modified.porcentaje;
        if(!form.isValid()) { return; }

        beneficiario.set('parentesco', dialogo.down('parentesco-beneficiario-selector').getRawValue());
        beneficiario.set('tipo', dialogo.down('tipo-beneficiario-selector').getRawValue());
        
        store.data.items.forEach(data => {
            if(data.data.numeroBeneficiario == beneficiario.data.numeroBeneficiario){
                data.set('beneficiario', beneficiario.data.nombrePersona);
                data.set('direccion',beneficiario.data.direccion);
                data.set('telefono',beneficiario.data.telefono);
                data.set('ciudad',beneficiario.data.ciudad);
                data.set('rfc',beneficiario.data.rfc);
            }
        });
        for (var i = 0; i < this.registros.length; i++){
            if (this.registros[i].numeroBeneficiario == beneficiario.data.numeroBeneficiario)
                this.registros[i] = beneficiario.data;
        }    
             
        this.insertarBeneficiariosTabla(beneficiario);
        this.preparaCalculoPorcentajeTotalDevolucion();
        this.onActualizaBeneficiarioBD(beneficiario);
        this.restaNetoaPagar(importeFondo,oldPorcentaje,solicitud);
        this.calculaNetoaPagar(importeFondo,beneficiario.data.porcentaje,solicitud);
        dialogo.close();

    },

    onActualizaBeneficiarioBD(beneficiario){
    //request al back end
    let storeBenef = this.lookup('beneficiarioTabla').getStore();
    let nombrePersona = beneficiario.data.nombrePersona;
    let id = beneficiario.data.id;
    let direccion = beneficiario.data.direccion;
    let telefono = beneficiario.data.telefono;
    let importe = beneficiario.data.importe;
    let rfc = beneficiario.data.rfc;
    let porcentaje = beneficiario.data.porcentaje;
    let ciudad = beneficiario.data.ciudad;
    let parentesco = beneficiario.data.parentescoBeneficiarioId;
    let tipo = beneficiario.data.tipoBeneficiarioId;

    Ext.getBody().mask('actualizando...');
    // if (beneficiario.dirty) {
        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/ingresos/asegurado/beneficiario-testament/actualizar-beneficiario',
            params: { beneficiario: nombrePersona,
                      id: id,
                      direccion: direccion,
                      ciudad: ciudad,
                      telefono: telefono,
                      importe: importe,
                      rfc: rfc,
                      porcentaje: porcentaje,
                      parentesco: parentesco,
                      tipo: tipo },
            // scope: this,
            success() {
                Ext.Msg.alert(
                'Operación completada',
                `Se ha realizado la actualización del beneficiario correctamente
                <b>${nombrePersona}</b>`);
            },
            failure() {
                Ext.Msg.alert('Error al actualizar beneficiario',
                    `Se originó un error al momento de actualizar el beneficiario en la Base de datos`);
            },
            callback() {
                Ext.getBody().unmask();
            }
        });
        Ext.getBody().unmask();
    },

    actualizaTotalBeneficiarios(numero) {
        this.setViewModelDataProp('totalBeneficiarios', numero);
    },


    onValidaConceptoImportePorRegistro(component) {
        const nodes = component.getNodes(0, component.getStore().count());

        const ids = this.buscaConceptoImporteValidos(nodes);
        this.marcaRegistrosInvalidos(nodes, ids, ' row-error');
    },

    onAgregarNuevoBenecificiario() {
       if(this.registros.length <= 0){
        this.onEditarTotalDevolucionxBenef();
        } else{
        let beneficiario = new Pce.ingresos.asegurado.BeneficiarioTestament();
        this.mostrarDialogoBeneficiario(beneficiario);
        }
    },

    onEditarTotalDevolucionxBenef() {
        let solicitud = this.getViewModelDataProp('solicitud');
        let importePagar = solicitud.data.importePagar;
        let formPanel = Ext.create('Ext.form.Panel', {
            reference: 'formTotalFondo',
            items: [{
                xtype: 'numberfield',
                name: 'importePagar',
                allowBlank: false,
                minValue: 1,
                fieldLabel: 'NETO A PAGAR',
                value: importePagar
            }]
        });

        Ext.create('Pce.view.dialogo.FormaContenedor', {
            title: 'CONFIRMAR NETO A PAGAR',
            defaultListenerScope: true,
            modal: true,
            autoShow: true,
            layout: 'fit',
            bodyPadding: 10,
            buttons: [{
                text: 'Aceptar',
                handler: 'onAceptar'
            }, {
                text: 'Cancelar',
                handler: 'onCancelar'
            }],
            items: [{
                xtype: formPanel
            }],
            onAceptar() {
                let form = this.down('form');
                if (form.isValid()) {
                    this.fireEvent('guardar', this, form.getForm().getFieldValues().importePagar);
                }
            },
            onCancelar() {
                this.close();
            },
            listeners: {
                guardar: this.onEditarTotalFondoxBenef.bind(this)
            }
        });
    },
    onEditarTotalFondoxBenef(dialogo, valor) {
        const solicitud = this.getViewModelDataProp('solicitud');
        let importeFondo = solicitud.data.importeFondo;

        if(valor > 0 && valor <= importeFondo) {
            const aseguradoId = this.asegurado.getId();
            const solicitudId = parseInt(this.getView().solicitudId);
            let solicitud = this.getViewModelDataProp('solicitud');
            solicitud.data.importePagar = valor;
            solicitud.dirty = true;
            this.setViewModelDataProp('solicitud', solicitud);
            this.lookup('importePagar').setValue(valor);
            this.getBeneficiarios(aseguradoId,solicitudId);
            let beneficiario = new Pce.ingresos.asegurado.BeneficiarioTestament();
            this.mostrarDialogoBeneficiario(beneficiario);
        }else{
            this.mostrarMensajeAlerta('Importe Invalido', 'Favor de ingresar un importe valido' );
        }
        
        dialogo.close();
        
    },

    // confirmarNetoaPagar(){
    //     let totalFondo = this.lookup('importePagar').value;
    //     if(totalFondo === undefined)
    //     totalFondo = 0;
    //     return new Promise(resolve => {
    //         Ext.Msg.confirm(
    //             'Confirmar Operación',
    //             `Se realizará el cálculo del porcentaje sobre el importe a pagar actual <b>${totalFondo}</b>
    //             ¿Deseas proceder?`,
    //             ans => resolve(ans)
    //         );
    //     });
    // },

    onGuardarChequeSolicitud(dialogo) {
        const form = dialogo.down('form').getForm();
        if (!form.isValid()) { return; }
        dialogo.close();
    },

    onEditarBeneficiario(view, rowIndex, collIndex, item, e, record) {
        this.mostrarDialogoBeneficiarioEdicion(record);
    },

    onEliminarCheque(view, rowIndex, collIndex, item, e, record){
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que desea eliminar este cheque a 
            <b><i>${record.get('beneficiario')}</i></b>?`,
            ans => {
                if (ans === 'yes'){
                    let storeCheques = this.getStoreCheques();
                    let chequeIndex = storeCheques.find('beneficiario', record.get('beneficiario'), null, null, null, true);
                    if (chequeIndex >= 0) {
                        let cheque = storeCheques.getAt(chequeIndex);
                        this.calculaImporteTotalCheques(cheque.get('importe'), 'resta');
                        view.getStore().removeAt(rowIndex);
                        // storeCheques.removeAt(chequeIndex);
                        this.setViewModelDataProp('solicitudValida', true);
                        if(cheque.data.tipoEmision == 'DEP'){
                            this.lookup('adeudoDependenciaSelector').setValue('false');
                            this.lookup('importeDep').setValue(0);
                        }
                        if(cheque.data.tipoEmision == 'SM'){
                            this.lookup('AdeudoSMForma').getForm().reset();
                            this.lookup('importeSM').setValue(0);
                        }
                    this.eliminarChequeBD(cheque.data.id);
                    } else {
                        this.mostrarMensajeAlerta('Error',
                            'No fue posible encontrar el pago correspondiente al cheque seleccionado');
                    }
                    this.validaPorcentajeDevolucionUsado()
                }
            }
        );
    },

    eliminarChequeBD(id){
        Ext.getBody().mask('actualizando...');
        // if (beneficiario.dirty) {
            Ext.Ajax.request({
                method: 'PUT',
                url: '/api/cajas/chequera/cheque-solicitud/eliminar-cheque',
                params: { id: id },
                // scope: this,
                success() {
                    Ext.Msg.alert(
                    'Operación completada',
                    `Se ha eliminado el cheque correctamente`);
                },
                failure() {
                    Ext.Msg.alert('Error al actualizar cheque',
                        `Se originó un error al momento de eliminar el cheque en la Base de datos`);
                },
                callback() {
                    Ext.getBody().unmask();
                }
            });
            Ext.getBody().unmask();
    },

    onEliminarBeneficiario(view, rowIndex, collIndex, item, e, record) {
        Ext.Msg.confirm(
            'Eliminar',
            `¿Seguro que deseas eliminar este beneficiario? 
            <b><i>${record.get('nombrePersona')}</i></b>`,
            ans => {
                if (ans === 'yes'){
                    let registro = null;
                    let storeCheques = this.getStoreCheques();
                    let chequeIndex = storeCheques.find('beneficiario', record.get('nombrePersona'), null, null, null, true);
                    const solicitud = this.getViewModelDataProp('solicitud');
                    let importeFondo = solicitud.data.importeFondo;
                    this.restaNetoaPagar(importeFondo,record.data.porcentaje,solicitud);
                    view.getStore().removeAt(rowIndex);
                    if(this.registros.length > 0){
                        for (var i = 0; i < this.registros.length; i++){
                            if (this.registros[i].nombrePersona == record.get('nombrePersona')){
                                registro = this.registros[i];
                                this.porcentajeTotal -= registro.porcentaje;
                                this.registros.splice(i,1);

                            }
                        }    
                    }
                    this.lookup('nuevoBeneficiarioBoton').enable();
                    if (chequeIndex >= 0) {
                        let cheque = storeCheques.getAt(chequeIndex);
                        this.calculaImporteTotalCheques(cheque.get('importe'), 'resta');

                        storeCheques.removeAt(chequeIndex);
                        this.eliminarChequeBD(cheque.data.id);
                    } else {
                        this.mostrarMensajeAlerta('Error',
                            'No fue posible encontrar el pago correspondiente al beneficiario seleccionado');
                    }
                    this.validaPorcentajeDevolucionUsado()
                    this.eliminaBeneficiarioBD(record.data.id);
                }
            }
        );
    },
    eliminaBeneficiarioBD(id){
        Ext.getBody().mask('actualizando...');
        // if (beneficiario.dirty) {
            Ext.Ajax.request({
                method: 'PUT',
                url: '/api/ingresos/asegurado/beneficiario-testament/eliminar-beneficiario',
                params: { beneficiarioId: id },
                // scope: this,
                success() {
                    Ext.Msg.alert(
                    'Operación completada',
                    `Se ha eliminado el beneficiario correctamente`);
                },
                failure() {
                    Ext.Msg.alert('Error al actualizar beneficiario',
                        `Se originó un error al momento de eliminar el beneficiario en la Base de datos`);
                },
                callback() {
                    Ext.getBody().unmask();
                }
            });
            Ext.getBody().unmask();
    },
    onEditarCheque(view, row, col, item, e, cheque) {
        this.mostrarDialogoCheque(cheque);
    },

    onActualizarSolicitud(){
        const solicitud = this.getViewModelDataProp('solicitud');
        const chequesStore = this.preparaChequesStore();
        if (solicitud.dirty || chequesStore.needsSync) {
            this.confirmarActualizacion(solicitud.getId()).then(ans => {
                if (ans === 'yes') {
                    this.helperDevolucion
                        .onActualizarSolicitud(solicitud, chequesStore);
                    Ext.util.History.back();
                }
            });
        } else {
            Ext.Msg.alert('Sin cambios', `No se ha registrado ningún cambio para actualizar`);
        }
    },

    onCancelarEdicion() {
        this.confirmarCancelacion().then(ans => {
            if (ans === 'yes') { Ext.util.History.back(); }
        });
    },

    onGenerarChequeSM(boton) {
        if (!this.getStoreCheques().findRecord('tipoEmision', 'PCE')) {
            const me = this;
            const solicitud = this.getViewModelDataProp('solicitud');
            const importeSM = solicitud.get('importeServicioMedico');

            solicitud.set({
                name: 'importePagar',
                importePagar: Math.round((solicitud.get('importePagar') - importeSM) * 100) / 100
            });

            if (!this.datosPce) {
                this.cargaDatosPensiones()
                    .then(pce => {
                        me.datosPce = pce;
                        const chequeSM = this.generaChequeSm(me.datosPce, importeSM);
                        this.insertaNuevoCheque(chequeSM);
                    });
            } else {
                const chequeSM = this.generaChequeSm(me.datosPce, importeSM);
                this.insertaNuevoCheque(chequeSM);
            }

            boton.disable();
        } else {
            this.mostrarMensajeAlerta('Cheque existente','ya existe cheque para servicio médico');
        }
    },

    onGenerarChequeDependencia(boton) {
        if (!this.getStoreCheques().findRecord('tipoEmision', 'DEP')) {
            const solicitud = this.getViewModelDataProp('solicitud');
            const importeDependencia = solicitud.get('importeDependencia');

            solicitud.set({
                name: 'importePagar',
                importePagar: Math.round((solicitud.get('importePagar') - importeDependencia) * 100) / 100
            });

            this.getDependencia(solicitud.get('dependenciaMovimiento'))
                .then(dependencia => {
                    const chequeDep = this.generaChequeDependencia(dependencia, importeDependencia);
                    this.insertaNuevoCheque(chequeDep);
                });

            boton.disable();
        } else {
            this.mostrarMensajeAlerta('Cheque existente','ya existe cheque para ésta dependencia');
        }
    },

    onAdeudoDependenciaChange(selector, valor) {
        if (!valor) {
            this.lookup('AdeudoDepForma').getForm().reset();
            const solicitud = this.getViewModelDataProp('solicitud');
            solicitud.set('dependenciaAdeudo', 0);
            this.setViewModelDataProp('solicitud', solicitud);
        }
    },

    validaBajaDependencias(asegurado) {
        let me = this;
        let store = this.lookup('dependencia-tabla').getStore();
        store.removeAll();

        Ext.Ajax.request({
            url: '/api/afiliacion/dependencia-asegurado/baja-dependencia',
            method: 'GET',
            params: { numeroAfiliacion: asegurado.getId() },
            callback(operation, success, response) {
                if (success) {
                    if (response.status !== 204) {
                        // Enlista dependencias que no registran BAJA. Se debe generar baja
                        // en todas y cada una antes de proceder.
                        // EXCEPCION: cuando es fallecido esta validacion es innecesaria
                        me.setDependenciasSinBaja(store, JSON.parse(response.responseText));
                    } else {
                        me.setViewModelDataProp('bajaDependencias', true);
                        me.setViewModelDataProp('solicitudValida', true);
                    }
                }
            }
        });
    },

    validaAdeudos(solicitud) {
        if (solicitud.modified) {
            const adeudos = Object.keys(solicitud.modified);
            if (adeudos.includes('importeDependencia')) {
                const store = this.getStoreCheques();
                const datos = store.getData().getRange();

                if (!datos.map(el => el.get('tipoEmision')).includes('DEPENDENCIA')) {
                    const cheque = this.creaCheque({
                        beneficiario: solicitud.get('aseguradoNombreCompleto'),
                        porcentaje: 100,
                        rfc: solicitud.get(''), // OBTENER RFC DEPENDENCIA
                        estatus: solicitud.get('estatus'),
                        importe: solicitud.get('importePagar'),
                        solicitudDevolucionId: solicitud.getId()
                    }, solicitud);
                    this.insertaChequeBeneficiario(store, cheque);
                }
            }
            if (adeudos.includes('importeServicioMedico')) {
                console.log('adeudoSM');
            }
        }
    },

    validaAdeudoDependencia() {

    },
    validaChequeBeneficiarioExistente(nuevoCheque) {
        let store = this.getStoreCheques();
        let cheques = store.getData().items;
        let nuevoBeneficiario = nuevoCheque.get('beneficiario');
       
        if(store.data.items.length > 0) {
            if(!store.findRecord('beneficiario',nuevoBeneficiario)) {
                this.insertaChequeBeneficiario(store, nuevoCheque);
            }else {
                let BenefExistente = store.findRecord('beneficiario',nuevoBeneficiario);
                BenefExistente.set('porcentaje',nuevoCheque.get('porcentaje')); 
                BenefExistente.set('importe',nuevoCheque.get('importe')); 

            }
        } 
        else{
        this.insertaChequeBeneficiario(store, nuevoCheque);
        }
    },

    generaChequeSm(datosPce, importeSM) {
        return this.creaCheque({
            rfc: datosPce.get('rfc'),
            nombrePersona: datosPce.get('descripcion'),
            razonSocial: datosPce.get('razonSocial'),
            porcentaje: 100,
            importe: importeSM,
            tipoEmision: 'PCE',
            etapa: this.solicitud.get('etapa'),
            solicitudId: this.solicitud.getId()
        });
    },

    generaChequeDependencia(dependencia, importeDependencia) {
        return this.creaCheque({
            rfc: dependencia.get('rfc') ? dependencia.get('rfc') : 'XAXX010101000',
            nombrePersona: dependencia.get('descripcion'),
            razonSocial: dependencia.get('razonSocial') ? dependencia.get('razonSocial') : 'PUBLICO EN GENERAL',
            porcentaje: 100,
            importe: importeDependencia,
            tipoEmision: 'DEP',
            etapa: this.solicitud.get('etapa'),
            solicitudId: this.solicitud.getId()
        });
    },

    insertaNuevoCheque(cheque) {
        const store = this.getStoreCheques();

        store.insert(store.getCount() + 1, [cheque]);
    },

    confirmarActualizacion(solicitudId) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `La solicitud con folio <b>${solicitudId}</b> ha sido modificada, ¿Desea guardar los cambios?`,
                ans => resolve(ans));
        });
    },

    confirmarCancelacion() {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `La solicitud perderá los cambios realizados, ¿Desea cancelar y volver?`,
                ans => resolve(ans));
        });
    },

    cargaBeneficiariosTabla(fallecido) {
        if (!fallecido) {
            this.preparaCalculoPorcentajeTotalDevolucion();
            this.agregaNuevoBeneficiarioSolicitud(1);
            return;
        }

        if (this.registros.length > 0) {
            this.getStoreBeneficiarioTabla().loadData(this.registros);

            if (this.sumaPorcentajesBeneficiariosAfil(this.registros) > 100) {
                this.invalidaSolicitud('La suma de porcentaje de beneficiarios excede el 100%. Es indispensable corregir datos en afiliación para poder proceder');
            } else {
                this.fallecido = fallecido;
                this.preparaCalculoPorcentajeTotalDevolucion();
            }
        }
    },

    sumaPorcentajesBeneficiariosAfil(items) {
        if(items.length > 0) {
            return items.map(item=> item.porcentaje).reduce((a,c)=>a+c);
        }
        return 100;
    },

    preparaChequesStore() {
        const store = this.getStoreCheques();
        store.setProxy({
            type: 'rest',
            url: '/api/cajas/chequera/cheque-solicitud',
            writer: { writeAllFields: true },
            api: {
                create: '/api/cajas/chequera/cheque-solicitud/',
                read: undefined,
                destroy: undefined,
                update: '/api/cajas/chequera/cheque-solicitud/actualizar'
            }
        });
        return store;
    },

    getStoreCheques() {
        const store = this.lookup('emisionChequeTabla').getStore();
        if (store) { return store; }
    },

    getStoreBeneficiarioTabla() {
        let tabla = this.getBeneficiarioTabla();
        if (tabla) { return tabla.getStore(); }
    },

    getBeneficiarioTabla() {
        const tabla = this.lookup('beneficiarioTabla');
        if (tabla) { return tabla; }
    },

    preparaChequeAsegurado(porcentaje, datosAsegurado) {
        this.porcentajeTotal = porcentaje;
        datosAsegurado.importe = this.calculaImportePorPorcentaje(porcentaje);

        const chequeBeneficiario = this.creaCheque(datosAsegurado);

        this.insertaChequeBeneficiario(this.getStoreCheques(), chequeBeneficiario);
    },

    preparaChequeCalculoBenef(beneficiario) {
        let importeCheque = this.calculaImportePorPorcentaje(beneficiario.porcentaje);
        
        this.calculaImporteTotalCheques(importeCheque, 'sum');
        beneficiario.importe = importeCheque;

        return this.creaCheque(beneficiario);
    },

    creaCheque(datos,importe) {
        return new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: datos.nombrePersona,
            porcentaje: datos.porcentaje,
            rfc: datos.rfc,
            estatus: datos.etapa,
            importe: datos.importe,
            tipoEmision: datos.tipoEmision,
            solicitudDevolucionId: datos.solicitudId,
            numeroBeneficiario: datos.numeroBeneficiario
        });
    },

    creaChequeAsegurado(porcentaje, importeCheque) {
        let asegurado = this.getViewModelDataProp('asegurado');
        return new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: asegurado.get('nombreCompleto'),
            porcentaje: porcentaje,
            rfc: asegurado.get('rfc'),
            importe: importeCheque,
            solicitudDevolucionId: 0
        });
    },

    insertaChequeBeneficiario(store, nuevoCheque) {
        store.insert(0, [nuevoCheque]);
        this.agregaNuevoBeneficiarioSolicitud(store.count());
    },

    agregaNuevoBeneficiarioSolicitud(cant) {
        this.setViewModelDataProp('totalBeneficiarios', cant);
    },

    calculaImportePorPorcentaje(porcentaje) {
        // let totalFondo = this.getViewModelDataProp('solicitud').get('importePagar');
        let totalFondo = this.lookup('importePagar').value;
        console.log(this.getViewModelDataProp('solicitud'));
        let importeDep = this.lookup('importeDep').value;
        let importeSM = this.lookup('importeSM').value;
        if(totalFondo <= (importeDep+importeSM)){
        return 0;   
        }else{
        return (totalFondo - importeDep - importeSM) * (porcentaje/100);
        }
    },

    calculaPorcentajeTotalDevolucion() {
        let chequeBeneficiario;
        this.porcentajeTotal = 0;

        Ext.getBody().mask('calculando pagos por beneficiario...');

        if(this.fallecido) {
            let store = this.getStoreCheques();
            store.removeAll();

            if(this.registros.length > 0) {
                this.registros.forEach(benef => { // prepara cheques por cada beneficiario
                    this.porcentajeTotal += benef.porcentaje;
                    chequeBeneficiario = this.preparaChequeCalculoBenef(benef); // convierte beneficiario a cheque
                    this.validaChequeBeneficiarioExistente(chequeBeneficiario);
                });
            }
        }
        else {
            this.preparaAseguradoCheque(100);
        }

        this.validaPorcentajeDevolucionUsado();
        Ext.getBody().unmask();
    },

    calculaImporteTotalCheques(importeCheque, oper) {
        let totalFondo = this.getViewModelDataProp('solicitud').get('importeFondo');
        let importeRestante = 0;

        if(oper === 'sum') {
            importeRestante = totalFondo - importeCheque;
        } else {
            importeRestante = totalFondo + importeCheque;
        }

        this.setViewModelDataProp('importeRestante', importeRestante);
    },

    invalidaSolicitud(msg) {
        this.mostrarMensajeAlerta('Solicitud inválida', msg);
        this.setViewModelDataProp('solicitudValida', false);
        this.lookup('tapGuardar').disable();
        this.lookup('AdeudoDepForma').disable();
        this.lookup('AdeudoSMForma').disable();
        this.lookup('beneficiarioTabla').disable();
        this.lookup('estadoCuentapanel').disable();
        this.lookup('emisionChequeTabla').disable();
    },

    convierteValorNumerico(dato) {
        let num = dato.replace(/[^0-9.-]+/g, '');
        return Number(num);
    },

    mostrarMensajeAlerta(titulo, mensaje) {
        Ext.Msg.alert(titulo, mensaje);
    },

    setDependenciasSinBaja(store, datos) {
        store.loadData(datos, false);

        this.setViewModelDataProp('bajaDependencias', false);
        this.invalidaSolicitud(`Asegurado aún no registra bajas en la(s) dependencia(s) que labora.
                            Debe registrar todas las bajas o ser fallecido antes de proceder con la solicitud.`);
    },

    setUltimaDependencia(numDep, desc) {
        this.setViewModelDataProp('ultimaDependenciaAsegurado', numDep);
        this.setViewModelDataProp('ultimaDependenciaDesc', desc);
    },

    getAsegurado(aseguradoId) {
        Pce.afiliacion.asegurado.Asegurado.load(aseguradoId, {
            success: afil => {
            this.setViewModelDataProp('asegurado', afil),
            this.asegurado = afil; }
        });
    },

    getUltimaDependenciaAsegurado(aseguradoId, me) {
        const store = me.lookup('dependenciaAseguradoSelector').getStore();

        store.load({
            params: { numeroAfiliacion: aseguradoId },
            callback(record, operation, success) {
                if (success) {
                    if (record.length > 0) {
                        me.setUltimaDependencia(
                            record[0].get('numeroDependencia'),
                            record[0].get('descripcion')
                        );
                    }
                }
            }
        });
    },

    getEstadoDeCuenta(aseguradoId, me) {
        me.lookup('aseguradoEstadoCuentaTabla').getStore().load({
            params: { numeroAfiliacion: aseguradoId }
        });
    },

    getBeneficiarios(aseguradoId,solicitudId) {
        let me = this;
        Ext.Ajax.request({
            url: '/api/ingresos/asegurado/beneficiario-testament/por-num-afiliacion',
            method: 'GET',
            params: { numeroAfiliacion: aseguradoId,
                      solicitudId: solicitudId },
            callback: (opt, success, response) => {
                if (success) { // obtiene y almacena el numero de beneficiarios de afiliación
                    me.registros = JSON.parse(response.responseText);
                        if (this.registros.length > 0) {
                            for(var i = 0; i < this.registros.length; i++){
                                this.porcentajeTotal += this.registros[i].porcentaje;
                                this.insertarBeneficiariosTabla(this.registros[i]);
                            }
                            if(this.porcentajeTotal >= 100){
                                this.lookup('nuevoBeneficiarioBoton').disable();
                            }
                        } 
                } else {
                    Ext.Msg.alert(
                        'Error al cargar beneficiarios',
                        `Ocurrió un error al cargar los beneficiarios asociados de éste asegurado`
                    );
                }
            }
        });
    },

    getChequesSolicitud(solicitudId) {
        this.getStoreCheques().load({
            params: { solicitudId: solicitudId },
        });
    },

    getInfoAsegurado(aseguradoId) {
        Pce.afiliacion.asegurado.AseguradoInfo.load(aseguradoId, {
            success: info => {
                this.setViewModelDataProp('aseguradoInfo', info);
                this.helperDevolucion
                    .determinaJubilacionAsegurado(aseguradoId, info)
                    .then(info => {
                        if (info.get('jubilado') === 'SI') {
                            this.invalidaSolicitud('Los asegurados jubilados no pueden solicitar la devolución de su fondo');
                        }
                });
            }
        });
    },

    getDependencia(dependenciaId) {
        return new Promise((resolve) => {
            Pce.afiliacion.dependencia.Dependencia.load(dependenciaId, {
                success: dependencia => {
                    if (dependencia) { resolve(dependencia); }
                }
            });
        });
    },

    getViewModelDataProp(prop) {
        let vm = this.getViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },

    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },
});