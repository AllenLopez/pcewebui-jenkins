/**
 * @class Pce.ingresos.asegurado.procesos.devolucionFondo.ConsultaSolicitudDevolucionController
 * @extends Ext.app.ViewController
 * @alias controller.consulta-solicitud-devolucion-controller
 * Controlador de pantalla ConsultaSolicitudDevolucionPanel que encapsula funcionalidad para consultar y filtrar listas de solicitudes elaboradas
 */

Ext.define('Pce.ingresos.asegurado.procesos.devolucionFondo.ConsultaSolicitudDevolucionController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-solicitud-devolucion-controller',

    requres: [
        'Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoDetallePanel'
    ],

    onBuscarSolicitud() {
        let solicitudFiltro = this.obtieneValoresFormulario();

        this.cargaSolicitudDevolucionTabla(solicitudFiltro);
    },

    onGenerarReporte() {
        let solicitudFiltro = this.obtieneValoresFormulario();

        this.imprimirReporteConsultaMovimientosDep(solicitudFiltro);
    },

    cargaSolicitudDevolucionTabla(solicitudFiltro) {
        let store = this.lookup('solicitudDevolucionTabla').getStore();
        store.load({
            params: solicitudFiltro
        });
    },

    onMostrarDetallesDevolucion(tabla, i, j, item, e, record) {
        this.mostrarDialogoChequesTabla(record);
    },

    mostrarDialogoChequesTabla(record) {
        let vm = Ext.create('Ext.app.ViewModel', {
            data: {
                solicitud: record.getData()
            }
        });

        Ext.create('Ext.window.Window',{
            title: 'Detalles de solicitud',
            autoShow: true,
            modal: true,
            width: 1200,
            height: 600,
            layout: 'fit',
            items: {
                xtype: 'devolucion-fondo-detalle',
                viewModel: vm,
            },
        });
    },

    onRealizarAfectacionTap() {
        let me = this;
        let solicitud = me.obtieneSolicitud();
        me.confirmarAfectacion(solicitud).then(ans => {
            if (ans === 'yes') {
                Ext.getBody().mask('Procesando...');

                solicitud.set('etapa', 'CP');
                solicitud.set('afectacion', true);
                    solicitud.save({ 
                    success(record) {
                            Ext.Ajax.request({
                            method: 'PUT',
                            url: '/api/cajas/chequera/cheque-solicitud/afectar',
                            params: { devolucionId: record.getId(),
                                      transaccionDevId: record.getData().resultado.transaccionDevId},
                                      
                            success(respCheque) {
                                // let respuestaCheque = JSON.parse(respCheque.responseText);

                                Ext.Msg.alert(
                                    'Operación completada',
                                    `Se ha realizado la afectación de cartera y cheque(s) exitosamente bajo la transacción:
                                    <b>${record.get('resultado').transaccionDevId}</b>`);
                                    // Se generó cheque con el folio: <b>${respuestaCheque.id}</b>
                                me.gridRefresh();
                            },
                            failure() {
                                Ext.Msg.alert('Error',
                                    'Ocurrio un error al intentar completar emisión del cheque de la solicitud');
                            },
                            callback() {
                                Ext.getBody().unmask();
                            }
                        });  
                    },
                    failure(record) {
                        Ext.Msg.alert(
                            'Error',
                            `Error al intentar la afectación correspondiente a la solicitud ${record.get('folio')}`
                        );
                        Ext.getBody().unmask();
                    }
                });
            }
        });
    },

    onAutorizarTap() {
        let solicitud = this.obtieneSolicitud();
        this.confirmarAutorizacion(solicitud).then(ans => {
            if (ans === 'yes') {
                Ext.getBody().mask('Autorizando...');

                solicitud.set('etapa', 'AU');
                solicitud.save({
                    success: this.onSolicitudAutorizarSuccess.bind(this),
                    failure: this.onSolicitudAutorizarFailure.bind(this)
                });
            }
        });
    },

    onCancelarTap() {
        let solicitud = this.obtieneSolicitud();
        this.confirmarCancelacion(solicitud).then(ans => {
            if (ans === 'yes') {
                Ext.getBody().mask('Cancelando...');

                solicitud.set('etapa', 'CA');
                solicitud.save({
                    success: this.onSolicitudCancelarSuccess.bind(this),
                    failure: this.onSolicitudCancelarFailure.bind(this)
                });
            }
        });
    },

    confirmarAutorizacion(solicitud) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `La solicitud ${solicitud.data.folio} será <b>autorizada</b>. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },
    confirmarCancelacion(solicitud) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `La solicitud ${solicitud.data.folio} será <b>cancelada</b>. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },
    confirmarAfectacion(solicitud) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se realizará la <b>afectación de cartera</b> para la solicitud con el folio: <b>${solicitud.data.folio}</b>.
                ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    onSolicitudAutorizarSuccess() {
        let solicitud = this.obtieneSolicitud();
        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/cajas/chequera/cheque-solicitud/autorizar',
            params: { devolucionId: solicitud.getId() },
            scope: this,
            success() {
                Ext.Msg.alert(
                    'Solicitud autorizada',
                    `Se ha realizado la autorización con éxito`
                );
            },
            failure() {
                Ext.Msg.alert('Solicitud autorizada con error',
                    `La solicitud se ha autorizado.
                    Sin embargo, ocurrió un error al intentar autorizar el pago de la solicitud`);
            },
            callback() {
                Ext.getBody().unmask();
            }
        });
    },
    onSolicitudAutorizarFailure() {
        Ext.Msg.alert('Error', 'Ha ocurrido un error y la solicitud no pudo ser autorizada');
    },

    onSolicitudCancelarSuccess() {
        let solicitud = this.obtieneSolicitud();
        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/cajas/chequera/cheque-solicitud/cancelar',
            params: { devolucionId: solicitud.getId() },
            scope: this,
            success() {
                Ext.Msg.alert(
                    'Solicitud cancelada',
                    `Se ha realizado la cancelación con éxito`
                );
            },
            failure() {
                Ext.Msg.alert('Solicitud cancelada con error',
                    `La solicitud se ha cancelado.
                    Sin embargo, ocurrió un error al intentar cancelar el pago de la solicitud`);
            },
            callback() {
                Ext.getBody().unmask();
            }
        });
    },
    onSolicitudCancelarFailure() {
        Ext.Msg.alert('Error', 'Ha ocurrido un error y la solicitud no pudo ser cancelada');
    },

    onSolicitudAfectacionSuccess(solicitud) {
        Ext.Msg.alert(
            'Operación completada',
            `Se ha realizado la afectación correspondiente a la solicitud ${solicitud.get('folio')} bajo la transacción
            <b>${solicitud.data.resultado.transaccionId}</b>.`
        );
        Ext.getBody().unmask();
    },
    onSolicitudAfectacionFailure(solicitud) {
        Ext.Msg.alert(
            'Error',
            `Error al intentar la afectación correspondiente a la solicitud ${solicitud.get('folio')}`
        );
        Ext.getBody().unmask();
    },

    onEditarSolicitud(tabla, i, j, item, e, record) {
        if (record) {
            this.redirectTo(`#edicion-solicitud-devolucion-fondo/${record.getId()}`);
        }
    },

    onMostrarSolicitudChequeTap() {
        let solicitud = this.obtieneSolicitud();
        if(solicitud) {
            this.imprimirSolicitudCheque(solicitud);
        }
    },

    onImprimirSolicitud(tabla, i, j, item, e, record) {
        this.imprimirSolicitudDevolucion(record.get('asegurado'), record.getId());
    },

    imprimirSolicitudDevolucion(numAfil, solicitudId) {
        window.open(`/api/ingresos/asegurado/fondo/solicitud-devolucion/descargar?pnum_afil=${numAfil}&pidsolicitud=${solicitudId}`);
    },

    imprimirSolicitudCheque(solicitud) {
        window.open(`/api/ingresos/asegurado/fondo/solicitud-devolucion/descargar-solicitud-cheque?pnum_afil=${solicitud.get('asegurado')}&pidsolicitud=${solicitud.getId()}`);
    },

    imprimirReporteConsultaMovimientosDep(solicitudFiltro) {
        let dateformat = 'd/m/Y';
        let fechaInicial = Ext.util.Format.date(solicitudFiltro.fechaInicial, dateformat);
        let fechaFinal = Ext.util.Format.date(solicitudFiltro.fechaFinal, dateformat);

        window.open(`/api/ingresos/asegurado/fondo/solicitud-devolucion/descargar-consulta-movimientos?pnum_afil=${solicitudFiltro.numeroAfiliacion}&pidsolicitud=${solicitudFiltro.folioSolicitud}&pestatus=${solicitudFiltro.estatus}&pfechainicial=${fechaInicial}&pfechafinal=${fechaFinal}`);
    },

    crearVMSolicitud(record) {
        return Ext.create('Ext.app.ViewModel', {
            data: {
                solicitud: record.getData()
            }
        });
    },

    obtieneValoresFormulario() {
        let asegurado = this.getViewModelDataProp('numeroAfiliacion');
        let folio = this.getViewModelDataProp('folioSolicitud');
        let estatus = this.getViewModelDataProp('etapa');
        let fechaInicial = this.getViewModelDataProp('fechaInicial');
        let fechaFinal = this.getViewModelDataProp('fechaFinal');

        return {
            numeroAfiliacion: asegurado,
            folioSolicitud: folio,
            estatus: estatus,
            fechaInicial: fechaInicial,
            fechaFinal: fechaFinal
        };
    },

    obtieneSolicitud() {
        return this.getViewModel().get('solicitud');
    },

    gridRefresh() {
        this.lookup('solicitudDevolucionTabla').getStore().reload();
    },

    getViewModelDataProp(prop) {
        let vm = this.getLocalViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },

    getLocalViewModel() {
        let vm = this.getViewModel();
        if(vm) { return vm; }
    }

});