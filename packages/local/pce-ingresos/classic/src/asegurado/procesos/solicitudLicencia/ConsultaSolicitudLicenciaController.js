/*
* @class Pce.ingresos.asegurado.procesos.solicitudLicencia.ConsultaSolicitudLicenciaController
* @extends Ext.app.ViewController
* @alias consulta-solicitud-licencia
* Controlador de panel de consulta de solicitudes de licencia por numero afiliación y dependencia
*
* @author Jorge Escamilla
* */

Ext.define('Pce.ingresos.asegurado.procesos.solicitudLicencia.ConsultaSolicitudLicenciaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-solicitud-licencia',

    onBuscarTap() {
        let { asegurado, dependencia } = this.getViewModel().getData();
        const store = this.lookup('solicitudLicenciaTabla').getStore();

        store.setProxy({
            type: 'rest',
            url: '/api/asegurado/licencia'
        }).load({
            params: {
                numAfil: asegurado,
                numDependencia: dependencia
            }
        });
    },

    onGenerarReporteTap() {
        const { asegurado } = this.getViewModel().getData();

        window.open(`/api/asegurado/licencia/descargar?numAfil=${asegurado}`);
    },
    cargarAfiliado(numeroAfiliacion) {
        let selectorAsegurado = this.lookup('aseguradoSelector');
        this.view.numeroAfiliacion = numeroAfiliacion;
        if (!numeroAfiliacion) {
            return;
        }

        Pce.afiliacion.asegurado.Asegurado.load(numeroAfiliacion, {
            success: (record) => {
                this.getViewModel().set('afiliado', record);

                selectorAsegurado.getStore().loadData([record]);
                selectorAsegurado.select(record);
            }
        });
    },

    onCambioAfiliado(selector, numeroAfiliacion) {
        this.cargarAfiliado(numeroAfiliacion);
    },

});