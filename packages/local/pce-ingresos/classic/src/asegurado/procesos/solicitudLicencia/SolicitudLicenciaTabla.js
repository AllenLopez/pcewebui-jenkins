/**
 * @class Pce.ingresos.asegurado.procesos.solicitudLicencia.SolicitudLicenciaTabla
 * @extends Ext.grid.Panel
 * @xtype solicitud-licencia-tabla}
 * 
 * Modificaciones:
 * - Tooltip en los campos ID Licencia, Asegurado, Fecha inicio, Fecha fin, Tipo licencia, Dependencia, Días licencia, Importe.
 * @author Alonso Rico
 */

Ext.define('Pce.ingresos.asegurado.procesos.solicitudLicencia.SolicitudLicenciaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'solicitud-licencia-tabla',

    requires: [
        'Pce.licenciaSolicitud.LicenciaSolicitudStore'
        // 'Pce.caja.licenciaSolicitud.LicenciaSolicitudStore'
    ],

    store: {
        type: 'licencia-solicitud'
    },

    columns: [{
        text: 'Id Licencia',
        dataIndex: 'id',
        tooltip: 'Identificador único de la licencia otorgada al asegurado.',
        flex: 1,
    }, {
        text: 'Asegurado',
        dataIndex: 'aseguradoNombreCompleto',
        tooltip: 'Nombre del asegurado que solicito licencia.',
        flex: 3,
    }, {
        text: 'Fecha inicio',
        dataIndex: 'fechaInicio',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        tooltip: 'Fecha en que inicio la licencia.',
        flex: 1,
    }, {
        text: 'Fecha fin',
        dataIndex: 'fechaFin',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        tooltip: 'Fecha en que termino la licencia.',
        flex: 1,
    }, {
        text: 'Tipo licencia',
        dataIndex: 'tipoLicencia',
        tooltip: 'Muestra el tipo de licencia otorgada: P, I, E,C.',
        flex: 1,
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        tooltip: 'Dependencia donde solicito la licencia.',
        flex: 4,
    }, {
        text: 'Dias licencia',
        dataIndex: 'dias',
        tooltip: 'Cantidad de días que duró la licencia.',
        flex: 1,
    }, {
        text: 'Importe',
        dataIndex: 'total',
        renderer: v => Ext.util.Format.usMoney(v),
        align: 'right',
        tooltip: 'Cantidad a pagar por los días de licencia.',
        flex: 1,
    }]
});