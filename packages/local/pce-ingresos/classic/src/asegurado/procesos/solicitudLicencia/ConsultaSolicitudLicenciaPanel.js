/**
 * @class Pce.ingresos.asegurado.procesos.solicitudLicencia.ConsultaSolicitudLicenciaPanel
 * @extends Ext.panel.Panel
 * @xtype consulta-solicitud-licencia
 * Pantalla de consulta de solicitudes de pago de licancias de asegurados
 * @author Jorge Escamilla
 * 
 * Modificaciones:
 * @extends Ext.container.Container (para agregar los demas cambios)
 * -Visualización de información del asegurado (Número de afiliación, nombre completo, RFC y dirección).
 * -Nombre en botones "Buscar" y "Generar reporte".
 * @author Alonso Rico
 */

Ext.define('Pce.ingresos.asegurado.procesos.solicitudLicencia.ConsultaSolicitudLicenciaPanel', {
    extend: 'Ext.container.Container',
    xtype: 'consulta-solicitud-licencia',

    requires: [
        'Pce.ingresos.asegurado.procesos.solicitudLicencia.ConsultaSolicitudLicenciaController',
        'Pce.ingresos.asegurado.procesos.solicitudLicencia.SolicitudLicenciaTabla',

        'Pce.afiliacion.asegurado.Asegurado',
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.afiliacion.asegurado.AseguradoDisplayer'
    ],

    controller: 'consulta-solicitud-licencia',

    title: 'Consulta de licencias',

    layout: 'fit',

    viewModel: {
        data: {
            asegurado: null,
            dependencia: null,
            afiliado: null    //new
        }
    },
 
    items: [{
        xtype: 'solicitud-licencia-tabla',
        reference: 'solicitudLicenciaTabla',
        title: 'Consulta de licencia (Servicio médico)',
        dockedItems: [{
            xtype: 'toolbar',
            //docked: 'top',
            items: [{
                xtype: 'asegurado-selector',
                reference: 'aseguradoSelector', //new
                labelAlign: 'left',             //new
                style: 'margin-left: 10px;',    //new
                bind: {
                    value: '{asegurado}'
                },
                listeners: {                    //new
                    change: 'onCambioAfiliado'  //new  
                },                              //new
                
                flex: 4
            }, {
                xtype: 'dependencia-selector',
                bind: {
                    value: '{dependencia}'
                },
                flex: 4
            }, {
                text: 'Buscar',
                handler: 'onBuscarTap',
                flex: 1
            }, {
                text: 'Generar reporte',
                handler: 'onGenerarReporteTap',
                bind: {
                    disabled: '{!asegurado}'
                },
                flex: 1
            }],
        }, {  //new ----------------------------------------
            xtype: 'toolbar',
            hidden: true,
            bind: {
                hidden: '{!afiliado}'
            },
            items: [{
                xtype: 'asegurado-displayer'
            }]
        },]
    }]


});