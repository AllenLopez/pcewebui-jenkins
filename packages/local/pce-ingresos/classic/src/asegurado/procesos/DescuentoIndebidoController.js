/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.DescuentoIndebidoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.descuento-indebido',

    onAseguradoChange(selector, numAfil) {
        this.lookup('estadoDeCuentaSelector').getStore()
            .porNumeroAfiliacion(numAfil);
        this.lookup('dependenciaSelector').getStore()
            .load({
                params: {
                    numeroAfiliacion: numAfil
                }
        });
    },
    onProcesarTap() {
        let forma = this.lookup('forma');
        if (!forma.isValid()) {
            return;
        }
        this.confirmarProceso().then(ans => {
            if (ans === 'yes') {
                this.procesar(forma.getForm().getFieldValues());
            }
        });
    },
    onProcesarSuccess(response) {
        let data = JSON.parse(response.responseText);
        let transaccion = data.transaccionId;
        Ext.Msg.alert(
            'Operación completada',
            `El movimiento se ha procesado exitosamente bajo la transacción <b>${transaccion}</b>`
        );
    },
    onProcesarFailure() {
        Ext.Msg.alert(
            'Error',
            `La operación no pudo ser completada`
        );
    },
    procesar(data) {
        Ext.Ajax.request({
            url: '/api/ingresos/asegurado/movimiento/descuento-indebido',
            params: data,
            scope: this,
            success: this.onProcesarSuccess,
            failure: this.onProcesarFailure
        });
    },
    confirmarProceso() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                'Se procesará el movimiento y se actualizará el estado de cuenta correspondiente. ¿Deseas proceder?',
                ans => resolve(ans)
            );
        });
    }
});