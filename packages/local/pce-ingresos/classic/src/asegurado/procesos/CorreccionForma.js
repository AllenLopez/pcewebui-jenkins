/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.CorreccionForma', {
    extend: 'Ext.form.Panel',
    xtype: 'asegurado-correccion-forma',

    title: 'Movimiento de corrección al número de afiliación',
    layout: 'vbox',
    items: [{
        xtype: 'hiddenfield',
        name: 'tipoMovimiento',
        value: 'COR'
    }, {
        xtype: 'asegurado-selector',
        name: 'afiliado',
        labelWidth: 160,
        width: '50%',
        displayTpl: [
            '<tpl for=".">',
            '{id}',
            '</tpl>'
        ],
        allowBlank: false,
        bind: {
            selection: '{afiliado}'
        },
        listeners:{
            change(comp,numAfil){
                if(!numAfil){
                    return;
                }
                let numDerechohabiente = comp.lastSelection[0].getData().numDerechohabiente;
                Ext.Ajax.request({
                    url: `/api/afiliacion/asegurado/empleos/${numDerechohabiente}`,
                    method: 'GET',
                    callback(op, success, response){
                        let storeDep = Ext.getCmp('dependencia').getStore();
                        let dependencias = JSON.parse(response.responseText);
                        let selectedItems = [];
                        dependencias.forEach(dep => {
                            let found = selectedItems.find(x=> x === dep.idDependencia);
                            if(!found){
                                selectedItems.push(dep.idDependencia);
                            }
                        });
                        storeDep.clearFilter();
                        storeDep.filterBy(record => Ext.Array.indexOf(selectedItems, record.get('id')) !== -1, this);

                    }
                });
            }
        }
    }, {
        xtype: 'toolbar',
        hidden: true,
        bind: {
            hidden: '{!afiliado}'
        },
        items: [{
            xtype: 'asegurado-displayer'
        }]
    },
    {
        xtype: 'form',
        layout: {
            type: 'table',
            columns: 2
        },
        defaults: {
            width: '100%'
        },
        width: '100%',
        items: [
            {
                xtype: 'textfield',
                name: 'numeroAfiliacionOrigen',
                fieldLabel: 'Número de afiliación incorrecto',
                labelWidth: 160,
                readOnly: true,
                cls: 'read-only',
                bind: {
                    value: '{afiliado.numeroAfiliacion}'
                }
            }, {
                xtype: 'asegurado-selector',
                name: 'numeroAfiliacionDestino',
                fieldLabel: 'Número de afiliación correcto',
                labelWidth: 160,
                colspan: 1,
                displayTpl: [
                    '<tpl for=".">',
                    '{id}',
                    '</tpl>'
                ],
                allowBlank: false
            },
        ]
    }, {
        xtype: 'dependencia-selector',
        name: 'dependencia',
        id: 'dependencia',
        labelWidth: 160,
        width: '50%',
        allowBlank: false
    }, {
        xtype: 'concepto-selector',
        name: 'concepto',
        fieldLabel: 'Concepto',
        labelWidth: 160,
        width: '50%',
        allowBlank: false
    }, {
        xtype: 'numberfield',
        name: 'importe',
        fieldLabel: 'Importe',
        tooltip:'Importe del concepto',
        labelWidth: 160,
        width: '30%',
        allowBlank: false
    }]
});