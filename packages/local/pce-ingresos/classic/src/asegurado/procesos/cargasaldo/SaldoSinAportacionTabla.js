Ext.define('Pce.ingresos.asegurado.procesos.cargasaldo.SaldoSinAportacionTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'saldo-sin-aportacion-tabla',

    requires: [
        'Pce.ingresos.asegurado.sinaportacion.AseguradoSinaportacionStore'
    ],

    store: {
        type: 'asegurado-sin-aportacion'
    },

    emptyText: 'No hay datos para mostrar',

    columns: [{
        text: 'Núm. afil',
        dataIndex: 'aseguradoNumeroAfiliacion',
        flex: 1
    }, {
        text: 'Nombre',
        dataIndex: 'aseguradoNombreCompleto',
        flex: 3
    }, {
        text: 'Regimen',
        dataIndex: 'coRegimenClaveAfiliac',
        flex: 0.7
    }, {
        text: 'Fecha Ultima Aportación',
        dataIndex: 'fechaUltimaAportacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 2
    }, {
        text: 'Clave Dep.',
        dataIndex: 'dependenciaNumeroDependencia',
        flex: 1
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        flex: 3
    }, {
        text: 'Saldo actual',
        dataIndex: 'saldo',
        renderer: v => Ext.util.Format.usMoney(v),
        flex: 1
    }],

    setRecord(fecha, regimen, transaccion) {
        this.mesAportacion = fecha;
        this.regimen = regimen;
        this.transaccionId = transaccion;

        this.store.load({
            params: {
                mesAportacion: fecha,
                regimen: regimen,
                transaccionId: transaccion
            }
        });
    },

    afterRender() {
        this.callParent();
        if(this.mesAportacion && this.regimen && this.transaccionId) {
            this.setRecord(this.mesAportacion, this.regimen, this.transaccionId);
        }
        if (this.mesAportacion && this.regimen) {
            this.setRecord(this.mesAportacion, this.regimen, 0);
        }
    }
});