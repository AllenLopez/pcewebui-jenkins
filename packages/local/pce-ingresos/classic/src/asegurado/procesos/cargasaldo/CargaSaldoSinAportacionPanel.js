/**
 * @class Pce.ingresos.asegurado.procesos.cargasaldo.CargaSaldoAseguradoCanceladoPanel
 * @extends Ext.panel.Panel
 * @xtype carga-saldo-jubilado-panel
 * Pantalla de búsqueda de asegurados cancelados sin aportacion mayor a 5 años
 * @author Alan López
 * */

Ext.define('Pce.ingresos.asegurado.procesos.cargasaldo.CargaSaldoSinAportacionPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'carga-saldo-sin-aportacion-panel',

    requires: [
        'Ext.form.field.ComboBox',
        'Pce.ingresos.asegurado.procesos.cargasaldo.CargaSaldoSinAportacionController',
        'Pce.ingresos.asegurado.procesos.cargasaldo.SaldoSinAportacionTabla'
    
    ],

    controller: 'carga-saldo-sin-aportacion',
    
    layout: {
        type: 'fit',
    },

    viewModel: {
        data: {
            // mesJubilacion: null,
            // ejercicioJubilacion: null,
            regimen: null,
            fechaMovimiento: null,
            aseguradosCount: 0,
        }, 
        stores: {
            asegurados: {
                type: 'asegurado-sin-aportacion',
                proxy: {
                    type: 'rest',
                    url: '/api/ingresos/ajustesaldo/carga-saldo-sinaportacion',
                    reader: {
                        type: 'json',
                    }
                },
                listeners: {
                    load: 'onStoreUpdate',
                    update: 'onStoreUpdate',
                    datachanged: 'onStoreUpdate'
                }
            }
        }
    },

    title: 'Carga de saldo de asegurados sin aportacion mayor a 5 años',

    items: [{
        xtype: 'saldo-sin-aportacion-tabla',
        reference: 'saldoSinAportacionTabla',
        layout: 'fit',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            reference: 'formulario',
            items: [{
                xtype: 'datefield',
                reference: 'fechaMovimiento',
                name: 'fechaMovimiento',
                fieldLabel: 'Fecha',
                forceSelection: true,
                format: 'd/m/y',
                bind: {
                    value: '{fechaMovimiento}'
                }
                // xtype: 'combobox',
                // name: 'mesJubilacion',
                // fieldLabel: 'Mes',
                // displayField: 'nombre',
                // valueField: 'mes',
                // store: {
                //     type: 'meses-ejercicio'
                // },
                // forceSelection: true,
                // listeners: {
                //     change: 'onSeleccionChange'
                // },
                // queryMode: 'local',
                // bind: {
                //     value: '{mesJubilacion}'
                // }
                },
                // {
                //     xtype: 'combobox',
                //     reference: 'ejerciciosSelector',
                //     name: 'mesJubilacion',
                //     fieldLabel: 'Ejercicio',
                //     displayField: 'ejercicio',
                //     valueField: 'ejercicio',
                //     store: {
                //         type: 'ejercicio',
                //     },
                //     forceSelection: true,
                //     listeners: {
                //         change: 'onSeleccionChange'
                //     },
                //     queryMode: 'local',
                //     bind: {
                //         value: '{ejercicioJubilacion}'
                //     }
                //},
                {
                    xtype: 'combobox',
                    name: 'regimen',
                    reference: 'regimenSelector',
                    displayField: 'desc',
                    valueField: 'value',
                    fieldLabel: 'Regimen',
                    queryMode: 'local',
                    listeners: {
                        change: 'onSeleccionChange'
                    },
                    store: {
                        fields: ['desc'],
                        data: [
                            {desc: 'LEY ANTERIOR', value: 'LA'},
                            {desc: 'TRANSICIÓN', value: 'TR'},
                        ]
                    },
                    bind: {
                        value: '{regimen}'
                    }
                }]
         },{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Asegurados saldo mayor a 5 años',
                handler: 'onConsultarSaldoAseguradoTap',
                bind: {
                    disabled: '{!regimen}',
                }
            }, {
                text: 'Cargar saldo',
                reference: 'cargaSaldoTap',
                handler: 'onCargarSaldoTap',
                bind: {
                    disabled: '{aseguradosCount < 1}'
                }
            },{
                text: 'Reporte de carga saldo',
                handler: 'onMostrarReporteTap',
                bind: {
                    disabled: '{aseguradosCount < 1}'
                }
            },
            {
                xtype: 'displayfield',
                fieldLabel: 'Total asegurados',
                bind: {
                    value: '{aseguradosCount}'
                }
            }]
        }],
        bind: {
            store: '{asegurados}'
        }
    }]

});