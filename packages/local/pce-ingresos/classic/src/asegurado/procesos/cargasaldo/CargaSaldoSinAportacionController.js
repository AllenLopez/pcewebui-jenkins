/**
 * @class Pce.ingresos.jubilado.procesos.CargaSaldoJubiladoController
 * @extend Ext.app.ViewController
 * @alias cancelacion-saldo-Asegurado
 *
 * @author Alan López
 */

Ext.define('Pce.ingresos.asegurado.procesos.cargasaldo.CargaSaldoSinAportacionController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.carga-saldo-sin-aportacion',

    onConsultarSaldoAseguradoTap() {
        const { regimen } = this.getViewModel().getData();
        const { fechaMovimiento } =  this.getViewModel().getData();
        const store = this.getAseguradoSinaportacionStore();
        
        store.load({
            params: {
                fechaMovimiento: fechaMovimiento,
                regimen: regimen
            },
        });
    },

    onSeleccionChange() {
        const store = this.getAseguradoSinaportacionStore();
        if (store.count() > 0) {
            store.load({});
            this.getViewModel().set('aseguradosCount', 0);
        }
    },

    onCargarSaldoTap() {
        const me = this;
        const { aseguradosCount, regimen, fechaMovimiento } = me.getViewModel().getData();
        // const fecha = { fechaMovimiento } = me.getViewModel().getData();
        
        if (aseguradosCount < 1) {
            Ext.Msg.alert('No hay registros para procesar',
                `Debe haber al menos un elemento en el listado de asegurados sin aportacion 
                 para poder continuar. Realice una nueva búsqueda por fecha y régimen`);
            return;
        }

        me.confirmarCargaSaldo(aseguradosCount, regimen, fechaMovimiento)
            .then(resp => {
                if (resp === 'yes') {
                    Ext.getBody().mask('Procesando...');
                    me.cargaListadoSaldoAsegurados(fechaMovimiento, regimen);
                }
            });
    },
    cargaListadoSaldoAsegurados(fechaMovimiento, regimen) {
        const me = this;
        Ext.Ajax.request({
            method: 'POST',
            url: '/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/cargar-asegurados',
            jsonData: {
                FechaMovimiento: fechaMovimiento,
                Regimen: regimen
            },
            success(response) {
                me.cargaListadoSaldoAseguradoSuccess(
                    JSON.parse(response.responseText),
                    fechaMovimiento, regimen);
                    Ext.getBody().unmask();
            },
            failure(response) {
                me.cargaListadoSaldoAseguradoFail(JSON.parse(response.responseText));
                Ext.getBody().unmask();
            }
        });
    },
    
    confirmarCargaSaldo(asegurados, regimen, fechaMovimiento) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar carga',
                `A continuación se cargaran saldos de ${asegurados} asegurados 
                sin aportacion en mas de 5 años. 
                ¿Desea proceder?`,
                ans => resolve(ans));
        });
    },

    onMostrarReporteTap() {
        const vm = this.getViewModel().getData();
        let fecha = Ext.util.Format.date(vm.fechaMovimiento,'d/m/y')
        this.mostrarReporteAsegurado(fecha, vm.regimen);
    },

    cargaListadoSaldoAseguradoSuccess(response, fechaMovimiento, regimen) {
        if (response.success){
            Ext.Msg.alert(
                'Proceso terminado',
                `Carga de saldos completada con éxito`
            );
            let fecha = Ext.util.Format.date(fechaMovimiento,'d/m/y')
            this.mostrarReporteAsegurado(fecha, regimen);
            this.lookup('regimenSelector').reset();
            this.lookup('fechaMovimiento').reset();
        } else {
            this.mostrarMensajeAlerta(response.mensaje);
        }

        Ext.getBody().unmask();
    },

    cargaListadoSaldoAseguradoFail(response) {
        this.mostrarMensajeAlerta(response.mensaje);
        Ext.getBody().unmask();
    },

    afectacionSaldoAseguradoSuccess(response, transaccionId) {
        if (response.success){
            Ext.Msg.alert(
                'Proceso terminado',
                `La transacción ${transaccionId}, se completó exitosamente`
            );
        } else {
            Ext.Msg.alert(
                'Ocurrió un error',
                `Se generó un error al procesar: ${response.mensaje}`
            );
        }

        Ext.getBody().unmask();
    },

    mostrarReporteAsegurado(fecha, regimen) {
        window.open(`/api/ingresos/ajustesaldo/carga-saldo-sinaportacion/descargar-asegurados?fecha=${fecha}&regimen=${regimen}`);
    },

    mostrarMensajeAlerta(mensaje) {
        Ext.Msg.alert(
            'Alerta',
            `Se verificó la información al procesar: ${mensaje}`
        );
    },

    getTransaccion(id) {
        const me = this;

        Ext.Ajax.request({
            method: 'GET',
            url: `/api/ingresos/transaccion/${id}`,
            success(response) {
                const transaccion = Ext.decode(response.responseText);
                me.setViewModelDataProp('transaccionEtapa', me.decodeEtapa(transaccion.etapa));
            }
        });
    },

    getAseguradoSinaportacionStore() {
        return this.lookup('saldoSinAportacionTabla').getStore();
    },

    decodeEtapa(etapa) {
        switch (etapa) {
            case 'TR': return 'EN TRAMITE';
            case 'CT': return 'EN CONTROL';
            case 'CA': return 'CANCELADA';
            case 'AU': return 'AUTORIZADA';
            default: return 'Se define al cargar saldos';
        }
    },

    onStoreUpdate(store) {
        let records = store.getCount();
        if (records > 0) {
            this.setViewModelDataProp('aseguradosCount', records);
        }
    },
    //revisar si se utiliza o no
    onSeleccionChange() {
        const store = this.getAseguradoSinaportacionStore();
        if (store.count() > 0) {
            store.load({});
            this.getViewModel().set('aseguradosCount', 0);
        }
    },

    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },
});