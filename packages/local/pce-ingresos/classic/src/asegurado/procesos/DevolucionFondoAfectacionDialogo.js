/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.DevolucionFondoAfectacionDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'asegurado-devolucion-fondo-afectacion-dialogo',

    requires: [
        'Ext.layout.container.Form',
        'Pce.ingresos.asegurado.EstadoDeCuenta'
    ],

    title: 'Realizar afectación',
    solicitud: null,
    autoShow: true,
    modal: true,
    width: 600,
    viewModel: {
        data: {
            importe: null
        }
    },
    defaultListenerScope: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'form',
        bodyPadding: 10,
        layout: 'form',
        items: [{
            xtype: 'dependencia-selector',
            name: 'dependencia',
            bind: '{dependencia}',
            allowBlank: false,
            store: {
                type: 'dependencias',
                proxy: {type: 'ajax', url: '/api/afiliacion/dependencia/por-asegurado'}
            }
        }, {
            xtype: 'numberfield',
            name: 'importe',
            fieldLabel: 'Importe',
            allowBlank: false,
            bind: '{importe}',
            validator(v) {
                if (v <= 0) {
                    return 'El importe no puede ser cero';
                }

                return true;
            }
        }]
    }],

    buttons: [{
        text: 'Aceptar',
        handler: 'onAceptarTap'
    }, {
        text: 'Cancelar',
        handler: 'onCancelarTap'
    }],

    onAceptarTap() {
        let form = this.down('form');
        if (form.isValid()) {
            this.solicitud.set({
                importe: this.viewModel.get('importe'),
                dependencia: this.viewModel.get('dependencia')
            });
            this.close();
        }
    },
    onCancelarTap() {
        this.close();
    },

    afterRender() {
        this.callParent();
        this.down('dependencia-selector').getStore().load({
            params: {
                numeroAfiliacion: this.solicitud.get('asegurado')
            }
        });
        this.cargarEstadoDeCuenta(this.solicitud).then(edoCta => {
            this.down('[name="importe"]').setMaxValue(-edoCta.data.saldo);
        });
    },
    cargarEstadoDeCuenta(solicitud) {
        return Pce.ingresos.asegurado.EstadoDeCuenta.porAseguradoYConcepto(
            solicitud.data.asegurado,
            solicitud.data.concepto
        );
    }
});