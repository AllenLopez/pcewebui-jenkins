/**
 * @author Rubén Maguregui
 */
var tipos = Ext.create('Ext.data.Store', {
    fields: ['id', 'value'],
    data : [
        {"id":"CARGO", "value":"CARGO"},
        {"id":"ABONO", "value":"ABONO"},
    ]
});
Ext.define('Pce.ingresos.asegurado.procesos.CargoAbonoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'cargo-abono-forma',

    title: 'Cargo/Abono afiliado',
    layout: 'vbox',
    items: [{
        xtype: 'hiddenfield',
        name: 'tipoMovimiento',
        value: 'MACA'
    }, {
        xtype: 'asegurado-selector',
        name: 'numeroAfiliacion',
        width: '50%',
        allowBlank: false,
        bind: {
            selection: '{afiliado}'
        },
        listeners:{
            change: function(comp,numAfil){
                if(!numAfil){
                    return;
                }
                let numDerechohabiente = comp.lastSelection[0].getData().numDerechohabiente;
                Ext.Ajax.request({
                    url: '/api/afiliacion/asegurado/empleos/'+numDerechohabiente,
                    method: 'GET',
                    callback(op, success, response){
                        let storeDep = Ext.getCmp('dependencia').getStore();
                        let dependencias = JSON.parse(response.responseText);
                        let selectedItems = [];
                        dependencias.forEach(dep => {
                            let found = selectedItems.find(x=> x== dep.idDependencia);
                            if(!found){
                                selectedItems.push(dep.idDependencia);
                            }
                        });
                        storeDep.clearFilter();
                        storeDep.filterBy(function(record, id){
                            return Ext.Array.indexOf(selectedItems, record.get("id")) !== -1;
                        }, this);
                        
                    }   
                });
            }
        }
    }, {
        xtype: 'toolbar',
        hidden: true,
        bind: {
            hidden: '{!afiliado}'
        },
        items: [{
            xtype: 'asegurado-displayer'
        }]
    }, {
        xtype: 'dependencia-selector',
        name: 'dependencia',
        id: 'dependencia',
        width: '50%',
        allowBlank: false
    }, {
        xtype: 'concepto-selector',
        name: 'concepto',
        fieldLabel: 'Concepto',
        labelWidth: 100,
        width: '50%',
        allowBlank: false
    }, {
        xtype: 'combobox',
        name: 'tipo',
        fieldLabel: 'Tipo movimiento',
        labelWidth: 100,
        width: '50%',
        allowBlank: false,
        store: tipos,
        displayField: 'value',
        value: "CARGO",
        valueField: 'id',
    }, {
        xtype: 'datefield',
        name: 'fecha',
        fieldLabel: 'Fecha',
        reference: 'fecha',
        width: '20%',
        allowBlank: false
    }, {
        xtype: 'numberfield',
        name: 'importe',
        fieldLabel: 'Importe',
        labelWidth: 100,
        width: 280,
        allowBlank: false
    }]
});