/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.MovimientoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'asegurado-movimiento-panel',

    requires: [
        'Pce.ingresos.asegurado.procesos.MovimientoController',

        // Formas para cada tipo de movimiento
        'Pce.ingresos.asegurado.procesos.CorreccionForma',
        'Pce.ingresos.asegurado.procesos.TraspasoForma',
        'Pce.ingresos.asegurado.procesos.CargoAbonoForma',
    ],

    controller: 'asegurado-movimiento',
    url: '/api/ingresos/asegurado/movimiento/procesar',
    viewModel:{
        afiliado: null
    },
    header: false,
    defaults: {
        bodyPadding: 10
    },

    tipoMovimiento: null,

    // Mapa de tipos de movimiento al xtype de sus formas correspondientes
    formas: {
        correccion: 'asegurado-correccion-forma',
        traspaso: 'asegurado-traspaso-forma',
        cargoAbono: 'cargo-abono-forma'
    },

    fbar: [{
        text: 'Procesar movimiento',
        handler: 'onProcesarMovimientoTap',
        tooltip:'Procesar movimiento',
    }],

    afterRender() {
        this.callParent();
        if (this.tipoMovimiento) {
            if (!this.formas[this.tipoMovimiento]) {
                throw new Error(
                    `No existe una forma registrada para el tipo
                     de movimiento ${this.tipoMovimiento}`
                );
            }
            if(this.tipoMovimiento == 'cargoAbono'){
                this.url = '/api/ingresos/asegurado/movimiento/cargo-abono';
            }
            this.add({
                xtype: this.formas[this.tipoMovimiento],
                reference: 'movimientoForma',
                url: this.url
            });
        }
    }
});