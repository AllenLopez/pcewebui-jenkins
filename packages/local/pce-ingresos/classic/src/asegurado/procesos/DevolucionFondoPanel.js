/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.DevolucionFondoPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'devolucion-fondo-panel',

    requires: [
        // 'Pce.ingresos.asegurado.procesos.DevolucionFondoController',
        // 'Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudForma',
        // 'Pce.ingresos.asegurado.procesos.devolucionFondo.SolicitudDevolucionTabla'
    ],

    // controller: 'devolucion-fondo',
    viewModel: {
        data: {
            asegurado: null,
            solicitud: null
        },
        stores: {
            solicitudes: {
                type: 'fondo-solicitud-devolucion'
            }
        }
    },

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'fondo-solicitud-devolucion-forma',
        reference: 'forma'
    }, {
        xtype: 'fondo-solicitud-devolucion-tabla',
        title: 'Solicitudes previas',
        emptyText: 'No se encontraron solicitudes previas',
        flex: 1,
        tbar: [{
            text: 'Autorizar',
            handler: 'onAutorizarTap',
            disabled: true,
            tooltip:'Realizar autorización de solicitud',
            bind: {
                disabled: '{!solicitud || solicitud.etapa != "CAPTURA"}'
            }
        }, {
            text: 'Realizar afectación',
            handler: 'onRealizarAfectacionTap',
            disabled: true,
            tooltip:'Realizar afectación en devolución de fondo propio',
            bind: {
                disabled: '{!solicitud || solicitud.etapa != "AUTORIZADA"}'
            }
        }, {
            text: 'Cancelar',
            handler: 'onCancelarTap',
            disabled: true,
            tooltip:'Cancelar solicitud',
            bind: {
                disabled: '{!solicitud || solicitud.etapa == "CANCELADA" || solicitud.etapa == "COMPLETADA"}'
            }
        }],
        bind: {
            store: '{solicitudes}',
            disabled: '{!asegurado}',
            title: 'Solicitudes previas - {asegurado.nombreCompleto}',
            selection: '{solicitud}'
        }
    }]
});