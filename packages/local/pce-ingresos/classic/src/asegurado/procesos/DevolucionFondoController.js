Ext.define('Pce.ingresos.asegurado.procesos.DevolucionFondoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.devolucion-fondo',

    requires: [
        'Pce.ingresos.asegurado.procesos.DevolucionFondoAfectacionDialogo'
    ],

    onAseguradoChange(selector) {
        let asegurado = selector.getSelection();
        if (asegurado) {
            asegurado.getInfo().then(info => {
                this.getViewModel().set('aseguradoInfo', info);
            });
            this.cargarSolicitudesPrevias(asegurado);
        }
    },

    cargarSolicitudesPrevias(asegurado) {
        this.getViewModel().get('solicitudes').load({
            params: {numeroAfiliacion: asegurado.getId()}
        });
    },

    onGuardarNuevaSolicitudTap() {
        let forma = this.lookup('forma');
        let asegurado = this.getViewModel().get('asegurado');
        let datos = forma.getValues();

        if (!forma.isValid()) {
            return;
        }

        this.cargarEstadoDeCuentaAseguradoConcepto(datos).then(edoCta => {
            if (!edoCta) {
                Ext.Msg.alert(
                    'Error',
                    'No es posible crear una solicitud para el concepto seleccionado'
                );
            } else {
                this.confirmarCreacionDeSolicitud(asegurado).then(ans => {
                    if (ans === 'yes') {
                        this.crearSolicitud(datos);
                    }
                });
            }
        });
    },
    crearSolicitud(datos) {
        let solicitud = new Pce.ingresos.asegurado.fondo.SolicitudDevolucion({
            asegurado: datos.asegurado,
            concepto: datos.concepto
        });
        solicitud.save({
            success: this.onSolicitudSaveSuccess.bind(this),
            failure: this.onSolicitudSaveFailure.bind(this)
        });
    },

    onSolicitudSaveSuccess() {
        Ext.toast('La solicitud se ha guardado exitosamente');
        this.getViewModel().get('solicitudes').reload();
    },
    onSolicitudSaveFailure() {
        Ext.Msg.alert('Error', 'Ha ocurrido un error y la solicitud no pudo ser guardada');
    },

    confirmarCreacionDeSolicitud(asegurado) {
        let datos = asegurado.data;
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se creará una nueva solicitud de devolución de fondo 
                propio para el asegurado <b>${datos.id}</b> (${datos.nombreCompleto}).
                ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    onAutorizarTap() {
        let solicitud = this.getViewModel().get('solicitud');
        this.confirmarAutorizacion(solicitud).then(ans => {
            if (ans === 'yes') {
                solicitud.set('etapa', 'AUTORIZADA');
                solicitud.save({
                    success: this.onSolicitudAutorizarSuccess.bind(this),
                    failure: this.onSolicitudSaveFailure.bind(this)
                });
            }
        });
    },
    onRealizarAfectacionTap() {
        let solicitud = this.getViewModel().get('solicitud');
        Ext.create('Pce.ingresos.asegurado.procesos.DevolucionFondoAfectacionDialogo', {
            solicitud,
            listeners: {
                close: () => {
                    if (null !== solicitud.get('importe')) {
                        this.confirmarAfectacion(solicitud).then(ans => {
                            if (ans === 'yes') {
                                solicitud.set('etapa', 'COMPLETADA');
                                solicitud.save({
                                    success: this.onSolicitudAfectacionSuccess.bind(this),
                                    failure: this.onSolicitudAfectacionFailure.bind(this)
                                });
                            }
                        });
                    }
                }
            }
        });
    },
    onCancelarTap() {
        let solicitud = this.getViewModel().get('solicitud');
        this.confirmarCancelacion(solicitud).then(ans => {
            if (ans === 'yes') {
                solicitud.set('etapa', 'CANCELADA');
                solicitud.save();
            }
        });
    },

    confirmarAutorizacion(solicitud) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `La solicitud ${solicitud.data.folio} será autorizada. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },
    confirmarAfectacion(solicitud) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se realizará la afectación de cartera para la solicitud 
                ${solicitud.data.folio}. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    confirmarCancelacion(solicitud) {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `La solicitud ${solicitud.data.folio} será cancelada. ¿Deseas proceder?`,
                ans => resolve(ans)
            );
        });
    },

    onSolicitudAutorizarSuccess() {
        Ext.toast('La solicitud se ha autorizado');
    },

    onSolicitudAfectacionSuccess(solicitud) {
        Ext.Msg.alert(
            'Operación completada',
            `Se ha realizado la afectación correspondiente a la solicitud ${solicitud.data.folio} bajo la transacción
            <b>${solicitud.data.resultado.transaccionId}</b>.`
        );
    },
    onSolicitudAfectacionFailure(solicitud) {
        Ext.Msg.alert(
            'Error',
            `Error al intentar la afectación correspondiente a la solicitud ${solicitud.data.folio}`
        );
    },

    cargarEstadoDeCuentaAseguradoConcepto(datos) {
        return Pce.ingresos.asegurado.EstadoDeCuenta.porAseguradoYConcepto(
            datos.asegurado,
            datos.concepto
        );
    }
});