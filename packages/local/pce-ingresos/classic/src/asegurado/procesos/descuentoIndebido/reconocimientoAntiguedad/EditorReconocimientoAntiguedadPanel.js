Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.reconocimientoAntiguedad.EditorReconocimientoAntiguedadPanel',{
extend: 'Ext.panel.Panel',
xtype: 'editor-reconocimiento-antiguedad-panel',

requires: [
    'Pce.ingresos.asegurado.procesos.descuentoIndebido.reconocimientoAntiguedad.EditorReconocimientoAntiguedadController',
    'Pce.afiliacion.dependencia.DependenciaSelector',
    'Pce.afiliacion.dependencia.DependenciaAseguradoStore',
],
controller: 'editor-reconocimiento-antiguedad',

viewModel: {
    data: {
        solicitud: null,
        asegurado: null,
        aseguradoInfo: null,
        quincenaFin: null,
        quincenaInicio: null,
        importeDevolucion: 0,
        solicitudValida: true
    }
},

title: 'Editor de solicitud descuento indebido por reconocimiento antiguedad',
layout: {
    type: 'table',
    columns: 5
},

bodyPadding: 10,
defaults: {
    width: '100%'
},

items: [{
    xtype: 'textfield',
    
    fieldLabel: '<b>Asegurado</b>',
    readOnly: true,
    cls: 'read-only',
    bind: {
        value: '{solicitud.aseguradoNombreCompleto}'
    },
    colspan: 4 
}, { 
    xtype: 'textfield',
    fieldLabel: '<b>Núm. Afil</b>',
    readOnly: true,
    cls: 'read-only',
    bind: {
        value: '{solicitud.asegurado}',
    },
    colspan: 1
} ,{
    xtype: 'textfield',
    fieldLabel: '<b>RFC</b>',
    readOnly: true,
    cls: 'read-only',
    bind: {
        value: '{asegurado.rfc}'
    },
    colspan: 2
}, {
    xtype: 'textfield',
    fieldLabel: '<b>Concepto</b>',
    readOnly: true,
    cls: 'read-only',
    bind: {
        value: '{solicitud.conceptoDescripcion}'
    },
    colspan: 2
},{
    xtype: 'textfield',
    fieldLabel: '<b>Dirección</b>',
    readOnly: true,
    cls: 'read-only',
    bind: {
        value: '{asegurado.direccion}'
    },
    colspan: 2
}, {
    xtype: 'textfield',
    fieldLabel: '<b>Teléfono</b>',
    bind: {
        value: '{solicitud.telefono}'
    },
    colspan: 2
}, {
    xtype: 'textfield',
    fieldLabel: '<b>Jubilado</b>',
    readOnly: true,
    cls: 'read-only',
    bind: {
        value: '{aseguradoInfo.jubilado}'
    },
    colspan: 1
}, {
    xtype: 'displayfield',
    fieldLabel: '<b>Estatus</b>',
    readOnly: true,
    cls: 'read-only',
    bind: {
        value: '{solicitud.etapa}'
    },
    renderer(v) {
        switch (v) {
            case 'TR': return 'TRAMITE';
            case 'CA': return 'CANCELADO';
            case 'AU': return 'AUTORIZADO';
            case 'CP': return 'COMPLETADO';
            default: return 'OTRO';
        }
    },
    colspan: 2
}, {
    xtype: 'textfield',
    fieldLabel: '<b>Observaciones</b>',
    width: '100%',
    colspan: 6,
    bind: {
        value: '{solicitud.observaciones}'
    }
},{
    xtype: 'combobox',
    reference: 'dependenciaSelector',
    name: 'dependencia',
    valuefield: 'numeroDependencia',
    displayField: 'descripcion',
    fieldLabel: '<b>Dependencia</b>',
    width: '100%',
    queryMode: 'local',
    store: {
        type: 'dependencias',
        autoLoad: true,
        proxy: {
            type: 'rest',
            url: '/api/afiliacion/dependencia-asegurado/dependencias'
        }
    },
    bind: {
        value: '{solicitud.dependenciaDescripcion}'
    },
    listeners: {
        change: 'setNumDependencia'
    },
    colspan: 4
},{
    xtype: 'textfield',
    fieldLabel: '<b>Departamento</b>',
    bind: {
        value: '{solicitud.departamento}'
    },
    colspan: 2
}, {
    xtype: 'datefield',
    fieldLabel: '<b>Desde</b>',
    renderer(v) {
        return Ext.util.Format.date(v, 'd/m/Y h:s');
    },
    bind: {
        value: '{solicitud.quincenaInicio}'
    },
    colspan: 2
}, {
    xtype: 'datefield',
    fieldLabel: '<b>Hasta</b>',
    renderer(v) {
        return Ext.util.Format.date(v, 'd/m/Y h:s');
    },
    bind: {
        value: '{solicitud.quincenaFin}'
    },
    colspan: 2
}, {
    xtype: 'textfield',
    fieldLabel: '<b>Importe a devolver</b>',
    labelWidth: 130,
    allowBlank: false,
    renderer(v) {
        return `<b>${Ext.util.Format.usMoney(v)}</b>`;
    },
    bind: {
        value: '{solicitud.importeReconocimientoAntiguedad}'
    },
    colspan: 2
}],

buttons: [{
    text: 'Actualizar solicitud',
    reference: 'tapGuardar',
    tooltip: 'Permite editar los valores de la solicitud',
    handler: 'onActualizarSolicitud'  
},{
    text: 'Cancelar',
    handler: 'onCancelarEdicion'
}]

   })