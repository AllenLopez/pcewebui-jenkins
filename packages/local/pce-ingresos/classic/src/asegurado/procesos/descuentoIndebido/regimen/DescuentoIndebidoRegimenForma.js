/**
 * @class Pce.ingresos.asegurado.proceso.descuentoIndebido.regimen.DescuentoIndebidoRegimenForma
 * @extends Ext.form.Panel
 * @xtype descuento-indebido-regimen-forma
 *
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.proceso.descuentoIndebido.regimen.DescuentoIndebidoRegimenForma', {
    extend: 'Ext.form.Panel',
    xtype: 'descuento-indebido-regimen-forma',

    requires: [
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaAseguradoStore',
    ],

    layout: {
        type: 'table',
        columns: 6
    },

    bodyPadding: 10,
    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'asegurado-selector',
        fieldLabel: 'Asegurado',
        name: 'asegurado',
        forceSelection: true,
        listeners: {
            change: 'onAseguradoChange'
        },
        bind: {
            selection: '{asegurado}'
        },
        colspan: 4
    }, {
        xtype: 'textfield',
        name: 'numeroAfiliacion',
        fieldLabel: 'Núm. Afil',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.id}'
        },
        colspan: 1
    } ,{
        xtype: 'textfield',
        name: 'rfc',
        fieldLabel: 'RFC',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.rfc}'
        },
        colspan: 1
    }, {
        xtype: 'textfield',
        name: 'direccion',
        fieldLabel: 'Direccion',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.direccion}'
        },
        colspan: 3
    }, {
        xtype: 'textfield',
        name: 'telefono',
        fieldLabel: 'Telefono',
        bind: {
            value: '{asegurado.telefono}'
        },
        readOnly: true,
        cls: 'read-only',
        colspan: 2
    }, {
        xtype: 'textfield',
        name: 'jubilado',
        fieldLabel: 'Jubilado',
        bind: {
            value: '{aseguradoInfo.jubilado}'
        },
        cls: 'read-only',
        colspan: 1
    }, {
        xtype: 'textarea',
        name: 'observaciones',
        fieldLabel: 'Observaciones',
        allowBlank: true,
        bind: {
            disabled: '{!asegurado}'
        },
        grow: true,
        anchor: '100%',
        colspan: 6
    }, {
        xtype: 'dependencia-selector',
        reference: 'dependenciaSelector',
        name: 'dependencia',
        fieldLabel: 'Dependencia',
        valueField: 'id',
        forceSelection: true,
        store: {
            type: 'dependencia-asegurado',
            autoLoad: true,
            proxy: {
                type: 'rest',
                url: '/api/afiliacion/dependencia-asegurado/dependencias'
            }
        },
        bind: {
            disabled: '{!asegurado}'
        },
        colspan: 6
    }, {
        xtype: 'textfield',
        name: 'departamento',
        fieldLabel: 'Departamento',
        bind: {
            disabled: '{!asegurado}'
        },
        colspan: 2
    }, {
        xtype: 'datefield',
        name: 'quincenaInicio',
        fieldLabel: 'Desde',
        forceSelection: true,
        bind: {
            value: '{quincenaInicio}',
            maxValue: '{quincenaFin}',
            disabled: '{!asegurado}'
        },
        colspan: 2
    }, {
        xtype: 'datefield',
        name: 'quincenaFin',
        reference: 'fechaInicioSelector',
        fieldLabel: 'Hasta',
        forceSelection: true,
        bind: {
            minValue: '{quincenaInicio}',
            disabled: '{!quincenaInicio}',
            value: '{quincenaFin}',
        },
        colspan: 2
    }, {
        xtype: 'numberfield',
        name: 'importeRetencion',
        fieldLabel: '$ Devolución (retención)',
        readOnly: true,
        labelWidth: 160,
        cls: 'read-only',
        bind: {
            value: '{importeRetencion}'
        },
        colspan: 2
    }, {
        xtype: 'numberfield',
        name: 'importeAportacion',
        fieldLabel: '$ Aportacion',
        readOnly: true,
        labelWidth: 160,
        cls: 'read-only',
        bind: {
            value: '{importeAportacion}'
        },
        colspan: 2
    },
    ]


});