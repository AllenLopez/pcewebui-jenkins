/**
 * @class Pce.ingresos.asegurado.proceso.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedad
 * @extends Ext.panel.Panel
 * @xtype di-reconocimiento-antiguedad
 *
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.proceso.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedad', {
    extend: 'Ext.panel.Panel',
    xtype: 'di-reconocimiento-antiguedad',

    requires: [
        'Pce.ingresos.asegurado.prcesos.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedadController',
        'Pce.ingresos.asegurado.proceso.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedadForma'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    scrollable: 'y',

    title: 'Descuento indebido por reconocimiento de antigüedad',

    controller: 'di-reconocimiento-antiguedad-panel',

    viewModel: {
        data: {
            asegurado: null,
            aseguradoInfo: null,
            quincenaFin: null,
            quincenaInicio: null,
            importeDevolucion: 0
        }
    },

    items: [{
        xtype: 'di-reconocimiento-antiguedad-forma',
        reference: 'DIReconocimientoAntiguedadForma',
    }],

    buttons: [{
        text: 'Guardar solicitud',
        tooltip: 'Guarda la solicitud de descuento indebido',
        bind: {
            disabled: '{!quincenaFin || importeDevolucion < 1}'
        },
        handler: 'onProcesarTap'
    }]
});