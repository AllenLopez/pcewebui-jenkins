/**
 * @class Pce.ingresos.asegurado.proceso.descuentoIndebido.prestamo.DescuentoIndebidoPrestamoPanel
 * @extends Ext.panel.Panel
 * @xtype di-prestamo
 *
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.proceso.descuentoIndebido.prestamo.DescuentoIndebidoPrestamoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'di-prestamo',

    requires: [
        'Pce.ingresos.asegurado.prcesos.descuentoIndebido.prestamo.DescuentoIndebidoPrestamoController',
        'Pce.ingresos.asegurado.proceso.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedadForma'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    scrollable: 'y',

    title: 'Descuento indebido por prestamo',

    controller: 'di-prestamo-panel',

    viewModel: {
        data: {
            asegurado: null,
            aseguradoInfo: null,
            importeDevolucion: 0
        }
    },

    items: [{
        xtype: 'di-reconocimiento-antiguedad-forma',
        reference: 'DIReconocimientoAntiguedadForma',
        items: [{
            xtype: 'asegurado-selector',
            fieldLabel: 'Asegurado',
            name: 'asegurado',
            labelWidth: 130,
            listeners: {
                change: 'onAseguradoChange'
            },
            bind: {
                selection: '{asegurado}'
            },
            colspan: 4
        }, {
            xtype: 'textfield',
            name: 'numeroAfiliacion',
            fieldLabel: 'Núm. Afil',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.id}'
            },
            colspan: 1
        } ,{
            xtype: 'textfield',
            name: 'rfc',
            fieldLabel: 'RFC',
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.rfc}'
            },
            colspan: 1
        }, {
            xtype: 'textfield',
            name: 'direccion',
            fieldLabel: 'Dirección',
            labelWidth: 130,
            readOnly: true,
            cls: 'read-only',
            bind: {
                value: '{asegurado.direccion}'
            },
            colspan: 3
        }, {
            xtype: 'textfield',
            name: 'telefono',
            fieldLabel: 'Teléfono',
            bind: {
                value: '{asegurado.telefono}'
            },
            readOnly: true,
            cls: 'read-only',
            colspan: 2
        }, {
            xtype: 'textfield',
            name: 'jubilado',
            fieldLabel: 'Jubilado',
            bind: {
                value: '{aseguradoInfo.jubilado}'
            },
            cls: 'read-only',
            colspan: 1
        }, {
            xtype: 'textarea',
            name: 'observaciones',
            fieldLabel: 'Observaciones',
            labelWidth: 130,
            allowBlank: false,
            grow: true,
            anchor: '100%',
            colspan: 6
        }, {
            xtype: 'dependencia-selector',
            reference: 'dependenciaSelector',
            name: 'dependencia',
            fieldLabel: 'Dependencia',
            valueField: 'id',
            labelWidth: 130,
            forceSelection: true,
            store: {
                type: 'dependencia-asegurado',
                autoLoad: true,
                proxy: {
                    type: 'rest',
                    url: '/api/afiliacion/dependencia-asegurado/dependencias'
                }
            },
            colspan: 4
        }, {
            xtype: 'textfield',
            name: 'departamento',
            fieldLabel: 'Departamento',
            labelWidth: 130,
            allowBlank: false,
            colspan: 2
        }, {
            xtype: 'numberfield',
            name: 'importeReconocimientoAntiguedad',
            fieldLabel: 'Importe a devolver',
            labelWidth: 130,
            allowBlank: false,
            bind: {
                value: '{importeDevolucion}'
            },
            colspan: 2
        }]
    }],

    buttons: [{
        text: 'Guardar solicitud',
        tooltip: 'Guarda la solicitud de descuento indebido',
        bind: {
            disabled: '{importeDevolucion <= 0}'
        },
        handler: 'onProcesarTap'
    }]
});