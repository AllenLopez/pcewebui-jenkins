/**
 * @class Pce.ingresos.asegurado.proceso.descuentoIndebido.DescuentoIndebidoRegimenPanel
 * @extends Ext.panel.Panel
 * @xtype di-reconocimiento-antiguedadñ
 *
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.proceso.descuentoIndebido.regimen.DescuentoIndebidoRegimenPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'di-regimen',

    requires: [
        'Pce.ingresos.asegurado.proceso.descuentoIndebido.regimen.DescuentoIndebidoRegimenForma',
        'Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetalleTabla',

        'Pce.ingresos.asegurado.prcesos.reconocimientoAntiguedad.regimen.DescuentoIndebidoRegimenController'
    ],

    controller: 'di-regimen-panel',

    layout: {
       type: 'vbox',
       align: 'stretch'
    },

    scrollable: 'y',

    title: 'Descuento indebido por régimen incorrecto',

    viewModel: {
        data: {
            asegurado: null,
            aseguradoInfo: null,
            quincenaInicio: null,
            quincenaFin: null,
            importeRetencion: null,
            importeAportacion: null,
            detalles: []
        }
    },

    items: [{
        xtype: 'descuento-indebido-regimen-forma',
        reference: 'DescuentoIndebidoRegmenForma'
    }, {
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                text: 'Calcular importes',
                tooltip: 'Agrega dependencia y detalles de importes y fechas',
                bind: {
                    disabled: '{!quincenaFin}'
                },
                handler: 'onCalcularImportes'
            }]
        }]
    }, {
        xtype: 'descuento-indebido-detalle-tabla',
        reference: 'DescuentoIndebidoDetalleTabla',
        bind: {
            disabled: '{!asegurado}'
        }
    }],

    buttons: [{
        text: 'Guardar solicitud',
        reference: 'guardarTap',
        tooltip: 'Guarda la solicitud de descuento indebido',
        bind: {
            disabled: '{importeRetencion <= 0}'
        },
        handler: 'onProcesarTap'
    }]
});