/**
 * @class Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetalleTabla
 * @extends Ext.grid.Panel
 * @xtype descuento-indebido-detalle-tabla
 * Enlista los registros de detalle por descuento indebido
 *
 * @author Jorge Escamilla
 */
Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetalleTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'descuento-indebido-detalle-tabla',

    requires: [
        'Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoDetalleStore',

        'Ext.grid.plugin.CellEditing'
    ],

    store: {
        type: 'descuento-indebido-detalle',
        proxy: {
            type: 'rest',
            api: {
                create: '/api/ingresos/asegurado/descuento-indebido-detalle/detalle-regimen-incorrecto',
                read: '/api/ingresos/asegurado/descuento-indebido-detalle/calculo-regimen-incorrecto',
                update: undefined,
                destroy: undefined
            }
        }
    },

    emptyText: 'No hay datos para mostrar',

    selModel: {
        selType: 'checkboxmodel'
    },

    plugins: [{
        ptype: 'cellediting',
        reference: 'captura-editor',
        clicksToEdit: 1
    }],

    listeners: {
        selectionChange: 'onSelectionChange',
        deselect: 'onDeselectRecord',
        select: 'onSelectRecord',
        edit: 'onEditImporte'
    },

    columns: [{
        text: 'Regimen asegurado',
        dataIndex: 'regimenPce',
        flex: 1
    }, {
        text: 'Regimen incorrecto',
        dataIndex: 'regimenIncorrecto',
        flex: 1
    }, {
        text: 'Descripcion movimiento',
        dataIndex: 'descripcionMovimiento',
        flex: 2
    }, {
        text: 'Quincena',
        dataIndex: 'fechaMovimiento',
        renderer(v) {
            return Ext.util.Format.date(v, 'd/m/Y');
        },
        flex: 1
    }, {
        text: '$ Retención',
        dataIndex: 'importeRetencion',
        renderer: v => Ext.util.Format.usMoney(v),
        editor: {
            xtype: 'numberfield',
            hideTrigge: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false
        },
        flex: 1
    }, {
        text: '$ Aportacion',
        dataIndex: 'importeAportacion',
        renderer: v => Ext.util.Format.usMoney(v),
        editor: {
            xtype: 'numberfield',
            hideTrigge: true,
            mouseWheelEnabled: false,
            keyNavEnabled: false
        },
        flex: 1
    }]
});

