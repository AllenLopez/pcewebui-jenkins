/**
 * @class Pce.ingresos.asegurado.prcesos.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedadController
 * @extends Ext.app.ViewController
 * @alias controller.di-reconocimiento-antiguedad-panel
 * Controlador de pantalla de descuento indebido por reconocimiento de antiguedad
 * que contiene funciones, validaciones y métodos para afectar cartera del asegurado
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.prcesos.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedadController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.di-reconocimiento-antiguedad-panel',

    solicitud: null,
    asegurado: null,
    // init: function(){
    //     Ext.getBody().mask('Afectando cartera...');
    // },

    onProcesarTap() {
        let me = this;
        let form = this.obtenerFormulario();

        if(!form.isValid()) {
            return;
        }

        me.confirmarProceso().then(ans => {
            if(ans === 'yes') {
                Ext.getBody().mask('procesando solicitud...');

                me.guardarSolicitud(form.getFieldValues())
                    .then(solicitud => {
                        let solicitudId = solicitud.getId();
                        let afiliado = solicitud.data.asegurado;
                        let etapa = solicitud.data.etapa;

                            me.guardarChequeSolicitud(solicitud)
                                .then(cheque => {
                                    console.log(cheque);
                                        Ext.Msg.alert('Proceso completado',
                                            `La solicitud de descuento indebido se guardo correctamente con el folio: <b>${solicitudId}</b>`);

                                        form.reset();
                                        Ext.getBody().unmask();
                                        me.imprimirSolicitudDescuentoIndebido('DIRA', solicitudId, afiliado, etapa);
                                    },
                                    error => {
                                        Ext.Msg.alert('Error en proceso',
                                            `Ha ocurrido un error con la generación del cheque y no pudo ser guardado. Únicamente la solicitud con el folio: ${error.errorMessage} se guardo correctamente`);
                                        form.reset();
                                        Ext.getBody().unmask();
                                    });
                        },
                        error => {
                            Ext.Msg.alert('Error',
                                `Ha ocurrido un error y la solicitud no pudo ser guardada: ${error.errorMessage}`);
                            Ext.getBody().unmask();
                        });
            }
        });
    },

    guardarSolicitud(datos) {
        return new Promise((resolve, reject) => {
            let descuento = new Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebido(datos);
            descuento.set('claveRegimen', this.getViewModelDataProp('aseguradoInfo').get('claveRegimen'));

            descuento.save({
                success(record) {
                    resolve(record);
                },
                failure(record, batch) {
                    let error = batch.getError();
                    let msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                    reject(msg);
                }
            });
        });
    },

    guardarChequeSolicitud(solicitud) {
        let cheque = new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: this.asegurado.get('nombreCompleto'),
            rfc: this.asegurado.get('rfc'),
            importe: solicitud.get('importeReconocimientoAntiguedad'),
            solicitudDevolucionId: 0,
            descuentoIndebidoDevId: solicitud.getId()
        });

        return new Promise((resolve, reject) => {
            cheque.save({
                success(record) {
                    resolve(record);
                },
                failure(record, batch) {
                    let error = batch.getError();
                    let msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                    reject(msg);
                }
            });
        });
    },

    onAseguradoChange(selector) {
        let asegurado = selector.getSelection();
        if(asegurado) {
            this.asegurado = asegurado;
            this.cargaAseguradoInfo(asegurado);

            let store = this.lookup('dependenciaSelector').getStore();
            store.load({
                params: {
                    numeroAfiliacion: asegurado.getId()
                }
            });
        }
    },

    confirmarProceso() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se guardará la solicitud de descuento indebido por reconocimiento de antigüedad. ¿Deseas proceder?`,
                ans => resolve(ans));
        });
    },

    obtenerFormulario() {
        return this.getView().down('form').getForm();
    },

    cargaAseguradoInfo(asegurado) {
        asegurado.getInfo().then(info => {
            this.setViewModelDataProp('aseguradoInfo', info);
            if(info.get('jubilado') === 'SI') {
                this.invalidaSolicitud('Los asegurados jubilados no pueden solicitar la devolución por descuento indebido');
            }
        });
    },

    imprimirSolicitudDescuentoIndebido(clave, solicitudId, afiliado, etapa) {
        window.open(`/api/ingresos/asegurado/descuento-indebido/descargar?claveMov=${clave}&solicitudId=${solicitudId}&numafil=${afiliado}&etapa=${etapa}`);
    },

    invalidaSolicitud(msg) {
        this.mostrarMensajeAlerta('Solicitud inválida', msg);
        this.setViewModelDataProp('solicitudValida', false);
        this.setViewModelDataProp('importeDevolucion', 0);
    },

    mostrarMensajeAlerta(titulo, mensaje) {
        Ext.Msg.alert(titulo, mensaje);
    },

    getViewModelDataProp(prop) {
        let vm = this.obtieneViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },

    obtieneViewModel() {
        let vm = this.getViewModel();
        if(vm) {
            return vm;
        }
    },

    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },
});