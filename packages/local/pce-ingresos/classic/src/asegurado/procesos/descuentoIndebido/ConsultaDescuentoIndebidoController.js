/**
 * @class Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoController
 * @extends Ext.app.ViewController
 * @alias consulta-descuento-indebido-panel
 * Controlador para panel de consulta de desceunto indebido en el que hace uso de los valores del formulario para filtrar los resultados de busqueda
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.consulta-descuento-indebido-panel',

    chequeSolicitud: null,

    onBuscar() {
        let filtros = this.obtieneFiltros();
        let store = this.lookup('DescuentoIndebidoTabla').getStore();
        store.load({
            params: filtros
        });
    },

    onGenerarReporte() {
        let filtros = this.obtieneFiltros();

        if(!this.validaFiltros(filtros)) {
            Ext.Msg.alert('Alerta', 'Debe definir al menos un parametro de búsqueda');
            return;
        }

        this.imprimirReporteConsultaMovimientosDep(filtros);
    },

    onAutorizarTap() {
        let record = this.obtieneSolicitud().getData();
        this.confirmarActualizacion(record, 'autorizada').then(ans => {
            if(ans === 'yes') {
                Ext.getBody().mask('autorizando...');
                Ext.Ajax.request({
                    method: 'PUT',
                    url: '/api/ingresos/asegurado/descuento-indebido/autorizar',
                    params: record,
                    scope: this,
                    success: this.autorizarSuccess,
                    failure: this.autorizarFailure
                });
            }
        });
    },

    onMostrarDetallesDescuento(tabla, i, j, item, e, record) {
        this.mostrarDialogoDetalleDescuento(record);
    },

    autorizarSuccess() {
        let solicitud = this.obtieneSolicitud();
        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/cajas/chequera/cheque-solicitud/autorizar',
            params: { descuentoId: solicitud.getId() },
            scope: this,
            success: this.autorizarChequeSuccess,
            failure: this.autorizarChequeFailure
        });
    },

    autorizarFailure() {
        Ext.Msg.alert('Error',
            'Ocurrio un error al intentar autorizar la solicitud');
        Ext.getBody().unmask();
    },

    autorizarChequeSuccess() {
        Ext.Msg.alert(
            'Solicitud autorizada',
            `Se ha realizado la autorización con éxito`
        );

        this.gridRefresh();

        Ext.getBody().unmask();
    },

    autorizarChequeFailure() {
        Ext.Msg.alert('Solicitud autorizada con error',
            `La solicitud se ha autorizado.
            Sin embargo, ocurrió un error al intentar autorizar el cheque de la solicitud`);
        Ext.getBody().unmask();
    },

    onCancelarTap() {
        let record = this.obtieneSolicitud().getData();
        this.confirmarActualizacion(record, 'cancelada').then(ans => {
            if(ans === 'yes') {
                Ext.getBody().mask('cancelando...');
                Ext.Ajax.request({
                    method: 'PUT',
                    url: '/api/ingresos/asegurado/descuento-indebido/cancelar',
                    params: record,
                    scope: this,
                    success: this.cancelarSuccess,
                    failure: this.cancelarFailure
                });
            }
        });
    },

    cancelarSuccess() {
        let solicitud = this.obtieneSolicitud();
        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/cajas/chequera/cheque-solicitud/cancelar',
            params: { descuentoId: solicitud.getId() },
            scope: this,
            success: this.cancelarChequeSuccess,
            failure: this.cancelarChequeFailure
        });
    },

    cancelarFailure() {
        Ext.Msg.alert('Error',
            'Ocurrio un error al intentar cancelar la solicitud');
        Ext.getBody().unmask();
    },

    cancelarChequeSuccess() {
        Ext.Msg.alert(
            'Solicitud cancelada',
            `Se ha realizado la cancelación con éxito`
        );

        this.gridRefresh();

        Ext.getBody().unmask();
    },

    cancelarChequeFailure() {
        Ext.Msg.alert('Solicitud cancelada con error',
            `La solicitud se ha cancelado.
            Sin embargo, ocurrió un error al intentar cancelar el cheque de la solicitud`);
        Ext.getBody().unmask();
    },

    onRealizarAfectacionTap() {
        let me = this;
        let record = me.obtieneSolicitud().getData();
        me.confirmarAfectacion(record).then(ans => {
            if(ans === 'yes') {
                Ext.getBody().mask('procesando...');

                Ext.Ajax.request({
                    method: 'PUT',
                    url: '/api/ingresos/asegurado/descuento-indebido/afecta-cartera',
                    params: record,
                    success(resp) {
                        let respuestaSolicitud = JSON.parse(resp.responseText);

                        if(respuestaSolicitud.exito) {
                            let solicitud = me.obtieneSolicitud();

                            Ext.Ajax.request({
                                method: 'PUT',
                                url: '/api/cajas/chequera/cheque-solicitud/afectar',
                                params: { descuentoId: solicitud.getId() },
                                success(respCheque) {
                                    let respuestaCheque = JSON.parse(respCheque.responseText);

                                    Ext.Msg.alert(
                                        'Operación completada',
                                        `Se ha realizado la afectación de cartera exitosamente bajo la transacción: <b>${respuestaSolicitud.transaccionId}</b>.
                                        Se generó cheque con el folio: <b>${respuestaCheque.id}</b>`);

                                    me.gridRefresh();
                                },
                                failure() {
                                    Ext.Msg.alert('Error',
                                        'Ocurrio un error al intentar completar emisión del cheque de la solicitud');
                                }
                            });

                        } else {
                            Ext.Msg.alert('Error',
                                `Ocurrio un error al intentar realizar la afectación: ${respuestaSolicitud.mensaje}`);
                        }

                        Ext.getBody().unmask();
                    },
                    failure(resp) {
                        let error = JSON.parse(resp.responseText);
                        Ext.Msg.alert('Error',
                            `Ocurrio un error al intentar realizar la devolución: ${error.errorMessage}`);

                        Ext.getBody().unmask();
                    }
                });
            }
        });
    },

    confirmarActualizacion(record, etapa) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `La solicitud de descuento indebido con folio <b>${record.id}</b>, será <b>${etapa}</b>.
                ¿Deseas proceder?`,
                ans => resolve(ans));
        });
    },

    confirmarAfectacion(record) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `Se realizará la <b>afectación de cartera</b> para la solicitud con folio: <b>${record.id}</b>.
                ¿Deseas proceder?`,
                ans => resolve(ans));
        });
    },

    mostrarDialogoDetalleDescuento(record) {
        let vm = Ext.create('Ext.app.ViewModel', {
            data: { descuento: record.getData() }
        });

        Ext.create('Ext.window.Window',{
            title: `Detalles de solicitud de ${record.get('tipoMovimientoDescripcion')}`,
            autoShow: true,
            modal: true,
            width: 1200,
            layout: 'fit',
            items: {
                xtype: 'descuento-indebido-detalle',
                viewModel: vm,
            },
        });
    },

    imprimirReporteConsultaMovimientosDep(filtros) {
        let {asegurado, etapa, fechaInicio, fechaFin, folio, movimiento} = filtros;
        let dateformat = 'd/m/Y';
        let fechaInicial = Ext.util.Format.date(fechaInicio, dateformat);
        let fechaFinal = Ext.util.Format.date(fechaFin, dateformat);

        window.open(`/api/ingresos/asegurado/descuento-indebido/descargar-consulta-movimientos?pnum_afil=${asegurado}&pidsolicitud=${folio}&pestatus=${etapa}&pfechainicial=${fechaInicial}&pfechafinal=${fechaFinal}&=${folio}&pcatmovimiento=${movimiento}`);
    },
    //Imprime la solicitud
    onImprimirSolicitud(tabla, i, j, item, e, record) {
        this.imprimirSolicitudDescuentoIndebido(record.get("tipoMovimientoDescripcion"),record.get('asegurado'), record.getId(),record.get("etapa"));
    },

    imprimirSolicitudDescuentoIndebido(movimiento,asegurado, folio,etapa) {
        window.open(`/api/ingresos/asegurado/descuento-indebido/descargar?claveMov=${movimiento}&numafil=${asegurado}&solicitudId=${folio}&etapa=${etapa}`);
    },

   
    validaFiltros({asegurado, etapa, fechaInicio, fechaFin, folio, movimiento, solicitud}) {
        let continua = false;
        if (asegurado) { continua = true ;}
        if (etapa)  { continua = true ;}
        if (fechaInicio)  { continua = true ;}
        if (fechaFin)  { continua = true ;}
        if (folio) { continua = true ;}
        if (movimiento > 0) { continua = true ; }
        if (solicitud > 0) { continua = true ; }

        return continua;
    },

    obtieneFiltros() {
        return this.getViewModel().getData();
    },

    obtieneSolicitud() {
        return this.getViewModel().get('solicitud');
    },

    gridRefresh() {
        this.lookup('DescuentoIndebidoTabla').getStore().reload();
    },
    
    onEditarSolicitud(tabla, i, j, item, e, record) {
        if (record) {
            if(record.data.tipoMovimiento == 233){
                this.redirectTo(`#edicion-solicitud-descuento-indebido-ra/${record.getId()}`);
            }else{
               return; //se queda pendiente esta linea para la pantalla edicion descuento indebido por prestamo
            }
        }
    },
    crearVMSolicitud(record) {
        return Ext.create('Ext.app.ViewModel', {
            data: {
                solicitud: record.getData()
            }
        });
    },
    getViewModelDataProp(prop) {
        let vm = this.getLocalViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },

    getLocalViewModel() {
        let vm = this.getViewModel();
        if(vm) { return vm; }
    }

});