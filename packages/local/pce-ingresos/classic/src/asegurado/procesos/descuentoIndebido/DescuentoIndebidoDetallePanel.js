Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetallePanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'descuento-indebido-detalle',

    requires: [
        'Pce.ingresos.asegurado.procesos.devolucionFondo.ChequeFondoDevolucionTabla'
    ],

    defaults: {
        width: '100%'
    },

    padding: 10,

    emptyText: 'No hay cheques agregados',

    layout: {
        type: 'vbox',
    },

    fieldDefaults: {
        xtype: 'displayfield',
        msgTarget: 'side',
        labelWidth: 140,
        labelAlign: 'top',
    },

    defaultType: 'textfield',
    items: [{
        xtype: 'fieldcontainer',
        layout: 'hbox',
        defaultType: 'displayfield',
        width: '100%',
        fieldDefaults: {
            labelAlign: 'top'
        },
        items: [{
            fieldLabel: '<b>Núm. afiliado</b>',
            readOnly: true,
            cls: 'read-only',
            width: '15%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.asegurado}'
            }
        }, {
            fieldLabel: '<b>Nombre completo</b>',
            readOnly: true,
            cls: 'read-only',
            width: '30%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.aseguradoNombreCompleto}'
            }
        },{
            fieldLabel: '<b>Teléfono</b>',
            readOnly: true,
            cls: 'read-only',
            width: '15%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.telefono}'
            }
        },
        // {
        //     fieldLabel: '<b>Dirección</b>',
        //     readOnly: true,
        //     cls: 'read-only',
        //     width: '15%',
        //     margin: '0 0 0 5',
        //     bind: {
        //         value: '{descuento.dirección}'
        //     }
        // }
    ],
    }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        defaultType: 'displayfield',
        width: '100%',
        fieldDefaults: {
            labelAlign: 'top'
        },
        items: [{
            fieldLabel: '<b>Etapa de solicitud</b>',
            readOnly: true,
            cls: 'read-only',
            width: '15%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.etapa}'
            },
            renderer(v) {
                switch (v) {
                    case 'TR': return '<b>EN TRAMITE</b>';
                    case 'CA': return '<b>CANCELADO</b>';
                    case 'AU': return '<b>AUTORIZADO</b>';
                    case 'CP': return '<b>COMPLETADO</b>';
                    default: return 'OTRO';
                }
            }
        }, {
            fieldLabel: '<b>Concepto</b>',
            readOnly: true,
            cls: 'read-only',
            width: '35%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.conceptoDescripcion}'
            },
        }, {
            fieldLabel: '<b>Dependencia</b>',
            readOnly: true,
            cls: 'read-only',
            width: '50%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.dependenciaDescripcion}'
            },
        }]
    }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        defaultType: 'displayfield',
        width: '100%',
        fieldDefaults: {
            labelAlign: 'top'
        },
        items: [{
            fieldLabel: '<b>Fecha creación</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.fechaCreacion}'
            },
            renderer: v => `${Ext.util.Format.date(v, 'd/m/Y')}`
        }, {
            fieldLabel: '<b>Fecha autorización</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.fechaAutorizacion}'
            },
            renderer: v => {
                return v !== '0001-01-01T00:00:00' ? `${Ext.util.Format.date(v, 'd/m/Y')}` : 'Pendiente';
            }
        }, {
            fieldLabel: '<b>Fecha cancelación</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.fechaCancelacion}'
            },
            renderer: v => {
                return v !== '0001-01-01T00:00:00' ? `${Ext.util.Format.date(v, 'd/m/Y')}` : 'Pendiente';
            }
        }, {
            fieldLabel: '<b>Fecha Afectación</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.fechaAfectacion}'
            },
            renderer: v => {
                return v !== '0001-01-01T00:00:00' ? `${Ext.util.Format.date(v, 'd/m/Y')}` : 'Pendiente';
            }
        }, ]
    }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        defaultType: 'displayfield',
        width: '100%',
        fieldDefaults: {
            labelAlign: 'top'
        },
        items: [{
            fieldLabel: '<b>Importe aportación</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.importeAportacion}'
            },
            renderer: v => {
                return v !== 0 ? `${Ext.util.Format.usMoney(v)}` : 'N/A';
            }
        }, {
            fieldLabel: '<b>Importe retención</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.importeRetencion}'
            },
            renderer: v => {
                return v !== 0 ? `${Ext.util.Format.usMoney(v)}` : 'N/A';
            }
        }, {
            fieldLabel: '<b>Importe Prestamo</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.importePrestamo}'
            },
            renderer: v => {
                return v !== 0 ? `${Ext.util.Format.usMoney(v)}` : 'N/A';
            }
        }, {
            fieldLabel: '<b>Importe Reconocimiento de Antigüedad</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.importeReconocimientoAntiguedad}'
            },
            renderer: v => {
                return v !== 0 ? `${Ext.util.Format.usMoney(v)}` : 'N/A';
            }
        }, ]
    }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        defaultType: 'displayfield',
        width: '100%',
        fieldDefaults: {
            labelAlign: 'top'
        },
        items: [{
            fieldLabel: '<b>Usuario creación</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.usuarioCreacion}'
            },
        }, {
            fieldLabel: '<b>Usuario Autorización</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.usuarioAutorizacion}'
            }
        }, {
            fieldLabel: '<b>Usuario Cancelación</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.usuarioCancelacion}'
            }
        }, {
            fieldLabel: '<b>Usuario Afectación</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.usuarioAfectacion}'
            }
        }]
    }, {
        xtype: 'fieldcontainer',
        layout: 'hbox',
        defaultType: 'displayfield',
        width: '100%',
        fieldDefaults: {
            labelAlign: 'top'
        },
        items: [{
            fieldLabel: '<b>Quincena inicial</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.quincenaInicio}'
            },
            renderer: v => {
                return v !== '0001-01-01T00:00:00' ? `${Ext.util.Format.date(v, 'd/m/Y')}` : 'Pendiente';
            }
        }, {
            fieldLabel: '<b>Quincena final</b>',
            readOnly: true,
            cls: 'read-only',
            width: '25%',
            margin: '0 0 0 5',
            bind: {
                value: '{descuento.quincenaFin}'
            },
            renderer: v => {
                return v !== '0001-01-01T00:00:00' ? `${Ext.util.Format.date(v, 'd/m/Y')}` : 'Pendiente';
            }
        }]
    }, {
        xtype: 'cheque-devolucion-tabla',
        reference: 'chequeDevolucionTabla',
        columns: [{
            text: 'Núm. cheque',
            dataIndex: 'numCheque',
            renderer: v => { return v > 0 ? v : 'Pendiente emisión'; },
            flex: 2
        }, {
            text: 'Fecha emisión',
            dataIndex: 'fechaCheque',
            renderer: v => {
                let fechaCheque = Ext.util.Format.date(v, 'd/m/Y');
                return fechaCheque !== '01/01/0001' ? fechaCheque : '';
            },
            flex: 2
        }, {
            text: 'Estatus',
            dataIndex: 'estatus',
            renderer(v) {
                switch (v) {
                    case 'TR': return 'EN TRAMITE';
                    case 'CA': return 'CANCELADO';
                    case 'AU': return 'AUTORIZADO';
                    case 'CP': return 'COMPLETADO';
                    default: return 'OTRO';
                }
            },
            flex: 2
        }, {
            text: 'Beneficiario',
            dataIndex: 'beneficiario',
            flex: 6
        },{
            text: 'RFC',
            dataIndex: 'rfc',
            flex: 3
        },{
            text: 'Importe',
            dataIndex: 'importe',
            renderer(v) {
                return Ext.util.Format.usMoney(v);
            },
            flex: 3
        }],
    }],


    beforeRender() {
        this.callParent();

        let descuento = this.getViewModel().get('descuento.id');
        let store = this.down('grid').getStore();

        store.setProxy({
            type: 'rest',
            url: '/api/cajas/chequera/cheque-solicitud/por-descuento-indebido'
        }).load({
            params: {
                solicitudId: descuento
            }
        });
    },
});