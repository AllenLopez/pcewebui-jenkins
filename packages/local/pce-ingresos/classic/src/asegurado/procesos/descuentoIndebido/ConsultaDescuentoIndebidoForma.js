/**
 * @class Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoForma
 * @extends Ext.form.Panel
 * @xtype consulta-descuento-indebido-forma
 * Formulario para filtros de consulta de descuentos indebidos
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoForma', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'consulta-descuento-indebido-toolbar',

    requires: [
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.ingresos.movimiento.TipoMovimientoStore'
    ],

    layout: {
        type: 'table',
        columns: 4
    },

    defaults: {
        width: '100%',
        padding: 10,
    },

    items:[{
        xtype: 'combobox',
        name: 'motivo',
        fieldLabel: 'Motivo',
        displayField: 'descripcion',
        store: {
            type: 'tipos-movimiento',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '/api/ingresos/asegurado/descuento-indebido/tipos-descuento'
            }
        },
        colspan: 2
    }, {
        xtype: 'asegurado-selector',
        fieldLabel: 'Asegurado',
        colspan: 2
    }, {
        xtype: 'datefield',
        name: 'fechaInicio',
        reference: 'fechaInicioSelector',
        fieldLabel: 'Periodo',
        forceSelection: true,
        colspan: 1
    }, {
        xtype: 'datefield',
        name: 'fechaFin',
        fieldLabel: 'Hasta',
        forceSelection: true,
        colspan: 1
    }, {
        text: 'Buscar',
        colspan: 1
    }],
});