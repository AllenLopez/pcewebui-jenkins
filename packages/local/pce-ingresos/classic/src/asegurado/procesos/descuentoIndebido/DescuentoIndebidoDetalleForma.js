/**
 * @class Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetalleForma
 * @extends Ext.form.Panel
 * @xtype dependencia-detalle-di-forma
 *
 *
 * @author Jorge Escamilla
 */
Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetalleForma', {
    extend: 'Ext.form.Panel',
    xtype: 'descuento-indebido-detalle-forma',

    requires: [
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaAseguradoStore',
    ],

    layout: {
        type: 'table',
        columns: 4
    },

    bodyPadding: 10,

    defaults: { width: '100%' },

    items: [{
        xtype: 'dependencia-selector',
        name: 'dependencia',
        fieldLabel: 'Dependencia',
        valueField: 'id',
        store: {
            type: 'dependencia-asegurado',
            autoLoad: true,
            proxy: {
                type: 'rest',
                url: '/api/afiliacion/dependencia-asegurado/dependencias'
            }
        },
        colspan: 2
    }, {
        xtype: 'textfield',
        name: 'departamento',
        fieldLabel: 'Departamento',
        colspan: 2
    }, {
        xtype: 'datefield',
        name: 'quincenaInicio',
        fieldLabel: 'Periodo',
        forceSelection: true,
        colspan: 2
    }, {
        xtype: 'datefield',
        name: 'quincenaFin',
        fieldLabel: 'Hasta',
        forceSelection: true,
        colspan: 2
    }]
});