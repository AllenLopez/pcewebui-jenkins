Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.reconocimientoAntiguedad.EditorReconocimientoAntiguedadController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.editor-reconocimiento-antiguedad',

    helperSolicitud: Pce.asegurado.procesos.descuentoIndebido.ActualizaSolicitudDescIndebidoHelper,
    solicitud: null,
    asegurado: null,

    init() {
        this.cargaDatosSolicitud();
    },

    cargaDatosSolicitud() {
        const me = this;
        const solicitudId = parseInt(me.getView().solicitudId);
        if (solicitudId && solicitudId > 0) {
            me.cargarSolicitud(solicitudId, me);
        } 
    },

    cargarSolicitud(solicitudId, me) {
        Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoEditor.load(solicitudId,
            {
            success(record) {
                const aseguradoId = record.get('asegurado');

                me.solicitud = record;
                me.setViewModelDataProp('solicitud', record);
                me.getAsegurado(aseguradoId);
                me.getInfoAsegurado(aseguradoId);
                me.getDependenciaAsegurado(aseguradoId, me);
            }
        });
    },

    marcaRegistrosInvalidos(nodes, ids, rowError) {
        if (ids.length > 1) {
            nodes.forEach(el => {
                if (ids.includes(el.id)) {
                    el.className = el.className.concat(rowError);

                    this.invalidaSolicitud(`El usuario tiene saldo en dos o mas cuentas.
                    Primero debe realizar un traspaso antes de continuar con la solicitud`);
                }
            });
        }
    },

    onActualizarSolicitud(){
        const solicitud = this.getViewModelDataProp('solicitud');
        if (solicitud.dirty) {
            this.confirmarActualizacion(solicitud.getId()).then(ans => {
                if (ans === 'yes') {
                    this.helperSolicitud
                        .onActualizarSolicitud(solicitud);                
                    Ext.util.History.back();
                    this.onImprimirSolicitud(solicitud)
                }
            });
        } else { 
             Ext.Msg.alert('Sin cambios', `No se ha registrado ningún cambio para actualizar`);
        }
    },
 
    onCancelarEdicion() {
        this.confirmarCancelacion().then(ans => {
            if (ans === 'yes') { Ext.util.History.back(); }
        });
    },


    confirmarActualizacion(solicitudId) {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `La solicitud con folio <b>${solicitudId}</b> ha sido modificada, ¿Desea guardar los cambios?`,
                ans => resolve(ans));
        });
    },

    confirmarCancelacion() {
        return new Promise(resolve => {
            Ext.Msg.confirm('Confirmar operación',
                `La solicitud perderá los cambios realizados, ¿Desea cancelar y volver?`,
                ans => resolve(ans));
        });
    },

    invalidaSolicitud(msg) {
        this.mostrarMensajeAlerta('Solicitud inválida', msg);
        this.setViewModelDataProp('solicitudValida', false);
        this.lookup('tapGuardar').disable();
    },

    mostrarMensajeAlerta(titulo, mensaje) {
        Ext.Msg.alert(titulo, mensaje);
    },
    getAsegurado(aseguradoId) {
        Pce.afiliacion.asegurado.Asegurado.load(aseguradoId, {
            success: afil => { 
                this.setViewModelDataProp('asegurado', afil);
                this.asegurado = afil; }
        });
    },
    getDependenciaAsegurado(aseguradoId, me) {
        const store = me.lookup('dependenciaSelector').getStore();
        store.load({
            params: { numeroAfiliacion: aseguradoId }
        });     
        
    },
    getInfoAsegurado(aseguradoId) {
        Pce.afiliacion.asegurado.AseguradoInfo.load(aseguradoId, {
            success: info => {
                this.setViewModelDataProp('aseguradoInfo', info);
                this.helperSolicitud
                    .determinaJubilacionAsegurado(aseguradoId, info)
                    if (info.get('jubilado') === 'SI') {
                            this.invalidaSolicitud('Los asegurados jubilados no pueden solicitar la devolución de su fondo');
                    }
            }
        });
    },
    getDependencia(dependenciaId) {
        return new Promise((resolve) => {
            Pce.afiliacion.dependencia.Dependencia.load(dependenciaId, {
                success: dependencia => {
                    if (dependencia) { resolve(dependencia); }
                }
            });
        });
    },
    setNumDependencia(selector,valor){ 
        const dependenciaId = this.getViewModelDataProp('dependenciaSelector').selection.id;
        if(dependenciaId > 0){
            const solicitud = this.getViewModelDataProp('solicitud');
            solicitud.set('dependencia', dependenciaId);
            this.setViewModelDataProp('solicitud', solicitud);
        } 
    },

    getViewModelDataProp(prop) {
        let vm = this.getViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },
    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },
    onImprimirSolicitud(tabla, i, j, item, e, record) {
        const solicitud = this.getViewModelDataProp('solicitud');
        const tipoMovimientoClave = solicitud.getData().tipoMovimientoClave;
        const asegurado = solicitud.getData().asegurado;
        const solicitudId = solicitud.getId();
        const etapa = solicitud.getData().etapa;
        this.imprimirSolicitudDescuentoIndebido(tipoMovimientoClave,asegurado,solicitudId,etapa);
    },

    imprimirSolicitudDescuentoIndebido(movimiento,asegurado, folio,etapa) {
        window.open(`/api/ingresos/asegurado/descuento-indebido/descargar?claveMov=${movimiento}&numafil=${asegurado}&solicitudId=${folio}&etapa=${etapa}`);
    },
});