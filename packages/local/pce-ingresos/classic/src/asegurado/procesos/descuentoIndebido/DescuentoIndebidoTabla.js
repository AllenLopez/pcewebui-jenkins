/**
 * @class Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoTabla
 * @extends Ext.grid.Panel
 * @xtype consulta-descuento-tabla
 * Enlista los registros de descuento indebido realizados
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'consulta-descuento-tabla',

    requires: [
        'Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebidoStore'
    ],

    store: {
        type: 'descuento-indebido'
    },

    columns: [{
        text: 'Folio',
        dataIndex: 'id',
        flex: 1,
    }, {
        text: 'Tipo descuento indebido',
        dataIndex: 'tipoMovimientoDescripcion',
        flex: 2
    }, {
        text: 'Núm. Afil.',
        dataIndex: 'asegurado',
        flex: 1
    } ,{
        text: 'Nombre',
        dataIndex: 'aseguradoNombreCompleto',
        flex: 2
    }, {
        text: 'Etapa',
        dataIndex: 'etapa',
        renderer(v) {
            switch(v) {
                case 'TR': return 'EN TRÁMITE';
                case 'AU': return 'AUTORIZADO';
                case 'CA': return 'CANCELADO';
                case 'CP': return 'COMPLETADO';
            }
        },
        flex: 1
    }, {
        text: 'Fecha creación',
        dataIndex: 'fechaCreacion',
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Fecha primer descuento',
        dataIndex: 'quincenaInicio',
        forceSelection: true,
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Fecha último descuento',
        dataIndex: 'quincenaFin',
        forceSelection: true,
        renderer: v => Ext.util.Format.date(v, 'd/m/Y'),
        flex: 1
    }, {
        text: 'Dependencia',
        dataIndex: 'dependenciaDescripcion',
        flex: 3
    }, {
        text: '$ Rec. Antigüedad',
        dataIndex: 'importeReconocimientoAntiguedad',
        renderer: Ext.util.Format.usMoney,
        hidden: true,
        flex: 1
    }, {
        text: '$ Retención',
        dataIndex: 'importeRetencion',
        renderer: Ext.util.Format.usMoney,
        hidden: true,
        flex: 1
    }, {
        text: '$ Aportación',
        dataIndex: 'importeAportacion',
        renderer: Ext.util.Format.usMoney,
        hidden: true,
        flex: 1
    }, {
        text: '$ Prestamo',
        dataIndex: 'importePrestamo',
        renderer: Ext.util.Format.usMoney,
        hidden: true,
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: 70,
        items: [{
            iconCls: 'x-fa fa-search',
            tooltip: 'ver detalles',
            handler: 'onMostrarDetallesDescuento'
        },{
            iconCls: 'x-fa fa-print',
            tooltip: 'imprimir solicitud',
            handler: 'onImprimirSolicitud'
        },{
            iconCls: 'x-fa fa-pencil',
            tooltip: 'editar solicitud',
            handler: 'onEditarSolicitud',
              isDisabled(view, rowIndex, collIndex, item, record) {
                const etapa = record.get('etapa');
                return (etapa === 'CP' || etapa === 'CA');
            } 
        }]
    }]
});