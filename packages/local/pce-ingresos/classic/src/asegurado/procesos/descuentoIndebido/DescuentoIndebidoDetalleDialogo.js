/**
 * @class Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetalleDialogo
 * @extends Ext.window.Window
 * @xtype dialogo-dependencia-detalle_di
 * Pantalla de captura de detalles de decolucion por dependencia, regimen y rango de quincenas
 */

Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoDetalleDialogo', {
    extend: 'Ext.window.Window',
    xtype: 'dialogo-di-detalle',

    autoShow: true,
    modal: true,
    width: 900,

    defaultListenerScope: true,

    record: null,
    aseguradoId: null,

    items: [{
        xtype: 'descuento-indebido-detalle-forma'
    }],

    buttons: [{
        text: 'Aceptar',
        handler: 'onAceptarTap'
    }, {
        text: 'Cancelar',
        handler: 'onCancelarTap'
    }],

    beforeRender() {
        let store = this.down('form').getForm().findField('dependencia').getStore();
        store.load({
            params: {
                numeroAfiliacion: this.aseguradoId
            }
        });
    },

    afterRender() {
        this.callParent();
        if (this.record) {
            this.down('form').loadRecord(this.record);
        }
    },

    onAceptarTap() {
        let form = this.down('form');
        let dep = form.getForm().findField('dependencia').getSelection();

        if(form.isValid()) {
            this.record.set('dependenciaDescripcion', dep.get('descripcion'));
            form.updateRecord();

            this.fireEvent('guardar', this, this.record);
        }
    },

    onCancelarTap() {
        this.close();
    },
});