/**
 * @class Pce.ingresos.asegurado.proceso.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedadForma
 * @extends Ext.form.Panel
 * @xtype di-reconocimiento-antiguedad-forma
 *
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.proceso.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedadForma', {
    extend: 'Ext.form.Panel',
    xtype: 'di-reconocimiento-antiguedad-forma',

    requires: [
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaAseguradoStore',
    ],

    layout: {
        type: 'table',
        columns: 6
    },

    bodyPadding: 10,
    defaults: {
        width: '100%'
    },

    items: [{
        xtype: 'asegurado-selector',
        fieldLabel: 'Asegurado',
        name: 'asegurado',
        listeners: {
            change: 'onAseguradoChange'
        },
        bind: {
            selection: '{asegurado}'
        },
        colspan: 4
    }, {
        xtype: 'textfield',
        name: 'numeroAfiliacion',
        fieldLabel: 'Núm. Afil',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.id}'
        },
        colspan: 1
    } ,{
        xtype: 'textfield',
        name: 'rfc',
        fieldLabel: 'RFC',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.rfc}'
        },
        colspan: 1
    }, {
        xtype: 'textfield',
        name: 'direccion',
        fieldLabel: 'Dirección',
        readOnly: true,
        cls: 'read-only',
        bind: {
            value: '{asegurado.direccion}'
        },
        colspan: 3
    }, {
        xtype: 'textfield',
        name: 'telefono',
        fieldLabel: 'Teléfono',
        allowBlank: false,
        colspan: 2
    }, {
        xtype: 'textfield',
        name: 'jubilado',
        fieldLabel: 'Jubilado',
        bind: {
            value: '{aseguradoInfo.jubilado}'
        },
        cls: 'read-only',
        colspan: 1
    }, {
        xtype: 'textarea',
        name: 'observaciones',
        fieldLabel: 'Observaciones',
        allowBlank: false,
        grow: true,
        anchor: '100%',
        colspan: 6
    }, {
        xtype: 'dependencia-selector',
        reference: 'dependenciaSelector',
        name: 'dependencia',
        fieldLabel: 'Dependencia',
        valueField: 'id',
        store: {
            type: 'dependencia-asegurado',
            autoLoad: true,
            proxy: {
                type: 'rest',
                url: '/api/afiliacion/dependencia-asegurado/dependencias'
            }
        },
        colspan: 4
    }, {
        xtype: 'textfield',
        name: 'departamento',
        fieldLabel: 'Departamento',
        colspan: 2
    }, {
        xtype: 'datefield',
        name: 'quincenaInicio',
        fieldLabel: 'Desde',
        forceSelection: true,
        bind: {
            value: '{quincenaInicio}',
            maxValue: '{quincenaFin}'
        },
        colspan: 2
    }, {
        xtype: 'datefield',
        name: 'quincenaFin',
        reference: 'fechaInicioSelector',
        fieldLabel: 'Hasta',
        forceSelection: true,
        bind: {
            minValue: '{quincenaInicio}',
            disabled: '{!quincenaInicio}',
            value: '{quincenaFin}'
        },
        colspan: 2
    }, {
        xtype: 'numberfield',
        name: 'importeReconocimientoAntiguedad',
        fieldLabel: 'Importe a devolver',
        labelWidth: 130,
        allowBlank: false,
        bind: {
            value: '{importeDevolucion}'
        },
        colspan: 2
    }]


});