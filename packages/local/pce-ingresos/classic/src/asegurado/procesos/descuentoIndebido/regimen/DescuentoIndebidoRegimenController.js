/**
 * @class Pce.ingresos.asegurado.prcesos.reconocimientoAntiguedad.DescuentoIndebidoRegimenController
 * @extends Ext.app.ViewController
 * @alias controller.di-regimen-panel
 * Controlador de pantalla de descuento indebido por regimen incorrecto
 * la cual permite calclular aportaciones y retenciones de la dependencia seleccionada
 * por cada una de las quincenas de cierto periodo y sumarizar los importes totales
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.prcesos.reconocimientoAntiguedad.regimen.DescuentoIndebidoRegimenController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.di-regimen-panel',

    solicitud: null,
    asegurado: null,
    importeRetencion: 0,
    importeAportacion: 0,
    mensajeCI: `La solicitud no será procesada ni guardada debido a que
            este asegurado pertenece al régimen de <b>Cuenta Individual</b>`,
    mensajeJubilado: 'Los asegurados jubilados no pueden solicitar la devolución del descuento indebido',

    onAseguradoChange(selector) {
        let asegurado = selector.getSelection();
        if(asegurado) {
            let detallesStore = this.obtieneStoreTablaDetalle();
            let grid = this.obtieneGridDetalle();
            this.asegurado = asegurado;
            this.cargaAseguradoInfo(asegurado);
            this.resetImportes();

            let store = this.lookup('dependenciaSelector').getStore();
            store.load({
                params: {
                    numeroAfiliacion: asegurado.getId()
                }
            });

            detallesStore.removeAll();
            grid.view.refresh();
        }
    },

    onCalcularImportes() {
        let filtro = this.lookup('DescuentoIndebidoRegmenForma').getValues();
        let store = this.obtieneStoreTablaDetalle();

        this.resetImportes();

        store.load({
            params: {
                asegurado: this.asegurado.getId(),
                dependencia: filtro.dependencia,
                quincenaInicio: filtro.quincenaInicio,
                quincenaFin: filtro.quincenaFin,
                concepto: 1
            }
        });
    },

    onProcesarTap() {
        let me = this;
        let form = this.obtenerFormulario();

        if(!form.isValid()) {
            return;
        }

        if(me.getViewModelDataProp('detalles').length <= 0) {
            Ext.Msg.alert('solicitud incompleta',
                `Por favor llene todos los campos solicitados.
                Revise que exista un importe válido para devolver al asegurado`);
            return;
        }

        this.confirmarProceso().then(ans => {
            if(ans === 'yes') {
                Ext.getBody().mask('Cargando retenciones...');
                let filtro = me.obtenerDatosFormulario();
                let records = me.getViewModelDataProp('detalles');
                let grid = me.obtieneGridDetalle();

                this.guardarSolicitud(filtro, records)
                    .then(solicitud => {
                            let solicitudId = solicitud.getId();
                            let detalles = me.getViewModelDataProp('detalles');
                            let store = this.obtieneStoreTablaDetalle();

                            me.guardarDescuentoIndebidoDetalle(detalles, solicitudId)
                                .then(totalSuccess => {
                                    me.guardarChequeSolicitud(solicitud)
                                        .then(cheque => {
                                            console.log(cheque);
                                                Ext.Msg.alert('Proceso completado',
                                                    `La solicitud de descuento indebido se guardo correctamente con el folio: <b>${solicitudId}</b>`);

                                                Ext.toast(`Movimientos procesados con exito: ${totalSuccess}`);

                                                form.reset();
                                                store.removeAll();
                                                grid.view.refresh();
                                                Ext.getBody().unmask();

                                                me.imprimirSolicitudDescuentoIndebido('DIRE',
                                                    me.asegurado.getId(),
                                                    solicitudId,
                                                    solicitud.get('etapa'));
                                            },
                                            error => {
                                                Ext.Msg.alert('Error en proceso',
                                                    `Ha ocurrido un error con la generación del cheque y no pudo ser guardado. Únicamente la solicitud con el folio: ${error.errorMessage} se guardo correctamente`);
                                                form.reset();
                                                Ext.getBody().unmask();
                                            });
                                    },
                                    error =>{
                                        Ext.Msg.alert('Error',
                                            `Ha ocurrido un error y los detalles de solicitud no pudieron ser guardados: ${error.errorMessage}`);
                                        Ext.getBody().unmask();
                                    });
                        },
                        error => {
                            Ext.Msg.alert('Error',
                                `Ha ocurrido un error y la solicitud no pudo ser guardada: ${error.errorMessage}`);
                            Ext.getBody().unmask();
                        });
            }
        });
    },

    onSelectionChange(tabla, seleccion) {
        if(seleccion.length < 1) {
            this.setViewModelDataProp('importeRetencion', 0);
            this.setViewModelDataProp('importeAportacion', 0);
        }

        // almacena registros seleccionados a propiedad de vm
        this.setViewModelDataProp('detalles', seleccion);
    },

    onSelectRecord(rowModel, record) {
        let impRetencion = record.get('importeRetencion');
        let impAportacion = record.get('importeAportacion');

        this.sumaValoresImportes(impRetencion, impAportacion);

    },

    onDeselectRecord(tabla, record) {
        let impRetencion = record.get('importeRetencion');
        let impAportacion = record.get('importeAportacion');

        this.restaValoresImportes(impRetencion, impAportacion);
    },

    onEditImporte(editor, contexto) {
        console.log(contexto);

        if(contexto.field === 'importeRetencion') {
            this.restaValoresImportes(contexto.originalValue, 0);
            this.sumaValoresImportes(contexto.value, 0);
        } else {
            this.restaValoresImportes(0, contexto.originalValue);
            this.sumaValoresImportes(0, contexto.value);
        }
    },

    cargaAseguradoInfo(asegurado) {
        asegurado.getInfo().then(info => {
            this.setViewModelDataProp('aseguradoInfo', info);
            if(info.get('jubilado') === 'SI') {
                this.invalidaSolicitud(this.mensajeJubilado, true);
            } else {
                this.lookup('DescuentoIndebidoDetalleTabla').setDisabled(false);
                this.lookup('guardarTap').setDisabled(false);
            }
        });
    },

    confirmarProceso() {
        return new Promise(resolve => {
            Ext.Msg.confirm(
                'Confirmar operación',
                `Se guardará la solicitud y registros seleccionados de descuento indebido por régimen incorrecto. ¿Deseas proceder?`,
                ans => resolve(ans));
        });
    },

    guardarSolicitud(filtro) {
        return new Promise((resolve, reject) => {
            let descuento = new Pce.ingresos.asegurado.descuentoIndebido.DescuentoIndebido(filtro);

            descuento.getProxy().setApi({
                create: '/api/ingresos/asegurado/descuento-indebido/regimen-incorrecto',
            });

            descuento.save({
                success(record) {
                    resolve(record);
                },
                failure(record, batch) {
                    let error = batch.getError();
                    let msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                    reject(msg);
                }
            });
        });
    },

    guardarDescuentoIndebidoDetalle(detalles, solicitudId) {
        return new Promise((resolve, reject) => {
            detalles.forEach(el => {
                el.data.DescuentoIndebido = solicitudId;
                el.phantom = true;
            });

            let store = this.obtieneStoreTablaDetalle();
            store.removeAll();
            store.loadData(detalles);

            console.log(store);
            store.sync({
                success(batch) {
                    console.log('success');
                    resolve(batch.getTotal());
                },
                failure(record, batch) {
                    console.log(record);
                    console.log(batch);
                    let error = batch.getError();
                    let msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                    reject(msg);
                }
            });
        });
    },

    guardarChequeSolicitud(solicitud) {
        let cheque = new Pce.ingresos.asegurado.chequeFondo.ChequeFondoSolicitud({
            beneficiario: this.asegurado.get('nombreCompleto'),
            rfc: this.asegurado.get('rfc'),
            importe: solicitud.get('importeRetencion'),
            solicitudDevolucionId: 0,
            descuentoIndebidoDevId: solicitud.getId()
        });

        return new Promise((resolve, reject) => {
            cheque.save({
                success(record) {
                    resolve(record);
                },
                failure(record, batch) {
                    let error = batch.getError();
                    let msg = Ext.isObject(error) ? JSON.parse(error.response.responseText) : error;
                    reject(msg);
                }
            });
        });
    },

    invalidaSolicitud(msg, deshabilitado) {
        // ture para desabilitar, false para habilitar
        this.resetImportes();
        this.lookup('DescuentoIndebidoDetalleTabla').setDisabled(deshabilitado);
        this.lookup('guardarTap').setDisabled(deshabilitado);
        this.mostrarMensajeAlerta('Solicitud inválida', msg);
    },

    mostrarMensajeAlerta(titulo, mensaje) {
        Ext.Msg.alert(titulo, mensaje);
    },

    imprimirSolicitudDescuentoIndebido(clave, numAfil, solicitudId, etapa) {
        window.open(`/api/ingresos/asegurado/descuento-indebido/descargar?claveMov=${clave}&numafil=${numAfil}&solicitudId=${solicitudId}&etapa=${etapa}`);
    },

    sumaValoresImportes(impRetencion, impAportacion) {
        this.importeRetencion += impRetencion;
        this.importeAportacion += impAportacion;
        this.setImportesViewModel();
    },

    restaValoresImportes(impRetencion, impAportacion) {
        this.importeRetencion -= impRetencion;
        this.importeAportacion -= impAportacion;
        this.setImportesViewModel();
    },

    resetImportes() {
        this.importeRetencion = 0;
        this.importeAportacion = 0;
        this.setImportesViewModel();
    },

    setImportesViewModel() {
        this.setViewModelDataProp('importeRetencion', this.importeRetencion);
        this.setViewModelDataProp('importeAportacion', this.importeAportacion);
    },

    obtieneGridDetalle() {
        return this.lookup('DescuentoIndebidoDetalleTabla');
    },

    obtieneStoreTablaDetalle() {
        return this.lookup('DescuentoIndebidoDetalleTabla').getStore();
    },

    obtenerDatosFormulario() {
        return this.obtenerFormulario().getFieldValues();
    },

    obtenerFormulario() {
        return this.getView().down('form').getForm();
    },

    obtieneViewModel() {
        let vm = this.getViewModel();
        if(vm) {
            return vm;
        }
    },

    getViewModelDataProp(prop) {
        let vm = this.obtieneViewModel();
        if(vm) {
            let data = vm.getData();
            if(data) { return data[prop]; }
        }
    },

    setViewModelDataProp(prop, val) {
        let vm = this.getViewModel();
        if(vm) { vm.set(prop, val); }
    },
});