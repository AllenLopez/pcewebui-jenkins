/**
 * @class Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoPanel
 * @extends Ext.panel.Panel
 * @xtype consulta-descuento-indebido
 * Panel de filtro y consulta de solicitudes de descuento indebido para procesamiento posterior al trámite
 *
 * @author Jorge Escamilla
 */

Ext.define('Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'consulta-descuento-indebido',

    requires: [
        'Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoController',
        'Pce.ingresos.asegurado.procesos.descuentoIndebido.DescuentoIndebidoTabla'
    ],

    controller: 'consulta-descuento-indebido-panel',

    title: 'Consulta de solicitudes de descuentos indebidos',

    layout: 'fit',

    viewModel: {
        data: {
            folio: 0,
            asegurado: 0,
            movimiento: 0,
            fechaInicio: null,
            fechaFin: null,
            etapa: null,
            solicitud: null
        }
    },

    items:[{
        xtype: 'consulta-descuento-tabla',
        reference: 'DescuentoIndebidoTabla',
        bind: {
            selection: '{solicitud}'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                xtype: 'numberfield',
                fieldLabel: 'Folio',
                bind: {
                    value: '{folio}'
                },
                colspan: 1
            }, {
                xtype: 'combobox',
                name: 'movimiento',
                fieldLabel: 'Tipo',
                displayField: 'descripcion',
                valueField: 'id',
                store: {
                    type: 'tipos-movimiento',
                    proxy: {
                        type: 'ajax',
                        url: '/api/ingresos/asegurado/descuento-indebido/tipos-descuento'
                    }
                },
                bind: {
                    value: '{movimiento}'
                },
                flex: 3
            }, {
                xtype: 'asegurado-selector',
                name: 'asegurado',
                fieldLabel: 'Asegurado',
                bind: {
                    value: '{asegurado}'
                },
                flex: 3
            }, ]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'datefield',
                name: 'fechaInicio',
                reference: 'fechaInicioSelector',
                fieldLabel: 'Periodo',
                forceSelection: true,
                bind: {
                    value: '{fechaInicio}'
                },
                flex: 1
            }, {
                xtype: 'datefield',
                name: 'fechaFin',
                fieldLabel: 'Hasta',
                forceSelection: true,
                bind: {
                    minValue: '{fechaInicio}',
                    value: '{fechaFin}'
                },
                flex: 1
            }, {
                xtype: 'combobox',
                name: 'etapa',
                fieldLabel: 'Etapa',
                displayField: 'desc',
                valueField: 'etapa',
                store: {
                    fields: ['etapa', 'desc'],
                    data: [
                        {etapa: 'TR', desc: 'EN TRÁMITE'},
                        {etapa: 'AU', desc: 'AUTORIZADO'},
                        {etapa: 'CA', desc: 'CANCELADO'},
                        {etapa: 'CP', desc: 'COMPLETADO'},
                    ]
                },
                queryMode: 'local',
                bind: {
                    value: '{etapa}'
                },
                flex: 1
            }, {
                text: 'Buscar',
                handler: 'onBuscar',
                flex: 1
            }, {
                text: 'Generar reporte',
                handler: 'onGenerarReporte',
                flex: 1
            }]
        }, {
            xtype: 'toolbar',
            dock: 'top',
            bind: {
                disabled: '{!solicitud}'
            },
            items: [{
                text: 'Autorizar',
                tooltip: 'realiza autorización de solicitud',
                disabled: true,
                bind: {
                    disabled: '{solicitud.etapa !== "TR"}'
                },
                handler: 'onAutorizarTap',
            }, {
                text: 'Cancelar',
                tooltip: 'actualiza etapa a cancelada',
                disabled: true,
                bind: {
                    disabled: '{!solicitud || solicitud.etapa === "CP" || solicitud.etapa === "CA"}'
                },
                handler: 'onCancelarTap'
            }, {
                xtype: 'tbseparator'
            }, {
                text: 'Realiza afectación',
                tooltip: 'realiza cargo a favor por el motivo de descuento indebido registrado',
                disabled: true,
                bind: {
                    disabled: '{!solicitud || solicitud.etapa !== "AU"}'
                },
                handler: 'onRealizarAfectacionTap',
            }]
        }]
    }]
});