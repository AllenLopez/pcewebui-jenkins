/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.procesos.DescuentoIndebidoPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'descuento-indebido-panel',

    requires: [
        'Pce.ingresos.asegurado.procesos.DescuentoIndebidoController',

        'Pce.ingresos.asegurado.procesos.DescuentoIndebidoForma'
    ],

    title: 'Devolución de descuento indebido',
    controller: 'descuento-indebido',
    items: [{
        xtype: 'descuento-indebido-forma',
        reference: 'forma'
    }]
});