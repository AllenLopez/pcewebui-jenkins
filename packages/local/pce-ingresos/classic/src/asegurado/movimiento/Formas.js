/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.movimiento.Formas', {
    singleton: true,

    requires: [
        'Pce.ingresos.asegurado.movimiento.forma.PagoDirectoPrestamoPersonal'
    ],

    /**
     *
     * @param {string} movimiento
     * @returns {string}
     */
    getXType(movimiento) {
        return `${movimiento}-forma`;
    }
});