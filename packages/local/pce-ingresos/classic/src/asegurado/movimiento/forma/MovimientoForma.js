/**
 * @author Rubén Maguregui
 *
 * @instance
 * @name Pce.ingresos.asegurado.movimiento.forma.MovimientoForma
 * @description Forma base para el registro de movimiento manuales para un
 * asegurado
 */
Ext.define('Pce.ingresos.asegurado.movimiento.forma.MovimientoForma', {
    extend: 'Ext.form.Panel',
    xtype: 'asegurado-movimiento-forma',

    requires: [
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.ingresos.asegurado.cartera.CarteraServicio'
    ],

    /**
     * @property {Pce.ingresos.asegurado.cartera.CarteraServicio}
     */
    servicioCartera: null,

    viewModel: {
        data: {
            descripcion: 'Forma para procesamiento de movimiento manual',
            procesarMovimientoText: 'Procesar movimiento',
            procesarMovimientoIcono: 'x-fa fa-check'
        }
    },

    defaultListenerScope: true,
    referenceHolder: true,

    title: 'Movimiento manual',

    dockedItems: [{
        xtype: 'container',
        style: 'padding: 10px;',
        bind: {
            html: '{descripcion}'
        }
    }, {
        xtype: 'toolbar',
        items: [{
            xtype: 'asegurado-selector',
            name: 'numeroAfiliacion',
            width: '50%',
            bind: {
                selection: '{asegurado}'
            },
            listeners: {
                select: 'onAseguradoSelect'
            }
        }]
    }],
    bbar: [{
        handler: 'onProcesarMovimientoTap',
        formBind: true,
        bind: {
            text: '{procesarMovimientoText}',
            iconCls: '{procesarMovimientoIcono}'
        }
    }, ' ', {
        xtype: 'component',
        bind: {
            html: '{descripcionFinal}'
        }
    }],

    onAseguradoSelect() {/* Implementar en subclases */},

    onProcesarMovimientoTap() {

    },

    /**
     * @returns {Pce.ingresos.asegurado.cartera.CarteraServicio}
     */
    getServicioCartera() {
        if (null === this.servicioCartera) {
            this.servicioCartera = new Pce.ingresos.asegurado.cartera.CarteraServicio();
        }
        return this.servicioCartera;
    }
});