/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.asegurado.movimiento.forma.PagoDirectoPrestamoPersonal', {
    extend: 'Pce.ingresos.asegurado.movimiento.forma.MovimientoForma',
    xtype: 'pago-directo-prestamo-personal-forma',

    requires: [
        'Ext.layout.container.Table',
        'Ext.form.field.Text',
        'Ext.form.field.Number',

        'Pce.ingresos.asegurado.EstadoDeCuentaSelector',
        'Pce.ingresos.asegurado.EstadoDeCuentaStore',
        // 'Pce.caja.banco.BancoSelector',
        // 'Pce.caja.banco.CuentaBancoSelector',
        'Pce.ingresos.FormaPagoSelector'
    ],

    viewModel: {
        data: {
            descripcion: 'Pago directo de prestamos personales',
            procesarMovimientoText: 'Registrar pago',
            procesarMovimientoIcono: 'x-fa fa-money',
            asegurado: null,
            estadoDeCuenta: null
        },
        formulas: {
            descripcionFinal(get) {
                let importe = get('importe');

                return `<b>Importe a pagar:</b> ${Ext.util.Format.usMoney(importe)}`;
            }
        }
    },

    title: 'Pago préstamo personal',
    layout: {
        type: 'table',
        columns: 4
    },

    bind: {
        title: 'Pago préstamo personal - {asegurado.nombreCompleto}'
    },

    defaults: {
        width: '100%'
    },
    items: [{
        xtype: 'asegurado-estado-de-cuenta-selector',
        reference: 'estadoDeCuentaSelector',
        emptyText: 'Selecciona el estado de cuenta',
        valueNotFoundText: 'No existen estados de cuenta de préstamo personal para el asegurado seleccionado',
        colspan: 2,
        store: {
            type: 'asegurado-estado-de-cuenta',
            filters: [{
                property: 'conceptoClave',
                value: 'C'
            }]
        },
        bind: {
            disabled: '{!asegurado}',
            selection: '{estadoDeCuenta}'
        },
    }, {
        xtype: 'numberfield',
        reference: 'importeCampo',
        name: 'importe',
        fieldLabel: 'Importe',
        allowBlank: false,
        minValue: 0.01,
        value: 0,
        bind: {
            maxValue: '{estadoDeCuenta.saldo}',
            value: '{importe}',
            disabled: '{!estadoDeCuenta}'
        }
    }, {
        xtype: 'forma-pago-selector',
        name: 'formaPago',
        allowBlank: false
    }, {
        xtype: 'banco-selector',
        name: 'banco',
        fieldLabel: 'Banco',
        allowBlank: false,
        listeners: {
            change: 'onBancoChange'
        }
    }, {
        xtype: 'banco-cuenta-selector',
        reference: 'cuentaBancoSelector',
        name: 'cuentaBancaria',
        fieldLabel: 'Cuenta',
        allowBlank: false
    }, {
        xtype: 'textfield',
        name: 'numeroCheque',
        fieldLabel: 'Número de cheque',
        labelWidth: 140,
        colspan: 2
    }],

    onAseguradoSelect(selector, asegurado) {
        this.lookup('estadoDeCuentaSelector').getStore().porNumeroAfiliacion(
            asegurado.get('numeroAfiliacion')
        );
    },
    onBancoChange(selector, banco) {
        this.lookup('cuentaBancoSelector').getStore().load({
            params: {bancoId: banco}
        });
    },

    onProcesarMovimientoTap() {
        let data = this.getForm().getFieldValues();
        this.getServicioCartera().procesar({
            numeroAfiliacion: data.numeroAfiliacion,
            tipo: 'ABONO',
            tipoMovimiento: 'PDPP',
            estadoDeCuentaId: data.estadoDeCuentaId,
            importe: data.importe,
            formaPago: data.formaPago,
            banco: data.banco,
            numeroCheque: data.numeroCheque
        }).then(resultado => {
            window.location = `/api/ingresos/certificado/descargar?transaccionId=${resultado.transaccion.id}&tipo=AFIL_PP`;
        });
    }
});