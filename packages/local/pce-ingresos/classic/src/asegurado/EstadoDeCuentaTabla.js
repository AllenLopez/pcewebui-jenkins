Ext.define('Pce.ingresos.asegurado.EstadoDeCuentaTabla', {
    extend: 'Ext.grid.Panel',
    xtype: 'asegurado-estado-de-cuenta-tabla',


    columns: [{
        text: 'Clave',
        dataIndex: 'conceptoClave',
        tooltip: `Identificador del concepto`,
    }, {
        text: 'Concepto',
        dataIndex: 'conceptoDescripcion',
        tooltip: `Nombre del concepto que se carga al estado de cuenta del afiliado`,
        flex: 3
    }, {
        text: 'Saldo',
        dataIndex: 'saldo',
        tooltip: `Total de cada concepto descontado del salario del afiliado.`,
        renderer: Ext.util.Format.usMoney,
        flex: 1
    }, {
        xtype: 'actioncolumn',
        width: 50,
        align: 'center',
        items: [{
            iconCls: 'x-fa fa-search',
            handler: 'onConsultarMovimientos',
            tooltip: 'Consultar movimientos',
            getClass(value, meta, record) {
                return (record.get('conceptoClave') !== 'B') ?
                    'x-fa fa-search' : // muestra el icono
                    'x-hide-display'; // esconde el icono
            },
            isDisabled(view, rowIndex, collIndex, item, record) {
                return (record.get('conceptoClave') === 'B');
            }
        }]
    }]
});