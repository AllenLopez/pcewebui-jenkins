Ext.define('Pce.ingresos.asegurado.EstadoDeCuentaAdminPanel', {
    extend: 'Ext.container.Container',
    xtype: 'asegurado-estado-de-cuenta-admin-panel',
    requires: [
        'Pce.ingresos.asegurado.EstadoDeCuentaAdminPanelController',

        'Pce.ingresos.asegurado.EstadoDeCuentaTabla',

        'Pce.ingresos.asegurado.EstadoDeCuenta',
        'Pce.afiliacion.asegurado.Asegurado',

        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.asegurado.AseguradoSelector',
        'Pce.afiliacion.asegurado.AseguradoDisplayer'
    ],

    controller: 'asegurado-estado-de-cuenta-admin-panel',
    viewModel: {
        data: {
            afiliado: null,
            dependencia: null
        }
    },

    layout: 'fit',

    numeroAfiliacion: undefined,

    items: [{
        xtype: 'asegurado-estado-de-cuenta-tabla',
        reference: 'tablaEstadosDeCuenta',
        title: 'Estados de cuenta de afiliado',
        store: {
            model: 'Pce.ingresos.asegurado.EstadoDeCuenta'
        },
        bind: {
            title: 'Estados de cuenta {dependencia.Descripcion}'
        },
        dockedItems: [{
            xtype: 'toolbar',
            reference: 'consultaEdoCtaForma',
            items: [{
                xtype: 'asegurado-selector',
                emptyText: 'Ingresa número de afiliación, nombre o RFC.',
                reference: 'aseguradoSelector',
                labelAlign: 'left',
                style: 'margin-left: 10px;',
                listeners: {
                    change: 'onCambioAfiliado'
                },
                flex: 2,
            }]
        }, {
            xtype: 'toolbar',
            hidden: true,
            bind: {
                hidden: '{!afiliado}'
            },
            items: [{
                xtype: 'asegurado-displayer'
            }]
        }]
    }],

    afterRender() {
        this.callParent();
    },
});