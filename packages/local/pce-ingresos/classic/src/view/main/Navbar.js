/**
 * Barra de navegación para el módulo de ingresos
 */
Ext.define('Pce.ingresos.view.main.Navbar', {
    extend: 'Ext.container.Container',
    xtype: 'navbar',
    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Split',
        'Ext.button.Segmented',
        'Ext.menu.Menu'
    ],

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        xtype: 'component',
        cls: 'site-bar',
        flex: 1
    }, {
        xtype: 'toolbar',
        items: {
            xtype: 'segmentedbutton',
            reference: 'menu',
            items: []
        }
    }]
});