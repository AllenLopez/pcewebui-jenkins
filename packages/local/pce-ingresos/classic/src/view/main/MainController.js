Ext.define('Pce.ingresos.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ingresos-main',

    requires: [
        'Pce.ingresos.Inicio',
        'Pce.ingresos.catalogo.CatalogoContenedor',
        'Pce.ingresos.asegurado.EstadoDeCuentaAdminPanel',
        'Pce.ingresos.dependencia.EstadoDeCuentaAdminPanel',
        'Pce.ingresos.nomina.CargaNominaAsistente',
        'Pce.ingresos.nomina.CargaNominaDinamicaPanel',
        'Pce.ingresos.nomina.CargaNominaPrestamosPanel',
        'Pce.ingresos.nomina.AfectacionNominaPrestamosPanel',
        'Pce.ingresos.facturacionElectronica.FacturacionElectronicaPanel',
        'Pce.ingresos.transaccion.TransaccionAdminNominaDinamicaPanel',
        'Pce.ingresos.transaccion.TransaccionAdminPanel',
        'Pce.ingresos.transaccion.cancelacionTransaccion.CancelacionTransaccionPanel',
        'Pce.ingresos.reporte.ReportePanel',
        'Pce.ingresos.reporte.afiliado.ReporteAfiliadoPanel',
        'Pce.ingresos.reporte.dependencia.ReporteDependenciaPanel',
        'Pce.ingresos.reporte.dependenciaConcepto.ReporteDependenciaConceptoPanel',
        'Pce.ingresos.reporte.saldoConcepto.ReporteSaldoConceptoPanel',
        'Pce.ingresos.reporte.antiguedadSaldosConcepto.ReporteAntiguedadSaldoConceptoPanel',
        'Pce.ingresos.reporte.adeudoDependencia.ReporteAdeudoDependenciaPanel',
        'Pce.ingresos.reporte.avisosCargo.ReporteAvisosCargoPanel',
        'Pce.ingresos.reporte.movimientoAfiliado.ReporteMovimientoAfiliadoPanel',
        'Pce.reporte.afiliado.ReporteAportacionVectorPanel',
        'Pce.reporte.afiliado.ReporteAportacionFondoPorPagarPanel',

        'Pce.ingresos.avisocargo.EmisionAvisoCargoPanel',
        'Pce.ingresos.avisocargo.ImpresionAvisoCargoPanel',
        'Pce.ingresos.poliza.ConsultaPolizaPanel',
        'Pce.ingresos.poliza.ExportacionPolizaPanel',
        'Pce.ingresos.poliza.captura.ConsultaCapturaPolizaPanel',
        'Pce.ingresos.poliza.captura.CapturaPolizaPanel',
        'Pce.ingresos.poliza.GeneraPolizaPanel',
        'Pce.ingresos.poliza.SeriePolizaPanel',
        'Pce.ingresos.poliza.TipoPolizaPanel',
        'Pce.ingresos.quincena.GeneraQuincenaPanel',
        'Pce.ingresos.aplicacionSaldo.AplicacionSaldoPanel',
        'Pce.ingresos.avisocargo.AvisoCargoManualPanel',
        'Pce.ingresos.notacredito.NotaCreditoDependenciaPanel',
       
        'Pce.ingresos.jubilado.procesos.CargaSaldoJubiladoPanel',
        'Pce.ingresos.jubilado.procesos.ConsultaSaldoJubiladoPanel',

        'Pce.ingresos.asegurado.procesos.MovimientoPanel',

        'Pce.ingresos.asegurado.procesos.devolucionFondo.EditorDevolucionFondoDetallePanel',

        'Pce.ingresos.asegurado.procesos.devolucionFondo.DevolucionFondoSolicitudPanel',
        'Pce.ingresos.asegurado.procesos.devolucionFondo.ConsultaSolicitudDevolucionPanel',
        'Pce.ingresos.asegurado.procesos.descuentoIndebido.ConsultaDescuentoIndebidoPanel',
        'Pce.ingresos.asegurado.proceso.descuentoIndebido.reconocimientoAntiguedad.DIReconocimientoAntiguedad',
        'Pce.ingresos.asegurado.proceso.descuentoIndebido.regimen.DescuentoIndebidoRegimenPanel',
        'Pce.ingresos.asegurado.proceso.descuentoIndebido.prestamo.DescuentoIndebidoPrestamoPanel',
        'Pce.ingresos.asegurado.procesos.descuentoIndebido.reconocimientoAntiguedad.EditorReconocimientoAntiguedadPanel',
        'Pce.ingresos.asegurado.procesos.solicitudLicencia.ConsultaSolicitudLicenciaPanel',
        'Pce.ingresos.asegurado.procesos.cargasaldo.CargaSaldoSinAportacionPanel',
        'Pce.ingresos.dependencia.diferencialsm.DiferencialSmPanel',
        'Pce.ingresos.reporte.recargos.ReporteRecargosPanel'
    ],

    routes: {
        // 'main': 'main',
        'catalogo/:catalogo': 'catalogo',
        'asegurado/estado-de-cuenta': 'aseguradoEstadoDeCuenta',
        'asegurado/carga-saldo-asegurado-cancelado': 'cargaSaldoAseguradoCancelado',
        'dependencia/estado-de-cuenta': 'dependenciaEstadoDeCuenta',
        'nomina/carga': 'nominaCarga',
        'nomina-dinamica/carga': 'nominaDinamicaCarga',
        'nomina-carga-prestamos/carga' : 'cargaNominaPrestamos',
        'nomina-afectacion/carga' : 'nominaAfectacion',
        'transacciones-nomina-dinamica/pendientes': 'transaccionesNominaDinamicaPendientes',
        'transacciones/pendientes': 'transaccionesPendientes',
        'transacciones/cancelacion': 'transaccionesCancelacion',
        'archivo-fact-electronica': 'archivoFacturacionElectronica',
        // 'reportes': 'reportes',
        'reportes/afiliado': 'reporteAfiliado',
        'reportes/dependencia': 'reporteDependencia',
        'reportes/dependencia-concepto': 'reporteDependenciaConcepto',
        'reportes/saldo-concepto': 'reporteSaldoConcepto',
        'reportes/antiguedad-saldo-concepto-dep': 'reporteAntiguedadSaldoConcepto',
        'reportes/adeudo-dependencia': 'reporteAdeudoDependencia',
        'reportes/avisos-cargo': 'reporteAvisosCargo',
        'reportes/movimiento-afil': 'reporteMovimientoAfil',
        'reportes/aportacion-vector': 'reporteAportacionVector',
        'reportes/aportaciones-por-pagar': 'reporteAportacionesPorPagar',
        'aviso-cargo/emision': 'emisionAvisoCargo',
        'aviso-cargo/consulta': 'consultaAvisoCargo',
        'aviso-cargo/nota-credito-dependencia': 'notaCreditoDependencia',
        'poliza/consulta': 'consultaPolizaPanel',
        'poliza/exportacion': 'exportacionPolizaPanel',
        'aviso-cargo/manual': 'avisoCargoManual',
        'descuento-indebido/reconocimiento-antiguedad': 'descuentoIndebidoReconocimientoAntiguedad',
        'descuento-indebido/regimen': 'descuentoIndebidoRegimen',
        'descuento-indebido/prestamo': 'descuentoIndebidoPrestamo',
        'descuento-indebido/consulta': 'consultaDescuentoIndebido',
        // 'edicion-solicitud-descuento-indebido-ra': 'edicionSolicitudDescuentoIndebidoRA',
        'edicion-solicitud-descuento-indebido-ra/:folio': 'edicionSolicitudDescuentoIndebidoRA',
        'solicitud-devolucion-fondo': 'solicitudDevolucionFondo',
        'consulta-solicitud-devolucion-fondo': 'consultaSolicitudDevolucionFondo',
        'edicion-solicitud-devolucion-fondo/:folio': 'edicionSolicitudDevolucionFondo',
        'jubilado/carga-saldo': 'cargaSaldoJubilado',
        'consulta-actualizacion-saldo': 'consultaSaldoJubilado',
        'asegurado/movimiento/:tipo': 'aseguradoMovimiento',
        'catalogo/serie-poliza': 'seriePoliza',
        'catalogo/tipo-poliza': 'tipoPoliza',
        'catalogo/quincenas': 'generaQuincena',
        'poliza/generar': 'generaPolizaPanel',
        'poliza/consulta-captura': 'consultaCapturaPolizaPanel',
        'poliza/captura': 'capturaPolizaPanel',
        'diferencial-sm': 'diferencialSm',
        'aplicacion-saldo': 'aplicacionSaldo',
        'consulta-solicitud-licencia': 'consultaSolicitudLicencia',
        'recargos/reporte': 'reporteRecargos',
        'cerrar-sesion': 'cerrarSesion'
 
    },
    beforeRender: function(){
        let me = this;
        Ext.Ajax.request({
            url: '/api/seguridad/autenticacion/estatus?host=/apps/Ingresos/index.html',
            method: 'GET',
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                if(resultado.estatus === 'anonimo'){
                    window.location = '/apps/Seguridad/index.html#login';
                    return;
                }
                if(window.location.pathname !== resultado.redireccion){
                    window.location = resultado.redireccion;
                    return;
                } 

                let menu = me.lookup('menu');

                menu.add(resultado.menu);
            }
        });


    },

    render(){

    },

    main() {
        this.view.renderPage({
            xtype: 'inicio'
        });
    },

    cerrarSesion() {
        Ext.Ajax.request({
            url: '/api/seguridad/autenticacion/cerrar-sesion',
            method: 'GET',
            callback(opt, success, response) {
                let resultado = Ext.decode(response.responseText);
                if(resultado.estatus === 'anonimo'){
                    window.location = '/apps/Seguridad/index.html#login';
                    return;
                }
            }
        });
    },

    catalogo(catalogo) {
        this.view.renderPage({
            xtype: 'catalogo-contenedor',
            catalogo: catalogo
        });
    },

    aseguradoEstadoDeCuenta() {
        this.view.renderPage({
            xtype: 'asegurado-estado-de-cuenta-admin-panel'
        });
    },

    dependenciaEstadoDeCuenta() {
        this.view.renderPage({
            xtype: 'dependencia-estado-de-cuenta-admin-panel'
        });
    },

    nominaCarga() {
        this.view.renderPage({
            xtype: 'carga-nomina-asistente'
        });
    },

    nominaDinamicaCarga() {
        this.view.renderPage({
            xtype: 'carga-nomina-dinamica-panel'
        });
    },

    cargaNominaPrestamos() {
     
        this.view.renderPage({
            xtype: 'carga-nomina-prestamos-panel',
          
        });
    },

    nominaAfectacion(){
       this.view.renderPage({
           xtype: 'afectacion-nomina-prestamos-panel'
       })
    },

    transaccionesPendientes() {
        this.view.renderPage({
            xtype: 'transaccion-admin-panel'
        });
    },

    transaccionesCancelacion() {
        this.view.renderPage({
            xtype: 'cancelacion-transaccion-panel'
        });
    },

    transaccionesNominaDinamicaPendientes() {
        this.view.renderPage({
            xtype: 'transaccion-admin-nomina-dinamica-panel'
        });
    },

    archivoFacturacionElectronica() {
        this.view.renderPage({
            xtype: 'facturacion-electronica-panel'
        });
    },

    aplicacionSaldo() {
        this.view.renderPage({
            xtype: 'ingresos-aplicacion-saldo'
        });
    },

    cargaSaldoJubilado() {
        this.view.renderPage({
            xtype: 'carga-saldo-jubilado-panel'
        });
    },

    cargaSaldoAseguradoCancelado() { 
        this.view.renderPage({
            xtype: 'carga-saldo-sin-aportacion-panel' 
        });
    },

    consultaSaldoJubilado() {
        this.view.renderPage({
            xtype: 'consulta-saldo-jubilado-panel'
        });
    },

    reportes() {
        this.view.renderPage({
            xtype: 'reporte-panel'
        });
    },

    reporteAfiliado() {
        this.view.renderPage({
            xtype: 'reporte-afiliado-panel'
        });
    },

    reporteDependencia() {
        this.view.renderPage({
            xtype: 'reporte-dependencia-panel'
        });
    },

    reporteDependenciaConcepto(){
        this.view.renderPage({
            xtype: 'reporte-dependencia-concepto-panel'
        });
    },
    reporteSaldoConcepto(){
        this.view.renderPage({
            xtype: 'reporte-saldo-concepto-panel'
        });
    },
    reporteAntiguedadSaldoConcepto(){
        this.view.renderPage({
            xtype: 'reporte-antiguedad-saldo-concepto-panel'
        });
    },

    reporteAdeudoDependencia() {
        this.view.renderPage({
            xtype: 'reporte-adeudo-dependencia-panel'
        });
    },

    reporteAvisosCargo() {
        this.view.renderPage({
            xtype: 'reporte-avisos-cargo-panel'
        });
    },

    reporteMovimientoAfil() {
        this.view.renderPage({
            xtype: 'reporte-movimiento-afil-panel'
        });
    },

    reporteAportacionVector() {
        this.view.renderPage({
            xtype: 'reporte-afil-vector'
        });
    },

    reporteAportacionesPorPagar() {
        this.view.renderPage({
            xtype: 'reporte-aportacion-fondo-pagar'
        });
    },

    emisionAvisoCargo() {
        this.view.renderPage({
            xtype: 'emision-aviso-cargo-panel'
        });
    },

    consultaAvisoCargo() {
        this.view.renderPage({
            xtype: 'impresion-aviso-de-cargo-panel'
        });
    },

    consultaCapturaPolizaPanel() {
        this.view.renderPage({
            xtype: 'consulta-captura-poliza-panel'
        });
    },

    capturaPolizaPanel() {
        this.view.renderPage({
            xtype: 'captura-poliza-panel'
        });
    },

    consultaPolizaPanel() {
        this.view.renderPage({
            xtype: 'consulta-poliza-panel'
        });
    },

    exportacionPolizaPanel() {
        this.view.renderPage({
            xtype: 'exportacion-poliza-panel'
        });
    },

    generaPolizaPanel() {
        this.view.renderPage({
            xtype: 'genera-poliza-panel'
        });
    },

    avisoCargoManual() {
        this.view.renderPage({
            xtype: 'aviso-de-cargo-manual-panel'
        });
    },
    
    notaCreditoDependencia(){
        this.view.renderPage({
            xtype: 'nota-credito-dependencia-panel'
        });
    },

    descuentoIndebido() {
        this.view.renderPage({
            xtype: 'consulta-descuento-indebido'
        });
    },
    consultaDescuentoIndebido() {
        this.view.renderPage({
            xtype: 'consulta-descuento-indebido'
        });
    },
    descuentoIndebidoReconocimientoAntiguedad(){
        this.view.renderPage({
            xtype: 'di-reconocimiento-antiguedad'
        });
    },
    descuentoIndebidoRegimen() {
        this.view.renderPage({
            xtype: 'di-regimen'
        });
    },
    descuentoIndebidoPrestamo(){
        this.view.renderPage({
            xtype: 'di-prestamo'
        });
    },
    edicionSolicitudDescuentoIndebidoRA(folio){
        this.view.renderPage({
            xtype: 'editor-reconocimiento-antiguedad-panel',
            solicitudId: folio
        });

    },    
    devolucionFondo() {
        this.view.renderPage({
            xtype: 'devolucion-fondo-panel'
        });
    },
    solicitudDevolucionFondo() {
        this.view.renderPage({
            xtype: 'fondo-solicitud-devolucion-panel'
        });
    },

    consultaSolicitudDevolucionFondo() {
        this.view.renderPage({
            xtype: 'consulta-solicitud-devolucion-panel'
        });
    },

    edicionSolicitudDevolucionFondo(folio) {
        this.view.renderPage({
            xtype: 'editor-devolucion-fondo-panel',
            solicitudId: folio
        });
    },

    seriePoliza() {
        this.view.renderPage({
            xtype: 'serie-poliza-panel'
        });
    },

    tipoPoliza() {
        this.view.renderPage({
            xtype: 'tipo-poliza-panel'
        });
    },

    generaQuincena() {
        this.view.renderPage({
            xtype: 'genera-quincena-panel'
        });
    },

    aseguradoMovimiento(tipo) {
        this.view.renderPage({
            xtype: 'container',
            items: {
                xtype: 'asegurado-movimiento-panel',
                tipoMovimiento: tipo
            }
        });
    },

    consultaSolicitudLicencia() {
        this.view.renderPage({
            xtype: 'consulta-solicitud-licencia'
        });
    },

    diferencialSm() {
        this.view.renderPage({
            xtype: 'diferencial-sm-panel'
        });
    },

    reporteRecargos() {
        this.view.renderPage({
            xtype: 'reporte-recargo-panel'
        });
    }
});