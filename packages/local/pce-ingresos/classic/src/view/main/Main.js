Ext.define('Pce.ingresos.view.main.Main', {
    extend: 'Pce.view.main.Main',
    xtype: 'ingresos-main',

    requires: [
        'Pce.ingresos.view.main.MainController',

        // Barra de navegación para el módulo de ingresos
        'Pce.ingresos.view.main.Navbar'
    ],

    controller: 'ingresos-main',

    dockedItems: [{
        // Barra de navegación
        xtype: 'navbar',
        dock: 'top'
    }]
});