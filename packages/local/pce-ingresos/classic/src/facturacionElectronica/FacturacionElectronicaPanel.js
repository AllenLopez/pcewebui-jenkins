/**
 * @class Pce.ingresos.facturacionelectronica.FacturacionElectronicaPanel
 * @extends Ext.panel.Panel
 * @xtype facturacion-electronica-panel
 * Pantalla de generación de archivo para facturación electrónica segpun tipo de movimiento y rango de fechas
 */
Ext.define('Pce.ingresos.facturacionElectronica.FacturacionElectronicaPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'facturacion-electronica-panel',
    
    requires: [
        'Ext.layout.container.Form',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',

        'Pce.ingresos.facturacionElectronica.FacturacionElectronicaStore',
        'Pce.ingresos.fecturacionElectronica.FacturacionElectronicaController'
    ],

    title: 'Generación de archivo de facturación electrónica',
    layout: 'fit',
    
    controller: 'facturacion-electronica-panel',
    viewModel: {
        data: {
            tipoMovimiento: null,
            fechaInicial: null,
            fechaFinal: null
        }
    },

    items:[{
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            items: [{
                xtype: 'combobox',
                fieldLabel: 'Tipo de movimiento',
                forceSelection: true,
                valueField: 'reporteId',
                displayField: 'nombre',
                store: {
                    type: 'facturacion-electronica',
                    autoLoad: true
                },
                labelWidth: 150,
                width: 450,
                bind: {
                    selection: '{tipoMovimiento}'
                   
                }
            }, {
                xtype: 'datefield',
                reference: 'fechaInicio',
                fieldLabel: 'Desde',
                allowBlank: false,
                name: 'fechaInicio',
                bind: {
                    value: '{fechaInicial}',
                    maxValue: '{fechaFinal}'
                }
            }, {
                xtype: 'datefield',
                fieldLabel: 'Hasta',
                reference: 'fechaFinal',
                allowBlank: false,
                name: 'fechaFinal',
                bind: {
                    minValue: '{fechaInicial}',
                    disabled: '{!fechaInicial}',
                    value: '{fechaFinal}'
                }
            }, {
                text: 'Generar archivo',
                disabled: true,
                bind: {
                    disabled: '{!fechaFinal}'
                },
                handler: 'onGenerarArchivo',
            }]
        }]
    }]
    
});