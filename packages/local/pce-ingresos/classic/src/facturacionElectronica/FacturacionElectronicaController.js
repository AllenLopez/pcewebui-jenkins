/**
 * @class Pce.ingresos.fecturacionElectronica.FacturacionElectronicaController
 * @extends Ext.app.ViewController
 *@alias controller.facturacion-electronica-panel
 * Controllador de pantalla para descargar archivo de facturación electrónica basado en
 * tipo, fecha inicial y fecha final
 */
Ext.define('Pce.ingresos.fecturacionElectronica.FacturacionElectronicaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.facturacion-electronica-panel',

    onGenerarArchivo() {
        let vm = this.getViewModel();
        
        let reporteId = vm.data.tipoMovimiento.data.reporteId;
        let fecha1 = Ext.util.Format.date(vm.data.fechaInicial, 'd/m/Y');
        let fecha2 = Ext.util.Format.date(vm.data.fechaFinal, 'd/m/Y');
        
        this.generarArchivo(reporteId, fecha1, fecha2);
    },

    generarArchivo(reporteId, fecha1, fecha2) {
        window.open(`/api/ingresos/facturacion-electronica/descargar?tipoMovId=${reporteId}&fecha1=${fecha1}&fecha2=${fecha2}`);
    }

});