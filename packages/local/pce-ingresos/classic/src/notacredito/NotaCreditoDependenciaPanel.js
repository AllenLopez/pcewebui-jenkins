/**
 * @author Alan López
 * @class NotaCredito
 * @extends Panel
 * @xtype nota-credito-dependencia
 * @desc Formulario para generar notas de credito a dependencias
 */
Ext.define('Pce.ingresos.notacredito.NotaCreditoDependenciaPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'nota-credito-dependencia-panel',
    reference: 'notaCreditoDependenciaPanel',

    requires: [
        'Pce.ingresos.notacredito.NotaCreditoDependenciaController',
        'Pce.afiliacion.dependencia.DependenciaSelector',
        'Pce.afiliacion.dependencia.DependenciaStore',

        'Pce.ingresos.dependencia.PagoManualConceptosTabla',
    ],

    controller: 'nota-credito-dependencia-controller',

    viewModel: {
        data: {
            dependencia: null,
            fechaMovimiento: new Date(),
            avisoCargo: null,
            importe: null,
            botonInvalido: false
        }
    },
    
    title: 'Emisión de notas de crédito',
    
     items: [{
        margin: 5,
        items: [{
            layout: 'hbox',
            items: [{
                xtype: 'dependencia-selector',
                reference: 'dependenciaSelector',
                name: 'dependencia',
                allowBlank: false,
                margin: 5,
                flex: 3,
                labelWidth: 90,
                listeners: {
                    change: 'onSeleccionDependencia'
                },
                bind: {
                    selection: '{dependencia}'
                }
            }, {
                xtype: 'emision-avisocargo-selector',
                reference: 'emisionAvisoCargoSelector',
                name: 'avisocargo',
                emtpyText: 'Sin datos encontrados',
                labelWidth: 40,
                margin: 5,
                flex: 1,
                listeners: {
                    change: 'onSeleccionAvisoCargo'
                },
                bind: {
                    selection: '{avisoCargo}'
                }
            },
            {
                xtype: 'datefield',
                reference: 'fechaMovimiento',
                fieldLabel: 'Fecha',
                labelWidth: 40,
                maxValue: new Date(),
                margin: 5,
                flex: 1,
                allowBlank: false,
                format: 'd/m/Y',
                bind: 
                {
                    value: '{fechaMovimiento}'
                    
                },
            
            }]
        }]
    }, {
        items: [{
            xtype: 'ingresos-pago-manual-concepto-tabla',
            reference: 'tablaEstadosdeCuenta',
            store: {
                type: 'dependencia-estados-de-cuenta',
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/api/ingresos/dependencia/estado-de-cuenta/emision-nota-credito-dependencia'
                }
            }
        }]
    }],
    buttons: [{
        text: 'Procesar Movimiento',
        reference: 'botonProcesar',
        handler: 'onCrearNotaCredito',
        bind: {
            disabled: '{!botonInvalido}'
        }
    }]
});

