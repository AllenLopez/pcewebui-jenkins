/**
 * @author: Alan López
 * @class
 * @description Controlador de formulario de notas de credito dependencias 
 * que realiza un registro de notas de creito para la dependencia dado un 
 * folio de aviso de cargo y una fecha para procesar un movimiento.
 */
Ext.define('Pce.ingresos.notacredito.NotaCreditoDependenciaController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.nota-credito-dependencia-controller',

    requires: [
        'Ext.window.Toast',
        'Pce.ingresos.transaccion.Transaccion'
    ],

    sumaTotal: 0,

    onCrearNotaCredito() {
        let viewModel = this.getViewModel();
        let dependenciaFiltro = this.lookup('dependenciaSelector');
        let EmisionFiltro = this.lookup('emisionAvisoCargoSelector');
        let FechaFiltro = this.lookup('fechaMovimiento');
        let storeConceptos = this.lookup('tablaEstadosdeCuenta').getStore();
        const fechaMovimiento = viewModel.getData().fechaMovimiento;
        if (this.sumaTotal > 0 && fechaMovimiento) {
        Ext.Msg.confirm(
            'Confirmar operación',
            `Se generará un nueva nota de crédito, se afectará el estado de 
            cuenta para la dependencia y los conceptos correspondientes. 
            ¿Deseas proceder?`,
            ans => {
                if (ans === 'yes') {
                    Ext.getBody().mask('Creando nota de crédito...');
                    Ext.Ajax.request({
                        method: 'POST',
                        url: '/api/ingresos/nota-credito',
                        jsonData: {
                            dependenciaId: viewModel.get('dependencia').getId(),
                            emisionAvisoCargoId: viewModel.get('avisoCargo').getId(), 
                            importe: this.sumaTotal,
                            fechaMovimiento: fechaMovimiento,
                            EstadosdeCuenta: this.lookup('tablaEstadosdeCuenta')
                            .getStore()
                            .getRange()
                            .map(model => model.data)
                        },
                        success(response) {
                            const resp = JSON.parse(response.responseText);
                            if (resp.exito) {
                                Ext.Msg.alert('Operación exitosa',
                                `La operación se completó correctamente.
                                Se generó la nota de crédito: ${resp.folio}, con transacción: ${resp.transaccionId}`);
                                window.open(`/api/ingresos/nota-credito/descargar?Pfolio=${resp.folio}`);
                                FechaFiltro.reset();
                                EmisionFiltro.reset();
                                dependenciaFiltro.reset();
                                storeConceptos.removeAll();
                            }
                        },
                        failure(response) {
                            Ext.Msg.alert('error', `Ocurrió un error al intentar procesar los datos: ${mensaje}`);
                        }
                        
                    });
                    Ext.getBody().unmask();
                }
            }
        );
      
        }else{
            Ext.Msg.alert(
                'Total Incorrecto',
                `El total a liquidar debe ser mayor que 0 y/o la fecha es invalida`
            );
        }
    },

    onSeleccionFechaMovimiento(fechaMovimiento){
        let selector = this.lookup('fechaMovimiento');
        selector.getStore().load({
            params: {
                fechaMovimiento: fechaMovimiento
            }
        })
    },

    onSeleccionDependencia(component, dependenciaId) {    
        let selector = this.lookup('emisionAvisoCargoSelector');
        selector.getStore().getProxy().setExtraParams({
            dependenciaId: dependenciaId
        });
        selector.getStore().load();

    },
    
    onSeleccionAvisoCargo(selector, avisoCargoId) {
        const dependenciaId = this.getViewModel().getData().dependencia.getId();
            if (avisoCargoId) {
            this.lookup('tablaEstadosdeCuenta').getStore().load({
                params: {
                    avisoCargoId: avisoCargoId,
                    dependenciaId: dependenciaId,
               },
            });
        }
    },
    validaImporte(val) {
        let edoCta = this.ownerCt.editingPlugin.estadoDeCuenta;
        let saldo = edoCta.get('saldo');
        if (val > saldo) {
            return `El importe a liquidar no debe ser mayor a 
            ${Ext.util.Format.usMoney(saldo)}`;
        }
        return true;
    },

    invalidaBoton() {
            this.setViewModelDataProp('botonInvalido', false);
            this.lookup('botonProcesar').disable();
    },

    onBeforeEdit(editor, context) {
        editor.estadoDeCuenta = context.record;
    },

    onValidateEdit(editor, context) {
        let saldo = context.record.get('saldo');
        if (context.value > saldo) {
            Ext.Msg.alert(
                'Importe incorrecto',
                `El importe a liquidar no debe ser mayor a ${Ext.util.Format.usMoney(saldo)}`,
            );
            editor.cancelEdit();
        }
    },
    onSummary(value, summaryData, dataIndex, context) {
        let saldo = context.record.get('saldo');
        if (value !== undefined && value > 0 && saldo >= value) {
            this.sumaTotal = Math.round(value * 100) / 100;
            this.lookup('botonProcesar').enable();
        }
        else{
            this.lookup('botonProcesar').disable();
        }
        return `<b>Total a Liquidar: ${Ext.util.Format.usMoney(value)}</b>`;
    },  
    
});