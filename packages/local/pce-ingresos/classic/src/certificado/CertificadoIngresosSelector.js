/**
 * @author Felipe Fierro
 *
 * @class
 * @name Pce.ingresos.certificado.CertificadoIngresosSelector
 */
Ext.define('Pce.ingresos.certificado.CertificadoIngresosSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'certificado-ingresos-selector',
    requires: [
        'Pce.ingresos.certificado.CertificadoIngresos'
    ],

    valueField: 'id',
    displayField: 'certificado',
    fieldLabel: 'Certificado',
    forceSelection: true,
    queryMode: 'local',

    store: {
        type: 'certificado-ingresos'
    },
    displayTpl: [
        '<tpl for=".">',
            '{certificado} - [{descripcionConcepto}]',
        '</tpl>'
    ],
    tpl: [
        '<table style="width: 100%; border-collapse: collapse;">',
        '<tbody>',
            '<tpl for=".">',
                '<tr role="option" class="x-boundlist-item">',
                    '<td><b>{certificado}</b> - [{descripcionConcepto}]   </td>',
                '</tr>',
            '</tpl>',
        '</tbody>',
    '</table>'
    ]
});