/**
 * @author Rubén Maguregui
 */
Ext.define('Pce.ingresos.FormaPagoSelector', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'forma-pago-selector',

    requires: [
        'Pce.ingresos.formaPago.FormaPagoStore'
    ],

    fieldLabel: 'Forma de pago',
    valueField: 'id',
    displayField: 'descripcion',
    forceSelection: true,
    queryMode: 'local',

    store: {
        type: 'forma-pago',
        autoLoad: true
    }
});