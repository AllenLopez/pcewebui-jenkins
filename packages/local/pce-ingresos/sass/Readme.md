# pce-ingresos/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    pce-ingresos/sass/etc
    pce-ingresos/sass/src
    pce-ingresos/sass/var
