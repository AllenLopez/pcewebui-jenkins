# pce-theme/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    pce-theme/sass/etc
    pce-theme/sass/src
    pce-theme/sass/var
