/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Configuracion.Application', {
    extend: 'Ext.app.Application',

    name: 'Configuracion',

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    stores: [
        // TODO: add global / shared stores here
    ],

    launch: function () {
        Ext.util.Format.decimalSeparator = '.';
        Ext.util.Format.thousandSeparator = ',';
        Ext.Ajax.setTimeout(1000 * 60 * 10);

        //Manejo de la sesion
        let timer = null;
        // let sessionTimeout = 60 * 60;
        let sessionTimeout = 9000000;
        Ext.Ajax.on('requestcomplete', function () {
            if (timer) {
                clearTimeout(timer);
                timer = null;
            }

            timer = setTimeout(
                function () {
                    Ext.MessageBox.show({
                        title:'Sesión terminada',
                        msg: `Se cerró sesión por inactividad.`,
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.INFO,
                        fn:() => {
                            window.location = '/apps/Seguridad/index.html#login';
                        }
                    });
                }, sessionTimeout * 1000
            );

        });

        Ext.Ajax.on('requestexception', function (conn, response) {
            if (response.status === 401) {
                window.location = '/apps/Seguridad/index.html#login';
            }
        });
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
